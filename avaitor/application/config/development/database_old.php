<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

/*  */

/*
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = 'cdn123';
$db['default']['database'] = 'coyote';
*/

/* connection for master for write only */
$db['default']['hostname'] = '10.10.10.2';
$db['default']['username'] = 'coyotev3';
$db['default']['password'] = 'hzKKPYnCYt9NSMjH';
$db['default']['database'] = 'coyotev3';

$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = 'ava_';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = TRUE;

//$db['mssql']['hostname'] = '192.168.0.15\MSSQL2012';
/*$db['mssql']['hostname'] = '10.10.10.4\MSSQL2012';
$db['mssql']['username'] = 'coyote';
$db['mssql']['password'] = 'cdn123';
$db['mssql']['database'] = 'AA01Sandbox';*/
$db['mssql']['hostname'] = 'dbs99.coyotepos.com.au';
$db['mssql']['username'] = 'CSAdmin';
$db['mssql']['password'] = '$qu1dlyD1ddly';
$db['mssql']['database'] = 'AASandbox';

$db['mssql']['dbdriver'] = 'mssql';
$db['mssql']['dbprefix'] = '';
$db['mssql']['pconnect'] = FALSE;
$db['mssql']['db_debug'] = FALSE;
$db['mssql']['cache_on'] = FALSE;
$db['mssql']['cachedir'] = '';
$db['mssql']['char_set'] = 'utf8';
$db['mssql']['dbcollat'] = 'utf8_general_ci';
$db['mssql']['swap_pre'] = '';
$db['mssql']['autoinit'] = TRUE;
$db['mssql']['stricton'] = FALSE;

 /* connection for slave for read only 
$db['read']['hostname'] = '10.10.10.21';
$db['read']['username'] = 'airdoc';
$db['read']['password'] = 'MeBaH8hKErt22vdU';
$db['read']['database'] = 'airdoc';
*/


$db['read']['hostname'] = '10.10.10.2';
$db['read']['username'] = 'coyotev3';
$db['read']['password'] = 'hzKKPYnCYt9NSMjH';
$db['read']['database'] = 'coyotev3';

$db['read']['dbdriver'] = 'mysql';
$db['read']['dbprefix'] = 'ava_';
$db['read']['pconnect'] = TRUE;
$db['read']['db_debug'] = TRUE;
$db['read']['cache_on'] = FALSE;
$db['read']['cachedir'] = '';
$db['read']['char_set'] = 'utf8';
$db['read']['dbcollat'] = 'utf8_general_ci';
$db['read']['swap_pre'] = '';
$db['read']['autoinit'] = TRUE;
$db['read']['stricton'] = TRUE;

/* End of file database.php */
/* Location: ./application/config/database.php */
