<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Manage Beacons actions
 *
 * @package    Coyote
 * @category   Controllers
 * @author     Tosif Qureshi 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Beacon extends Admin_Controller
{

	/**
	 * Controller constructor sets the Title and Permissions
	 */
	 
	/*Offers : Start*/
	
	public function __construct() {
		parent::__construct();
		Template::set('toolbar_title', 'Offer');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('beacon_model')){
			$this->load->model('beacon_model', 'beacon_model');
		}
		$this->lang->load('beacon_admin');
		$this->load->library('form_validation');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index() {

		$beacons = $this->beacon_model->get_beacons_listing(); 		
		Template::set('beacons',$beacons);
		Template::set_view('admin/beacon/listing');
		Template::render();
	}//end index
	
	//--------------------------------------------------------------------

	/**
	 * Function to manage beacon form
	 * @input : beacon_id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($beacon_id=0) {
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$beacon_id = decode($beacon_id); // decode offer id
		$data = array();
		
		if((isset($_POST['save'])) && $beacon_id=='') {
			if($this->save_beacon()) {       
					Template::set_message(lang('message_saved_beacon'),'success');
					redirect('backend/beacon');
			}
		} else if(isset($_POST['save'])) { 
			if($this->save_beacon('update',$beacon_id)) {
					Template::set_message(lang('message_update_beacon'),'success');
					redirect('backend/beacon');
			}
		}
		if($beacon_id) {
			$beacon = $this->beacon_model->get_beacons_listing($beacon_id);	
			if(empty($beacon)) {
				Template::set_message(lang('message_invalid_beacon'),'error');
				redirect('backend/beacon');
			}	
			$data['beacon_title']	= lang('edit_beacon');
			$data['beacon_id']		= encode($beacon_id);
			$data['beacon_code']	= $beacon->beacon_code;		
			$data['description']	= $beacon->description;		
			$data['status']			= $beacon->status;	
			$data['created_at']		= $beacon->created_at;	
			$data['store_id']		= $beacon->store_id;
		} else {
			$data['beacon_title']	= lang('add_new_beacon');
			$data['beacon_id']		= '';
			$data['beacon_code']	= '';	
			$data['description']	= '';		
			$data['status']			= '';
			$data['created_at']		= '';
			$data['store_id']		= '';
		}	
		$data['stores'] = $this->beacon_model->get_store_list();
		Template::set('data',$data);
		Template::set_view('admin/beacon/beacon_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store beacon in database
	 * @input : type , beacon_id
	 * @return: void
	 * @access: public
	 */
	public function save_beacon($type = 'insert',$beacon_id=0) {
		$data = array();
		if($beacon_id != 0) {
			$_POST['beacon_id'] = $beacon_id;
			$data['beacon_id'] = $beacon_id;
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('beacon_code', 'lang:beacon_code', 'trim|required');		
	
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		
		// set post values in store bundle
		$data['beacon_code']    = $this->input->post('beacon_code');
		$data['description']    = $this->input->post('description');
		$data['store_id']    	= $this->input->post('store_id');
		$data['updated_by'] 	= $this -> session -> userdata('user_id');
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_delete'] 		= 0;
		
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->beacon_model->manage_store_beacon($data);
		}
		else if ($type == 'update') {
			$id = $this->beacon_model->manage_store_beacon($data);
		}
		return $id;

	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to update beacon status
	 * @input : beacon_id , status
	 * @return: void
	 * @access: public
	 */
	public function beacon_status($beacon_id='',$status='') {
		$data['beacon_id'] = decode($beacon_id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_model->manage_store_beacon($data);
		Template::set_message(lang('message_status_beacon'),'success');
		redirect('backend/beacon');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to remove beacon
	 * @input : beacon_id , status
	 * @return: void
	 * @access: public
	 */
	public function delete_beacon($beacon_id='') {
		$data['beacon_id']  = decode($beacon_id);
		$data['is_delete']  = 1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_model->manage_store_beacon($data);
		Template::set_message(lang('message_delete_beacon'),'success');
		redirect('backend/beacon');
	}
	
	function check_active_store() {   	// controller is called will add/edit date time through ajax request  
		$data['beacon_id'] = decode($this->input->post('beacon_id'));
		$data['store_id'] = $this->input->post('store_id');
		echo $offerCount = $this->beacon_model->check_store($data);  // returns the no of offer associate with beacon and store
		die;
	}
	
	function check_active_beacon_offer() {   	// called while deactive and delete the beacon from beacon listing
		$data['beacon_id'] = decode($this->input->post('beacon_id'));
		$data['store_id'] = $this->input->post('store_id');
		echo $offerCount = $this->beacon_model->get_active_beacon_offer($data);  // returns the no of activer offer associate with beacon
		die;
	}
	
	function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'offer_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/beacon_offer');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	} //to_upload_image
	/*Offers : End*/
		
	
	//--------------------------------------------------------------------

	/**
	 * Show listing of simple offers
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function simple_offer() {
		// fetch sinple type offer data
		$offers = $this->beacon_offers_model->get_offer_listing(0,1); 
		// set params in view loading
		Template::set('offers',$offers);
		Template::set_view('admin/beacon_offer/simple_listing');
		Template::render();
	}//end index
	
	//--------------------------------------------------------------------

	/**
	 * Display beacon setting form
	 *
	 * @return void
	 */
	public function settings() {
		// get setting data
		$setting_data = $this->beacon_model->get_beacons_setting('beacon_notification_time');
		$beacon_notification_time = (!empty($setting_data)) ? $setting_data->value : '';
				
		Template::set('beacon_notification_time',$beacon_notification_time);
		Template::set_view('admin/beacon/beacon_settings');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store beacon setting in database
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	public function store_settings() {
		$data = array();
		// apply validation rules for input params
		$this->form_validation->set_rules('beacon_notification_time', 'lang:beacon_notification_time', 'trim|required|digits');		
		// check validation 
		if($this->form_validation->run() === FALSE) {
			Template::set_message("Invalid input value",'error');
			redirect('backend/beacon/settings');
		}
		
		// set post values in store settings
		$beacon_notification_time    = $this->input->post('beacon_notification_time');
		$form_action    = $this->input->post('form_action');
		$this->beacon_model->save_beacon_setting($beacon_notification_time,$form_action);
		// redirect to setting page with message
		Template::set_message(lang('beacon_setting_success_msg'),'success');
		redirect('backend/beacon/settings');
		
	}
    
    
    //--------------------------------------------------------------------

	/**
	 * Display Cart Setting Form
	 *
	 * @return void
	 */
	public function cart_settings() {
		// get setting data
		$setting_data = $this->beacon_model->get_beacons_setting('cart_product_limit');
		$cart_product_limit = (!empty($setting_data)) ? $setting_data->value : '';
				
		Template::set('cart_product_limit',$cart_product_limit);
		Template::set_view('admin/beacon/cart_settings');
		Template::render();
	}
    
    /**
	 * Function to store cart setting in database
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	public function cart_store_settings() {
		$data = array();
		// apply validation rules for input params
		$this->form_validation->set_rules('cart_product_limit', 'lang:cart_product_limit', 'trim|required|digits');		
		// check validation 
		if($this->form_validation->run() === FALSE) {
			Template::set_message("Invalid input value",'error');
			redirect('backend/beacon/cart_settings');
		}
		
		// set post values in store settings
		$cart_product_limit    = $this->input->post('cart_product_limit');
		$form_action    = $this->input->post('form_action');
		$this->beacon_model->save_cart_setting($cart_product_limit,$form_action);
		// redirect to setting page with message
		Template::set_message(lang('beacon_setting_success_msg'),'success');
		redirect('backend/beacon/cart_settings');
		
	}
	
  
		
}//end class
