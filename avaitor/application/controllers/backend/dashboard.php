<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Avaitor
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Avaitor
 * @author    Avaitor Dev Team
 * @copyright Copyright (c) 2011 - 2012, Avaitor Dev Team
 * @license   http://guides.ciAvaitor.com/license.html
 * @link      http://ciAvaitor.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Reports controller
 *
 * The base controller which displays the homepage of the Admin Reports context in the Avaitor app.
 *
 * @package    Avaitor
 * @subpackage Controllers
 * @category   Controllers
 * @author     Avaitor Dev Team
 * @link       http://guides.ciAvaitor.com/helpers/file_helpers.html
 *
 */
class Dashboard extends Admin_Controller
{


	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		Template::set('toolbar_title', 'Reports');

		$this->auth->restrict('Site.Reports.View');
		$this->users_tbl = 'ava_users';
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index()
	{
		$month_array = array();
		for($month=0;$month < 6;$month++) {
			$month_array[$month]['mNumber']= date('n', strtotime("-$month month"));
			$month_array[$month]['mName']= date('F', strtotime("-$month month"));
		}
		$user_count  		 	= $this->common_model->get_users_count();
		$stores_offers_count 	= $this->common_model->stores_offers_count();
		$simple_offer_count  	= $this->common_model->simple_stores_offer_count();
		$device_count		 	= $this->common_model->device_count();
		$beacons_count		 	= $this->common_model->beacons_count();
		$reward_campaign_count  = $this->common_model->reward_campaign_count();
		$scratch_n_win_winners  = $this->common_model->scratch_n_win_winners();
		$campaign_compitition_log  = $this->manage_scratch_win_yearly_log(date('Y'));
		$events				    = $this->common_model->events_counts();
		$product_loyalty	    = $this->common_model->product_loyalty_count();
		$competitions		    = $this->common_model->competitions_count();
		$joined_device_log 		= $this->common_model->joined_user_device_log($month_array);
		Template::set('user_count',$user_count);
		Template::set('stores_offers_count',$stores_offers_count);
		Template::set('stores_offers_count',$stores_offers_count);
		Template::set('device_count',$device_count);
		Template::set('beacons_count',$beacons_count);
		Template::set('reward_campaign_count',$reward_campaign_count);
		Template::set('campaign_compitition_log',!empty($campaign_compitition_log) ? ($campaign_compitition_log) : "");
		Template::set('events',$events);
		Template::set('product_loyalty',$product_loyalty);
		Template::set('competitions',$competitions);
		Template::set('joined_device_log',$this->manage_registered_devide_log());
		Template::set('joined_device_log_',$this->manage_registered_devide_log_(date('Y')));
		Template::set('customer_beacon_offer_log',$this->get_customer_beacon_log());
		Template::set_view('admin/dashboard/index');
		Template::render();
	}//end index()
	
	//--------------------------------------------------------------------
	
	/**
	 * @description : Function used to prepare device log as per time range
	 * @input : null
	 * @return : array
	 * 
	 */
	private function manage_registered_devide_log() {
		// set month array
		$month_array = array();
		for($month=0;$month < 6;$month++) {
			$month_array[$month]['mNumber'] = date('n', strtotime("-$month month"));
			$month_array[$month]['mName']	= date('F', strtotime("-$month month"));
		}
		// get all registerd device log
		$joined_device_log 		= $this->common_model->joined_user_device_log($month_array);
		$month1=$month_array[0]['mName'];
		$month1_AND = (!empty($joined_device_log["AND_".$month1]))?$joined_device_log["AND_".$month1]:"";
		$month1_IOS = (!empty($joined_device_log["IOS_".$month1]))?$joined_device_log["IOS_".$month1]:"";

		$month2=$month_array[1]['mName'];
		$month2_AND = (!empty($joined_device_log["AND_".$month2]))?$joined_device_log["AND_".$month2]:"";
		$month2_IOS = (!empty($joined_device_log["IOS_".$month2]))?$joined_device_log["IOS_".$month2]:"";

		$month3=$month_array[2]['mName'];
		$month3_AND = (!empty($joined_device_log["AND_".$month3]))?$joined_device_log["AND_".$month3]:"";
		$month3_IOS = (!empty($joined_device_log["IOS_".$month3]))?$joined_device_log["IOS_".$month3]:"";

		$month4=$month_array[3]['mName'];
		$month4_AND = (!empty($joined_device_log["AND_".$month4]))?$joined_device_log["AND_".$month4]:"";
		$month4_IOS = (!empty($joined_device_log["IOS_".$month4]))?$joined_device_log["IOS_".$month4]:"";

		$month5=$month_array[4]['mName'];
		$month5_AND = (!empty($joined_device_log["AND_".$month5]))?$joined_device_log["AND_".$month5]:"";
		$month5_IOS = (!empty($joined_device_log["IOS_".$month5]))?$joined_device_log["IOS_".$month5]:"";

		$month6=$month_array[5]['mName'];
		$month6_AND = (!empty($joined_device_log["AND_".$month6]))?$joined_device_log["AND_".$month6]:"";
		$month6_IOS = (!empty($joined_device_log["IOS_".$month6]))?$joined_device_log["IOS_".$month6]:"";
		return array('month1'=>$month1,'month2'=>$month2,'month3'=>$month3,'month4'=>$month4,'month5'=>$month5,'month6'=>$month6,'month1_AND'=>$month1_AND,'month1_IOS'=>$month1_IOS,'month2_AND'=>$month2_AND,'month2_IOS'=>$month2_IOS,'month3_AND'=>$month3_AND,'month3_IOS'=>$month3_IOS,'month4_AND'=>$month4_AND,'month4_IOS'=>$month4_IOS,'month5_AND'=>$month5_AND,'month5_IOS'=>$month5_IOS,'month6_AND'=>$month6_AND,'month6_IOS'=>$month6_IOS,);
   }
   
   private function manage_registered_devide_log_($joinedYear='') {
		// set month array
		$month_array = array();
		for($month=1;$month <= 12;$month++) {
			$month_array[$month]['mNumber'] = date('n', mktime(0, 0, 0, $month, 10));
			$month_array[$month]['mName'] = date('F', mktime(0, 0, 0, $month, 10));
		}
		// get all registerd device log
		$joined_device_log 	= $this->common_model->joined_user_device_log_($month_array,$joinedYear);
		foreach($month_array as $val){
			$month = $val['mName'];
			$android_users[] = (!empty($joined_device_log["AND_".$month]))?$joined_device_log["AND_".$month]:0;
			$ios_users[] = (!empty($joined_device_log["IOS_".$month]))?$joined_device_log["IOS_".$month]:0;
		}
		return array('android_users'=>$android_users,'ios_users'=>$ios_users);
   }

	/**
	 * @description : Function used to get yearly joined users log
	 * @input : null
	 * @output : json
	 * @access : public
	 **/
	public function yearly_joined_users_linechart(){
		$data    = $this->input->get();
		$result  = false;
		$year_email_log = array();
		$joinedYear = (!empty($data['joinedYear'])) ? $data['joinedYear'] : date('Y');
		if(!empty($joinedYear)){
			$result   = true;
			// get perticuler year's joined users log
			$year_users_log = $this->manage_registered_devide_log_($joinedYear);
		}
	   echo json_encode(
	   array('result'=>$result,
	   'android_users'=>(isset($year_users_log['android_users'])?$year_users_log['android_users']:0),
	   'ios_users'=>(isset($year_users_log['ios_users'])?$year_users_log['ios_users']:0)));die;
	}
	
	
	//--------------------------------------------------------------------
	
	/**
	 * @description : Function used to prepare scratcNWin yearly status wise entry
	 * @input : null
	 * @return : array
	 * 
	 */
	private function manage_scratch_win_yearly_log($campain_year='') {
		// fetch campaign's yearly records
		$campaign_compitition_log  = $this->common_model->campaign_compitition_log($campain_year);
		// prepare campaign data as per type
		$win_January = (!empty($campaign_compitition_log['win_January'])) ? $campaign_compitition_log['win_January'] : 0;
		$sec_January = (!empty($campaign_compitition_log['sec_January'])) ? $campaign_compitition_log['sec_January'] : 0;
		$nowin_January = (!empty($campaign_compitition_log['nowin_January'])) ? $campaign_compitition_log['nowin_January'] : 0;
	   
		$win_February = (!empty($campaign_compitition_log['win_February'])) ? $campaign_compitition_log['win_February'] : 0;
		$sec_February = (!empty($campaign_compitition_log['sec_February'])) ? $campaign_compitition_log['sec_February'] : 0;
		$nowin_February = (!empty($campaign_compitition_log['nowin_February'])) ? $campaign_compitition_log['nowin_February'] : 0;
	   
		$win_March = (!empty($campaign_compitition_log['win_March'])) ? $campaign_compitition_log['win_March'] : 0;
		$sec_March = (!empty($campaign_compitition_log['sec_March'])) ? $campaign_compitition_log['sec_March'] : 0;
		$nowin_March = (!empty($campaign_compitition_log['nowin_March'])) ? $campaign_compitition_log['nowin_March'] : 0;
	   
		$win_April = (!empty($campaign_compitition_log['win_April'])) ? $campaign_compitition_log['win_April'] : 0;
		$sec_April = (!empty($campaign_compitition_log['sec_April'])) ? $campaign_compitition_log['sec_April'] : 0;
		$nowin_April = (!empty($campaign_compitition_log['sec_April'])) ? $campaign_compitition_log['sec_April'] : 0;
	   
		$win_May = (!empty($campaign_compitition_log['win_May'])) ? $campaign_compitition_log['win_May'] : 0;
		$sec_May = (!empty($campaign_compitition_log['sec_May'])) ? $campaign_compitition_log['sec_May'] : 0;
		$nowin_May = (!empty($campaign_compitition_log['nowin_May'])) ? $campaign_compitition_log['nowin_May'] : 0;
	   
		$win_June = (!empty($campaign_compitition_log['win_June'])) ? $campaign_compitition_log['win_June'] : 0;
		$sec_June = (!empty($campaign_compitition_log['sec_June'])) ? $campaign_compitition_log['sec_June'] : 0;
		$nowin_June = (!empty($campaign_compitition_log['nowin_June'])) ? $campaign_compitition_log['nowin_June'] : 0;
	   
		$win_July = (!empty($campaign_compitition_log['win_July'])) ? $campaign_compitition_log['win_July'] : 0;
		$sec_July = (!empty($campaign_compitition_log['sec_July'])) ? $campaign_compitition_log['sec_July'] : 0;
		$nowin_July = (!empty($campaign_compitition_log['nowin_July'])) ? $campaign_compitition_log['nowin_July'] : 0;
	   
		$win_August = (!empty($campaign_compitition_log['win_August'])) ? $campaign_compitition_log['win_August'] : 0;
		$sec_August = (!empty($campaign_compitition_log['sec_August'])) ? $campaign_compitition_log['sec_August'] : 0;
		$nowin_August = (!empty($campaign_compitition_log['nowin_August'])) ? $campaign_compitition_log['nowin_August'] : 0;
	   
		$win_September = (!empty($campaign_compitition_log['win_September'])) ? $campaign_compitition_log['win_September'] : 0;
		$sec_September = (!empty($campaign_compitition_log['sec_September'])) ? $campaign_compitition_log['sec_September'] : 0;
		$nowin_September = (!empty($campaign_compitition_log['nowin_September'])) ? $campaign_compitition_log['nowin_September'] : 0;
	   
		$win_October = (!empty($campaign_compitition_log['win_October'])) ? $campaign_compitition_log['win_October'] : 0;
		$sec_October = (!empty($campaign_compitition_log['sec_October'])) ? $campaign_compitition_log['sec_October'] : 0;
		$nowin_October = (!empty($campaign_compitition_log['nowin_October'])) ? $campaign_compitition_log['nowin_October'] : 0;
	   
		$win_November = (!empty($campaign_compitition_log['win_November'])) ? $campaign_compitition_log['win_November'] : 0;
		$sec_November = (!empty($campaign_compitition_log['sec_November'])) ? $campaign_compitition_log['sec_November'] : 0;
		$nowin_November = (!empty($campaign_compitition_log['nowin_November'])) ? $campaign_compitition_log['nowin_November'] : 0;
	   
		$win_December = (!empty($campaign_compitition_log['win_December'])) ? $campaign_compitition_log['win_December'] : 0;
		$sec_December = (!empty($campaign_compitition_log['sec_December'])) ? $campaign_compitition_log['sec_December'] : 0;
		$nowin_December = (!empty($campaign_compitition_log['nowin_December'])) ? $campaign_compitition_log['nowin_December'] : 0;
	   
		$winner_data = "[$win_January,$win_February,$win_March,$win_April,$win_May,$win_June,$win_July,$win_August,$win_September,$win_October,$win_November,$win_December]";
		$sec_chance_data = "[$sec_January,$sec_February,$sec_March,$sec_April,$sec_May,$sec_June,$sec_July,$sec_August,$sec_September,$sec_October,$sec_November,$sec_December]";
		$no_win_data = "[$nowin_January,$nowin_February,$nowin_March,$nowin_April,$nowin_May,$nowin_June,$nowin_July,$nowin_August,$nowin_September,$nowin_October,$nowin_November,$nowin_December]";
		$winner_array = array($win_January,$win_February,$win_March,$win_April,$win_May,$win_June,$win_July,$win_August,$win_September,$win_October,$win_November,$win_December);
		$sec_chance_array = array($sec_January,$sec_February,$sec_March,$sec_April,$sec_May,$sec_June,$sec_July,$sec_August,$sec_September,$sec_October,$sec_November,$sec_December);
		$no_win_array = array($nowin_January,$nowin_February,$nowin_March,$nowin_April,$nowin_May,$nowin_June,$nowin_July,$nowin_August,$nowin_September,$nowin_October,$nowin_November,$nowin_December);
		return array('winner_data'=>$winner_data,'sec_chance_data'=>$sec_chance_data,'no_win_data'=>$no_win_data,'winner_array'=>$winner_array,'sec_chance_array'=>$sec_chance_array,'no_win_array'=>$no_win_array);
	}
	
	/**
	 * @description : Function used to manage ajax request for yearly scratch&win log
	 * @input : edm_id
	 * @output : json
	 * @access : public
	 **/
	public function yearly_scratch_win_bar_chart(){
		$data    = $this->input->get();
		$result  = false;
		$year_email_log = array();
		$campain_year = (!empty($data['campainYear'])) ? $data['campainYear'] : date('Y');
		if(!empty($campain_year)){
			$result  = true;
			// get perticuler year's scratch&win log
			$campaign_compitition_log = $this->manage_scratch_win_yearly_log($campain_year);
		}
	   echo json_encode(
	   array('result'=>$result,
	   'winner_array'=>$campaign_compitition_log['winner_array'],'sec_chance_array'=>$campaign_compitition_log['sec_chance_array'],'no_win_array'=>$campaign_compitition_log['no_win_array']));die;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * @description : Function used to get all active beacon offers customer view counts
	 * @input : null
	 * @return : array
	 * 
	 */
	 function get_customer_beacon_log() {
		$beacon_offer_data = array();
		// get active beacon offers
		$beacon_offers = $this->common_model->get_active_beacon_offers();
		foreach($beacon_offers as $offer) {
			$beacon_offer['beacon_offer_name'] = $offer['offer_name'];
			// get offer view count
			$customer_offer_views = $this->common_model->get_customer_beacon_log($offer['beacon_offer_id']);
			$beacons = array();
			$stores = array();
			$beacon_view_count = array();
			foreach($customer_offer_views as $views) {
				$beacons[] = $views['beacon_code'];
				$stores[] = $views['store_name'];
				$beacon_view_count[] = $views['beacon_offer_seen_count'];
			}
			$beacon_offer['beacons'] = $beacons;
			$beacon_offer['stores'] = $stores;
			$beacon_offer['beacon_view_count'] = $beacon_view_count;
			$beacon_offer_data[] = $beacon_offer;
		}
		return $beacon_offer_data;
	 }

}//end class
