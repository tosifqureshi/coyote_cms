<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Provides Admin functions for email template listing, view, add, edit, delete 
 *
 * @package    Coyote
 * @subpackage Modules Email Template
 * @category   Controllers
 * @author     Pushpraj 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Email_template extends Admin_Controller {
	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	 
	/*email_template : Start*/
	public function __construct() {
		parent::__construct();
		Template::set('toolbar_title', 'Email Template');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('email_template_model')){
			$this->load->model('email_template_model');
		}
		$this->lang->load('email_template');
	}//end __construct()

	//--------------------------------------------------------------------
	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index() {								// show the list of active and not deleted email_template
		$emailTemplates = $this->email_template_model->getEmailTemplateList("","",array('2'));		
	  //$emailTemplates = $this->email_template_model->getEmailTemplateList();		
		Template::set('emailTemplates',$emailTemplates);
		Template::set_view('admin/email_template/listing');
		Template::render();
	}//end index
	
	public function create($id="") {				// controller will call in both the cases on add and edit
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode email_template id
		$data = array();
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_email_template()) {       
					Template::set_message(lang('message_saved_email_template'),'success');
					redirect('backend/email_template');
			}
		} else if(isset($_POST['save'])) { 
			if($this->save_email_template('update',$id)) {
					Template::set_message(lang('message_update_email_template'),'success');
					redirect('backend/email_template');
			}
		}
		$data['email_template_title']		= lang('add_new_email_template');
		if($id) {
			$data['email_template_title']	= lang('edit_email_template');
			$records	= $this->email_template_model->getEmailTemplateList($id);	
			$data['email_id']	= isset($records->id) ? $records->id : '';
			$data['subject']	= isset($records->subject) ? $records->subject : '';
			$data['body']	    = isset($records->body) ? $records->body : '';
			$data['required_tag'] = isset($records->required_tag) ? $records->required_tag : '';
			$data['purpose']    = isset($records->purpose) ? $records->purpose : '';
		}else{
			$records	= $this->email_template_model->getEmailTemplateList("","default_template","");
			$data['email_id']	= '';
			$data['subject']	= isset($records->subject) ? $records->subject : '';
			$data['required_tag'] = isset($records->required_tag) ? $records->required_tag : '';
			$data['body']	    = isset($records->body) ? $records->body : '';
			$data['purpose']	= '';
		}
		Template::set('data',$data);
		Template::set_view('admin/email_template/email_template_form');
		Template::render();	
	}
	
	public function save_email_template($type = 'insert',$id=0) {
		$data = array();
		if(!empty($id)) {
			$data['id'] = $id;
		} 
		$this->form_validation->set_rules('subject', 'lang:subject', 'trim|required|max_length[50]');		
		$this->form_validation->set_rules('body', 'lang:body', 'trim|required');	
	
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		$data['subject'] = $this->input->post('subject');
		$data['body']    = $this->input->post('body');
		$data['purpose']    = $this->input->post('purpose');
		if($type == 'insert') {
			$data['created_date'] = date('Y-m-d H:i:s');
			$id = $this->email_template_model->addEmailTemplate($data);
		}
		else if ($type == 'update'){
			$id = $this->email_template_model->addEmailTemplate($data);
		}
		return $id;
	}
	
	public function template_view($id= '') {
		$data['url'] = base_url('backend/email_template/template_preiview/'.$id);
		Template::set('data',$data);
		Template::set_view('admin/email_template/email_html');
		Template::render();
	}
	
	public function template_preiview($id= ''){
		$id = decode($id);
		$data['records']	= $this->email_template_model->getEmailTemplateList($id);	
		$this->load->view('admin/email_template/preview',$data);
	}
	/*email_template : End*/
}//end class
