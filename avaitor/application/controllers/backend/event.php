<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Provides Admin functions for event listing,view,add,edit,delete 
 *
 * @package    Coyote
 * @subpackage Modules_In store event
 * @category   Controllers
 * @author     Pushpraj 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Event extends Admin_Controller {
	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	 
	/*events : Start*/
	public function __construct() {
		parent::__construct();
		Template::set('toolbar_title', 'Event');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('event_model')){
			$this->load->model('event_model', 'event_model');
		}
		$this->lang->load('event_admin');
	}//end __construct()

	//--------------------------------------------------------------------
	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index() {								// show the list of active and not deleted event
		$events = $this->event_model->getEventList();		
		Template::set('events',$events);
		Template::set_view('admin/event/listing');
		Template::render();
	}//end index
	
	public function create($id="") {				// controller will call in both the cases on add and edit
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode event id
		$data = array();
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_event()) {       
					Template::set_message(lang('message_saved_event'),'success');
					redirect('backend/event');
			}
		} else if(isset($_POST['save'])) { 
			if($this->save_event('update',$id)) {
					Template::set_message(lang('message_update_event'),'success');
					redirect('backend/event');
			}
		}
		if($id) {
			$data['event_title']	= lang('edit_event');
			$data['records']	= $this->event_model->getEventList($id);	
			$data['gallary_records']	= $this->event_model->get_gallary_image($id);	
		} else {
			$data['event_title']		= lang('add_new_event');
		}
		Template::set('data',$data);
		Template::set_view('admin/event/event_form');
		Template::render();	
	}
	
	public function save_event($type = 'insert',$id=0) {
		
		$data = array();
		if($id != 0) {
			$data['event_id'] = $id;
		} 
		$this->form_validation->set_rules('event_name', 'lang:event_name', 'trim|required|max_length[80]');		
		$this->form_validation->set_rules('event_short_description', 'lang:event_short_description', 'trim|required|max_length[100]');	
		$this->form_validation->set_rules('event_long_description', 'lang:event_long_description', 'trim');	
		$this->form_validation->set_rules('event_address', 'lang:event_address', 'trim|required');	
		$this->form_validation->set_rules('event_latitude', 'lang:event_latitude', 'trim|required');	
		$this->form_validation->set_rules('event_longitude', 'lang:event_longitude', 'trim|required');	
		$this->form_validation->set_rules('event_date', 'lang:event_date', 'trim|required');	
		//$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');	
		//$this->form_validation->set_rules('end_date', 'lang:end_date', 'trim|required');	
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("Some error occured while saving form.",'error');
			return FALSE;
		}
		
		if (isset($_FILES['event_image_1']) && ($_FILES['event_image_1']['name']) != '' ) {
			$data['event_image_1'] = $this->to_upload_image('event_image_1');
		}
		
		
		$event_date = date("Y-m-d",strtotime($this->input->post('event_date')));
		//$start_date = date("Y-m-d H:i:s",strtotime($this->input->post('start_date')));
		//$end_date 	= date("Y-m-d H:i:s",strtotime($this->input->post('end_date')));

		$data['event_name']     = $this->input->post('event_name');
		$data['event_short_description']    = $this->input->post('event_short_description');
		$data['event_long_description']    	= $this->input->post('event_long_description');
		$data['event_address']	= $this->input->post('event_address');
		$data['event_latitude']	= $this->input->post('event_latitude');
		$data['event_longitude']	= $this->input->post('event_longitude');
		$data['event_date']    	= $event_date;
		//$data['start_date']    	= $start_date;
		//$data['end_date']    	= $end_date;
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_default_image']     = 1;
		/*if($this->input->post('is_default_image')) {
			$data['is_default_image']     = $this->input->post('is_default_image');
		} else {
			$data['is_default_image']     = '0';
		}*/

		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->event_model->add_event($data);
			if($id != '') {
				for($i=2;$i<=7;$i++) {
					if (isset($_FILES['event_image_'.$i]) && ($_FILES['event_image_'.$i]['name']) != '' ) {
						$newData[$i] = $this->to_upload_image('event_image_'.$i);
					}
				}
				$this->event_model->add_gallary_image($newData,$id);
			}
		}
		else if ($type == 'update') {
			$id = $this->event_model->add_event($data);
		}
		return $id;

	}
	
	public function event_status($id='',$status='') {
		$data['event_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->event_model->add_event($data);
		Template::set_message(lang('message_status_event'),'success');
		redirect('backend/event');
	}
	
	public function event_delete($id='') {
		$data['event_id'] = decode($id);
		$data['is_delete'] = 1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->event_model->add_event($data);
		Template::set_message(lang('message_delete_event'),'success');
		redirect('backend/event');
	}
	
	function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'event_images/';
		$config['upload_path']	= $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] 	= '3000';
		$config['max_width'] 	= '0';
		$config['max_height'] 	= '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces']	= TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/event');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	} //to_upload_image
	
	function add_image(){
		$gallaryId = $_REQUEST['gallaryId'];
		if($gallaryId != ''){
			if (isset($_FILES['file']) && ($_FILES['file']['name']) != '' ) {
				$data['image'] = $this->to_upload_image('file');
			}
			$this->event_model->update_gallary_image($data,$gallaryId);
		}
		die;
	}
	
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_image() {
		// get data from post
		$gallaryId = $this->input->post('gallaryId');
		$return = false;
		if(!empty($gallaryId)) {
			// update image value as blank
			$data['image'] = "";
			$this->event_model->update_gallary_image($data,$gallaryId);
			$return = true;
		}
		echo $return;die;
	}
	/*Events : End*/
}//end class
