<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Manage Promotions actions
 *
 * @package    Coyote
 * @subpackage Modules_Promotion
 * @category   Controllers
 * @author     Tosif Qureshi 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Kiosk_promotion extends Admin_Controller
{


	/**
	 * Initiaze construct
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		Template::set('toolbar_title', 'Promotions');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('promotion_model')){
			$this->load->model('promotion_model', 'promotion_model');
		}
		$this->lang->load('promotion_admin');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Show listing of Promotions
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function index() {	
		// get promotion records		
		$promotions = $this->promotion_model->get_kiosk_promotion_list();
		// load template params
		Template::set('promotions',$promotions);
		Template::set_view('admin/promotions/kiosk_listing');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manage promotion create actions (add / update)
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function create($promo_id = "") {
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$promo_id = decode($promo_id); // decode promotion id
		$data = array();
		
		// manage add or update action if form comes from submit request
		if((isset($_POST['save'])) && $promo_id=='') {
			if($this->save_promotion()) {   
					Template::set_message(lang('message_saved_promo'),'success');
					redirect('backend/kiosk_promotion');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_promotion('update',$promo_id)) {
					Template::set_message(lang('message_update_promo'),'success');
					redirect('backend/kiosk_promotion');
			}
		}
		
		// get promotion details
		if(!empty($promo_id)) {
			$promotion = $this->promotion_model->get_kiosk_promotion_list($promo_id);	
			if(empty($promotion)) {
				Template::set_message(lang('message_invalid_promo'),'error');
				redirect('backend/kiosk_promotion');
			}
		}
		
		// set form input field values
		$data['promo_id']	        = (!empty($promo_id)) ? encode($promo_id) : '';
		$data['promo_title']	    = (!empty($promo_id)) ? lang('edit_kiosk_promotion') : lang('add_new_kiosk_promotion');
		$data['promo_name']			= (!empty($promo_id) && isset($promotion->promo_name)) ? $promotion->promo_name : '';		
		//$data['promo_price']		= (!empty($promo_id) && isset($promotion->promo_price)) ? $promotion->promo_price : '';	
		$data['promo_short_desc']	= (!empty($promo_id) && isset($promotion->promo_short_desc)) ? $promotion->promo_short_desc : '';
		$data['promo_long_desc']	= (!empty($promo_id) && isset($promotion->promo_long_desc)) ? $promotion->promo_long_desc : '';
		$data['kiosk_small_promo_image']	= (!empty($promo_id) && isset($promotion->kiosk_small_promo_image)) ? $promotion->kiosk_small_promo_image : '';
		$data['kiosk_large_promo_image']	= (!empty($promo_id) && isset($promotion->kiosk_large_promo_image)) ? $promotion->kiosk_large_promo_image : '';
		$data['promo_start_date']	= (!empty($promo_id) && isset($promotion->promo_start_date)) ? date("d-m-Y H:i:s",strtotime($promotion->promo_start_date)) : '';
		$data['promo_end_date']		= (!empty($promo_id) && isset($promotion->promo_end_date)) ? date("d-m-Y H:i:s",strtotime($promotion->promo_end_date)) : '';
		$data['default_image_type']	= (!empty($promo_id) && isset($promotion->default_image_type)) ? $promotion->default_image_type : '';
		
		// load form view params 
		Template::set('data',$data);
		Template::set_view('admin/promotions/kiosk_promotion_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manage promotion form
	 * @access: private
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	private function save_promotion($type = 'insert',$id=0) {
		
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['promo_id'] = $id;
		} 
		// check form fields validation
		$this->form_validation->set_rules('promo_name', 'lang:promo_name', 'trim|required|max_length[80]');		
		//$this->form_validation->set_rules('promo_price', 'lang:promo_price', 'trim|numeric');		
		$this->form_validation->set_rules('promo_short_desc', 'lang:promo_short_desc', 'trim');	
		$this->form_validation->set_rules('promo_long_desc', 'lang:promo_long_desc', 'trim');		
		$this->form_validation->set_rules('promo_start_date', 'lang:start_time', 'trim|required');	
		$this->form_validation->set_rules('promo_end_date', 'lang:end_time', 'trim|required');	
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		
		// Set input field values
		if (isset($_FILES['kiosk_small_promo_image']) && ($_FILES['kiosk_small_promo_image']['name']) != '' ) {
			$data['kiosk_small_promo_image'] = $this->to_upload_image('kiosk_small_promo_image');
		}
		if (isset($_FILES['kiosk_large_promo_image']) && ($_FILES['kiosk_large_promo_image']['name']) != '' ) {
			$data['kiosk_large_promo_image'] = $this->to_upload_image('kiosk_large_promo_image');
		}
		$data['promo_name']      = $this->input->post('promo_name');
		$data['promo_short_desc']= $this->input->post('promo_short_desc');
		$data['promo_long_desc'] = $this->input->post('promo_long_desc');
		$data['promo_start_date']= date("Y-m-d H:i:s",strtotime($this->input->post('promo_start_date')));
		$data['promo_end_date']  = date("Y-m-d H:i:s",strtotime($this->input->post('promo_end_date')));
		if($this->input->post('default_image_type')) {
			$data['default_image_type']     = $this->input->post('default_image_type');
		} else {
			$data['default_image_type']     = '0';
		}
		//echo '<pre>';
		//print_r($data);die;		
		// add or update promotion data into db
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->promotion_model->add_kiosk_promotion($data);
		} else if ($type == 'update') {
			$id = $this->promotion_model->add_kiosk_promotion($data);
		}
		return $id;

	}
	
	//--------------------------------------------------------------------

	/**
	 * Update status of promotion
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @auther: Tosif Qureshi
	 */
	public function promotion_status($id='',$status='') {
		$data['promo_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->promotion_model->add_kiosk_promotion($data);
		Template::set_message(lang('message_status_promo'),'success');
		redirect('backend/kiosk_promotion');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove promotion
	 * @input : id
	 * @return: void
	 * @access: public
	 * @auther: Tosif Qureshi
	 */
	public function promotion_delete($id='') {
		$data['promo_id'] = decode($id);
		$data['is_delete'] = 1;
		$id = $this->promotion_model->add_promotion($data);
		Template::set_message(lang('message_delete_promo'),'success');
		redirect('backend/kiosk_promotion');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Upload promotion image
	 * @input : image
	 * @return: void
	 * @access: private
	 * @auther: Tosif Qureshi
	 */
	private function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'promotion_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/kiosk_promotion');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	/**
	 * Function to remove image from list
	 * @input  : promo_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_image() {
		// get data from post
		$promo_id = $this->input->post('promo_id');
		$image_type = $this->input->post('image_type');
		$return = false;
		if(!empty($image_type) && !empty($promo_id)) {
			// update image value as blank
			$data['promo_id'] = decode($promo_id);
			$data['promo_image_'.$image_type] = "";
			$this->promotion_model->add_kiosk_promotion($data);
			$return = true;
		}
		echo $return;die;
	}
		
}//end class
