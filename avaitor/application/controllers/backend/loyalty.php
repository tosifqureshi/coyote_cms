<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Manage Loyalty actions
 *
 * @package    Coyote
 * @subpackage Modules_Loyalty
 * @category   Controllers
 * @author     Tosif Qureshi 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Loyalty extends Admin_Controller
{


	/**
	 * Initiaze construct
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		Template::set('toolbar_title', 'Loyalty');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('loyalty_model')){
			$this->load->model('loyalty_model', 'loyalty_model');
		}
		$this->lang->load('loyalty_admin');
	} //end __construct()

	//--------------------------------------------------------------------

	/**
	 * Show listing of Loyalty programs
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function index() {	
		// get loyalty records		
		$loyalty_data = $this->loyalty_model->get_loyalty_list();
		// load template params
		Template::set('loyalty_data',$loyalty_data);
		Template::set_view('admin/loyalty/listing');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manage loyalty create actions (add / update)
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function create($loyalty_id = "") {
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$loyalty_id = decode($loyalty_id); // decode loyalty id
		$data = array();
		
		// manage add or update action if form comes from submit request
		if((isset($_POST['save'])) && $loyalty_id=='') {
			if($this->save_loyalty()) {   
					Template::set_message(lang('message_saved_loyalty'),'success');
					redirect('backend/loyalty');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_loyalty('update',$loyalty_id)) {
					Template::set_message(lang('message_update_loyalty'),'success');
					redirect('backend/loyalty');
			}
		}
		
		// get promotion details
		if(!empty($loyalty_id)) {
			$loyalty = $this->loyalty_model->get_loyalty_list($loyalty_id);	
			if(empty($loyalty)) {
				Template::set_message(lang('message_invalid_loyalty'),'error');
				redirect('backend/loyalty');
			}
		}
		
		// set form input field values
		$data['loyalty_id']	        = (!empty($loyalty_id)) ? encode($loyalty_id) : '';
		$data['loyalty_title']	    = (!empty($loyalty_id)) ? lang('edit_loyalty') : lang('add_new_loyalty');
		$data['loyalty_name']		= (!empty($loyalty_id) && isset($loyalty->loyalty_name)) ? $loyalty->loyalty_name : '';		
		$data['loyalty_price']		= (!empty($loyalty_id) && isset($loyalty->loyalty_price)) ? $loyalty->loyalty_price : '';	
		$data['loyalty_short_desc']	= (!empty($loyalty_id) && isset($loyalty->loyalty_short_desc)) ? $loyalty->loyalty_short_desc : '';
		$data['loyalty_long_desc']	= (!empty($loyalty_id) && isset($loyalty->loyalty_long_desc)) ? $loyalty->loyalty_long_desc : '';
		$data['loyalty_image_1']	= (!empty($loyalty_id) && isset($loyalty->loyalty_image_1)) ? $loyalty->loyalty_image_1 : '';	
		$data['loyalty_image_2']	= (!empty($loyalty_id) && isset($loyalty->loyalty_image_2)) ? $loyalty->loyalty_image_2 : '';
		$data['loyalty_image_3']	= (!empty($loyalty_id) && isset($loyalty->loyalty_image_3)) ? $loyalty->loyalty_image_3 : '';	
		$data['used_image']			= (!empty($loyalty_id) && isset($loyalty->used_image)) ? $loyalty->used_image : '';	
		$data['not_used_image']		= (!empty($loyalty_id) && isset($loyalty->not_used_image)) ? $loyalty->not_used_image : '';
		$data['free_image']			= (!empty($loyalty_id) && isset($loyalty->free_image)) ? $loyalty->free_image : '';
		$data['loyalty_start_date']	= (!empty($loyalty_id) && isset($loyalty->loyalty_start_date)) ? date("d-m-Y H:i:s",strtotime($loyalty->loyalty_start_date)) : '';
		$data['loyalty_end_date']	= (!empty($loyalty_id) && isset($loyalty->loyalty_end_date)) ? date("d-m-Y H:i:s",strtotime($loyalty->loyalty_end_date)) : '';
		//$data['default_image_type']	= (!empty($loyalty_id) && isset($loyalty->default_image_type)) ? $loyalty->default_image_type : '';
		$data['store_id']		    = (!empty($loyalty_id) && isset($loyalty->store_id)) ? $loyalty->store_id : '';
		$data['paid_qty']			= (!empty($loyalty_id) && isset($loyalty->paid_qty) && !empty($loyalty->paid_qty)) ? $loyalty->paid_qty : '';
		$data['free_qty']			= (!empty($loyalty_id) && isset($loyalty->free_qty) && !empty($loyalty->free_qty)) ? $loyalty->free_qty : '';		
		$data['discount']			= (!empty($loyalty_id) && isset($loyalty->discount) && !empty($loyalty->discount)) ? $loyalty->discount : '';		
		$data['stores']             = $this->loyalty_model->get_store_list(); // get stores list
		$data['barcode_image']		= (!empty($loyalty_id) && isset($loyalty->barcode_image) && !empty($loyalty->barcode_image)) ? $loyalty->barcode_image : '';
		$data['barcode']	        = (!empty($loyalty_id) && isset($loyalty->barcode) && !empty($loyalty->barcode)) ? $loyalty->barcode : '';
		//$data['loyalty_no']	        = (!empty($loyalty_id) && isset($loyalty->loyalty_no) && !empty($loyalty->loyalty_no)) ? $loyalty->loyalty_no : '';
		//$data['loyalty_product_size']= (!empty($loyalty_id) && isset($loyalty->loyalty_product_size) && !empty($loyalty->loyalty_product_size)) ? $loyalty->loyalty_product_size : '';
		$data['trigger_product_ids']= (!empty($loyalty_id) && isset($loyalty->trigger_product_ids) && !empty($loyalty->trigger_product_ids)) ? $loyalty->trigger_product_ids : '';
		$data['reward_product_ids']= (!empty($loyalty_id) && isset($loyalty->reward_product_ids) && !empty($loyalty->reward_product_ids)) ? $loyalty->reward_product_ids : '';
		//$data['loyalty_qty']        = $this->loyalty_model->get_loyalty_qty($loyalty_id); // get loyalty quantity list
		// load form view params 
		Template::set('data',$data);
		Template::set_view('admin/loyalty/loyalty_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manage loyalty form
	 * @access: private
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	private function save_loyalty($type = 'insert',$id=0) {
		
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['loyalty_id'] = $id;
		}
		$postData = $this->input->post();
		
		// check form fields validation
		$this->form_validation->set_rules('loyalty_name', 'lang:loyalty_name', 'trim|required|max_length[80]');		
		$this->form_validation->set_rules('loyalty_price', 'lang:loyalty_price', 'trim|numeric');		
		$this->form_validation->set_rules('loyalty_short_desc', 'lang:loyalty_short_description', 'trim|max_length[80]');	
		$this->form_validation->set_rules('loyalty_long_desc', 'lang:loyalty_long_description', 'trim');		
		$this->form_validation->set_rules('loyalty_start_date', 'lang:start_time', 'trim|required');	
		$this->form_validation->set_rules('loyalty_end_date', 'lang:end_time', 'trim|required');	
		$this->form_validation->set_rules('store_id[]', 'lang:loyalty_store', 'trim|required');
		//$this->form_validation->set_rules('loyalty_product_size[]', 'lang:loyalty_product_size', 'trim|required');
		$this->form_validation->set_rules('paid_qty', 'lang:paid_qty', 'trim|required|numeric|greater_than[0]');
		$this->form_validation->set_rules('free_qty', 'lang:free_qty', 'trim|required|numeric|greater_than[0]');
		$this->form_validation->set_rules('discount', 'lang:discount', 'trim|required|numeric');
		//$this->form_validation->set_rules('loyalty_no', 'lang:loyalty_no', 'trim|numeric|greater_than[0]');
		$this->form_validation->set_rules('barcode', 'lang:offer_barcode', 'trim|numeric|max_length[12]');	
		$this->form_validation->set_rules('trigger_product_ids', 'lang:loyalty_trigger_products', 'trim|required');	
		$this->form_validation->set_rules('reward_product_ids', 'lang:loyalty_reward_products', 'trim|required');	
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* There is some error while saving form.!!",'error');
			return FALSE;
		}
		
		// Set input field values
		if (isset($_FILES['loyalty_image_1']) && ($_FILES['loyalty_image_1']['name']) != '' ) {
			$data['loyalty_image_1'] = $this->to_upload_image('loyalty_image_1');
		}
		if (isset($_FILES['loyalty_image_2']) && ($_FILES['loyalty_image_2']['name']) != '' ) {
			$data['loyalty_image_2'] = $this->to_upload_image('loyalty_image_2');
		}
		if (isset($_FILES['loyalty_image_3']) && ($_FILES['loyalty_image_3']['name']) != '' ) {
			$data['loyalty_image_3'] = $this->to_upload_image('loyalty_image_3');
		}
		if (isset($_FILES['used_image']) && ($_FILES['used_image']['name']) != '' ) {
			$data['used_image'] = $this->to_upload_image('used_image');
		}
		if (isset($_FILES['not_used_image']) && ($_FILES['not_used_image']['name']) != '' ) {
			$data['not_used_image'] = $this->to_upload_image('not_used_image');
		}
		if (isset($_FILES['free_image']) && ($_FILES['free_image']['name']) != '' ) {
			$data['free_image'] = $this->to_upload_image('free_image');
		}
		// manage store ids
		$store_data = '';
		$store_ids = $this->input->post('store_id');
		if(is_array($store_ids) && !empty($store_ids)) {
			$store_data = implode(',',$store_ids);
		}
		$store_data = ",".$store_data.",";
		// manage loyalty_product_size
		$product_size_data = '';
		$product_sizes = $this->input->post('loyalty_product_size');
		if(is_array($product_sizes) && !empty($product_sizes)) {
			$product_size_data = implode(',',$product_sizes);
		}
		$product_size_data = ",".$product_size_data.",";
		// prepare loyalty data  
		$data['loyalty_name']      = $this->input->post('loyalty_name');
		
		if($this->input->post('loyalty_price')) {
			$data['loyalty_price']     = $this->input->post('loyalty_price');
		} else {
			$data['loyalty_price']     = '0';
		}
		$data['loyalty_short_desc']= $this->input->post('loyalty_short_desc');
		$data['loyalty_long_desc'] = $this->input->post('loyalty_long_desc');
		$data['paid_qty']      	   = $this->input->post('paid_qty');
		$data['free_qty']    	   = $this->input->post('free_qty');
		$data['discount']    	   = $this->input->post('discount');
		//$data['loyalty_no']    	   = $this->input->post('loyalty_no');
		$data['trigger_product_ids']      = $this->input->post('trigger_product_ids');
		$data['reward_product_ids']    	  = $this->input->post('reward_product_ids');
		$data['loyalty_start_date']= date("Y-m-d H:i:s",strtotime($this->input->post('loyalty_start_date')));
		$data['loyalty_end_date']  = date("Y-m-d H:i:s",strtotime($this->input->post('loyalty_end_date')));
		if($this->input->post('default_image_type')) {
			$data['default_image_type']     = $this->input->post('default_image_type');
		} else {
			$data['default_image_type']     = '1';
		}
		$data['store_id']    	   = $store_data;
		//$data['loyalty_product_size']    	   = $product_size_data;
		// manage barcode
		$barcode = $this->input->post('barcode');
		if(!empty($barcode)) {
			// get barcode data
			
			$barcode_data = $this->loyalty_model->get_barcode_data($barcode,0,$id);
			if($barcode_data == 0) {
				// generate barcode image
				$barcode_image = $this->generate_zend_barcode($barcode);
			}
		}
		// set barcode values
		$data['barcode'] = (!empty($barcode)) ? $barcode : '';
		$data['barcode_image']   = (isset($barcode_image) && !empty($barcode_image)) ? $barcode_image : '';
		// manage paid and free quantity as combine qty(s)
		/*$paid_qty = array_filter($this->input->post('paid_qty'));
		$free_qty = array_filter($this->input->post('free_qty'));
		if(!empty($paid_qty) && !empty($free_qty) && (count($paid_qty) == count($free_qty))) {
			foreach($paid_qty as $key=>$qty) {
				$loyalty_data['paid_qty']  = $qty;
				$loyalty_data['free_qty']  = $free_qty[$key];
				$loyalty_qty[] = $loyalty_data;
			}
		}*/
		
		// add or update loyalty data into db
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$loyalty_id = $this->loyalty_model->add_loyalty($data);
			// manage loyalty quantity
			//$this->manage_loyalty_qty($loyalty_id,$loyalty_qty);
		} else if ($type == 'update') {
			$loyalty_id = $this->loyalty_model->add_loyalty($data);
			// manage loyalty quantity
			//$this->manage_loyalty_qty($loyalty_id,$loyalty_qty);
		}
		return $loyalty_id;

	}
	
	//--------------------------------------------------------------------

	/**
	 * Add loyalty quantity
	 * @input : loyalty_qty , loyalty_id
	 * @return: void
	 * @access: public
	 * @auther: Tosif Qureshi
	 */
	private function manage_loyalty_qty($loyalty_id=0,$loyalty_qty='') {
		if(!empty($loyalty_qty)) {
			// remove previose quantity log
			$this->loyalty_model->remove_loyalty_qty($loyalty_id);
			// add loyalty quantites
			foreach($loyalty_qty as $qty) {
				// prepare loyalty qty data
				$loyalty_qty_data = array('loyalty_id'=>$loyalty_id,'paid_qty'=>$qty['paid_qty'],'free_qty'=>$qty['free_qty']);
				$this->loyalty_model->add_loyalty_qty($loyalty_qty_data);
			}
		}
	}
	
	//--------------------------------------------------------------------

	/**
	 * Update status of loyalty
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @auther: Tosif Qureshi
	 */
	public function loyalty_status($id='',$status='') {
		$data['loyalty_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->loyalty_model->add_loyalty($data);
		Template::set_message(lang('message_status_loyalty'),'success');
		redirect('backend/loyalty');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove loyalty
	 * @input : id
	 * @return: void
	 * @access: public
	 * @auther: Tosif Qureshi
	 */
	public function loyalty_delete($id='') {
		$data['loyalty_id'] = decode($id);
		$data['is_delete'] = 1;
		$id = $this->loyalty_model->add_loyalty($data);
		Template::set_message(lang('message_delete_loyalty'),'success');
		redirect('backend/loyalty');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Upload loyalty image
	 * @input : image
	 * @return: void
	 * @access: private
	 * @auther: Tosif Qureshi
	 */
	private function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'loyalty_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/loyalty');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_image() {
		// get data from post
		$loyalty_id = $this->input->post('loyalty_id');
		$image_type = $this->input->post('image_type');
		$return = false;
		if(!empty($image_type) && !empty($loyalty_id)) {
			// update image value as blank
			$data['loyalty_id'] = decode($loyalty_id);
			$data['loyalty_image_'.$image_type] = "";
			$this->loyalty_model->add_loyalty($data);
			$return = true;
		}
		echo $return;die;
	}
	
	/**
	 * Function to generate barcode from zend library
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	private function generate_zend_barcode($barcode='') {
		// load zend library
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		// prepare barcode from zend library
		$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
		// set barcode image path
		$dir_path = IMAGEUPLOADPATH.'barcodes/';
		// set barcode image name
		//$image_name = time().$barcode;
		$image_name = $barcode.'.png';
		// create image from barcode
		ImagePNG($file, $dir_path.$image_name);
		ImageDestroy($file);
		return $image_name;
		
	}
	
    /*
    *  @access: public
    *  @description: This method is use to update the loyalty order 
    *  @author: Tosif Qureshi
    *  @return: json_obj
    */ 
    function update_order() {
		// Get post request params
		$update_data   = $this->input->post();
		if(!empty($update_data)) {
			$loyalty_id    = decode($update_data['id']);
			$from_position = $update_data['fromPosition'];
			$to_position   = $update_data['toPosition'];
			$return_status = "error";
			// get To loyalty id
			$to_loyalty = $this->loyalty_model->get_loyalty_from_order($to_position);
			if(!empty($to_loyalty)) {
				// update order of from position
				$this->loyalty_model->add_loyalty(array('loyalty_id'=>$loyalty_id,'position'=>$to_position));
				// update order of to position
				$this->loyalty_model->add_loyalty(array('loyalty_id'=>$to_loyalty->loyalty_id,'position'=>$from_position));
				$return_status = "success";
				
			}
		}
		echo $return_status;
	}
		
}//end class
