<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Provides Admin functions for Offer listing,view,add,edit,delete 
 *
 * @package    Coyote
 * @subpackage Modules_In store offer
 * @category   Controllers
 * @author     Pushpraj 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Offer extends Admin_Controller
{


	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	 
	/*Offers : Start*/
	
	public function __construct()
	{
		parent::__construct();
		Template::set('toolbar_title', 'Offer');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('offers_model')){
			$this->load->model('offers_model', 'offers_model');
		}
		$this->lang->load('offers_admin');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index() {								// show the list of active and not deleted offer
		$offers = $this->offers_model->getOfferList();		
		Template::set('offers',$offers);
		Template::set_view('admin/offer/listing');
		Template::render();
	}//end index
	
	public function create($id="") {				// controller will call in both the cases on add and edit
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode offer id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_offer()) {       
					Template::set_message(lang('message_saved_offer'),'success');
					redirect('backend/offer');
			}
		} else if(isset($_POST['save'])) { 
			if($this->save_offer('update',$id)) {
					Template::set_message(lang('message_update_offer'),'success');
					redirect('backend/offer');
			}
		}
		if($id) {
			//$offer_id = decode($id);
			$data['offer_title']		= lang('edit_offer');
			$offer_id = $id;
			$offer = $this->offers_model->getOfferList($offer_id);	
			if(empty($offer)) {
				Template::set_message(lang('message_invalid_offer'),'error');
				redirect('backend/offer');
			}	
			$data['offer_id']			= encode($offer_id);
			$data['offer_name']			= $offer->offer_name;		
			$data['offer_price']		= $offer->offer_price;		
			$data['offer_short_description']	= $offer->offer_short_description;	
			$data['offer_long_description']		= $offer->offer_long_description;	
			$data['offer_image_1']		= $offer->offer_image_1;	
			$data['offer_image_2']		= $offer->offer_image_2;	
			$data['offer_image_3']		= $offer->offer_image_3;
			$data['offer_detail_page_image']= $offer->offer_detail_page_image;
			$data['start_time']			= date("Y-m-d H:i:s",strtotime($offer->start_time));
			$data['end_time']			= date("Y-m-d H:i:s",strtotime($offer->end_time));
			//$data['beacon_id']		= $offer->beacon_id;	
			//$data['store_ids']			= $offer->store_ids;
			//$data['category_id']		= $offer->category_id;	
			$data['is_banner_image']	= $offer->is_banner_image;	
			$data['is_default_image']	= $offer->is_default_image;
			$data['is_image_show_in_slider']	= $offer->is_image_show_in_slider;
			//$data['price']			    = (isset($offer->price)) ? $offer->price : 0;	
		} else {
			$data['offer_title']		= lang('add_new_offer');
			$data['offer_id']			=	'';
			$data['offer_name']			=	'';
			$data['offer_price']		=	'';
			$data['offer_short_description']	=	'';
			$data['offer_long_description']		=	'';
			$data['offer_image_1']		=	'';
			$data['offer_image_2']		=	'';
			$data['offer_image_3']		=	'';
			$data['offer_detail_page_image'] = '';
			$data['start_time']			=	'';
			$data['end_time']			=	'';
			//$data['beacon_id']		= 	'';
			//$data['store_ids']			= 	'';
			//$data['category_id']		=   ''; 
			$data['is_banner_image']	= 	'';
			$data['is_default_image']	= 	'';
			$data['is_image_show_in_slider']	= 	'';
			//$data['price']			    =   '';
		}
		$data['beacons'] = $this->offers_model->getBeaconList();		
		//$data['stores'] = $this->offers_model->getStoreList();
		//$data['stores'] = $this->offers_model->get_store_list();
		//$data['categories'] = $this->offers_model->get_category_list();	
		Template::set('data',$data);
		Template::set_view('admin/offer/offer_form');
		Template::render();	
	}
	
	public function save_offer($type = 'insert',$id=0) {
		
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['offer_id'] = $id;
		} 
		$this->form_validation->set_rules('offer_name', 'lang:offer_name', 'trim|required|max_length[80]');		
		$this->form_validation->set_rules('offer_price', 'lang:offer_price', 'trim|required|numeric');	
		//$this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');		
		$this->form_validation->set_rules('offer_short_description', 'lang:offer_short_description', 'trim');	
		$this->form_validation->set_rules('offer_long_description', 'lang:offer_long_description', 'trim');	
		//$this->form_validation->set_rules('beacon_id', 'lang:offer_beacon', 'trim|required');	
		//$this->form_validation->set_rules('store_ids[]', 'lang:offer_store', 'required');	
		//$this->form_validation->set_rules('category_id', 'lang:category', 'trim|required');	
		$this->form_validation->set_rules('start_time', 'lang:start_time', 'trim|required');	
		$this->form_validation->set_rules('end_time', 'lang:end_time', 'trim|required');	
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		
		if (isset($_FILES['offer_image_1']) && ($_FILES['offer_image_1']['name']) != '' ) {
			$data['offer_image_1'] = $this->to_upload_image('offer_image_1');
		}
		if (isset($_FILES['offer_image_2']) && ($_FILES['offer_image_2']['name']) != '' ) {
			$data['offer_image_2'] = $this->to_upload_image('offer_image_2');
		}
		if (isset($_FILES['offer_image_3']) && ($_FILES['offer_image_3']['name']) != '' ) {
			$data['offer_image_3'] = $this->to_upload_image('offer_image_3');
		}
		if (isset($_FILES['offer_detail_page_image']) && ($_FILES['offer_detail_page_image']['name']) != '' ) {
			$data['offer_detail_page_image'] = $this->to_upload_image('offer_detail_page_image');
		}
		
		// manage store ids
		/* $store_data = '';
		$store_ids = $this->input->post('store_ids');
		if(is_array($store_ids) && !empty($store_ids)) {
			$store_data = implode(',',$store_ids);
			//$store_data = json_encode($store_ids);
		}
		$store_data = ",".$store_data.","; */
		$start_time = date("Y-m-d H:i:s",strtotime($this->input->post('start_time')));
		$end_time = date("Y-m-d H:i:s",strtotime($this->input->post('end_time')));

		$data['offer_name']     = ($this->input->post('offer_name'));
		$data['offer_price']    = $this->input->post('offer_price');
		//$data['price']    	    = $this->input->post('price');
		$data['offer_short_description']    = ($this->input->post('offer_short_description'));

		$data['offer_long_description']    	= ($this->input->post('offer_long_description'));
		$data['beacon_id']    	= 0;
		//$data['store_ids']    	= $store_data;
		//$data['category_id']    = $this->input->post('category_id');
		$data['start_time']    	= $start_time;
		$data['end_time']    	= $end_time;
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_image_show_in_slider']= (!empty($this->input->post('is_image_show_in_slider'))) ? 1 : 0;
		if($this->input->post('is_banner_image')) {
			$data['is_banner_image']     = $this->input->post('is_banner_image');
		} else {
			$data['is_banner_image']     = '0';
		}
		if($this->input->post('is_default_image')) {
			$data['is_default_image']     = $this->input->post('is_default_image');
		} else {
			$data['is_default_image']     = '0';
		}
		/*if($this->offers_model->check_beacon($data) > 0) {			// check beacon is available on given data and time or not
			Template::set_message(lang('cannot_place_offer'));
			return FALSE;
		}*/
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->offers_model->add_offer($data);
		}
		else if ($type == 'update') {
			$id = $this->offers_model->add_offer($data);
		}
		return $id;

	}
	
	public function offer_status($id='',$status='') {
		$data['offer_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/offer');
	}
	
	public function offer_delete($id='') {
		$data['offer_id'] = decode($id);
		$data['is_delete'] = 1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/offer');
	}
	
	public function copy_offer($id='') {
		$offer_id 	= decode($id);
		$offer 		= $this->offers_model->getOfferList($offer_id);
		
		$offer_name	= $offer->offer_name;	
		$offer_name_count = $this->offers_model->check_offer_name($offer_name);
		if($offer_name_count > 0) {
			$data['offer_name'] = $offer_name." (copy)";
		} else {
			$data['offer_name'] = $offer_name;
		}
		$data['offer_price']		= $offer->offer_price;	
		//$data['price']		= (isset($offer->price)) ? $offer->price : 0;		
		$data['offer_short_description']	= $offer->offer_short_description;	
		$data['offer_long_description']		= $offer->offer_long_description;	
		$data['offer_image_1']		= $offer->offer_image_1;	
		$data['offer_image_2']		= $offer->offer_image_2;	
		$data['offer_image_3']		= $offer->offer_image_3;	
		$data['start_date']			= $offer->start_date;
		$data['start_time']			= $offer->start_time;
		$data['end_date']			= $offer->end_date;
		$data['end_time']			= $offer->end_time;	
		//$data['store_ids']			= $offer->store_ids;
		//$data['category_id']		= $offer->category_id;	
		$data['is_banner_image']	= $offer->is_banner_image;	
		$data['is_default_image']	= $offer->is_default_image;	
		$data['offer_image_1']		= $offer->offer_image_1;	
		$data['offer_image_2']		= $offer->offer_image_2;	
		$data['offer_image_3']		= $offer->offer_image_3;	
		
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['created_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$id = $this->offers_model->add_offer($data);
		if(isset($id)){
			Template::set_message(lang('message_copy_offer'),'success');
		} else {
			Template::set_message(lang('error_message_copy_offer'),'error');
		}
		redirect('backend/offer');
	}
	
	function checkFreeBeacon() {   	// controller is called will add/edit date time through ajax request  
		$data['beacon_id'] = $this->input->post('beacon_id');
		//$store_ids         = $this->input->post('store_id');
		$data['start_time'] = $this->input->post('start_time');
		$data['end_time'] = $this->input->post('end_time');
		$data['offer_id'] = $this->input->post('offer_id');
		/*($data['store_id'] = '';
		if(is_array($store_ids) && !empty($store_ids)) {
			$data['store_id'] = implode(',',$store_ids);
		}*/
		echo $offerCount = $this->offers_model->check_beacon($data);  // returns the no of offer associate with beacon and store
		die;
	}
	
	function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'offer_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/offer');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	} //to_upload_image
	
	
	/**
	 * Get stores records from mssql database
	 * 
	 */
	 public function manage_ms_stores() {
		// get store records
		$store_data = $this->offers_model->get_store_data();
		// insert store data in table
		foreach($store_data as $store) {
			// prepare store data
			$store_values = array('store_name'=>$store['STOR_DESC'],
			'ms_store_id'=>$store['STOR_STORE'],
			'ms_status'=>$store['STOR_STATUS'],
			'store_add_1'=>$store['STOR_ADDR_1'],
			'store_add_2'=>$store['STOR_ADDR_2'],
			'store_add_3'=>$store['STOR_ADDR_3'],
			'store_post_code'=>$store['STOR_POST_CODE'],
			'store_phone_no'=>$store['STOR_PHONE'],
			'store_lat'=>$store['STOR_LAT'],
			'store_lng'=>$store['STOR_LONG'],
			'store_distance'=>$store['STOR_DISTANCE'],
			'created_at'=>date('Y-m-d H:i:s'),
			 'updated_at'=>date('Y-m-d H:i:s'));
			 // check ms store id
			 $ms_store_id = $this->offers_model->check_store_info($store['STOR_STORE']);
			 $store_data = $this->offers_model->get_store_data();
			// insert store data
			$this->offers_model->manage_store_data($store_values);
		}
		return true;
	 }
	 
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_offer_image() {
		// get data from post
		$offer_id = $this->input->post('offer_id');
		$image_type = $this->input->post('image_type');
		$return = false;
		if(!empty($image_type) && !empty($offer_id)) {
			// update offer image value as blank
			$data['offer_id'] = decode($offer_id);
			$data['offer_image_'.$image_type] = "";
			$this->offers_model->add_offer($data);
			$return = true;
		}
		echo $return;die;
	}
	
	// testing category 
	public function test_cat() {
	
		$test = $this->offers_model->get_category();
		echo '<pre>';
		print_r($test);die;
	 }
	 
	/*Offers : End*/
		
		
}//end class
