<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Avaitor
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Avaitor
 * @author    Avaitor Dev Team
 * @copyright Copyright (c) 2011 - 2012, Avaitor Dev Team
 * @license   http://guides.ciAvaitor.com/license.html
 * @link      http://ciAvaitor.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Admin Reports controller
 *
 * The base controller which displays the homepage of the Admin Reports context in the Avaitor app.
 *
 * @package    Avaitor
 * @subpackage Controllers
 * @category   Controllers
 * @author     Avaitor Dev Team
 * @link       http://guides.ciAvaitor.com/helpers/file_helpers.html
 *
 */
class Reports extends Admin_Controller
{


	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		Template::set('toolbar_title', 'Reports');

		$this->auth->restrict('Site.Reports.View');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index()
	{
		Template::set_view('admin/reports/index');
		Template::render();
	}//end index()

	//--------------------------------------------------------------------


}//end class
