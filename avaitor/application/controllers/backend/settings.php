<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Manage common setting actions
 *
 * @package    Coyote
 * @category   Controllers
 * @author     Tosif Qureshi 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Settings extends Admin_Controller
{

	/**
	 * Controller constructor sets the Title and Permissions
	 */
	
	public function __construct() {
		parent::__construct();
		Template::set('toolbar_title', 'Offer');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('beacon_model')){
			$this->load->model('beacon_model', 'beacon_model');
		}
		$this->lang->load('beacon_admin');
		$this->load->library('form_validation');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index() {

	}//end index
	
    
    //--------------------------------------------------------------------

	/**
	 * Display Tablet Setting Form
	 *
	 * @return void
	 */
	public function tablet_settings() {
		// get setting data
		$setting_data = $this->beacon_model->get_beacons_setting('unlock_password');
		$unlock_password = (!empty($setting_data)) ? $setting_data->value : '';
		// get version setting data
		$app_version_data = $this->beacon_model->get_app_setting('android',2);
		$app_version  = (!empty($app_version_data)) ? $app_version_data->mini_app_version : '';
		$os_version  = (!empty($app_version_data)) ? $app_version_data->mini_os_version : '';
		// set view params	
		Template::set('unlock_password',$unlock_password);
		Template::set('app_version',$app_version);
		Template::set('os_version',$os_version);
		Template::set_view('admin/settings/tablet_settings');
		Template::render();
	}
	
	/**
	 * Function to store tablet setting in database
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	public function tablet_store_settings() {
		$data = array();
		// apply validation rules for input params
		$this->form_validation->set_rules('unlock_password', 'lang:unlock_password', 'trim|required');	
		$this->form_validation->set_rules('app_version', 'lang:app_version', 'trim|required|number');
		//$this->form_validation->set_rules('os_version', 'lang:os_version', 'trim|required|number');
		// check validation 
		if($this->form_validation->run() === FALSE) {
			Template::set_message("Invalid input value",'error');
			redirect('backend/settings/tablet_settings');
		}
		
		// set post values in store settings
		$unlock_password = $this->input->post('unlock_password');
		$form_action     = $this->input->post('form_action');
		$this->beacon_model->save_tablet_setting($unlock_password,$form_action);
		
		// set post value for app version and update version data
		$app_version     = $this->input->post('app_version');
		//$os_version      = $this->input->post('os_version');
		$form_ver_action = $this->input->post('form_version_action');
		$this->beacon_model->save_tablet_version_setting(array('mini_app_version'=>$app_version),$form_ver_action);
		// redirect to setting page with message
		Template::set_message(lang('beacon_setting_success_msg'),'success');
		redirect('backend/settings/tablet_settings');
		
	}
	
	 
    //--------------------------------------------------------------------

	/**
	 * Display Kiosk Setting Form
	 *
	 * @return void
	 */
	public function kiosk_settings() {
		// get version setting data
		$app_version_data = $this->beacon_model->get_kiosk_setting();
		$employee_discount  = (!empty($app_version_data)) ? $app_version_data->employee_discount : '';
		$normal_discount  = (!empty($app_version_data)) ? $app_version_data->normal_discount : '';
		$exclude_category  = (!empty($app_version_data)) ? $app_version_data->exclude_category : '';
		// set view params	
		Template::set('employee_discount',$employee_discount);
		Template::set('normal_discount',$normal_discount);
		Template::set('exclude_category',$exclude_category);
		Template::set_view('admin/settings/kiosk_settings');
		Template::render();
	}
	
	/**
	 * Function to store kiosk setting in database
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	public function kiosk_store_settings() {
		$data = array();
		// apply validation rules for input params
		$this->form_validation->set_rules('employee_discount', 'lang:employee_discount', 'trim|number');	
		$this->form_validation->set_rules('normal_discount', 'lang:normal_discount', 'trim|number');
		// check validation 
		if($this->form_validation->run() === FALSE) {
			Template::set_message("Invalid input value",'error');
			redirect('backend/settings/kiosk_settings');
		}
		
		// set post value for app version and update version data
		$employee_discount     = $this->input->post('employee_discount');
		$normal_discount    = $this->input->post('normal_discount');
		$exclude_category    = $this->input->post('exclude_category');
		$form_ver_action = $this->input->post('form_version_action');
		$this->beacon_model->save_kiosk_setting(array('employee_discount'=>$employee_discount,'normal_discount'=>$normal_discount,'exclude_category'=>$exclude_category),$form_ver_action);
		// redirect to setting page with message
		Template::set_message(lang('beacon_setting_success_msg'),'success');
		redirect('backend/settings/kiosk_settings');
		
	}
	
    
    
		
}//end class
