<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Manage the action of vip offers i.e Beacons and Simple Offers
 *
 * @package    Coyote
 * @subpackage Modules VIP Offer
 * @category   Controllers
 * @author     Tosif Qureshi 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Vip_offer extends Admin_Controller
{


	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		Template::set('toolbar_title', 'Offer');
		$this->auth->restrict('Site.Reports.View');
		if (!class_exists('beacon_offers_model')){
			$this->load->model('beacon_offers_model', 'beacon_offers_model');
		}
		$this->lang->load('beaconoffers_admin');
		$this->load->library('form_validation');
		// set competition images path
		$this->image_path = FCPATH.'uploads/offer_images/'; 
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Load index action
	 *
	 * @return void
	 */
	public function index() {
		// call default beacon listing action
		$this->beacon();
	}//end index
	
	//--------------------------------------------------------------------

	/**
	 * Show listing of simple offers
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function beacon() {
		// fetch sinple type offer data
		$offers = $this->beacon_offers_model->get_offer_listing(0,0); 
		// set params in view loading
		Template::set('offers',$offers);
		Template::set_view('admin/beacon_offer/listing');
		Template::render();
	}
	
	public function create($id="") {				// controller will call in both the cases on add and edit
		/*echo "string";
		die();*/
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode offer id
		$data = array();
		
		if((isset($_POST['save'])) && !empty($id)) {
			$beacon_updated_id = $this->save_offer('update',$id);
			if(!empty($beacon_updated_id)) {
				Template::set_message(lang('message_update_offer'),'success');
			} else {
				Template::set_message(lang('message_invalid_beacon_offer'),'error');
			}
			redirect('backend/vip_offer/beacon');
		} else if(isset($_POST['save'])) {
			$beacon_inserted_id = $this->save_offer('insert');
			if(!empty($beacon_inserted_id)) { 
				Template::set_message(lang('message_saved_offer'),'success');	
			} else {
				Template::set_message(lang('message_invalid_beacon_offer'),'error');
			}
			redirect('backend/vip_offer/beacon');
		}
		if($id) {
			//	$offer_id = decode($id);
			$data['offer_title']		= lang('edit_beacon_offer');
			$beacon_offer_id = $id;
			$offer = $this->beacon_offers_model->getOfferList($beacon_offer_id);	
			
			if(empty($offer)) {
				Template::set_message(lang('message_invalid_beacon_offer'),'error');
				redirect('backend/vip_offer/beacon');
			}		
			$data['beacon_offer_id']			= encode($beacon_offer_id);
			$data['offer_name']					= $offer->offer_name;		
			$data['offer_price']				= $offer->offer_price;		
			$data['offer_short_description']	= $offer->offer_short_description;	
			$data['offer_long_description']		= $offer->offer_long_description;
			$data['offer_detail_page_image']		= $offer->offer_detail_page_image;
			$data['offer_image_1']		= $offer->offer_image_1;	
			$data['offer_image_2']		= $offer->offer_image_2;	
			$data['offer_image_3']		= $offer->offer_image_3;
			$data['start_date']			= date("Y-m-d",strtotime($offer->start_date));
			$data['end_date']			= date("Y-m-d",strtotime($offer->end_date));
			$data['start_time']			= $offer->start_time;
			$data['end_time']			= $offer->end_time;	
			//$data['start_time']			= date("d-m-Y H:i:s",strtotime($offer->start_time));
			//$data['end_time']			= date("d-m-Y H:i:s",strtotime($offer->end_time));
			$data['beacon_id']			= $offer->beacon_id;	
			$data['store_id']			= $offer->store_id;	
			$data['category_id']		= $offer->category_id;
			$data['is_banner_image']	= $offer->is_banner_image;	
			$data['barcode_image']		= $offer->barcode_image;
			$data['barcode']	        = $offer->barcode;
			$data['beacon_code']	    = $offer->beacon_code;
			$data['is_image_show_in_slider']	    = $offer->is_image_show_in_slider;	
			//$data['price']	       	    = (isset($offer->price)) ? $offer->price : 0;	
		} else {
			$data['offer_title']		= lang('add_new_beacon_offer');
			$data['beacon_offer_id']			=	0;
			$data['offer_name']			=	'';
			$data['offer_price']		=	'';
			$data['offer_short_description']	=	'';
			$data['offer_long_description']		=	'';
			$data['offer_detail_page_image'] = '';
			$data['offer_image_1']		=	'';
			$data['offer_image_2']		=	'';
			$data['offer_image_3']		=	'';
			$data['start_date']			=	'';
			$data['end_date']			=	'';
			$data['start_time']			=	'';
			$data['end_time']			=	'';
			$data['beacon_id']			= 	'';
			$data['store_id']			= 	'';
			$data['category_id']		=   '';
			$data['is_banner_image']	= 	'';
			$data['barcode_image']	    = 	'';
			$data['barcode']	        = 	'';
			$data['beacon_code']	    =   '';
			$data['is_image_show_in_slider']  =   0;
			//$data['price']	       	    =   '';
		}
		$data['beacons'] = $this->beacon_offers_model->getBeaconList();		
		//$data['stores'] = $this->beacon_offers_model->getStoreList();
		$data['stores'] = $this->beacon_offers_model->get_store_list('beacon');	
		$data['categories'] = $this->beacon_offers_model->get_category_list();	
		Template::set('data',$data);
		Template::set_view('admin/beacon_offer/offer_form');
		Template::render();	
	}
	
	public function save_offer($type = 'insert',$id=0) {
		
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['beacon_offer_id'] = $id;
		}
		
		$start_date = date("Y-m-d",strtotime($data['start_date']));
		$end_date 	= date("Y-m-d",strtotime($data['end_date']));
		
		$this->form_validation->set_rules('offer_name', 'lang:offer_name', 'trim|required|max_length[80]');		
		$this->form_validation->set_rules('offer_price', 'lang:offer_price', 'trim|required|numeric');	
		//$this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');	
		$this->form_validation->set_rules('offer_short_description', 'lang:offer_short_description', 'trim');	
		$this->form_validation->set_rules('offer_long_description', 'lang:offer_long_description', 'trim');	
		//$this->form_validation->set_rules('beacon_id', 'lang:offer_beacon', 'trim|required');	
		$this->form_validation->set_rules('store_id[]', 'lang:offer_store', 'trim|required');
		$this->form_validation->set_rules('category_id', 'lang:category', 'trim|required');
		$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');	
		$this->form_validation->set_rules('end_date', 'lang:end_date', 'trim|required');
		$this->form_validation->set_rules('start_time', 'lang:start_time', 'trim|required');	
		$this->form_validation->set_rules('end_time', 'lang:end_time', 'trim|required');
		$this->form_validation->set_rules('barcode', 'lang:offer_barcode', 'trim|numeric|max_length[12]');	
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}

		if (isset($_FILES['offer_detail_page_image']) && ($_FILES['offer_detail_page_image']['name']) != '' ) {
			$data['offer_detail_page_image'] = $this->to_upload_image('offer_detail_page_image');
		}

		// set offer images value as default blank
		if (isset($_FILES['offer_image_1']) && ($_FILES['offer_image_1']['name']) != '' ) {
			$data['offer_image_1'] = $this->to_upload_image('offer_image_1');
		}
		if (isset($_FILES['offer_image_2']) && ($_FILES['offer_image_2']['name']) != '' ) {
			$data['offer_image_2'] = $this->to_upload_image('offer_image_2');
		}
		if (isset($_FILES['offer_image_3']) && ($_FILES['offer_image_3']['name']) != '' ) {
			$data['offer_image_3'] = $this->to_upload_image('offer_image_3');
		}
		// upload barcode image
		/*if (isset($_FILES['barcode_image']) && ($_FILES['barcode_image']['name']) != '' ) {
			$data['barcode_image'] = $this->upload_barcode_image('barcode_image');
		}*/
		$start_date = date("Y-m-d",strtotime($this->input->post('start_date')));
		$end_date = date("Y-m-d",strtotime($this->input->post('end_date')));
		
		// manage store ids
		$store_data = '';
		$store_ids = $this->input->post('store_id');
		if(is_array($store_ids) && !empty($store_ids)) {
			$store_data = implode(',',$store_ids);
			//$store_data = json_encode($store_ids);
		}
		$store_data = ",".$store_data.",";
		
		$data['offer_name']     = ($this->input->post('offer_name'));
		$data['offer_price']    = $this->input->post('offer_price');
		//$data['price']    		= $this->input->post('price');
		$data['offer_short_description']    = ($this->input->post('offer_short_description'));
		$data['offer_long_description']    	= ($this->input->post('offer_long_description'));

		//$data['beacon_id']    	= $this->input->post('beacon_id');
		$data['store_id']    	= $store_data;
		$data['category_id']    = $this->input->post('category_id');
		$data['start_date']    	= $start_date;
		$data['end_date']    	= $end_date;
		$data['start_time']    	= $this->input->post('start_time');
		$data['end_time']    	= $this->input->post('end_time');
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_image_show_in_slider']= (!empty($this->input->post('is_image_show_in_slider'))) ? 1 : 0;
		if($this->input->post('is_banner_image')) {
			$data['is_banner_image']     = $this->input->post('is_banner_image');
		} else {
			$data['is_banner_image']     = '0';
		}

		if($this->beacon_offers_model->check_beacon($data) != "") { // check beacon is available on given data and time or not
			Template::set_message(lang('cannot_place_offer'));
			return FALSE;
		}
		// manage barcode
		$barcode = $this->input->post('barcode');
		if(!empty($barcode)) {
			// get barcode data
			
			$barcode_data = $this->beacon_offers_model->get_barcode_data($barcode,0,$id);
			if($barcode_data == 0) {
				// generate barcode image
				$barcode_image = $this->generate_zend_barcode($barcode);
			}
		}
		// set barcode values
		$data['barcode'] = (!empty($barcode)) ? $barcode : '';
		$data['barcode_image']   = (isset($barcode_image) && !empty($barcode_image)) ? $barcode_image : '';
		// manage storing offer data
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->beacon_offers_model->add_offer($data);
		}
		else if ($type == 'update') {
			// update the offer seen count
			$this->update_offer_seen_count($data);
			// update offer data
			$id = $this->beacon_offers_model->add_offer($data);
		}
		return $id;

	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to update offer seen count
	 * @access: private
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	private function update_offer_seen_count($data) {
		if(!empty($data) && isset($data['beacon_offer_id']) && !empty($data['beacon_offer_id'])) {
			// set offer id
			$beacon_offer_id = $data['beacon_offer_id'];
			// get offer information
			$offer = $this->beacon_offers_model->getOfferList($beacon_offer_id);
			if((!empty($offer)) && ($data['start_time'] != $offer->start_time || $data['end_time'] != $offer->end_time)) {
				// update offer seen count as zero
				$offer_cust_data = array('offer_seen_count'=>0);
				$this->beacon_offers_model->update_offer_seen_count($offer_cust_data,$beacon_offer_id);
			}
		}
	}
	
	public function offer_status($id='',$status='',$store_id='') {
		$data['beacon_offer_id'] = decode($id);
		$status = decode($status);
		$store_id = decode($store_id);
		
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
			// check beacon availability in store
			if(!empty($store_id)) {
				// get store's beacon data
				//$beacon_data = $this->beacon_offers_model->get_store_beacon($store_id,$data['beacon_offer_id']);
				$beacon_data = $this->beacon_offers_model->getOfferList($data['beacon_offer_id']);
				if(!empty($beacon_data)) {
					// authenticate beacon availability for given date time
					$store_ids = (isset($beacon_data->store_id) && !empty($beacon_data->store_id)) ? explode(',',$beacon_data->store_id) : '';
					$beacon_offer_data['start_date']= (isset($beacon_data->start_date) && !empty($beacon_data->start_date)) ? $beacon_data->start_date : '';
					$beacon_offer_data['end_date'] 	= (isset($beacon_data->end_date) && !empty($beacon_data->end_date)) ? $beacon_data->end_date : '';
					$beacon_offer_data['start_time']= (isset($beacon_data->start_time) && !empty($beacon_data->start_time)) ? $beacon_data->start_time : '';
					$beacon_offer_data['end_time'] 	= (isset($beacon_data->end_time) && !empty($beacon_data->end_time)) ? $beacon_data->end_time : '';
					$beacon_offer_data['beacon_offer_id']  = $data['beacon_offer_id'];
					$assign_store = array();
					$store_name_set = '';
					if(!empty($store_ids) && $beacon_offer_data['start_time'] != '' && $beacon_offer_data['end_time'] != '' && $data['beacon_offer_id'] != '') {
						foreach($store_ids as $store_id) {
							$beacon_offer_data['store_id'] = $store_id;
							$offerCount = $this->beacon_offers_model->check_beacon($beacon_offer_data);
							if($offerCount > 0) {
								$assign_store[] = $beacon_offer_data['store_id'];
							}
						}
				
						$store_names = '';
						if(!empty($assign_store)){
							$store_names = $this->beacon_offers_model->get_store_names($assign_store);
							foreach($store_names as $store_name){
								$store_name_set .= $store_name['store_name'].',';
							}
						}
					}
					
					if(!empty($store_name_set)) {
						Template::set_message(lang('beacons_already_occupied'),'error');
						redirect('backend/vip_offer');
					}
				}
			}
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/vip_offer');
	}
	
	public function offer_delete($id='') {
		$data['beacon_offer_id'] = decode($id);
		$data['is_delete'] = 1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/vip_offer');
	}
	
	function checkFreeBeacon() {   	// controller is called will add/edit date time through ajax request  
		//$data['beacon_id'] = $this->input->post('beacon_id');
		$store_ids = $this->input->post('store_ids');
		$data['start_date']	= $this->input->post('start_date');
		$data['end_date'] 	= $this->input->post('end_date');
		$data['start_time'] = $this->input->post('start_time');
		$data['end_time'] 	= $this->input->post('end_time');
		$data['beacon_offer_id'] = $this->input->post('beacon_offer_id');
		$assign_store = array();
		$store_name_set = '';
		if(!empty($store_ids) && $data['start_time'] != '' && $data['end_time'] != '' && $data['beacon_offer_id'] != '') {
			foreach($store_ids as $store_id) {
				$data['store_id'] = $store_id;
				$offerCount = $this->beacon_offers_model->check_beacon($data);
				if($offerCount > 0) {
					$assign_store[] = $data['store_id'];
				}
			}
			//print_r($assign_store); die;
			$store_names = '';
			if(!empty($assign_store)){
				$store_names = $this->beacon_offers_model->get_store_names($assign_store);
				foreach($store_names as $store_name){
					$store_name_set .= $store_name['store_name'].',';
				}
			}
		}
		echo $store_name_set;
		die;
		//echo $offerCount = $this->beacon_offers_model->check_beacon($data);  // returns the no of offer associate with beacon and store
	}
	
	function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'offer_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '5120';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/vip_offer');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	} //to_upload_image
	/*Offers : End*/
		
	
	//--------------------------------------------------------------------

	/**
	 * Show listing of simple offers
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function simple() {
		// fetch sinple type offer data
		$offers = $this->beacon_offers_model->get_offer_listing(0,1); 
		// set params in view loading
		Template::set('offers',$offers);
		Template::set_view('admin/beacon_offer/simple_listing');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manege simple offer form
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function create_simple_offer($id="") {
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode offer id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_simple_offer()) {       
					Template::set_message(lang('message_saved_offer'),'success');
					redirect('backend/vip_offer/simple');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_simple_offer('update',$id)) {
					Template::set_message(lang('message_update_offer'),'success');
					redirect('backend/vip_offer/simple');
			}
		}
		$data['offer_title']	= lang('add_new_simple_offer');
		if($id) {
			
			$data['offer_title']		= lang('edit_simple_offer');
			$beacon_offer_id = $id;
			// get simple offer data
			$offer = $this->beacon_offers_model->getOfferList($beacon_offer_id);
			if(empty($offer)) {
				Template::set_message(lang('message_invalid_simple_offer'),'error');
				redirect('backend/vip_offer/simple');
			}	
		}
		
		// set input form values
		$data['beacon_offer_id']		= (!empty($beacon_offer_id)) ? encode($beacon_offer_id) : '';
		$data['offer_name']				= isset($offer->offer_name) ? $offer->offer_name : '';		
		$data['offer_price']			= isset($offer->offer_price) ? $offer->offer_price : '';		
		$data['offer_short_description']= isset($offer->offer_short_description) ? $offer->offer_short_description : '';	
		$data['offer_long_description']	= isset($offer->offer_long_description) ? $offer->offer_long_description : '';
		$data['offer_detail_page_image']			= isset($offer->offer_detail_page_image) ? $offer->offer_detail_page_image : '';
		$data['offer_image_1']			= isset($offer->offer_image_1) ? $offer->offer_image_1 : '';	
		$data['offer_image_2']			= isset($offer->offer_image_1) ? $offer->offer_image_2 : '';
		$data['offer_image_3']			= isset($offer->offer_image_1) ? $offer->offer_image_3 : '';
		$data['start_time']				= date("Y-m-d H:i:s",strtotime($offer->start_time));
		$data['end_time']				= date("Y-m-d H:i:s",strtotime($offer->end_time));	
		$data['store_id']				= isset($offer->store_id) ? $offer->store_id : '';	
		$data['category_id']			= isset($offer->category_id) ? $offer->category_id : '';
		$data['is_banner_image']		= isset($offer->is_banner_image) ? $offer->is_banner_image : '';	
		$data['barcode_image']			= isset($offer->barcode_image) ? $offer->barcode_image : '';
		$data['barcode']	       		= isset($offer->barcode) ? $offer->barcode : '';	
		$data['is_image_show_in_slider'] = isset($offer->is_image_show_in_slider) ? $offer->is_image_show_in_slider : 0;

		$data['beacons'] = $this->beacon_offers_model->getBeaconList();		
		$data['stores'] = $this->beacon_offers_model->get_store_list();	
		$data['categories'] = $this->beacon_offers_model->get_category_list();	
		Template::set('data',$data);
		Template::set_view('admin/beacon_offer/simple_offer_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manege simple offer form
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function save_simple_offer($type = 'insert',$id=0) {
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['beacon_offer_id'] = $id;
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('offer_name', 'lang:offer_name', 'trim|required');		
		$this->form_validation->set_rules('offer_price', 'lang:offer_price', 'trim|required|numeric');		
		$this->form_validation->set_rules('offer_short_description', 'lang:offer_short_description', 'trim');	
		$this->form_validation->set_rules('offer_long_description', 'lang:offer_long_description', 'trim');	
		$this->form_validation->set_rules('store_id', 'lang:offer_store', 'trim|required');	
		$this->form_validation->set_rules('category_id', 'lang:category', 'trim|required');	
		$this->form_validation->set_rules('start_time', 'lang:start_time', 'trim|required');	
		$this->form_validation->set_rules('end_time', 'lang:end_time', 'trim|required');
		$this->form_validation->set_rules('barcode', 'lang:offer_barcode', 'trim|numeric|required|max_length[12]');	
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}

		if (isset($_FILES['offer_detail_page_image']) && ($_FILES['offer_detail_page_image']['name']) != '' ) {
			$data['offer_detail_page_image'] = $this->to_upload_image('offer_detail_page_image');
		}
		
		// upload banner images
		if (isset($_FILES['offer_image_1']) && ($_FILES['offer_image_1']['name']) != '' ) {
			$data['offer_image_1'] = $this->to_upload_image('offer_image_1');
		}
		if (isset($_FILES['offer_image_2']) && ($_FILES['offer_image_2']['name']) != '' ) {
			$data['offer_image_2'] = $this->to_upload_image('offer_image_2');
		}
		if (isset($_FILES['offer_image_3']) && ($_FILES['offer_image_3']['name']) != '' ) {
			$data['offer_image_3'] = $this->to_upload_image('offer_image_3');
		}
		// upload barcode image
		/*if (isset($_FILES['barcode_image']) && ($_FILES['barcode_image']['name']) != '' ) {
			$data['barcode_image'] = $this->upload_barcode_image('barcode_image');
		}*/
		// set post values in store bundle		
		$startDateExp = explode(" ",$this->input->post('start_time'));
		$endDateExp = explode(" ",$this->input->post('end_time'));
		$start_date = date("Y-m-d",strtotime($startDateExp[0]));
		$end_date = date("Y-m-d",strtotime($endDateExp[0]));
		$data['offer_name']    	= ($this->input->post('offer_name'));
		$data['offer_price']    = $this->input->post('offer_price');
//		$data['price']    		= $this->input->post('price');
		$data['offer_short_description']    = ($this->input->post('offer_short_description'));

		$data['offer_long_description']    	= ($this->input->post('offer_long_description'));
		$data['store_id']    	= $this->input->post('store_id');
		$data['category_id']    = $this->input->post('category_id');
		$data['start_date']    	= $start_date;
		$data['end_date']    	= $end_date;
		$data['start_time']    	= $startDateExp[1];
		$data['end_time']    	= $endDateExp[1];
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_simple_offer']= 1;
		$data['is_image_show_in_slider']= (!empty($this->input->post('is_image_show_in_slider'))) ? 1 : 0;
		if($this->input->post('is_banner_image')) {
			$data['is_banner_image']     = $this->input->post('is_banner_image');
		} else {
			$data['is_banner_image']     = '0';
		}

		if($this->beacon_offers_model->check_beacon($data) > 0) { // check beacon is available on given data and time or not
			Template::set_message(lang('cannot_place_offer'));
			return FALSE;
		}
		// manage barcode
		$barcode = $this->input->post('barcode');
		if(!empty($barcode)) {
			// get barcode data
			$barcode_data = $this->beacon_offers_model->get_barcode_data($barcode,1,$id);
			if($barcode_data == 0) {
				// generate barcode image
				$barcode_image = $this->generate_zend_barcode($barcode);
			}
		}
		// set barcode values
		$data['barcode'] = (!empty($barcode)) ? $barcode : '';
		$data['barcode_image']   = (isset($barcode_image) && !empty($barcode_image)) ? $barcode_image : '';
		// manage storing offer data
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->beacon_offers_model->add_offer($data);
		}
		else if ($type == 'update') {
			// update offer data
			$id = $this->beacon_offers_model->add_offer($data);
		}
		return $id;

	}
	
	//--------------------------------------------------------------------

	/**
	 * Show listing of fallback offers
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function fallback() {
		// fetch sinple type offer data
		$offers = $this->beacon_offers_model->get_offer_listing(0,0,1); 
		// set params in view loading
		Template::set('offers',$offers);
		Template::set_view('admin/beacon_offer/fallback_listing');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manage fallback offer form
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function create_fallback_offer($id="") {
		$id = decode($id); // decode offer id
		$data = array();
		$data['offer_title'] = lang('add_new_fallback_offer');
		if((isset($_POST['save'])) && empty($id)) {
			if($this->save_fallback_offer()) {     
					Template::set_message(lang('message_saved_offer'),'success');
					redirect('backend/vip_offer/fallback');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_fallback_offer('update',$id)) {
					Template::set_message(lang('message_update_offer'),'success');
					redirect('backend/vip_offer/fallback');
			}
		}
		if($id) {
			$data['offer_title'] = lang('edit_fallback_offer');
			$beacon_offer_id = $id;
			$offer = $this->beacon_offers_model->getOfferList($beacon_offer_id);	
			
			if(empty($offer)) {
				Template::set_message(lang('message_invalid_beacon_offer'),'error');
				redirect('backend/vip_offer/fallback');
			}
		} 
		// set input form values
		$data['beacon_offer_id']			= (!empty($beacon_offer_id)) ? encode($beacon_offer_id) : '';
		$data['offer_name']					= isset($offer->offer_name) ? $offer->offer_name : '';		
		$data['offer_price']				= isset($offer->offer_price) ? $offer->offer_price : '';		
		$data['offer_short_description']	= isset($offer->offer_short_description) ? $offer->offer_short_description : '';	
		$data['offer_long_description']		= isset($offer->offer_long_description) ? $offer->offer_long_description : '';
		$data['offer_detail_page_image'] = isset($offer->offer_detail_page_image) ? $offer->offer_detail_page_image : '';
		$data['image_1']			= isset($offer->offer_image_1) ? $offer->offer_image_1 : '';	
		$data['image_2']			= isset($offer->offer_image_1) ? $offer->offer_image_2 : '';
		$data['image_3']			= isset($offer->offer_image_1) ? $offer->offer_image_3 : '';
		$data['beacon_id']			= isset($offer->offer_image_1) ? $offer->beacon_id : '';	
		$data['store_id']			= isset($offer->offer_image_1) ? $offer->store_id : '';	
		$data['category_id']		= isset($offer->offer_image_1) ? $offer->category_id : '';
		$data['is_banner_image']	= isset($offer->offer_image_1) ? $offer->is_banner_image : '';	
		$data['barcode_image']		= isset($offer->offer_image_1) ? $offer->barcode_image : '';
		$data['barcode']	        = isset($offer->offer_image_1) ? $offer->barcode : '';
		$data['beacon_code']	    = isset($offer->offer_image_1) ? $offer->beacon_code : '';	
		$data['is_image_show_in_slider'] = isset($offer->is_image_show_in_slider) ? $offer->is_image_show_in_slider : '';	

		$data['beacons'] = $this->beacon_offers_model->getBeaconList();
		$data['stores'] = $this->beacon_offers_model->get_store_list('beacon');	
		$data['categories'] = $this->beacon_offers_model->get_category_list();	

		Template::set('data',$data);
		Template::set_view('admin/beacon_offer/fallback_offer_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Manage store procedure of fallback beacon offer
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function save_fallback_offer($type = 'insert',$id=0) {
	
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['beacon_offer_id'] = $id;
		}
		
		// apply validation rules for input params
		$this->form_validation->set_rules('offer_name', 'lang:offer_name', 'trim|required');		
		$this->form_validation->set_rules('offer_price', 'lang:offer_price', 'trim|required|numeric');		
		$this->form_validation->set_rules('offer_short_description', 'lang:offer_short_description', 'trim');	
		$this->form_validation->set_rules('offer_long_description', 'lang:offer_long_description', 'trim');	
		$this->form_validation->set_rules('store_id[]', 'lang:offer_store', 'trim|required');
		$this->form_validation->set_rules('category_id', 'lang:category', 'trim|required');	
		$this->form_validation->set_rules('barcode', 'lang:offer_barcode', 'trim|numeric|required|max_length[12]');
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			redirect('backend/vip_offer/create_fallback_offer');
			return FALSE;
		}
		

		if (isset($_FILES['offer_detail_page_image']) && ($_FILES['offer_detail_page_image']['name']) != '' ) {
			$data['offer_detail_page_image'] = $this->to_upload_image('offer_detail_page_image');
		}
		

		// upload banner images
		if (isset($_FILES['image_1']) && ($_FILES['image_1']['name']) != '' ) {
			$data['offer_image_1'] = $this->to_upload_image('image_1');
		}
		if (isset($_FILES['image_2']) && ($_FILES['image_2']['name']) != '' ) {
			$data['offer_image_2'] = $this->to_upload_image('image_2');
		}
		if (isset($_FILES['image_3']) && ($_FILES['image_3']['name']) != '' ) {
			$data['offer_image_3'] = $this->to_upload_image('image_3');
		}
		// manage store ids
		$store_data = '';
		$store_ids = $this->input->post('store_id');
		if(is_array($store_ids) && !empty($store_ids)) {
			$store_data = implode(',',$store_ids);
			//$store_data = json_encode($store_ids);
		}
		$store_data = ",".$store_data.",";
		
		$data['offer_name']    	= $this->input->post('offer_name');
		$data['offer_price']    = $this->input->post('offer_price');
		$data['offer_short_description']    = $this->input->post('offer_short_description');
		$data['offer_long_description']    	= $this->input->post('offer_long_description');
		$data['store_id']    	= $store_data;
		$data['category_id']    = $this->input->post('category_id');
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_default'] 	= 1;
		$data['is_image_show_in_slider']= (!empty($this->input->post('is_image_show_in_slider'))) ? 1 : 0;
		if($this->input->post('is_banner_image')) {
			$data['is_banner_image']     = $this->input->post('is_banner_image');
		} else {
			$data['is_banner_image']     = '0';
		}
		
		if($this->beacon_offers_model->check_default_beacon_offer($data) > 0) { // check beacon is available on given data and time or not
			Template::set_message(lang('cannot_place_offer_for_store'));
			redirect('backend/vip_offer/create_fallback_offer');
			return FALSE;
		}
		
		// manage barcode
		$barcode = $this->input->post('barcode');
		if(!empty($barcode)) {
			// get barcode data
			$barcode_data = $this->beacon_offers_model->get_barcode_data($barcode,1,$id);
			if($barcode_data == 0) {
				// generate barcode image
				$barcode_image = $this->generate_zend_barcode($barcode);
			}
		}
		// set barcode values
		$data['barcode'] = (!empty($barcode)) ? $barcode : '';
		$data['barcode_image']   = (isset($barcode_image) && !empty($barcode_image)) ? $barcode_image : '';
		// manage storing offer data
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->beacon_offers_model->add_offer($data);
		}
		else if ($type == 'update') {
			// update offer data
			$id = $this->beacon_offers_model->add_offer($data);
		}
		return $id;
	}
	
	/**
	 * function used to check default offer availability
	 * @param null
	 * @return string
	 * @access public
	 * 
	 */
	public function checkDefaultBeacon() {   	// controller is called will add/edit date time through ajax request  
		
		$store_ids = $this->input->post('store_ids');
		$data['beacon_offer_id'] = $this->input->post('beacon_offer_id');
		$assign_store = array();
		$store_name_set = '';
		if(!empty($store_ids) && $data['beacon_offer_id'] != '') {
			foreach($store_ids as $store_id) {
				$data['store_id'] = $store_id;
				$offerCount = $this->beacon_offers_model->check_default_beacon_offer($data);
				if($offerCount > 0) {
					$assign_store[] = $data['store_id'];
				}
			}
			
			$store_names = '';
			if(!empty($assign_store)){
				$store_names = $this->beacon_offers_model->get_store_names($assign_store);
				foreach($store_names as $store_name){
					$store_name_set .= $store_name['store_name'].',';
				}
			}
		}
		echo $store_name_set;die;
	}
	
	/**
	 * Function to remove fallback beacon offer
	 * @input  : id
	 * @output : void
	 * @access : public
	 */
	public function fallback_remove($id='') {
		$data['beacon_offer_id'] = decode($id);
		$data['is_delete'] = 1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/vip_offer/fallback');
	}
	
	/**
	 * Function to update fallback offer's status
	 * @input  : id , status
	 * @output : void
	 * @access : public
	 */
	public function update_fallback_status($id='',$status='') {
		$data['beacon_offer_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/vip_offer/fallback');
	}
	
	/**
	 * Function to store barcode image
	 * @input  : image
	 * @output : string
	 * @access : private
	 */
	private function upload_barcode_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'barcodes/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '5120';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('backend/vip_offer/simple');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	/**
	 * Function to remove simple offer
	 * @input  : id
	 * @output : void
	 * @access : public
	 */
	public function simple_offer_delete($id='') {
		$data['beacon_offer_id'] = decode($id);
		$data['is_delete'] = 1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/vip_offer/simple');
	}
	
	/**
	 * Function to update simple offer's status
	 * @input  : id , status
	 * @output : void
	 * @access : public
	 */
	public function update_offer_status($id='',$status='') {
		$data['beacon_offer_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->beacon_offers_model->add_offer($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('backend/vip_offer/simple');
	}
	
	/**
	 * Function to get beacons html
	 * @input  : store_id
	 * @output : string
	 * @access : public
	 */
	public function get_store_beacons($store_id=0) {
		// get store id from post
		$store_id = $this->input->post('store_id');
		$beacons = '';
		if(!empty($store_id)) {
			// get beacons data
			$beacons = $this->beacon_offers_model->get_store_beacons($store_id);
		}
		$beacon_html = '<select data-placement="right" data-toggle="tooltip" id="beacon_id" name="beacon_id" data-original-title="Select the beacon for the offer.">';
		$beacon_html .= '<option value="">Select beacon</option>';
		if(!empty($beacons)) {
			foreach($beacons as $beacon) {
				$beacon_html .= '<option value="'.$beacon['beacon_id'].'">"'.$beacon['beacon_code'].'"</option>';
			}
		}
		$beacon_html .= '</select>';
		echo $beacon_html;die;
	}
	
	
	function test_barcode() {
		$image = $this->generate_ean_barcode('7567345323423');
		echo $image;die; 
	}
	/**
	 * Function to generate barcode
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	function generate_ean_barcode($barcode='') {
		// load barcode generate library
		$this->load->library('ean13');
		$ean13 = new ean13;
		$ean13->article = $barcode;  // initial article code
		$ean13->article .= $ean13->generate_checksum();   // add the proper checksum value
		$ean13->reverse();   // the string is printed backwards
		$ean13->article = $ean13->codestring();   // returns a string as input for the truetype font
		$ean13->create_image();   // render the image as PNG image
		$imgName = $ean13->article.'.png';
		return $imgName;
	}
	
	/**
	 * Function to generate barcode
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	function generate_barcode($barcode='') {
		// load barcode generate library
		$this->load->library('barcode');
		
		$fontSize = 10; // GD1 in px ; GD2 in point
		$marge = 10; // between barcode and hri in pixel
		  $x = 80;  // barcode center
		  $y = 50;  // barcode center
		$height = 50;  // barcode height in 1D ; module size in 2D
		$width = 2;  // barcode height in 1D ; not use in 2D
		$angle =0; // rotation in degrees 

		$type = 'code128';

		// ————————————————– //
		// ALLOCATE GD RESSOURCE
		// ————————————————– //
		$im = imagecreatetruecolor(200, 100);
		$black = ImageColorAllocate($im,0x00,0x00,0x00);
		$white = ImageColorAllocate($im,0xff,0xff,0xff);
		imagefilledrectangle($im, 0, 0, 300, 300, $white);

		$src = imagecreatefromgif($barcode);
		imagecopyresampled($tmp, $src, 0, 0, 0, 0, 300, 300, 300, 300);
		$dirPath = IMAGEUPLOADPATH.'barcodes/';
		$dst = $dirPath.$im;

		// ————————————————– //
		// GENERATE
		// ————————————————– //
		
		$data = Barcode::gd($im, $black, $x, $y, $angle, $type,   array('code'=>$barcode), $width, $height);
		header('Content-type: image/gif');
		$imgName = $barcode.'.jpg';
		imagejpeg($im, $dirPath.$imgName);
		imagegif($im);
		imagedestroy($im);
		return $imgName;
	}
	
	/**
	 * Function to generate barcode from zend library
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	private function generate_zend_barcode($barcode='')
	{
	
		// load zend library
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		// prepare barcode from zend library
		$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
		// set barcode image path
		$dir_path = IMAGEUPLOADPATH.'barcodes/';
		// set barcode image name
		//$image_name = time().$barcode;
		$image_name = $barcode.'.png';
		// create image from barcode
		ImagePNG($file, $dir_path.$image_name);
		ImageDestroy($file);
		return $image_name;
		
	}
	
	/**
	 * Function to manage beacon availability of store
	 * @input  : store id
	 * @output : json
	 * @access : public
	 */
	public function manage_beacon_availability() {
		// get store id from post
		$store_id = $this->input->post('store_id');
		$beacon_offer_id = $this->input->post('beacon_offer_id');
		// get beacon data
		$beacon_ids = 1;
		$beacon_data = $this->beacon_offers_model->get_store_beacon($store_id,$beacon_offer_id);
		if(!empty($beacon_data) && $beacon_data->is_simple_offer == 0) {
			$beacon_ids = $beacon_data->beacon_id.'_'.$beacon_data->beacon_code;
			if(!empty($beacon_data->beacon_offer_id) && $beacon_offer_id != $beacon_data->beacon_offer_id && $beacon_data->offer_status == 1 && $beacon_data->offer_deleted == 0) {
				$beacon_ids = 0;
			}
		}
		echo $beacon_ids;die;
	}
	
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_offer_image() {
		// get data from post
		$offer_id = $this->input->post('offer_id');
		$image_type = $this->input->post('image_type');
		$return = false;
		if(!empty($image_type) && !empty($offer_id)) {
			// update offer image value as blank
			$data['beacon_offer_id'] = $offer_id;
			$data['offer_image_'.$image_type] = "";
			$this->beacon_offers_model->add_offer($data);
			$return = true;
		}
		echo $return;die;
	}
	
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_image() {
		// get data from post
		$id = $this->input->post('id');
		$image_type = $this->input->post('image_type');
		$file_name = $this->input->post('file_name');
		$return = false;
		if(!empty($image_type) && !empty($id)) {
			// update image value as blank
			$data['beacon_offer_id'] = decode($id);
			$data['offer_image_'.$image_type] = "";
			// delete image from folder
			unlink($this->image_path.$file_name);
			// update image value in db
			$this->beacon_offers_model->add_offer($data);
			$return = true;
		}
		echo $return;die;
	}
	
	
	/**
	 * Function to check barcode existanse
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	public function check_barcode() {
		$beacon_data = $this->beacon_offers_model->get_store_beacon(1);
		echo '<pre>';
		print_r($beacon_data);die;
		
		// get barcode from post
		$barcode = $this->input->post('barcode');
		$barcode_not_exist = true;
		if(!empty($barcode)) {
			// get barcode data
			$barcode_data = $this->beacon_offers_model->get_barcode_data($barcode);
			if(!empty($barcode_data) && $barcode_data > 0) {
				$barcode_not_exist = false;
			}
		}
		echo $barcode_not_exist;die;
	}
		
}//end class
