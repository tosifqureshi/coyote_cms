<?php 
class cron extends Front_Controller
{
	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("edm/edm_model");
		$this->edm_tbl		  = 'edm';		 
		$this->email_template = 'email_template';	
		$this->master_log	  = 'edm_master_log';	 
		$this->edm_queue_log  = 'ava_edm_queue_log';	 
		$this->reward_cards   = 'reward_cards';	 
		$this->read_db = $this->load->database('read', TRUE);
	}//end __construct()
    
    /***** ALL CRON FUNCTION OF CI *****/
    public function index(){
	
	}
	/**
	 * edm_master_log for sending edm to the user
	 * @access edm_master_log
	 * @return  none 
	 **/	
	public function edm_master_log(){
		$edm_data = $this->edm_model->get_active_edm();
		if(!empty($edm_data)){
			foreach($edm_data  as $edata){
				$occurance_data = !empty($edata->occurance_data)?json_decode($edata->occurance_data):"";
				$master_log = $this->common_model->getDataFromTabel($this->master_log,'id',array('edm_id'=>$edata->id,'DATE(created_date)'=>date('Y-m-d')),'','id','DESC',1,'','','',1);
				if(empty($master_log)){
					  $check_occur = $this->check_occurance($edata->occurance_type,$occurance_data);
						if($check_occur){
							$master_log_data=array('edm_id'=>$edata->id,'created_date'=>date('Y-m-d H:i:s'));
							$this->common_model->addDataIntoTable($this->master_log,$master_log_data);	
						}
				}			 
			}
		}
		die;
	}
	
	/**
	 * edm_queue_log for sending edm to the user
	 * @access edm_queue_log
	 * @return  user list 
	 **/	
	public function edm_queue_log(){
		$master_log_data = $this->common_model->getDataFromTabel($this->master_log,'id,edm_id',array('DATE(created_date)'=>date('Y-m-d'),'status'=>0),'','','','','',1,'','');	
		if(!empty($master_log_data)){
		  foreach($master_log_data as $master_data){
		    $edmId    = $master_data['edm_id'];
		    $queueId  = $master_data['id'];
		    $edm_data = $this->edm_model->get_active_edm($edmId,TRUE);
		    if(!empty($edm_data)){
				if(!empty($edm_data['rec_type'])){
					// check for manual emails
					$manual_emails    = $edm_data['manual_emails'];
					if(!empty($manual_emails)) {
						// convert manual emails in array
						$edm_users = array_filter(explode(',',$manual_emails));
						$edm_users = array_values($edm_users);
						$rowcount  = count($edm_users);
						$is_always_send_to_all_users  = $edm_data['is_always_send_to_all_users'];
						if($is_always_send_to_all_users == 0) {
							// get existing queued email whos viewed by user
							$viewed_emails = $this->edm_model->edm_queue_sent_emails($edmId);
							if(!empty($viewed_emails)) {
								$viewed_email_array = [];
								foreach($viewed_emails as $val) {
									$viewed_email_array[] = $val['email'];
								}
								$edm_users = array_values(array_diff($edm_users, $viewed_email_array));
							}
						}
					} else {
						$edm_user_result = $this->edm_model->get_edm_user($edm_data);
						$rowcount  = $edm_user_result['rowcount'];
						$edm_users = $edm_user_result['result'];
					}
					
					if(!empty($rowcount)){
						$edm_user_sql = "";
						$Iscomma = 0;	
						foreach($edm_users as $users){
							if(!empty($manual_emails)) {
								$userId = 0;
								$email  = $users;
							} else {
								$userId = $users['id'];
								$email  = $users['email'];
							}
							$created_at    = date('Y-m-d H:i:s');
							$edm_user_sql .= (($Iscomma == 0)?"":",")."('$edmId','$userId','$email','$created_at')";
							$Iscomma ++;
						}
						if(!empty($edm_user_sql)){
							$insertSQL = "INSERT INTO ".$this->edm_queue_log."(edm_id,user_id,email,create_date) VALUES $edm_user_sql";
							$this->db->query($insertSQL);
							$this->common_model->updateDataFromTable($this->master_log,array('record_count'=>$rowcount,'status'=>2),'id',$queueId);
					   }
					}
				 }
			  }
		   }
		}
		die;
	 }
	
	/**
	 * check_occurance for edm 
	 * @access check edm is occurance is today or not
	 * @return  TRUE/FALSE
	 **/	
	public function check_occurance($occurance_type,$occurance_data){
		$return = FALSE;
		switch($occurance_type){
			case 1:
			$return = TRUE;
			break;
			case 2:
			$weeks   = $occurance_data;
			if(!empty($weeks)){
			 $date_day = strtolower(date('l',strtotime(date("Y-m-d"))));	
			 $return = (in_array($date_day,$weeks))?TRUE:FALSE;
			}
			break;
			case 3:
			$monthly = $occurance_data;
			if(!empty($monthly)){
			 $date_number = date('j',strtotime(date("Y-m-d")));											
			 $return = (in_array($date_number,$monthly))?TRUE:FALSE;
			}
			break;
			case 4:
			$yearly  = $occurance_data;
			if(!empty($yearly)){
			$return = (strtotime(date("Y-m-d")) == strtotime($yearly))?TRUE:FALSE;
			}
			break;
		}		
		return $return;
	}
	
	/**
	 * @Create email queue
	 * @function email worker
	 * @return  TRUE/FALSE
	 **/		
	public function create_worker(){
		$limit  = 50;
		$offset = 0;
		$query  = "SELECT MIN(id) as offsets,edm_id,count(edm_id) as edm_count FROM ".$this->edm_queue_log." WHERE is_read = 0 AND DATE(create_date) = '".date('Y-m-d')."' GROUP BY edm_id";
		$sql    = $this->read_db->query($query);
		$result	= $sql->result_array();
		if(!empty($result)){
			$edm_count	  = $result[0]['edm_count'];
			$edm_id		  = $result[0]['edm_id'];
			$offsets 	  = 0;
			if($edm_count > 0){
				if($edm_count){
					$workers 	 = $edm_count/$limit;
					$num_workers = ceil($workers);		  
					if($num_workers > 0){			
						$curl_array = array();		  
						$total_edm = $edm_count; 
						for($count=1;$count<=$num_workers;$count++){
							$qlimit = ($total_edm > $limit)?$limit:$total_edm;
							$curl_array[] = base_url('services/reward/send_edms?edm_id='.$edm_id.'&limit='.$qlimit.'&offset='.$offsets); 
							$offsets    = $offsets+$limit;
							$total_edm  = $total_edm-$limit;
						 }
						 $this->multiRequest($curl_array);
					}
				}
			}
		}
	}
   
   
   /**
	 * @Create multiRequest CURL
	 * @function multiRequest
	 * @return  CURL status
	 **/	
	function multiRequest($data,$options = array()){ 
		$curly  = array();
		$result = array();		 
		$mh     = curl_multi_init();
		foreach ($data as $key => $url){
			$curly[$key] = curl_init();
			curl_setopt($curly[$key], CURLOPT_URL,$url);
			curl_setopt($curly[$key], CURLOPT_HEADER,         0);
			curl_setopt($curly[$key], CURLOPT_RETURNTRANSFER, 1);		 
			
			//~ if (is_array($d)) {
				//~ if (!empty($d['post'])) {
					//~ curl_setopt($curly[$key], CURLOPT_POST,1);
					//~ curl_setopt($curly[$key], CURLOPT_POSTFIELDS, $d['post']);
				//~ }
			//~ }
			if (!empty($options)) {
			  curl_setopt_array($curly[$key], $options);
			}
			curl_multi_add_handle($mh, $curly[$key]);
		}
		$running = null;
		do{
			curl_multi_exec($mh, $running);
		}while($running > 0);
		  
		foreach($curly as $id => $c){
			$result[$id] = curl_multi_getcontent($c);
			curl_multi_remove_handle($mh,$c);
		}
		curl_multi_close($mh);
		return $result;
	 }
	
	function test_email_send() {
		
	}

   
}
?>
