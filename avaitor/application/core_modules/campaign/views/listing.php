<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('campaign') ?></h4>
				<a href="<?php echo site_url("admin/settings/campaign/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_campaign'); ?></button>
				</a>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				 <div class="table-responsive m-t-40">
					<?php 
					// define campaign types
					$campaign_types = array('All Campaigns','Scratch and Win','Crack the Egg');
					$data = array('name'=>'campaign_type','id'=>'campaign_type', 'class'=>'campaign_type');
					echo form_dropdown($data,$campaign_types,set_value('campaign_type',$campaign_type));?>
					<div style="clear:both;padding-bottom:10px"></div>
					<table id="competition_table"  class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><?php echo lang('sno'); ?></th>
								<th class="no-sort"><?php echo lang('snw-image'); ?></th>
								<th class="adjust_th"><?php echo lang('title'); ?></th>
								<th class="adjust_th_type"><?php echo lang('type'); ?></th>
								<th class="adjust_th"><?php echo lang('start_date'); ?></th>
								<th class="adjust_th"><?php echo lang('end_date'); ?></th>
								<th><?php echo lang('status'); ?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
								<th class="no-sort"><?php echo lang('Action'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							if(isset($results) && is_array($results) && count($results)>0):
							foreach($results as $result) :
								// set encoded id
								$snw_id = encode($result['snw_id']); ?>
								<tr>
									<td><?php echo $i;?></td>
									<td>
										<?php if(isset($result['banner_image']) && !empty($result['banner_image'])) {
											$snw_image = $result['banner_image'];
											$snw_image = (!empty($snw_image)) ? base_url('uploads/scratchnwin_images/'.$snw_image) : base_url('uploads/No_Image_Available.png');?>
											<img  id="imageresource" src="<?php echo $snw_image;?>" width="110" height="90" title="<?php echo $result['title'];?>">
										<?php } ?>
									</td>
									<td class="mytooltip tooltip-effect-1 desc-tooltip">
										<span class="text-ttip">
											<a href="<?php echo site_url("admin/settings/campaign/create/".$snw_id);?>"><?php echo (strlen($result['title']) > 20) ? substr($result['title'],0,20).'...' :  $result['title'];?></a>
										</span>
										<span class="tooltip-content clearfix">
											<span class="tooltip-text">
												<?php 
												$start_date_time = date('Y-m-d',strtotime($result['start_date']));
												$end_date_time = date('Y-m-d',strtotime($result['end_date'].' +1 days'));
												$diff =  get_date_diffrence($start_date_time, $end_date_time);
												echo '<b>'.lang('title').' : </b>'.$result['title'];
												echo '<br>';
												echo '<b>'.lang('start_date').' : </b>'.date('F j, Y',strtotime($result['start_date'])) ;
												echo '<br>';
												echo '<b>'.lang('end_date').'  : </b>'.date('F j, Y',strtotime($result['end_date'])) ;
												echo '<br>';
												echo $diff;?>
											</span> 
										</span>
										
									</td>
									<td><?php echo lang('campaign_type_'.$result['campaign_type']);?></td>
									<td><?php echo date('F j, Y',strtotime($result['start_date']));?></td>
									<td><?php echo date('F j, Y',strtotime($result['end_date']));?></td>
									<td>
										<?php
										// set change status url
										$status_url = site_url("admin/settings/campaign/change_status/".$snw_id.'/'.encode($result['status']));
										if($result['status'] == '0') { ?>
											<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
											<?php
										} else {
											?>
											<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
										} ?>
									</td>
									<td>
										<a class="remove-btn" onclick="removeData('<?php echo site_url("admin/settings/campaign/delete/".$snw_id);?>')"><i class="ti-trash"></i></a>
									</td>
									<td>
										<?php if($result['campaign_type'] == 3) { ?>
											<a href="<?php echo site_url("admin/settings/campaign/generate_participant_excel/".$snw_id);?>" title="<?php echo lang('participants_entries_download');?>"><i class="ti-download"></i></a>
										<?php }?>
									</td>
								</tr>
							<?php 
							$i++;
							endforeach;endif; ?>
						 </tbody>
					</table>                 
				</div>
			</div>
		</div>
	</div>
</div>

<!----------- Start competition members listing ------------->
<div id="modal_members_list"></div>

<script>
	$(document).ready(function() {
		$('#competition_table').DataTable();
	});
	
	/* 
	| ------------------------------------------------------------
	| Show competition members listing on popup 
	| ------------------------------------------------------------
	*/ 
	function competition_members(competition_id) {
		$.ajax ({
			type: "POST",
			data : {competition_id:competition_id},
			url: BASEURL+'admin/settings/competition/competition_members',
			async: false,
			success: function(data) {
				if(data) {
					$('#modal_members_list').html(data);
					$('#competitionmodal').modal('show');
				}
			}, 
			error: function(error){
				bootbox.alert(error);
			}
		});	
	}

	$( ".campaign_type" ).change(function() {
		// get selected campaign type 
		var selected_campaign_type  = $(this).val();
		window.location.href = BASEURL+'admin/settings/campaign/index/'+selected_campaign_type;
	});
</script>

