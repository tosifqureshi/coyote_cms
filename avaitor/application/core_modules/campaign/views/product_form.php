<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	$snw_dn = (isset($campaign_type) && ($campaign_type == 3)) ? 'dn' : '';
	$cte_dn = (!isset($campaign_type) || $campaign_type == 1) ? 'dn' : '';?>
	<tr class="snw-product-half-side product_tr <?php echo $snw_dn;?>" id="qty_row_<?php echo $row_id;?>">
		<td>
			<?php
			$data = array('name'=>'product_ids[]', 'value'=>'','id'=>'product_id_'.$row_id,'class'=>'form-control product_input number_input digits required','row_id'=>$row_id,'exist_id'=>'');
			echo form_input($data);
			$data = array('type'=>'hidden','name'=>'snw_product_ids[]','value'=>'');
			echo form_input($data);
			?>
		</td>
		<td>
			<?php
			$data = array('name'=>'product_name[]', 'value'=>'','readonly'=>true,'id'=>'product_name_'.$row_id,'class'=>'form-control','row_id'=>$row_id);
			echo form_input($data);?>
		</td>
		<td>
			<?php
			$data = array('name'=>'promo_code[]', 'value'=>'','readonly'=>true,'id'=>'promo_code_'.$row_id,'row_id'=>$row_id,'class'=>'form-control promo_code required');
			echo form_input($data);?>
		</td>
		<td>
			<?php
			$data = array('name'=>'prizes_per_day[]', 'value'=>'','readonly'=>true,'id'=>'prizes_per_day_'.$row_id,'class'=>'form-control prizes_count required digits','min'=>"1",'is_per_day_count'=>1,'row_id'=>$row_id);
			echo form_input($data);?>
		</td>
		<td>
			<?php
			$data = array('name'=>'total_prizes[]', 'value'=>'','readonly'=>true,'id'=>'total_prizes_'.$row_id,'class'=>'form-control prizes_count required cte_number_input digits','min'=>"1",'row_id'=>$row_id);
			echo form_input($data);?>
		</td>
		
		<td>
			<?php
			$data = array('name'=>'total_number_claimed[]', 'value'=>'','id'=>'total_number_claimed_'.$row_id,'class'=>'form-control','readonly'=>true);
			echo form_input($data);?>
		</td>
		
		<td>
			<div class="img-box">
				<?php
				$product_image = base_url('uploads/No_Image_Available.png');
				$title = "No Image Available";
				?>
				<div class="cell-img" id="product_image_cell_<?php echo $row_id;?>"><img id="picker_product_image_<?php echo $row_id;?>" class="product_img_picker" src="<?php echo $product_image; ?>"  data-toggle='tooltip', data-placement='right', title='' , row_id='<?php echo $row_id;?>' /></div>
				<?php
				$data = array('name'=>'product_image[]', 'id'=>'product_image_'.$row_id, 'value'=>'','class'=>'',  'data-toggle'=>'tooltip', 'data-placement'=>'right','row_id'=>$row_id, 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_product_image(this)');
				echo form_upload($data);
				// set image value in hidden type
				$data = array('name'=>'product_image_val[]', 'value'=>'','id'=>'hidden_product_image_'.$row_id,'type'=>'hidden');
				echo form_input($data);
				?>
			</div>
		</td>
		<td>	
			<?php
			// set day array
			$days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
			$day_btn_txt = '';
			$day_title_txt = '';
			foreach($days as $key=>$val) {
				// set button text
				$day_btn_txt .= ucfirst(substr($val,0,1));
				$day_btn_txt .= (count($days)>($key+1)) ? ' | ' : '';
				// set btn title text
				$day_title_txt .= ucfirst(substr($val,0,3));
				$day_title_txt .= (count($days)>($key+1)) ? ' | ' : '';
			}
			echo '<button id="daysavailability_btn_'.$row_id.'" class="btn default daysavailability" campaign_type="1" row_id = "'.$row_id.'" type="button" title="'.$day_title_txt.'">'.$day_btn_txt.'</button>';
			?>
			<!-- start the bootstrap modal where the image will appear -->
			<div class="modal fade" id="daysavailabilitymodal_<?php echo $row_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title modal_title_<?php echo $row_id;?>" id="myModalLabel_<?php echo $row_id;?>">&nbsp;</h4>
						</div>
						<div class="modal-body modal-days-avail">
							<?php
							$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
							foreach($week_days as $day) {
								$checked = 'checked';
								$short_day_name = ucfirst(substr($day,0,3));
								?>
								<input type="checkbox" name='<?php echo $short_day_name;?>[]' id='<?php echo $day.'_'.$row_id.'_1';?>' value="<?php echo $day;?>" class='days_avail_check day_checkbox_cls_<?php echo $row_id;?>'  <?php echo $checked;?> row_id = '<?php echo $row_id;?>' short_day_name = '<?php echo $short_day_name;?>' campaign_type = "1">
								<label for="<?php echo $day.'_'.$row_id.'_1';?>"><?php echo ucfirst($day);?></label>
								<input type="hidden" name='<?php echo $day;?>[]' value="1" id='day_checked_<?php echo $short_day_name.'_'.$row_id;?>'>
								<?php
							 } ?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
						</div>
					</div>
				</div>
			</div>
			<!-- end the bootstrap modal where the image will appear -->
		</td>
		<td>
			<span class="romove_row new-prod-row-icon" row_id=<?php echo $row_id;?> snw_product_id='' title="<?php echo lang('product_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
		</td>
	</tr>
	
	<tr class="snw-product-half-side brand_tr <?php echo $cte_dn;?>" id="brand_row_<?php echo $row_id;?>">	
		<td>
			<?php
			$data = array('name'=>'brand[]', 'value'=>'','id'=>'brand_'.$row_id,'class'=>'form-control brand_input required','row_id'=>$row_id);
			echo form_input($data);?>
		</td>
		<td>
			<div class="page-product-csv">
				<div class="fleft">
					<input type="file" class="csv_input" id="csvfile_<?php echo $row_id;?>" />				
				</div>
				<div class="fleft clear">
					<button type="button" class="btn btn-primary uploadcsv" row_id=<?php echo $row_id;?>>Import Product CSV</button>
				</div>
				<div class="csvproducts" id="csvproducts_<?php echo $row_id;?>"></div>			
			</div>
			<?php
			$data = array('type'=>'hidden','name'=>'brand_product_ids[]','class'=>'form-control brand_input','id'=>'brand_product_ids_'.$row_id,'value'=>'');
			echo form_input($data);
			?>
		</td>
		<td>
			<?php
			$data = array('name'=>'brand_promo_code[]', 'value'=>'','id'=>'brand_promo_code_'.$row_id,'row_id'=>$row_id,'class'=>' form-control brand_input required');
			echo form_input($data);?>
		</td>
		<td>
			<div class="img-box">
				<?php
				$brand_image = base_url('uploads/No_Image_Available.png');
				$title = "No Image Available";
				?>
				<div class="cell-img" id="brand_image_cell_<?php echo $row_id;?>"><img id="picker_brand_product_image_<?php echo $row_id;?>" class="brand_product_img_picker" src="<?php echo $brand_image; ?>"  data-toggle='tooltip', data-placement='right', title='' , row_id='<?php echo $row_id;?>' /></div>
				<?php
				$data = array('name'=>'brand_image[]', 'id'=>'brand_product_image_'.$row_id, 'value'=>'','class'=>'',  'data-toggle'=>'tooltip', 'data-placement'=>'right','row_id'=>$row_id, 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_brand_image(this)');
				echo form_upload($data);
				// set image value in hidden type
				$data = array('name'=>'brand_image_val[]', 'value'=>'','id'=>'hidden_brand_product_image_'.$row_id,'type'=>'hidden');
				echo form_input($data);
				?>
			</div>
		</td>
		<td>	
			<?php
			// set day array
			$days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
			$day_btn_txt = '';
			$day_title_txt = '';
			foreach($days as $key=>$val) {
				// set button text
				$day_btn_txt .= ucfirst(substr($val,0,1));
				$day_btn_txt .= (count($days)>($key+1)) ? ' | ' : '';
				// set btn title text
				$day_title_txt .= ucfirst(substr($val,0,3));
				$day_title_txt .= (count($days)>($key+1)) ? ' | ' : '';
			}
			echo '<button id="branddaysavailability_btn_'.$row_id.'" class="btn default daysavailability" campaign_type="3" row_id = "'.$row_id.'" type="button" title="'.$day_title_txt.'">'.$day_btn_txt.'</button>';
			?>
			<!-- start the bootstrap modal where the image will appear -->
			<div class="modal fade dn" id="branddaysavailabilitymodal_<?php echo $row_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title modal_title_<?php echo $row_id;?>" id="myModalLabel_<?php echo $row_id;?>">&nbsp;</h4>
						</div>
						<div class="modal-body">
							<table class="brand-availablility-table">
								<thead class="bg_ccc">
									<tr>
										<th>Availability</th>
										<th>Day</th>
										<th>Entry Point(s)</th>
									</tr>
								</thead>
								<tbody>	
									<?php
									$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
									foreach($week_days as $day) {
										$checked = 'checked';
										$short_day_name = ucfirst(substr($day,0,3));?>
										<tr>
											<td>
												<input type="checkbox" name='<?php echo $short_day_name;?>[]' id='<?php echo $day.'_'.$row_id.'_2';?>' value="<?php echo $day;?>" class='days_avail_check brandday_checkbox_cls_<?php echo $row_id;?>'  <?php echo $checked;?> row_id = '<?php echo $row_id;?>' short_day_name = '<?php echo $short_day_name;?>' campaign_type="2">
												<label for="<?php echo $day.'_'.$row_id.'_2';?>"></label>
												<input type="hidden" name='<?php echo $day;?>[]' value="1" id='brandday_checked_<?php echo $short_day_name.'_'.$row_id;?>'>
											</td>
											<td><?php echo ucfirst($day);?></td>
											<td>
												<select class="entry-select" name = '<?php echo $day.'_entry_point';?>[]' id="entry_point_<?php echo $day.'_'.$row_id;?>" >
													<?php 
													for($point=1;$point<=100;$point++) {
														echo '<option value="'.$point.'">'.$point.'</option>';
													} ?>
												</select>
											</td>
										</tr>
										<?php									
									 }?>
								</tbody>		 
							 </table>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default save_entries" row_id = '<?php echo $row_id;?>' data-dismiss="modal">Ok</button>
						</div>
					</div>
				</div>
			</div>
			<!-- end the bootstrap modal where the image will appear -->
		</td>
		<td>
			<span class="romove_brand_row new-prod-row-icon" row_id=<?php echo $row_id;?> brand_id='' title="<?php echo lang('brand_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
		</td>
	</tr>
	
