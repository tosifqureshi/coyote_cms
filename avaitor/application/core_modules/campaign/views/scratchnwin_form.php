<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<link href="<?php echo base_url('avaitor/themes/admin/css/jquery.tagit.css');  ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('avaitor/themes/admin/css/tagit.ui-zendesk.css'); ?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('avaitor/themes/admin/js/tag-it.js'); ?>" type="text/javascript" charset="utf-8"></script>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $scratchnwin_title;?></h4>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs customtab2" role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#content_tab" role="tab"> <span class=""><?php echo lang('content');?></span></a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attributes_tab" role="tab"> <span class=""><?php echo lang('attributes');?></span></a> </li>
				</ul>
				<!-- Tab panes -->
				<?php 
				$snw_id = isset($snw_id) ? $snw_id : '';
				echo form_open_multipart('admin/settings/campaign/create/'.$snw_id,'id="add_scratchnwin_form" class="add_form form-material form-with-label"'); ?>
					<div class="tab-content">
						<!-- Content area start here -->
						<div class="tab-pane active" id="content_tab" role="tabpanel">
							<div class="p-t-20">
								<div class="row">
									<div class="col-sm-12 col-xl-6">
										<!-- Title input start here -->
										<div class="form-group">
											<label for="title"><?php echo lang('title');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'title','id'=>'title', 'value'=> isset($title) ? $title : '' , 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('beacon_code'));
											echo form_input($data);
											?>
										</div>
										<!-- Title input end here -->
																								
										<!-- Start / end date fields start here -->
										<div class="row form-group">
											<div class="col-md-6">
												<label for="co_start_date"><?php echo lang('start_date');?><i style="color:red;">*</i></label>
												<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date" title="<?php echo lang('tooltip_offer_start_time');?>" readonly="readonly">
											</div>
												
											<div class="col-md-6">
												<label for="co_end_date"><?php echo lang('end_date');?><i style="color:red;">*</i></label>
												<input type="text" name="end_date" class="form-control required" value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date" title="<?php echo lang('tooltip_offer_end_time');?>" readonly="readonly">
											</div>
										</div>
										<!-- Start / end date fields end here -->
									
										<!-- Campaign type radio options start here -->
										<?php
										$readonlyC = '';
										$nameC = '';
										if(!empty($snw_id)){
											$readonlyC = ' readonly="readonly"  disabled="disabled"';
											$nameC = 'name="campaign_type_disable"';
											$data = '<input name="campaign_type" value="'.$campaign_type.'" id="campaign_type" type="hidden">';
											echo $data; 
										}else{
										$nameC = 'name="campaign_type"';
										} ?>

										<div class=" form-group">
											<label><?php echo lang('campaign_type');?></label>
											<div class="demo-radio-button">
												<input <?php echo $nameC; ?> <?php echo $readonlyC; ?> type="radio" value="1"  <?php echo (isset($campaign_type) && ($campaign_type == 2 || $campaign_type == 3)) ? '' : 'checked="checked"';?> class="with-gap radio-col-light-blue notifytimeAction" id="campaign_type_1">
												<label for="campaign_type_1"><?php echo lang('campaign_type_1');?></label> 
												
												<input type="radio" <?php echo $nameC; ?> <?php echo $readonlyC; ?> value="3" <?php echo (isset($campaign_type) && $campaign_type ==3) ? 'checked' : '';?> class="with-gap radio-col-light-blue notifyintervalAction" id="campaign_type_3">
												<label for="campaign_type_3"><?php echo lang('campaign_type_3');?></label>
											 </div>
										</div>
										<!-- Campaign type radio options end here -->  
									
										<!-- Notification time input start here -->
										<?php 
										$cte_dn = '';
										$snw_dn = '';
										$hide_cte = 'dn';
										$hide_snw = '';
										if(isset($campaign_type) && ($campaign_type == 1)){						
											$snw_dn = 'dn';
										} else if((isset($campaign_type) && $campaign_type ==3)){                              
											$cte_dn = 'dn';
											$hide_snw = 'dn';
											$hide_cte = '';
										}
										?>
										<div class="row form-group">
											<div class="col-md-6 notificationtimediv">
												<label class="">
													<?php echo lang('notification_time');?><i style="color:red;">*</i>
												</label>
												<div class="form-group input-append date" id="hour_min_time">
													<?php
													$data	= array('name'=>'notification_time', 'data-format'=>'hh:mm', 'value'=>isset($notification_time) ? $notification_time : '','id'=>'notification_time','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_time'), 'readonly'=>'readonly', 'class'=>'required form-control');
													echo form_input($data);
													?>
													<span class="add-on" id="notification_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-time"> </i> </span>
												</div>
											</div>
										
											<!-- Prize Frequency input start here -->
											<div class="col-md-6 prize-frequency-box <?php echo $cte_dn;?>">
												<label for="prize_frequency"><?php echo lang('prize_frequency');?><i style="color:red;">*</i></label>
												<?php
												$data = array('name'=>'prize_frequency','id'=>'prize_frequency', 'value'=> isset($prize_frequency) ? $prize_frequency : 7 , 'class'=>'form-control required digits','min'=>1);
												echo form_input($data);?>
											</div>
											<!-- Prize Frequency input end here -->  
										</div>
										<!-- Notification time input end here -->
									
										<!-- Second Chance Draw radio options start here -->
										<div class="form-group second-chance-box <?php echo $cte_dn;?>">
											<label class=""><?php echo lang('number_of_second_chance');?></label>
											<div class="demo-checkbox">
												<input type="checkbox" name="second_chance_draw" value="1" id="second_chance_draw" <?php echo (isset($second_chance_draw) && !empty($second_chance_draw)) ? 'checked="checked"' : '';?>  class="filled-in chk-col-light-blue">
												<label for="second_chance_draw">
													<?php
													$display_none = (isset($second_chance_draw) && !empty($second_chance_draw)) ? '' : 'display:none;';
													$data = array('name'=>'second_chance_count','id'=>'second_chance_count', 'value'=> isset($second_chance_count) ? $second_chance_count : 0 , 'class'=>'form-control form-control-line required digits','style'=>$display_none);
													echo form_input($data);?>
												</label>
											 </div>
										</div>
										<!-- Second Chance Draw radio options end here -->
									
										<!-- Campaign beacon promotion radio options start here -->
										<div class="form-group ">
											<div class="demo-checkbox">
												<input type="checkbox" name="beacon_promotion" value="1" id="beacon_promotion" <?php echo (isset($beacon_promotion) && !empty($beacon_promotion)) ? 'checked="checked"' : '';?>  class="filled-in chk-col-light-blue">
												<label for="beacon_promotion"><?php echo lang('beacon_promotion');?></label>
											</div>
										</div>
										<!-- Campaign beacon promotion radio options end here -->
									  
									</div>	
									<!-- Images right side block start here -->
									<div class="col-sm-12 col-xl-6">
										 <div class="form-group ">
											 <label><?php echo lang('banner_image');?></label>
											 <div class="row">
												<div class="col-md-4 " >
													  <?php
														// set banner image action
														$banner_image_val = (isset($banner_image) && !empty($banner_image)) ? $banner_image :'';
														$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
														if(!empty($banner_image_val)) {
														$banner_image_src = base_url('uploads/scratchnwin_images/'.$banner_image);
														$title = lang('tooltip_offer_image');
														
														$banner_image_val_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$banner_image; 
														 if(!file_exists($banner_image_val_doc)){
															 // $promo_image_1 = base_url('uploads/No_Image_Available.png');
															$banner_image_src = base_url('uploads/No_Image_Available.png');
															$title = "No Image Available";
														 }
														} else {
															$title = lang('tooltip_image');
															$banner_image_src = base_url('uploads/upload.png');
														} ?>
													<div class="cell-img" id="banner_image_cell">
														<img id="snw_banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
														<?php
														$data	= array('name'=>'banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)','class'=>'input-file-now');
														echo form_upload($data);
														// set banner image value in hidden type
														$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
														echo form_input($data);?>
													</div>
													
												</div>
											 </div>
										</div>
									</div> 
									<!-- Images right side block end here -->
								</div>
								<!-- Beacon Offer details div start here -->
								<?php 
								$beacon_dn = (isset($beacon_promotion) && !empty($beacon_promotion)) ? '' : 'dn';
								// set beacon offer value in hidden type
								$data = array('name'=>'beacon_offer_id', 'value'=>(isset($beacon_offer_id) && !empty($beacon_offer_id)) ? $beacon_offer_id : 0,'id'=>'beacon_offer_id','type'=>'hidden');
								echo form_input($data);?>
								<div id="beacon_promotion_div" class="row <?php echo $beacon_dn;?>">
									<!-- Beacon Offer details div start here -->
									<div class="col-md-6">
										<!-- Beacon offer stores dropdown start here -->
										<div class="form-group row">
											<label class="control-label col-md-12">
												<?php echo lang('offer_store');?> <span class="red">*</span>
											</label>
											<div class="demo-checkbox col-md-12">
												<input type="checkbox" id="select_all" class="filled-in chk-col-light-blue" />
												<label for="select_all"><?php echo lang('select_all');?></label>
											</div>
											<div class="col-md-6">
												<?php
												$store_id_array = (!empty($store_id)) ? explode(',',$store_id) : '';
												$data	= array('name'=>'store_id[]','multiple'=>'multiple','id'=>'beacon_store_id', 'class'=>'required form-control', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_store'));
												echo form_dropdown($data,$stores,set_value('store_id',$store_id_array));?>
											</div>	 
										</div>  
										<!-- Beacon offer stores dropdown end here -->
										
										<!-- Beacon offer title start here -->
										<div class="form-group  ">
											<label for="title"><?php echo lang('beacon_offer_name');?><span class="red">*</span></label>
											<?php
											$data	= array('name'=>'offer_name', 'value'=>set_value('offer_name',isset($offer_name) ? $offer_name : ''), 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_name'));
											echo form_input($data);
											?>
										</div>
										<!-- Beacon offer title end here -->
										
										<!-- Beacon offer short description start here -->
										<?php
										$data = array('name'=>'offer_short_description', 'value'=>'','type'=>'hidden');
										echo form_input($data);
										?>
										<!-- Beacon offer short description end here -->
										
										<!-- Beacon offer long description start here -->
										<div class="form-group html-editor">
											<label><?php echo lang('offer_long_description');?></label>
											<?php
											$data = array('name'=>'offer_long_description', 'class'=>'mceEditor redactor span8', 'value'=>isset($offer_long_description) ? $offer_long_description : '');
											echo form_textarea($data);?>
										</div>
										<!-- Beacon offer long description start here -->
									</div>	
									<!-- Beacon Offer details div end here -->
									
									<!-- Beacon Offer Image right side block start here -->	
									<div class="col-md-6">
										<div class="form-group">
											<label><?php echo lang('beacon_offer_image');?></label> 
											<div class="row">
												<div class="col-md-6 col-sm-6 col-lg-4" >
													<div class="img-box">
														<?php
														// set banner image action
														$offer_image_val = (isset($offer_image) && !empty($offer_image)) ? $offer_image :'';
														$img_1_display = (isset($offer_image_val) && !empty($offer_image_val)) ? '' :'dn';
														if(!empty($offer_image_val)) {
															$offer_image_src = base_url('uploads/scratchnwin_images/'.$offer_image);
															$title = lang('beacon_tooltip_image');
															 $offer_image_src_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$offer_image; 
															 if(!file_exists($offer_image_src_doc)){
																$offer_image_src = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															 }
														} else {
															$title = lang('beacon_tooltip_image');
															$offer_image_src = base_url('uploads/upload.png');
														} ?>
														<div class="cell-img" id="offer_image_cell">
															<img id="snw_offer_image_picker" src="<?php echo $offer_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
														</div>
														<?php
														$data	= array('name'=>'offer_image', 'id'=>'offer_image', 'value'=>$offer_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
														echo form_upload($data);
														// set banner image value in hidden type
														$data = array('name'=>'offer_image_val', 'value'=>$offer_image_val,'id'=>'offer_image_val','type'=>'hidden');
														echo form_input($data);
														$data = array('name'=>'existing_offer_image', 'value'=>$offer_image_val,'type'=>'hidden');
														echo form_input($data);?>        
												   </div>
												</div>
											</div>	 
											<div class="alert red-alert alert-danger alert-dismissible ">
												   <i class="icon fa fa-warning"></i>
													<?php echo ('Upload up to 3MB images.');?>
											</div>
										</div>
									</div> 
									<!-- Beacon Offer Image right side block end here -->	 
								</div>	
								<!-- Beacon Offer details div end here -->
							</div>	 
						</div>
						<!-- Content area end here -->
						
						<!-- Attribute area start here -->
						<div class="tab-pane  p-20" id="attributes_tab" role="tabpanel">
							<!-- Add product button -->
							<button onclick="add_row()" class="btn waves-effect waves-light btn-info add-product-btn float-right"  type="button">Add Product</button>	
							
							<div class="table-responsive">
								<table class="table table-bordered th-border" id="product_tables">
									<thead>
										<tr class="product_tr <?php echo $hide_snw;?>">
											<th class='snw_product_table_th'><?php echo lang('product_number');?></th>
											<th class='snw_product_table_th'><?php echo lang('product_name');?></th>
											<th class='snw_product_table_th'><?php echo lang('product_promo_code');?></th>
											<th class='snw_product_table_th'><?php echo lang('prizes_per_day');?></th>
											<th class='snw_product_table_th'><?php echo lang('total_prizes');?></th>                  <th class='snw_product_table_th'><?php echo lang('total_number_claimed');?></th>			<th class='snw_product_table_th'><?php echo lang('product_image');?></th>
											<th class='snw_product_table_th width155'><?php echo lang('product_avail_days');?></th>
											<th><?php echo lang('product_actions');?></th>
										</tr>
								   
										<tr class="brand_tr <?php echo $hide_cte;?>">
											<th class='snw_product_table_th'><?php echo lang('brand_name');?></th>
											<th class='snw_product_table_th'><?php echo lang('upload_csv');?></th>
											<th class='snw_product_table_th'><?php echo lang('product_promo_code');?></th>			
											<th class='snw_product_table_th'><?php echo lang('brand_image');?></th>
											<th class='snw_product_table_th width155'><?php echo lang('product_avail_days');?></th>
											<th><?php echo lang('product_actions');?></th>
										</tr>
									 </thead>
									<tbody class="product_table_tbody">
										<?php
										$qty_row = 1;               
										if(isset($snw_products) && !empty($snw_products) && count($snw_products) > 0) {
											foreach($snw_products as $snw_product) { ?>
												<tr class="snw-product-half-side product_tr" id="qty_row_<?php echo $qty_row;?>">
													<td>
														<?php
														$data = array('name'=>'product_ids[]', 'value'=>$snw_product['product_id'],'id'=>'product_id_'.$qty_row,'class'=>'form-control product_input number_input digits required','row_id'=>$qty_row,'exist_id'=>$snw_product['product_id']);
														echo form_input($data);
														$data = array('type'=>'hidden','name'=>'snw_product_ids[]','value'=>$snw_product['id']);
														echo form_input($data);
														?>
													</td>
													<td>
														<?php
														$data = array('name'=>'product_name[]', 'value'=>$snw_product['product_name'],'readonly'=>true,'id'=>'product_name_'.$qty_row,'class'=>'form-control','row_id'=>$qty_row);
														echo form_input($data);?>
													</td>
													<td>
														<?php
														$data = array('name'=>'promo_code[]', 'value'=>$snw_product['promo_code'],'id'=>'promo_code_'.$qty_row,'row_id'=>$qty_row,'class'=>'form-control promo_code required');
														echo form_input($data);?>
													</td>
													<td>
														<?php
														$data = array('name'=>'prizes_per_day[]','value'=>$snw_product['prizes_per_day'],'id'=>'prizes_per_day_'.$qty_row,'class'=>'form-control prizes_count digits  required','min'=>"1",'row_id'=>$qty_row,'is_per_day_count'=>1);
														echo form_input($data);?>
													</td>
													<td>
														<?php
														$data = array('name'=>'total_prizes[]', 'value'=>$snw_product['total_prizes'],'id'=>'total_prizes_'.$qty_row,'class'=>'form-control prizes_count digits cte_number_input required','min'=>"1",'row_id'=>$qty_row);
														echo form_input($data);?>
													</td>
													<?php if($campaign_type == 1) { ?>                                      
														<td>
															<?php
															// get product claimed total count
															$total_number_claimed = get_prise_claimed_count($snw_id,$snw_product['product_id']);
															$data = array('name'=>'total_number_claimed[]', 'value'=>$total_number_claimed,'readonly'=>true,'id'=>'total_number_claimed_'.$qty_row,'class'=>'form-control','row_id'=>$qty_row);
															echo form_input($data);?>
														</td>
													<?php } ?>	
													<td>
														<div class="img-box">
														<?php
														$product_image_val = (!empty($snw_product['product_image'])) ? $snw_product['product_image'] :'';
														$img_display = (!empty($product_image_val)) ? '' :'dn';
														$title = lang('tooltip_promo_image');
														$product_image = base_url('uploads/upload.png');
														if(isset($product_image_val) && $product_image_val != '') {
															$product_image = base_url('uploads/scratchnwin_images/'.$product_image_val);
															$title = lang('tooltip_offer_image');
															$product_image_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$product_image_val; 
															 if(!file_exists($product_image_doc)){
																$product_image = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															 }
														} ?>
														<div class="cell-img" id="product_image_cell_<?php echo $qty_row;?>"><img id="picker_product_image_<?php echo $qty_row;?>" class="product_img_picker" src="<?php echo $product_image; ?>"  data-toggle='tooltip', data-placement='right', title='' , row_id='<?php echo $qty_row;?>' /></div>
														<?php
														$data = array('name'=>'product_image[]', 'id'=>'product_image_'.$qty_row, 'value'=>$product_image_val, 'class'=>'', 'data-toggle'=>'tooltip', 'data-placement'=>'right','row_id'=>$qty_row, 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_product_image(this)');
														echo form_upload($data);
														// set image value in hidden type
														$data = array('name'=>'product_image_val[]', 'value'=>$product_image_val,'id'=>'hidden_product_image_'.$qty_row,'type'=>'hidden');
														echo form_input($data);
														// set existing image value in hidden type
														$data = array('name'=>'existing_product_image[]', 'value'=>$product_image_val,'id'=>'existing_product_image_'.$qty_row,'type'=>'hidden');
														echo form_input($data);?>
														</div>
													</td>
														
													<td>	
														<?php
														// set day array
														$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
														$days = (isset($snw_product['days_availability']) && !empty($snw_product['days_availability'])) ? explode(',',$snw_product['days_availability']) : $week_days;
														$days = array_filter($days);
														$day_btn_txt = '';
														$day_title_txt = '';
														foreach($days as $key=>$val) {
															// set button text
															$day_btn_txt .= ucfirst(substr($val,0,1));
															$day_btn_txt .= (count($days)>($key)) ? ' | ' : '';
															// set btn title text
															$day_title_txt .= ucfirst(substr($val,0,3));
															$day_title_txt .= (count($days)>($key)) ? ' | ' : '';
														}
														echo '<button id="daysavailability_btn_'.$qty_row.'" class="btn default daysavailability" campaign_type="1" snw_product_id = "'.$snw_product['id'].'"  row_id = "'.$qty_row.'" type="button" title="'.$day_title_txt.'">'.$day_btn_txt.'</button>';?>
														<!-- start the bootstrap modal where the image will appear -->
														<div class="modal fade" id="daysavailabilitymodal_<?php echo $qty_row;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
																		<h4 class="modal-title modal_title_<?php echo $qty_row;?>" id="myModalLabel">&nbsp;</h4>
																	</div>
																	<div class="modal-body modal-days-avail">
																		<?php
																		$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
																		foreach($week_days as $day) {
																			$checked = (in_array($day,$days)) ? 'checked' : '';
																			$is_day_checked = (in_array($day,$days)) ? 1 : 0;
																			$short_day_name = ucfirst(substr($day,0,3));?>
																			<input type="checkbox" name='<?php echo $short_day_name;?>[]' id='<?php echo $day.'_'.$qty_row.'_1';?>' value="<?php echo $day;?>" class='days_avail_check day_checkbox_cls_<?php echo $qty_row;?>'  <?php echo $checked;?> row_id = '<?php echo $qty_row;?>' short_day_name = '<?php echo $short_day_name;?>' campaign_type="1">
																			<label for="<?php echo $day.'_'.$qty_row.'_1';?>">
																			<?php
																			echo ucfirst($day);	?></label>
																			<input type="hidden" name='<?php echo $day;?>[]' value="<?php echo $is_day_checked;?>" id='day_checked_<?php echo $short_day_name.'_'.$qty_row;?>'>
																			<?php				
																		} ?>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
																	</div>
																</div>
															</div>
														</div>
														<!-- end the bootstrap modal where the image will appear -->
													</td>
													<?php 
													$classS = 'style="display:none"';
													$campaign_type = (isset($campaign_type) && !empty($campaign_type))?$campaign_type:'';
													if($campaign_type == '3'){
														$classS = '';
													}?>
													<td>
														<?php
														$btn_class = ($snw_product['status'] == '0') ? 'deactive-btn' : 'active-btn';
														$icon_class = ($snw_product['status'] == '0') ? 'fa-toggle-off' : 'fa-toggle-on';?>
														<span class="product_status_td_<?php echo $qty_row;?>">
															<a class="scratch-prod-icon <?php echo $btn_class;?>" id="product_status" href="javascript:;" snw_product_id = '<?php echo $snw_product['id'];?>'  row_id = '<?php echo $qty_row;?>' title="<?php echo lang('product_status_btn_note_'.$snw_product['status']);?>"><i class="fa <?php echo $icon_class;?>"></i></a>
														</span>
														<?php //if($qty_row != 1) { ?>
															<span class="romove_row scratch-prod-icon"  row_id=<?php echo $qty_row;?> snw_product_id='<?php echo $snw_product['id'];?>' title="<?php echo lang('product_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
														<?php //} ?>
													</td>											
												</tr>
												<?php
												$qty_row++;		
											}
										} 
										
										if(isset($cte_brands) && !empty($cte_brands) && count($cte_brands) > 0) {
											foreach($cte_brands as $cte_brand) { ?>
												<tr class="snw-product-half-side brand_tr"  id="brand_row_<?php echo $qty_row;?>">
													<td>
														<?php
														$data = array('name'=>'brand[]', 'value'=>$cte_brand['brand'],'class'=>'brand_input required','id'=>'brand_'.$qty_row,'row_id'=>$qty_row,'class'=>'form-control required');
														echo form_input($data);
														$data = array('type'=>'hidden','name'=>'cte_brand_ids[]','value'=>$cte_brand['id']);
														echo form_input($data);
														?>	
													</td>
													<td>
														<div class="page-product-csv">
															<div class="fleft clear">
																<input type="file" class="csv_input" id="csvfile_<?php echo $qty_row;?>" />  
															</div>
															<div class="fleft clear">
																<button type="button" class="btn btn-primary uploadcsv" row_id=<?php echo $qty_row;?>>Import Product CSV</button>
															</div>
															<div class="csvproducts" id="csvproducts_<?php echo $qty_row;?>">
															<?php 
															// show brand products
															$products = array_filter(explode(',',$cte_brand['products']));
															foreach($products as $product) { ?>
																<span class="chosen-choices fl ml5">&nbsp;
																	<span>
																		<?php echo $product;?>
																	</span>&nbsp;&nbsp;
																	<span class="search-choice-close crp" row_id="<?php echo $qty_row;?>" prod_id="<?php echo $product;?>" >
																		<strong> X </strong>
																	</span>&nbsp;
																</span>
															<?php } ?>
															</div>			
														</div>
														<?php
														$data = array('type'=>'hidden','name'=>'brand_product_ids[]','class'=>'brand_input','id'=>'brand_product_ids_'.$qty_row,'value'=>$cte_brand['products']);
														echo form_input($data);
														?>
													</td>
													<td>
														<?php
														$data = array('name'=>'brand_promo_code[]', 'value'=>$cte_brand['promo_code'],'id'=>'brand_promo_code_'.$qty_row,'row_id'=>$qty_row,'class'=>'brand_input required');
														echo form_input($data);?>
													</td>	
													<td>
														<div class="img-box">
															<?php
															$brand_image_val = (!empty($cte_brand['brand_image'])) ? $cte_brand['brand_image'] :'';
															$img_display = (!empty($brand_image_val)) ? '' :'dn';
															$title = lang('tooltip_promo_image');
															$product_image = base_url('uploads/upload.png');
															if(isset($brand_image_val) && $brand_image_val != '') {
																$brand_image = base_url('uploads/scratchnwin_images/'.$brand_image_val);
																$title = lang('tooltip_offer_image');
																$brand_image_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$brand_image_val; 
																 if(!file_exists($brand_image_doc)){
																	$brand_image = base_url('uploads/No_Image_Available.png');
																	$title = "No Image Available";
																 }
															} ?>
															<div class="cell-img" id="brand_image_cell_<?php echo $qty_row;?>"><img id="picker_brand_product_image_<?php echo $qty_row;?>" class="brand_product_img_picker" src="<?php echo $brand_image; ?>"  data-toggle='tooltip', data-placement='right', title='' , row_id='<?php echo $qty_row;?>' /></div>
															<?php
															$data = array('name'=>'brand_image[]', 'id'=>'brand_product_image_'.$qty_row, 'value'=>$brand_image_val, 'class'=>'', 'data-toggle'=>'tooltip', 'data-placement'=>'right','row_id'=>$qty_row, 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_brand_image(this)');
															echo form_upload($data);
															// set image value in hidden type
															$data = array('name'=>'brand_image_val[]', 'value'=>$brand_image_val,'id'=>'hidden_brand_product_image_'.$qty_row,'type'=>'hidden');
															echo form_input($data);
															// set existing image value in hidden type
															$data = array('name'=>'existing_product_image[]', 'value'=>$brand_image_val,'id'=>'existing_product_image_'.$qty_row,'type'=>'hidden');
															echo form_input($data);
															?>
														</div>	
													</td>
													
													<td>	
														<?php
														// set day array
														$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
														$days = (isset($cte_brand['days_availability']) && !empty($cte_brand['days_availability'])) ? explode(',',$cte_brand['days_availability']) : $week_days;
														$days = array_filter($days);
														$day_btn_txt = '';
														$day_title_txt = '';
														foreach($days as $key=>$val) {
															// set button text
															$day_btn_txt .= ucfirst(substr($val,0,1));
															$day_btn_txt .= (count($days)>($key)) ? ' | ' : '';
															// set btn title text
															$day_title_txt .= ucfirst(substr($val,0,3));
															$day_title_txt .= (count($days)>($key)) ? ' | ' : '';
														}
														echo '<button id="branddaysavailability_btn_'.$qty_row.'" class="btn default daysavailability" campaign_type="3" snw_product_id = "'.$cte_brand['id'].'"  row_id = "'.$qty_row.'" type="button" title="'.$day_title_txt.'">'.$day_btn_txt.'</button>';?>
														<!-- start the bootstrap modal where the image will appear -->
														<div class="modal fade dn" id="branddaysavailabilitymodal_<?php echo $qty_row;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
																		<h4 class="modal-title modal_title_<?php echo $row_id;?>" id="myModalLabel_<?php echo $row_id;?>">&nbsp;</h4>
																	</div>
																	<div class="modal-body">
																		<table class="brand-availablility-table">
																			<thead class="bg_ccc">
																				<tr>
																					<th>Availability</th>
																					<th>Day</th>
																					<th>Entry Point(s)</th>
																				</tr>
																			</thead>
																			<tbody>	
																				<?php
																				$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
																				$brand_entries = get_brand_entries($cte_brand['id']);
																				foreach($week_days as $day) {
																					$checked = (in_array($day,$days)) ? 'checked' : '';
																					$is_day_checked = (in_array($day,$days)) ? 1 : 0;
																					$short_day_name = ucfirst(substr($day,0,3));
																					$day_entry_points = !empty($brand_entries[$day]) ? $brand_entries[$day] : 0;?>
																					<tr>
																						<td>
																							<input type="checkbox" name='<?php echo $short_day_name;?>[]' id='<?php echo $day.'_'.$qty_row.'_2';?>' value="<?php echo $day;?>" class='days_avail_check brandday_checkbox_cls_<?php echo $qty_row;?>'  <?php echo $checked;?> row_id = '<?php echo $qty_row;?>' short_day_name = '<?php echo $short_day_name;?>' campaign_type="2">
																							<label for="<?php echo $day.'_'.$qty_row.'_2';?>"></label>
																							<input type="hidden" name='<?php echo $day;?>[]' value="<?php echo $is_day_checked;?>" id='brandday_checked_<?php echo $short_day_name.'_'.$qty_row;?>'>
																						</td>
																						<td><?php echo ucfirst($day);?></td>
																						<td>
																							<select class="entry-select" name = '<?php echo $day.'_entry_point';?>[]' id="entry_point_<?php echo $day.'_'.$qty_row;?>" >
																								<?php 
																								for($point=1;$point<=100;$point++) {
																									$selected = ($day_entry_points == $point) ? 'selected' : '';
																									echo '<option value="'.$point.'" '.$selected.'>'.$point.'</option>';
																								} ?>								
																							</select>
																							</td>
																					</tr>
																					<?php									
																				 }?>
																			</tbody>		 
																		 </table>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-default save_entries" row_id = '<?php echo $row_id;?>' data-dismiss="modal">Ok</button>
																	</div>
																</div>
															</div>
														</div>
														<!-- end the bootstrap modal where the image will appear -->
													</td>
													
													<td>
														<?php
														$btn_class = ($cte_brand['status'] == '0') ? 'deactive-btn' : 'active-btn';
														$icon_class = ($cte_brand['status'] == '0') ? 'fa-toggle-off' : 'fa-toggle-on';?>
														<span class="brand_status_td_<?php echo $qty_row;?>">
															<a class="scratch-prod-icon <?php echo $btn_class;?>" id="brand_status" href="javascript:;" brand_id = '<?php echo $cte_brand['id'];?>'  row_id = '<?php echo $qty_row;?>' title="<?php echo lang('brand_status_btn_note_'.$cte_brand['status']);?>"><i class="fa <?php echo $icon_class;?>"></i></a>
														</span>
														
														<span class="romove_brand_row scratch-prod-icon"  row_id=<?php echo $qty_row;?> brand_id='<?php echo $cte_brand['id'];?>' title="<?php echo lang('brand_delete_btn_note');?>"><i class="fa fa-trash"></i></span>	
													</td>				
												</tr>
												<?php
												$qty_row++;									
											}
										}
										
										if($qty_row == 1) {
											// render blank product entities 
											echo $product_form;
											$qty_row++;
										}
										?>
									</tbody>	
								</table>
								<input type="hidden" value="<?php echo $qty_row;?>" name='last_qty_row' id='last_qty_row'>
								<!-- Product fields end here -->
							</div>	 
						</div>	
						<!-- Attribute area end here -->
						<div class="row pb-3 float-right">
							<?php
							// set current date in hidden field 
							$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
							echo form_input($data);?>
							<div class="mx-1">
								<button type="submit" name="save" class="CampSaveEve btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/campaign' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					</div>	
				<?php echo form_close();?>
			</div>
		</div>
	</div> 
</div> 

<script type="text/javascript">
	// MAterial Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
    $(document).ready(function() {
        $("#add_scratchnwin_form").validate({
        });
    });
    
    /* 
    | -----------------------------------------------------
    | Handle form posting validation
    | -----------------------------------------------------
    */
    $( "#add_scratchnwin_form" ).submit(function( event ) {
        
        // set start date for calculate day diffrence
        var start_date	=  $( "#start_date" ).val().split(' ');
        var startdateSplit = start_date[0].split('-');
        var new_start_date = startdateSplit[1] + '/' + startdateSplit[0] + '/' + startdateSplit[2];
        // set end date for calculate day diffrence
        var end_date	 =  $( "#end_date" ).val().split(' ');
        var enddateSplit = end_date[0].split('-');
        var new_end_date = enddateSplit[1] + '/' + enddateSplit[0] + '/' + enddateSplit[2];
        // show error if start date less then end date
        if(new_start_date >= new_end_date ) {
            bootbox.alert("End date must be greater than start date.");
            return false;
        }            
        // show error if banner image not exist
        var banner_image_val = $('#banner_image_val').val();
        if(banner_image_val == '') {
            $('#banner_image_cell').addClass('error');
            bootbox.alert("Please upload banner image.");
            return false;
        }
        // show error if beacon offer image not exist
        var offer_image_val = $('#offer_image_val').val();
        if($("#beacon_promotion").is(':checked') && offer_image_val == ''){
            $('#offer_image_cell').addClass('error');
            return false;
        }
        
        var is_validate = true;
		var campaign_type = '<?php echo (isset($campaign_type)) ? $campaign_type : '';?>';
        if(campaign_type == '' || campaign_type == undefined ) {
			campaign_type = $('input[name=campaign_type]:checked', '#add_scratchnwin_form').val();
		}
		if(campaign_type ==1) {
			$( ".product_input" ).each(function( e ) {
				var row_id = $(this).attr('row_id');
				var product_id = $('#product_id_'+row_id).val();
				var promo_code = $('#promo_code_'+row_id).val();
				var product_image = $('#hidden_product_image_'+row_id).val();
				
				var prizes_per_day = $('#prizes_per_day_'+row_id).val();
				var total_prizes_per_week = $('#total_prizes_'+row_id).val();
				
				if(product_id == '') {
					$('#product_id_'+row_id).addClass('error');
					is_validate =false;
				}
				if(promo_code == '') {
					$('#promo_code_'+row_id).addClass('error');
					is_validate =false;
				}
				if(product_image == '') {
					$('#product_image_cell_'+row_id).addClass('error');
					is_validate =false;
				}
				if(prizes_per_day == '') {
					$('#prizes_per_day_'+row_id).addClass('error');
					is_validate =false;
				}
				if(total_prizes_per_week == '') {
					$('#total_prizes_'+row_id).addClass('error');
					is_validate =false;
				}
			});
		} else {
			$( ".brand_input" ).each(function( e ) {
				var row_id = $(this).attr('row_id');
				var brand = $('#brand_'+row_id).val();
				var product_id = $('#brand_product_ids_'+row_id).val();
				var promo_code = $('#brand_promo_code_'+row_id).val();
				var product_image = $('#hidden_brand_product_image_'+row_id).val();
				
				if(brand == '') {
					$('#brand_'+row_id).addClass('error');
					is_validate =false;
				}
				if(product_id == '') {
					$('#csvproducts_'+row_id).addClass('error');
					$('#csvproducts_'+row_id).html('Please upload product csv');
					is_validate = false;
				}
				if(promo_code == '') {
					$('#brand_promo_code_'+row_id).addClass('error');
					is_validate =false;
				}
				if(product_image == '') {
					$('#brand_image_cell_'+row_id).addClass('error');
					is_validate =false;
				}
			});
		}
        if(is_validate == false) {
            bootbox.alert("Please fill all required attributes.");
            return false;
        }
        return;
        event.preventDefault();
    });
    
    /* 
    | -----------------------------------------------------
    | Function used to remove image
    | -----------------------------------------------------
    */
    $(document).delegate('.product_input','change',function(e){
        var product_id = $(this).val();
        var row_id = $(this).attr('row_id');
        var exist_id = $(this).attr('exist_id');
        get_product_data(product_id,row_id,exist_id);
    });
    
    function get_product_data(product_id,row_id,exist_id) {
        $('.CampSaveEve').attr('disabled', true); 
        var product_ids = new Array();
        var currentRowId = '';
        var j =0;
        $.each( $( ".product_input" ), function() {
            currentRowId = $(this).attr('row_id');
            if(row_id != currentRowId){
                product_ids[j] = $(this).val();
				j++;
            }
        });
        
        // check existing product id
        if(exist_id == product_id) {
          
        } else if(exist_id != '' && exist_id == product_id) {
            bootbox.alert("Product already exist.");
            $('#product_id_'+row_id).val(exist_id);
            $('.CampSaveEve').removeAttr('disabled');
            return false;
        } 
        
        if ($.inArray(product_id, product_ids) > -1) {
            bootbox.alert("Product already exist.");
            $('#qty_row_'+row_id).html('');
            $('.CampSaveEve').removeAttr('disabled');
            return false;
        } else {
			$('#loader1').fadeIn();
            $.ajax ({
                type: "POST",
                dataType:'jSon',
                data : {product_id:product_id},
                url: "<?php echo base_url() ?>admin/settings/campaign/getproductdata",
                success: function(data){
					$('#loader1').fadeOut();
                    if(data.product_name != '') {
                        $('.CampSaveEve').removeAttr('disabled');
                        $('#product_name_'+row_id).val(data.product_name);
                        $('#total_number_claimed_'+row_id).val(0);
                        $('#promo_code_'+row_id).removeAttr("readonly");
                        $('#prizes_per_day_'+row_id).removeAttr("readonly");
                        $('#total_prizes_'+row_id).removeAttr("readonly");
                    } else {
                        $('.CampSaveEve').removeAttr('disabled');
                        $('#product_name_'+row_id).val('');
                        $('#product_id_'+row_id).val('');
                        $('#prizes_per_day_'+row_id).val('');
                        $('#total_prizes_'+row_id).val('');
                        $('#promo_code_'+row_id).val('');
                        $('#total_number_claimed_'+row_id).val('');
                        $('#promo_code_'+row_id).attr('readonly', true);
                        $('#prizes_per_day_'+row_id).attr('readonly', true);
                        $('#total_prizes_'+row_id).attr('readonly', true);
                    }
                }
            });
        }
    }
    
    function read_url_product_image(input) {
        
        var pickerId = '#picker_'+input.id;
        var fieldId	= '#'+input.id;
        $('#hidden_'+input.id).val(input.value);
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var size = file.size/1000000; // get image size in mb;
            console.log(size);
            if(size <= 3.0955135) {
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //alert(pickerId);
                        $(pickerId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $(fieldId).val("");
                    bootbox.alert("File not supported!");
                }
            } else {
                $(fieldId).val("");
                bootbox.alert("File size should be less then 3MB!");
            }
        }
    }
    function read_url_brand_image(input) {
        
        var pickerId = '#picker_'+input.id;
        var fieldId	= '#'+input.id;
        $('#hidden_'+input.id).val(input.value);
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var size = file.size/1000000; // get image size in mb;
            console.log(size);
            if(size <= 3.0955135) {
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //alert(pickerId);
                        $(pickerId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $(fieldId).val("");
                    bootbox.alert("File not supported!");
                }
            } else {
                $(fieldId).val("");
                bootbox.alert("File size should be less then 3MB!");
            }
        }
    }
    
    $(document).delegate('.product_img_picker','click',function(e){
        var row_id = $(this).attr('row_id');
        $("input[id='product_image_"+row_id+"']").click();
    });
    
	$(document).delegate('.brand_product_img_picker','click',function(e){
        var row_id = $(this).attr('row_id');
        $("input[id='brand_product_image_"+row_id+"']").click();
    });
    
    $("#snw_banner_image_picker").click(function() {
        $("input[name='banner_image']").click();
    });
    
    $("#snw_offer_image_picker").click(function() {
        $("input[name='offer_image']").click();
    });
    
    function read_url_banner_image(input) {
    
        var pickerId= '#snw_'+input.id+'_picker';
        var fieldId	= '#'+input.id;
        $(fieldId+'_val').val(input.value);
        
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var size = file.size/1000000; // get image size in mb;
            if(size <= 3.0955135) {
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //alert(pickerId);
                        $(pickerId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $(fieldId).val("");
                    bootbox.alert("File not supported!");
                }
            } else {
                $(fieldId).val("");
                bootbox.alert("File size should be less then 3MB!");
            }
        }
    }
    
    /* 
    | -----------------------------------------------------
    | Manage add quantity row 
    | -----------------------------------------------------
    */ 
    function add_row() {
        // get last product row number
        var last_row = $('#last_qty_row').val();
        last_row = parseInt(last_row)+1;
        $('#last_qty_row').val(last_row);
        
        /* Check Which campaign type is selected */
        var campaign_type = '<?php echo (isset($campaign_type)) ? $campaign_type : '';?>';
        if(campaign_type == '' || campaign_type == undefined ) {
			campaign_type = $('input[name=campaign_type]:checked', '#add_scratchnwin_form').val();
		}
		$('#loader1').fadeIn();
        $.ajax ({
            type: "POST",
            dataType:'jSon',
            data : {row_id:last_row,campaign_type:campaign_type},
            url: "<?php echo base_url() ?>admin/settings/campaign/product_form",
            success: function(data){
				$('#loader1').fadeOut();
                if(data.status) {
                    $('#product_tables .brand_tr:last').after(data.product_form);
                }              
            }
        });
    }
    
    /* 
    | -----------------------------------------------------
    | Remove product row
    | -----------------------------------------------------
    */ 
    $(document).delegate(".romove_row","click",function(e){
        var row_id = $(this).attr('row_id');
        var snw_product_id = $(this).attr('snw_product_id');
        var snw_id = '<?php echo $snw_id;?>';
        var rowCount = $('#product_tables >tbody >tr').length;
        if(rowCount == 1) {
            bootbox.alert("You have to keep at least one product in the campaign!");
            return false;
        }
        
        if(snw_product_id != undefined && snw_product_id != '') {
            bootbox.confirm("Are you sure want to delete this?", function(result) {
                if(result==true) {
					$('#loader1').fadeIn();
                    $.ajax ({
                        type: "POST",
                        dataType:'jSon',
                        data : {snw_product_id:snw_product_id,snw_id:snw_id},
                        url: "<?php echo base_url() ?>admin/settings/campaign/remove_snw_products",
                        success: function(data){
							$('#loader1').fadeOut();
                            if(data.status) {
                                // hide the selected row
                                $('#qty_row_'+row_id).remove();
                            } else {
                                bootbox.alert("You can't remove this product until it's in active price list!");
                            }
                        }
                    });
                }
            });
        } else {
            // hide the selected row
            $('#qty_row_'+row_id).html('');
        }
    });
    
     /* 
    | -----------------------------------------------------
    | Remove brand row
    | -----------------------------------------------------
    */ 
    $(document).delegate(".romove_brand_row","click",function(e){
        var row_id = $(this).attr('row_id');
        var brand_id = $(this).attr('brand_id');
        var cte_id = '<?php echo $snw_id;?>';
        var rowCount = $('#product_tables >tbody >tr').length;
        if(rowCount == 1) {
            bootbox.alert("You have to keep at least one brand in the campaign!");
            return false;
        }
        
        if(brand_id != undefined && brand_id != '') {
            bootbox.confirm("Are you sure want to delete this?", function(result) {
                if(result==true) {
					$('#loader1').fadeIn();
                    $.ajax ({
                        type: "POST",
                        dataType:'jSon',
                        data : {brand_id:brand_id,cte_id:cte_id},
                        url: "<?php echo base_url() ?>admin/settings/campaign/remove_brands",
                        success: function(data){
							$('#loader1').fadeOut();
                            if(data.status == 1) {
                                // hide the selected row
                                $('#brand_row_'+row_id).remove();
                            }  else if(data.status == 2) {
                                bootbox.alert("You have to take atleast one active brand!");
                            } else {
                                bootbox.alert("You can't remove this brand until it's in active price list!");
                            }
                        }
                    });
                }
            });
        } else {
            // hide the selected row
            $('#brand_row_'+row_id).html('');
        }
    });
    
    /* 
    | -----------------------------------------------------
    | Get day diffrance betweem start and end date
    | -----------------------------------------------------
    */
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function daydiff(first, second) {
        return (second-first)/(1000*60*60*24);
    }
    
    /* 
    | -----------------------------------------------------
    | Reset product's prizes sent count as 0
    | -----------------------------------------------------
    */
    $(document).delegate('.reset_price_sent','click',function(e){
        var row_id = $(this).attr('row_id');
        var snw_product_id = $(this).attr('snw_product_id');
        var prizes_sent_this_week = $('#prizes_sent_this_week_'+row_id).val();
        if(prizes_sent_this_week == 0 || prizes_sent_this_week == '') {
            bootbox.alert("Prizes sent count must be greater then 0");
            return false;
        }
        if(snw_product_id != undefined && snw_product_id != '') {
            bootbox.confirm("Are you want to reset the prizes sent count?", function(result) {
                if(result==true) {
                    $.ajax ({
                        type: "POST",
                        dataType:'jSon',
                        data : {snw_product_id:snw_product_id},
                        url: "<?php echo base_url() ?>admin/settings/campaign/reset_prize_sent_log",
                        success: function(data) {
                            if(data.status) {
                                // hide the selected row
                                $('#prizes_sent_this_week_'+row_id).val(0);
                            }
                        }
                    });
                }
            });
        }
    });
    
    /* 
    | -----------------------------------------------------
    | Change product's status active or inactive
    | -----------------------------------------------------
    */
    $(document).delegate('#product_status','click',function(e){
        var row_id = $(this).attr('row_id');
        var snw_product_id = $(this).attr('snw_product_id');
        var snw_id = '<?php echo $snw_id;?>';
        
        if(snw_product_id != undefined && snw_product_id != '') {
            bootbox.confirm("Are you want to change this status?", function(result) {
                if(result==true) {
					$('#loader1').fadeIn();
                    $.ajax ({
                        type: "POST",
                        dataType:'jSon',
                        data : {snw_product_id:snw_product_id,row_id:row_id,snw_id:snw_id},
                        url: "<?php echo base_url() ?>admin/settings/campaign/change_product_status",
                        success: function(data) {
							$('#loader1').fadeOut();
                            if(data.status == 1) {
                                var status_html = '<a row_id='+row_id+' snw_product_id= '+snw_product_id+' href="javascript:;" id="product_status" class="scratch-prod-icon '+data.btn_class+'"><i class="fa '+data.icon_class+'"></i></a>';
                                $('.product_status_td_'+row_id).html(status_html);
                            } else if(data.status == 2) {
                                bootbox.alert("You have to take atleast one active product!");
                            }
                        }
                    });
                }
            });
        }
    });
    
    /* 
    | -----------------------------------------------------
    | Change brand's status active or inactive
    | -----------------------------------------------------
    */
    $(document).delegate('#brand_status','click',function(e){
        var row_id = $(this).attr('row_id');
        var brand_id = $(this).attr('brand_id');
        var cte_id = '<?php echo $snw_id;?>';
        
        if(brand_id != undefined && brand_id != '') {
            bootbox.confirm("Are you want to change this status?", function(result) {
                if(result==true) {
					$('#loader1').fadeIn();
                    $.ajax ({
                        type: "POST",
                        dataType:'jSon',
                        data : {brand_id:brand_id,row_id:row_id,cte_id:cte_id},
                        url: "<?php echo base_url() ?>admin/settings/campaign/change_brand_status",
                        success: function(data) {
							$('#loader1').fadeOut();
                            if(data.status == 1) {
                                var status_html = '<a row_id='+row_id+' brand_id= '+brand_id+' href="javascript:;" id="brand_status" class="scratch-prod-icon '+data.btn_class+'"><i class="fa '+data.icon_class+'"></i></a>';
                                $('.brand_status_td_'+row_id).html(status_html);
                            } else if(data.status == 2) {
                                bootbox.alert("You have to take atleast one active brand!");
                            }
                        }
                    });
                }
            });
        }
    });
    
    /* 
    | -----------------------------------------------------
    | Validate Prizes counts
    | -----------------------------------------------------
    */
    $(document).delegate('.prizes_count','blur',function(e){
        var row_id = $(this).attr('row_id');
        var is_per_day_count = $(this).attr('is_per_day_count');
        var prizes_per_day = $('#prizes_per_day_'+row_id).val();
        var total_prizes_per_week = $('#total_prizes_'+row_id).val();
        //console.log(prizes_per_day+'=='+total_prizes_per_week+'=='+is_per_day_count);
        prizes_per_day = parseInt(prizes_per_day);
        total_prizes_per_week = parseInt(total_prizes_per_week);
        if(prizes_per_day != '' && total_prizes_per_week != '' && prizes_per_day >= total_prizes_per_week) {
            if(is_per_day_count == 1 && is_per_day_count != undefined) {
                $('#prizes_per_day_'+row_id).val('');
            } else{
                $('#total_prizes_'+row_id).val('');
            }
            bootbox.alert("Total prizes should be greater than Prizes per day!");
            return false;
        } 
    });
    
    /* 
    | -----------------------------------------------------
    | Manage availabily of second draw count
    | -----------------------------------------------------
    */
    $('#second_chance_draw').click(function() {
        if($("#second_chance_draw").is(':checked')){
            $('#second_chance_count').show();
        } else {
            $('#second_chance_count').hide();
        }
    });
    
    /* 
    | -----------------------------------------------------
    | Manage beacon offer availabily of promotion
    | -----------------------------------------------------
    */
    $('#beacon_promotion').click(function() {
        if($("#beacon_promotion").is(':checked')){
            $('#beacon_promotion_div').fadeIn('slow');
        } else {
            $('#beacon_promotion_div').fadeOut('slow');
        }
    });
    
    /* 
    | -----------------------------------------------------
    | Manage select all stores
    | -----------------------------------------------------
    */
    $('#select_all').click(function() {
        if($("#select_all").is(':checked')) {
            $('#beacon_store_id option').prop('selected', true);
        } else {
            $('#beacon_store_id option').prop('selected', false);
        }
    });
    
    /*
    | ------------------------------------------------------------
    | Manage available day's hidden value
    | ------------------------------------------------------------
    */ 
    $(document).delegate('.days_avail_check','click',function(e){
        
        var row_id = $(this).attr('row_id');
        var value = $(this).attr('value');
        var short_day_name = $(this).attr('short_day_name');
		var campaign_type = $(this).attr('campaign_type');
		// set id/ class name as per campaign type
		var avail_btn_id  = 'daysavailability_btn_'+row_id;
        var day_check_cls = 'day_checkbox_cls_';
        var day_check_id  = 'day_checked_';
		if(campaign_type == 2) {
			avail_btn_id  = 'branddaysavailability_btn_'+row_id;
			day_check_cls = 'brandday_checkbox_cls_';
			day_check_id  = 'brandday_checked_';
		}
		
        var id = value+'_'+row_id+'_'+campaign_type;
        if($("#"+id).is(':checked')){
            $('#'+day_check_id+short_day_name+'_'+row_id).val('1');
        } else {
            $('#'+day_check_id+short_day_name+'_'+row_id).val('0');
        }
		
        // set week button text
        var day_btn_val = '';
        var day_btn_title = '';
        var is_day_avail = false;
        $('input.'+day_check_cls+row_id+'[type=checkbox]').each(function () {
            if(this.checked) {
                var day_val = $(this).val();
                // set button html text
                day_btn_val += day_val[0].toUpperCase();
                day_btn_val += ' | ';
                // set button title text
                day_btn_title += day_val[0].toUpperCase() + day_val.substring(1,3);
                day_btn_title += ' | ';
                is_day_avail = true;
            }
        });
       
        if(is_day_avail) {
            day_btn_val = $.trim(day_btn_val);
            day_btn_title = $.trim(day_btn_title);
            day_btn_val = (day_btn_val != '') ? day_btn_val.slice(0, -1) : '';
            day_btn_title = (day_btn_title != '') ? day_btn_title.slice(0, -1) : '';
            $('#'+avail_btn_id).attr('title',day_btn_title);
            $('#'+avail_btn_id).html(day_btn_val);
            $('.modal_title_'+row_id).html('&nbsp;');
        } else {
            $('.modal_title_'+row_id).addClass('error');
            $('.modal_title_'+row_id).html('Please select atleast one day!');
            $("#"+id).parents('span').addClass("checked");
            $("#"+id).prop('checked', 'checked');
            $('#'+day_check_id+short_day_name+'_'+row_id).val('1');
        }
    });
    
    /*
    | ------------------------------------------------------------
    | Show bootstrap image popup on listing view
    | ------------------------------------------------------------
    */ 
    $(document).delegate( ".daysavailability", "click", function(e) {
        var row_id = $(this).attr('row_id');
        var campaign_type = $(this).attr('campaign_type');
        $('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
        if(campaign_type == 3) {
			$('#branddaysavailabilitymodal_'+row_id).modal('show');
		} else {
			$('#daysavailabilitymodal_'+row_id).modal('show');
		}     
        $('.modal_title_'+row_id).html('&nbsp;');
    });
    
    /* Script Function to manage Crack the egg and Scratch n win */
    $(document).ready(function() {
		
		$(document).delegate('.notifyintervalAction','click',function(e){               
            $('.prize-frequency-box').hide();                
            $('.second-chance-box').hide();             
            $('.product_tr').hide();             
            $('.brand_tr').show();             
        });
		$(document).delegate('.notifytimeAction','click',function(e){
			$('.prize-frequency-box').show();                
            $('.second-chance-box').show(); 
            $('.product_tr').show();             
            $('.brand_tr').hide();                   
        });
       
		/* only type numberic value */
        $(document).delegate('.number_input','keypress',function(e){
             if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57)) {
                   return false;
            }
        });
             
		/* only type numberic value */
		$(document).delegate('.cte_number_input','keypress',function(e){
			console.log(e.which);
			 if (e.which != 8 && e.which != 0 && (e.which < 47 || e.which > 57)) {
				   return false;
			}
		});                
    });
	
	$(document).delegate('.import_csv_btn','click',function(e){
		var row_id = $(this).attr('row_id');
		$('#uploadcsvmodal_'+row_id).modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        $('.modal_title_'+row_id).html('&nbsp;');
	});
	
	$(document).delegate( ".uploadcsv", "click", function() {
		var row_id = $(this).attr('row_id');
		var brand = $('#brand_'+row_id).val();
		if(brand == '') {
			 bootbox.alert("Please add Brand name first!");
            return false;
		}
		var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv)$/;  
       //Checks whether the file is a valid csv file  
       if (regex.test($("#csvfile_"+row_id).val().toLowerCase())) {  
           //Checks whether the browser supports HTML5  
           if (typeof (FileReader) != "undefined") {  
               var reader = new FileReader();  
               reader.onload = function (e) {
					//var html = '';                   
					var product_ids = $('#brand_product_ids_'+row_id).val();
					var brand_products_html = $("#csvproducts_"+row_id).html();           
					//Splitting of Rows in the csv file  
					var csvrows = e.target.result.split("\n");
					for (var i = 0; i < csvrows.length; i++) {
						if (csvrows[i] != "" && Math.floor(csvrows[i]) == csvrows[i] && $.isNumeric(csvrows[i]) ) {                 
							var csvcols = csvrows[i].split(",");
							var prod_id = csvcols[0]+',';
							if(product_ids.indexOf(prod_id) == -1) { // accept only non duplicate id
								brand_products_html += '<span class="chosen-choices fl ml5">&nbsp;<span>'+csvcols[0]+'</span>&nbsp;&nbsp;<span class="search-choice-close crp" row_id='+row_id+' prod_id="'+csvcols[0]+'" ><strong> X </strong></span>&nbsp;</span>';
								product_ids += prod_id;
							}
						}  
					}             
					$("#csvproducts_"+row_id).html(brand_products_html);
					if(product_ids.length > 0){
						$('#brand_product_ids_'+row_id).val(product_ids);
					}
				}
				$('#csvproducts_'+row_id).removeClass('error');
				reader.readAsText($("#csvfile_"+row_id)[0].files[0]);  
			}  
			else {  
               alert("Sorry! Your browser does not support HTML5!");  
			}  
       } else {  
           alert("Please upload a valid CSV file!");  
       }
		return;
		e.preventDefault(); 
   });
   
   /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".search-choice-close","click",function(e){
		$(this).parent('span').remove();
		var prod_id = $(this).attr('prod_id');
		var row_id = $(this).attr('row_id');
		var product_ids = $('#brand_product_ids_'+row_id).val();
		if(prod_id != '') {
			product_ids = product_ids.replace(prod_id+',','');
			$('#brand_product_ids_'+row_id).val(product_ids);
		}
    });
	
	/* 
	| -----------------------------------------------------
	| Store day's entry points
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".save_entries","click",function(e){
		var row_id = $(this).attr('row_id');
		$('input.brandday_checkbox_cls_'+row_id+'[type=checkbox]').each(function () {
			if(this.checked) {
				 var day_val = $(this).val();
				 //console.log(day_val);
			}
		});
	});
</script>




