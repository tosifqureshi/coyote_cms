<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Competition Controller
 *
 * Manages the competition functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Competition
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('competition');
		$this->load->library('form_validation');
		$this->load->model('competition/Competition_model', 'competition_model');
		// set competition images path
		$this->image_path = FCPATH.'uploads/competition_images/'; 
		// set product images path
		$this->product_image_path = FCPATH.'uploads/product_images/'; 
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the competition listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
	
		// get competition listing
		$competitions = $this->competition_model->get_competitions();
		// update expire competitions
		$this->competition_model->update_expiry_competitions();
		Template::set('competitions',$competitions);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage competition form
	 * @input : competition_id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($competition_id=0) {
		
		$competition_id = decode($competition_id); // decode competition id
		$data = array();
		
		if((isset($_POST['save'])) && $competition_id=='') {
			if($this->save_competition()) {       
					Template::set_message(lang('co_insert_msg'),'success');
					redirect('admin/settings/competition');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_competition($competition_id)) {
					Template::set_message(lang('co_update_msg'),'success');
					redirect('admin/settings/competition');
			}
		}
		$data['competition_title']	= lang('add_competition');
		if($competition_id) {
			$competition = $this->competition_model->get_competitions($competition_id);	
			if(empty($competition)) {
				Template::set_message(lang('co_invalid_msg'),'error');
				redirect('admin/settings/competition');
			}
			$data['competition_title']	= lang('edit_competition');
			$data['competition_id']	= encode($competition_id);
			$data['title']			= $competition->title;		
			$data['short_description']= $competition->short_description;		
			$data['description']	= $competition->description;		
			$data['start_date']		= date('d-m-Y',strtotime($competition->start_date));
			$data['end_date']		= date('d-m-Y',strtotime($competition->end_date));
			$data['status']			= $competition->status;
			$data['created_at']		= $competition->created_at;
			$data['image_1']		= $competition->image_1;
			$data['image_2']		= $competition->image_2;
			$data['image_3']		= $competition->image_3;
			$data['default_image_type']	= $competition->default_image_type;
			$data['type']			= $competition->type;
			$data['reset_days']		= $competition->reset_days;
			$data['product_images']	= $this->competition_model->get_co_product_images($competition_id);
		}
		// set product emtity form
		$data['co_product_form'] = $this->load->view('co_product_form',array('row_id'=>1),true);
		
		Template::set('data',$data);
		Template::set_view('competition_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store competition in database
	 * @input : type , competition_id
	 * @return: void
	 * @access: private
	 */
	private function save_competition($competition_id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($competition_id)) {
			$data['competition_id']  = $competition_id;
			// get competition details
			$competition = $this->competition_model->get_competitions($competition_id);
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('title', 'lang:title', 'trim|required|max_length[80]');			
		$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');	
		$this->form_validation->set_rules('end_date', 'lang:end_date', 'trim|required');
		$this->form_validation->set_rules('reset_days', 'lang:reset_days', 'trim|required|digits');
		$product_ids     = $this->input->post('product_ids');
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		} else if($product_ids == '') {
			Template::set_message("Please add products ",'error');
			return FALSE;
		}
		
		// Set input field values
		if (isset($_FILES['image_1']) && ($_FILES['image_1']['name']) != '' ) {
			// remove old image
			(isset($competition->image_1) && !empty($competition->image_1)) ? unlink($this->image_path.$competition->image_1) : '';
			// upload image
			$data['image_1'] = $this->to_upload_image('image_1');
		}
		if (isset($_FILES['image_2']) && ($_FILES['image_2']['name']) != '' ) {
			// remove old image
			(isset($competition->image_2) && !empty($competition->image_2)) ? unlink($this->image_path.$competition->image_2) : '';
			// upload image
			$data['image_2'] = $this->to_upload_image('image_2');
		}
		if (isset($_FILES['image_3']) && ($_FILES['image_3']['name']) != '' ) {
			// remove old image
			(isset($competition->image_3) && !empty($competition->image_3)) ? unlink($this->image_path.$competition->image_3) : '';
			// upload image
			$data['image_3'] = $this->to_upload_image('image_3');
		}
		
		// set post values in store bundle
		$data['title']   			= $this->input->post('title');
		$data['short_description']  = $this->input->post('short_description');
		$data['description']    	= $this->input->post('description');
		$data['created_by'] 		= $this -> session -> userdata('user_id');
		$data['start_date']			= date("Y-m-d",strtotime($this->input->post('start_date')));
		$data['end_date']  			= date("Y-m-d",strtotime($this->input->post('end_date')));
		$data['default_image_type'] = (!empty($this->input->post('default_image_type'))) ? $this->input->post('default_image_type') : 1;
		$data['type']   			= $this->input->post('type');
		$data['reset_days']   		= $this->input->post('reset_days');
		if(empty($competition_id)) {
			$current_date =  date("Y-m-d H:i:s");
			// set default result date as start date
			$data['competition_result_date'] = $data['start_date'];
			$data['last_participant_cron_date'] = $current_date;
			$data['created_at'] = $current_date;
		}
		// add or update loyalty data into db
		$competition_id = $this->competition_model->save_competition_data($data);
		// manage products image data
		$this->manage_product_images($competition_id);
		return $competition_id;
	}
	
	/**
	 * Add competition's product data
	 * @input : competition_id
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	private function manage_product_images($competition_id=0) {
		// manage product images log
		$product_ids     = array_filter($this->input->post('product_ids'));
		$product_name    = array_filter($this->input->post('product_name'));
		$product_img_ids = array_filter($this->input->post('product_img_ids'));
		$product_group	 = array_filter($this->input->post('product_group'));
		$product_qty	 = array_filter($this->input->post('product_qty'));
		$product_points	 = array_filter($this->input->post('product_points'));
		
		if(!empty($product_ids)) {
			foreach($product_ids as $key=>$id) {
				$product_data = array();
				
				$product_data['product_id']     = $id;
				$product_data['competition_id']  = $competition_id;
				$product_data['product_name']   = (isset($product_name[$key])) ? $product_name[$key] : '';
				$product_data['product_group']  = (isset($product_group[$key])) ? $product_group[$key] : '';
				$product_data['product_qty']    = (isset($product_qty[$key])) ? $product_qty[$key] : '';
				$product_data['product_point']	= (isset($product_points[$key])) ? $product_points[$key] : '';
				$product_data['modified_at']	= date("Y-m-d H:i:s");
				
				if(isset($product_img_ids[$key]) && !empty($product_img_ids[$key])) {
					$product_data['product_img_id']    = $product_img_ids[$key];
				}
				
				// add competition product data
				$this->competition_model->add_product_image($product_data);
			}
		}
		return true;
	}

	//--------------------------------------------------------------------

	/**
	 * Upload competition image
	 * @input : image
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	private function to_upload_image($image='',$upload_at='competition_images/') {
		
		$dirPath = IMAGEUPLOADPATH.$upload_at;
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('admin/settings/competition');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	//--------------------------------------------------------------------

	/**
	 * Update status of competition
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['competition_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->competition_model->save_competition_data($data);
		Template::set_message(lang('co_update_status'),'success');
		redirect('admin/settings/competition');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove competition
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$data['competition_id'] = decode($id);
		$data['is_deleted'] = 1;
		$id = $this->competition_model->save_competition_data($data);
		Template::set_message(lang('co_delete_msg'),'success');
		redirect('admin/settings/competition');
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_image() {
		// get data from post
		$id = $this->input->post('id');
		$image_type = $this->input->post('image_type');
		$file_name = $this->input->post('file_name');
		$return = false;
		if(!empty($image_type) && !empty($id)) {
			// update image value as blank
			$data['competition_id'] = decode($id);
			$data['image_'.$image_type] = "";
			// delete image from folder
			unlink($this->image_path.$file_name);
			// update image value in db
			$this->competition_model->save_competition_data($data);
			$return = true;
		} else if(!empty($image_type)) {
			$return = true;
		}
		echo $return;die;
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to get members purchase data in competition duration
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function competition_members($competition_id=0) {
		// get data from post
		//$competition_id = $this->input->post('competition_id');
		if(!empty($competition_id)) {
			$competition_id = decode($competition_id);
			// get competition details
			$competition = $this->competition_model->get_competitions($competition_id);
			if(!empty($competition)) {
				// set competition params for fetch purchase data
				$product_ids = (!empty($competition->product_ids)) ? $competition->product_ids : '';
				$start_date = (!empty($competition->start_date)) ? date('Ymd',strtotime($competition->start_date)) : '';
				$end_date = (!empty($competition->end_date)) ? date('Ymd',strtotime($competition->end_date)) : '';
				
				// set competition's updated start and end date
				$co_start_date	= date('Ymd',strtotime($competition->competition_result_date)); // like 20150120
				$co_end_date	= date('Ymd',strtotime(date('Y-m-d H:i:s'))); // like 20160428
				
				// get members purchase data in competition duration
				$co_members = $this->competition_model->get_competition_leader_data($competition_id,$co_start_date,$co_end_date);
			}
		}
		// set competition title
		$competition_title = (isset($competition->title) && !empty($competition->title)) ? $competition->title : '';
		// set members result if exists
		$co_members = (isset($co_members) && !empty($co_members)) ? $co_members : '';
		Template::set('competition_title',$competition_title);
		Template::set('competition_members',$co_members);
		Template::set_view('competition_members');
		Template::render();
	}
	
	/**
	 * Function used to show listing of lucky winners of competition 
	 * @input : null
	 * @return: void
	 * @access: public
	 *  
	 */
	 public function luckydraw() {
		
		// get lucky draw winners data
		$competition_winners = $this->competition_model->get_luckydraw_competition();
		
		Template::set('competition_winners',$competition_winners);
		Template::set('co_header',lang('co_draw_winner'));
		Template::set_view('competition_winners');
		Template::render();
	 }
		
	/**
	 * Function used to show listing of leader board winners of competition 
	 * @input : null
	 * @return: void
	 * @access: public
	 *  
	 */
	 public function leaderboard() {
		
		// get lucky draw winners data
		$competition_winners = $this->competition_model->get_leaderboard_competition();
		
		Template::set('competition_winners',$competition_winners);
		Template::set('leaderboard',1);
		Template::set('co_header',lang('co_leaderboard_winner'));
		Template::set_view('competition_winners');
		Template::render();
	 }	
	 
	/**
	 * Function used to show listing of leader board details
	 * @input : int competition_id
	 * @return: void
	 * @access: public
	 *  
	 */
	 public function leaderboarddetails($competition_id=0) {
		 
		if(!empty($competition_id)) {
			$competition_id = decode($competition_id);
			// get competition details
			$competition = $this->competition_model->get_competitions($competition_id);
			
			if(!empty($competition)) {
				// set competition data
				$co_type = (!empty($competition->type)) ? $competition->type : '';
				if($co_type == 2) {
					//get members leaderboard records
					$winning_leaderboard_result = $this->leaderboard_members($competition_id);
				} else {
					//get members purchase records for lucky draw competitions
					$winning_leaderboard_result = $this->luckydraw_members($competition_id);
				}
			} else {
				//Template::set_message($error,'error');
				redirect('admin/settings/competition');	
			}	
		} else {
			//Template::set_message($error,'error');
			redirect('admin/settings/competition');	
		}	
		
		Template::set('winning_leaderboard_result',$winning_leaderboard_result);
		Template::set('co_type',$co_type);
		Template::set('competition_id',$competition_id);
		Template::set_view('competition_winners_leaderboard');
		Template::render();
	 }
	 
	/**
	 * Function used to get members leaderboard records
	 * @input : int competition_id
	 * @return: array
	 * @access: private
	 *  
	 */
	 private function leaderboard_members($competition_id=0) {
		// get winners leaderboard data
		$leaderboard_data = $this->competition_model->get_co_winners_log($competition_id);
		// prepare competition leaderboards winner data
		$co_reset_date = '';
		foreach($leaderboard_data as $leaderboard) {
			$winning_result['winning_result_date'] = $leaderboard['created_at'];
			$winning_result['spent_points'] = $leaderboard['spent_points'];
			$winning_result['winner_id'] = $leaderboard['winner_id'];
			
			// set result date
			$competition_result_date = date('Y-m-d',strtotime($leaderboard['created_at']));
			// get members leaderboard results
			$member_leaderboard_data = $this->competition_model->get_member_leaderboard_log($competition_id,$competition_result_date,$co_reset_date);
			$co_reset_date = $competition_result_date;
			$member_winning_array = array();
			foreach($member_leaderboard_data as $val) {
				$member_leaderboard['member_id'] = $val['member_id'];
				$member_info = $this->competition_model->member_info($val['member_id']);
				$member_leaderboard['member_name'] = $member_info['member_name'];
				$member_leaderboard['member_email'] = $member_info['member_email'];
				$member_leaderboard['total_spent_points'] = $val['total_spent_points'];
				$member_winning_array[] = $member_leaderboard;
			}
			$winning_result['leaderboard_members'] = $member_winning_array;
			$winning_leaderboard_result[] = $winning_result;
		}
		return $winning_leaderboard_result;
	 }
	 
	 /**
	 * Function used to get members luckydraw records
	 * @input : int competition_id
	 * @return: array
	 * @access: private
	 *  
	 */
	 private function luckydraw_members($competition_id=0) {
		 
		// get luckydraw end date result data
		$co_luckydraw_data = $this->competition_model->get_competition_luckydraw_log($competition_id);
		// prepare competition leaderboards data
		$co_reset_date = '';
		$member_leaderboard_result = array();
		
		foreach($co_luckydraw_data as $luckydraw) {
			$luckydraw_result = array();
			$luckydraw_result['winning_result_date'] = $luckydraw['created_at'];
			// set result date
			$competition_result_date = $luckydraw['created_at'];
			// get lucky draw winner result
			$luckydraw_winner_data = $this->competition_model->get_luckydraw_winner_data($competition_id,$competition_result_date);
			
			if(!empty($luckydraw_winner_data)) {
				// set winner member's details
				$luckydraw_result['member_id'] = $luckydraw_winner_data->member_id;
				$member_info = $this->competition_model->member_info($luckydraw_winner_data->member_id);
				$luckydraw_result['member_name'] = $member_info['member_name'];
				$luckydraw_result['member_email'] = $member_info['member_email'];
				$luckydraw_result['luckydraw_winner'] = 1;
			} else {
				
				// get members leaderboard results
				$member_leaderboard_data = $this->competition_model->get_member_leaderboard_log($competition_id,$competition_result_date,$co_reset_date);
				$co_reset_date = $competition_result_date;
				$member_array = array();
				foreach($member_leaderboard_data as $val) {
					$member_leaderboard['member_id'] = $val['member_id'];
					$member_info = $this->competition_model->member_info($val['member_id']);
					$member_leaderboard['member_name'] = $member_info['member_name'];
					$member_leaderboard['member_email'] = $member_info['member_email'];
					//$member_leaderboard['total_spent_points'] = $val['total_spent_points'];
					$member_array[] = $member_leaderboard;
				}
				$luckydraw_result['leaderboard_members'] = $member_array;
				
			}
			$member_leaderboard_result[] = $luckydraw_result;
		}
		return $member_leaderboard_result;
	 }

	
	/**
	 * Function used to show listing of lucky winners of competition 
	 * @input : null
	 * @return: void
	 * @access: public
	 *  
	 */
	 public function luckydrawold() {
		
		// get competition details
		$competitions = $this->competition_model->get_competitions(0,1);
		$draw_winners = array();
		if(!empty($competitions)) {
			foreach($competitions as $competition) {
				// set competition params for fetch purchase data
				$product_ids = (!empty($competition['product_ids'])) ? $competition['product_ids'] : '';
				$start_date = (!empty($competition['start_date'])) ? date('Ymd',strtotime($competition['start_date'])) : '';
				$end_date = (!empty($competition['end_date'])) ? date('Ymd',strtotime($competition['end_date'])) : '';
				// get members purchase data in competition duration
				$co_members = $this->competition_model->competitions_winner_data($product_ids,$start_date,$end_date);
				if(!empty($co_members)) {
					// set competition result values in array
					$co_draw_winners['co_id'] = $competition['competition_id'];
					$co_draw_winners['co_title'] = $competition['title'];
					$co_draw_winners['start_date'] = $competition['start_date'];
					$co_draw_winners['end_date'] = $competition['end_date'];
					// set winner's result values in array
					$co_draw_winners['co_member_name'] = $co_members['member_name'];
					$co_draw_winners['co_member_email'] = $co_members['member_email'];
					$co_draw_winners['co_member_id'] = $co_members['member_id'];
					$co_draw_winners['co_purchased_qty'] = $co_members['purchased_qty'];
					$draw_winners[] = $co_draw_winners;
				}
			}
		}
		Template::set('competition_winners',$draw_winners);
		Template::set_view('competition_winners');
		Template::render();
	 }
	 
	 //--------------------------------------------------------------------
	
	/**
	 * Function to get product data
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function getproductdata() {
		// get data from post
		$product_id = $this->input->post('product_id');
		$product_name = '';
		if(!empty($product_id)) {
			// get product details from product id
			$product_data = $this->competition_model->get_product_data($product_id);
			// set product name
			$product_name = (isset($product_data->Prod_Desc) && !empty(isset($product_data->Prod_Desc))) ? $product_data->Prod_Desc : '';
		} 
		// set return json response
		echo json_encode(array('product_name'=>$product_name));die;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to get set lucky draw winner
	 * @input  : json winner_data , int competition_id ,string draw_date
	 * @output : string
	 * @access : public
	 */
	public function set_luckydraw_winner() {
		// get data from post
		$member_id = $this->input->post('member_id');
		$competition_id = $this->input->post('competition_id');
		$draw_date = $this->input->post('draw_date');
		$status = false;
		if(!empty($member_id) && !empty($competition_id) && !empty($draw_date)) {
			// set post values in store winner log
			$data['competition_id'] = $competition_id;
			$data['member_id']      = $member_id;
			$data['created_at']    	= $draw_date;
			$winner_id = $this->competition_model->add_luckydraw_winner($data);
			if(!empty($winner_id)) {
				$status = true;
			}
		} 
		// set return json response
		echo json_encode(array('status'=>$status));die;
	}
	
	
	/**
	 * Function to remove product image from competition
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_competition_products() {
		// get data from post
		$id = $this->input->post('product_img_id');
		$file_name = $this->input->post('product_image');
		$return = false;
		if(!empty($id)) {
			// update image value as blank
			$data['product_img_id'] = decode($id);
			$data['is_deleted'] = 1;
			$data['modified_at'] = date('Y-m-d H:i:s');
			// delete image from folder
			if(!empty($file_name) && file_exists($this->product_image_path.$file_name)) {
				unlink($this->product_image_path.$file_name);
			}
			// update image value in db
			$this->competition_model->add_product_image($data);
			$return = true;
		}
		// set return json response
		echo json_encode(array('status'=>$return));die;
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to render competition product form 
	 * @input : row_id
	 * @return  void
	 * @access public
	 */
	public function product_form()
	{
		// get data from post
		$row_id = $this->input->post('row_id');
		$data['row_id'] = $row_id;
		$tpl_data = $this->load->view('co_product_form',$data,true);
		echo json_encode(array('status'=>true,'product_form'=>$tpl_data));die;

	}//end product_form()

}//end Settings

// End of Admin Competition Controller
/* Location: ./application/core_modules/competition/controllers/settings.php */
