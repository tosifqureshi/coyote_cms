<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Competition Model
 *
 * The central way to access and perform CRUD on Competition.
 *
 * @package    Avaitor
 * @subpackage Modules_Competition
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Competition_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
		$this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // define tables
		$this->product_table = 'ProdTbl';
		$this->outp_table    = 'OutpTbl';
		$this->store_table   = 'STORTBL';
		$this->competitions_table  = 'competitions';
		$this->competition_products_table = 'competition_products';
		$this->leaderboards_table = 'leaderboards';
		$this->competition_winners_table = 'competition_winners';
		$this->competition_luckydraw_table = 'ava_competition_luckydraw';
    }

	/**
	 * Function to get product data from MSSQL database
	 * @input : 
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	public function get_ms_products($limit=0,$offset=0) { 
		$result= mssql_query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM PRODTBL where Prod_Number != "") a WHERE row > '.$limit.' and row <= '.$offset);
		$result= $this->mssql_db->query('SELECT ACC_NUMBER,ACC_FIRST_NAME,ACC_SURNAME,ACC_ADDR_1,ACC_ADDR_2,ACC_POST_CODE,ACC_ACCOUNT_NAME,ACC_CONTACT,ACC_MOBILE,ACC_EMAIL,ACC_GENDER,ACC_STATUS,ACC_PASSWORD,ACC_DATE_OF_BIRTH,ACC_DATE_ADDED FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY ACC_NUMBER) as row FROM ACCOUNT where ACC_EMAIL != "") a WHERE row > '.$limit.' and row <= '.$offset);
		//echo $this -> mssql_db -> last_query();die;
		return $result -> result_array();
		
	}

	/**
	 * Get listing of competitions
	 * @input : competition_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_competitions($competition_id = 0,$co_expired=0) {
		
		$this -> read_db -> select('competitions.*');
		if ($competition_id) {
			$this -> read_db -> where("competition_id", $competition_id);
			$result = $this -> read_db -> get($this->competitions_table);
			return $result -> row();
		}
		$this -> read_db -> where(array('competitions.is_deleted'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		if($co_expired == 1) {
			// set current date
			$current_date = date('Y-m-d H:i:s');
			$this -> read_db -> where('competitions.end_date <',$current_date);
			//$this -> read_db -> where_or('competitions.is_expired','1');
		}
		$this -> read_db -> order_by('competitions.competition_id', 'desc');
		$result = $this -> read_db -> get($this->competitions_table);
		return $result -> result_array();
		
	}
	
	/**
	 * Set competitions status as deactive if the expires
	 * @input : null
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	function update_expiry_competitions() {
		// set current date
		$current_date = date('Ymd');
		$this -> read_db -> select('competition_id,end_date,status');
		$this -> read_db -> where('is_deleted','0');   
		$this -> read_db -> where('status','1');   
		$this -> read_db -> where('end_date <',$current_date);
		$result = $this -> read_db -> get($this->competitions_table);
		$co_result = $result -> result_array();
		// update competition status
		if(!empty($co_result)) {
			foreach($co_result as $result) {
				$this->db->where('competition_id', $result['competition_id']);
				$this->db->update($this->competitions_table, array('status'=>0));
			}
		}
		return true;
		
	}
	
	/**
	 * Get listing of leaderboard participants in competition
	 * @input : competition_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_competition_leader_data($competition_id=0,$co_start_date='',$co_end_date='') {
		
		$this->read_db->select('member_id,sum(spent_points) as total_spent_points');
		$this->read_db->where('competition_id',$competition_id);
		$this->read_db->where('created_at >=', $co_start_date);
		$this->read_db->where('created_at <=', $co_end_date);
		$this->read_db->group_by('member_id');
		$this->read_db->order_by('total_spent_points','desc');
		$result = $this->read_db->get($this->leaderboards_table);
		
		return $result->result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation of competition data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_competition_data($data) {
		
		if(isset($data['competition_id']) && !empty($data['competition_id'])) { // update data
			$this->db->where('competition_id', $data['competition_id']);
			$this->db->update($this->competitions_table, $data);
			return $data['competition_id'];
		} else { // add data
			$this->db->insert($this->competitions_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Function to get members purchase log in competition duration
	 * @input : string $product_ids , string $start_date , string $end_date
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	public function competitions_members_data($product_ids,$start_date,$end_date) {
		$result = $this->mssql_db->query("SELECT A.ACC_FIRST_NAME as member_name , A.ACC_EMAIL as member_email , ATX.ATRX_ACCOUNT as member_id, SUM(ATX.ATRX_QTY) as purchased_qty  FROM ACCOUNT_TRXTBL ATX LEFT JOIN ACCOUNT A on ATX.ATRX_ACCOUNT = A.ACC_NUMBER WHERE  ATX.ATRX_PRODUCT in (".$product_ids.") and ATX.ATRX_DATE between '".$start_date."' and '".$end_date."' and ATX.ATRX_TYPE = 'ITEMSALE' GROUP BY ATX.ATRX_ACCOUNT,A.ACC_FIRST_NAME , A.ACC_EMAIL order by purchased_qty DESC");
		return $result -> result_array();
	}
	
	/**
	 * Function to get competition winner log in competition duration
	 * @input : data
	 * @output: obj array
	 * @author: Tosif Qureshi
	 */
	public function competitions_winner_data($product_ids,$start_date,$end_date) {
		$result = $this->mssql_db->query("SELECT top 1 A.ACC_FIRST_NAME as member_name , A.ACC_EMAIL as member_email , ATX.ATRX_ACCOUNT as member_id, SUM(ATX.ATRX_QTY) as purchased_qty  FROM ACCOUNT_TRXTBL ATX LEFT JOIN ACCOUNT A on ATX.ATRX_ACCOUNT = A.ACC_NUMBER WHERE  ATX.ATRX_PRODUCT in (".$product_ids.") and ATX.ATRX_DATE between '".$start_date."' and '".$end_date."' and ATX.ATRX_TYPE = 'ITEMSALE' GROUP BY ATX.ATRX_ACCOUNT,A.ACC_FIRST_NAME , A.ACC_EMAIL order by purchased_qty DESC");
		return (array) $result -> row();
	}
	
	/**
	 * Function to get product details from product id
	 * @input : product_id
	 * @output: obj array
	 * @author: Tosif Qureshi
	 */
	public function get_product_data($product_id) {
		$product_result = $this->mssql_db->query("select Prod_Desc, Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT where OUTP_STATUS = 'active' and Prod_Number = $product_id");
		return $product_result -> row();
	}
	
	/**
	 * Function to get competition's product images list
	 * @input : competition_id
	 * @output: array
	 * @access: public
	 */
	public function get_co_product_images($competition_id=0) {
		if(!empty($competition_id)) {
			$this -> read_db -> select('*');
			$this -> read_db -> where(array('competition_id'=>$competition_id,'is_deleted'=>0)); 
			$this -> read_db -> order_by('product_img_id', 'asc');
			$result = $this -> read_db -> get($this->competition_products_table);
			return $result -> result_array();
		}	else {
			return false;
		}
	}
	
	/**
	 * Function to manage product image insertion
	 * @input : data(product image data)
	 * @output: int(product_img_id)
	 * @access: public
	 */
	public function add_product_image($data) {
		
		if(isset($data['product_img_id']) && !empty($data['product_img_id'])) { // update data
			$this->db->where('product_img_id', $data['product_img_id']);
			$this->db->update($this->competition_products_table, $data);
			return $data['product_img_id'];
		} else { // add data
			$this->db->insert($this->competition_products_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Get listing of leaderboard competitions
	 * @input : type
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_leaderboard_competition() {
		
		$this->read_db->select($this->competition_winners_table.'.member_id,'.$this->competition_winners_table.'.competition_id,'.$this->competition_winners_table.'.created_at');
		$this->read_db->select($this->competitions_table.'.title,'.$this->competitions_table.'.image_1,'.$this->competitions_table.'.image_2,'.$this->competitions_table.'.image_3,'.$this->competitions_table.'.default_image_type');
		$this->read_db->where($this->competitions_table.'.type',2);
		$this->read_db->join($this->competitions_table ,$this->competitions_table.'.competition_id = '.$this->competition_winners_table.'.competition_id');
		$this->read_db->order_by($this->competition_winners_table.'.created_at','desc');
		$this->read_db->group_by($this->competition_winners_table.'.competition_id');
		$result = $this->read_db->get($this->competition_winners_table);
		return $result->result_array();
		
	}
	
	/**
	 * Get listing of competition's winners data
	 * @input : competition_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_co_winners_log($competition_id=0) {
		
		$this->read_db->select('winner_id,member_id,competition_id,spent_points,created_at');
		$this->read_db->where('competition_id',$competition_id);
		$this->read_db->order_by('created_at','asc');
		$result = $this->read_db->get($this->competition_winners_table);
		return $result->result_array();
	}
	
	/**
	 * Get listing of competition's luckydraw data
	 * @input : competition_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_competition_luckydraw_log($competition_id=0) {
		
		$this->read_db->select('draw_id,competition_id,created_at');
		$this->read_db->where('competition_id',$competition_id);
		$this->read_db->order_by('created_at','asc');
		$result = $this->read_db->get($this->competition_luckydraw_table);
		return $result->result_array();
	}
	
	
	/**
	 * Get listing of competition's members leaderbord data
	 * @input : competition_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_member_leaderboard_log($competition_id,$competition_result_date,$co_reset_date) {
		
		// set leaderboard query
		$member_leaderboard_query = "SELECT member_id,sum(spent_points) as total_spent_points FROM ava_leaderboards WHERE competition_id = '".$competition_id."' AND ";
		if(!empty($co_reset_date)) {
			$co_reset_date = date('Y-m-d',strtotime($co_reset_date));
			$member_leaderboard_query .= " created_at >= '".$co_reset_date."' AND";
		}
		$member_leaderboard_query .= " created_at < '".$competition_result_date."'  group by member_id order by total_spent_points desc limit 20";
		// load result
		$result = $this->read_db->query($member_leaderboard_query);
		return $result->result_array();
	}
	
	/**
	 * Function to get member's information
	 * @input : int member_id
	 * @output: obj array
	 * @author: Tosif Qureshi
	 */
	public function member_info($member_id) {
		$result = $this->mssql_db->query("SELECT ACC_FIRST_NAME as member_name , ACC_EMAIL as member_email FROM ACCOUNT WHERE  ACC_NUMBER = $member_id");
		return (array) $result -> row();
	}
	
	/**
	 * Get listing of lucky draw competitions
	 * @input : competition_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_luckydraw_competition() {
		
		$this->read_db->select($this->competition_luckydraw_table.'.competition_id,'.$this->competition_luckydraw_table.'.created_at');
		$this->read_db->select($this->competitions_table.'.title,'.$this->competitions_table.'.image_1,'.$this->competitions_table.'.image_2,'.$this->competitions_table.'.image_3,'.$this->competitions_table.'.default_image_type');
		$this->read_db->where($this->competitions_table.'.type',1); // 1: lucky draw , 2: leader board
		$this->read_db->join($this->competitions_table ,$this->competitions_table.'.competition_id = '.$this->competition_luckydraw_table.'.competition_id');
		$this->read_db->order_by($this->competition_luckydraw_table.'.created_at','desc');
		$this->read_db->group_by($this->competition_luckydraw_table.'.competition_id');
		$result = $this->read_db->get($this->competition_luckydraw_table);
		return $result->result_array();
		
	}
	
	/**
	 * Function to insert lucky draw winner data
	 * @input : data
	 * @output: int(winner_id)
	 * @access: public
	 */
	public function add_luckydraw_winner($data) {
		
		$this->db->insert($this->competition_winners_table, $data);
		return $this->db->insert_id();
		
	}
	
	/**
	 * Get luckydraw winner data as per result date
	 * @input : competition_id ,competition_result_date
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_luckydraw_winner_data($competition_id=0,$competition_result_date='') {
		
		$this->read_db->select('*');
		$this->read_db->where('competition_id',$competition_id);
		$this->read_db->where('created_at',$competition_result_date);
		$result = $this->read_db->get($this->competition_winners_table);
		return $result -> row();
	}

}//end User_model
