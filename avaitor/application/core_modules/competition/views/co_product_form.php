<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

	<tr class="product-half-side" id="qty_row_<?php echo $row_id;?>">
		<td>
			<?php
			$data = array('name'=>'product_ids[]', 'value'=>'','id'=>'product_id_'.$row_id,'class'=>'product_input digits required form-control','row_id'=>$row_id,'exist_id'=>'');
			echo form_input($data);
			?>
		</td>
		<td>
			<?php
			$data = array('name'=>'product_name[]', 'value'=>'','readonly'=>true,'id'=>'product_name_'.$row_id,'class'=>'form-control','row_id'=>$row_id);
			echo form_input($data);?>
		</td>
		<td>
			<?php
			$data = array('name'=>'product_group[]', 'value'=>'','id'=>'product_group_'.$row_id,'class'=>'product_group_check form-control','qtyRow'=>$row_id,'typeCheck'=>'group');
			echo form_input($data);?>
		</td>
		<td>
			<?php
			$data = array('name'=>'product_qty[]', 'value'=>'','id'=>'product_qty_'.$row_id,'class'=>'required number form-control','min'=>"1");
			echo form_input($data);?>
		</td>
		<td>
			<?php
			$data = array('name'=>'product_points[]', 'value'=>'','id'=>'product_points_'.$row_id,'class'=>'number required product_points_check form-control','min'=>"1",'qtyRow'=>$row_id,'typeCheck'=>'point');
			echo form_input($data);?>
		</td>
		<td>
			<?php 
			/* For First Product Remove Button condition */ 
			if($row_id != 1){ ?>
				<span class="romove_row" row_id=<?php echo $row_id;?> product_img_id='' product_image=''><i class="fa fa-trash"></i></span>
			<?php
			} ?>       
		</td>
		<?php 
		$data = array('type'=>'hidden','name'=>'product_img_ids[]','value'=>'');
		echo form_input($data);?>
	</tr>


