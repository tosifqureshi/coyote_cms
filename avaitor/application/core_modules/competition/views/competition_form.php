<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<script>
// Global Variable to add Group points
	var groupA = new Array();
	var pointA = new Array();
</script>
<?php
/* To check Product is running or not for product delete condition */
$productDeleteFlag = 0;
$currentDate = date('d-m-Y');
$start_date = isset($start_date) ? $start_date : '';
$end_date = isset($end_date) ? $end_date : '';
if(!empty($start_date) && !empty($end_date)) {
	$current_timestamp = strtotime($currentDate);
	$start_date_timestamp = strtotime($start_date);
	$end_date_timestamp = strtotime($end_date);
	//if(($start_date_timestamp <= $current_timestamp) && ($current_timestamp <= $end_date_timestamp)){
	if($start_date_timestamp <= $current_timestamp){
		$productDeleteFlag = 1;
	}
}
$competition_id = isset($competition_id) ? $competition_id : '';?>

<div class="row">          
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $competition_title;?></h4>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs customtab2" role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#content_tab" role="tab"> <span class=""><?php echo lang('content');?></span></a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attribute_tab" role="tab"><span class=""><?php echo lang('attributes');?></span></a> </li>
				</ul>
				<!-- Tab panes -->
				<?php echo form_open_multipart('admin/settings/competition/create/'.$competition_id,'id="add_competition_form", class=" form-with-label"'); ?>
					<div class="tab-content">
						<div class="tab-pane active" id="content_tab" role="tabpanel">
							<div class="p-t-20">
								<div class="row">
									<div class="col-md-6">
										<!-- Title input start here -->
										<div class="form-group form-material ">
											<label for="title"><?php echo lang('co_title');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'title','id'=>'title', 'value'=> isset($title) ? $title : '' , 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('beacon_code'));
											echo form_input($data);
											?>
										</div>
										<!-- Title input end here -->
										
										<!-- Competition type radio options start here -->
										<div class=" form-group form-material ">
											<label><?php echo lang('co_type');?><i style="color:red;">*</i></label>
											<div class="demo-radio-button">
												<input type="radio" value="1" name="type" id="co_lucky_draw" <?php echo (isset($type) && $type ==2) ? '' : 'checked';?> class="with-gap radio-col-light-blue" >
												<label for="co_lucky_draw"><?php echo lang('co_lucky_draw');?></label> 
												<input type="radio" value="2" name="type" id="co_leader_board" <?php echo (isset($type) && $type ==2) ? 'checked' : '';?> class="with-gap radio-col-light-blue" >
												<label for="co_leader_board"><?php echo lang('co_leader_board');?></label>
											 </div>
										</div>
										<!-- Competition type radio options end here -->
										
										<!-- Reset days input start here -->
										<div class="form-group form-material ">
											<label for="title"><?php echo lang('co_reset_days');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'reset_days','id'=>'reset_days', 'value'=> isset($reset_days) ? $reset_days : '' , 'class'=>'form-control form-control-line required digits', 'data-toggle'=>'tooltip', 'data-placement'=>'top');
											echo form_input($data);
											?>
										</div>
										<!-- Reset days input end here -->
										
										<!-- Start / end date fields start here -->
										<div class="row form-group form-material ">
											 <div class="col-md-6">
												<label class="m-t-20"><?php echo lang('co_start_date');?><i style="color:red;">*</i></label>
												<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date">
											</div>	
										
											<div class="col-md-6">
												<label class="m-t-20"><?php echo lang('co_end_date');?><i style="color:red;">*</i></label>
												<input type="text" name="end_date" class="form-control required"  value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date">
											</div>
										</div>
										<!-- Start / end date fields end here -->
										
										<!-- Short description input start here -->
										<div class="form-group form-material ">
											<label class=""><?php echo lang('co_short_description');?></label>
											<?php
											$data	= array('name'=>'short_description', 'value'=>isset($short_description) ? $short_description : '','class'=> 'form-control form-control-line','rows'=>3);
											echo form_textarea($data);
											?>
										</div>
										<!-- Short description input end here -->
										
										<!-- Description input start here -->
										<div class="form-group html-editor">
											<label><?php echo lang('co_description');?></label>
											<?php
											$data = array('name'=>'description', 'class'=>'mceEditor redactor span8', 'value'=>isset($description) ? $description : '');
											echo form_textarea($data);?>
										</div>
										<!-- Description input end here -->
										<input type="hidden" value="<?php echo $competition_id; ?>" name='competition_id' id='competition_id'>
									</div>	
									<div class="col-md-6">
										<div class="form-group ">
											<label><?php echo lang('co_images');?></label>
											<div class="row">
												<div class="col-md-4">
													<div class="img-box">
														<?php
														// set remove image action
														$remove_img_url = 'admin/settings/competition/remove_image';
														$image_val_1 = (isset($image_1) && !empty($image_1)) ? $image_1 :'';
														$img_1_display = (isset($image_val_1) && !empty($image_val_1)) ? '' :'dn';
														if(!empty($image_val_1)) {
															$co_image_1 = base_url('uploads/competition_images/'.$image_1);
															$title = lang('tooltip_offer_image');
															$co_image_1_doc = $this->config->item('document_path').'uploads/competition_images/'.$image_1; 
															 if(!file_exists($co_image_1_doc)){
																$promo_image_1 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															 }
														} else {
															$title = lang('co_tooltip_image');
															$co_image_1 = base_url('uploads/upload.png');
														} ?>
														<a id="remove_btn_1" class="<?php echo $img_1_display;?>"  href="javascript:void(0);" onclick="removeImage(1,'<?php echo $competition_id;?>','<?php echo $remove_img_url;?>','<?php echo $image_val_1;?>')">×</a>
														<div class="cell-img" >
															<img id="co_image-picker_1" src="<?php echo $co_image_1; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
														</div>
														<?php
														$data	= array('name'=>'image_1', 'id'=>'image_1', 'value'=>$image_val_1,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_image_co(this)');
														echo form_upload($data);
														$data	= array('name'=>'default_image_type', 'value'=>'1','id'=>'is_default_image_1','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image')); // fist image is alway the default image
														echo form_radio($data);?>
														<label for="is_default_image_1"><?php echo lang('co_default_image');?></label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="img-box">
														<?php
														$image_val_2 = (isset($image_2) && !empty($image_2)) ? $image_2 :'';
														$img_2_display = (isset($image_2) && !empty($image_2)) ? '' :'dn';
														if(!empty($image_val_2)) {
														$co_image_2 = base_url('uploads/competition_images/'.$image_2);
														$title = lang('tooltip_offer_image');
															$co_image_2_doc = $this->config->item('document_path').'uploads/competition_images/'.$image_2; 
															 if(!file_exists($co_image_2_doc)){
																$co_image_2 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															 }
														} else {
															$title = lang('tooltip_offer_image');
															$co_image_2 = base_url('uploads/upload.png');
														}
														?>
														<a id="remove_btn_2" class="<?php echo $img_2_display;?>"  href="javascript:void(0);" onclick="removeImage(2,'<?php echo $competition_id;?>','<?php echo $remove_img_url;?>','<?php echo $image_val_2;?>')">×</a>
														<div class="cell-img" ><img id="co_image-picker_2" src="<?php echo $co_image_2; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
														<?php
														$data	= array('name'=>'image_2', 'id'=>'image_2', 'value'=>$image_val_2,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image_co(this)');
														echo form_upload($data);
														if(isset($default_image_type) && $default_image_type == 2) { 
															$data	= array('name'=>'default_image_type', 'value'=>'2','id'=>'is_default_image_2','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
														} else {
															$data	= array('name'=>'default_image_type', 'value'=>'2','id'=>'is_default_image_2','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
														}
														echo form_radio($data);?>
														<label for="is_default_image_2"><?php echo lang('co_default_image');?></label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="img-box">
														<?php
														$image_val_3 = (isset($image_3) && !empty($image_3)) ? $image_3 :'';
														$img_3_display = (isset($image_3) && !empty($image_3)) ? '' :'dn';
														if(!empty($image_val_3)) {
														$co_image_3 = base_url('uploads/competition_images/'.$image_3);
														$title = lang('tooltip_offer_image');
															$co_image_3_doc = $this->config->item('document_path').'uploads/competition_images/'.$image_3; 
															 if(!file_exists($co_image_3_doc)){
																$co_image_3 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															 }
														} else {
															$title = lang('tooltip_offer_image');
															$co_image_3 = base_url('uploads/upload.png');
														}
														?>
														<a id="remove_btn_3" class="<?php echo $img_3_display;?>"  href="javascript:void(0);" onclick="removeImage(3,'<?php echo $competition_id;?>','<?php echo $remove_img_url;?>','<?php echo $image_val_3;?>')">×</a>
														<div class="cell-img" ><img id="co_image-picker_3" src="<?php echo $co_image_3; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
														<?php
														$data	= array('name'=>'image_3', 'id'=>'image_3', 'value'=>$image_val_3,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image_co(this)');
														echo form_upload($data);
														
														if(isset($default_image_type) && $default_image_type == 3) { 
															$data	= array('name'=>'default_image_type', 'value'=>'3','id'=>'is_default_image_3','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
														} else {
															$data	= array('name'=>'default_image_type', 'value'=>'3','id'=>'is_default_image_3','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
														}
														echo form_radio($data);	?>
														<label for="is_default_image_3"><?php echo lang('co_default_image');?></label>
													</div>
												</div>	
												<div class="alert red-alert alert-danger alert-dismissible ">
													<i class="icon fa fa-warning"></i>
													Upload up to 3MB images. 
												</div>	
											</div>	
										</div>	
									</div>	
								</div>	
							</div>	
						</div>	
						<div class="tab-pane p-20" id="attribute_tab" role="tabpanel">
							<!-- Add product button -->
							<button onclick="add_row()" class="btn waves-effect waves-light btn-info add-product-btn float-right"  type="button">Add Product</button>	
							<div class="table-responsive">
								<table class="table table-bordered th-border" id="product_tables">
									<thead>
										<tr>
											<th class='product_table_th'><?php echo lang('co_product_number');?></th>
											<th class='product_table_th'><?php echo lang('co_product_name');?></th>
											<th class='product_table_th'><?php echo lang('co_product_group');?></th>
											<th class='product_table_th'><?php echo lang('co_product_qty');?></th>
											<th class='product_table_th'><?php echo lang('co_product_points');?></th>
											<th class='width45'></th>
										</tr>
									</thead>
									<tbody>
										<?php
										$qty_row = 1;
										if(!empty($product_images) && count($product_images) > 0) {
											foreach($product_images as $product_image) {
												$data = array('type'=>'hidden','name'=>'product_img_ids[]','value'=>$product_image['product_img_id']);
												echo form_input($data); ?>
												<tr class="product-half-side" id="qty_row_<?php echo $qty_row;?>">
													<td>
														<?php
														$data = array('name'=>'product_ids[]', 'value'=>$product_image['product_id'],'id'=>'product_id_'.$qty_row,'class'=>'product_input digits required form-control','row_id'=>$qty_row,'exist_id'=>$product_image['product_id']);
														echo form_input($data);
														?>
													</td>
													<td>
														<?php
														$data = array('name'=>'product_name[]', 'value'=>$product_image['product_name'],'readonly'=>true,'id'=>'product_name_'.$qty_row,'class'=>'form-control','row_id'=>$qty_row);
														echo form_input($data);?>
													</td>
													<td>
														<?php
														$data = array('name'=>'product_group[]','value'=>$product_image['product_group'],'id'=>'product_group_'.$qty_row,'class'=>'product_group_check form-control','qtyRow'=>$qty_row,'typeCheck'=>'group');
														echo form_input($data);?>
													</td>
													<td>
														<?php
														$data = array('name'=>'product_qty[]', 'value'=>$product_image['product_qty'],'id'=>'product_qty_'.$qty_row,'class'=>'digits required form-control');
														echo form_input($data);?>
													</td>
													<td>
														<?php
														$data = array('name'=>'product_points[]', 'value'=>$product_image['product_point'],'id'=>'product_points_'.$qty_row,'class'=>'digits required product_points_check form-control','qtyRow'=>$qty_row,'typeCheck'=>'point');
														echo form_input($data);?>
													</td>
													<td>
														<!--  && $productDeleteFlag != 1  -->
														<?php //if($qty_row != 1) { ?>
															<span class="romove_row" row_id=<?php echo $qty_row;?> product_img_id='<?php echo encode($product_image['product_img_id']);?>' product_image='<?php //echo $product_image_val;?>'><i class="fa fa-trash"></i></span>
														<?php //} ?>
													</td>
													<?php 
													if(!empty($product_image['product_group']) && !empty($product_image['product_point']) ){
													?>
													<script>
														var groupText = '';
														// Global Variable to add Group points
														groupText = '<?php echo $product_image['product_group']; ?>';
														groupA[<?php echo $product_image['product_qty']; ?>] = '<?php echo $product_image['product_group']; ?>';
														pointA[groupText] ='<?php echo $product_image['product_point']; ?>';
													</script>
													<?php
													} ?>				
												</tr>
												<?php
												$qty_row++;
											}
										} else {
											// render blank product entities 
											echo $co_product_form;
											$qty_row++;
										} ?>
									</tbody>	
								</table>
								<input type="hidden" value="<?php echo $qty_row;?>" name='last_qty_row' id='last_qty_row'>
								<!-- Product fields end here --> 
							</div>	
						</div>	
						<div class="row pb-3 float-right">
							<?php
							// set current date in hidden field 
							$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
							echo form_input($data);?>
							<div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/competition' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>	
					</div>	
				<?php echo form_close();?>
			</div>	
		</div>	
	</div>	
</div>	

<script type="text/javascript">
	// Initialize material Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});

	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
		
	$(document).ready(function() {
		$("#add_competition_form").validate({
			
		});
	});
	
	$( "#add_competition_form" ).submit(function( event ) {
		
		// get reset days
		var reset_days = $('#reset_days').val();
		// set start date for calculate day diffrence
		var start_date	=  $( "#start_date" ).val().split(' ');
		var startdateSplit = start_date[0].split('-');
		var new_start_date = startdateSplit[1] + '/' + startdateSplit[0] + '/' + startdateSplit[2];
		// set end date for calculate day diffrence
		var end_date	 =  $( "#end_date" ).val().split(' ');
		var enddateSplit = end_date[0].split('-');
		var new_end_date = enddateSplit[1] + '/' + enddateSplit[0] + '/' + enddateSplit[2];
		// get diffrence between start and end dates
		var day_diff = daydiff(parseDate(new_start_date), parseDate(new_end_date));
		// show error if reset day diffrence less than date range
		if(day_diff < reset_days ) {
			bootbox.alert("Reset days must be less than competition date range.");
			return false;
		}
		
		//return false;
		var is_validate = true;
		$( ".product_input" ).each(function( e ) {
			var row_id = $(this).attr('row_id');
			var product_id = $('#product_id_'+row_id).val();
			var product_qty = $('#product_qty_'+row_id).val();
			var product_points = $('#product_points_'+row_id).val();
			
			if(product_id == '') {
				$('#product_id_'+row_id).addClass('error');
				is_validate =false;
			}
			if(product_qty == '') {
				$('#product_qty_'+row_id).addClass('error');
				is_validate =false;
			}
			if(product_points == '') {
				$('#product_points_'+row_id).addClass('error');
				is_validate =false;
			}
		});
		
		if(is_validate == false) {
			bootbox.alert("Please fill all required product attributes.");
			return false;
		}
		return;
		event.preventDefault();
	});
	
	/* 
	| -----------------------------------------------------
	| Function used to remove image
	| -----------------------------------------------------
	*/
	$(document).delegate('.product_input','blur',function(e){
		var product_id = $(this).val();
		var row_id = $(this).attr('row_id');
		var exist_id = $(this).attr('exist_id');
		get_product_data(product_id,row_id,exist_id);
	});
	
	function get_product_data(product_id,row_id,exist_id) {
		
        /*
		var product_ids = $("input[name='product_ids[]']")
              .map(function(){return $(this).val();}).get();
            
		var lastElementIndex = product_ids.length-1; // Gets last element index
		product_ids.splice(lastElementIndex, 1); // Removes last array element
		*/
		
        var product_ids = new Array();
        var currentRowId = '';
        var j =0;
        $.each( $( ".product_input" ), function() {
            currentRowId = $(this).attr('row_id');
            if(row_id != currentRowId){
                product_ids[j] = $(this).val();
            j++;
            }
        });
        
        
		// check existing product id
		if(exist_id == product_id) {
			return false;
		} else if(exist_id != '' && exist_id == product_id) {
			bootbox.alert("Product already associated with this competition.");
			$('#product_id_'+row_id).val(exist_id);
			return false;
		} 
		
		if ($.inArray(product_id, product_ids) > -1) {
			bootbox.alert("Product already associated with this competition.");
			$('#qty_row_'+row_id).html('');
			return false;
		} else {
			$.ajax ({
				type: "POST",
				dataType:'jSon',
				data : {product_id:product_id},
				url: "<?php echo base_url() ?>admin/settings/competition/getproductdata",
				success: function(data){
					if(data.product_name != '') {
						$('#product_name_'+row_id).val(data.product_name);
					} else {
						$('#product_name_'+row_id).val('');
						$('#product_id_'+row_id).val('');
					}
				}
			});
		}
	}
	
	function read_url_product_image(input) {
		
		var pickerId = '#picker_'+input.id;
		var fieldId	= '#'+input.id;
		$("#remove_"+input.id).show();
		$('#image_val_'+input.id).val(input.value);
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	$(document).delegate('.product_img_picker','click',function(e){
		var row_id = $(this).attr('row_id');
		$("input[id='p_image_"+row_id+"']").click();
	});
	
	/* 
	| -----------------------------------------------------
	| Manage add quantity row 
	| -----------------------------------------------------
	*/ 
	function add_row() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
		last_row = parseInt(last_row)+1;
		$('#last_qty_row').val(last_row);
		$('#loader1').fadeIn();
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {row_id:last_row},
			url: "<?php echo base_url() ?>admin/settings/competition/product_form",
			success: function(data){
				$('#loader1').fadeOut();
				if(data.status) {
					$('#product_tables tr:last').after(data.product_form)
					// prepare qunatity row html
					//$("#multiple_qty").append(data.product_form);
				}				
			}
		});
	}
	
	/* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".romove_row","click",function(e){
		var row_id = $(this).attr('row_id');
		var product_img_id = $(this).attr('product_img_id');
		var product_image = $(this).attr('product_image');
		var rowCount = $('#product_tables >tbody >tr').length;
		if(rowCount == 1) {
			bootbox.alert("Remove is not possible in case of single product!");
			return false;
		}
		
		if(product_img_id != undefined && product_img_id != '') {
			bootbox.confirm("Are you sure want to delete this?", function(result) {
				if(result==true) {
					$.ajax ({
						type: "POST",
						dataType:'jSon',
						data : {product_img_id:product_img_id,product_image:product_image},
						url: "<?php echo base_url() ?>admin/settings/competition/remove_competition_products",
						success: function(data){
							if(data.status) {
								// hide the selected row
								//$('#qty_row_'+row_id).html('');
								$('#qty_row_'+row_id).remove();
							}
						}
					});
				}
			});
		} else {
			// hide the selected row
			$('#qty_row_'+row_id).html('');
		}
	});
	
	
	/* 
	| -----------------------------------------------------
	| Get day diffrance betweem start and end date
	| -----------------------------------------------------
	*/
	function parseDate(str) {
		var mdy = str.split('/');
		return new Date(mdy[2], mdy[0]-1, mdy[1]);
	}

	function daydiff(first, second) {
		return (second-first)/(1000*60*60*24);
	}
	
	
		/* Amit Neema */
	/* 
	| -----------------------------------------------------
	| To Check Group entry with respect to Points
	| -----------------------------------------------------
	*/
	// Global Variable to add Group points
	//var groupA = new Array();
	//var pointA = new Array();
	$(document).ready(function() {
		$(document).on('change',".product_group_check,.product_points_check",function(){
			var currentThis = $(this);
			var val = currentThis.val(); 
			var qtyRow = currentThis.attr('qtyRow'); 
			var typeCheck = currentThis.attr('typeCheck'); 
			if(val != ""){
				if(typeCheck == 'point'){
                   var grp = $('#product_group_'+qtyRow).val();
                   var productnameCount = 0; // product name count
                    $( ".product_group_check" ).each(function() {    
                        if($(this).val() == grp){
                            productnameCount++; // To update first time
                        }
                    });
                    var checkProductG = 0;
                    var qtyRowC = 0;
                    $( ".product_group_check" ).each(function() {    
                        if($(this).val() == grp){
                            qtyRowC = $(this).attr('qtyRow'); 
                            if($('#product_points_'+qtyRowC).val() != ''){
                                checkProductG++;
                            }
                        }
                    });
                    if(checkProductG == 1){
                        productnameCount = 1;
                    }
                    
                    if(grp != ''){
						var index = groupA.indexOf(grp); 
                        if(index <= 0 || (productnameCount == 1) ){
							groupA[qtyRow] = grp;
							pointA[grp] = val; 	
						}else{ 
							if(pointA[grp] != "" && (productnameCount > 1)){  // Insert old Value;
                                bootbox.alert("Group value already defined.");
                                $('#product_points_'+qtyRow).val(pointA[grp]);	
							}else{ // Insert Current Value;
								$('#product_points_'+qtyRow).val(val);	
							}
						}
					}
				}else{
					var point = $('#product_points_'+qtyRow).val();
					var index = groupA.indexOf(val); 
					if(index <= 0){
						groupA[qtyRow] = val;
						pointA[val] = point; 	
					}else{
						if(pointA[grp] != ""){
							$('#product_points_'+qtyRow).val(pointA[val]);	
						}
					}
					
				}
			}
		});  
    });
</script>


