<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="span9 dash-board">
	<div class="well-white">
		<div class="page-header">
			<h1><?php echo $competition_title;?></h1>
			<a href="<?php echo site_url("admin/settings/competition"); ?>" role="button" class="btn btn-primary pull-right"><?php echo lang('competitions'); ?></a>
		</div>
		<div>
<?php			
if (isset($message) && !empty($message)){
	echo '<div class="alert alert-success">' . $message	 . '</div>';
} ?>
	<div role="grid" class="dataTables_wrapper" id="dyntable_wrapper">
		<table id="competition_member_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">                
			<thead>
				<tr role="row">
					<th><?php echo lang('co_sno');?></th>
					<th><?php echo lang('co_member_id');?></th>
					<th><?php echo lang('co_member_name'); ?></th>
					<th><?php echo lang('co_member_spent_points'); ?></th>
				</tr>
			</thead>
						
			<tbody role="alert" aria-live="polite" aria-relevant="all">
				<?php
				$i=1;
				if(isset($competition_members) && is_array($competition_members) && count($competition_members)>0):
					foreach($competition_members as $member_txn) : ?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo $member_txn['member_id'];?></td>
							<td><?php echo get_user_name($member_txn['member_id']);?></td>
							<td><?php echo $member_txn['total_spent_points'];?></td>
						</tr>
					<?php 
					$i++;
					endforeach ; endif; ?>
				</tbody>
			</table>                 
		</div>
	</div>
</div>
					
<script>
	jQuery('#competition_member_table').dataTable({
		"aoColumnDefs": [{ "targets": 'no-sort',"bSortable": false}],
		"bJQueryUI": true,
		"aaSorting": [[ 0, "asc" ]],
		"sPaginationType": "full_numbers"
	});
</script>
