<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo $co_header;?></h4>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="competition_member_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('co_sno'); ?></th>
								<th class="no-sort"><?php echo lang('co_image'); ?></th>
								<th><?php echo lang('co_name'); ?></th>
								<th><?php echo lang('co_created_at'); ?></th>
							</tr>
						</thead>
									
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($competition_winners) && is_array($competition_winners) && count($competition_winners)>0):
								foreach($competition_winners as $winner) :
									// set encoded competition id
									$competition_id = encode($winner['competition_id']);
									if(isset($leaderboard) && !empty($leaderboard)) :
										// set leaderboard detail page url 
										$co_detail_url = site_url("admin/settings/competition/leaderboarddetails/".$competition_id);
									else :
										// set luckydraw detail page url 
										$co_detail_url = site_url("admin/settings/competition/luckydrawdetails/".$competition_id);
									endif;?>
									<tr class="crp" onclick="window.location.href= '<?php echo site_url("admin/settings/competition/leaderboarddetails/".$competition_id);?>'">
										<td><?php echo $i;?></td>
										<td>
											<?php if(isset($winner['default_image_type']) && !empty($winner['default_image_type'])) {
												$co_image = $winner['image_'.$winner['default_image_type']] ;
												$co_image = (!empty($co_image)) ? base_url('uploads/competition_images/'.$co_image) : base_url('uploads/No_Image_Available.png');?>
												<img  id="imageresource" src="<?php echo $co_image;?>" width="110" height="90" title="<?php echo $winner['title'];?>">
											<?php } ?>
										</td>
										<td><?php echo $winner['title'];?></td>
										<td><?php echo date('F j, Y',strtotime($winner['created_at']));?></td>
									</tr>
								<?php 
								$i++;
								endforeach ; 
							endif; ?>
						</tbody>	
					</table>
				</div>		
			</div>
		</div>
	</div>
</div>
					
<script>
	$(document).ready(function() {
		$('#competition_member_table').DataTable();
	});
</script>
