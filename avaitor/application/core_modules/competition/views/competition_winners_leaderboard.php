<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="page-header">
    <h1><?php echo lang('co_leaderboard_heading_'.$co_type);?></h1>
</div>
<div class="tabbable ">
	<ul class="nav nav-tabs">
		<li class="active">&nbsp;</li>
		<?php 
		foreach($winning_leaderboard_result as $key=>$data) {
			$active_class = ($key == 0) ? 'active' : ''; ?>
			<li class="<?php echo $active_class;?>"><a href="#content_tab<?php echo $key;?>" data-toggle="tab"><?php echo date('Y-m-d',strtotime($data['winning_result_date']));?></a></li>
		<?php } ?>
	</ul>
	
	<div class="tab-content scroll ">
		<?php
		if($co_type == 1) {
			// lucky draw members listing
			foreach($winning_leaderboard_result as $key=>$data) {
				
				$active_class = ($key == 0) ? 'active' : '';
				
				?>
				<div class="tab-pane <?php echo $active_class;?>" id="content_tab<?php echo $key;?>">
					<?php 
					if(!isset($data['luckydraw_winner'])) {
						$leaderboard_members = $data['leaderboard_members'];
						// convert array to json object
						$leaderboards['members'] = $leaderboard_members;
						$leaderboards_json = json_encode($leaderboards);
						
						if(!empty($leaderboard_members) && count($leaderboard_members) > 0) { ?>
							<!-- Pick a winner section start here-->
							<button type="button" class="btn default" onclick='pick_member(<?php echo $leaderboards_json;?>,<?php echo $key;?>,"<?php echo $data['winning_result_date']?>")'><?php echo 'Pick a Winner';?></button>
							<div class="dn luckydraw_memberbox" id="co_draw_winner_table_<?php echo $key;?>"></div>
							<!-- Pick a winner section end here-->
						<?php } else {
							echo 'No purchase record found.';
						}
					} else { ?>
						<table class="co_winner_table">
							<tr>
								<th><?php echo lang('co_winner_id');?></th>
								<th><?php echo lang('co_winner_name');?></th>
							</tr>
							<tr>
								<td><?php echo $data['member_id'];?></td>
								<td><?php echo $data['member_name'];?></td>
							</tr>
						</table>
					<?php 
					} ?>
				</div>
			<?php
			}
		} else {
			// leaderboard members listing
			foreach($winning_leaderboard_result as $key=>$data) {
				$leaderboard_members = $data['leaderboard_members'];
				$active_class = ($key == 0) ? 'active' : ''; ?>
				<div class="tab-pane <?php echo $active_class;?>" id="content_tab<?php echo $key;?>">
					<?php if(!empty($leaderboard_members)) { ?>
						<table class="co_winner_table">
							<tr>
								<th><?php echo lang('co_member_id');?></th>
								<th><?php echo lang('co_member_name');?></th>
								<th><?php echo lang('co_member_spent_points');?></th>
							</tr>
						<?php 
						foreach($leaderboard_members as $members) { ?>
							<tr>
								<td><?php echo $members['member_id'];?></td>
								<td><?php echo $members['member_name'];?></td>
								<td><?php echo $members['total_spent_points'];?></td>
							</tr>
						<?php } ?>
						</table>
					<?php } else {
						echo 'No purchase record found.';
					}?>
				</div>
			<?php } 
		} ?>
	</div>	
</div>
<script>
   /**
	* Function used to manage random member pickup process
	*/
	function pick_member(leaderboards_json,key,draw_date) {
		// pick random member data
		var random_winner = leaderboards_json.members[Math.floor(Math.random() * leaderboards_json.members.length)];
		var co_draw_winner_html = '';
		co_draw_winner_html += '<table class="co_winner_table"><tbody><tr><th>Member Number</th><th>Member Name</th></tr>';
		if(random_winner != '') {
			co_draw_winner_html += '<tr><td>'+random_winner.member_id+'</td><td>'+random_winner.member_name+'</td></tr>';
		}
		co_draw_winner_html += '</tbody></table><button type="button" class="btn btn-primary fr" onclick="set_winner(\''+random_winner.member_id+'\',\''+draw_date+'\')">Set as Winner</button>';
		$('#co_draw_winner_table_'+key).show().html(co_draw_winner_html);
		//console.log(random_winner);
	}
	
	function set_winner(member_id,draw_date) {
		if(member_id != '' && draw_date != '') {
			var competition_id = '<?php echo $competition_id;?>';
			bootbox.confirm("Are you sure want to set this member as winner?", function(result) {
				if(result==true) {
					$.ajax ({
						type: "POST",
						dataType:'jSon',
						data : {member_id:member_id,draw_date:draw_date,competition_id:competition_id},
						url: "<?php echo base_url() ?>admin/settings/competition/set_luckydraw_winner",
						success: function(data) {
							if(data.status) {
								window.location.href = window.location.href;
							} else {
								bootbox.alert("Error occured during winner insertion");
							}
						}
					});
				}
			});
		}
	}
</script>
