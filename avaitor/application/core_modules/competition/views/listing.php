<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('competitions') ?></h4>
				<a href="<?php echo site_url("admin/settings/competition/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_competition'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="competition_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('co_sno'); ?></th>
								<th class="no-sort"><?php echo lang('co_image'); ?></th>
								<th><?php echo lang('co_title'); ?></th>
								<th class="no-sort"><?php echo lang('type'); ?></th>
								<th><?php echo lang('co_created_at'); ?></th>
								<th><?php echo lang('co_status'); ?></th>
								<th class="no-sort"><?php echo lang('co_delete'); ?></th>
								<!--<th class="no-sort view-btn"><?php //echo lang('co_view_members'); ?></th>-->
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($competitions) && is_array($competitions) && count($competitions)>0):
							foreach($competitions as $competition) :
								// set encoded competition id
								$competition_id = encode($competition['competition_id']); ?>
								<tr>
									<td><?php echo $i;?></td>
									<td>
										<?php if(isset($competition['default_image_type']) && !empty($competition['default_image_type'])) {
											$co_image = $competition['image_'.$competition['default_image_type']] ;
											$co_image = (!empty($co_image)) ? base_url('uploads/competition_images/'.$co_image) : base_url('uploads/No_Image_Available.png');?>
											<img  id="imageresource" src="<?php echo $co_image;?>" width="110" height="90" title="<?php echo $competition['title'];?>">
										<?php } ?>
									</td>
									<td class="mytooltip tooltip-effect-1 desc-tooltip">
										<span class="tooltip-content clearfix">
											<span class="tooltip-text">
												<?php 
												$start_date_time = date('Y-m-d',strtotime($competition['start_date']));
												$end_date_time = date('Y-m-d',strtotime($competition['end_date'].' +1 days'));
												$diff =  get_date_diffrence($start_date_time, $end_date_time);
												echo '<b>'.lang('co_title').' : </b>'.$competition['title'] ;
												echo '<br>';
												echo '<b>'.lang('co_start_date').' : </b>'.date('F j, Y',strtotime($competition['start_date'])) ;
												echo '<br>';
												echo '<b>'.lang('co_end_date').'  : </b>'.date('F j, Y',strtotime($competition['end_date'])) ;
												echo '<br>';
												echo $diff;
												?>
											</span> 
										</span>
										<span class="text-ttip">
											<a href="<?php echo site_url("admin/settings/competition/create/".$competition_id);?>"><?php echo (strlen($competition['title']) > 20) ? substr($competition['title'],0,20).'...' :  $competition['title'];?></a>
										</span>
									</td>
									<td><?php echo ($competition['type'] == 1) ? 'Lucky draw' : 'Leader board';?></td>
									<td><?php echo date('F j, Y',strtotime($competition['created_at']));?></td>
									<td>
										<?php
										// set change status url
										$status_url = site_url("admin/settings/competition/change_status/".$competition_id.DIRECTORY_SEPARATOR.encode($competition['status']));
										if($competition['status'] == '0') { ?>
											<div style="display:none"><?php echo $competition['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
											<?php
										} else {
											?>
											<div style="display:none"><?php echo $competition['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
										} ?>
									</td>
									<td>
										<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/competition/delete/".$competition_id);?>')"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							<?php 
							$i++;
							endforeach ; endif; ?>
						</tbody>
					</table>
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

<!----------- Start competition members listing ------------->
<div id="modal_members_list"></div>


<script>
	$(document).ready(function() {
		$('#competition_table').DataTable();
	});
	
	/* 
	| ------------------------------------------------------------
	| Show competition members listing on popup 
	| ------------------------------------------------------------
	*/ 
	function competition_members(competition_id) {
		$.ajax ({
			type: "POST",
			data : {competition_id:competition_id},
			url: BASEURL+'admin/settings/competition/competition_members',
			async: false,
			success: function(data) {
				if(data) {
					$('#modal_members_list').html(data);
					$('#competitionmodal').modal('show');
				}
			}, 
			error: function(error){
				bootbox.alert(error);
			}
		});	
	}
</script>

