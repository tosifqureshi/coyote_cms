<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Deviceaddress Controller
 *
 * Manages the device address functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Deviceaddress
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('deviceaddress');
		$this->load->library('form_validation');
		$this->load->model('deviceaddress/Deviceaddress_model', 'deviceaddress_model');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the devices address listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
	
		// get listing
		$deviceaddress = $this->deviceaddress_model->get_devices();
		Template::set('deviceaddress',$deviceaddress);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage competition form
	 * @input : competition_id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode competition id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_deviceaddress()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/deviceaddress');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_deviceaddress($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/deviceaddress');
			}
		}
		$data['deviceaddress_title']	= lang('add_deviceaddress');
		if($id) {
			$deviceaddress = $this->deviceaddress_model->get_devices($id);	
			if(empty($deviceaddress)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/deviceaddress');
			}
			$data['deviceaddress_title']	= lang('edit_deviceaddress');
			$data['id']	= encode($id);
			$data['model_name']	   = $deviceaddress->model_name;		
			$data['model_number']  = $deviceaddress->model_number;		
			$data['description']   = $deviceaddress->description;	
			$data['imei_number']   = $deviceaddress->imei_number;
			$data['mac_number']	   = $deviceaddress->mac_number;	
			$data['status']		   = $deviceaddress->status;
			$data['created_at']	   = $deviceaddress->created_at;	
		}
		
		Template::set('data',$data);
		Template::set_view('deviceaddress_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store device address in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_deviceaddress($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
			// get competition details
			$deviceaddress = $this->deviceaddress_model->get_devices($id);
		}
		// set device type
		$device_type = $this->input->post('device_type');
		$imei_number = $this->input->post('imei_number');
		// apply validation rules for input params	
		$this->form_validation->set_rules('device_type', 'lang:device_type', 'trim|required');
		$this->form_validation->set_rules('mac_number', 'lang:mac_number', 'trim|required');
		if($device_type == 2) {
			$this->form_validation->set_rules('imei_number', 'lang:imei_number', 'trim|required');		
		}
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		} 
		
		// set post values in store bundle
		$data['model_name']    = $this->input->post('model_name');
		$data['model_number']  = $this->input->post('model_number');
		$data['device_type']   = $this->input->post('device_type');
		$data['description']   = $this->input->post('description');
		$data['mac_number']    = $this->input->post('mac_number');
		$data['imei_number']   = ($device_type == 2) ? $this->input->post('imei_number') : '';
		$data['created_by']    = $this -> session -> userdata('user_id');
		
		// add or update device data into db
		$id = $this->deviceaddress_model->save_device_data($data);
		return $id;
	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Update status of device address
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->deviceaddress_model->save_device_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/deviceaddress');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove device address
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$id = decode($id);
		$id = $this->deviceaddress_model->remove_deviceaddress($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/deviceaddress');
	}

}//end Settings

// End of Admin Device address Controller
/* Location: ./application/core_modules/deviceaddress/controllers/settings.php */
