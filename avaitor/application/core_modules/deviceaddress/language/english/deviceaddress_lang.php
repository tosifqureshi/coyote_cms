<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['deviceaddress']		= 'Device Management';
$lang['add_deviceaddress']	= 'Add New Device';
$lang['edit_deviceaddress']	= 'Edit Device';
$lang['sno']				= 'S.No.';
$lang['model_name']			= 'Model Name';
$lang['model_number']		= 'Model Number';
$lang['device_type']		= 'Device Type';
$lang['tab_wifi']			= 'Wi-Fi  ';
$lang['tab_wifi_gsm']		= 'Wi-Fi & GSM  ';
$lang['imei_number']		= 'IMEI Number';
$lang['mac_number']			= 'MAC Address';
$lang['description']		= 'Description';
$lang['created_at']			= 'Created At';
$lang['status']				= 'Status';
$lang['delete']				= 'Delete';
$lang['insert_msg']			= 'Successfully inserted';
$lang['update_msg']			= 'Successfully updated';
$lang['update_status']		= 'successfully change status';
$lang['delete_msg']			= 'Successfully removed';
$lang['please_fill_address']= 'Please fill at least IMEI or MAC number detail';
$lang['form_save']			= 'Save';
$lang['form_cancel']		= 'Cancel';
$lang['tab_wifi_note']		= 'Use Mac address for tablet Wi-Fi compatibility';
$lang['tab_wifi_gsm_note']  = 'Use Mac and IMEI address for tablet Wi-Fi & GSM compatibility';

