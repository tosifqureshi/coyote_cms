<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Deviceaddress Model
 *
 * The central way to access and perform CRUD on Deviceaddress.
 *
 * @package    Avaitor
 * @subpackage Modules_Deviceaddress
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Deviceaddress_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        // define tables
		$this->device_address_log_table = 'device_address_log';
    }

	/**
	 * Get listing of device address log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_devices($id = 0) {
		
		$this -> read_db -> select('*');
		if ($id) {
			$this -> read_db -> where("id", $id);
			$result = $this -> read_db -> get($this->device_address_log_table);
			return $result -> row();
		}
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->device_address_log_table);
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation of device data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_device_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->device_address_log_table, $data);
			return $data['id'];
		} else { // add data
			$this->read_db->insert($this->device_address_log_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Function to remove device address entry from db
	 * @input : id
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	public function remove_deviceaddress($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->device_address_log_table); 
			$return = true;
		}
		return $return;
	}
	

}//end User_model
