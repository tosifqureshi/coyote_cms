<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $deviceaddress_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					$id = isset($id) ? $id : '';
					echo form_open_multipart('admin/settings/deviceaddress/create/'.$id,'id="add_deviceaddress_form" class="form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Model name input start here -->
								<div class="form-group ">
									<label for="model_name"><?php echo lang('model_name');?></label>
									<?php
									$data = array('name'=>'model_name','id'=>'model_name', 'value'=> isset($model_name) ? $model_name : '' , 'class'=>'form-control form-control-line');
									echo form_input($data);
									?>
								</div>
								<!-- Model name input end here -->
								
								<!-- Model number input start here -->
								<div class="form-group ">
									<label for="model_number"><?php echo lang('model_number');?></label>
									<?php
									$data = array('name'=>'model_number','id'=>'model_number', 'value'=> isset($model_number) ? $model_number : '' , 'class'=>'form-control form-control-line');
									echo form_input($data);
									?>
								</div>
								<!-- Model number input end here -->
								
								<!-- Divice type radio options start here -->
								<div class=" form-group">
									<label><?php echo lang('device_type');?></label>
									<div class="demo-radio-button">
										<input name="device_type" id="tab_wifi" type="radio" value="1" class="device_type with-gap radio-col-light-blue" <?php echo (isset($device_type) && $device_type ==2) ? '' : 'checked';?> >
										<label for="tab_wifi"><?php echo lang('tab_wifi');?><span class="fa fa-info-circle" title="<?php echo lang('tab_wifi_note');?>"></span></label> 
										
										<input type="radio" id="tab_wifi_gsm" name="device_type" value="2" class="device_type with-gap radio-col-light-blue" <?php echo (isset($device_type) && $device_type ==2) ? 'checked' : '';?> >
										<label for="tab_wifi_gsm"><?php echo lang('app_type_2');?><span class="fa fa-info-circle" title="<?php echo lang('tab_wifi_gsm_note');?>"></span></label>
									 </div>
								</div>
								<!-- Divice type radio options end here -->
								
								<!-- MAC address input start here -->
								<div class="form-group " id="mac_number_div">
									<label for="mac_number"><?php echo lang('mac_number');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'mac_number','id'=>'mac_number', 'value'=> isset($mac_number) ? $mac_number : '' , 'class'=>'form-control form-control-line required');
									echo form_input($data);?>
								</div>
								<!-- MAC address input end here -->
								
								<!-- IMEI number input start here -->
								<?php $dn = (isset($device_type) && $device_type == 2) ? '' : 'dn';?>
								<div class="form-group <?php echo $dn;?>" id="imei_number_div">
									<label for="imei_number"><?php echo lang('imei_number');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'imei_number','id'=>'imei_number', 'value'=> isset($imei_number) ? $imei_number : '' , 'class'=>'form-control form-control-line required');
									echo form_input($data);
									?>
								</div>
								<!-- IMEI number input end here -->
								
								<!-- Long description start here -->	
								<div class="form-group html-editor">
									<label><?php echo lang('description');?></label>
									<?php
									$data = array('name'=>'description', 'class'=>'mceEditor redactor form-control', 'value'=>isset($description) ? $description : '');
									echo form_textarea($data);?>
								</div>
								<!-- Long description end here -->
							</div>
						</div>		
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/deviceaddress' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
						<?php
						// set current date in hidden field 
						$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
						echo form_input($data);?>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>					

<script type="text/javascript">
	$(document).ready(function() {
		$("#add_deviceaddress_form").validate({
			
		});
	});
	
	$( "#add_deviceaddress_form" ).submit(function( event ) {
		// get imei number
		var imei_number = $('#imei_number').val();
		// get mac address
		var mac_number = $('#mac_number').val();
		/*var regex = /[0-9a-f]{2}[:-][0-9a-f]{2}[:-][0-9a-f]{2}[:-][0-9a-f]{2}[:-][0-9a-f]{2}[:-][0-9a-f]{2}/;
		var is_valid_mac = regex.test(mac_number);
		if(is_valid_mac == false) {
			bootbox.alert("Please use valid Mac Address");
			return false;
		}*/
		
		// show error if reset day diffrence less than date range
		if( imei_number == '' &&  mac_number == '') {
			bootbox.alert("<?php echo lang('please_fill_address');?>");
			return false;
		}
		
		return;
		event.preventDefault();
	});
	
	$(document).delegate('.device_type','click',function(e){
		var value = $(this).attr('value');
		if(value == 2) {
			$('#imei_number_div').show();
		} else {
			$('#imei_number_div').hide();
		}
	});
	
</script>


