<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('deviceaddress') ?></h4>
				<a href="<?php echo site_url("admin/settings/deviceaddress/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_deviceaddress'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<div class="table-responsive m-t-40">
					<table id="deviceaddress_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('model_name'); ?></th>
								<th><?php echo lang('model_number'); ?></th>
								<th><?php echo lang('imei_number'); ?></th>
								<th><?php echo lang('mac_number'); ?></th>
								<th><?php echo lang('created_at'); ?></th>
								<th><?php echo lang('status'); ?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($deviceaddress) && is_array($deviceaddress) && count($deviceaddress)>0):
								foreach($deviceaddress as $address) :
									// set encoded id
									$id = encode($address['id']); ?>
									
									<tr>
										<td class="crp" row_id="<?php echo $id;?>"><?php echo $i;?></td>
										<td class="crp" row_id="<?php echo $id;?>"><?php echo (strlen($address['model_name']) > 20) ? substr($address['model_name'],0,20).'...' :  $address['model_name'];?></td>
										<td class="crp" row_id="<?php echo $id;?>"><?php echo $address['model_number'];?></td>
										<td class="crp" row_id="<?php echo $id;?>"><?php echo $address['imei_number'];?></td>
										<td class="crp" row_id="<?php echo $id;?>"><?php echo $address['mac_number'];?></td>
										<td class="crp" row_id="<?php echo $id;?>"><?php echo date('F j, Y',strtotime($address['created_at']));?></td>
										<td>
											<?php
											// set change status url
											$status_url = site_url("admin/settings/deviceaddress/change_status/".$id.DIRECTORY_SEPARATOR.encode($address['status']));
											if($address['status'] == '0') { ?>
												<div style="display:none"><?php echo $address['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
												<?php
											} else {
												?>
												<div style="display:none"><?php echo $address['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
											} ?>
										</td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/deviceaddress/delete/".$id);?>')"><i class="fa fa-trash"></i></a>
										</td>
										
									</tr>
									
								<?php 
								$i++;
								endforeach ;
							 endif; ?>
						</tbody>
					</table>
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

<script>
	$(document).ready(function() {
		$('#deviceaddress_table').DataTable();
	});
	
	$(document).delegate('.crp','click',function(e){
		var row_id = $(this).attr('row_id');
		window.location.href= BASEURL+'admin/settings/deviceaddress/create/'+row_id;
	});
	
</script>

