<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	Copyright (c) 2011 Lonnie Ezell

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

$lang['edm_title']			= 'Email Direct Marketing';
$lang['edm_title_labl']		= 'EDM Campaign Title';
$lang['edm_add_btn_title']  = 'Add New EDM';
$lang['edm_config_title']   = 'EDM Configuration';
$lang['edm_top_selling_product'] = 'Top Selling Product(s)';
$lang['edm_bottom_selling_product'] = 'Bottom Selling Product(s)';
$lang['edm_other_product']  = 'Add Other Product(s)';
$lang['edm_exclude_cat']    = 'Exclude Category';
$lang['exclude_cat_help']   = 'Category ids should be comma seprated like X,X,X. Products belong to this ids will be exclude in result.';
$lang['edm_start_date']     = 'Start Date';
$lang['edm_end_date']       = 'End Date';
$lang['edm_occurance_type']	= 'Occurance Type';
$lang['edm_Daily']   		= 'Daily';
$lang['edm_Weekly'] 		= 'Weekly';
$lang['edm_Monthly'] 		= 'Monthly';
$lang['edm_Yearly']  		= 'Yearly';
$lang['edm_Time']  			= 'Time';
$lang['edm_total_count']  	= 'Total Sent : ';
$lang['product_yearly_views']   = 'Product Yearly Views';
$lang['top_product_views']  	= 'EDM Product Views (Top 5 Produts)';
$lang['edm']  					= 'EDM';
$lang['edm_email_read_views']  	= 'EDM Email Read Views';
$lang['edm_yearly_read_log']  	= 'EDM Yearly Read Unread Log';
$lang['active_edm']  			= 'Active EDM';
$lang['expired_edm']  			= 'Expired EDM';
$lang['all_edm']  				= 'All EDM';
$lang['search_product']  		= 'Search Product(s)';
$lang['edm_remove_text']  		= 'Click here to edit edm';
$lang['edm_view_prod_page_text']= 'Click here to view edm product(s) page';
$lang['no_of_product_in_email'] = 'No. Of Products Display In Email';
$lang['no_of_product_in_email_help'] = 'How many products you want to display in email.';
$lang['no_of_top_selling_products']  = 'No. Of Top Selling Products';
$lang['no_of_bottom_selling_products']  = 'No. Of Bottom Selling Products';
$lang['select_product_1']  = 'Select top selling product';
$lang['select_product_2']  = 'Select bottom selling product';
$lang['no_of_top_selling_products_help_txt']  = 'This will decide that how many products you want in Top Selling Product(s) dropdown.';
$lang['no_of_bottom_selling_products_help_txt']  = 'This will decide that how many products you want in Bottom Selling Product(s) dropdown.';
$lang['filter_by_year'] = 'Filter By Last 5 Year :';
$lang['filter_by_edm'] 	= 'Filter By EDM :';
$lang['nav_tab1'] 		= 'EDM Information';
$lang['nav_tab2'] 		= 'Campaign Products';
$lang['nav_tab3'] 		= 'Schedule';
$lang['nav_tab4'] 		= 'Audiance/Demographic';
$lang['nav_tab5'] 		= 'Preview/Email Settings';
$lang['nav_other'] 		= 'Other';

$lang['email_banner_image']	= 'Email Banner Image';
$lang['product_mapping']	= 'Product Mapping';
$lang['email_subject']		= 'Subject';
$lang['email_template']		= 'Email Template Body';
$lang['img_upload_warning']	= 'Upload up to 1MB image. ';
$lang['email_header_text']	= 'Header Text';
$lang['email_description']	= 'Email Description';
$lang['edm_configured']		= 'EDM Successfully Configured.';
$lang['product_mapping_help_txt']	= 'This option will decide the availability of Product(s) under Email template.';
$lang['instore_offers_mapping']		= 'InStore Offers Mapping';
$lang['offers_mapping_help_txt']	= 'This option will decide the availability of Offer(s) under Email template.';
$lang['instore_offers']				= 'In-store Offers';
$lang['vip_offers']					= 'VIP Offers';
$lang['email_footer_content']		= 'Email Footer Text';
$lang['product_grid_title']			= 'Email Product Widget Title';
$lang['product_grid_title_help']	= 'This Title will display as heading of product grid/list.';
$lang['from_email']					= 'From';
$lang['product_id']					= 'Product Id';
$lang['product_name']				= 'Product Name';
$lang['product_price']				= 'Product Price';
$lang['special_price']				= 'Special Price';
$lang['product_image']				= 'Product Image';
$lang['delete_product']				= 'Remove Product';
$lang['other_product_search_help']	= 'Click here to add product(s) for email template.';
$lang['week_occurance_help']	= 'Click <i class="fa fa-calendar"></i> to set day(s) occurrence in week.';
$lang['month_occurance_help']	= 'Click <i class="fa fa-calendar"></i> to set day(s) occurrence in month.';
$lang['is_always_send_to_all_users']	= 'Always Send to All Users';
$lang['is_always_send_to_all_users_help'] = 'By check this option, You can send email to all users else only non viewed users will get this email.';
