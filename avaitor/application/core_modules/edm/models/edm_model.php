<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Competition Model
 *
 * The central way to access and perform CRUD on Competition.
 *
 * @package    Avaitor
 * @subpackage Edm_model
 * @category   Models
 * @author     CDN Team
 * @link       http://www.cdnsolutionsgroup.com
 */
class Edm_model extends BF_Model
{

	function __construct() {
		
        parent::__construct();    
        $this->read_db  = $this->load->database('read',TRUE); //Loading read database.
		$this->mssql_db = $this->load->database('mssql',TRUE); //Loading read database.
       // define tables
		$this->member_transaction_log = 'cte_member_transaction_log';
		$this->ava_targetpush_notificaiton = 'ava_targetpush_notificaiton';
        $this->categories_table = 'categories';
  		$this->stores_log_table = 'stores_log';
  		$this->master_log = 'edm_master_log';
  		$this->edm_queue_log = 'edm_queue_log';
  		$this->edm = 'edm';
  		$this->users = 'users';
  	    $this->offers_table = 'offers';
		$this->beacon_offers_table = 'beacon_offers';
		$this->image_library_table = 'ava_image_library';
	}
    
   /* Function to get products from the server */
	function get_products($limit=5,$type='top',$department_id=0){
		$orderBy = ($type == 'bottom')?'ASC':'DESC';
		//$data = $this->mssql_db->query("select * from ProdTbl WHERE PROD_NUMBER = 59232");
		//$data = $this->mssql_db->query("SELECT TOP(5) JNAD_PRODUCT, SUM(JNAD_QTY) AS TotalQuantity FROM JNLD_Account_TBL WHERE JNAD_TYPE ='SALE' GROUP BY JNAD_PRODUCT ORDER BY TotalQuantity DESC");
		$sql = "SELECT TOP($limit) a.JNAD_PRODUCT,SUM(a.JNAD_QTY) AS TotalQuantity,b.PROD_DESC FROM JNLD_Account_TBL a LEFT JOIN ProdTbl b ON b.PROD_NUMBER = a.JNAD_PRODUCT";
		if(!empty($department_id)) {
			$sql .= " JOIN CODETBL code ON code.CODE_KEY_NUM = b.PROD_DEPARTMENT";
		}
		$sql .= " WHERE a.JNAD_TYPE ='SALE'";
		if(!empty($department_id)) { 
			$sql .= " AND code.CODE_KEY_NUM NOT IN ($department_id) AND CODE_KEY_TYPE = 'DEPARTMENT'";
		}
		$sql .= " GROUP BY a.JNAD_PRODUCT,b.PROD_DESC ORDER BY SUM(a.JNAD_QTY) $orderBy";
		$data = $this->mssql_db->query($sql);
		return $data->result_array();
	}

   /**
	* Get all store log listing
	* @access: public
	**/
	public function get_store_list() {
		$this ->read_db-> select('*');
		$this ->read_db-> where('status',1);
		$this ->read_db-> where('store_name !=','');
		$this ->read_db-> order_by('store_name','ASC');
		$result = $this -> read_db -> get($this->stores_log_table);
		$return = array();
		if($result->num_rows() > 0){
			foreach($result -> result_array() as $row){
				$return[$row['store_id']] = $row['store_name'];			
			}
		}
		return $return;
	}
	/**
	 * Get all active edm
	 * @access: public
	**/
	public function get_active_edm($id=0,$isrow=0){
		$Today = date('Y-m-d');
		$SQL   = "SELECT * FROM ava_edm WHERE is_deleted = 0 AND (start_date <= '".$Today."' AND end_date >= '".$Today."')".(!empty($id)?" AND id = '".$id."'":"");
		$query = $this->db->query($SQL);
		return (($isrow)?$query->row_array():$query->result());
	}
	
	
	/**
	  * @Get all data of edm
	  * @param 
	  * @return array
	  **/
	 public function get_edmData($type='result',$limit=0, $offset=0,$search ='',$columns='',$dirs='DESC')
	 {
		$this->db->select('id,edm_title,start_date,end_date,created_date,is_deleted,product_add_type,send_time,manual_emails,is_always_send_to_all_users');
		$this->db->from($this->edm); 

     	if($search!=''){
			$Where = "(edm_title LIKE '%$search%')";
			$this->db->where($Where);	
     	}
     	if($columns!='' && $dirs !=''){
     		$col  = ($columns == 1)?'edm_title':'id';
     		$this->db->order_by($col,$dirs);
     	}
     	
		if($limit > 0){
     		$this->db->limit($limit,$offset);
     	}
		
		if($type=='count'){
     	return  $this->db->count_all_results();
     	}
     	
     	$query = $this->db->get();
     	return $query->result();
	 }
      
	 /**
	  * edm_TemplateData data from mssql product  tbl
	  * @access: public
	  **/
     public function edm_TemplateData($product_array,$maxcount = 3){
		$product_result =array();
		if(!empty($product_array)){
			foreach($product_array as $prodcutId){
				if($maxcount >= 1){
					if(!empty($prodcutId)){
						$result = $this->mssql_db->query("select Prod_Desc,Prod_Number from ProdTbl where (Prod_Number = $prodcutId)");
						$product_result[] = $result -> row_array();
						$maxcount --;		
					}
				}
			 }
		  }
		return $product_result;
	}
     
   /**
	* get_edm_user Get all user of edm
	* @access: receiver type
	**/
	public function get_edm_user($edm_data=""){
		$result   = "";
		$rowcount = 0;
		if(!empty($edm_data)){
			// set edm id
			$edm_id = $edm_data['id'];
			$is_always_send_to_all_users  = $edm_data['is_always_send_to_all_users'];
			// prepare edm receivers fetching sql
			$receiver_sql = "SELECT a.id,a.email FROM ".$this->db->dbprefix($this->users)." a";
			$res_type = json_decode($edm_data['rec_type']);
			// add sales based product clause
			if(in_array(3,$res_type)) {
				$product_add_type = !empty($edm_data['product_add_type']) ? $edm_data['product_add_type']:0;
				$product_top 	= !empty($edm_data['product_top'])?json_decode($edm_data['product_top']):array();
				$product_bottom = !empty($edm_data['product_bottom'])?json_decode($edm_data['product_bottom']):array();
				$product_other  = !empty($edm_data['product_other'])?$edm_data['product_other']:'';
				if($product_add_type == 2 && (!empty($product_top) || !empty($product_bottom))){
					$product_array = array_merge($product_top,$product_bottom);
					$product_array = implode(',',$product_array);
					$receiver_sql .= " LEFT JOIN ".$this->db->dbprefix($this->member_transaction_log)." b ON (b.member_id = a.acc_number AND b.product_id IN ($product_array)) ";
					
				} else if($product_add_type == 1 && !empty($product_other)){
					$receiver_sql .= " LEFT JOIN ".$this->db->dbprefix($this->member_transaction_log)." b ON (b.member_id = a.acc_number AND b.product_id IN ($product_other)) ";
				}
			}
			$receiver_sql .= " WHERE a.role_id=4 ";
			// add card type clause
			if(in_array(4,$res_type)) {
				$rec_reward_card_type = (!empty($edm_data['rec_reward_card_type']))?json_decode($edm_data['rec_reward_card_type']):"";	
				$rec_reward_card_type = implode(',',$rec_reward_card_type);
				$receiver_sql .= " AND a.reward_card_id IN ($rec_reward_card_type)";
			}
			// add specific home stores clause
			if(in_array(1,$res_type) && !empty($edm_data['rec_home_store_id'])) {
				$home_store_id = (!empty($edm_data['rec_home_store_id'])) ? json_decode($edm_data['rec_home_store_id']) : "";
				$home_store_id = implode(',',$home_store_id);
				$receiver_sql .= " AND a.store_id IN ($home_store_id)";
			}
			if($is_always_send_to_all_users == 0) {
				$receiver_sql .= " AND a.id NOT IN ( SELECT user_id FROM ava_edm_queue_log WHERE edm_id = $edm_id AND is_read = 2)";
			}
			$receiver_sql .= " GROUP BY a.id";
			$result = $this->read_db->query($receiver_sql);
			$rowcount = $result->num_rows();
			if($rowcount > 0){
				$result = $result->result_array();
			}
			return array('result'=>$result,'rowcount'=>$rowcount);			
		}
	}
     
    /**
	 * Function to  get edm count  from edm table
	 * @output: edm count
	 * @author: $$
	 **/
	public function get_edm_count() {
		$result = $this->read_db->query("SELECT SUM(if(is_deleted = 0, 1, 0)) AS count_active_edm,SUM(if(is_deleted = 1, 1, 0)) AS count_inactive_edm,COUNT(id) AS count_total_edm FROM ava_edm");
		return $result->row();
	}
	
	/**
	 * Function to  get edm count  from edm table
	 * @output: edm count 
	 * @author: $$
	 **/
	public function edm_mail_count($edmId=0){
		$result = $this->read_db->query("SELECT SUM(if(is_read = 0, 1, 0)) AS count_progress_mail,SUM(if(is_read = 1, 1, 0)) AS count_unread_mail,SUM(if(is_read = 2, 1, 0)) AS count_read_mail FROM ava_edm_queue_log WHERE edm_id =".$edmId);
		$return = $result->row();
		return $return;
	}
	
	/**
	 * @Function to  get edm statistics from ava_email_queue
	 * @output: edme mailcount 
	 * @author: $$
	 **/
	public function edmstatistics($month_limit=0){
		   $output = FALSE;
		if(!empty($month_limit)){
			 $SQL  = "SELECT YEAR(create_date) AS YEAR";
				$count = 0;
				foreach($month_limit as $months){
					 $mNumber = $months['mNumber'];
					 $mName   = $months['mName'];
					 $SQL  	 .= ",SUM(MONTH(create_date) = $mNumber AND is_read = 0) AS inprocess_$mName";
					 $SQL    .= ",SUM(MONTH(create_date) = $mNumber AND is_read = 1) AS unread_$mName";
					 $SQL    .= ",SUM(MONTH(create_date) = $mNumber AND is_read = 2) AS read_$mName";
					 $count++;
				}
			 $SQL   .=" FROM ava_edm_queue_log WHERE  create_date >= NOW()-INTERVAL 6 MONTH GROUP BY 1";
			 $result = $this->read_db->query($SQL);
			 $output = $result->row_array();
	  }
	  return $output;
	}
	
	/**
	 * @description : Function used to get email read/unread sum for current year
	 * @input : null
	 * @output : array
	 * @access : public
	 */
	public function email_read_unread_log($email_year='') {
		// get email read/unread sum for current year
		$result = $this->read_db->query(" 
			SELECT
			YEAR(create_date) AS YEAR,
			SUM(MONTH(create_date) = 1 AND is_read = 1) AS read_January,
			SUM(MONTH(create_date) = 1 AND is_read = 0) AS unread_January,																			
			SUM(MONTH(create_date) = 2 AND is_read = 1) AS read_February,
			SUM(MONTH(create_date) = 2 AND is_read = 0) AS unread_February,                               
			SUM(MONTH(create_date) = 3 AND is_read = 1) AS read_March,
			SUM(MONTH(create_date) = 3 AND is_read = 0) AS unread_March,                                     
			SUM(MONTH(create_date) = 4 AND is_read = 1) AS read_April,
			SUM(MONTH(create_date) = 4 AND is_read = 0) AS unread_April,                                    
			SUM(MONTH(create_date) = 5 AND is_read = 1) AS read_May,
			SUM(MONTH(create_date) = 5 AND is_read = 0) AS unread_May,                                    
			SUM(MONTH(create_date) = 6 AND is_read = 1) AS read_June,
			SUM(MONTH(create_date) = 6 AND is_read = 0) AS unread_June,                                 
			SUM(MONTH(create_date) = 7 AND is_read = 1) AS read_July,
			SUM(MONTH(create_date) = 7 AND is_read = 0) AS unread_July,                                     
			SUM(MONTH(create_date) = 8 AND is_read = 1) AS read_August,
			SUM(MONTH(create_date) = 8 AND is_read = 0) AS unread_August,                                     
			SUM(MONTH(create_date) = 9 AND is_read = 1) AS read_September,
			SUM(MONTH(create_date) = 9 AND is_read = 0) AS unread_September,                                       
			SUM(MONTH(create_date) = 10 AND is_read = 1) AS read_October,
			SUM(MONTH(create_date) = 10 AND is_read = 0) AS unread_October,                                  
			SUM(MONTH(create_date) = 11 AND is_read = 1) AS read_November,
			SUM(MONTH(create_date) = 11 AND is_read = 0) AS unread_November,                                    
			SUM(MONTH(create_date) = 12 AND is_read = 1) AS read_December,
			SUM(MONTH(create_date) = 12 AND is_read = 0) AS unread_December                            
			FROM ava_edm_queue_log
			WHERE YEAR(create_date) = $email_year"
		);
		return $result->row_array();
	}
	
	/**
	 * @description : Function used to get all edm sent email data
	 * @input : null
	 * @output : array
	 * @access : public
	 **/
	 public function get_all_edm_data() {
		$this ->read_db-> select('id,edm_title,start_date,end_date');
		$this ->read_db-> where('is_deleted',0);
		$this ->read_db-> order_by('created_date','DESC');
		$result = $this -> read_db -> get($this->edm);
		return $result->result_array();
	}
	
	/**
	 * @description : Function used to get edm's read & unread log
	 * @input : edm_id(int)
	 * @output : array
	 * @access : public
	 **/
	 public function get_edm_read_unread_log($edm_id=0) {
		$sql = "SELECT COUNT(id) AS edm_read,(SELECT COUNT(id) from ava_edm_queue_log WHERE edm_id = $edm_id AND is_read = 0)  as edm_unread from ava_edm_queue_log WHERE edm_id = $edm_id AND is_read = 1";
		$result = $this->read_db->query($sql);
		$output = $result->row_array();
		return $output;
	 }
	 
	 /**
	 * @description : Function used to get product viewed all edm
	 * @input : null
	 * @output : array
	 * @access : public
	 **/
	public function get_viewed_edm_data(){
		$sql = "SELECT e.id,e.edm_title, COUNT(evl.id) as edm_viewed FROM ava_edm e JOIN ava_edm_view_log evl ON e.id = evl.edm_id WHERE e.is_deleted = 0 GROUP BY evl.edm_id HAVING COUNT(evl.id) > 0 ORDER BY edm_viewed DESC";
		$result = $this->read_db->query($sql);
		$output = $result->result();
		return $output;
	}
	 
	/**
	 * @description : Function used to get edm's top product log
	 * @input : edm_id(int)
	 * @output : array
	 * @access : public
	 **/
	public function edm_product_view_data($edm_id=0){
		$sql = "SELECT product_id, SUM(view_count) as product_seen_count FROM ava_edm_view_log WHERE edm_id = $edm_id GROUP BY product_id HAVING SUM(view_count) > 0 ORDER BY view_count DESC LIMIT 5";
		$result = $this->read_db->query($sql);
		$output = $result->result_array();
		return $output;
	}
	
	/**
	 * @description : Function used to get yearly edm prduct view log
	 * @input : month_limit(array), edm_id(int)
	 * @output : array
	 * @access : public
	 **/
	 public function edm_product_yearly_viewed($month_limit=0,$edm_id=0,$prodYear=''){
		$output = FALSE;
		if(!empty($month_limit)){
			$sql  = "SELECT YEAR(created_date) AS YEAR";
			$count = 0;
			foreach($month_limit as $months){
				 $m_number = $months['mNumber'];
				 $m_name   = $months['mName'];
				 $sql  	  .= ",(SELECT SUM(view_count) FROM ava_edm_view_log WHERE YEAR(created_date) = $prodYear AND MONTH(created_date) = $m_number AND edm_id = $edm_id) AS viewed_$m_name";
				 $count++;
			}
			$sql .=" FROM ava_edm_view_log WHERE YEAR(created_date) = $prodYear AND edm_id = $edm_id GROUP BY 1";
			$result = $this->read_db->query($sql);
			$output = $result->row_array();
	  }
	  return $output;
	}
	
	/**
	 * @description : Function used to get all valid offers
	 * @input : null
	 * @output : array
	 * @access : public
	 **/
	public function get_all_offers(){
		// fetch all in-store offers
		$this ->read_db-> select('offer_id,offer_name,offer_short_description,offer_image_1,offer_image_2,offer_image_3,is_default_image,store_id,offer_price');
		$this ->read_db-> where('is_delete',0);
		$this ->read_db-> where('status',1);
		$this ->read_db-> where('DATE(start_time) <=',date('Y-m-d'));
		$this ->read_db-> where('DATE(end_time) >=',date('Y-m-d'));
		$this ->read_db-> order_by('created_at','DESC');
		$instore_result = $this -> read_db -> get($this->offers_table)->result_array();
		// fetch all beacon offers
		$this ->read_db-> select('beacon_offer_id,offer_name,offer_short_description,offer_image_1,offer_image_2,offer_image_3,is_default,store_id,offer_price');
		$this ->read_db-> where('is_delete',0);
		$this ->read_db-> where('status',1);
		$this ->read_db-> where('is_simple_offer',1);
		$this ->read_db-> where('DATE(start_date) <=',date('Y-m-d'));
		$this ->read_db-> where('DATE(end_date) >=',date('Y-m-d'));
		$this ->read_db-> order_by('created_at','DESC');
		$beacon_result = $this -> read_db -> get($this->beacon_offers_table)->result_array();
		
		return array('instore_offers'=>$instore_result,'simple_vip_offers'=>$beacon_result);
	}
	
	/**
	 * Get listing of edm log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_records() {
		
		$this -> read_db -> select('*');
		$this-> read_db ->from($this->edm);
		$this -> read_db -> order_by('created_date', 'desc');
		$result = $this -> read_db -> get();
		return $result -> result_array();
		
	}
	
	/**
	 * Get listing of emails whos already in email
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function edm_queue_sent_emails($edmId=0) {
		$this -> read_db -> select('email');
		$this-> read_db ->from($this->edm_queue_log);
		$this-> read_db ->where('edm_id',$edmId);
		$this-> read_db ->where('is_read',2);
		$this -> read_db -> group_by('email');
		$result = $this -> read_db -> get();
		return $result -> result_array();
		
	}

	/**
	 * Get listing of emails whos already in email
	 * @input : id
	 * @output: array
	 * @author: Pritesh Gami
	 */

	public function get_all_image() {

		$this -> read_db -> select('*');
		$result = $this -> read_db -> get($this->image_library_table);
		return $result -> result_array();
	}

	public function save_image($data) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$this->read_db->insert($this->image_library_table, $data);
		return $this->read_db->insert_id();
	}
	
	
}//end User_model
