<link href="<?php echo Template::theme_url('edm-dashboard/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" media="all"/>

<?php
$edm_id 	   			=  !empty($edm_data['id'])?$edm_data['id']:'';
$edm_title  			=  !empty($edm_data['edm_title'])?$edm_data['edm_title']:'';
$start_date 			=  !empty($edm_data['start_date'])?$edm_data['start_date']:'';
$end_date  				=  !empty($edm_data['end_date'])?$edm_data['end_date']:'';
$product_top  			=  !empty($edm_data['product_top'])?json_decode($edm_data['product_top']):array();
$product_bottom  		=  !empty($edm_data['product_bottom'])?json_decode($edm_data['product_bottom']):array();
$product_other  		=  !empty($edm_data['product_other'])?$edm_data['product_other']:'';
$occurance_type  		=  !empty($edm_data['occurance_type'])?$edm_data['occurance_type']:'';
$occurance_data  		=  !empty($edm_data['occurance_data'])?json_decode($edm_data['occurance_data']):'';
$rec_home_store_id  	=  !empty($edm_data['rec_home_store_id'])?json_decode($edm_data['rec_home_store_id']):array();
$rec_previous_purchase  =  !empty($edm_data['rec_previous_purchase'])?$edm_data['rec_previous_purchase']:'';
$rec_all  				=  !empty($edm_data['rec_all'])?$edm_data['rec_all']:'';
$rec_type			  	=  !empty($edm_data['rec_type'])?json_decode($edm_data['rec_type']):array();
$rec_reward_card_type  	=  !empty($edm_data['rec_reward_card_type'])?json_decode($edm_data['rec_reward_card_type']):array();
$no_products  			=  !empty($edm_data['no_products'])?$edm_data['no_products']:'';
$no_of_top_products  	=  !empty($edm_data['no_of_top_products'])?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
$no_of_bottom_products  =  !empty($edm_data['no_of_bottom_products'])?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
$is_subscription  		=  !empty($edm_data['is_subscription'])?$edm_data['is_subscription']:'';
$created_date  			=  !empty($edm_data['created_date'])?$edm_data['created_date']:'';
$product_top_val  		=  !empty($edm_data['product_top'])?$edm_data['product_top']:'';
$product_bottom_val  	=  !empty($edm_data['product_bottom'])?$edm_data['product_bottom']:'';
$product_add_type  		=  !empty($edm_data['product_add_type'])?$edm_data['product_add_type']:0;

//$SelectTop = (empty($product_top))?"<span class='hida'>Select top selling product</span>":"";
$SelectTop = "";
$TopHtml   = "";
$TopName   = "";
$isTop_dn  = "dn";
if(!empty($top)){
	foreach($top as $topData){
		$prId = $topData['JNAD_PRODUCT'];
		$prId = floor($prId);
		$prName = $topData['PROD_DESC'];
		$isTopchecked = "";
		if(in_array($topData["JNAD_PRODUCT"],$product_top)){
			$selectoPtion  = $prName.',';
			$TopName      .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isTopchecked  = "checked";	 
			$isTop_dn 	    = "";			 
		}
		$TopHtml .= "<li><input type='checkbox' class='topSellingProducts' id=".$prId." name='product_top[]' ".$isTopchecked." value='$prId' data-name='$prName' />
		<label class='custom-control-label' for=".$prId.">$prName</label>
		</li>";
	 }
	 $SelectTop .= "<ul class='edm-multiSel dn'>".$TopName."</ul>";
}
//$SelectBottom = (empty($product_bottom))?"<span class='hida'>Select bottom selling product</span>":"";
$SelectBottom = '';
$BottomHtml   = "";
$BottomName   = "";
$isBottom_dn  = "dn";
if(!empty($bottom)){
	foreach($bottom as $bottomData){
		$prId   = $bottomData['JNAD_PRODUCT'];
		$prId = floor($prId);
		$prName = $bottomData['PROD_DESC'];
		$isBottomchecked = "";
		if(in_array($bottomData["JNAD_PRODUCT"],$product_bottom)){
			$selectoPtion = $prName.',';
			$BottomName  .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isBottomchecked = "checked";
			$isBottom_dn = "";
		}
		$BottomHtml .= "<li><input type='checkbox' class='bottomSellingProducts' name='product_bottom[]'  ".$isBottomchecked."  id=checkbox_".$prId." value='$prId' data-name='$prName' /><label class='custom-control-label' for=checkbox_".$prId.">$prName</label></li>";
	}
	$SelectBottom .= "<ul class='edm-multiSel dn'>".$BottomName."</ul>";
}

$SelectStore  = (empty($rec_home_store_id))?"<span class='hida'>Select Store</span>":"";
$StoreHtml   = "";
$StoreName   = "";
$isStore_dn  = "dn";
if(!empty($storeList)){
	 foreach($storeList as $key=>$value){
		   $prId   = $key;
		   $prName = $value;
           $isStorechecked = "";
         if(in_array($prId,$rec_home_store_id)){
			 $selectoPtion = $value.',';
			 $StoreName   .= "<span title='$selectoPtion'>$selectoPtion</span>";
			 $isStorechecked = "checked";
			 $isStore_dn = "";
		 }
         $StoreHtml .= "<li><input type='checkbox' class='homeStores' name='rec_home_store_id[]' ".$isStorechecked." value='$prId' id=checkbox_".$prId." data-name='$prName' /><label class='custom-control-label' for=checkbox_".$prId.">$prName</label></li>";
	 }
 }

$days = array(
		'sunday'   =>'Sunday',
		'monday'   =>'Monday',
		'tuesday'  =>'Tuesday',
		'wednesday'=>'Wednesday',
		'thursday' =>'Thursday',
		'friday'   =>'Friday',
		'saturday' =>'Saturday',
	);
// set product id array
$product_ids = [];
$product_names = [];
$product_prices = [];
$special_prices = [];
$product_images = [];

if(!empty($edm_data['edm_products_json'])) {
	$edm_products = json_decode($edm_data['edm_products_json']);
	foreach($edm_products as $val) {
		$product_ids[] = $val->product_id;
		$product_names[$val->product_id] = $val->product_name;
		$product_prices[$val->product_id] = $val->product_price;
		$special_prices[$val->product_id] = $val->special_price;
		if(!empty($val->product_image)){
			$product_images[$val->product_id] = $val->product_image;		
		}
	}
}
$product_ids = (count($product_ids) > 0 ) ? json_encode($product_ids) : '';
$product_names = (count($product_names) > 0 ) ? json_encode($product_names) : '';
$product_prices = (count($product_prices) > 0 ) ? json_encode($product_prices) : '';
$special_prices = (count($special_prices) > 0 ) ? json_encode($special_prices) : '';
$product_images = (count($product_images) > 0 ) ? json_encode($product_images) : '';


// set sell based product id array

$sell_based_product_ids = [];
if(!empty($edm_data['edm_sell_base_products_json'])) {
	$edm_products = json_decode($edm_data['edm_sell_base_products_json']);
	foreach($edm_products as $val) {
		$sell_based_product_ids[] = $val->product_id;
	}
}
$sell_based_product_ids = (count($sell_based_product_ids) > 0 ) ? json_encode($sell_based_product_ids) : '';

?>
<!-- Title input start here -->
<!-- new tab Start Content panel -->
<div class="row">          
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
					<?php echo form_open_multipart($this->uri->uri_string(),array('name'=>'frm_edm','id'=>'frm_edm')); ?>
						 <ul class="nav nav-tabs md-tabs edm-tabs process" role="tablist">
							<li class="active nav-item" data-tab="info_tab">
							<a class="nav-link"><?php echo lang('nav_tab1');?></a></li>
							<li class="nav-item" data-tab="product_tab"><a class="nav-link"><?php echo lang('nav_tab2');?></a></li>
							<li class="nav-item" data-tab="edm_email_tab"><a class="nav-link"><?php echo lang('nav_tab5');?></a></li>
							<li class="nav-item" data-tab="frequency_tab"><a class="nav-link"><?php echo lang('nav_tab3');?></a></li>
							<li class="nav-item" data-tab="receiver_tab"><a class="nav-link"><?php echo lang('nav_tab4');?></a></li>
						</ul>

				<div class="tab-content scroll mt10">
									<!-- Start edm general tab panel -->
					<div class="tab-pane active" id="info_tab">
						<div class="p-t-20">
							<div class="row">
								<div class="col-sm-6">
									<!-- EDM title input start here -->
									<div class="form-group form-material">
										<label for="edm-other-other_tab"><?php echo lang('edm_title_labl');?><i style="color:red;">*</i></label>															
										<?php
										echo form_hidden('edm_id',$edm_id);
										$data = array('name'=>'edm_title','id'=>'edm_title', 'value'=>$edm_title,'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
										echo form_input($data);
										?>
									</div>
									<!-- EDM title input end here -->


									<!-- Start_date input start here -->	
									<div class="form-group form-material" id="start_date_div1">
										<label for="co_start_date"><?php echo lang('edm_start_date');?><i style="color:red;">*</i></label>
										<?php
										$data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$start_date,'id'=>'start_date','class'=>'form-control form-control-line required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
										echo form_input($data);
										?>
									</div>

									<div class="form-group form-material" id="end_date_div1">
										<label for="co_end_date"><?php echo lang('edm_end_date');?></label>

										<?php
										$data = array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$end_date,'id'=>'end_date' , 'class'=>'form-control form-control-line' ,'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_end_date'), 'readonly'=>'readonly');
										echo form_input($data);?>
									</div>

									<div class="clearboth mb10"></div>	
													
									<!-- Email description text start here -->
									<div class="form-group form-material">
									<label><?php echo lang('email_description');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'email_description', 'class'=>'edmmceEditor redactor form-control form-control-line required', 'value'=>(!empty($edm_data['email_description'])?$edm_data['email_description']:''));
									echo form_textarea($data);

									?>
									</div>		
									<!-- Email description text end here -->
													
									<!-- Email footer content box start here -->
									<div class="form-group form-material">
										<label><?php echo lang('email_footer_content');?></label>
										<?php 
										$data= array('name'=>'email_footer_text', 'class'=>'footermceEditor redactor form-control form-control-line', 'value'=>(!empty($edm_data['email_footer_text'])?$edm_data['email_footer_text']:''));
										echo form_textarea($data);
										?>
									</div>		
									<!-- Email footer content box end here -->
								</div>
								<!-- End edm general tab panel -->
								<div class="col-sm-6">
											<!-- Banner image box start here -->
											<div class="img-wrapper">
												<label for="title" class="img-title"><?php echo lang('email_banner_image');?></label> 	
												<div class="img-box">
													<?php
													// set banner image action
													$banner_image_val = (isset($email_banner_image) && !empty($email_banner_image)) ? $email_banner_image :'';
													$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
													if(!empty($banner_image_val)) {
														$banner_image_src = base_url('uploads/edm_images/'.$banner_image_val);
														$title = lang('tooltip_offer_image');
												
														$banner_image_val_doc = $this->config->item('document_path').'uploads/edm_images/'.$email_banner_image; 
														if(!file_exists($banner_image_val_doc)){
															// $promo_image_1 = base_url('uploads/No_Image_Available.png');
															$banner_image_src = base_url('uploads/No_Image_Available.png');
															$title = "No Image Available";
														}
													} else {
														$title = lang('tooltip_image');
														$banner_image_src = base_url('uploads/upload.png');
													} ?>
													<div class="cell-img" id="banner_image_cell">
														<img id="banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
													</div>
													<?php
													$data = array('name'=>'email_banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
													echo form_upload($data);
													// set banner image value in hidden type
													$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
													echo form_input($data);?>
												</div>
												<div class="alert red-alert alert-danger alert-dismissible ">
												   <i class="icon fa fa-warning"></i>
													<?php echo lang('img_upload_warning');?>
												</div>
												<!-- Banner image box end here -->
											</div>	
								</div>
						</div>
					</div>

						<!-- submit button here -->
						<div class="form-box pb-3">
							<div class="mx-1">
								<button type="submit" name="next" class="btn btn-primary btn-margin"><?php echo 'Next';?></button>
							</div>
						</div>
							<!-- submit button here -->
							<div class="clearboth"></div>
							<!-- End referral system panel -->	
					</div>



			<!-- Start general config panel -->
				<div class="tab-pane min-loader form-wrapper" id="product_tab">
					<div class="p-t-20">
						<div class="row">
							<div class="col-sm-12">
								<div class="product-mapping-box">
											<!-- No product mapping radio start here -->
											<div class="form-box edm-receiver radio-product">
												<input  type="radio" name="product_add_type" id="no_product_mapping_radio" value="0" <?php echo (empty($product_add_type)) ? "checked" : "";?> >
												<label class="custom-control-label" for="no_product_mapping_radio">
												 <?php echo 'No Product Mapping';?>
												</label>
											</div>
											<!-- No product mapping radio end here -->	
											
											<!-- Product direct search radio start here -->
											<div class="form-box edm-receiver radio-product">
												<input  type="radio" name="product_add_type" id="direst_product_search_radio" value="1" <?php echo ($product_add_type ==1) ? "checked" : "";?> >
												<label class="custom-control-label" for="direst_product_search_radio" >
												 <?php echo 'Direct Product Search';?>
												</label>
											</div>
											<!-- Product direct search radio end here -->		
											
											<!-- Sell based product search radio start here -->
											<div class="form-box edm-receiver radio-product">
												<input  type="radio" name="product_add_type" id="sell_based_product_radio" value="2"  <?php echo ($product_add_type == 2) ? "checked" : "";?> >
												<label class="custom-control-label" for="sell_based_product_radio">
												 <?php echo 'Sell Based Products';?>
												</label>
											</div>

											<!-- Sell based product search radio end here -->
											
											<!-- Product box title input start here -->
								<div class="row">
									<div class="col-sm-6">
											<div class="form-group form-material include-products <?php echo (!empty($product_add_type)) ? "" : "dn";?> ">
												<label><?php echo lang('product_grid_title');?><i style="color:red;">*</i><span class="fa fa-info-circle ml5" title="<?php echo lang('product_grid_title_help');?>"></span></label>
												<?php
												$data = array('name'=>'product_grid_title','id'=>'product_grid_title','value'=>set_value('product_grid_title', isset($edm_data['product_grid_title'])?$edm_data['product_grid_title']: ''), 'class'=>'form-control form-control-line required');
												echo form_input($data);?>
											</div>	
											<!-- Product box title input end here -->
										
											<!-- minimum reward point to redeem input start here -->
											<div class="form-group form-material eclud-cat include-products <?php echo (!empty($product_add_type)) ? "" : "dn";?>">
												 <label for="title"><?php echo 'Exclude Category';?><span class="fa fa-info-circle ml5" title="<?php echo lang('exclude_cat_help');?>"></span></label>
												 <?php
													$data = array('name'=>'exclude_cat','id'=>'exclude_cat', 'value'=> set_value('product_grid_title', isset($edm_data['exclude_cat'])?$edm_data['exclude_cat']: '') , 'class'=>'span8', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_exclude_cat'),'class'=>'form-control form-control-line');
													echo form_input($data);
												 ?>
											</div>
									</div>			
								</div>			
											<!-- minimum reward point to redeem input end here -->
											
											<!-- Product direct search box start here -->						
											<div class="form-box mt10 <?php echo ($product_add_type ==1) ? "" : "dn";?>" id="product_direct_searchbox">
												<label><?php echo lang('edm_other_product');?><i style="color:red;">*</i></label>
												<div class="row">
												<div class="col-sm-6">
												<div id="searchProductButton">
													<button type="button" name="product_search" class="btn btn-primary otherPr" ><?php echo lang('search_product');?></button>
												</div>
												<div class="alert alert-success alert-dismissible other-product-help">
													<i class="icon fa fa-info"></i> 
													<?php echo lang('other_product_search_help');?>
												</div>
												</div>
												</div>
												<div class="mt10 "  id="showproductDiv"></div>
												<input type="hidden" id="product_other" name="product_other" value="<?php echo (!empty($edm_data['product_other'])) ? $edm_data['product_other'] : ''?>">
												<input type="hidden" name="deleted_product_id" id="deleted_product_id" value="">

												<input type="hidden" id="product_exist" name="product_exist" value="<?php echo (!empty($edm_data['product_other'])) ? $edm_data['product_other'] : ''?>">
												
												<!-- Product price options start here -->
												<?php 
												$edm_products_json = (!empty($edm_data['edm_products_json'])) ? $edm_data['edm_products_json'] : ''; ?>
												<div class="clearboth product-grid-form mb10">
													<table class="table table-bordered <?php echo (!empty($edm_products_json)) ? '' : 'dn';?>" id="product-table"> 
														<thead class="bg_ccc"> 
															<th><?php echo lang('product_id')?></th>
															<th><?php echo lang('product_name')?></th>
															<th><?php echo lang('product_price')?></th>
															<th><?php echo lang('special_price')?></th>                         
															<th><?php echo lang('product_image')?></th>                         
															<th><?php echo lang('delete_product')?></th>                         
														</thead>
														<tbody id="email_products" class="product_table_tbody">
															<?php
															$products = json_decode($edm_products_json);
															if(!empty($products)) {
																foreach($products as $val) {
																	if(!empty($val->product_image)){
																		$image_path = base_url().'uploads/image_library/'.$val->product_image;
																		$product_image = $val->product_image;
																	}else{
																		$image_path = base_url('uploads/No_Image_Available.png');
																		$product_image = '';
																	}
																	echo '<tr id="'.$val->product_id.'">';
																	echo '<td><input type="text" readonly name="product_id[]" value="'.$val->product_id.'" class="form-control readonly_input width100"></td>';
																	echo '<td><input type="text" readonly name="product_name[]" value="'.$val->product_name.'" class="form-control readonly_input width100"></td>';
																	echo '<td><input type="text" name="product_price[]" value="'.$val->product_price.'" id="product_price_'.$val->product_id.'" class="form-control number product_input" row_id='.$val->product_id.'></td>';
																	echo '<td><input type="text" name="special_price[]" value="'.$val->special_price.'" id="special_price_'.$val->product_id.'" class="form-control number product_input" row_id='.$val->product_id.'></td>';
																	echo '<td class="upload_product_image"><img src="'.$image_path.'" class="product_image_preview_'.$val->product_id.'" id="select_product_button_'.$val->product_id.'" aria-hidden="true"   data-name="'.$product_image.'" data-id="'.$val->product_id.'" data-toggle="modal" data-target="#productImageModel"><input type="hidden" name="product_image[]" value="'.$product_image.'" id="product_image_'.$val->product_id.'"></td>';

																	echo '<td><a class="common-btn remove-btn" row_id='.$val->product_id.'><i class="fa fa-trash"></i></a></td>';
																}
															} ?>
														</tbody>	
													</table>
												</div>		
												<!-- Product price options end here -->
											</div>
											<!-- Product direct search box end here -->
											
											<!-- Sell based product search box start here -->	
											<div id="sellbased_product_searchbox" class="<?php echo ($product_add_type == 2) ? "" : "dn";?>">
												<!-- number of top selling product input start here -->
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group form-material">
													<label><?php echo lang('no_of_top_selling_products');?>
														<i style="color:red;">*</i>
														<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
													</label>
													<?php
													$data = array('name'=>'no_of_top_products','id'=>'no_of_top_products', 'value'=> $no_of_top_products ,
													'class'=>'form-control form-control-line required product_selling_fetch_limit','product_selling_type'=>1, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_top_selling_products'));
													echo form_input($data);
													$data = array('type'=>'hidden','id'=>'no_of_top_products_hidden','value'=> $no_of_top_products);
													echo form_input($data);
													?>						
												</div>
												<!-- number of top selling product input end here -->
											
												<!-- number of bottom selling product input start here -->
												<div class="form-group form-material">
													<label>
														<?php echo lang('no_of_bottom_selling_products');?><i style="color:red;">*</i>
														<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
													</label>
													<?php
													$data = array('name'=>'no_of_bottom_products','id'=>'no_of_bottom_products', 'value'=> $no_of_bottom_products ,
													'class'=>'form-control form-control-line required product_selling_fetch_limit','product_selling_type'=>2, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_bottom_selling_products'));
													echo form_input($data);
													$data = array('type'=>'hidden','id'=>'no_of_bottom_products_hidden','value'=> $no_of_bottom_products);
													echo form_input($data);
													?>
												</div>
											</div>
										</div>
												<!-- number of bottom selling product input end here -->
											
												<!-- maximum reward point input start here -->
												<div class="form-box mt10 indispaly">
													<label>
														<!--
														<span class="fl mr5">
															<input  type="checkbox" onchange="cbchange(this)" data-show="topsell-pr" id="topSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_top)?"checked":""); ?>>
														</span>
														-->
														<span class="fl mr5">
															<?php echo lang('edm_top_selling_product');?>
														</span> 
													</label>  
													<span class="topsell-pr">			 						 
														<dl class="edm-dropdown" id="top"> 
															<dt>
																<a href="#" class="selectProducts_1">
																	<?php echo $SelectTop;?>
																</a>
															</dt>
															<dd>
																<div class="mutliSelect" name="top">
																	<ul class="ulProductHtml_1">
																		<?php echo $TopHtml; ?>
																	</ul>
																</div>
															</dd>
														</dl>
													 </span>
												</div>
												<!-- maximum reward points input end here -->
											
												<!-- reward point expiry input start here -->
												<div class="form-box mt10 indispaly">
													<label>
														<!--
														<span class="fl mr5">
															<input  type="checkbox" onchange="cbchange(this)" data-show="bottomsell-pr" id="bottomSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_bottom)?"checked":""); ?>>
														</span>
														-->
														<span class="fl mr5">	
															<?php echo lang('edm_bottom_selling_product');?>
														</span>	
													</label>
													<span class="bottomsell-pr">
														<dl class="edm-dropdown" id="bottom"> 
															<dt>
																<a href="#" class="selectProducts_2">
																	<?php echo $SelectBottom; ?>
																</a>
															</dt>
															<dd>
																<div class="mutliSelect"  name="bottom">
																	<ul class="ulProductHtml_2">
																		<?php echo $BottomHtml; ?>
																	</ul>
																</div>
															</dd>
														</dl>
													</span>
												</div>
												<!-- reward point expiry input end here -->
												
												<!-- Product price options start here -->
												<?php 
												$edm_sell_base_products_json = (!empty($edm_data['edm_sell_base_products_json'])) ? $edm_data['edm_sell_base_products_json'] : ''; ?>
												<div class="clearboth product-grid-form mb10">
													<table class="table table-bordered <?php echo (!empty($edm_sell_base_products_json)) ? '' : 'dn';?>" id="sellbased-product-table"> 
														<thead class="bg_ccc"> 
															<th><?php echo lang('product_id')?></th>
															<th><?php echo lang('product_name')?></th>
															<th><?php echo lang('product_price')?></th>
															<th><?php echo lang('special_price')?></th>
															<th><?php echo lang('product_image')?></th>
															<th><?php echo lang('delete_product')?></th>                        
														</thead>
														<tbody id="email_sellbased_products" class="product_table_tbody">
															<?php
															$products = json_decode($edm_sell_base_products_json);

															if(!empty($products)) {
																foreach($products as $val) {
																	if(!empty($val->product_image)){
																		$image_path = base_url().'uploads/image_library/'.$val->product_image;
																		$product_image = $val->product_image;
																	}else{
																		$image_path = base_url('uploads/No_Image_Available.png');
																		$product_image = '';
																	}
																	
																	echo '<tr id="'.$val->product_id.'">';
																	echo '<td><input type="text" readonly name="product_id[]" value="'.$val->product_id.'" class="form-control readonly_input width100"></td>';
																	echo '<td><input type="text" readonly name="product_name[]" value="'.$val->product_name.'" class="form-control readonly_input width100"></td>';
																	echo '<td><input type="text" name="product_price[]" value="'.$val->product_price.'" id="product_price_'.$val->product_id.'" class="form-control number sell_product_input" row_id='.$val->product_id.'></td>';
																	echo '<td><input type="text" name="special_price[]" value="'.$val->special_price.'" id="special_price_'.$val->product_id.'" class="form-control number sell_product_input" row_id='.$val->product_id.'></td>';
																	echo '<td class="upload_product_image"><img src="'.$image_path.'" class="product_image_preview_'.$val->product_id.'" id="select_product_button_'.$val->product_id.'" aria-hidden="true"   data-name="'.$product_image.'" data-id="'.$val->product_id.'" data-toggle="modal" data-target="#productImageModel"><input type="hidden" name="product_image[]" value="'.$product_image.'" id="product_image_'.$val->product_id.'"></td>';
																	echo '<td><a class="common-btn remove-btn" row_id='.$val->product_id.'><i class="fa fa-trash"></i></a></td>';
																}
															} ?>
														</tbody>	
													</table>
												</div>						
												<!-- Product price options end here -->
											</div>	
											<!-- Sell based product search box end here -->
											
											<!-- number of product show on email input start here -->
									<div class="row">
										<div class="col-sm-6">	
											<div class="form-group form-material include-products <?php echo (!empty($product_add_type)) ? "" : "dn";?>">
												<label for="edm-other-quantity"><?php echo lang('no_of_product_in_email');?><i style="color:red;">*</i><span class="fa fa-info-circle ml5" title="<?php echo lang('no_of_product_in_email_help');?>"></span></label>
												<?php
												$data = array('name'=>'no_products','id'=>'no_products', 'value'=> $no_products ,
												'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
												echo form_input($data);
												?>
											</div>
										</div>
									</div>	
											<!-- number of product show on email input end here -->
								</div>
									<div class="clearboth"></div>				
							</div>	
						</div>
					</div>	
				<!-- submit button here -->
				<div class="form-box pb-3">
				<?php
				$data = array('type'=>'hidden','name'=>'edm_products_json','id'=>'edm_products_json','value'=> !empty($edm_data['edm_products_json'])?$edm_data['edm_products_json']:'');
				echo form_input($data);
				$data = array('type'=>'hidden','name'=>'edm_sell_base_products_json','id'=>'edm_sell_base_products_json','value'=> !empty($edm_data['edm_sell_base_products_json'])?$edm_data['edm_sell_base_products_json']:'');
				echo form_input($data);?>
				<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
				<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
				</div>
				<!-- submit button here -->					
			</div>
		<!-- End general config panel -->




									
			<!-- Start edm email tab panel -->
			<div class="tab-pane co_products_tab min-loader" id="edm_email_tab">
				<div class="p-t-20">
					<div class="row">
					<div class="col-sm-6">
										<?php 
										$template_sugession_display = (!empty($edm_template)) ? 'display_none' : '';
										$hide_template = (!empty($edm_template)) ? '' : 'display_none';?>
										<!-- Template layout suggession box start here -->
										<div id="choose-template" class="text-center <?php echo $template_sugession_display ;?>">
											<button class="choose mr20" type="button" id="grid_view"><img src="<?php echo site_url().'uploads/edm_images/edm-grid-view.jpg'?>" class="img-responsive" alt=""><span>Grid Template</span></button>
											<button class="choose" type="button" id="list_view"><img src="<?php echo site_url().'uploads/edm_images/edm-list-view.jpg'?>" class="img-responsive" alt=""><span>List Template</span></button>
											<!-- submit button here -->
											<div class="form-box">
												<button type="button" name="cancel" class="btn btn-info back-btn fl" onclick="backnext()"><?php echo 'Back';?></button> 
											 </div>
											<!-- submit button here -->
										</div>
										<!-- Template layout suggession box end here -->
										
										<div class="email-template-wrapper clearboth email-template-box <?php echo $hide_template ;?>">
											<?php echo $this->load->view('edm_email_template_form'); ?>
											
										</div>
					</div>
					</div>
				</div>
											<!-- submit button here -->
											<div class="form-box pb-3 p-t-20">
												<?php
												$data = array('type'=>'hidden','name'=>'template_type','id'=>'template_type','value'=>(!empty($edm_data['template_type']))?$edm_data['template_type']:'');
												echo form_input($data);?>
												<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
												<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
											 </div>
											<!-- submit button here -->				
			</div>						
			<!-- End edm email tab panel -->



									
			<!-- Start frequency panel -->
			<div class="tab-pane" id="frequency_tab">
				<div class="p-t-20">
					<div class="row">
					<div class="col-sm-12">
										<div class="">
											<!-- edm_occurance input start here -->
											<div class="form-box">
												<label for="co_end_date"><b><?php echo lang('edm_occurance_type');?></b><i style="color:red;">*</i></label>
												<div class="app">
													<div class="appointment">
														<div class="col-md-12">
															<div class="menu-Recurrence">
																<ul class = "freq-edm">
																  	<li>	
																		<div class="valuedm">	
																			<input  type="radio" class="edm_occurance" name="occurance_type" id="daily" data-div="daily_div" value="1" <?php echo ($occurance_type == 1 || $occurance_type == '' )?"checked":""; ?> >
																			<label class="custom-control-label" for="daily">
																			<?php echo lang('edm_Daily');?>
																			</label>
																	  	</div>
																   	</li>
																	<li>	
																		<input  type="radio" class="edm_occurance" name="occurance_type" id="weekly" data-div="weekly_div" value="2" <?php echo ($occurance_type == 2)?"checked":""; ?> >
																		<label class="custom-control-label" for="weekly">
																		<?php echo lang('edm_Weekly');?>
																		</label>			
																	</li>
																	<li>
																		<input  type="radio" class="edm_occurance" name="occurance_type" id="monthly" data-div="monthly_div" value="3" <?php echo ($occurance_type == 3)?"checked":""; ?> >
																		<label class="custom-control-label" for="monthly">
																		<?php echo lang('edm_Monthly');?>
																		</label>			
																	</li>
																	<li>
																		<input  type="radio" class="edm_occurance" name="occurance_type" id="yearly" data-div="yearly_div" value="4" <?php echo ($occurance_type == 4)?"checked":""; ?> >
																		<label class="custom-control-label" for="yearly">
																		<?php echo lang('edm_Yearly');?>
																		</label>
																	</li>
																</ul>
															</div>
															<?php 
															$daily = $weeks = $monthly = array();
															switch($occurance_type){
																case 1:
																$daily = $occurance_data;
																break;
																case 2:
																$weeks = $occurance_data;
																break;
																case 3:
																$monthly = $occurance_data;
																break;
																case 4:
																$yearly = $occurance_data;
																break;
															}														
															?>
															<!-- daily_div -->
															<div class="values-seen">
																<div class="col-md-9 occurance-div <?php echo ($occurance_type == 1 || $occurance_type == '') ? "" : "dn";?>" id="daily_div"  >
																	<div class="pull-left">
																		<h5>Promotional Messages will repeat on every day.</h5>
																	</div>
																</div>
																<!-- weekly_div -->	
																<div class="col-md-10 col-xs-12 occurance-div  <?php echo ($occurance_type == 2 && !empty($weeks))?"":"dn"; ?>" id="weekly_div">
																	<div class="pull-left">
																		<h5>Promotional Messages will repeat every week on.</h5>
																		<ul class="list-inline ul_week custcheck-box" id="recurring_day_ul" >
																			<?php
																			$dayCount = 1;			
																			foreach($days as $daykey => $dayvalue){ ?>							
																				<li>
																					<input id="box<?php echo $dayCount; ?>" class="weekly_days" name="weekly_days[]" value="<?php echo $daykey; ?>" <?php echo (in_array($daykey,$weeks))?'checked':'';?> type="checkbox" />
																					<label for="box<?php echo $dayCount;?>"><?php echo $dayvalue; ?></label>
																				</li>
																			<?php
																				$dayCount++;
																			}?> 	 
																		</ul>
																		<div class="alert other-product-help">
																			<i class="icon fa fa-info"></i>
																			<?php echo lang('week_occurance_help');?>			
																		</div>
																	</div>
																</div>
																<!-- monthly_div -->
																<div class="col-md-10 col-xs-12 occurance-div <?php echo ($occurance_type == 3 && !empty($monthly))?"":"dn"; ?>" id="monthly_div" >
																	<h5>Promotional Messages will repeat every month of</h5>
																	<ul class="list-inline ul_month custcheck-box" id="recurring_date_ul">
																		 <?php 
																		 for($i=1; $i<=31; $i++){ ?>
																			<li>
																				<input id="<?php echo $i;?>" class="monthly_days" name="monthly_dates[]" value="<?php echo $i;?>" <?php echo (in_array($i,$monthly))?'checked':''; ?> type="checkbox" />
																				<label for="<?php echo $i;?>"><?php echo $i;?></label>
																			</li>
																		  <?php } ?>
																	</ul>
																	<div class="alert other-product-help">
																		<i class="icon fa fa-info"></i>
																		<?php echo lang('month_occurance_help');?>			
																	</div>
																</div>
																<!-- yearly_div -->
																<div class="col-md-9 occurance-div <?php echo ($occurance_type == 4 && !empty($yearly))?"":"dn"; ?>" id="yearly_div" >
																	<h5>Promotional Messages will repeat every year of.</h5>												
																	<div class="input-group" style="margin:10px 0;">							
																		<div class="half_date">
																			<div class="form-group input-append date" id="yearly_time_div">
																				<?php
																				$data	= array('name'=>'yearly_time', 'data-format'=>'dd-MM-yyyy', 'value'=>isset($yearly)?$yearly:'','id'=>'yearly_time','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
																				 echo form_input($data);?>
																				<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
																			</div>																   
																		</div>
																	</div>
																	<div class="clearfix"></div>
																</div>
																<div class="clearfix"></div>
															</div>
															<!-- appointment border -->
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>	
											<!-- edm_occurance input end here --> 
											
											<!-- edm send time input start here -->
											<div class="form-box">
												<label><b><?php echo lang('edm_Time');?></b><i style="color:red;">*</i></label>
												<div class="half_date mb10">
													<div class="form-group input-append date" id="start_time_div">
														<?php
														$data	= array('name'=>'send_time', 'data-format'=>'hh:mm:ss', 'value'=>set_value('start_time', (!empty($edm_data['send_time'])?$edm_data['send_time']:'')),'id'=>'start_time','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_time'), 'readonly'=>'readonly', 'class'=>'required');
														echo form_input($data);
														?>
														<span class="add-on" id="start_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-time"> </i> </span>
													</div>
												</div>
											</div>
											<!-- edm send time input end here --> 
											
											<!-- read email block input start here -->
											<div class="form-box">
												<label><b><?php echo lang('is_always_send_to_all_users');?></b><span class="fa fa-info-circle ml5" title="<?php echo lang('is_always_send_to_all_users_help');?>"></span></label>
												<div class="mb10">
													<input  type="checkbox" class="rec3" name="is_always_send_to_all_users" data-type='bottom' value="1" <?php echo (!empty($is_always_send_to_all_users)) ? "checked" : "";?>>
												</div>
											</div>
											<!-- read email block input end here -->
										</div>
					</div>
					</div>
				</div>
					<!-- submit button here -->
					<div class="form-box pb-3">
					<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
					<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
					</div>
					<!-- submit button here -->	
					<div class="clearboth"></div>
			</div>
			<!-- End frequency panel -->




									
			<!-- Start referral system panel -->
			<div class="tab-pane co_products_tab form-wrapper" id="receiver_tab">
				<div class="p-t-20">
					<div class="row">
					<div class="col-sm-12">
											<!-- invite points input start here -->
											<div class="col-sm-4 form-box edm-receiver radio-homerec ">
												<input  type="radio" class="rec2 receiver_type" id="all_user" name="rec_type[]" data-type='bottom' onchange="cdradio(this)"  data-show="rec-home" value="2"  <?php echo (empty($edm_id))?"checked":((in_array(2,$rec_type))?"checked":"");?> >
												<label class="custom-control-label" for="all_user">
												 <?php echo 'All User';?>
												</label>
											</div>
											<!-- invite points input end here -->	
											
											<!-- referral status dropdown start here -->
											<div class="col-sm-4 form-box edm-receiver radio-homerec">
												<div class="edm-p-relative">
													<input  type="radio" class="rec1 receiver_type" id="specific_home_store" name="rec_type[]" data-type='bottom' onchange="cdradio(this)" data-show="rec-home" value="1" <?php echo (in_array(1,$rec_type))?"checked":"";?> >
													<label class="custom-control-label" for="specific_home_store"><?php echo 'Specific Home Store';?></label>
													<span class="rec-home <?php echo (!empty($rec_home_store_id) && (in_array(1,$rec_type)))?"":"dn"; ?>">
															<dl class="edm-dropdown" id="store"> 
																<dt>
																<a href="#">
																   <?php echo $SelectStore; ?>      
																  <p class="edm-multiSel"><?php echo $StoreName; ?></p>  
																</a>
																</dt>
																<dd>
																	<div class="mutliSelect"  name="store">
																		<ul>
																			<?php echo $StoreHtml; ?>
																		</ul>
																	</div>
																</dd>
															</dl>
													 </span>
												</div>	
											</div>
											<!-- referral status dropdown end here -->
											
											<!-- Manual email input start here -->
											<div class="col-sm-4 form-box edm-receiver radio-homerec">
												<input  type="radio" class="rec2 receiver_type" id="manual_email" name="rec_type[]" data-type='bottom' onchange="cdradio(this)"  data-show="rec-home" value="5" <?php echo ((in_array(5,$rec_type))?"checked":"");?> >
												<label class="custom-control-label" for="manual_email">
													<?php echo 'Manual Email(s)';?>
												</label>
												<div class="manual_import_email <?php echo (in_array(5,$rec_type)) ? "":"dn"; ?>">
													<div class="fleft clear">
														<input type="file" class="csv_input" id="csvfile" />  
													</div>
													<div class="fleft clear">
														<button type="button" class="btn btn-primary uploadcsv">Import Email CSV</button>
													</div>	
												</div>
											</div>
											<div class="form-box manual_import_email <?php echo (in_array(5,$rec_type)) ? "":"dn"; ?>" id="csvemails">
												<?php
												// show brand products
												$email_manuals = (!empty($edm_data['manual_emails'])?$edm_data['manual_emails']:'');
												$emails = array_filter(explode(',',$email_manuals));
												foreach($emails as $email) { ?>
													<span class="edm-chosen-choices fl ml5">&nbsp;
														<span>
															<?php echo $email;?>
														</span>&nbsp;&nbsp;
														<span class="search-choice-close crp" email_id="<?php echo $email;?>" >
															<strong> X </strong>
														</span>&nbsp;
													</span>
												<?php }?>
											</div>
											<?php
											$data = array('type'=>'hidden','name'=>'manual_emails','id'=>'manual_emails','value'=>$email_manuals);
											echo form_input($data);?>
											<!-- Manual email input end here -->
										<div class="preward-class">	
											<!-- invite points limit input start here -->
											<div class="form-box edm-receiver previosly_purchased non_manual_email <?php echo ($product_add_type == 0 || (in_array(5,$rec_type))) ? 'dn' : '';?>">
												<input  type="checkbox" id="previously_purchased" class="rec3" name="rec_type[]" data-type='bottom' value="3" <?php echo (in_array(3,$rec_type))?"checked":"";?>>
												<label class="custom-control-label" for="previously_purchased" ><?php echo 'Previously Purchased';?></label>
											</div>
											<!-- invite points limit input end here -->
											
											<!-- invite points limit input start here -->

											<div class="form-box edm-receiver reward_cards non_manual_email <?php echo (in_array(5,$rec_type)) ? 'dn' : '';?>">
												<input  class="main-rec" id="reward_card_type" name="rec_type[]" onchange="cbchange(this)" data-show="sub-rec" type="checkbox" class="rec4" data-show="" value="4" <?php echo (in_array(4,$rec_type))?"checked":"";?>>
												<label class="custom-control-label" for="reward_card_type"><?php echo 'Reward Card Type';?></label>
												<span class="sub-rec  <?php echo (!empty($rec_reward_card_type))?"":"dn"; ?> ">
													<ul class="edm-rewardcard-value" >
													   <?php 
													   if(!empty($rewardCard_data)){
													   foreach($rewardCard_data as $cards){
													   ?>
													   <li><input  name ="rec_rc[]" type="checkbox" class="rec4_<?php echo $cards->id;?>" id="rec4_<?php echo $cards->id;?>" data-type='bottom' <?php echo (in_array($cards->id,$rec_reward_card_type))?'checked':''; ?> data-show="" value="<?php echo $cards->id; ?>" > <label class="custom-control-label" for="rec4_<?php echo $cards->id;?>" >
													   	<?php echo ($cards->card_name)?$cards->card_name:''; ?>
													   </label> </li>
													   <?php 
														} 
													}?>
													</ul> 
												</span>
											</div>
										</div>
											<!-- invite points limit input end here -->
				    </div>
			    	</div>							
			    </div>							
				<!-- submit button here -->
				<div class="form-box pb-3" style="padding:10px 0;">
				<button type="button" onclick="backnext()" name="cancel" class="btn btn-info back-btn"><?php echo 'Back';?></button> 
				<button type="submit" name="submit"   class="btn btn-primary"><?php echo 'Submit';?></button>
				</div>
				<!-- submit button here -->	
				<div class="clearboth"></div>
			</div>
			<!-- End referral system panel -->



									
									<div class="clearboth"></div>
								</div>	
								
							<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	
<!-- start the bootstrap modal where the other product appear -->
	<div class="modal fade dn" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">Product Search</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
				</div>
				<div class="modal-body">
                  <div class="tab-pane" id="product_tab">
                  	<select id="productType" name="productType" style="width:100px;margin-right:10px" class="fr">
                        <option value="id">Number</option>
                        <option value="name">Name </option>
                    </select>		
                    <input type="text" placeholder="Product Id / Product Name" name="product_direct_search" id="product_direct_search" style="width:200px;margin-right:10px" class="fr" >
                    <button onclick="product_search()" class="btn default add-product-btn fr"  type="button product_direct_search_button" style="margin-top:0px">Product Search</button>
                        <div class="clearboth"></div>
                        <div id="error_message" style="font-size:11px;color:red"></div> 
                   
                            <!-- Product list / form  start here -->
                            <div class="clearboth product-grid-form p-t-20" id="appendProductTable">
                                <table class="table table-bordered" id="product_tables"> 
                                    <thead class="bg_ccc">
                                        <tr>
                                            <th class="width:20px"><input type="checkbox" name="all_product_check" value="1" id="all_product_check"  ></th>
                                            <th class='product_table_th'><?php echo lang('product_number');?></th>
                                            <th class='product_table_th'><?php echo lang('product_name');?></th>
                                            <th class='width45'></th>
                                        </tr>
                                    </thead>
                                  
                                </table>
                                <!-- Product fields end here -->
                            </div>
                        
                </div> 
				</div>
				<div class="modal-footer">
                    <button onclick="add_into_filter()" class="btn btn-primary add-on-filter"  type="button">Add Into Filter</button>	
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end the bootstrap modal where other product will appear -->

	<!-- start the bootstrap modal where other product images will appear -->
	<div class="modal fade dn" id="productImageModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="myModalLabel">Select Product Image</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</div>
				      <div class="modal-body">

                    <ul class="nav nav-tabs customtab2" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#chooseImage" aria-controls="chooseImage" role="tab" data-toggle="tab" onclick="show_modal_footer()">Select Image</a>
                        </li>

                        <li class="nav-item"><a class="nav-link" href="#addImage" aria-controls="addImage" role="tab" data-toggle="tab" onclick="hide_modal_footer()">Add New Image</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="chooseImage">
							<?php
							/*
							$image_path = base_url().'uploads/image_library';

							if(!empty($get_all_images)){
							echo "<table class='table table-striped'><thead><tr><th>Select</th><th>Name</th><th>View</th></tr></thead>"; 
							$i = 1;
							foreach ($get_all_images as $value) {

							$url = $image_path.'/'.$value['name'];

							echo "<tr><td><input  type='radio' class='custom-control-input' name='select_product_image' id=radio_".$i." value=". $value['name'] ."><label class='custom-control-label' for=radio_".$i."></label></td>
							<td>". $value['name'] ."</td>
							<td><img src=". $url ." width='50'></td>
							</tr>";

							$i++;
							}

							echo "</table>"; 

							}
							*/
							?>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="addImage">

							<!-- <div class="upload_single_image_section"> -->
							<?php echo form_open_multipart('',array('name'=>'save_product_image','id'=>'save_product_image')); ?>
							<h3>Add New Image</h3>
							<?php
							$banner_image_src = base_url('uploads/No_Image_Available.png');
							?>
							<div class="cell-img" id="product_image_cell">
							<img id="product_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
							</div>
							<?php
							$data = array('name'=>'product_image', 'id'=>'product_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_product_image(this)');
							echo form_upload($data);
							// set banner image value in hidden type
							$data = array('name'=>'product_image_val', 'value'=>$banner_image_val,'id'=>'product_image_val','type'=>'hidden');
							echo form_input($data);?>
							<div class="upload_image_section">
							<input type="submit" id="submitButton" class="btn btn-primary"  name="submitButton" value="Upload">
							</div>

                        </div>
                    </div>
                
				      <input type="hidden"  name="current_product_id" value="">
				       <!-- <button type="button" onclick="show_upload_image();" class="btn btn-default pull-right">Add New Image</button> -->
				      </div>
				      <div class="modal-footer upload-image-modal-footer">
				      <button type="button" id="submit_product_image" data-dismiss="modal" class="btn btn-primary">Submit</button>
				      <button type="button" class="btn btn-info back-btn" data-dismiss="modal">Close</button>
				      </div>
				    </div>

				  </div>
				</div>
<!-- start the bootstrap modal where other product images will appear -->

<script>

		$('#start_date').bootstrapMaterialDatePicker({
			weekStart: 0,
			time: false,
			format : 'YYYY-MM-DD'
		});
      
		$('#end_date').bootstrapMaterialDatePicker({
			weekStart: 0,
			time: false,
			format : 'YYYY-MM-DD'
		});

		$('#yearly_time').bootstrapMaterialDatePicker({
			weekStart: 0,
			time: false,
			format : 'YYYY-MM-DD'
		});

		$('#start_time').bootstrapMaterialDatePicker({
			date: false,
			shortTime: false,
			format: 'HH:mm'
		});

	var productIDArray = [];
	var productNAMEArray = [];
	var productPRICEArray = [];
	var productSPRICEArray = [];
	var productIMAGEArray = [];
	var productDepartments = [];
	var product_ids = '<?php echo $product_ids; ?>';
	var sell_based_product_ids = '<?php echo $sell_based_product_ids; ?>';
	var product_names = '<?php echo $product_names; ?>';
	var product_prices = '<?php echo $product_prices; ?>';
	var special_prices = '<?php echo $special_prices; ?>';
	var product_images = '<?php echo $product_images; ?>';
	if(product_ids != '') {
		productIDArray = JSON.parse(product_ids);
	}
	if(sell_based_product_ids != '') {
		productIDArray = JSON.parse(sell_based_product_ids);
	}
	if(product_names != '') {
		productNAMEArray = JSON.parse(product_names);
	}
	if(product_prices != '') {
		productPRICEArray = JSON.parse(product_prices);
	}
	if(special_prices != '') {
		productSPRICEArray = JSON.parse(special_prices);
	}
	if(product_images != '') {
		productIMAGEArray = JSON.parse(product_images);
	}
	var EmailProducts = {};
   
	$(document).ready(function() {
		

		// remove other product and manage template products
	$(document).delegate('.remove-btn','click',function(e){
		var row_id = $(this).attr('row_id');
		var filtered_other_products = new Array();
		var product_ids_val = '';
		if(productIDArray.length > 0) {
			for(var i=0;i<productIDArray.length;i++){
				if(row_id != productIDArray[i]) {
					product_ids_val += productIDArray[i]+',';
					var ProductArray = {};
					ProductArray['product_id'] = productIDArray[i];
					ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
					ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
					ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
					filtered_other_products.push(ProductArray);
				}
			}
		}
		var deleted_product_id = [];
		$('#product_other').val(product_ids_val);
		var pre_deleted_product_id = $('#deleted_product_id').val();
		var deleted_product_id = pre_deleted_product_id+row_id+',';
		$('#deleted_product_id').val(deleted_product_id);

		// convert products array to json
		var edm_products_json = JSON.stringify(filtered_other_products);
		$('#edm_products_json').val(edm_products_json);
		$('#edm_sell_base_products_json').val(edm_products_json);
		$(this).closest('tr').remove();
		//$('table#product-table tr#'+row_id).remove();
		//$('table#sellbased-product-table tr#'+row_id).remove();
		$('input:checkbox[value="' + row_id + '"]').prop("checked", false);

		var product_exist = $('#product_exist').val();
		var product_exist = removeValue(product_exist , row_id , ',');
		$('#product_exist').val(product_exist);
	});



   });
	
	function show_loader(action,className){
		var div = "<div class='loader loaderdiv'><i class='fa fa-refresh fa-spin' style='font-size: 28px;color: #111;position: absolute;left: 50%;bottom: 50%;transform: translate(-50%,-50%);'></i></div>";
		if(action){
			$('.'+className).css({"position": "relative","padding": "0px"});
			$('.'+className).prepend(div);
		}else{
			$('.'+className).css({"position":"","padding": ""});
			$('.loaderdiv').remove();
		}
	}	
	
	$(document).delegate(".otherPr", "click", function(e){
		$('#productmodal').modal('show');
	});
	
	/* Hide and Show manual product box*/     
    $(document).on('click','#is_product_group',function(){
        if($(this).is(':checked')){
		$('#productgropuShowHide').show();
        }else{
			$('#productgropuShowHide').hide();
			$('#product_tab_link').hide(); // Hide Product Groups
			$('#filter_tab_link').show(); // Hide Product Groups
        } 
    });
	
   /* 
	| -----------------------------------------------------
	| Manage add quantity row 
	| -----------------------------------------------------
	*/ 
	function product_search() {
		// get last product row number
		var product_direct_search = $('#product_direct_search').val();
		var productType = $('#productType').val();
        var exclude_cat = $('#exclude_cat').val();
        if(product_direct_search == ""){
			//bootbox.alert("Empty search is not valid");
			$('#error_message').show();
			$('#error_message').html("Empty search is not valid");
			$('#error_message').hide(4000);
			$('#product_direct_search').val('');
			return false;
        }
        
        if(productType == 'id'){
            if(isNaN(product_direct_search)){
                //bootbox.alert("Invalid product number");
                $('#error_message').show();
                $('#error_message').html("Invalid product number");
                $('#error_message').hide(4000);
                $('#product_direct_search').val('');
                return false;
            }else{
               //alert('Input OK');
            }
        }
        $('#loader1').fadeIn();
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {product_direct_search:product_direct_search,productType:productType,department_id:exclude_cat},
			url  : BASEURL+"admin/settings/targetpush/search_product_direct",
			success: function(data){
				$('#appendProductTable').html(data.html);
				$('#loader1').fadeOut();
			}
		});
	}
	
   /* 
	| -----------------------------------------------------
	| Manage add filter product row 
	| -----------------------------------------------------
	*/ 
	
	function add_into_filter() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
		var row;
		var product_id;
		var product_ids_val = '';
		var product_name;
		var product_price;
		var special_price;
		var product_qty;
		var html = '';
		var other_products = [];

		$('#email_products').html('');
		var image_path = BASEURL+'uploads/No_Image_Available.png';
		var deleted_ids = new Array();
		var deleted_ids = $('#deleted_product_id').val();
		//alert(productIDArray);
		//alert(deleted_ids);
		var deleted_ids = deleted_ids.split(',');
		var myPROArray = productIDArray.filter( function( el ) {
		return deleted_ids.indexOf( el ) < 0;
		} );
		//alert(myPROArray);
		productIDArray = myPROArray;

		var j = productIDArray.length;
		$('#loader1').fadeIn();
		$( ".single_product_check:checkbox:checked" ).each(function( index ) {
			row = parseInt($(this).attr('qty_row'));
			product_name = $(this).attr('product_name_'+row);
			product_qty = $(this).attr('product_qty_'+row);
			product_department = $(this).attr('prod_department_'+row);
			product_id = parseInt($(this).val());
			if(jQuery.inArray(product_id,productIDArray) < 0){
				productIDArray[j] = product_id;
				productNAMEArray[product_id] = product_name;
				productPRICEArray[product_id] = product_price;
				productSPRICEArray[product_id] = special_price;
				productDepartments[product_id] = product_department;
				j++;
			}
		});
		
		if(productIDArray.length > 0) {
			for(var i=0;i<productIDArray.length;i++){
				var image_paths = BASEURL+'uploads/image_library/'+productIMAGEArray[productIDArray[i]];
				if(productIMAGEArray[productIDArray[i]] == null){
					var image_paths = BASEURL+'uploads/No_Image_Available.png';
					productIMAGEArray[productIDArray[i]] = '';
				}if(productPRICEArray[productIDArray[i]] == null){
					productPRICEArray[productIDArray[i]] = '';
				}if(productSPRICEArray[productIDArray[i]] == null){
					productSPRICEArray[productIDArray[i]] = '';
				}
				product_ids_val += productIDArray[i]+',';
				var product_tr = '';
				product_tr += '<tr id="'+productIDArray[i]+'">';
				product_tr += '<td><input type="text" readonly name="product_id[]" value="'+productIDArray[i]+'" class="form-control readonly_input width100"></td>';
				product_tr += '<td><input type="text" readonly name="product_name[]" value="'+productNAMEArray[productIDArray[i]]+'" class="form-control readonly_input width100"></td>';
				product_tr += '<td><input type="text" name="product_price[]" value="'+productPRICEArray[productIDArray[i]]+'" id="product_price_'+productIDArray[i]+'" class="form-control required product_input" row_id="'+productIDArray[i]+'"></td>';
				product_tr += '<td><input type="text" name="special_price[]" value="'+productSPRICEArray[productIDArray[i]]+'" id="special_price_'+productIDArray[i]+'" class="form-control required product_input" row_id="'+productIDArray[i]+'"></td>';

				product_tr += '<td class="upload_product_image"><img src="'+image_paths+'" class="product_image_preview_'+productIDArray[i]+'" id="select_product_button_'+productIDArray[i]+'" aria-hidden="true" data-name="" data-id="'+productIDArray[i]+'" data-toggle="modal" data-target="#productImageModel"><input type="hidden" name="product_image[]" value="'+productIMAGEArray[productIDArray[i]]+'" id="product_image_'+productIDArray[i]+'"></td>';


				product_tr += '<td><a class="common-btn remove-btn" row_id='+productIDArray[i]+'><i class="fa fa-trash"></i></a></td>';
				$('#product-table').append(product_tr);
				var ProductArray = {};
				ProductArray['product_id'] = productIDArray[i];
				ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
				ProductArray['product_department'] = productDepartments[productIDArray[i]];
				ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
				ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
				ProductArray['product_image'] = $('#product_image_'+productIDArray[i]).val();
				other_products.push(ProductArray);
			}
		}
		
		if(productIDArray != ""){
			$('#product_other').val(product_ids_val);
		}
		if(productIDArray != ""){
			$('#product_exist').val(product_ids_val);
		}
		var edm_products_json = '';
		if(other_products.length > 0){
			$('#productmodal').modal('hide');
			$('#productImageModel').modal('hide');
			// convert products array to json
			var edm_products_json = JSON.stringify(other_products);
			$('#product-table').show();
		}
		$('#edm_products_json').val(edm_products_json);
		//alert(edm_products_json);
		$('#loader1').fadeOut();
   }
 
   /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".search-choice-close","click",function(e){
		$(this).parent('span').remove();
		var prod_id = $(this).attr('prod_id');
		var product_ids = $('#product_other').val();
		if(prod_id != '') {
			product_ids = product_ids.replace(prod_id+',','');
			$('#product_other').val(product_ids);
		}
    });
    
   	$( "#daily").click(function() {
		$('#daily_div').css("display","block");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","none");  
	});

	$("#weekly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","block");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","none");
	});

	$("#monthly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","block");
		$('#yearly_div').css("display","none");
	});

	$("#yearly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","block");
	});
	
	function cdradio(res){
		var showdiv = $(res).attr('data-show');
		var res_val = parseInt($(res).val());
		var product_add_type = $("input[name='product_add_type']:checked").val();
		if(showdiv!= ''){
			if(res.checked){
				if(res_val === 1){
					$('.'+showdiv).show();
					$('.manual_import_email').hide();
					$('.reward_cards').show();
					if(product_add_type != 0) {
						$('.previosly_purchased').show();
					}
				}else if(res_val === 5) {
					$('.'+showdiv).hide();
					$('.manual_import_email').show();
					$('.non_manual_email').hide();
				}else{
					$('.'+showdiv).hide();
					$('.manual_import_email').hide();
					$('.reward_cards').show();
					if(product_add_type != 0) {
						$('.previosly_purchased').show();
					}
				}
			}
		}
	}
	
	function cbchange(res){
		var showdiv = $(res).attr('data-show');
		if(showdiv!= ''){
			if(res.checked){
				$('.'+showdiv).fadeIn('slow');	
			}else{
				$('.'+showdiv).fadeOut('slow');	
			}
		}
	}
	
	// Manage product pick box availability
	$("input[name='product_add_type']").change(function(){
		var product_add_type = $(this).val();
		if(product_add_type == 1) {
			$('#product_direct_searchbox').show();
			$('.include-products').show();
			$('#sellbased_product_searchbox').hide();
			$('.previosly_purchased').show();
		} else if (product_add_type == 2){
			// manage product box visibility
			$('#sellbased_product_searchbox').show();
			$('.include-products').show();
			$('#product_direct_searchbox').hide();
			$('.previosly_purchased').show();
		} else {
			$('#product_direct_searchbox').hide();
			$('.include-products').hide();
			$('#sellbased_product_searchbox').hide();
			$('.previosly_purchased').hide();
		}
	});
	
	$(document).ready(function(){
		$("#frm_edm").validate({
			rules: {
				"rec_type[]": { 
						required: true, 
						minlength: 1 
				},
				"rec_rc[]": { 
						required: true, 
						minlength: 1 
				},
				no_products: { 
					min: 1
				} 
			}, 
			messages: { 
					"rec_type[]": "Please select at least one types of receiver.",
					"rec_rc[]": "Please select at least one reward card type."
			},
			submitHandler: function() {
				var activeTab = $("ul.nav-tabs").find(".active").attr('data-tab');
				switch(activeTab) {
					case 'receiver_tab':
					    // Validate home store selection in case of receiver type selection
						var rec_type = $('input.receiver_type:checked').val();
						if(rec_type == 1) {
							var home_stores = $('input.homeStores:checked').length;
							if(home_stores  == 0) {
								bootbox.alert("Please Select Home Store(s)!");
								return false;
								break;
							}
						} else if(rec_type == 5) {
							var manual_emails = $('#manual_emails').val();
							if(manual_emails  == '' || manual_emails  == ',') {
								bootbox.alert("Please Upload Valid Email CSV!");
								return false;
								break;
							}
						}
						// Manage store procedure
						savedata();
					break;
					case 'product_tab':
						var product_required = false;
						var product_search_type = $("input[name='product_add_type']:checked").val();
						if(product_search_type != 0) {
							var product_input_cls = 'sell_product_input';
							if(product_search_type == 1) {
								var product_other = $('#product_other').val();
								var product_exist = $('#product_exist').val();
								if(product_other == '' || product_exist == '' ) {
									product_required = true;
								}
								product_input_cls = 'product_input';
							} else {
								var topSellingProducts = $('input.topSellingProducts:checked').length;
								var bottomSellingProducts = $('input.bottomSellingProducts:checked').length;
								product_required = true;
								if( topSellingProducts > 0 || bottomSellingProducts  > 0) {
									product_required = false;
								}
							}
							// check product price validation
							var is_validate = true;
							$( "."+product_input_cls ).each(function( e ) {
								var row_id = $(this).attr('row_id');
								var product_price = $('#product_price_'+row_id).val();
								var special_price = $('#special_price_'+row_id).val();
								var product_image = $('#product_image_'+row_id).val();
								
								if(product_price == '' || isNaN(product_price) ) {
									$('#product_price_'+row_id).addClass('error');
									is_validate =false;
								}
								if(special_price == '' || isNaN(special_price)) {
									$('#special_price_'+row_id).addClass('error');
									is_validate = false;
								}
								if(product_image == '') {
									//$('#select_product_button_'+row_id).addClass('error');
									is_validate = false;
								}
							});
							if(is_validate == false) {
								bootbox.alert("Please fill all required attributes.");
								return false;
							} else {
								if(product_search_type ==2){
									manage_product_json();
								} else {
									manage_other_product_json();
								}
							}	
						}
						// prapare template body 
						var template_type = $('#template_type').val();
						if(template_type != '') {
							prepare_email_template();
						}
						if(product_required) {
							bootbox.alert("Please select product(s) first!");
							return false;
							break;
						}
					case 'info_tab': 
						// set end date for calculate day diffrence
						var end_time = $( "#end_date" ).val().split(' ');
						if(end_time != '') {
							var split_end_date = end_time[0].split("-");
							var new_end_date   = split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0];
							// set start date for calculate day diffrence
							var start_date = $( "#start_date" ).val().split(' ');
							var split_date 		= start_date[0].split("-");
							var new_start_date	= split_date[2]+'/'+split_date[1]+'/'+split_date[0];	
							
							// init in dates formate
							var sDate =  new Date(new_start_date);
							var eDate =  new Date(new_end_date);
							
							// show error if start date less then end date
							if(start_date != '' && sDate >= eDate ) {
								bootbox.alert("End date must be greater than start date");
								$( "#end_date" ).val('');
								return false;
								break;
							} 
						}
						// get description
						var email_description = tinymce.get('email_description').getContent();
						if(email_description == '') {
							bootbox.alert("Email Description should not be blank!");
							return false;
						}
						var existing_top_products = '';
						var existing_bottom_products = '';
						var no_of_top_products = $('#no_of_top_products').val();
						var no_of_bottom_products = $('#no_of_bottom_products').val();
						<?php if(!empty($product_top_val)) { ?>
							existing_top_products = JSON.parse('<?php echo $product_top_val;?>');
						<?php } 
						if(!empty($product_bottom_val)) { ?>
							existing_bottom_products = JSON.parse('<?php echo $product_bottom_val;?>');
						<?php } ?>
						//manage_selling_product(no_of_top_products,1,existing_top_products);
						//manage_selling_product(no_of_bottom_products,2,existing_bottom_products);
						// show product multiselect dropdown values
						$('.ulProductHtml_1').show();
						$('.ulProductHtml_2').show();
					case 'frequency_tab': 
						var occurance_type = $("input[name='occurance_type']:checked").val();
						occurance_selected_days = 1;
						if(occurance_type == 2) {
							occurance_selected_days = $('input.weekly_days:checked').length;
						} else if(occurance_type == 3) {
							occurance_selected_days = $('input.monthly_days:checked').length;
						}
						if(occurance_selected_days  == 0) {
							bootbox.alert("Please select occurance day(s)!");
							return false;
							break;
						}
					default:
						backnext(true);
				}
			}
		});

		
		$("#productImageModel").on("hidden.bs.modal", function () {
			$("#product_image_picker").attr("src",BASEURL+'uploads/No_Image_Available.png');
		});

	});	
	
	// Manage form store procedure
	function savedata(){
		var formData = new FormData($('#frm_edm')[0]);
		$('#loader1').fadeIn();
		$.ajax({
			url: BASEURL+"admin/settings/edm/edm_configration",
			type: 'POST',
			data: formData,
			async: false,
			success: function (data) {
				$('#loader1').fadeOut();
				window.location.href = BASEURL+"admin/settings/edm";
			},
			cache: false,
			contentType: false,
			processData: false
		}, "json");
		return false;
	}
	
	function createEdmTemplate(){
		backnext(true);
		var fromData=$("#frm_edm").serialize();	
		$.post(BASEURL+"admin/settings/edm/edm_template_config",fromData, function(data){
			if(data.result){
				//$('#body').val(data.view);
				CKEDITOR.instances['body'].setData(data.view);
				show_loader(0,'min-loader');
			}
		}, "json");
	}

	function backnext(type){
		var activeTab = $("ul.edm-tabs").find(".active");
		//remove from current
		$(activeTab).removeClass("active");
		$("#"+$(activeTab).attr('data-tab')).removeClass("active");
		//add next 
		if(type){
			$(activeTab).next().addClass('active');
			$("#"+$(activeTab).next().attr('data-tab')).addClass('active');	
		}else{
		//add next 
			$(activeTab).prev().addClass('active');
			$("#"+$(activeTab).prev().attr('data-tab')).addClass('active');
		}
	}

	$(".edm-dropdown dt a").on('click', function() {
		//console.log($(this).parent().next().find('.mutliSelect')..slideToggle('fast'));
		$(this).parent().next().find('.mutliSelect ul').slideToggle('fast');
		//$(".edm-dropdown dd ul").slideToggle('fast');
		//$(this).parent().next("dd ul").slideToggle('fast');
	});
	
	$(".edm-dropdown dd ul li a").on('click', function() {
		$(this).parent().next().find('.mutliSelect ul').hide();
		//$(".dropdown dd ul").hide();
	});

	function getSelectedValue(id) {
		return $("#" + id).find("dt a span.value").html();
	}

	$(document).bind('click', function(e) {
		//var $clicked = $(e.target);
		//if (!$clicked.parents().hasClass("edm-dropdown")) $(".edm-dropdown dd ul").hide();
	});

	$('.mutliSelect input[type="checkbox"]').on('click', function() {
		var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').attr('data-name'),
		title = $(this).attr('data-name') + ",";
		if ($(this).is(':checked')) {
			var html = '<span title="' + title + '">' + title + '</span>';
			// $(this).parent().find('.multiSel').append(html);
			//$('.multiSel').append(html);
			$(this).closest('.edm-dropdown').find('.edm-multiSel').append(html);
			$(".hida").hide();
		} else {
			$('span[title="' + title + '"]').remove();
			var ret = $(".hida");
			$(this).closest('.edm-dropdown').find('dt a').append(ret);
			//$('.dropdown dt a').append(ret);
		}
	});
	
	// fetch top or bottom products on the basis of excluded categories
	$("#exclude_cat").on("blur", function(){
		var product_search_type = $("input[name='product_add_type']:checked").val();
		if(product_search_type == 1) {
			var exclude_cat = $('#exclude_cat').val();
			var department_ids = exclude_cat.split(',');
			var product_json = $('#edm_products_json').val();
			if(product_json != '' && department_ids.length > 0) {
				var products = $.parseJSON(product_json);
				var product_count = products.length;
				for(var i=0;i<product_count;i++) {
					var product_department = products[i]['product_department'];
					if(jQuery.inArray(product_department, department_ids) !== -1) {
						var row_id = products[i]['product_id'];
						var filtered_other_products = new Array();
						var product_ids_val = '';
						if(productIDArray.length > 0) {
							for(var i=0;i<productIDArray.length;i++){
								if(row_id != productIDArray[i]) {
									product_ids_val += productIDArray[i]+',';
									var ProductArray = {};
									ProductArray['product_id'] = productIDArray[i];
									ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
									ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
									ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
									filtered_other_products.push(ProductArray);
								}
							}
						}
						$('#product_other').val(product_ids_val);
						// convert products array to json
						var edm_products_json = JSON.stringify(filtered_other_products);
						$('#edm_products_json').val(edm_products_json);
						$('table#product-table tr#'+row_id).remove();
					}
				}	
			}
		}else if(product_search_type == 2) {
			var existing_top_products = '';
			var existing_bottom_products = '';
			var no_of_top_products = $('#no_of_top_products').val();
			var no_of_bottom_products = $('#no_of_bottom_products').val();
			<?php if(!empty($product_top_val)) { ?>
				existing_top_products = JSON.parse('<?php echo $product_top_val;?>');
			<?php } 
			if(!empty($product_bottom_val)) { ?>
				existing_bottom_products = JSON.parse('<?php echo $product_bottom_val;?>');
			<?php } ?>
			manage_selling_product(no_of_top_products,1,existing_top_products);
			manage_selling_product(no_of_bottom_products,2,existing_bottom_products);
		}
	});
	
	// display top or bottom selling products in dropdown 
	$(".product_selling_fetch_limit").on("blur", function(){
		var product_limit =  $(this).val();
		var product_selling_type = $(this).attr('product_selling_type');
		var field_id = $(this).attr('id');
		var prev_limit = $('#'+field_id+'_hidden').val();
		if(prev_limit != product_limit) {
			$('#'+field_id+'_hidden').val(product_limit);
			var existing_products = '';
			if(product_selling_type == 1) {
				<?php if(!empty($product_top_val)) { ?>
					existing_products = JSON.parse('<?php echo $product_top_val;?>');
				<?php } ?>
			} else {
				<?php if(!empty($product_bottom_val)) { ?>
					existing_products = JSON.parse('<?php echo $product_bottom_val;?>');
				<?php } ?>
			}
			if(product_limit !== 0 || product_limit !== ''){
				manage_selling_product(product_limit,product_selling_type,existing_products);
			}
		}
	});
	
	// manage dynamic product dropdown appearance
	function manage_selling_product(product_limit,selling_product_type,existing_products) {
		var exclude_cat = $('#exclude_cat').val();
		var department_id = '';
		if(exclude_cat != '' && exclude_cat != ',' ) {
			department_id = exclude_cat;
		} 
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: '<?php echo site_url("admin/settings/edm/get_products_selling_dropdown"); ?>',
			data: {product_limit:product_limit, selling_product_type:selling_product_type, existing_products : existing_products, department_id : department_id},
			beforeSend: function() {
				$('#loader1').fadeIn();	
			},
			success: function(data){
				if(data.result){
					$('.selectProducts_'+selling_product_type).html(data.selectedProducts);
					$('.ulProductHtml_'+selling_product_type).html(data.ulProductHtml);
					if(data.no_more_existing_products != '') {
						var non_existing_products = JSON.parse(data.no_more_existing_products);
						if(non_existing_products.length > 0) {
							for(var i=0;i<non_existing_products.length;i++){
								$('table#sellbased-product-table tr#'+non_existing_products[i]).remove();
							}
						}
					}
					// show product multiselect dropdown values
					$('.ulProductHtml_1').show();
					$('.ulProductHtml_2').show();
				}
			},
			complete:function(){
				$('#loader1').fadeOut();	
			}
		});
	}
	
	$("#banner_image_picker").click(function() {
		$("input[name='email_banner_image']").click();
	});

	function read_url_banner_image(input) {
		
		var pickerId= '#'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		//$('#banner_image_val').val(input.value);
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 1.0955135) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 1MB!");
			}
		}
	}
	
	// Initialize description mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "edmmceEditor",
		height: '230px',
		width: '585px',
		theme_advanced_resizing: true,
		plugins: ['link image'],
		/*image_advtab: true,
		file_picker_callback: function(callback, value, meta) {
		if (meta.filetype == 'image') {
		$('#upload').trigger('click');
		$('#upload').on('change', function() {
		var file = this.files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
		callback(e.target.result, {
		alt: ''
		});
		};
		reader.readAsDataURL(file);
		});
		}
		},*/
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				if(edm_description != '' && edm_description != undefined) {
					$('.edm_description_tr').show();
					$('.edm_description').html(edm_description);
				}
			});
		}
	});
	
	// Initialize footer content mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "footermceEditor",
		height: '230px',
		width: '585px',
		theme_advanced_resizing: true,
		plugins: ['link'],
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				if(edm_description != '' && edm_description != undefined) {
					$('.email_footer_text_tr').show();
					$('.email_footer_text').html(edm_description);
				}
				
			});
		}
	});
	
	// Prepare products box inside email template
	function manage_product_grid() {
		var product_search_type = $("input[name='product_add_type']:checked").val();
		var no_products   = $('#no_products').val();
		var product_ids	  = new Array();
		var product_names = new Array();
		var product_json = '';
		var product_html = '';
		if(product_search_type == 1) {
			product_json = $('#edm_products_json').val();
		} else if(product_search_type == 2) {
			product_json = $('#edm_sell_base_products_json').val();
		}
		
		if(product_json != '') {
			var products = $.parseJSON(product_json);
			var product_count = products.length;
			if(no_products <= product_count){
				product_count = no_products;
			}
			product_html = set_template_html(product_count,products);
		}
		
		if(product_html != '') {
			$('.product_html_tr').show();
			$('.products').show();
			$('.products_grid').html(product_html);
		}	
	}
	
	// Prepare html templates base on type
	function set_template_html(product_count,products) {
		var template_type = $('#template_type').val();
		var product_html ='';
		if(template_type == 'grid_view') {
			var counter = 1;
			var total_product_count = product_count;
			for(var i=0;i<product_count;i++) {
				var product_price = $('#product_price_'+products[i]['product_id']).val();
				var special_price = $('#special_price_'+products[i]['product_id']).val();
				if(counter == 1) {
					product_html += '<tr><td><table border="0" width="100%" cellpadding="0" cellspacing="0" class="style" style="text-align: center;"><tr>';
				}
				product_html += '<td class="border-bottom" style="width:32%;background-color: #fefefe !important;border: 1px solid #ccc;border-radius: 4px;padding: 0px -1px 10px 12px;vertical-align: top;text-align: left;"><p style="text-align: center;"> <img width="160" height="71" src="'+BASEURL+'uploads/image_library/'+products[i]['product_image']+'"></p>';
				product_html += '<h4 style="text-align: center;"> <strong>'+products[i]['product_name']+'</strong> </h4><div style="text-align: center;" class="price-grid">';
				product_html += '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="50%" style="text-align: right;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;">List Price</td><td width="50%" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;font-family: "Courier New", Courier, monospace;text-align: left;font-weight: 600;padding-left: 4%;letter-spacing: 0px;">$'+product_price+'</td></tr>';
				product_html += '<tr><td width="50%" style="text-align: right;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;" class="text-right">Price</td><td width="50%" class="price-new">$'+special_price+'</td></tr></table></div>';
				product_html += '<div class="clearfix"></div><div style="text-align: center;height: 20px;"><a style="color: #e78b28;font-size: 11px; href="{VIEW_PRODUCT}'+products[i]['product_id']+'">Explore More</a> </div></td>';
				if(counter == 1 || counter == 2) {
					product_html += '<td style="width:10px;">&nbsp;</td>';
				}
				
				// setup additional coloumn in case of less then 3 TD
				if(total_product_count == 1 && counter < 3) {
					var remain_td = 1;
					if(counter == 1) {
						remain_td = 2;
					}
					for(var k=0;k<remain_td;k++) {
						product_html += '<td class="border-bottom" style="width:32%;">&nbsp;</td>';
						product_html += '<td style="width:10px;">&nbsp;</td>';
					}
					product_html += '</tr></table></td></tr>';
					product_html += '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
				}
				if(counter == 3) {
					product_html += '</tr></table></td></tr>';
					product_html += '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
					counter = 1;
				} else {
					counter++;
				}
				total_product_count--;
			}
		} else {
			for(var i=0;i<product_count;i++) {
				var product_price = $('#product_price_'+products[i]['product_id']).val();
				var special_price = $('#special_price_'+products[i]['product_id']).val();
				product_html += '<tr><td><table class="style list" width="100%" style="text-align: center;"><tr>';  
				product_html += '<td width="20%" class="vt"><img src="'+BASEURL+'uploads/image_library/'+products[i]['product_image']+'"></td>';
				product_html += '<td width="60%" style="vertical-align: top;"><h4 style="font-size: 14px;line-height: 15px!important; margin: 2px 0px 0px 0px;padding-left: 5%;color: #54667a;font-weight: 300;float: left;"> <strong>'+products[i]['product_name']+'</strong> </h4>';
				product_html += '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="40%" style="text-align: left;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">List Price</td><td width="" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;text-align: left;font-weight: 600;padding-left: 0;letter-spacing: 0px;">$'+product_price+'</td></tr>';
				product_html += '<tr><td width="40%" style="text-align: left;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">Price</td><td width="" class="price-new2" style="color: #ffcb00;font-size: 32px;font-weight: 600;line-height: 30px;letter-spacing: -3px;padding-left: 0;text-align: left;">$'+special_price+'</td></tr></table></td>';
				product_html += '<td width="20%" style="vertical-align: middle;"><div class="explore"> <a href="{VIEW_PRODUCT}'+products[i]['product_id']+'">Explore More</a> </div></td></tr></table></td></tr><tr><td height="20" style="font-size:20px; line-height:20px;"></td></tr>';
			}
		}
		return product_html;
	}
	
	// Manage template selection
	$(document).delegate(".choose","click",function(e){
		var id = $(this).attr('id');
		$('#'+id+'_template').removeClass('dn');
		$('#choose-template').addClass('display_none');
		$('.email-template-box').removeClass('display_none');
		$('#template_type').val(id);
		prepare_email_template();
	});
	
	// Prapare
	function prepare_email_template() {
		// prepare email template body
		var banner_img_val = $('#banner_image_val').val();
		if(banner_img_val != '') {
			var banner_img = $('#banner_image_picker').attr('src');
			$('.banner-img').show();
			var banner_image = '<img id="banner-image" src="'+banner_img+'" border="0" style="width:525px;">'
			$('.banner-img').html(banner_image);
		}
		// set product widget title
		var product_grid_title =  $("#product_grid_title").val();
		if(product_grid_title != undefined) {
			$('.product_widget_title_tr').show();
			$('.product_widget_title').html('<h4>'+product_grid_title+'</h4>');
		}
		var product_search_type = $("input[name='product_add_type']:checked").val();
		if(product_search_type != 0) {
			// prepare product grid in email template
			manage_product_grid();
		} else {
			$('.product_html_tr').hide();
			$('.products').hide();
			$('.products_grid').html('');
			$('#edm_products_json').val('');
			$('#edm_sell_base_products_json').val('');
			$('.product_widget_title_tr').hide();
		}
	}
	
	$(document).on('change','.topSellingProducts',function(){
		var product_id = $(this).val();
		var product_name = $(this).attr('data-name');
		var product_tr = '';
		var image_path = BASEURL+'uploads/No_Image_Available.png';
		if(this.checked) {
			product_tr += '<tr id="'+product_id+'">';
			product_tr += '<td><input type="text" readonly name="product_id[]" value="'+product_id+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" readonly name="product_name[]" value="'+product_name+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" name="product_price[]" id="product_price_'+product_id+'" class="form-control required number sell_product_input" row_id="'+product_id+'"></td>';
			product_tr += '<td><input type="text" name="special_price[]" id="special_price_'+product_id+'" class="form-control required number sell_product_input" row_id="'+product_id+'"></td>';
			product_tr += '<td class="upload_product_image"><img src="'+image_path+'" class="product_image_preview_'+product_id+'" id="select_product_button_'+product_id+'" aria-hidden="true" data-name="" data-id="'+product_id+'" data-toggle="modal" data-target="#productImageModel"><input type="hidden" name="product_image[]" value="" id="product_image_'+product_id+'"></td>';
			product_tr += '<td><a class="common-btn remove-btn" row_id='+product_id+'><i class="fa fa-trash"></i></a></td>';


			$('#sellbased-product-table').append(product_tr);
		} else {
			$('table#sellbased-product-table tr#'+product_id).remove();
		}
		var checked_top_products = $('input.topSellingProducts:checked').length;
		var checked_bottom_products = $('input.bottomSellingProducts:checked').length;
		if(checked_top_products > 0 || checked_bottom_products > 0) {
			$('#sellbased-product-table').show();
		} else {
			$('#sellbased-product-table').hide();
		}
	});
	
	$(document).on('change','.bottomSellingProducts',function(){
		var product_id = $(this).val();
		var product_name = $(this).attr('data-name');
		var product_tr = '';
		var image_path = BASEURL+'uploads/No_Image_Available.png';
		if(this.checked) {
			product_tr += '<tr id="'+product_id+'">';
			product_tr += '<td><input type="text" readonly name="product_id[]" value="'+product_id+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" readonly name="product_name[]" value="'+product_name+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" name="product_price[]" id="product_price_'+product_id+'" class="form-control required number sell_product_input" row_id='+product_id+'></td>';
			product_tr += '<td><input type="text" name="special_price[]" id="special_price_'+product_id+'" class="form-control required number sell_product_input" row_id='+product_id+'></td>';

			product_tr += '<td class="upload_product_image"><img src="'+image_path+'" class="product_image_preview_'+product_id+'" id="select_product_button_'+product_id+'" aria-hidden="true" data-name="" data-id="'+product_id+'" data-toggle="modal" data-target="#productImageModel"><input type="hidden" name="product_image[]" value="" id="product_image_'+product_id+'"></td>';
			product_tr += '<td><a class="common-btn remove-btn" row_id='+product_id+'><i class="fa fa-trash"></i></a></td>';

			$('#sellbased-product-table').append(product_tr);
		} else {
			$('table#sellbased-product-table tr#'+product_id).remove();
		}
		var checked_top_products = $('input.topSellingProducts:checked').length;
		var checked_bottom_products = $('input.bottomSellingProducts:checked').length;
		if(checked_top_products > 0 || checked_bottom_products > 0) {
			$('#sellbased-product-table').show();
		} else {
			$('#sellbased-product-table').hide();
		}
	});
	
	// Prepare selected products json
	function manage_product_json() {
		var product_ids	  = new Array();
		var product_names = new Array();
		var selling_products = new Array();
		$('input.topSellingProducts[type=checkbox]').each(function () {
			if(this.checked) {
				var ProductArray = {};
				ProductArray['product_id'] = $(this).val();
				ProductArray['product_name'] = $(this).attr('data-name');
				ProductArray['product_price'] = $('#product_price_'+ProductArray['product_id']).val();
				ProductArray['special_price'] = $('#special_price_'+ProductArray['product_id']).val();
				ProductArray['product_image'] = $('#product_image_'+ProductArray['product_id']).val();
				selling_products.push(ProductArray);
			}
		});
		
		$('input.bottomSellingProducts[type=checkbox]').each(function () {
			if(this.checked) {
				var ProductArray = {};
				ProductArray['product_id'] = $(this).val();
				ProductArray['product_name'] = $(this).attr('data-name');
				ProductArray['product_price'] = $('#product_price_'+ProductArray['product_id']).val();
				ProductArray['special_price'] = $('#special_price_'+ProductArray['product_id']).val();
				ProductArray['product_image'] = $('#product_image_'+ProductArray['product_id']).val();
				selling_products.push(ProductArray);
			}
		});
		
		var product_search_type = $("input[name='product_add_type']:checked").val();
		// convert products array to json
		var edm_products_json = '';
		if(selling_products.length > 0 && product_search_type == 2) {
			var edm_products_json = JSON.stringify(selling_products);
		}
		$('#edm_sell_base_products_json').val(edm_products_json);
		//alert(edm_products_json);
		return true;
	}
	
	// Prepare non selling selected products json
	function manage_other_product_json() {
		var filtered_other_products = new Array();
		var last_ids = new Array();
		var last_ids = $('input[name="product_id[]"]').map(function() {return this.value;}).get().join(',');
		var last_ids = last_ids.split(',');
		var productIDArray = last_ids;
		if(productIDArray.length > 0) {
			for(var i=0;i<productIDArray.length;i++){
				var ProductArray = {};
				ProductArray['product_id'] = productIDArray[i];
				ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
				ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
				ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
				ProductArray['product_image'] = $('#product_image_'+productIDArray[i]).val();
				filtered_other_products.push(ProductArray);
			}
		}
		// convert products array to json
		var edm_products_json = JSON.stringify(filtered_other_products);
		console.log(edm_products_json+'||Json');
		$('#edm_products_json').val(edm_products_json);
		return true;
	}

	var removeValue = function(list, value, separator) {
	separator = separator || ",";
	var values = list.split(separator);
	for(var i = 0 ; i < values.length ; i++) {
	if(values[i] == value) {
	values.splice(i, 1);
	return values.join(separator);
	}
	}
	return list;
	}

	
	/* 
	| -----------------------------------------------------
	| Manage upload manual email adresses csv
	| -----------------------------------------------------
	*/ 
	$(document).delegate( ".uploadcsv", "click", function() {
		var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv)$/;  
       //Checks whether the file is a valid csv file  
       if (regex.test($("#csvfile").val().toLowerCase())) { 
           //Checks whether the browser supports HTML5  
           if (typeof (FileReader) != "undefined") {  
               var reader = new FileReader();  
               reader.onload = function (e) {
					//var html = '';                   
					var manual_emails = $('#manual_emails').val();
					var email_html = $("#csvemails").html();
					if(manual_emails == '') { // add comma for first element
						manual_emails = ',';
					}        
					//Splitting of Rows in the csv file  
					var csvrows = e.target.result.split("\n");
					for (var i = 0; i < csvrows.length; i++) {
						if (csvrows[i] != "") {                 
							var csvcols = csvrows[i].split(",");
							var email_id = csvcols[0]+',';
							if(manual_emails.indexOf(email_id) == -1) { // accept only non duplicate id
								var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
								if (filter.test(csvcols[0])) {
									email_html += '<span class="edm-chosen-choices fl ml5">&nbsp;<span>'+csvcols[0]+'</span>&nbsp;&nbsp;<span class="email-id-close crp" email_id="'+csvcols[0]+'" ><strong> X </strong></span>&nbsp;</span>';
									manual_emails += email_id;
								}
							}
						}  
					}             
					$("#csvemails").html(email_html);
					if(manual_emails.length > 0){
						$('#manual_emails').val(manual_emails);
					}
				}
				$('#csvemails').removeClass('error');
				reader.readAsText($("#csvfile")[0].files[0]);  
			}  
			else {  
                bootbox.alert("Sorry! Your browser does not support HTML5!");  
			}  
		} else {  
            bootbox.alert("Please upload a valid CSV file!");  
		}
		return;
		e.preventDefault(); 
   });
   
   /* 
	| -----------------------------------------------------
	| Remove manual email row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".email-id-close","click",function(e){
		$(this).parent('span').remove();
		var email_id = $(this).attr('email_id');
		var manual_emails = $('#manual_emails').val();
		if(email_id != '') {
			manual_emails = manual_emails.replace(email_id+',','');
			$('#manual_emails').val(manual_emails);
		}
    });


/************* code added by pritesh gami *****************/
/*
	function get_product_id(elem){
    	var productId = $(elem).data("id");
    	$('input[name="current_product_id"]').val(productId);
    	var productName = $(elem).attr('data-name');
    	$('[name="select_product_image"]').removeAttr('checked');
    	//alert(productName);
    	if (productName != ''){
    		$('input[name="select_product_image"][value="'+productName+'"]').prop('checked', true);
    	}
    	var check_active_tab = $(".check_active_tab").hasClass("active");
		if (check_active_tab == false )
		{
			jQuery('.active_tab').addClass('active');
		}
	}*/

	function hide_modal_footer(){
		jQuery(".upload-image-modal-footer").hide();
	}
	function show_modal_footer(){
		jQuery(".upload-image-modal-footer").show();
	}

	$(document).delegate("#submit_product_image","click",function(e){
		var product_image_val = $('input[name="product_image_val"]').val();
		//if(product_image_val == ''){
		var productId = $('input[name="current_product_id"]').val();
		
		var product_image_name = $('input[name="select_product_image"]:checked').val();
	
		$("#product_image_"+productId).val(product_image_name);
		$("#select_product_button_"+productId).attr('data-name' , product_image_name );
		$(".product_image_preview_"+productId).attr("src",BASEURL+'uploads/image_library/'+product_image_name);
		//}
		$('.modal-backdrop .fade').css('opacity' , '1');
    });


    $("#product_image_picker").click(function() {
		$("input[name='product_image']").click();
	});

	function read_url_product_image(input) {
		
		var pickerId= '#'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		//$('#banner_image_val').val(input.value);
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 1.0955135) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 1MB!");
			}
		}
	}

$("#save_product_image").submit(function() {
	var product_image_val = $('#product_image_val').val();
        if(product_image_val == '') {
            $('#product_image_cell').addClass('error');
            bootbox.alert("Please select product image.");
            return false;
        }
    $('#loader1').fadeIn();
	var formData = new FormData($('#save_product_image')[0]);
	var productId = $('input[name="current_product_id"]').val();
	$.ajax({
			url: BASEURL+"admin/settings/edm/save_product_image",
			type: 'POST',
			data: formData,
			success: function (res) {
		var stringify = JSON.parse(res);
		var product_image_name = stringify.product_image_name;
		$("#product_image_"+productId).val(product_image_name);
		$("#select_product_button_"+productId).attr('data-name' , product_image_name );
		$(".product_image_preview_"+productId).attr("src",BASEURL+'uploads/image_library/'+product_image_name);
		$("#productImageModel").modal('hide');
		$('.modal-backdrop').click();
		$('#loader1').fadeOut();
			},
			contentType: false,
			processData: false
		}, "json");
		return false;
	 });





$(document).delegate( ".upload_product_image img", "click", function() {
	var productId = $(this).data("id");
	$('input[name="current_product_id"]').val(productId);
	var productName = $(this).attr('data-name');
	$('#loader1').show();
	$.ajax({
			url: BASEURL+"admin/settings/edm/get_all_images",
			type: 'POST',
			success: function (res) {
				var image_table_html = JSON.parse(res);
				$("#productImageModel #chooseImage").html(image_table_html.get_all_images);
				$('#loader1').hide();
				$('[name="select_product_image"]').removeAttr('checked');
				if (productName != ''){
				$('input[name="select_product_image"][value="'+productName+'"]').prop('checked', true);
				}
			},
			contentType: false,
			processData: false
		}, "json");
		return false;
});


</script>

