<?php
$count_active_edm   = ($edm_count->count_active_edm)?$edm_count->count_active_edm:0;
$count_inactive_edm = ($edm_count->count_inactive_edm)?$edm_count->count_inactive_edm:0;
$count_total_edm  = ($edm_count->count_total_edm)?$edm_count->count_total_edm:0;

$month_name = $inprocess = $unread = $read ="";
if(!empty($month_array) && !empty($edm_statics)){
	$istrue = FALSE;
	$month_name = $inprocess = $unread = $read ="[";
	foreach($month_array as $month){
	   $month_name.=  (($istrue)?",":"")."'".$month['mName']."'";
	   $inprocess .=  (($istrue)?",":"")."'".$edm_statics['inprocess_'.$month['mName']]."'";
	   $unread    .=  (($istrue)?",":"")."'".$edm_statics['unread_'.$month['mName']]."'";
	   $read      .=  (($istrue)?",":"")."'".$edm_statics['read_'.$month['mName']]."'";
	   $istrue     = TRUE;
	}
	$month_name .="]"; 
	$inprocess 	.="]"; 
	$unread 	.="]"; 
	$read 		.="]";
}
?>
<!--
<link href="<?php //echo Template::theme_url('edm-dashboard/css/style.css'); ?>"     rel="stylesheet" type="text/css" media="all"/>
 <link href="<?php //echo Template::theme_url('edm-dashboard/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" media="all"/>

<link href="css/font-awesome.css" rel="stylesheet">
<style type="text/css">
	table.table.table-hover.table-changes td {padding: 15px 0px;}
	table.table.table-hover.table-changes{margin-bottom: 2px;}
</style>
<style>
	#canvas-holder {
	width: 100%;
	margin-top: 50px;
	text-align: center;
	}
	#chartjs-tooltip {
	opacity: 1;
	position: absolute;
	background: rgba(0, 0, 0, .7);
	color: white;
	border-radius: 3px;
	-webkit-transition: all .1s ease;
	transition: all .1s ease;
	pointer-events: none;
	-webkit-transform: translate(-50%, 0);
	transform: translate(-50%, 0);
	}
	.chartjs-tooltip-key {
	display: inline-block;
	width: 10px;
	height: 10px;
	margin-right: 10px;
	}
	.full-widthbar{margin: 15px 0;}
	.desktop .bootbox {margin: 0;
	padding: 0;
	width: 100%;
	background: transparent;
	top: 0 !important;}
	.desktop .bootbox  .modal-sm{ width: 26%;}
	.edm-chart{margin-top:10px;margin-bottom:10px;}
	.year-filter-label{float:left;width:124px;}
	.prod-filter-label{float:left;width:124px;}
	#lineChartContent canvas {width:410px!important;}
</style> -->

<!--//skycons-icons-->
<!-- <div class="col-md-12 addbtnss">
   <a  href="<?php// echo site_url(SITE_AREA .'/settings/edm/create'); ?>"><button class="btn btn-primary bottm-marg pull-right"><?php //echo lang('edm_add_btn_title'); ?></button></a>
</div> -->
<div class="inner-block">
	
	
	<!-- EDM listing box start here -->

	<div class="row">
		<div class="col-12 chit-chat-layer1">
			<div class="card">
				<div class="card-body">
						<h4 class="card-title float-left">EDM Listing</h4>
						<a  href="<?php echo site_url(SITE_AREA .'/settings/edm/create'); ?>"><button class="btn waves-effect waves-light btn-info float-right"><?php echo lang('edm_add_btn_title'); ?></button></a>
					<?php echo $this->load->view('edm_listing'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<!-- EDM listing box end here -->
	
	<!-- EDM Product view charts start here -->
	<div class="row">
			<div class="col-lg-6 col-md-12">
				<div class="card">
				 	<div class="card-body">
						<h4 class="card-title"><?php echo lang('product_yearly_views');?></h4>
						<div class="chart-sect">
							<div class="camp-selct">
								<div class="filte" >
									<label for="sell" class="prod-filter-label"><?php echo lang('filter_by_year')?></label>
									<div class="col-md-4">
										<select class="form-control" id="prodYear" onchange="yearlyProdLineChart()">
											<?php
											// set past 5 years range
											$year1 = date('Y', strtotime(date('Y-m-d').'-4 year'));
											$year2 = date('Y');
											$years = array_reverse(range($year1, $year2),true);
											if(!empty($years)){
												foreach ($years as $year){ ?> 
													<option value = <?php echo $year; ?> ><?php echo $year; ?></option>
													<?php 
												}
											}?>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="">
								<div class="chart-sect" id="lineChartContent">
									<canvas id="product-line-canvas"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- EDM top product view chart box start here-->
			<div class="col-lg-6 col-md-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title"><?php echo lang('top_product_views');?></h4>
						<div class="chart-sect">
							<div class="camp-selct">
								<div class="filte">
									<label for="sell" class="prod-filter-label"><?php echo lang('filter_by_edm')?></label>
									<div class=" col-md-6">
										<select class="form-control" id="edmcount" onchange="edmcount()">
											<?php 
											if(!empty($viewed_edm_data)){
												foreach ($viewed_edm_data as $edm){ ?> 
													<option value = <?php echo $edm->id; ?> ><?php echo $edm->edm_title; ?></option>
													<?php 
												}
											}?>
										</select>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="">
								<div class="chart-sect" id="pieChartContent">
									<canvas id="prod_edm_canvas" class="prod_edm_canvas"/>
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>  
			<!--  EDM top product view chart box end here -->
	</div>
	<!-- EDM Product view charts end here -->
	
	<div class="clearfix"> </div>
	<!--  EDM view pie chart box start here-->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title"><?php echo lang('edm_email_read_views')?></h4>
					<div class="chart-sect">
						<div class="camp-selct">
							<div class="col-md-4 ml-15">
								<select class="form-control" id="select_edm">
									<option value='1'><?php echo lang('active_edm');?></option>
									<option value='2'><?php echo lang('expired_edm');?></option>
									<option value='3'><?php echo lang('all_edm');?></option>
								</select>
							</div>
							<div class="clearfix"></div>
						</div>
						<div>
							<div class="">
								<div class="active-edm">
									<?php
									// display active edm charts
									if(!empty($edm_active_inactive_records['active_edm'])) {
										foreach($edm_active_inactive_records['active_edm'] as $key=>$val) {  
											$total_sent_count = $val['edm_read_count']+$val['edm_unread_count'];?>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 edm-chart">
												<div class="">
													<div><b><?php echo $val['edm_title']?></b></div>
													<div><?php echo lang('edm_total_count').$total_sent_count?></div>
													<div class="chart-sect">
														<canvas id="active-edm-chart-<?php echo $key;?>" />
													</div>
												</div>	
											</div>
										<?php 
										}
									}?>
								</div>
								<div class="clearfix"></div>
								<div class="inactive-edm">
									<?php
									// display inactive edm charts
									if(!empty($edm_active_inactive_records['inactive_edm'])) {?>
										<?php
										foreach($edm_active_inactive_records['inactive_edm'] as $key=>$val) { 
											$total_sent_count = $val['edm_read_count']+$val['edm_unread_count']; ?>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 edm-chart">
												<div class="">
													<div><b><?php echo $val['edm_title']?></b></div>
													<div><?php echo lang('edm_total_count').$total_sent_count?></div>
													<div class="chart-sect">
														<canvas id="inactive-edm-chart-<?php echo $key;?>" />
													</div>
												</div>	
											</div>
										<?php 
										}
									}?>
								</div>	
								<div class="clearfix"></div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  EDM view pie chart box end here-->
	
	<div class="clearfix"></div>
	
	<!-- EDM email viewed line chart multiple axes box start here -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-8 col-xs-8">
			<div class="card">
				<div class="card-body rean-unread-loader">
				<h4 class="card-title"><?php echo lang('edm_yearly_read_log')?></h4>
					<div class="chart-sect">
						<div class="camp-selct">
							<label for="sell" class="year-filter-label"><?php echo lang('filter_by_year')?></label>
							<div class="col-md-3">
								<select class="form-control" id="emailYear" onchange="yearlyEmailBarChart()">
									<?php
									// set past 5 years range
									$year1 = date('Y', strtotime(date('Y-m-d').'-4 year'));
									$year2 = date('Y');
									$years = array_reverse(range($year1, $year2),true);
									if(!empty($years)){
										foreach ($years as $year){ ?> 
											<option value = <?php echo $year; ?> ><?php echo $year; ?></option>
											<?php 
										}
									}?>
								</select>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="">
							<div class="chart-sect" id="barChartContent">
								 <canvas id="bar-canvas"></canvas>
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div> 
	</div>
	<!-- EDM email viewed line chart multiple axes box end here -->
	
<script src="<?php echo Template::theme_url('edm-dashboard/js/jquery-2.1.1.min.js'); ?>"></script> 
<script src="<?php echo Template::theme_url('js/chartjs.js'); ?>"></script>
<script src="<?php echo Template::theme_url('js/Chart.bundle.min.js'); ?>"></script>
<script src="<?php echo Template::theme_url('edm-dashboard/js/utils.js'); ?>"></script> 
<script type="text/javascript">
   
   function show_loader(action,className){
		var div = "<div class='loader loaderdiv'><i class='fa fa-refresh fa-spin' style='font-size: 28px;color: #111;position: absolute;left: 50%;bottom: 50%;transform: translate(-50%,-50%);'></i></div>";
		if(action){
			$('.'+className).css({"position": "relative","padding": "0px"});
			$('.'+className).prepend(div);
		}else{
			$('.'+className).css({"position":"","padding": ""});
			$('.loaderdiv').remove();
		}
	}	
						
	edmcount();
	function edmcount(){
		var edmval =  $('#edmcount').val();
		if(edmval !==0 || edmval !== ''){
			$.ajax({
				type: "GET",
				dataType: "json",
				url: '<?php echo site_url("admin/settings/edm/edm_product_log"); ?>',
				data: {edmval:edmval},
				beforeSend: function() {
					show_loader(1,'min-loader');
				},
				success: function(data){
				if(data.result){
					var view_count = (data.view_count) ? data.view_count : 0;
					var products   = (data.products) ? data.products : 0;
					// render chart
					load_product_chart(products,view_count,edmval);
				  }
				},
				complete:function(){
				show_loader(0,'min-loader');
				}
			});
		}
	}
	
	// Manage yearly read-unread mail bar chart
	function yearlyEmailBarChart() {
		var emailYear =  $('#emailYear').val();
		if(emailYear !==0 || emailYear !== ''){
			$.ajax({
				type: "GET",
				dataType: "json",
				url: '<?php echo site_url("admin/settings/edm/yearlyEmailBarChart"); ?>',
				data: {emailYear:emailYear},
				beforeSend: function() {
					show_loader(1,'rean-unread-loader');
				},
				success: function(data){
					if(data.result){
						var read_emails = (data.read_emails) ? data.read_emails : 0;
						var unread_emails   = (data.unread_emails) ? data.unread_emails : 0;
						// render chart
						drawYearBarchart(read_emails,unread_emails);
					}
				},
				complete:function(){
				show_loader(0,'rean-unread-loader');
				}
			});
		}
	} 
	
	// Draw email year bar chart
	function drawYearBarchart(read_emails,unread_emails) {
		// Email bar chart
		var barChartData = {
			labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			datasets: [{
				label: 'Read',
				backgroundColor: window.chartColors.red,
				data: read_emails
			}, {
				label: 'Un-Read',
				backgroundColor: window.chartColors.blue,
				data: unread_emails
			}]
		};
		
		emailconfig ={
			type: 'bar',
			data: barChartData,
			options: {
				title:{
					display:false,
					text:""
				},
				tooltips: {					
					intersect: false
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true
					}]
				}
			}
		};
		// remove existing chart canvas
		$('#bar-canvas').remove();
		$('#barChartContent').append('<canvas id="bar-canvas"></canvas>');
		var bcctx  = document.getElementById("bar-canvas").getContext("2d");
		window.mybc   = new Chart(bcctx,emailconfig);
	}
	
	// Manage yearly product stastistics on line chart
	function yearlyProdLineChart() {
		var prodYear =  $('#prodYear').val();
		if(prodYear !==0 || prodYear !== ''){
			$.ajax({
				type: "GET",
				dataType: "json",
				url: '<?php echo site_url("admin/settings/edm/yearlyProdLineChart"); ?>',
				data: {prodYear:prodYear},
				beforeSend: function() {
					show_loader(1,'prod-line-chart-loader');
				},
				success: function(data){
					if(data.result){
						var edm_product_yearly_viewed = (data.edm_product_yearly_viewed) ? data.edm_product_yearly_viewed : 0;	
						var edm_datasets = [];
						var edm_count = 0;
						for (var i = 0; i < edm_product_yearly_viewed.length; i++) {
							var edm_data = {};
							// prepare product counts
							var product_view_count = edm_product_yearly_viewed[i]['product_counts'];
							var product_count_array = new Array();
							for (var j = 0; j < product_view_count.length; j++) {
								product_count_array.push(product_view_count[j]);
							}
							edm_data['label'] = edm_product_yearly_viewed[i]['edm_title'];
							edm_data['borderColor'] = edm_color(edm_count);
							edm_data['backgroundColor'] = edm_color(edm_count);
							edm_data['fill'] = false;
							edm_data['data'] = product_count_array;
							edm_datasets.push(edm_data);
							edm_count++;
						}
						drawYearProdLinechart(1,edm_datasets);
						return false;
						
					}
				},
				complete:function(){
				show_loader(0,'prod-line-chart-loader');
				}
			});
		}
	} 
	
	// Draw email yearly product line chart
	function drawYearProdLinechart(is_ajax=0,edm_datasets=[]) {
		if(is_ajax != 1) {
			var edm_datasets = [];
			var edm_count = 0;
			<?php
			// prepare yearly edm product view data
			if(!empty($edm_product_yearly_viewed)) {
				foreach($edm_product_yearly_viewed as $key=>$val) { ?>
					var edm_data = {};
					// prepare product counts
					var product_view_count = JSON.parse('<?php echo json_encode($val['product_counts']); ?>');
					var product_count_array = new Array();
					for (var i = 0; i < product_view_count.length; i++) {
						product_count_array.push(product_view_count[i]);
					}
					var edm_title = '<?php echo $val['edm_title'];?>';
					edm_data['label'] = (edm_title.length > 15) ? edm_title.substring(0,15)+'...' : edm_title;
					edm_data['borderColor'] = edm_color(edm_count);
					edm_data['backgroundColor'] = edm_color(edm_count);
					edm_data['fill'] = false;
					//edm_data['data'] = product_count_array.reverse();
					edm_data['data'] = product_count_array;
					edm_datasets.push(edm_data);
					edm_count++;
				<?php 
				}
			}?>
		}

		var lineChartData = {
			type: 'line',
			data: {
				labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
				datasets: edm_datasets
			},
			options: {
				responsive: true,                 
			}
		};
		// remove existing chart canvas
		$('#product-line-canvas').remove();
		$('#lineChartContent').append('<canvas id="product-line-canvas" width="392" height="196"></canvas>');
		// load yearly edm product view line chart
		var yptx = document.getElementById("product-line-canvas").getContext("2d");
        window.myLine = new Chart(yptx, lineChartData);
	}
	
	// set product line color
	function edm_color(key) {
		var color = window.chartColors.yellow;
		// set edm line chart color
		switch(key) {
			case 0 : 
				color = window.chartColors.green;
				break;
			case 1 : 
				color = window.chartColors.blue;
				break;
			case 2 : 
				color = window.chartColors.red;
				break;		
		}
		return color;
	}
	
	var active_edm_chart_config = [];
	<?php
	// prepare active edm read /unread email view charts
	if(!empty($edm_active_inactive_records['active_edm'])) {
		foreach($edm_active_inactive_records['active_edm'] as $key=>$val) { ?>
			var activeconfig = {
				type: 'pie',
				data: {
				datasets: [{
					data: ['<?php echo !empty($val['edm_read_count'])?$val['edm_read_count']:0;  ?>','<?php echo !empty($val['edm_unread_count'])?$val['edm_unread_count']:0;  ?>'],              
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.orange,	
					],
					label: 'Dataset 1'
					}],
					labels: [
						"Read",
						"Unread"
					]
				},
				options: {
					title:{
						display:false,
						text:"<?php echo $val['edm_title']; ?>"
					},
					responsive: true,
					animation: {
						animateScale: true,
						animateRotate: true
					}
				}
			};
			active_edm_chart_config.push(activeconfig);
		<?php
		}
	} ?>
	
	var inactive_edm_chart_config = [];
	<?php
	// prepare inactive edm read /unread email view charts
	if(!empty($edm_active_inactive_records['inactive_edm'])) {
		foreach($edm_active_inactive_records['inactive_edm'] as $key=>$val) { ?>
			var inactiveconfig = {
				type: 'pie',
				data: {
				datasets: [{
					data: ['<?php echo !empty($val['edm_read_count'])?$val['edm_read_count']:0;  ?>','<?php echo !empty($val['edm_unread_count'])?$val['edm_unread_count']:0;  ?>'],              
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.orange,	
					],
					label: 'Dataset 1'
					}],
					labels: [
						"Read",
						"Unread"
					]
				},
				options: {
					title:{
						display:false,
						text:"<?php echo $val['edm_title']; ?>"
					},
					responsive: true,
					animation: {
						animateScale: true,
						animateRotate: true
					}
				}
			};
			inactive_edm_chart_config.push(inactiveconfig);
		<?php
		}
	} ?>
	
	window.onload = function() {
		// load yearly email read-unread bar chart
		drawYearBarchart(<?php echo $read_emails;?>,<?php echo $unread_emails;?>);
		// load yearly product line chart
		drawYearProdLinechart();
		// load active edm read/unread chartes
		if(active_edm_chart_config.length > 0) {
			for (var i = 0; i < active_edm_chart_config.length; i++) {
				var actx   = document.getElementById("active-edm-chart-"+i).getContext("2d");
				window.myPie  = new Chart(actx,active_edm_chart_config[i]);    
			}
		}
		// load inactive edm read/unread chartes
		if(inactive_edm_chart_config.length > 0) {
			for (var i = 0; i < inactive_edm_chart_config.length; i++) {
				var iactx   = document.getElementById("inactive-edm-chart-"+i).getContext("2d");
				window.myPie  = new Chart(iactx,inactive_edm_chart_config[i]);    
			}
		}
		
        // hide expired edm pie chart
		setTimeout(function(){ $('.inactive-edm').hide(); }, 1000);
	};
	
	// Load top product view chart
	function load_product_chart(products,view_count,edm_id) {
		
		var top_products = JSON.parse(products);
		var top_product_array = new Array();
		if(top_products.length > 0) {
			for (var i = 0; i < top_products.length; i++) {
				top_product_array.push(top_products[i]);
			}
		} else {
			top_product_array.push(0);
		}
		
		var product_view_count = JSON.parse(view_count);
		var product_count_array = new Array();
		if(product_view_count.length > 0) {
			for (var i = 0; i < product_view_count.length; i++) {
				product_count_array.push(product_view_count[i]);
			}
		} else {
			product_count_array.push(0);
		}
		
		$('.chartjs-hidden-iframe').remove();
		//$('.prod_edm_canvas').attr('id','prodict-edm-chart-'+edm_id);
		var productconfig = {
			type: 'doughnut',
			data: {
			datasets: [{
				data: product_count_array,              
				backgroundColor: [
					window.chartColors.red,
					window.chartColors.orange,
					window.chartColors.yellow,
					window.chartColors.green,
					window.chartColors.blue,
					
				],
				label: 'Dataset 1'
				}],
				labels: top_product_array
			},
			options: {
				title:{
					display:false,
					text:""
				},
				responsive: true,
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};
		// remove existing chart canvas
		$('#prod_edm_canvas').remove();
		$('#pieChartContent').append('<canvas id="prod_edm_canvas"><canvas>');
		// load top product bar
		var tptx   = document.getElementById("prod_edm_canvas").getContext("2d");
		window.myDoughnut  = new Chart(tptx,productconfig);
	}
	
	// Manage EDM pie charts display based on types
	$( "#select_edm" ).change(function() {
		var edm =  $(this).val();
		if(edm == 1) { // display active edm charts
			$('.active-edm').show();
			$('.inactive-edm').hide();
		} else if(edm == 2) { // display inactive edm charts
			$('.active-edm').hide();
			$('.inactive-edm').show();
		} else { // display all edm charts
			$('.active-edm').show();
			$('.inactive-edm').show();
		}
	});
	
</script>
<!--climate end here-->
