
		<div class="table-responsive m-t-40">
					<table id="edm_table" class="table table-bordered table-striped">
						<thead>
                        <tr role="row">
							<th><?php echo 'S.No.'; ?></th>
							<th><?php echo 'Title'; ?></th>
							<th><?php echo 'Availability'; ?></th>
							<th><?php echo 'Status'; ?></th>
							<th><?php echo 'Action'; ?></th>
						</tr>
	                </thead>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
					<?php
					$i=1;
					if(isset($edm_data) && is_array($edm_data) && count($edm_data)>0):
						foreach($edm_data as $edm) :
						// set encoded beacon id
						$edm_id = encode($edm->id); 
						$en_status  = encode($edm->is_deleted);
						$btn_status = (empty($edm->is_deleted))?'active-btn':'deactive-btn';
						$edit_url   = site_url(SITE_AREA .'/settings/edm/create/'.$edm_id); 
						?>
						
						<tr>
						<td><?php echo $i;?></td>


						<td class="mytooltip tooltip-effect-1 desc-tooltip">
							<span class="text-ttip">
								<a href="<?php echo $edit_url; ?>"><?php echo (isset($edm->edm_title) && !empty($edm->edm_title)) ? ucfirst($edm->edm_title) : '' ; ?></a>
							</span> 
						</td>
						<td>
						<?php
							$avail_diff =  get_date_diffrence($edm->start_date,$edm->end_date,6,1);
							  if($avail_diff == 'Closed'){
									$txt      = 'closed';
									$lablType = 'error';
							  }else{
									$txt = $avail_diff;
									$lablType = 'success';
							  }
						echo "<span class='label label-$lablType'>$txt</span>";
						?>
						</td>
						<td>
							<?php if(empty($edm->is_deleted)){ ?>
							<a class="common-btn active-btn" onClick="changeStatus('<?php echo $edm_id;?>','<?php echo $en_status;?>')" href="javascript:;"><i class="fa fa-toggle-on"></i></a>
								<?php
							} else {
								?>
							<a class="common-btn deactive-btn" onClick="changeStatus('<?php echo $edm_id;?>','<?php echo $en_status;?>')" href="javascript:;"><i class="fa fa-toggle-off"></i></a>	
							<?php } ?>
							
						</td>
						<td>
							<a href='<?php echo $edit_url; ?>' class="mr5" data-toggle='tooltip' title='<?php echo lang('edm_remove_text');?>'> <i class='fa fa-pencil' aria-hidden='true'></i>  </a>
							<?php if(!empty($edm->product_add_type)) { ?>
								<a href='<?php echo site_url().'promotion/index.php?eid='.$edm_id.'&uid='.$admin_id;?>' target="_blank" data-toggle='tooltip' title='<?php echo lang('edm_view_prod_page_text');?>'><i class='fa fa-eye' aria-hidden='true'></i> </a>
							<?php }?>
						</td>
						</tr>
					<?php 
					$i++;
					endforeach ; endif; ?>
                </tbody>
            </table>                 
		</div>
		
<script>
$(document).ready(function() {
		$('#edm_table').DataTable();
	});
function changeStatus(edmid,status){
			bootbox.confirm("Are you sure want to change the Edm status?", function(result) {
			    if(result==true){
				 $('#loader1').fadeIn();
				 location.href="<?php echo site_url("admin/settings/edm/edm_status"); ?>/"+edmid+"/"+status;
				}
			});
        }
</script>		
		
