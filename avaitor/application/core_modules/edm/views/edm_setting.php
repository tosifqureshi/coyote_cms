<style>
 .chosen-choices
  {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    cursor: text;
    height: auto !important;
    height: 1%;
    overflow: hidden;
    padding: 0;
    position: relative;
    width: auto;
    }
.dropdown {
  position: relative;
      margin: 0;
    padding: 0;
    margin-bottom: 15px;
 }


.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: -1px 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #fff;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
     background-color: #95979829;
    display: block;
    min-height: 30px;
    line-height: 30PX;
    border: 0;
    width: 272px;
    padding: 7px 5px;
    color: black;
    margin-top:30px;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
    padding: 0 3px 2px 0;
    color: #3e3d3d;
    display: list-item;
    list-style-type: decimal;
    list-style-position: inside;
    line-height: 20px;
}

.dropdown dd ul {
     display: none;
     background-color: #e1e2e2;
    color: #3a3b36;
    left: 0px;
    padding: 2px 15px 15px 5px;
    top: 2px;
    width: 272px;
    list-style: none;
    height:90px;
    overflow: auto;
    font-size: 12px;
    border: 1px solid #ccc7c7;
    margin-bottom: 10px;
        overflow-y: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #fff;
}
.p-relative{ position:relative;}
.p-relative label.error {
    position: absolute;
    top: -12px;
    min-width: 300px;
}
.rewardcard-value {
    position: relative;
    padding-bottom: 20px;
}
.rewardcard-value label.error{ 
	position: absolute;
    bottom: 0px;
    min-width: 300px;
}

</style>  
<link href="<?php echo Template::theme_url('edm-dashboard/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" media="all"/>
<?php
// set edm param values
$edm_id 	 =  !empty($edm_data['id'])?$edm_data['id']:'';
$edm_title   =  !empty($edm_data['edm_title'])?$edm_data['edm_title']:'';
$product_top  			=  !empty($edm_data['product_top'])?json_decode($edm_data['product_top']):array();
$product_bottom  		=  !empty($edm_data['product_bottom'])?json_decode($edm_data['product_bottom']):array();
$product_other  		=  !empty($edm_data['product_other'])?$edm_data['product_other']:'';
$product_top_val  		=  !empty($edm_data['product_top'])?$edm_data['product_top']:'';
$product_bottom_val  	=  !empty($edm_data['product_bottom'])?$edm_data['product_bottom']:'';
$no_of_top_products  	=  !empty($edm_data['no_of_top_products'])?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
$no_of_bottom_products  =  !empty($edm_data['no_of_bottom_products'])?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
$no_products  			=  !empty($edm_data['no_products'])?$edm_data['no_products']:'';

$SelectTop = "";
$TopHtml   = "";
$TopName   = "";
$isTop_dn  = "dn";
if(!empty($top)){	
	foreach($top as $topData){
		$prId = $topData['JNAD_PRODUCT'];
		$prName = $topData['PROD_DESC'];
		$isTopchecked = "";
		if(in_array($topData["JNAD_PRODUCT"],$product_top)){
			$selectoPtion  = $prName.',';
			$TopName      .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isTopchecked  = "checked";	 
			$isTop_dn 	    = "";			 
		}
		$TopHtml .= "<li><input type='checkbox' class='topSellingProducts' name='product_top[]' ".$isTopchecked." value='$prId' data-name='$prName' />$prName</li>";
	 }
	 $SelectTop .= "<ul class='multiSel'>".$TopName."</ul>";
}

$SelectBottom = '';
$BottomHtml   = "";
$BottomName   = "";
$isBottom_dn  = "dn";
if(!empty($bottom)){
	foreach($bottom as $bottomData){
		$prId   = $bottomData['JNAD_PRODUCT'];
		$prName = $bottomData['PROD_DESC'];
		$isBottomchecked = "";
		if(in_array($bottomData["JNAD_PRODUCT"],$product_bottom)){
			$selectoPtion = $prName.',';
			$BottomName  .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isBottomchecked = "checked";
			$isBottom_dn = "";
		}
		$BottomHtml .= "<li><input type='checkbox' class='bottomSellingProducts' name='product_bottom[]'  ".$isBottomchecked."   value='$prId' data-name='$prName' />$prName</li>";
	}
	$SelectBottom .= "<ul class='multiSel'>".$BottomName."</ul>";
}
?>
	<div class="heading">
		<h1>Email Direct Marketing</h1>
	</div>
	<div class="row brd">
        <div class="col-md-6" style="padding:0px 15px 0px 0px; margin:0px;">
          <div class="form-group btm0">
            <ul class="nav nav-pills setup-panel" id="myNav">
              <li id="navStep1" class="li-nav active" step="#step-1"><a>
                <h4 class="list-group-item-heading">EDM</h4>
                </a></li>
              <li id="navStep2" class="li-nav disabled" step="#step-2"><a>
                <h4 class="list-group-item-heading">Mapping</h4>
                </a></li>
              <li id="navStep3" class="li-nav disabled" step="#step-3"><a>
                <h4 class="list-group-item-heading">Schedule</h4>
                </a></li>
              <li id="navStep4" class="li-nav disabled" step="#step-4"><a>
                <h4 class="list-group-item-heading">Audience</h4>
                </a></li>
              <li id="navStep5" class="li-nav disabled" step="#step-5"><a>
                <h4 class="list-group-item-heading">Settings</h4>
                </a></li>
            </ul>
          </div>
		<form class="container-fluid formnew">
			<h4>Create EDM Product</h4>
			<div class="row setup-content" id="step-1">
				<div class="col-xs-12"> 
					<!-- <form> -->
					<div class="row text-left">
						<div class="col-md-12">
							<div class="form-group text-left">
								<label class="control-label" for="name">EDM Title</label>
								<?php
								echo form_hidden('edm_id',$edm_id);
								$data = array('name'=>'edm_title','id'=>'edm_title', 'value'=>$edm_title,'class'=>'form-control input-md required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
								echo form_input($data);?>
							</div>
						</div>
						<!-- Text input-->
                  
						<!-- Text input-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="date">Start Date*</label>
								<div class="form-group input-append date" id="start_date_div1">
									<?php
									$start_date =  !empty($edm_data['start_date'])?$edm_data['start_date']:'';
									$data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$start_date,'id'=>'start_date','class'=>'required form-control input-md calender-icon date-picker','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
									 echo form_input($data);?>
									<div class="cland-icon add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="date">End Date*</label>
								<div class="form-group input-append date" id="end_date_div1">
									<?php
									$end_date =  !empty($edm_data['end_date'])?$edm_data['end_date']:'';
									$data = array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$end_date,'id'=>'end_date','class'=>'required form-control input-md calender-icon date-picker','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_end_date'), 'readonly'=>'readonly');
									echo form_input($data);?>
									<div class="cland-icon add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></div>
								</div>
							</div>
						</div>
						<div class="spacer15"></div>
						<!-- Banner image box start here -->
						<div class="form-group text-left">
							<div class="imge-wrap">
								<label for="title" class="img-title"><?php echo lang('banner_image');?></label> 	
								<div class="img-box">
									<?php
									// set banner image action
									$banner_image_val = (isset($banner_image) && !empty($banner_image)) ? $banner_image :'';
									$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
									if(!empty($banner_image_val)) {
										$banner_image_src = base_url('uploads/scratchnwin_images/'.$banner_image);
										$title = lang('tooltip_offer_image');
								
										$banner_image_val_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$banner_image; 
										if(!file_exists($banner_image_val_doc)){
											// $promo_image_1 = base_url('uploads/No_Image_Available.png');
											$banner_image_src = base_url('uploads/No_Image_Available.png');
											$title = "No Image Available";
										}
									} else {
										$title = lang('tooltip_image');
										$banner_image_src = base_url('uploads/upload.png');
									} ?>
									<div class="cell-img" id="banner_image_cell">
										<img id="banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
									</div>
									<?php
									$data	= array('name'=>'banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
									echo form_upload($data);
									// set banner image value in hidden type
									$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
									echo form_input($data);?>
								</div>
								<div style="clear:both;"></div>
								<div class="alert red-alert alert-danger alert-dismissible ">
									<i class="icon fa fa-warning"></i>
									Upload up to 1MB images. 
								</div>
							</div>
						</div>
						<!-- Banner image box end here -->
				</div>
                <div class="spacer15"></div>
                <div class="text-left">
					<input type="button" onclick="step1Next()" class="btn btn-md btn-info btncommon" value="Next">
                </div>
                <div class="clearfix"></div>
                <div class="spacer15"></div>
                <!-- </form> -->
                
              </div>
            </div>
          </form>
          <form class="container-fluid">
            <div class="row setup-content" id="step-2">
              <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                  <h1 class="text-center">STEP 2</h1>
                  
                  <!--<form>-->
                  <div class="container col-xs-12">
					<!-- Start general config panel -->
					<div class="tab-pane min-loader" id="product_tab">
						<div class="">
							<!-- number of top selling product input start here -->
							<div class="form-box">
								<label><?php echo lang('no_of_top_selling_products');?>
									<i style="color:red;">*</i>
									<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
								</label>
								<?php
								$data = array('name'=>'no_of_top_products','id'=>'no_of_top_products', 'value'=> $no_of_top_products ,
								'class'=>'span8 required product_selling_fetch_limit','product_selling_type'=>1, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_top_selling_products'));
								echo form_input($data);
								$data = array('type'=>'hidden','id'=>'no_of_top_products_hidden','value'=> $no_of_top_products);
								echo form_input($data);
								?>						
							</div>
							<!-- number of top selling product input end here -->
							
							<!-- number of bottom selling product input start here -->
							<div class="form-box">
								<label>
									<?php echo lang('no_of_bottom_selling_products');?><i style="color:red;">*</i>
									<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
								</label>
								<?php
								$data = array('name'=>'no_of_bottom_products','id'=>'no_of_bottom_products', 'value'=> $no_of_bottom_products ,
								'class'=>'span8 required product_selling_fetch_limit','product_selling_type'=>2, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_bottom_selling_products'));
								echo form_input($data);
								$data = array('type'=>'hidden','id'=>'no_of_bottom_products_hidden','value'=> $no_of_bottom_products);
								echo form_input($data);
								?>
							</div>
							<!-- number of bottom selling product input end here -->
							
							<!-- maximum reward point input start here -->
							<div class="form-box mt10 indispaly">
							<label>
							<span class="fl mr5">
								<input  type="checkbox" onchange="cbchange(this)" data-show="topsell-pr" id="topSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_top)?"checked":""); ?>>
								</span>
							<span class="fl mr5">
								<?php echo lang('edm_top_selling_product');?>
								</span> 
								</label>  
								 <span class="topsell-pr <?php echo $isTop_dn; ?>">			 						 
									 <dl class="dropdown" id="top"> 
										<dt>
										<a href="#" class="selectProducts_1">
											<?php echo $SelectTop;?>
										</a>
										</dt>
										<dd>
											<div class="mutliSelect" name="top">
												<ul class="ulProductHtml_1">
													<?php echo $TopHtml; ?>
												</ul>
											</div>
										</dd>
									</dl>
								 </span>
							</div>
							<!-- maximum reward points input end here -->
							
							<!-- reward point expiry input start here -->
							<div class="form-box mt10 indispaly">
							<label>
							 <span class="fl mr5">
								<input  type="checkbox" onchange="cbchange(this)" data-show="bottomsell-pr" id="bottomSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_bottom)?"checked":""); ?>></span>
							<span class="fl mr5">	
								<?php echo lang('edm_bottom_selling_product');?>
							</span>	
							</label>
								<span class="bottomsell-pr <?php echo $isBottom_dn; ?>">
									<dl class="dropdown" id="bottom"> 
										<dt>
										<a href="#" class="selectProducts_2">
											<?php echo $SelectBottom; ?>
										</a>
										</dt>
										<dd>
											<div class="mutliSelect"  name="bottom">
												<ul class="ulProductHtml_2">
													<?php echo $BottomHtml; ?>
												</ul>
											</div>
										</dd>
									</dl>
								</span>
							</div>

							<!-- reward point expiry input end here -->
							
							<!-- reward point expiry input start here -->
							<div class="form-box mt10 indispaly">
								<label>
									<span class="fl mr5">
										<input type="checkbox" name="is_product_group" class="otherPr__" value="1" id="is_product_group"  <?php echo (!empty($product_other)?"checked":""); ?>>
									</span>
									<span class="fl mt1"><?php echo lang('edm_other_product');?></span>
								</label>
								<div id="productgropuShowHide" style=" clear: both; padding-top: 10px;" class="<?php echo (!empty($product_other) ? "" : "dn"); ?>">
									<div class="mr10" id="searchProductButton">
									&nbsp;&nbsp;
									<button type="button" name="product_search" class="btn btn-primary otherPr" ><?php echo lang('search_product');?></button>
									</div>
									<div class="mt10 "  id="showproductDiv"></div>
									<input type="hidden" id="product_other" name="product_other" value="">
																			
								</div>					
							</div><br>
							<!-- reward point expiry input end here -->
						
							<!-- number of product show on email input start here -->
							<div class="form-box">
								<label for="edm-other-quantity"><?php echo lang('no_of_product_in_email');?><i style="color:red;">*</i></label>
								<?php
								$data = array('name'=>'no_products','id'=>'no_products', 'value'=> $no_products ,
								'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
								echo form_input($data);
								?>
							</div>
							<!-- number of product show on email input end here -->
						</div>
						<div class="clearboth"></div>
					</div>
					<!-- End general config panel -->
                  </div>
                  <!--</form> -->
                  
                  <input type="button" onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                  <input type="button" onclick="step2Next()" class="btn btn-md btn-info" value="Next">
                </div>
              </div>
            </div>
          </form>
          <form class="container-fluid">
            <div class="row setup-content" id="step-3">
              <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                  <h1 class="text-center">STEP 3</h1>
                  
                  <!--<form></form> -->
                  
                  <input type="button" onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                  <input type="button" onclick="step3Next()" class="btn btn-md btn-info" value="Next">
                </div>
              </div>
            </div>
          </form>
          <form class="container-fluid">
            <div class="row setup-content" id="step-4">
              <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                  <h1 class="text-center">STEP 4</h1>
                  
                  <!--<form></form> -->
                  
                  <input type="button" onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                  <input type="button" onclick="step4Next()" class="btn btn-md btn-info" value="Next">
                </div>
              </div>
            </div>
          </form>
          <form class="container-fluid">
            <div class="row setup-content" id="step-5">
              <div class="col-md-12 well text-center">
                <h1 class="text-center">STEP 5</h1>
                
                <!--<form></form> -->
                <input type="button" onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                <button class="btn btn-md btn-primary" value="Send"></button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-6" style="padding:0px; margin:0px;">
          <div class="row bgcolor ptb10">
            <form class="formnew lh38">
              <div class="col-xs-2">From</div>
              <div class="col-xs-10">
                <input id="name" name="name" placeholder="from" class="form-control input-md" type="text">
              </div>
             
              <div class="col-xs-2">Subject</div>
              <div class="col-xs-10">
                <input id="name" name="name" placeholder="Subject" class="form-control input-md" type="text">
              </div>
            </form>
          </div>
          <div class="graycolor p20">
            <div class="mt-4 bgcolor3" style="width:472px; margin:0px auto;">
              <table border="0" width="472" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
                <tr class="bgyellow p10">
                  <td style="height:52px; padding-left:10px;"><img src="<?php echo site_url('uploads/edm_images/logo.png'); ?>" /></td>
                </tr>
                <tr>
                  <td align="left"><table border="0" align="center" cellpadding="0" cellspacing="0" class="">
                      <tr>
                        <td align="center" class="section-img" style="border-style:none !important; display: block; border:0 !important;"><img id="banner-image" src="<?php echo site_url('uploads/edm_images/banner.jpg'); ?>" border="0" style="height:120px;" /></td>
                      </tr>
                      <tr>
                        <td height="10" style="font-size:10px; line-height:10px;"> </td>
                      </tr>
                      <tr>
                        <td height="40" style="font-size:18px; font-weight:800; padding-left:2%; line-height:30px; background:#e9921b; font-family:'Courier New', Courier, monospace; color:#fff; ">Hot Deals </td>
                      </tr>
                      <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;"> </td>
                      </tr>
                      <tr>
                        <td><table border="0" width="100%" cellpadding="0" cellspacing="0" class="style">
                            <tr>
                              <td class="brd" style="width:30%;"><p><img src="<?php echo site_url('uploads/edm_images/img1.jpg'); ?>"></p>
                                <h4>Coldrink Pack</h4>
                                <p>Cock, Sprit, Fanta</p>
                                <div><span class="price-old">$80.00</span><span class="price-new">$50.00</span></div>
                                <button class="btn btn-md btn-info btncommon2">More Details</button></td>
                              <td>&nbsp;</td>
                              <td class="brd" style="width:30%;"><p><img src="<?php echo site_url('uploads/edm_images/img2.jpg'); ?>"></p>
                                <h4>New @ NightOwl</h4>
                                <p>Cock, Sprit, Fanta</p>
                                <div><span class="price-old">$80.00</span><span class="price-new">$50.00</span></div>
                                <button class="btn btn-md btn-info btncommon2">More Details</button></td>
                              <td>&nbsp;</td>
                              <td align="right" style="background:#d9d9d9; text-align:center; width:32%;" class="brd"><img src="<?php echo site_url('uploads/edm_images/blank-image-small.png'); ?>"></td>
                            </tr>
                          </table></td>
                      <tr>
                        <td height="10" style="font-size:10px; line-height:10px;"> </td>
                      </tr>
                      <tr>
                        <td height="40" style="font-size:18px; font-weight:800; padding-left:2%; line-height:30px; background:#758393; font-family:'Courier New', Courier, monospace; color:#fff; ">Special Offers </td>
                      </tr>
                      <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;"> </td>
                      </tr>
                      <tr>
                        <td><table border="0" width="100%" cellpadding="0" cellspacing="0" class="style">
                            <tr>
                              <td align="left" style="width:40%;" class="brd"><p><img src="<?php echo site_url('uploads/edm_images/img3.jpg'); ?>"></p>
                              <td>&nbsp;</td>
                              <td align="right" style="width:40%; background:#d9d9d9; text-align:center;" class="brd"><img src="<?php echo site_url('uploads/edm_images/blank-image-large.png'); ?>"></td>
                            </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td height="15" style="font-size:15px; line-height:15px;"> </td>
                      </tr>
                      <tr>
                        <td align="center" class="" style="border-style:none !important; display: block; border:0 !important; color:#fff; background:#989898; font-size:12px; line-height:24px;">Copyright 2018 Nightowl</td>
                      </tr>
                    </table></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
   
<link rel="stylesheet" href="<?php echo Template::theme_url('css/edm-tab.css'); ?>" type="text/css" /> 
<script src="<?php echo Template::theme_url('js/edm-custom.js'); ?>" type="text/javascript"></script> 
<script src="<?php echo Template::theme_url('edm-dashboard/js/jquery.multi-select.js'); ?>"></script> 
<script>
$(document).ready(function() {
	if(jQuery('#start_date_div1').length > 0)  {
	   jQuery('#start_date_div1').datetimepicker({
		   pickTime: false,
		   startDate: new Date(), 
	   });
   }

   if(jQuery('#end_date_div1').length > 0)  { 
	   jQuery('#end_date_div1').datetimepicker({
		   pickTime: false,
		   startDate: new Date(), 
	   });
   }
});

$("#banner_image_picker").click(function() {
	$("input[name='banner_image']").click();
});

function read_url_banner_image(input) {
    
	var pickerId= '#'+input.id+'_picker';
	var fieldId	= '#'+input.id;
	$(fieldId+'_val').val(input.value);
	
	if (input.files && input.files[0]) {
		var file = input.files[0];
		var size = file.size/1000000; // get image size in mb;
		if(size <= 1.0955135) {
			var imageType = /image.*/;
			if (file.type.match(imageType)) {
				var reader = new FileReader();
				reader.onload = function (e) {
					//alert(pickerId);
					$(pickerId).attr('src', e.target.result);
					$('#banner-image').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				$(fieldId).val("");
				bootbox.alert("File not supported!");
			}
		} else {
			$(fieldId).val("");
			bootbox.alert("File size should be less then 1MB!");
		}
	}
}

function cbchange(res){
	var showdiv = $(res).attr('data-show');
	if(showdiv!= ''){
		if(res.checked){
			$('.'+showdiv).fadeIn('slow');	
		}else{
			$('.'+showdiv).fadeOut('slow');	
		}
	}
}
</script>
