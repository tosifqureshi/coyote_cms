<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/dataTables.buttons.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/jszip.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/buttons.html5.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/buttons.print.min.js'); ?>"></script>
	<div class="well-white">
		<div class="page-header">
			<h1><?php echo lang('edm_title') ?></h1>
			<a  href="<?php echo site_url(SITE_AREA .'/settings/edm/create'); ?>" class="btn btn-primary pull-right"><?php echo lang('edm_add_btn_title'); ?></a>
		</div>
	<div>	
	<div class="box-body">
	<div role="grid" class="dataTable" id="dyntable_wrapper">
		<table id="edm_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">    
			<thead>
				<tr role="row">
					<th><?php echo 'S.NO.'; ?></th>
					<th><?php echo 'Title'; ?></th>
					<th><?php echo 'Availability'; ?></th>
					<th><?php echo 'Status'; ?></th>
					<th><?php echo 'Action';; ?></th>
				</tr>
			</thead>
		</table>                 
	</div>
</div>

	<script>
	$(document).ready(function(){			
		$('#edm_table').dataTable({			  
		 "bPaginate": true,
         "bLengthChange": true,
         "bFilter": true,
         "bSort": true,
         "bInfo": false,
         "bAutoWidth": false,
         "processing": false,
         "serverSide": true,
         "searching": true,
         "destroy":true,
         "aoColumns":[
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			],
			"ajax": {
				"url":"<?php echo base_url('admin/settings/edm/edmlist'); ?>",				
			},
			"aoColumnDefs":[{ 'bSortable': false,'aTargets':[]}],
			"columnDefs":[{"targets": 0,"bSortable":false,"orderable":false,"visible":true}],
			lengthMenu: [
				[10, 25, 50, 100 ],
				[10, 25, 50, 100 ]
			],
			dom: 'lBfrtip',
			buttons: [{
						extend: 'excel',
						text:'edm filter excel',
						title: 'edm_report' 
					 }]
		});		
	var oTable;
	oTable = $('#edm_table').dataTable();
	$("#status").change(function(){		
	 oTable.fnFilter(); 
	});
	$("#role").change(function(){		
	 oTable.fnFilter(); 
	});
	//reset datatable filter 
	$(document).on('click','.resetfilter',function(){			
	 $('.bt_select').prop('selectedIndex',0);
	 $('#edm_table').DataTable().search('').draw();
	 oTable.fnFilterClear();
	});	
		
	});
	
	function changeStatus(edmid,status){
		bootbox.confirm("Are you sure want to change the Edm status?", function(result) {
			if(result==true){
			 $('#loader1').fadeIn();
			 location.href="<?php echo site_url("admin/settings/edm/edm_status"); ?>/"+edmid+"/"+status;
			}
		});
	}
</script>
	
	
 
