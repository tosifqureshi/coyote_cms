<!DOCTYPE html>
<html>
	<body style="font-family:arial; margin: 0px; padding: 0px;">
		<table style="width:500px;border: 1px solid grey;margin: 0 auto;border-collapse: collapse; position: relative;">
			<tbody style="display: table-cell;">
				<tr style="background: #ffd100;">
					<th>
						<span style="float: left"><img width="180" src="{#SITE_URL#}uploads/logo.jpg"></span>
						<span  style="float:right">
							<ul style="list-style: none;height: 21px;">
								<li style="float: left;display: inline-block;padding: 0 15px;line-height:1.2;font-size: 15px;"><a href=""style="text-decoration: none;color: #000;">Store</a></li>
								<li style="float: left;display: inline-block;padding: 0 15px;line-height:1.2;font-size: 15px;"><a style="text-decoration: none;color: #000;" href="">Offer</a></li>
								<li style="float: left;display: inline-block;padding: 0 15px;line-height:1.2;font-size: 15px;"><a style="text-decoration: none;color: #000;" href="">Our Product</a></li>
							</ul>
						</span>
					</th>                           
				</tr>
				<tr>
				   <td colspan="4">
					  <img width="525" src="{#SITE_URL#}uploads/banner.jpg">
				   </td>
				</tr>
				<tr style="height:10px" >
				   <td colspan="4"></td>
				</tr>
				<tr>
					<td style="text-align: center;" >
						<table style="margin:0 auto;">
							<tr>
								<?php
								if(!empty($edm_data)){
									foreach($edm_data as $product) { ?>		
										<td style="width:141px;padding: 10px; text-align: center;border: 1px solid #ccc;background: #f5f5f5;float:left;display: inline-block; margin:5px;">
											<img src="{#SITE_URL#}uploads/Homepage-offers_1.jpg" width="100%">
											<p>
												<?php echo (strlen($product['Prod_Desc']) > 14) ? substr($product['Prod_Desc'],0,14).'..' :  $product['Prod_Desc'];?>
											</p>
											<a href="{#VIEW_PRODUCT#}<?php echo (!empty($product['Prod_Number'])?encode($product['Prod_Number']):''); ?>" style="text-decoration: none;color: #000;border: 1px solid #000;padding: 5px 28px;font-size: 14px;">view</a>
										</td>
										<?php
									}
								}
								?>
							</tr>
						</table>
					</td>	
				</tr>	
				<tr style="border-bottom: 1px solid #f5eeee;">
					<td colspan="4">
						<a style="text-align: right;float: right;padding: 5px;" href="{#VIEW_MORE_PRODUCT#}">View more</a>
					</td>
				</tr>
				<tr style="padding:10px; background: #fff;">
				   <td colspan="4" style="text-align: center;margin: 0 auto;vertical-align: middle;">
					  <p style="display: inline-block;vertical-align: middle;">Let's Be Besties offers</p>
					  <a href="#" style="margin-right: 10px;"><img style="vertical-align: middle;" src="{#SITE_URL#}uploads/inta.png"></a>
					  <a href="#" style="margin-right: 10px;"><img style="vertical-align: middle;" src="{#SITE_URL#}uploads/tter.png"></a>
					  <a href="https://www.facebook.com/NightOwlConvenience" style="margin-right: 10px;"><img  style="vertical-align: middle;" src="{#SITE_URL#}uploads/fb.png"></a>
					  <a href="#" style="margin-right: 10px;"><img style="vertical-align: middle;" src="{#SITE_URL#}uploads/yt.png"></a>
				   </td>
				</tr>
				<tr style="text-align: center; text-align: center;background:#ffd100;color: #000;">
				   <td colspan="4" style="text-align: center;">
					  <img width="165" style="margin-top: 11px;" src="{#SITE_URL#}uploads/logo.jpg">
					  <p style="font-size:12px" >This message was sent to admin@cdnsol.com.<br>If you no longer wish to receive these emails, unsubscribe</p>
					  <img style="vertical-align: middle; position: absolute; opacity:0; left:0;" src="{#GIF_IMAGEURL#}"/>
					  <p style="font-size:12px">Copyright @ 2017 Cdnsol, Inc<br>
						 Unit No. 304, Princes Business Skypark, Indore 452010
					  </p>
				   </td>
				</tr>
			 </tbody>
		</table>
	</body>
</html>
