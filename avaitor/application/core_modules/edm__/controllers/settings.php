<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Competition Controller
 * Manages the competition functionality on the admin pages.
 * @package    Avaitor
 * @subpackage Modules_edm
 * @category   Controllers
 * @author     CDN Team
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------
	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();
		$this->auth->restrict('Core.Users.View');
		$this->load->library('form_validation');
		$this->load->model('edm/edm_model');
		$this->load->language('edm');
		//table name
		$this->edm_tbl		  = 'edm';		 
		$this->email_template = 'email_template';	
		$this->master_log	  = 'edm_master_log';	 
		$this->edm_queue_log  = 'ava_edm_queue_log';	 
		$this->reward_cards   = 'reward_cards';	 
		// set banner images path
		$this->edm_path = FCPATH.'uploads/edm_images/'; 
	}//end_construct()

    
	/*
	 * load the view of edm listing
	 * @access public
	 * @return  void
	 */
	public function index() {
		$month_array = array();
		for($month=0;$month < 6;$month++) {
			$month_array[$month]['mNumber']= date('n', strtotime("-$month month"));
			$month_array[$month]['mName']= date('F', strtotime("-$month month"));
		}
		// get only those edm whos product view count exists
		$viewed_edm_data = $this->edm_model->get_viewed_edm_data();
		// get current year's email read-unread log
		$year_email_log = $this->get_edm_read_unread_data(date('Y'));
		// set view params	
		Template::set('month_array',$month_array);
		Template::set('edm_statics',$this->edm_model->edmstatistics($month_array));
		Template::set('edm_data',$this->edm_model->get_edmData('result','','','','id','DESC'));
		Template::set('edm_count',$this->edm_model->get_edm_count());
		//Template::set('edm_read_unread_log',$this->get_edm_read_unread_data());
		Template::set('read_emails',(isset($year_email_log['read_emails'])?$year_email_log['read_emails']:0));
		Template::set('unread_emails',(isset($year_email_log['unread_emails'])?$year_email_log['unread_emails']:0));
		Template::set('viewed_edm_data',$viewed_edm_data);
		Template::set('edm_active_inactive_records',$this->get_all_edm_records());
		Template::set('edm_product_yearly_viewed',$this->edm_product_yearly_viewed($viewed_edm_data,date('Y')));
		Template::set('admin_id',encode($this -> session -> userdata('user_id')));
		//Template::set('edm_data',$this->edm_model->get_active_edm());
		Template::set_view('edm_list');
		Template::render();
	}
	
	/*
	 * load the view of edm listing
	 * @access public
	 * @return  void
	 */
	public function edmlisting() {
		Template::set_view('edm_table_list');
		Template::render();
	}
    
    /*
	 * load the view of create edm 
	 * @access public
	 * @return  void
	 */
	public function create($edmId=0){
		if(!empty($edmId)){
			$edmId    = decode($edmId);
			$edm_data = $this->common_model->getDataFromTabel($this->edm_tbl,'*',array('id'=>$edmId),'','','',1,'','',1,1);
			$edm_data = !empty($edm_data)?$edm_data:"";
			$email_id = ((isset($edm_data['email_id']))?$edm_data['email_id']:0);
			$edm_template = $this->common_model->getDataFromTabel($this->email_template,'*',array('id'=>$email_id),'','','',1,'','',1,1);
			Template::set('edm_template',(!empty($edm_template)?$edm_template:""));
			Template::set('edm_data',$edm_data);
		}
		// set product input limit
		$no_of_top_products  	=  (isset($edm_data['no_of_top_products']) && !empty($edm_data['no_of_top_products']))?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
		$no_of_bottom_products  =  (isset($edm_data['no_of_bottom_products']) && !empty($edm_data['no_of_bottom_products']))?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
		$rewardCard_data = $this->common_model->getDataFromTabel($this->reward_cards,'id,card_name',array('status'=>1,'is_deleted'=>0),'','','','','','',1,'');
		Template::set('rewardCard_data',$rewardCard_data);
		//Template::set('top',$this->getProductList('top',$no_of_top_products)) ;
		//Template::set('bottom',$this->getProductList('bottom',$no_of_bottom_products)) ;
		Template::set('storeList',$this->edm_model->get_store_list()) ;
		//Template::set('offers',$this->edm_model->get_all_offers());
		Template::set('grid_template_view',$this->load->view('grid_template_view',(isset($edm_data)) ? $edm_data : NULL,TRUE));
		Template::set('list_template_view',$this->load->view('list_template_view',(isset($edm_data)) ? $edm_data : NULL,TRUE));
		Template::set_view('edm_create');
		Template::render();
	}
	
	 /*
	 * load the view of create edm 
	 * @access public
	 * @return  void
	 */
	public function setting($edmId=0){
		if(!empty($edmId)){
			$edmId    = decode($edmId);
			$edm_data = $this->common_model->getDataFromTabel($this->edm_tbl,'*',array('id'=>$edmId),'','','',1,'','',1,1);
			$edm_data = !empty($edm_data)?$edm_data:"";
			$email_id = ((isset($edm_data['email_id']))?$edm_data['email_id']:0);
			$edm_template = $this->common_model->getDataFromTabel($this->email_template,'*',array('id'=>$email_id),'','','',1,'','',1,1);
			Template::set('edm_template',(!empty($edm_template)?$edm_template:""));
			Template::set('edm_data',$edm_data);
		}
		// set product input limit
		$no_of_top_products  	=  (isset($edm_data['no_of_top_products']) && !empty($edm_data['no_of_top_products']))?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
		$no_of_bottom_products  =  (isset($edm_data['no_of_bottom_products']) && !empty($edm_data['no_of_bottom_products']))?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
		//$rewardCard_data = $this->common_model->getDataFromTabel($this->reward_cards,'id,card_name',array('status'=>1,'is_deleted'=>0),'','','','','','',1,'');
		//echo '<pre>';print_r($rewardCard_data);die;
		//Template::set('rewardCard_data',$rewardCard_data);
		Template::set('top',$this->getProductList('top',$no_of_top_products)) ;
		Template::set('bottom',$this->getProductList('bottom',$no_of_bottom_products)) ;
		//Template::set('storeList',$this->edm_model->get_store_list()) ;
		//Template::set('offers',$this->edm_model->get_all_offers());
		Template::set_view('edm_setting');
		Template::render();
	}
	
    /*
	 * ajax request for product list
	 * @access getProductList
	 * @return  list
	 */
	public function getProductList($type,$limit=5,$department_id=''){
		$response  = $this->edm_model->get_products($limit,$type,$department_id);
		$options   = array();
		if(!empty($response)){
			$options = $response;
		}
		return $options;
	}	
    
   /*
	 * ajax request for product list
	 * @access getProductList
	 * @return  list
	 */
	public function edm_configration($id=0){
		$data   = $this->input->post();
		$msg    = "something went wrong";
		$return = FALSE;
		$res_data = $email_data = array();
		if(!empty($data)){
			$edm_id					 	= (!empty($data['edm_id'])?$data['edm_id']:'');
			$res_data['edm_title'] 		= (!empty($data['edm_title'])?$data['edm_title']:'');
			$res_data['start_date']	    = (!empty($data['start_date'])?date('Y-m-d', strtotime($data['start_date'])):'');
			$res_data['end_date'] 		= (!empty($data['end_date'])?date('Y-m-d', strtotime($data['end_date'])):'');
			$res_data['send_time'] 		= (!empty($data['send_time'])?date('H:i:s', strtotime($data['send_time'])):'');
			$res_data['product_add_type']  	= (!empty($data['product_add_type'])?$data['product_add_type']: '');
			$res_data['product_top'] 	= ((!empty($data['product_top']) && $res_data['product_add_type'] == 2)?json_encode($data['product_top']):'');
			$res_data['product_bottom'] = ((!empty($data['product_bottom']) && $res_data['product_add_type'] == 2)?json_encode($data['product_bottom']):'');
			$res_data['product_other'] 	= (!empty($data['product_other'])?$data['product_other']:'');
			$occurance_type 		    = (!empty($data['occurance_type'])?$data['occurance_type']:'');
			$res_data['occurance_type'] = $occurance_type;
			switch($occurance_type){
				case 1:
				$res_data['occurance_data'] = '';
				break;
				case 2:
				$res_data['occurance_data'] = (!empty($data['weekly_days'])?json_encode($data['weekly_days']):'');
				break;
				case 3:
				$res_data['occurance_data'] = (!empty($data['monthly_dates'])?json_encode($data['monthly_dates']):'');
				break;
				case 4:
				$res_data['occurance_data'] = (!empty($data['yearly_time'])?json_encode($data['yearly_time']):'');
				break;
			}
			$res_data['rec_type'] 				= (!empty($data['rec_type']) ? json_encode($data['rec_type']) : '');			
			$res_data['rec_home_store_id'] 	  	= ((!empty($data['rec_home_store_id']) && in_array(1,$data['rec_type'])) ? json_encode($data['rec_home_store_id']) : '');
			$res_data['rec_reward_card_type'] 	= ((!empty($data['rec_rc']) && !in_array(5,$data['rec_type']))?json_encode($data['rec_rc']):'');			
			$res_data['no_products'] 			= (!empty($data['no_products'])?$data['no_products']:'');
			$res_data['no_of_top_products'] 	= (!empty($data['no_of_top_products'])?$data['no_of_top_products']:0);
			$res_data['no_of_bottom_products'] 	= (!empty($data['no_of_bottom_products'])?$data['no_of_bottom_products']:0);
			$res_data['is_subscription']    	= (!empty($data['is_subscription'])?$data['is_subscription']:0);			
			$res_data['email_description'] 		= (!empty($data['email_description'])?$data['email_description']:'');
			$res_data['email_footer_text'] 		= (!empty($data['email_footer_text'])?$data['email_footer_text']:'');
			$res_data['product_mapping']   		= (!empty($data['product_mapping']) ? 1 : 0);	
			$res_data['is_always_send_to_all_users'] = (!empty($data['is_always_send_to_all_users']) ? 1 : 0);	
			$res_data['product_grid_title'] 	= (!empty($data['product_grid_title'])?$data['product_grid_title']: '');
			$res_data['instore_offers']  		= (!empty($data['instore_offers_json'])?$data['instore_offers_json']: '');
			$res_data['template_type']  		= (!empty($data['template_type'])?$data['template_type']: '');
			$res_data['exclude_cat']  			= (!empty($data['exclude_cat'])?$data['exclude_cat']: '');
			$res_data['edm_products_json'] 		= ((!empty($data['edm_products_json']) && $res_data['product_add_type'] == 1)?$data['edm_products_json']: '');
			$res_data['edm_sell_base_products_json']  = ((!empty($data['edm_sell_base_products_json']) && $res_data['product_add_type'] == 2)?$data['edm_sell_base_products_json']: '');
			$res_data['manual_emails']  		= ((!empty($data['manual_emails']) && in_array(5,$data['rec_type'])) ? preg_replace('/\s+/', '', $data['manual_emails']) : '');
			
			$email_id = (!empty($data['email_id'])?$data['email_id']:'');
			// Set input field values
			if (isset($_FILES['email_banner_image']) && ($_FILES['email_banner_image']['name']) != '' ) {
				// remove old image
				(isset($data['banner_image_val']) && !empty($data['banner_image_val'])) ? unlink($this->edm_path.$data['banner_image_val']) : '';
				
				// upload image
				$res_data['email_banner_image'] = $this->to_upload_banner_image('email_banner_image');
			} else if(!empty($data['banner_image_val'])) {
				$res_data['email_banner_image'] = $data['banner_image_val'];
			}
			// prepare email template
			$email_data['body'] = $this->prepare_email_template($res_data);
			$email_data['subject']  = (!empty($data['subject'])?$data['subject']:'');
			$email_data['email_from_name'] = (!empty($data['email_from_name'])?$data['email_from_name']:'');
			if(empty($edm_id)){
				$email_data['purpose'] 	      = 'edm_email_purpose';
				$email_data['language'] 	  = 'en';
				$email_data['template_type']  =  2;
				$edm_eamil_id = $this->common_model->addDataIntoTable($this->email_template,$email_data);
			
				$res_data['email_id'] = (!empty($edm_eamil_id)?$edm_eamil_id:0);
				$edm_Id = $this->common_model->addDataIntoTable($this->edm_tbl,$res_data);			
				$return = TRUE;
			}else{
				$edm_Id = $this->common_model->updateDataFromTable($this->edm_tbl,$res_data,'id',$edm_id);	
				$this->common_model->updateDataFromTable($this->email_template,$email_data,'id',$email_id);	
				$return = TRUE;
			}
		}
		Template::set_message(lang('edm_configured'),'success');
		echo json_encode(array('return'=>$return,'msg'=>$msg));
		die;
	}
	
	 /*
	 * Function used to prepare edm template html
	 * @input edm_data array
	 * @return string
	 * @access private
	 */
	private function prepare_email_template($email_data=array()) {
		// set action type as store template
		$email_data['action'] = 'save_template';
		if($email_data['template_type'] == 'grid_view') {
			$edm_template = $this->load->view('grid_template_view',$email_data,TRUE);
		} else {
			$edm_template = $this->load->view('list_template_view',$email_data,TRUE);
		}
		
		return $edm_template;
	}
	
	/**
	 * change the status of edm
	 * @access edmiId/status
	 * @return true/false
	 **/
	public function edm_status($edmId,$status){
		$edmId  = decode($edmId);
		$status = decode($status);
		$msg_type = "error";
		$message  = "Something went wrong please try after some time";
		if(!empty($edmId)){
		 $update_data = array('is_deleted'=>((empty($status))?1:0));	
		 $edm_Id 	  = $this->common_model->updateDataFromTable($this->edm_tbl,$update_data,'id',$edmId);
		 $msg_type 	  = "success";
		 $message  	  = "edm status updated sucessfully";
		}
		Template::set_message($message,$msg_type);
		redirect(SITE_AREA .'/settings/edm');
	}
	
	 /*
	 * ajax request for edmlist list
	 * @access edmlist
	 * @return  list
	 */
	public function edmlist(){
			  $column      = '';
			  $dirs        = '';
			  $search      = '';			
			  if (isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
				$dirs = $_GET['order'][0]['dir'];
			  }
			  if ($_GET['order'][0]['column'] == 0) {
				$dirs = 'DESC';
			  }
			  if (isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])) {
				$column = $_GET['order'][0]['column'];
			  }
			  if (isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
				$search = $_GET['search']['value'];
				$search = trim($search);
				$search = str_replace(array("'", "\"","“","”","’"), "", $search );
			  }
			  $total_records = $this->edm_model->get_edmData('count', '', '',$search,$column,$dirs);
			  $records       = $this->edm_model->get_edmData('result',$_GET['length'],$_GET['start'],$search,$column,$dirs); 
			  $messages      = array();
			  $content       = '[';
			  $i             = 0;
			  $s             = $_GET['start'] + 1;
			  	 
		  if (!empty($records)) {
			  foreach ($records as $record){	
					  $recordId     = $record->id;			  
					  $avail_diff =  get_date_diffrence($record->start_date,$record->end_date,6,1);
					  if($avail_diff == 'Closed'){
							$txt      = 'closed';
							$lablType = 'success';
					  }else{
							$txt = $avail_diff;
							$lablType = 'error';
					  }
					$en_status  = encode($record->is_deleted);
					$en_edmid   = encode($recordId);
					$btn_status = (empty($record->is_deleted))?'active-btn':'deactive-btn';
					$edit_url   = site_url(SITE_AREA .'/settings/edm/create/'.$en_edmid); 
					$content .= '[';          
					$messages[$i][0]  = $s;
					$messages[$i][1]  = (!empty($record->edm_title)) ? $record->edm_title : 'N/A';
					$messages[$i][2]  = "<span class='label label-$lablType'>$txt</span>";
					$messages[$i][3]  = "<a class='common-btn $btn_status' onClick=\"javascript: changeStatus('$en_edmid','$en_status');\"><i class='fa fa-toggle-off'></i></a>";
					$messages[$i][4]  = "<a href='$edit_url' data-toggle='tooltip' title='Click here to edit edm'>
										 <span class='label bg-green btn-xs'>
										 <i class='fa fa-pencil' aria-hidden='true'></i>
										 </span>       
										 </a>";
					  $i++;
					  $s++;
			}
			$content .= ']';
			$final_data = json_encode($messages);
		   }else{
			$final_data = '[]';
		   }
		  echo '{"draw":' . $_GET['draw'] . ',"recordsTotal":' . $total_records . ',"recordsFiltered":' . $total_records . ',"data":' . $final_data . '}';
		  die;
	}
	
	
		/*
     * @coyote user Export xls     * 
     */
    public function edmExportxls(){
		//----------get export record list--------------
        $getRecordListing  = $this->edm_model->get_edmData('result','','','','','');
        $recordListing = array();
        $i=0;
        if(!empty($getRecordListing)) {
            foreach($getRecordListing as $record) {                      
              $recordListing[$i][0]  = $record->id;
			  $recordListing[$i][1]  = (!empty($record->edm_title)) ?ucfirst($record->edm_title): 'N/A';
			  $recordListing[$i][2]  = (!empty($record->email)) ? $record->email : 'N/A';
			  $recordListing[$i][3]  = (!empty($record->start_date))?date('M j, y g:i A', strtotime($record->start_date)): 'N/A';
			  $recordListing[$i][4]  = (!empty($record->end_date))?date('M j, y g:i A', strtotime($record->end_date)): 'N/A';
			  $recordListing[$i][5]  = (!empty($record->created_date))?date('M j, y g:i A', strtotime($record->created_date)): 'N/A';
			  $i++;
            }
        }
        //------set header title-------------
        $headingTitle = array('ID','Edm Title','Email','Start Date','Store','End Date','Create Date');
        
        //------set export file name---------
        $filename  = 'Coyote-User-Report-'.date('d-M-Y-H-s-i').'.xls';
        //-----set report genereate time-------
        $currentDate = date('d-M-Y H:m:i A');
        
        $this->report_export->reportTitle         = 'Coyote Edm Export';
        $this->report_export->reportName          = 'Coyote Edm Export';
        $this->report_export->reportHeading       = 'Edm Report Export';
        $this->report_export->reportGenerateDate  = $currentDate;
        $this->report_export->filename            = $filename;
        $this->report_export->headingTitle        = $headingTitle;
        $this->report_export->exportData          = $recordListing;
        $this->report_export->columRange          = range("A","H");
        //print_r($this->report_export);die;
		//--------xls report export----------
		$this->report_export->xlsexport();
        
    }//restaurantLedgerExportxls

	
	
	/**
	 * send_edm for sending edm to the user
	 * @access send_edm
	 * @return  user list 
	 **/	
	public function edm_template(){
		$this->load->view('edm_template');
	}
	
	/**
	 * create edm template ajax request
	 * @access send_edm
	 * @return  user list 
	 **/
	public function edm_template_config(){
		$data     = $this->input->post();
		$result   = FALSE;
		$responce = array();
		if(!empty($data)){
			$product_top 	= !empty($data['product_top']) ? $data['product_top']:array();
			$product_bottom = !empty($data['product_bottom']) ? $data['product_bottom']:array();
			$product_other  = !empty($data['product_other']) ? array_filter(explode(',',$data['product_other'])) :array();
			$no_products    = !empty($data['no_products']) ? $data['no_products']: 3;
			if(!empty($product_top) || !empty($product_bottom) || !empty($product_other)){
				$product_array = array_merge($product_top,$product_bottom,$product_other);
				$responce['edm_data'] = $this->edm_model->edm_TemplateData($product_array,$no_products);
				$result    = TRUE;
				$view      = $this->load->view('edm_template',$responce,TRUE);	
				/*This is hard code url for img need to dynamic*/
				$site_url	    = "http://cdnsolutionsgroup.com/coyotev3/";
				$searchArray  = array("{#SITE_URL#}");
				$replaceArray = array($site_url);
				$view	    	= str_replace($searchArray,$replaceArray,$view);
			}
		}
		echo json_encode(array('result'=>$result,'view'=>(isset($view)?$view:"")));
		die;
	}
	
	/**
	 * @Get edm mail count
	 * @access public
	 * @return  get edm mail count
	 **/
	public function edm_mail_count(){
		$data     = $this->input->get();
		$result   = FALSE;
		if(!empty($data['edmval'])){
			$result   = TRUE;
			$edmCounts = $this->edm_model->edm_mail_count($data['edmval']);
			}
	   echo json_encode(array('result'=>$result,'count'=>(isset($edmCounts)?$edmCounts:"")));
	   die;
	}
	
	/**
	 * @description : Function used to get email's read unread log
	 * @input : null
	 * @output: array
	 * @access: private
	 * 
	 */
	 private function get_edm_read_unread_data($email_year='') {
		// get sent emails data
		$edm_read_unread_log = $this->edm_model->email_read_unread_log($email_year);
		// prepare emails read and unread year data
		$read_January 	= (!empty($edm_read_unread_log['read_January'])) ? $edm_read_unread_log['read_January'] : 0;
		$unread_January = (!empty($edm_read_unread_log['unread_January'])) ? $edm_read_unread_log['unread_January'] : 0;
		$read_February 	= (!empty($edm_read_unread_log['read_February'])) ? $edm_read_unread_log['read_February'] : 0;
		$unread_February = (!empty($edm_read_unread_log['unread_February'])) ? $edm_read_unread_log['unread_February'] : 0;
		$read_March 	= (!empty($edm_read_unread_log['read_March'])) ? $edm_read_unread_log['read_March'] : 0;
		$unread_March 	= (!empty($edm_read_unread_log['unread_March'])) ? $edm_read_unread_log['unread_March'] : 0;  
		$read_April 	= (!empty($edm_read_unread_log['read_April'])) ? $edm_read_unread_log['read_April'] : 0;
		$unread_April 	= (!empty($edm_read_unread_log['unread_April'])) ? $edm_read_unread_log['unread_April'] : 0;  
		$read_May 		= (!empty($edm_read_unread_log['read_May'])) ? $edm_read_unread_log['read_May'] : 0;
		$unread_May 	= (!empty($edm_read_unread_log['unread_May'])) ? $edm_read_unread_log['unread_May'] : 0;
		$read_June 		= (!empty($edm_read_unread_log['read_June'])) ? $edm_read_unread_log['read_June'] : 0;
		$unread_June 	= (!empty($edm_read_unread_log['unread_June'])) ? $edm_read_unread_log['unread_June'] : 0;
		$read_July 		= (!empty($edm_read_unread_log['read_July'])) ? $edm_read_unread_log['read_July'] : 0;
		$unread_July 	= (!empty($edm_read_unread_log['unread_July'])) ? $edm_read_unread_log['unread_July'] : 0;
		$read_August 	= (!empty($edm_read_unread_log['read_August'])) ? $edm_read_unread_log['read_August'] : 0;
		$unread_August 	= (!empty($edm_read_unread_log['unread_August'])) ? $edm_read_unread_log['unread_August'] : 0;
		$read_September = (!empty($edm_read_unread_log['read_September'])) ? $edm_read_unread_log['read_September'] : 0;
		$unread_September = (!empty($edm_read_unread_log['unread_September'])) ? $edm_read_unread_log['unread_September'] : 0;  
		$read_October 	= (!empty($edm_read_unread_log['read_October'])) ? $edm_read_unread_log['read_October'] : 0;
		$unread_October = (!empty($edm_read_unread_log['unread_October'])) ? $edm_read_unread_log['unread_October'] : 0; 
		$read_November 	= (!empty($edm_read_unread_log['read_November'])) ? $edm_read_unread_log['read_November'] : 0;
		$unread_November= (!empty($edm_read_unread_log['unread_November'])) ? $edm_read_unread_log['unread_November'] : 0;
		$read_December 	= (!empty($edm_read_unread_log['read_December'])) ? $edm_read_unread_log['read_December'] : 0;
		$unread_December= (!empty($edm_read_unread_log['unread_December'])) ? $edm_read_unread_log['unread_December'] : 0;
		// append read/unread months count
		$read_emails = "[$read_January,$read_February,$read_March,$read_April,$read_May,$read_June,$read_July,$read_August,$read_September,$read_October,$read_November,$read_December]";
		$unread_emails = "[$unread_January,$unread_February,$unread_March,$unread_April,$unread_May,$unread_June,$unread_July,$unread_August,$unread_September,$unread_October,$unread_November,$unread_December]";
		$read_emails_array = array($read_January,$read_February,$read_March,$read_April,$read_May,$read_June,$read_July,$read_August,$read_September,$read_October,$read_November,$read_December);
		$unread_emails_array = array($unread_January,$unread_February,$unread_March,$unread_April,$unread_May,$unread_June,$unread_July,$unread_August,$unread_September,$unread_October,$unread_November,$unread_December);
		return array('read_emails'=>$read_emails,'unread_emails'=>$unread_emails,'read_emails_array'=>$read_emails_array,'unread_emails_array'=>$unread_emails_array);
	 }
	 
	 //--------------------------------------------------------------------
	
	/**
	 * @description : Function used to get all active or inactive edm log
	 * @input : null
	 * @return : array
	 * 
	 */
	 function get_all_edm_records() {
		$active_edm = array();
		$inactive_edm = array();
		$current_date = date('Y-m-d');
		// get all edm data
		$edm_data = $this->edm_model->get_all_edm_data();
		foreach($edm_data as $edm) {
			// get edm read unread count
			$edm_read_unread_count = $this->edm_model->get_edm_read_unread_log($edm['id']);
			$edm_data = array();
			$edm_data['edm_title'] = $edm['edm_title'];
			$edm_data['edm_read_count'] = (!empty($edm_read_unread_count['edm_read'])) ? $edm_read_unread_count['edm_read'] : 0;
			$edm_data['edm_unread_count'] = (!empty($edm_read_unread_count['edm_unread'])) ? $edm_read_unread_count['edm_unread'] : 0;
			if(!empty($edm_data['edm_read_count']) || !empty($edm_data['edm_unread_count'])) {
				if($current_date <= $edm['end_date']) {
					$active_edm[] = $edm_data;
				} else {
					$inactive_edm[] = $edm_data;
				}
			}
		}
		return array('active_edm'=>$active_edm,'inactive_edm'=>$inactive_edm);
	 }
	 
	/**
	 * @description : Function used to get top product view counts
	 * @input : edm_id
	 * @output : json
	 * @access : public
	 **/
	public function edm_product_log(){
		$data    = $this->input->get();
		$result  = false;
		if(!empty($data['edmval'])){
			$result   = true;
			// get product seen counts
			$edm_product_counts = $this->edm_model->edm_product_view_data($data['edmval']);
			$products = array();
			$view_count = array();
			// set product count values
			foreach($edm_product_counts as $views) {
				$products[] = $views['product_id'];
				$view_count[] = $views['product_seen_count'];
			}
		}
	   echo json_encode(array('result'=>$result,'products'=>(isset($products)?json_encode($products):0),'view_count'=>(isset($view_count)?json_encode($view_count):0)));die;
	}
	
	/**
	 * @description : Function used to get yearly edm read-unread log
	 * @input : edm_id
	 * @output : json
	 * @access : public
	 **/
	public function yearlyEmailBarChart(){
		$data    = $this->input->get();
		$result  = false;
		$year_email_log = array();
		$emailYear = (!empty($data['emailYear'])) ? $data['emailYear'] : date('Y');
		if(!empty($emailYear)){
			$result   = true;
			// get perticuler year's email log
			$year_email_log = $this->get_edm_read_unread_data($emailYear);
		}
	   echo json_encode(
	   array('result'=>$result,
	   'read_emails'=>(isset($year_email_log['read_emails'])?$year_email_log['read_emails_array']:0),
	   'unread_emails'=>(isset($year_email_log['unread_emails'])?$year_email_log['unread_emails_array']:0)));die;
	}
	
	/**
	 * @description : Function used to get yearly edm prduct view log
	 * @input : viewed_edm_data(array)
	 * @output : array
	 * @access : public
	 **/
	public function edm_product_yearly_viewed($viewed_edm_data = array(),$prodYear='') {
		
		$month_array = array();
		for($month=1;$month <= 12;$month++) {
			$month_array[$month]['mNumber'] = date('n', mktime(0, 0, 0, $month, 10));
			$month_array[$month]['mName'] = date('F', mktime(0, 0, 0, $month, 10));
		}
		$edm_data = array();
		foreach($viewed_edm_data as $key=>$val) {
			if($key<=3){
				$edm_arr['edm_title'] = $val->edm_title;
				// get yearly product view record
				$product_viewed = $this->edm_model->edm_product_yearly_viewed($month_array,$val->id,$prodYear);
				$product_counts = array();
				foreach($month_array as $months){
					$mName   = $months['mName'];
					$product_counts[] = (!empty($product_viewed['viewed_'.$mName])) ? $product_viewed['viewed_'.$mName] : 0;
				}
				$edm_arr['product_counts'] = $product_counts;
				$edm_data[] = $edm_arr;
			}
		}
		return $edm_data;
	}
	
	/**
	 * @description : Function used to get yearly product log
	 * @input : edm_id
	 * @output : json
	 * @access : public
	 **/
	public function yearlyProdLineChart(){
		// get only those edm whos product view count exists
		$viewed_edm_data = $this->edm_model->get_viewed_edm_data();
		$data    = $this->input->get();
		$result  = false;
		$year_email_log = array();
		$prodYear = (!empty($data['prodYear'])) ? $data['prodYear'] : date('Y');
		if(!empty($prodYear)){
			$result   = true;
			// get perticuler year's product log
			$edm_product_yearly_viewed = $this->edm_product_yearly_viewed($viewed_edm_data,$prodYear);
		}
	   echo json_encode(
	   array('result'=>$result,
	   'edm_product_yearly_viewed'=>(isset($edm_product_yearly_viewed) ? $edm_product_yearly_viewed:0)));die;
	}
	
	/**
	 * @description : Function used to prepare edm products dropdown as per top / bottom type
	 * @input : null
	 * @output : json
	 * @access : public
	 **/
	public function get_products_selling_dropdown() {
		$data  = $this->input->post();
		$product_limit = $data['product_limit'];
		$selling_product_type = $data['selling_product_type'];
		$department_id = $data['department_id'];
		$existing_products = !empty($data['existing_products']) ? $data['existing_products'] : array();
		$sell_type = ($selling_product_type == 1) ? 'top' : 'bottom';
		$all_products = $this->getProductList($sell_type,$product_limit,$department_id);
		//$selectedProducts  = (empty($existing_products))?"<span class='hida'>".lang('select_product_'.$selling_product_type)."</span>":"";
		$selectedProducts  = '';
		$ulProductHtml   = "";
		$selected_product_name  = "";
		$isTop_dn  = "dn";
		$result = FALSE;
		$new_product_ids = array();
		$no_more_existing_product_ids = [];
		if(!empty($all_products)){
			foreach($all_products as $prod){
				$prId = $prod['JNAD_PRODUCT'];
				$prName = $prod['PROD_DESC'];
				$new_product_ids[] = (string)$prId;
				$isChecked = "";
				if(in_array($prod["JNAD_PRODUCT"],$existing_products)){
					$selectoPtion  = $prName.',';
					$selected_product_name      .= "<span title='$selectoPtion'>$selectoPtion</span>";
					$isChecked     = "checked";			 
				}
				if($selling_product_type == 1) {
					$ulProductHtml .= "<li><input type='checkbox' class='topSellingProducts' name='product_top[]' ".$isChecked." value='$prId' data-name='$prName' onchange='gettopSellingProducts()' />$prName</li>";
				} else {
					$ulProductHtml .= "<li><input type='checkbox' class='bottomSellingProducts' name='product_bottom[]' ".$isChecked." value='$prId' data-name='$prName' />$prName</li>";
				}
			}
			$selectedProducts .= "<ul class='multiSel dn'>".$selected_product_name."</ul>";
			$result = TRUE;
			// set no more existing products
			if(count($existing_products) > 0) {
				foreach($existing_products as $val) {
					if(!in_array($val, $new_product_ids)) {
						$no_more_existing_product_ids[] = $val;
					}
				}
			}
		}
		echo json_encode(array('ulProductHtml'=>$ulProductHtml,'selectedProducts'=>$selectedProducts,'selected_product_name'=>$selected_product_name,'result'=>$result,'no_more_existing_products'=>json_encode($no_more_existing_product_ids)));die;
	}
	
	 /*
	 * load the view of create edm 
	 * @access public
	 * @return  void
	 */
	public function createedm($edmId=0){

		if(!empty($edmId)){
			$edmId    = decode($edmId);
			$edm_data = $this->common_model->getDataFromTabel($this->edm_tbl,'*',array('id'=>$edmId),'','','',1,'','',1,1);
			$edm_data = !empty($edm_data)?$edm_data:"";
			$email_id = ((isset($edm_data['email_id']))?$edm_data['email_id']:0);
			$edm_template = $this->common_model->getDataFromTabel($this->email_template,'*',array('id'=>$email_id),'','','',1,'','',1,1);
			Template::set('edm_template',(!empty($edm_template)?$edm_template:""));
			Template::set('edm_data',$edm_data);
		}
		// set product input limit
		$no_of_top_products  	=  (isset($edm_data['no_of_top_products']) && !empty($edm_data['no_of_top_products']))?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
		$no_of_bottom_products  =  (isset($edm_data['no_of_bottom_products']) && !empty($edm_data['no_of_bottom_products']))?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
		$exclude_cat 			=  (isset($edm_data['exclude_cat']) && !empty($edm_data['exclude_cat'])) ? $edm_data['exclude_cat'] : '';
		$rewardCard_data = $this->common_model->getDataFromTabel($this->reward_cards,'id,card_name',array('status'=>1,'is_deleted'=>0),'','','','','','',1,'');
		
		Template::set('rewardCard_data',$rewardCard_data);
		Template::set('top',$this->getProductList('top',$no_of_top_products,$exclude_cat)) ;
		Template::set('bottom',$this->getProductList('bottom',$no_of_bottom_products,$exclude_cat)) ;
		Template::set('storeList',$this->edm_model->get_store_list()) ;
		//Template::set('offers',$this->edm_model->get_all_offers());
		Template::set('edm_template2',$this->load->view('edm_template2',NULL,TRUE));
		Template::set('grid_template_view',$this->load->view('grid_template_view',NULL,TRUE));
		Template::set('list_template_view',$this->load->view('list_template_view',NULL,TRUE));
		Template::set_view('edm_create2');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Upload edm banner image
	 * @input : image
	 * @return: void
	 * @access: private
	 */
	private function to_upload_banner_image($image='',$upload_at='edm_images/') {
		
		$dirPath = IMAGEUPLOADPATH.$upload_at;
		
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){					
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('admin/settings/edm');					
		}
		
		$imageData = $this -> upload -> data();
		
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	
}//end Settings

/* Location: ./application/core_modules/edm/controllers/setting.php */
