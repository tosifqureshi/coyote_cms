<style>
 .chosen-choices
  {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    cursor: text;
    height: auto !important;
    height: 1%;
    overflow: hidden;
    padding: 0;
    position: relative;
    width: auto;
    margin-top:0px;
    margin-bottom:5px;
    }
.dropdown {
  position: relative;
      margin: 0;
    padding: 0;
    margin-bottom: 15px;
 }


.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: -1px 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #fff;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
     background-color: #95979829;
    display: block;
    min-height: 30px;
    line-height: 30PX;
    border: 0;
    width: 272px;
    padding: 7px 5px;
    color: black;
    margin-top:30px;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
    padding: 0 3px 2px 0;
    color: #3e3d3d;
    display: list-item;
    list-style-type: decimal;
    list-style-position: inside;
    line-height: 20px;
}

.dropdown dd ul {
     display: none;
     background-color: #e1e2e2;
    color: #3a3b36;
    left: 0px;
    padding: 2px 15px 15px 5px;
    top: 2px;
    width: 272px;
    list-style: none;
    height:90px;
    overflow: auto;
    font-size: 12px;
    border: 1px solid #ccc7c7;
    margin-bottom: 10px;
        overflow-y: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #fff;
}
.p-relative{ position:relative;}
.p-relative label.error {
    position: absolute;
    top: -12px;
    min-width: 300px;
}
.rewardcard-value {
    position: relative;
    padding-bottom: 20px;
}
.rewardcard-value label.error{ 
	position: absolute;
    bottom: 0px;
    min-width: 300px;
}

</style>  
<link href="<?php echo Template::theme_url('edm-dashboard/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" media="all"/>

<?php
$edm_id 	   			=  !empty($edm_data['id'])?$edm_data['id']:'';
$edm_title  			=  !empty($edm_data['edm_title'])?$edm_data['edm_title']:'';
$start_date 			=  !empty($edm_data['start_date'])?$edm_data['start_date']:'';
$end_date  				=  !empty($edm_data['end_date'])?$edm_data['end_date']:'';
$product_top  			=  !empty($edm_data['product_top'])?json_decode($edm_data['product_top']):array();
$product_bottom  		=  !empty($edm_data['product_bottom'])?json_decode($edm_data['product_bottom']):array();
$product_other  		=  !empty($edm_data['product_other'])?$edm_data['product_other']:'';
$occurance_type  		=  !empty($edm_data['occurance_type'])?$edm_data['occurance_type']:'';
$occurance_data  		=  !empty($edm_data['occurance_data'])?json_decode($edm_data['occurance_data']):'';
$rec_home_store_id  	=  !empty($edm_data['rec_home_store_id'])?json_decode($edm_data['rec_home_store_id']):array();
$rec_previous_purchase  =  !empty($edm_data['rec_previous_purchase'])?$edm_data['rec_previous_purchase']:'';
$rec_all  				=  !empty($edm_data['rec_all'])?$edm_data['rec_all']:'';
$rec_type			  	=  !empty($edm_data['rec_type'])?json_decode($edm_data['rec_type']):array();
$rec_reward_card_type  	=  !empty($edm_data['rec_reward_card_type'])?json_decode($edm_data['rec_reward_card_type']):array();
$no_products  			=  !empty($edm_data['no_products'])?$edm_data['no_products']:'';
$no_of_top_products  	=  !empty($edm_data['no_of_top_products'])?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
$no_of_bottom_products  =  !empty($edm_data['no_of_bottom_products'])?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
$is_subscription  		=  !empty($edm_data['is_subscription'])?$edm_data['is_subscription']:'';
$created_date  			=  !empty($edm_data['created_date'])?$edm_data['created_date']:'';
$product_top_val  		=  !empty($edm_data['product_top'])?$edm_data['product_top']:'';
$product_bottom_val  	=  !empty($edm_data['product_bottom'])?$edm_data['product_bottom']:'';
$product_add_type  		=  !empty($edm_data['product_add_type'])?$edm_data['product_add_type']:0;

//$SelectTop = (empty($product_top))?"<span class='hida'>Select top selling product</span>":"";
$SelectTop = "";
$TopHtml   = "";
$TopName   = "";
$isTop_dn  = "dn";
if(!empty($top)){
	foreach($top as $topData){
		$prId = $topData['JNAD_PRODUCT'];
		$prName = $topData['PROD_DESC'];
		$isTopchecked = "";
		if(in_array($topData["JNAD_PRODUCT"],$product_top)){
			$selectoPtion  = $prName.',';
			$TopName      .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isTopchecked  = "checked";	 
			$isTop_dn 	    = "";			 
		}
		$TopHtml .= "<li><input type='checkbox' class='topSellingProducts' name='product_top[]' ".$isTopchecked." value='$prId' data-name='$prName' />$prName</li>";
	 }
	 $SelectTop .= "<ul class='multiSel dn'>".$TopName."</ul>";
}

//$SelectBottom = (empty($product_bottom))?"<span class='hida'>Select bottom selling product</span>":"";
$SelectBottom = '';
$BottomHtml   = "";
$BottomName   = "";
$isBottom_dn  = "dn";
if(!empty($bottom)){
	foreach($bottom as $bottomData){
		$prId   = $bottomData['JNAD_PRODUCT'];
		$prName = $bottomData['PROD_DESC'];
		$isBottomchecked = "";
		if(in_array($bottomData["JNAD_PRODUCT"],$product_bottom)){
			$selectoPtion = $prName.',';
			$BottomName  .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isBottomchecked = "checked";
			$isBottom_dn = "";
		}
		$BottomHtml .= "<li><input type='checkbox' class='bottomSellingProducts' name='product_bottom[]'  ".$isBottomchecked."   value='$prId' data-name='$prName' />$prName</li>";
	}
	$SelectBottom .= "<ul class='multiSel dn'>".$BottomName."</ul>";
}

$SelectStore  = (empty($rec_home_store_id))?"<span class='hida'>Select Store</span>":"";
$StoreHtml   = "";
$StoreName   = "";
$isStore_dn  = "dn";
if(!empty($storeList)){
	 foreach($storeList as $key=>$value){
		   $prId   = $key;
		   $prName = $value;
           $isStorechecked = "";
         if(in_array($prId,$rec_home_store_id)){
			 $selectoPtion = $value.',';
			 $StoreName   .= "<span title='$selectoPtion'>$selectoPtion</span>";
			 $isStorechecked = "checked";
			 $isStore_dn = "";
		 }
         $StoreHtml .= "<li><input type='checkbox' class='homeStores' name='rec_home_store_id[]' ".$isStorechecked." value='$prId' data-name='$prName' />$prName</li>";
	 }
 }

$days = array(
		'sunday'   =>'Sunday',
		'monday'   =>'Monday',
		'tuesday'  =>'Tuesday',
		'wednesday'=>'Wednesday',
		'thursday' =>'Thursday',
		'friday'   =>'Friday',
		'saturday' =>'Saturday',
	);
// set product id array
$product_ids = [];
$product_names = [];
if(!empty($edm_data['edm_products_json'])) {
	$edm_products = json_decode($edm_data['edm_products_json']);
	foreach($edm_products as $val) {
		$product_ids[] = $val->product_id;
		$product_names[$val->product_id] = $val->product_name;
	}
}
$product_ids = (count($product_ids) > 0 ) ? json_encode($product_ids) : '';
$product_names = (count($product_names) > 0 ) ? json_encode($product_names) : '';?>

<!-- Title input start here -->
<!-- new tab Start Content panel -->
 <div class="col-md-6" style="padding:0px 15px 0px 0px; margin:0px;">
	<div class="tabbable ">
		<?php echo form_open_multipart($this->uri->uri_string(),array('name'=>'frm_edm','id'=>'frm_edm')); ?>
			<ul class="nav nav-tabs edm-tabs process">
				<li class="active" data-tab="info_tab">
					<a><?php echo lang('nav_tab1');?></a></li>
				<li data-tab="product_tab"><a><?php echo lang('nav_tab2');?></a></li>
				<li data-tab="edm_email_tab"><a><?php echo lang('nav_tab5');?></a></li>
				<li data-tab="frequency_tab"><a><?php echo lang('nav_tab3');?></a></li>
				<li data-tab="receiver_tab"><a><?php echo lang('nav_tab4');?></a></li>
			</ul>
			<div class="tab-content scroll mt10">
				<!-- Start edm general tab panel -->
				<div class="tab-pane active" id="info_tab">
					<div class="form-wrapper clearboth">
						<!-- EDM title input start here -->
						<div class="form-box">
							<label for="edm-other-other_tab"><?php echo lang('edm_title_labl');?><i style="color:red;">*</i></label>															
							<?php
							echo form_hidden('edm_id',$edm_id);
							$data = array('name'=>'edm_title','id'=>'edm_title', 'value'=>$edm_title,'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
							echo form_input($data);
							?>
						</div>
						<!-- EDM title input end here -->
							
						<!-- Start_date input start here -->
						<div class="form-box">
							<div class="iline-view">
								<label for="co_start_date"><?php echo lang('edm_start_date');?><i style="color:red;">*</i></label>
								<div class="half_date">
								   <div class="form-group input-append date" id="start_date_div1">
									  <?php
										 $data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$start_date,'id'=>'start_date','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
										 echo form_input($data);
										 ?>
									  <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
								   </div>
								</div>
							</div>
							<div class="iline-view">
								<label for="co_end_date"><?php echo lang('edm_end_date');?></label>
								<div class="half_date">
								   <div class="form-group input-append date" id="end_date_div1">
									  <?php
										 $data = array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$end_date,'id'=>'end_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_end_date'), 'readonly'=>'readonly');
										 echo form_input($data);?>
									  <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
								   </div>
								</div>
							</div>	
						</div>
						<!-- End date input end here -->
						<div class="clearboth mb10"></div>	
								
						<!-- Email description text start here -->
						<div>
							<label><?php echo lang('email_description');?></label>
							<div class="control-group">	
								<?php
								$data	= array('name'=>'email_description', 'class'=>'edmmceEditor redactor span8 required', 'value'=>(!empty($edm_data['email_description'])?$edm_data['email_description']:''));
								echo form_textarea($data);
								?>
							</div>
						</div>		
						<!-- Email description text end here -->
							
						<!-- Email footer content box start here -->
						<div>
							<label><?php echo lang('email_footer_content');?></label>
							<div class="control-group">	
								<?php 
								$data	= array('name'=>'email_footer_text', 'class'=>'footermceEditor redactor span8', 'value'=>(!empty($edm_data['email_footer_text'])?$edm_data['email_footer_text']:''));
								echo form_textarea($data);
								?>
							</div>
						</div>		
						<!-- Email footer content box end here -->
					</div>
					<!-- End edm general tab panel -->
					
					<!-- Banner image box start here -->
					<div class="img-wrapper">
						<label for="title" class="img-title"><?php echo lang('email_banner_image');?></label> 	
						<div class="img-box">
							<?php
							// set banner image action
							$banner_image_val = (isset($email_banner_image) && !empty($email_banner_image)) ? $email_banner_image :'';
							$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
							if(!empty($banner_image_val)) {
								$banner_image_src = base_url('uploads/edm_images/'.$banner_image_val);
								$title = lang('tooltip_offer_image');
						
								$banner_image_val_doc = $this->config->item('document_path').'uploads/edm_images/'.$email_banner_image; 
								if(!file_exists($banner_image_val_doc)){
									// $promo_image_1 = base_url('uploads/No_Image_Available.png');
									$banner_image_src = base_url('uploads/No_Image_Available.png');
									$title = "No Image Available";
								}
							} else {
								$title = lang('tooltip_image');
								$banner_image_src = base_url('uploads/upload.png');
							} ?>
							<div class="cell-img" id="banner_image_cell">
								<img id="banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
							</div>
							<?php
							$data = array('name'=>'email_banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
							echo form_upload($data);
							// set banner image value in hidden type
							$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
							echo form_input($data);?>
						</div>
						<div class="alert red-alert alert-danger alert-dismissible ">
						   <i class="icon fa fa-warning"></i>
							<?php echo lang('img_upload_warning');?>
						</div>
						<!-- Banner image box end here -->
					</div>
					
					<!-- submit button here -->
					 <div class="form-box ">
						<button type="submit" name="next"   class="btn btn-primary btn-margin"><?php echo 'Next';?></button>
					 </div>
					<!-- submit button here -->
					<div class="clearboth"></div>
				</div>
				<!-- End referral system panel -->	
				
				<!-- Start general config panel -->
				<div class="tab-pane min-loader form-wrapper" id="product_tab">
					<div class="product-mapping-box">
						<!-- No product mapping radio start here -->
						<div class="form-box edm-receiver radio-product">
							<input  type="radio" name="product_add_type" value="0" <?php echo (empty($product_add_type)) ? "checked" : "";?> >
							<label>
							 <?php echo 'No Product Mapping';?>
							</label>
						</div>
						<!-- No product mapping radio end here -->	
						
						<!-- Product direct search radio start here -->
						<div class="form-box edm-receiver radio-product">
							<input  type="radio" name="product_add_type" value="1" <?php echo ($product_add_type ==1) ? "checked" : "";?> >
							<label>
							 <?php echo 'Direct Product Search';?>
							</label>
						</div>
						<!-- Product direct search radio end here -->		
						
						<!-- Sell based product search radio start here -->
						<div class="form-box edm-receiver radio-product">
							<input  type="radio" name="product_add_type" value="2"  <?php echo ($product_add_type == 2) ? "checked" : "";?>>
							<label>
							 <?php echo 'Sell Based Products';?>
							</label>
						</div>
						<!-- Sell based product search radio end here -->
						
						<!-- Product box title input start here -->
						<div class="form-box include-products <?php echo (!empty($product_add_type)) ? "" : "dn";?> ">
							<label><?php echo lang('product_grid_title');?><i style="color:red;">*</i><span class="fa fa-info-circle ml5" title="<?php echo lang('product_grid_title_help');?>"></span></label>
							<?php
							$data	= array('name'=>'product_grid_title','id'=>'product_grid_title','value'=>set_value('product_grid_title', isset($edm_data['product_grid_title'])?$edm_data['product_grid_title']: ''), 'class'=>'span8 required');
							echo form_input($data);?>
						</div>	
						<!-- Product box title input end here -->
					
						<!-- minimum reward point to redeem input start here -->
						<div class="form-box eclud-cat include-products <?php echo (!empty($product_add_type)) ? "" : "dn";?>">
							 <label for="title"><?php echo 'Exclude Category';?><span class="fa fa-info-circle ml5" title="<?php echo lang('exclude_cat_help');?>"></span></label>
							 <?php
								$data = array('name'=>'exclude_cat','id'=>'exclude_cat', 'value'=> isset($title) ? $title : '' , 'class'=>'span8', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
								echo form_input($data);
							 ?>
						</div>
						<!-- minimum reward point to redeem input end here -->
						
						<!-- Product direct search box start here -->						
						<div class="form-box mt10 <?php echo ($product_add_type ==1) ? "" : "dn";?>" id="product_direct_searchbox">
							<label><?php echo lang('edm_other_product');?><i style="color:red;">*</i></label>
							<div id="searchProductButton">
								<button type="button" name="product_search" class="btn btn-primary otherPr" ><?php echo lang('search_product');?></button>
							</div>
							<div class="alert alert-success alert-dismissible other-product-help">
								<i class="icon fa fa-info"></i> 
								<?php echo lang('other_product_search_help');?>
							</div>
							<div class="mt10 "  id="showproductDiv"></div>
							<input type="hidden" id="product_other" name="product_other" value="<?php echo (!empty($edm_data['product_other'])) ? $edm_data['product_other'] : ''?>">
							<!-- Product price options start here -->
							<?php 
							$edm_products_json = (!empty($edm_data['edm_products_json'])) ? $edm_data['edm_products_json'] : ''; ?>
							<div class="clearboth product-grid-form mb10">
								<table class="table table-bordered <?php echo (!empty($edm_products_json)) ? '' : 'dn';?>" id="product-table"> 
									<thead class="bg_ccc"> 
										<th><?php echo lang('product_id')?></th>
										<th><?php echo lang('product_name')?></th>
										<th><?php echo lang('product_price')?></th>
										<th><?php echo lang('special_price')?></th>                         
										<th><?php echo lang('delete_product')?></th>                         
									</thead>
									<tbody id="email_products" class="product_table_tbody">
										<?php
										$products = json_decode($edm_products_json);
										if(!empty($products)) {
											foreach($products as $val) {
												echo '<tr id="'.$val->product_id.'">';
												echo '<td><input type="text" readonly name="product_id[]" value="'.$val->product_id.'" class="form-control readonly_input width100"></td>';
												echo '<td><input type="text" readonly name="product_name[]" value="'.$val->product_name.'" class="form-control readonly_input width100"></td>';
												echo '<td><input type="text" name="product_price[]" value="'.$val->product_price.'" id="product_price_'.$val->product_id.'" class="form-control number product_input" row_id='.$val->product_id.'></td>';
												echo '<td><input type="text" name="special_price[]" value="'.$val->special_price.'" id="special_price_'.$val->product_id.'" class="form-control number product_input" row_id='.$val->product_id.'></td>';
												echo '<td><a class="common-btn remove-btn" row_id='.$val->product_id.'><i class="fa fa-trash"></i></a></td>';
											}
										} ?>
									</tbody>	
								</table>
							</div>		
							<!-- Product price options end here -->
						</div>
						<!-- Product direct search box end here -->
						
						<!-- Sell based product search box start here -->	
						<div id="sellbased_product_searchbox" class="<?php echo ($product_add_type == 2) ? "" : "dn";?>">
							<!-- number of top selling product input start here -->
							<div class="form-box">
								<label><?php echo lang('no_of_top_selling_products');?>
									<i style="color:red;">*</i>
									<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
								</label>
								<?php
								$data = array('name'=>'no_of_top_products','id'=>'no_of_top_products', 'value'=> $no_of_top_products ,
								'class'=>'span8 required product_selling_fetch_limit','product_selling_type'=>1, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_top_selling_products'));
								echo form_input($data);
								$data = array('type'=>'hidden','id'=>'no_of_top_products_hidden','value'=> $no_of_top_products);
								echo form_input($data);
								?>						
							</div>
							<!-- number of top selling product input end here -->
						
							<!-- number of bottom selling product input start here -->
							<div class="form-box">
								<label>
									<?php echo lang('no_of_bottom_selling_products');?><i style="color:red;">*</i>
									<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
								</label>
								<?php
								$data = array('name'=>'no_of_bottom_products','id'=>'no_of_bottom_products', 'value'=> $no_of_bottom_products ,
								'class'=>'span8 required product_selling_fetch_limit','product_selling_type'=>2, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_bottom_selling_products'));
								echo form_input($data);
								$data = array('type'=>'hidden','id'=>'no_of_bottom_products_hidden','value'=> $no_of_bottom_products);
								echo form_input($data);
								?>
							</div>
							<!-- number of bottom selling product input end here -->
						
							<!-- maximum reward point input start here -->
							<div class="form-box mt10 indispaly">
								<label>
									<!--
									<span class="fl mr5">
										<input  type="checkbox" onchange="cbchange(this)" data-show="topsell-pr" id="topSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_top)?"checked":""); ?>>
									</span>
									-->
									<span class="fl mr5">
										<?php echo lang('edm_top_selling_product');?>
									</span> 
								</label>  
								<span class="topsell-pr">			 						 
									<dl class="dropdown" id="top"> 
										<dt>
											<a href="#" class="selectProducts_1">
												<?php echo $SelectTop;?>
											</a>
										</dt>
										<dd>
											<div class="mutliSelect" name="top">
												<ul class="ulProductHtml_1">
													<?php echo $TopHtml; ?>
												</ul>
											</div>
										</dd>
									</dl>
								 </span>
							</div>
							<!-- maximum reward points input end here -->
						
							<!-- reward point expiry input start here -->
							<div class="form-box mt10 indispaly">
								<label>
									<!--
									<span class="fl mr5">
										<input  type="checkbox" onchange="cbchange(this)" data-show="bottomsell-pr" id="bottomSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_bottom)?"checked":""); ?>>
									</span>
									-->
									<span class="fl mr5">	
										<?php echo lang('edm_bottom_selling_product');?>
									</span>	
								</label>
								<span class="bottomsell-pr">
									<dl class="dropdown" id="bottom"> 
										<dt>
											<a href="#" class="selectProducts_2">
												<?php echo $SelectBottom; ?>
											</a>
										</dt>
										<dd>
											<div class="mutliSelect"  name="bottom">
												<ul class="ulProductHtml_2">
													<?php echo $BottomHtml; ?>
												</ul>
											</div>
										</dd>
									</dl>
								</span>
							</div>
							<!-- reward point expiry input end here -->
							
							<!-- Product price options start here -->
							<?php 
							$edm_sell_base_products_json = (!empty($edm_data['edm_sell_base_products_json'])) ? $edm_data['edm_sell_base_products_json'] : ''; ?>
							<div class="clearboth product-grid-form mb10">
								<table class="table table-bordered <?php echo (!empty($edm_sell_base_products_json)) ? '' : 'dn';?>" id="sellbased-product-table"> 
									<thead class="bg_ccc"> 
										<th><?php echo lang('product_id')?></th>
										<th><?php echo lang('product_name')?></th>
										<th><?php echo lang('product_price')?></th>
										<th><?php echo lang('special_price')?></th>                         
									</thead>
									<tbody id="email_sellbased_products" class="product_table_tbody">
										<?php
										$products = json_decode($edm_sell_base_products_json);
										if(!empty($products)) {
											foreach($products as $val) {
												echo '<tr id="'.$val->product_id.'">';
												echo '<td><input type="text" readonly name="product_id[]" value="'.$val->product_id.'" class="form-control readonly_input width100"></td>';
												echo '<td><input type="text" readonly name="product_name[]" value="'.$val->product_name.'" class="form-control readonly_input width100"></td>';
												echo '<td><input type="text" name="product_price[]" value="'.$val->product_price.'" id="product_price_'.$val->product_id.'" class="form-control number sell_product_input" row_id='.$val->product_id.'></td>';
												echo '<td><input type="text" name="special_price[]" value="'.$val->special_price.'" id="special_price_'.$val->product_id.'" class="form-control number sell_product_input" row_id='.$val->product_id.'></td>';
											}
										} ?>
									</tbody>	
								</table>
							</div>		
							<!-- Product price options end here -->
						</div>	
						<!-- Sell based product search box end here -->
						
						<!-- number of product show on email input start here -->
						<div class="form-box include-products <?php echo (!empty($product_add_type)) ? "" : "dn";?>">
							<label for="edm-other-quantity"><?php echo lang('no_of_product_in_email');?><i style="color:red;">*</i><span class="fa fa-info-circle ml5" title="<?php echo lang('no_of_product_in_email_help');?>"></span></label>
							<?php
							$data = array('name'=>'no_products','id'=>'no_products', 'value'=> $no_products ,
							'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
							echo form_input($data);
							?>
						</div>
						<!-- number of product show on email input end here -->
					</div>
					<!-- submit button here -->
					 <div class="form-box">
						 <?php
						$data = array('type'=>'hidden','name'=>'edm_products_json','id'=>'edm_products_json','value'=> !empty($edm_data['edm_products_json'])?$edm_data['edm_products_json']:'');
						echo form_input($data);
						$data = array('type'=>'hidden','name'=>'edm_sell_base_products_json','id'=>'edm_sell_base_products_json','value'=> !empty($edm_data['edm_sell_base_products_json'])?$edm_data['edm_sell_base_products_json']:'');
						echo form_input($data);?>
						<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
						<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
					 </div>
					<!-- submit button here -->
					<div class="clearboth"></div>
				</div>
				<!-- End general config panel -->	
				
				<!-- Start edm email tab panel -->
				<div class="tab-pane co_products_tab min-loader" id="edm_email_tab">
					<?php 
					$template_sugession_display = (!empty($edm_template)) ? 'display_none' : '';
					$hide_template = (!empty($edm_template)) ? '' : 'display_none';?>
					<!-- Template layout suggession box start here -->
					<div id="choose-template" class="text-center <?php echo $template_sugession_display ;?>">
						<button class="choose mr20" type="button" id="grid_view"><img src="<?php echo site_url().'uploads/edm_images/edm-grid-view.jpg'?>" class="img-responsive" alt=""><span>Grid Template</span></button>
						<button class="choose" type="button" id="list_view"><img src="<?php echo site_url().'uploads/edm_images/edm-list-view.jpg'?>" class="img-responsive" alt=""><span>List Template</span></button>
						<!-- submit button here -->
						<div class="form-box">
							<button type="button" name="cancel" class="btn btn-info back-btn fl" onclick="backnext()"><?php echo 'Back';?></button> 
						 </div>
						<!-- submit button here -->
					</div>
					<!-- Template layout suggession box end here -->
					
					<div class="email-template-wrapper clearboth email-template-box <?php echo $hide_template ;?>">
						<?php echo $this->load->view('edm_email_template_form'); ?>
						<!-- submit button here -->
						<div class="form-box">
							<?php
							$data = array('type'=>'hidden','name'=>'template_type','id'=>'template_type','value'=>(!empty($edm_data['template_type']))?$edm_data['template_type']:'');
							echo form_input($data);?>
							<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
							<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
						 </div>
						<!-- submit button here -->
					</div>
					
				</div>						
				<!-- End edm email tab panel -->
				
				<!-- Start frequency panel -->
				<div class="tab-pane" id="frequency_tab">
					<div class="">
						<!-- edm_occurance input start here -->
						<div class="form-box">
							<label for="co_end_date"><b><?php echo lang('edm_occurance_type');?></b><i style="color:red;">*</i></label>
							<div class="app">
								<div class="appointment">
									<div class="col-md-12">
										<div class="menu-Recurrence">
											<ul class = "freq-edm">
											  <li>	
												  <div class="valuedm">
											  <input class="edm_occurance" data-div="daily_div" type="radio" name="occurance_type"  id="daily" value="1" <?php echo ($occurance_type == 1 || $occurance_type == '')?"checked":""; ?> />			
											  <label for="recurring_type"><?php echo lang('edm_Daily');?></label>	
											  </div>
											  
											  </li>
											   <li>
												  <input class="edm_occurance" data-div="weekly_div" type="radio" name="occurance_type"  id="weekly" value="2" <?php echo ($occurance_type == 2)?"checked":""; ?> />											
												  <label for="recurring_type"><?php echo lang('edm_Weekly');?></label>
																											
											   </li>
											   <li>
												  <input class="edm_occurance" data-div="monthly_div" type="radio" name="occurance_type"  id="monthly" value="3" <?php echo ($occurance_type == 3)?"checked":""; ?> />
												  <label for="recurring_type"><?php echo lang('edm_Monthly');?></label>															
											   </li>
											   <li>
												  <input class="edm_occurance" data-div="yearly_div" type="radio" name="occurance_type"  id="yearly" value="4" <?php echo ($occurance_type == 4)?"checked":""; ?>/>
												  <label for="recurring_type"><?php echo lang('edm_Yearly');?></label>															
											   </li>
											</ul>
										</div>
										<?php 
										$daily = $weeks = $monthly = array();
										switch($occurance_type){
											case 1:
											$daily = $occurance_data;
											break;
											case 2:
											$weeks = $occurance_data;
											break;
											case 3:
											$monthly = $occurance_data;
											break;
											case 4:
											$yearly = $occurance_data;
											break;
										}														
										?>
										<!-- daily_div -->
										<div class="values-seen">
											<div class="col-md-9 occurance-div <?php echo ($occurance_type == 1 || $occurance_type == '') ? "" : "dn";?>" id="daily_div"  >
												<div class="pull-left">
													<h5>Promotional Messages will repeat on every day.</h5>
												</div>
											</div>
											<!-- weekly_div -->	
											<div class="col-md-10 col-xs-12 occurance-div  <?php echo ($occurance_type == 2 && !empty($weeks))?"":"dn"; ?>" id="weekly_div">
												<div class="pull-left">
													<h5>Promotional Messages will repeat every week on.</h5>
													<ul class="list-inline ul_week custcheck-box" id="recurring_day_ul" >
														<?php
														$dayCount = 1;			
														foreach($days as $daykey => $dayvalue){ ?>							
															<li>
																<input id="box<?php echo $dayCount; ?>" class="weekly_days" name="weekly_days[]" value="<?php echo $daykey; ?>" <?php echo (in_array($daykey,$weeks))?'checked':'';?> type="checkbox" />
																<label for="box<?php echo $dayCount;?>"><?php echo $dayvalue; ?></label>
															</li>
														<?php
															$dayCount++;
														}?> 	 
													</ul>
													<div class="alert other-product-help">
														<i class="icon fa fa-info"></i>
														<?php echo lang('week_occurance_help');?>			
													</div>
												</div>
											</div>
											<!-- monthly_div -->
											<div class="col-md-10 col-xs-12 occurance-div <?php echo ($occurance_type == 3 && !empty($monthly))?"":"dn"; ?>" id="monthly_div" >
												<h5>Promotional Messages will repeat every month of</h5>
												<ul class="list-inline ul_month custcheck-box" id="recurring_date_ul">
													 <?php 
													 for($i=1; $i<=31; $i++){ ?>
														<li>
															<input id="<?php echo $i;?>" class="monthly_days" name="monthly_dates[]" value="<?php echo $i;?>" <?php echo (in_array($i,$monthly))?'checked':''; ?> type="checkbox" />
															<label for="<?php echo $i;?>"><?php echo $i;?></label>
														</li>
													  <?php } ?>
												</ul>
												<div class="alert other-product-help">
													<i class="icon fa fa-info"></i>
													<?php echo lang('month_occurance_help');?>			
												</div>
											</div>
											<!-- yearly_div -->
											<div class="col-md-9 occurance-div <?php echo ($occurance_type == 4 && !empty($yearly))?"":"dn"; ?>" id="yearly_div" >
												<h5>Promotional Messages will repeat every year of.</h5>												
												<div class="input-group" style="margin:10px 0;">							
													<div class="half_date">
														<div class="form-group input-append date" id="yearly_time_div">
															<?php
															$data	= array('name'=>'yearly_time', 'data-format'=>'dd-MM-yyyy', 'value'=>isset($yearly)?$yearly:'','id'=>'yearly_time','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
															 echo form_input($data);?>
															<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
														</div>																   
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
										</div>
										<!-- appointment border -->
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>	
						<!-- edm_occurance input end here --> 
						
						<!-- edm send time input start here -->
						<div class="form-box">
							<label><b><?php echo lang('edm_Time');?></b><i style="color:red;">*</i></label>
							<div class="half_date mb10">
								<div class="form-group input-append date" id="start_time_div">
									<?php
									$data	= array('name'=>'send_time', 'data-format'=>'hh:mm:ss', 'value'=>set_value('start_time', (!empty($edm_data['send_time'])?$edm_data['send_time']:'')),'id'=>'start_time','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_time'), 'readonly'=>'readonly', 'class'=>'required');
									echo form_input($data);
									?>
									<span class="add-on" id="start_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-time"> </i> </span>
								</div>
							</div>
						</div>
						<!-- edm send time input end here --> 
						
						<!-- read email block input start here -->
						<div class="form-box">
							<label><b><?php echo lang('is_always_send_to_all_users');?></b><span class="fa fa-info-circle ml5" title="<?php echo lang('is_always_send_to_all_users_help');?>"></span></label>
							<div class="mb10">
								<input  type="checkbox" class="rec3" name="is_always_send_to_all_users" data-type='bottom' value="1" <?php echo (!empty($is_always_send_to_all_users)) ? "checked" : "";?>>
							</div>
						</div>
						<!-- read email block input end here -->
							
						<!-- submit button here -->
						<div class="form-box">
							<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
							<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
						</div>
						<!-- submit button here -->	 
					</div>
					<div class="clearboth"></div>
				</div>
				<!-- End frequency panel -->
				
				<!-- Start referral system panel -->
				<div class="tab-pane co_products_tab form-wrapper" id="receiver_tab">
					<div class="">
						<!-- invite points input start here -->
						<div class="form-box edm-receiver radio-homerec ">
							<input  type="radio" class="rec2 receiver_type" name="rec_type[]" data-type='bottom' onchange="cdradio(this)"  data-show="rec-home" value="2"  <?php echo (empty($edm_id))?"checked":((in_array(2,$rec_type))?"checked":"");?> >
							<label for="enable_referral" >
							 <?php echo 'All User';?>
							</label>
						</div>
						<!-- invite points input end here -->	
						
						<!-- referral status dropdown start here -->
						<div class="form-box edm-receiver radio-homerec">
							<div class="p-relative">
								<input  type="radio" class="rec1 receiver_type" name="rec_type[]" data-type='bottom' onchange="cdradio(this)" data-show="rec-home" value="1" <?php echo (in_array(1,$rec_type))?"checked":"";?> >
								<label for="enable_referral"><?php echo 'Specific Home Store';?></label>
								<span class="rec-home  <?php echo (!empty($rec_home_store_id) && (in_array(1,$rec_type)))?"":"dn"; ?>">
										<dl class="dropdown" id="store"> 
											<dt>
											<a href="#">
											   <?php echo $SelectStore; ?>      
											  <p class="multiSel"><?php echo $StoreName; ?></p>  
											</a>
											</dt>
											<dd>
												<div class="mutliSelect"  name="store">
													<ul>
														<?php echo $StoreHtml; ?>
													</ul>
												</div>
											</dd>
										</dl>
								 </span>
							</div>	
						</div>
						<!-- referral status dropdown end here -->
						
						<!-- Manual email input start here -->
						<div class="form-box edm-receiver radio-homerec">
							<input  type="radio" class="rec2 receiver_type" name="rec_type[]" data-type='bottom' onchange="cdradio(this)"  data-show="rec-home" value="5" <?php echo (empty($edm_id))?"checked":((in_array(5,$rec_type))?"checked":"");?> >
							<label for="enable_referral" >
								<?php echo 'Manual Email(s)';?>
							</label>
							<div class="manual_import_email <?php echo (in_array(5,$rec_type)) ? "":"dn"; ?>">
								<div class="fleft clear">
									<input type="file" class="csv_input" id="csvfile" />  
								</div>
								<div class="fleft clear">
									<button type="button" class="btn btn-primary uploadcsv">Import Email CSV</button>
								</div>	
							</div>
						</div>
						<div class="form-box manual_import_email <?php echo (in_array(5,$rec_type)) ? "":"dn"; ?>" id="csvemails">
							<?php
							// show brand products
							$email_manuals = (!empty($edm_data['manual_emails'])?$edm_data['manual_emails']:'');
							$emails = array_filter(explode(',',$email_manuals));
							foreach($emails as $email) { ?>
								<span class="chosen-choices fl ml5">&nbsp;
									<span>
										<?php echo $email;?>
									</span>&nbsp;&nbsp;
									<span class="search-choice-close crp" email_id="<?php echo $email;?>" >
										<strong> X </strong>
									</span>&nbsp;
								</span>
							<?php }?>
						</div>
						<?php
						$data = array('type'=>'hidden','name'=>'manual_emails','id'=>'manual_emails','value'=>$email_manuals);
						echo form_input($data);?>
						<!-- Manual email input end here -->
						
						<!-- invite points limit input start here -->
						<div class="form-box edm-receiver previosly_purchased non_manual_email <?php echo ($product_add_type == 0 || (in_array(5,$rec_type))) ? 'dn' : '';?>">
							<input  type="checkbox" class="rec3" name="rec_type[]" data-type='bottom' value="3" <?php echo (in_array(3,$rec_type))?"checked":"";?>>
							<label for="enable_referral" ><?php echo 'Previously Purchased';?></label>
						</div>
						<!-- invite points limit input end here -->
						
						<!-- invite points limit input start here -->
						<div class="form-box edm-receiver non_manual_email <?php echo (in_array(5,$rec_type)) ? 'dn' : '';?>">
							<input  class="main-rec" name="rec_type[]" onchange="cbchange(this)" data-show="sub-rec" type="checkbox" class="rec4" data-show="" value="4" <?php echo (in_array(4,$rec_type))?"checked":"";?>>
							<label for="enable_referral"><?php echo 'Reward Card Type';?></label>
							<span class="sub-rec  <?php echo (!empty($rec_reward_card_type))?"":"dn"; ?> ">
								<ul class="rewardcard-value" >
								   <?php 
								   if(!empty($rewardCard_data)){
								   foreach($rewardCard_data as $cards){
								   ?>
								   <li><input  name ="rec_rc[]" type="checkbox" class="rec4_<?php echo $cards->id;?>" data-type='bottom' <?php echo (in_array($cards->id,$rec_reward_card_type))?'checked':''; ?> data-show="" value="<?php echo $cards->id; ?>" > <?php echo ($cards->card_name)?$cards->card_name:''; ?></li>
								   <?php 
									} 
								}?>
								</ul> 
							</span>
						</div>
						<!-- invite points limit input end here -->
					</div>
						<!-- submit button here -->
						<div class="form-box" style="padding:10px 0;">
							<button type="button" onclick="backnext()" name="cancel" class="btn btn-info back-btn"><?php echo 'Back';?></button> 
							<button type="submit" name="submit"   class="btn btn-primary"><?php echo 'Submit';?></button>
					 </div>
						<!-- submit button here -->	
					<div class="clearboth"></div>
				</div>
				<!-- End referral system panel -->
				
				<div class="clearboth"></div>
			</div>	
			
		<?php echo form_close(); ?>
	</div>
</div>	
	
<!-- start the bootstrap modal where the other product appear -->
	<div class="modal fade dn" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Product Search</h4>
				</div>
				<div class="modal-body">
                  <div class="tab-pane" id="product_tab">
                    <button onclick="product_search()" class="btn default add-product-btn fr"  type="button product_direct_search_button" style="margin-top:0px">Product Search</button>		
                    <input type="text" placeholder="Product Id / Product Name" name="product_direct_search" id="product_direct_search" style="width:200px;margin-right:10px" class="fr" >
                    <select id="productType" name="productType" style="width:100px;margin-right:10px" class="fr">
                        <option value="id">Number</option>
                        <option value="name">Name </option>
                    </select>
                        <div class="clearboth"></div>
                        <div id="error_message" style="font-size:11px;color:red"></div> 
                   
                            <!-- Product list / form  start here -->
                            <div class="clearboth product-grid-form" id="appendProductTable">
                                <table class="table table-bordered" id="product_tables"> 
                                    <thead class="bg_ccc">
                                        <tr>
                                            <th class="width:20px"><input type="checkbox" name="all_product_check" value="1" id="all_product_check"  ></th>
                                            <th class='product_table_th'><?php echo lang('product_number');?></th>
                                            <th class='product_table_th'><?php echo lang('product_name');?></th>
                                            <th class='width45'></th>
                                        </tr>
                                    </thead>
                                  
                                </table>
                                <!-- Product fields end here -->
                            </div>
                        
                </div> 
				</div>
				<div class="modal-footer">
                    <button onclick="add_into_filter()" class="btn default add-on-filter"  type="button">Add Into Filter</button>		
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end the bootstrap modal where other product will appear -->
<script>
	var productIDArray = [];
	var productNAMEArray = [];
	var productDepartments = [];
	var product_ids = '<?php echo $product_ids; ?>';
	var product_names = '<?php echo $product_names; ?>';
	if(product_ids != '') {
		productIDArray = JSON.parse(product_ids);
	}
	if(product_names != '') {
		productNAMEArray = JSON.parse(product_names);
	}
	var EmailProducts = {};
   
	$(document).ready(function() {
		if(jQuery('#start_date_div1').length > 0)  { 
           jQuery('#start_date_div1').datetimepicker({
               pickTime: false,
               startDate: new Date(), 
           });
       }
   
       if(jQuery('#end_date_div1').length > 0)  { 
           jQuery('#end_date_div1').datetimepicker({
               pickTime: false,
               startDate: new Date(), 
           });
       }
       
       if(jQuery('#yearly_time_div').length > 0)  { 
           jQuery('#yearly_time_div').datetimepicker({
               pickTime: false,
               startDate: new Date(), 
           });
       }
   });
	
	function show_loader(action,className){
		var div = "<div class='loader loaderdiv'><i class='fa fa-refresh fa-spin' style='font-size: 28px;color: #111;position: absolute;left: 50%;bottom: 50%;transform: translate(-50%,-50%);'></i></div>";
		if(action){
			$('.'+className).css({"position": "relative","padding": "0px"});
			$('.'+className).prepend(div);
		}else{
			$('.'+className).css({"position":"","padding": ""});
			$('.loaderdiv').remove();
		}
	}	
	
	$(document).delegate(".otherPr", "click", function(e){
		$('#productmodal').modal('show');
	});
	
	/* Hide and Show manual product box*/     
    $(document).on('click','#is_product_group',function(){
        if($(this).is(':checked')){
		$('#productgropuShowHide').show();
        }else{
			$('#productgropuShowHide').hide();
			$('#product_tab_link').hide(); // Hide Product Groups
			$('#filter_tab_link').show(); // Hide Product Groups
        } 
    });
	
   /* 
	| -----------------------------------------------------
	| Manage add quantity row 
	| -----------------------------------------------------
	*/ 
	function product_search() {
		// get last product row number
		var product_direct_search = $('#product_direct_search').val();
		var productType = $('#productType').val();
        var exclude_cat = $('#exclude_cat').val();
        if(product_direct_search == ""){
			//bootbox.alert("Empty search is not valid");
			$('#error_message').show();
			$('#error_message').html("Empty search is not valid");
			$('#error_message').hide(4000);
			$('#product_direct_search').val('');
			return false;
        }
        
        if(productType == 'id'){
            if(isNaN(product_direct_search)){
                //bootbox.alert("Invalid product number");
                $('#error_message').show();
                $('#error_message').html("Invalid product number");
                $('#error_message').hide(4000);
                $('#product_direct_search').val('');
                return false;
            }else{
               //alert('Input OK');
            }
        }
        $('#loader1').fadeIn();
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {product_direct_search:product_direct_search,productType:productType,department_id:exclude_cat},
			url  : BASEURL+"admin/settings/targetpush/search_product_direct",
			success: function(data){
				$('#appendProductTable').html(data.html);
				$('#loader1').fadeOut();
			}
		});
	}
	
   /* 
	| -----------------------------------------------------
	| Manage add filter product row 
	| -----------------------------------------------------
	*/ 
	
	function add_into_filter() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
		var row;
		var product_id;
		var product_ids_val = '';
		var product_name;
		var product_qty;
		var html = '';
		var other_products = [];
		var j = productIDArray.length;
		$('#loader1').fadeIn();
		$( ".single_product_check:checkbox:checked" ).each(function( index ) {
			row = parseInt($(this).attr('qty_row'));
			product_name = $(this).attr('product_name_'+row);
			product_qty = $(this).attr('product_qty_'+row);
			product_department = $(this).attr('prod_department_'+row);
			product_id = parseInt($(this).val());
			if(jQuery.inArray(product_id,productIDArray) < 0){
				productIDArray[j] = product_id;
				productNAMEArray[product_id] = product_name;
				productDepartments[product_id] = product_department;
				j++;
			}
		});
		
		$('#email_products').html('');
		if(productIDArray.length > 0) {
			for(var i=0;i<productIDArray.length;i++){
				product_ids_val += productIDArray[i]+',';
				var product_tr = '';
				product_tr += '<tr id="'+productIDArray[i]+'">';
				product_tr += '<td><input type="text" readonly name="product_id[]" value="'+productIDArray[i]+'" class="form-control readonly_input width100"></td>';
				product_tr += '<td><input type="text" readonly name="product_name[]" value="'+productNAMEArray[productIDArray[i]]+'" class="form-control readonly_input width100"></td>';
				product_tr += '<td><input type="text" name="product_price[]" id="product_price_'+productIDArray[i]+'" class="form-control required product_input" row_id="'+productIDArray[i]+'"></td>';
				product_tr += '<td><input type="text" name="special_price[]" id="special_price_'+productIDArray[i]+'" class="form-control required product_input" row_id="'+productIDArray[i]+'"></td>';
				product_tr += '<td><a class="common-btn remove-btn" row_id='+productIDArray[i]+'><i class="fa fa-trash"></i></a></td>';
				$('#product-table').append(product_tr);
				var ProductArray = {};
				ProductArray['product_id'] = productIDArray[i];
				ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
				ProductArray['product_department'] = productDepartments[productIDArray[i]];
				ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
				ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
				other_products.push(ProductArray);
			}
		}
		
		if(productIDArray != ""){
			$('#product_other').val(product_ids_val);
		}
		var edm_products_json = '';
		if(other_products.length > 0){
			$('#productmodal').modal('hide');
			// convert products array to json
			var edm_products_json = JSON.stringify(other_products);
			$('#product-table').show();
		}
		$('#edm_products_json').val(edm_products_json);
		$('#loader1').fadeOut();
   }
 
   /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".search-choice-close","click",function(e){
		$(this).parent('span').remove();
		var prod_id = $(this).attr('prod_id');
		var product_ids = $('#product_other').val();
		if(prod_id != '') {
			product_ids = product_ids.replace(prod_id+',','');
			$('#product_other').val(product_ids);
		}
    });
    
   	$( "#daily").click(function() {
		$('#daily_div').css("display","block");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","none");  
	});

	$("#weekly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","block");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","none");
	});

	$("#monthly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","block");
		$('#yearly_div').css("display","none");
	});

	$("#yearly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","block");
	});
	
	function cdradio(res){
		var showdiv = $(res).attr('data-show');
		var res_val = parseInt($(res).val());
		if(showdiv!= ''){
			if(res.checked){
				if(res_val === 1){
					$('.'+showdiv).show();
					$('.manual_import_email').hide();
					$('.non_manual_email').show();
				}else if(res_val === 5) {
					$('.'+showdiv).hide();
					$('.manual_import_email').show();
					$('.non_manual_email').hide();
				}else{
					$('.'+showdiv).hide();
					$('.manual_import_email').hide();
					$('.non_manual_email').show();
				}
			}
		}
	}
	
	function cbchange(res){
		var showdiv = $(res).attr('data-show');
		if(showdiv!= ''){
			if(res.checked){
				$('.'+showdiv).fadeIn('slow');	
			}else{
				$('.'+showdiv).fadeOut('slow');	
			}
		}
	}
	
	// Manage product pick box availability
	$("input[name='product_add_type']").change(function(){
		var product_add_type = $(this).val();
		if(product_add_type == 1) {
			$('#product_direct_searchbox').show();
			$('.include-products').show();
			$('#sellbased_product_searchbox').hide();
			$('.previosly_purchased').show();
		} else if (product_add_type == 2){
			
			//~ var no_of_top_products =  $('#no_of_top_products').val();
			//~ var no_of_bottom_products =  $('#no_of_bottom_products').val();
			//~ var existing_top_products = '';
			//~ var existing_bottom_products = '';
			//~ 
			//~ <?php if(!empty($product_top_val)) { ?>
			//~ existing_top_products = JSON.parse('<?php echo $product_top_val;?>');
			//~ <?php } ?>
		    //~ 
			//~ <?php if(!empty($product_bottom_val)) { ?>
				//~ existing_bottom_products = JSON.parse('<?php echo $product_bottom_val;?>');
			//~ <?php } ?>
			//~ 
			//~ if(no_of_top_products !== 0 || no_of_top_products !== ''){
				//~ manage_selling_product(no_of_top_products,1,existing_top_products);
			//~ }
			//~ if(no_of_bottom_products !== 0 || no_of_bottom_products !== ''){
				//~ manage_selling_product(no_of_bottom_products,2,existing_bottom_products);
			//~ }
			// manage product box visibility
			$('#sellbased_product_searchbox').show();
			$('.include-products').show();
			$('#product_direct_searchbox').hide();
			$('.previosly_purchased').show();
		} else {
			$('#product_direct_searchbox').hide();
			$('.include-products').hide();
			$('#sellbased_product_searchbox').hide();
			$('.previosly_purchased').hide();
		}
	});
	
	$(document).ready(function(){
		$("#frm_edm").validate({
			rules: {
				"rec_type[]": { 
						required: true, 
						minlength: 1 
				},
				"rec_rc[]": { 
						required: true, 
						minlength: 1 
				},
				no_products: { 
					min: 1
				} 
			}, 
			messages: { 
					"rec_type[]": "Please select at least one types of receiver.",
					"rec_rc[]": "Please select at least one reward card type."
			},
			submitHandler: function() {
				var activeTab = $("ul.nav-tabs").find(".active").attr('data-tab');
				switch(activeTab) {
					case 'receiver_tab':
					    // Validate home store selection in case of receiver type selection
						var rec_type = $('input.receiver_type:checked').val();
						if(rec_type == 1) {
							var home_stores = $('input.homeStores:checked').length;
							if(home_stores  == 0) {
								bootbox.alert("Please Select Home Store(s)!");
								return false;
								break;
							}
						} else if(rec_type == 5) {
							var manual_emails = $('#manual_emails').val();
							if(manual_emails  == '' || manual_emails  == ',') {
								bootbox.alert("Please Upload Valid Email CSV!");
								return false;
								break;
							}
						}
						// Manage store procedure
						savedata();
					break;
					case 'product_tab':
						var product_required = false;
						var product_search_type = $("input[name='product_add_type']:checked").val();
						if(product_search_type != 0) {
							var product_input_cls = 'sell_product_input';
							if(product_search_type == 1) {
								var product_other = $('#product_other').val();
								if(product_other == '') {
									product_required = true;
								}
								product_input_cls = 'product_input';
							} else {
								var topSellingProducts = $('input.topSellingProducts:checked').length;
								var bottomSellingProducts = $('input.bottomSellingProducts:checked').length;
								product_required = true;
								if( topSellingProducts > 0 || bottomSellingProducts  > 0) {
									product_required = false;
								}
							}
							// check product price validation
							var is_validate = true;
							$( "."+product_input_cls ).each(function( e ) {
								var row_id = $(this).attr('row_id');
								var product_price = $('#product_price_'+row_id).val();
								var special_price = $('#special_price_'+row_id).val();
								
								if(product_price == '' || isNaN(product_price) ) {
									$('#product_price_'+row_id).addClass('error');
									is_validate =false;
								}
								if(special_price == '' || isNaN(special_price)) {
									$('#special_price_'+row_id).addClass('error');
									is_validate = false;
								}
							});
							if(is_validate == false) {
								bootbox.alert("Please fill all required attributes.");
								return false;
							} else {
								if(product_search_type ==2){
									manage_product_json();
								} else {
									manage_other_product_json();
								}
							}	
						}
						// prapare template body 
						var template_type = $('#template_type').val();
						if(template_type != '') {
							prepare_email_template();
						}
						if(product_required) {
							bootbox.alert("Please select product(s) first!");
							return false;
							break;
						}
					case 'info_tab':
						// set end date for calculate day diffrence
						var end_time = $( "#end_date" ).val().split(' ');
						if(end_time != '') {
							var split_end_date = end_time[0].split("-");
							var new_end_date   = split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0];
							// set start date for calculate day diffrence
							var start_date = $( "#start_date" ).val().split(' ');
							var split_date 		= start_date[0].split("-");
							var new_start_date	= split_date[2]+'/'+split_date[1]+'/'+split_date[0];	
							
							// init in dates formate
							var sDate =  new Date(new_start_date);
							var eDate =  new Date(new_end_date);
							
							// show error if start date less then end date
							if(start_date != '' && sDate >= eDate ) {
								bootbox.alert("End date must be greater than start date");
								$( "#end_date" ).val('');
								return false;
								break;
							} 
						}
						var existing_top_products = '';
						var existing_bottom_products = '';
						var no_of_top_products = $('#no_of_top_products').val();
						var no_of_bottom_products = $('#no_of_bottom_products').val();
						<?php if(!empty($product_top_val)) { ?>
							existing_top_products = JSON.parse('<?php echo $product_top_val;?>');
						<?php } 
						if(!empty($product_bottom_val)) { ?>
							existing_bottom_products = JSON.parse('<?php echo $product_bottom_val;?>');
						<?php } ?>
						manage_selling_product(no_of_top_products,1,existing_top_products);
						manage_selling_product(no_of_bottom_products,2,existing_bottom_products);
						// show product multiselect dropdown values
						$('.ulProductHtml_1').show();
						$('.ulProductHtml_2').show();
					case 'frequency_tab': 
						var occurance_type = $("input[name='occurance_type']:checked").val();
						occurance_selected_days = 1;
						if(occurance_type == 2) {
							occurance_selected_days = $('input.weekly_days:checked').length;
						} else if(occurance_type == 3) {
							occurance_selected_days = $('input.monthly_days:checked').length;
						}
						if(occurance_selected_days  == 0) {
							bootbox.alert("Please select occurance day(s)!");
							return false;
							break;
						}
					default:
						backnext(true);
				}
			}
		});
	});	
	
	// Manage form store procedure
	function savedata(){
		var formData = new FormData($('#frm_edm')[0]);
		$('#loader1').fadeIn();
		$.ajax({
			url: BASEURL+"admin/settings/edm/edm_configration",
			type: 'POST',
			data: formData,
			async: false,
			success: function (data) {
				$('#loader1').fadeOut();
				window.location.href = BASEURL+"admin/settings/edm";
			},
			cache: false,
			contentType: false,
			processData: false
		}, "json");
		return false;
	}
	
	function createEdmTemplate(){
		backnext(true);
		var fromData=$("#frm_edm").serialize();	
		$.post(BASEURL+"admin/settings/edm/edm_template_config",fromData, function(data){
			if(data.result){
				//$('#body').val(data.view);
				CKEDITOR.instances['body'].setData(data.view);
				show_loader(0,'min-loader');
			}
		}, "json");
	}

	function backnext(type){
		var activeTab = $("ul.nav-tabs").find(".active");
		//remove from current
		$(activeTab).removeClass("active");
		$("#"+$(activeTab).attr('data-tab')).removeClass("active");
		//add next 
		if(type){
			$(activeTab).next().addClass('active');
			$("#"+$(activeTab).next().attr('data-tab')).addClass('active');	
		}else{
		//add next 
			$(activeTab).prev().addClass('active');
			$("#"+$(activeTab).prev().attr('data-tab')).addClass('active');
		}
	}

	$(".dropdown dt a").on('click', function() {
		//console.log($(this).parent().next().find('.mutliSelect')..slideToggle('fast'));
		$(this).parent().next().find('.mutliSelect ul').slideToggle('fast');
		//$(".dropdown dd ul").slideToggle('fast');
		//$(this).parent().next("dd ul").slideToggle('fast');
	});
	
	$(".dropdown dd ul li a").on('click', function() {
		$(this).parent().next().find('.mutliSelect ul').hide();
		//$(".dropdown dd ul").hide();
	});

	function getSelectedValue(id) {
		return $("#" + id).find("dt a span.value").html();
	}

	$(document).bind('click', function(e) {
		//var $clicked = $(e.target);
		//if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
	});

	$('.mutliSelect input[type="checkbox"]').on('click', function() {
		var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').attr('data-name'),
		title = $(this).attr('data-name') + ",";
		if ($(this).is(':checked')) {
			var html = '<span title="' + title + '">' + title + '</span>';
			// $(this).parent().find('.multiSel').append(html);
			//$('.multiSel').append(html);
			$(this).closest('.dropdown').find('.multiSel').append(html);
			$(".hida").hide();
		} else {
			$('span[title="' + title + '"]').remove();
			var ret = $(".hida");
			$(this).closest('.dropdown').find('dt a').append(ret);
			//$('.dropdown dt a').append(ret);
		}
	});
	
	// fetch top or bottom products on the basis of excluded categories
	$("#exclude_cat").on("blur", function(){
		var product_search_type = $("input[name='product_add_type']:checked").val();
		if(product_search_type == 1) {
			var exclude_cat = $('#exclude_cat').val();
			var department_ids = exclude_cat.split(',');
			var product_json = $('#edm_products_json').val();
			if(product_json != '' && department_ids.length > 0) {
				var products = $.parseJSON(product_json);
				var product_count = products.length;
				for(var i=0;i<product_count;i++) {
					var product_department = products[i]['product_department'];
					if(jQuery.inArray(product_department, department_ids) !== -1) {
						var row_id = products[i]['product_id'];
						var filtered_other_products = new Array();
						var product_ids_val = '';
						if(productIDArray.length > 0) {
							for(var i=0;i<productIDArray.length;i++){
								if(row_id != productIDArray[i]) {
									product_ids_val += productIDArray[i]+',';
									var ProductArray = {};
									ProductArray['product_id'] = productIDArray[i];
									ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
									ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
									ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
									filtered_other_products.push(ProductArray);
								}
							}
						}
						$('#product_other').val(product_ids_val);
						// convert products array to json
						var edm_products_json = JSON.stringify(filtered_other_products);
						$('#edm_products_json').val(edm_products_json);
						$('table#product-table tr#'+row_id).remove();
					}
				}	
			}
		}else if(product_search_type == 2) {
			var existing_top_products = '';
			var existing_bottom_products = '';
			var no_of_top_products = $('#no_of_top_products').val();
			var no_of_bottom_products = $('#no_of_bottom_products').val();
			<?php if(!empty($product_top_val)) { ?>
				existing_top_products = JSON.parse('<?php echo $product_top_val;?>');
			<?php } 
			if(!empty($product_bottom_val)) { ?>
				existing_bottom_products = JSON.parse('<?php echo $product_bottom_val;?>');
			<?php } ?>
			manage_selling_product(no_of_top_products,1,existing_top_products);
			manage_selling_product(no_of_bottom_products,2,existing_bottom_products);
		}
	});
	
	// display top or bottom selling products in dropdown 
	$(".product_selling_fetch_limit").on("blur", function(){
		var product_limit =  $(this).val();
		var product_selling_type = $(this).attr('product_selling_type');
		var field_id = $(this).attr('id');
		var prev_limit = $('#'+field_id+'_hidden').val();
		if(prev_limit != product_limit) {
			$('#'+field_id+'_hidden').val(product_limit);
			var existing_products = '';
			if(product_selling_type == 1) {
				<?php if(!empty($product_top_val)) { ?>
					existing_products = JSON.parse('<?php echo $product_top_val;?>');
				<?php } ?>
			} else {
				<?php if(!empty($product_bottom_val)) { ?>
					existing_products = JSON.parse('<?php echo $product_bottom_val;?>');
				<?php } ?>
			}
			if(product_limit !== 0 || product_limit !== ''){
				manage_selling_product(product_limit,product_selling_type,existing_products);
			}
		}
	});
	
	// manage dynamic product dropdown appearance
	function manage_selling_product(product_limit,selling_product_type,existing_products) {
		var exclude_cat = $('#exclude_cat').val();
		var department_id = '';
		if(exclude_cat != '' && exclude_cat != ',' ) {
			department_id = exclude_cat;
		} 
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: '<?php echo site_url("admin/settings/edm/get_products_selling_dropdown"); ?>',
			data: {product_limit:product_limit, selling_product_type:selling_product_type, existing_products : existing_products, department_id : department_id},
			beforeSend: function() {
				$('#loader1').fadeIn();	
			},
			success: function(data){
				if(data.result){
					$('.selectProducts_'+selling_product_type).html(data.selectedProducts);
					$('.ulProductHtml_'+selling_product_type).html(data.ulProductHtml);
					if(data.no_more_existing_products != '') {
						var non_existing_products = JSON.parse(data.no_more_existing_products);
						if(non_existing_products.length > 0) {
							for(var i=0;i<non_existing_products.length;i++){
								$('table#sellbased-product-table tr#'+non_existing_products[i]).remove();
							}
						}
					}
					// show product multiselect dropdown values
					$('.ulProductHtml_1').show();
					$('.ulProductHtml_2').show();
				}
			},
			complete:function(){
				$('#loader1').fadeOut();	
			}
		});
	}
	
	$("#banner_image_picker").click(function() {
		$("input[name='email_banner_image']").click();
	});

	function read_url_banner_image(input) {
		
		var pickerId= '#'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		//$('#banner_image_val').val(input.value);
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 1.0955135) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 1MB!");
			}
		}
	}
	
	// Initialize description mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "edmmceEditor",
		height: '220px',
		width: '450px',
		theme_advanced_resizing: true,
		plugins: ['link'],
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				if(edm_description != '' && edm_description != undefined) {
					$('.edm_description_tr').show();
					$('.edm_description').html(edm_description);
				}
			});
		}
	});
	
	// Initialize footer content mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "footermceEditor",
		height: '220px',
		width: '450px',
		theme_advanced_resizing: true,
		plugins: ['link'],
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				if(edm_description != '' && edm_description != undefined) {
					$('.email_footer_text_tr').show();
					$('.email_footer_text').html(edm_description);
				}
				
			});
		}
	});
	
	// Prepare products box inside email template
	function manage_product_grid() {
		var product_search_type = $("input[name='product_add_type']:checked").val();
		var no_products   = $('#no_products').val();
		var product_ids	  = new Array();
		var product_names = new Array();
		var product_json = '';
		var product_html = '';
		if(product_search_type == 1) {
			product_json = $('#edm_products_json').val();
		} else if(product_search_type == 2) {
			product_json = $('#edm_sell_base_products_json').val();
		}
		
		if(product_json != '') {
			var products = $.parseJSON(product_json);
			var product_count = products.length;
			if(no_products <= product_count){
				product_count = no_products;
			}
			product_html = set_template_html(product_count,products);
		}
		
		if(product_html != '') {
			$('.product_html_tr').show();
			$('.products').show();
			$('.products_grid').html(product_html);
		}	
	}
	
	// Prepare html templates base on type
	function set_template_html(product_count,products) {
		var template_type = $('#template_type').val();
		var product_html ='';
		if(template_type == 'grid_view') {
			var counter = 1;
			var total_product_count = product_count;
			for(var i=0;i<product_count;i++) {
				var product_price = $('#product_price_'+products[i]['product_id']).val();
				var special_price = $('#special_price_'+products[i]['product_id']).val();
				if(counter == 1) {
					product_html += '<tr><td><table border="0" width="100%" cellpadding="0" cellspacing="0" class="style" style="text-align: center;"><tr>';
				}
				product_html += '<td class="border-bottom" style="width:32%;background-color: #fefefe !important;border: 1px solid #ccc;border-radius: 4px;padding: 0px -1px 10px 12px;vertical-align: top;text-align: left;"><p style="text-align: center;"> <img width="160" height="71" src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"></p>';
				product_html += '<h4 style="text-align: center;"> <strong>'+products[i]['product_name']+'</strong> </h4><div style="text-align: center;" class="price-grid">';
				product_html += '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="50%" style="text-align: right;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;">List Price</td><td width="50%" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;font-family: "Courier New", Courier, monospace;text-align: left;font-weight: 600;padding-left: 4%;letter-spacing: 0px;">$'+product_price+'</td></tr>';
				product_html += '<tr><td width="50%" style="text-align: right;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;" class="text-right">Price</td><td width="50%" class="price-new">$'+special_price+'</td></tr></table></div>';
				product_html += '<div class="clearfix"></div><div class="explore"><a href="{VIEW_PRODUCT}'+products[i]['product_id']+'">Explore More</a> </div></td>';
				if(counter == 1 || counter == 2) {
					product_html += '<td style="width:10px;">&nbsp;</td>';
				}
				
				// setup additional coloumn in case of less then 3 TD
				if(total_product_count == 1 && counter < 3) {
					var remain_td = 1;
					if(counter == 1) {
						remain_td = 2;
					}
					for(var k=0;k<remain_td;k++) {
						product_html += '<td class="border-bottom" style="width:32%;">&nbsp;</td>';
						product_html += '<td style="width:10px;">&nbsp;</td>';
					}
					product_html += '</tr></table></td></tr>';
					product_html += '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
				}
				if(counter == 3) {
					product_html += '</tr></table></td></tr>';
					product_html += '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
					counter = 1;
				} else {
					counter++;
				}
				total_product_count--;
			}
		} else {
			for(var i=0;i<product_count;i++) {
				var product_price = $('#product_price_'+products[i]['product_id']).val();
				var special_price = $('#special_price_'+products[i]['product_id']).val();
				product_html += '<tr><td><table class="style list" width="100%" style="text-align: center;"><tr>';
				product_html += '<td width="20%" class="vt"><img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"></td>';
				product_html += '<td width="60%" style="vertical-align: top;"><h4 style="font-size: 14px;line-height: 15px!important; margin: 2px 0px 0px 0px;padding-left: 5%;color: #54667a;font-weight: 300;float: left;"> <strong>'+products[i]['product_name']+'</strong> </h4>';
				product_html += '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="40%" style="text-align: left;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">List Price</td><td width="" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;text-align: left;font-weight: 600;padding-left: 0;letter-spacing: 0px;">$'+product_price+'</td></tr>';
				product_html += '<tr><td width="40%" style="text-align: left;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">Price</td><td width="" class="price-new2">$'+special_price+'</td></tr></table></td>';
				product_html += '<td width="20%" style="vertical-align: middle;"><div class="explore"> <a href="{VIEW_PRODUCT}'+products[i]['product_id']+'">Explore More</a> </div></td></tr></table></td></tr><tr><td height="20" style="font-size:20px; line-height:20px;"></td></tr>';
			}
		}
		return product_html;
	}
	
	// Manage template selection
	$(document).delegate(".choose","click",function(e){
		var id = $(this).attr('id');
		$('#'+id+'_template').removeClass('dn');
		$('#choose-template').addClass('display_none');
		$('.email-template-box').removeClass('display_none');
		$('#template_type').val(id);
		prepare_email_template();
	});
	
	// Prapare
	function prepare_email_template() {
		// prepare email template body
		var banner_img_val = $('#banner_image_val').val();
		if(banner_img_val != '') {
			var banner_img = $('#banner_image_picker').attr('src');
			$('.banner-img').show();
			var banner_image = '<img id="banner-image" src="'+banner_img+'" border="0" style="width:525px;">'
			$('.banner-img').html(banner_image);
		}
		// set product widget title
		var product_grid_title =  $("#product_grid_title").val();
		if(product_grid_title != undefined) {
			$('.product_widget_title_tr').show();
			$('.product_widget_title').html('<h4>'+product_grid_title+'</h4>');
		}
		var product_search_type = $("input[name='product_add_type']:checked").val();
		if(product_search_type != 0) {
			// prepare product grid in email template
			manage_product_grid();
		} else {
			$('.product_html_tr').hide();
			$('.products').hide();
			$('.products_grid').html('');
			$('#edm_products_json').val('');
			$('#edm_sell_base_products_json').val('');
			$('.product_widget_title_tr').hide();
		}
	}
	
	$(document).on('change','.topSellingProducts',function(){
		var product_id = $(this).val();
		var product_name = $(this).attr('data-name');
		var product_tr = '';
		if(this.checked) {
			product_tr += '<tr id="'+product_id+'">';
			product_tr += '<td><input type="text" readonly name="product_id[]" value="'+product_id+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" readonly name="product_name[]" value="'+product_name+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" name="product_price[]" id="product_price_'+product_id+'" class="form-control required number sell_product_input" row_id="'+product_id+'"></td>';
			product_tr += '<td><input type="text" name="special_price[]" id="special_price_'+product_id+'" class="form-control required number sell_product_input" row_id="'+product_id+'"></td>';
			$('#sellbased-product-table').append(product_tr);
		} else {
			$('table#sellbased-product-table tr#'+product_id).remove();
		}
		var checked_top_products = $('input.topSellingProducts:checked').length;
		var checked_bottom_products = $('input.bottomSellingProducts:checked').length;
		if(checked_top_products > 0 || checked_bottom_products > 0) {
			$('#sellbased-product-table').show();
		} else {
			$('#sellbased-product-table').hide();
		}
	});
	
	$(document).on('change','.bottomSellingProducts',function(){
		var product_id = $(this).val();
		var product_name = $(this).attr('data-name');
		var product_tr = '';
		if(this.checked) {
			product_tr += '<tr id="'+product_id+'">';
			product_tr += '<td><input type="text" readonly name="product_id[]" value="'+product_id+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" readonly name="product_name[]" value="'+product_name+'" class="form-control readonly_input width100"></td>';
			product_tr += '<td><input type="text" name="product_price[]" id="product_price_'+product_id+'" class="form-control required number sell_product_input" row_id='+product_id+'></td>';
			product_tr += '<td><input type="text" name="special_price[]" id="special_price_'+product_id+'" class="form-control required number sell_product_input" row_id='+product_id+'></td>';
			$('#sellbased-product-table').append(product_tr);
		} else {
			$('table#sellbased-product-table tr#'+product_id).remove();
		}
		var checked_top_products = $('input.topSellingProducts:checked').length;
		var checked_bottom_products = $('input.bottomSellingProducts:checked').length;
		if(checked_top_products > 0 || checked_bottom_products > 0) {
			$('#sellbased-product-table').show();
		} else {
			$('#sellbased-product-table').hide();
		}
	});
	
	// Prepare selected products json
	function manage_product_json() {
		var product_ids	  = new Array();
		var product_names = new Array();
		var selling_products = new Array();
		$('input.topSellingProducts[type=checkbox]').each(function () {
			if(this.checked) {
				var ProductArray = {};
				ProductArray['product_id'] = $(this).val();
				ProductArray['product_name'] = $(this).attr('data-name');
				ProductArray['product_price'] = $('#product_price_'+ProductArray['product_id']).val();
				ProductArray['special_price'] = $('#special_price_'+ProductArray['product_id']).val();
				selling_products.push(ProductArray);
			}
		});
		
		$('input.bottomSellingProducts[type=checkbox]').each(function () {
			if(this.checked) {
				var ProductArray = {};
				ProductArray['product_id'] = $(this).val();
				ProductArray['product_name'] = $(this).attr('data-name');
				ProductArray['product_price'] = $('#product_price_'+ProductArray['product_id']).val();
				ProductArray['special_price'] = $('#special_price_'+ProductArray['product_id']).val();
				selling_products.push(ProductArray);
			}
		});
		
		var product_search_type = $("input[name='product_add_type']:checked").val();
		// convert products array to json
		var edm_products_json = '';
		if(selling_products.length > 0 && product_search_type == 2) {
			var edm_products_json = JSON.stringify(selling_products);
		}
		$('#edm_sell_base_products_json').val(edm_products_json);
		return true;
	}
	
	// Prepare non selling selected products json
	function manage_other_product_json() {
		var filtered_other_products = new Array();
		if(productIDArray.length > 0) {
			for(var i=0;i<productIDArray.length;i++){
				var ProductArray = {};
				ProductArray['product_id'] = productIDArray[i];
				ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
				ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
				ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
				filtered_other_products.push(ProductArray);
			}
		}
		// convert products array to json
		var edm_products_json = JSON.stringify(filtered_other_products);
		console.log(edm_products_json+'||Json');
		$('#edm_products_json').val(edm_products_json);
		return true;
	}

	// remove other product and manage template products
	$(document).delegate('.remove-btn','click',function(e){
		var row_id = $(this).attr('row_id');
		var filtered_other_products = new Array();
		var product_ids_val = '';
		if(productIDArray.length > 0) {
			for(var i=0;i<productIDArray.length;i++){
				if(row_id != productIDArray[i]) {
					product_ids_val += productIDArray[i]+',';
					var ProductArray = {};
					ProductArray['product_id'] = productIDArray[i];
					ProductArray['product_name'] = productNAMEArray[productIDArray[i]];
					ProductArray['product_price'] = $('#product_price_'+productIDArray[i]).val();
					ProductArray['special_price'] = $('#special_price_'+productIDArray[i]).val();
					filtered_other_products.push(ProductArray);
				}
			}
		}
		$('#product_other').val(product_ids_val);
		// convert products array to json
		var edm_products_json = JSON.stringify(filtered_other_products);
		$('#edm_products_json').val(edm_products_json);
		$('table#product-table tr#'+row_id).remove();
	});
	
	/* 
	| -----------------------------------------------------
	| Manage upload manual email adresses csv
	| -----------------------------------------------------
	*/ 
	$(document).delegate( ".uploadcsv", "click", function() {
		var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv)$/;  
       //Checks whether the file is a valid csv file  
       if (regex.test($("#csvfile").val().toLowerCase())) { 
           //Checks whether the browser supports HTML5  
           if (typeof (FileReader) != "undefined") {  
               var reader = new FileReader();  
               reader.onload = function (e) {
					//var html = '';                   
					var manual_emails = $('#manual_emails').val();
					var email_html = $("#csvemails").html();
					if(manual_emails == '') { // add comma for first element
						manual_emails = ',';
					}        
					//Splitting of Rows in the csv file  
					var csvrows = e.target.result.split("\n");
					for (var i = 0; i < csvrows.length; i++) {
						if (csvrows[i] != "") {                 
							var csvcols = csvrows[i].split(",");
							var email_id = csvcols[0]+',';
							if(manual_emails.indexOf(email_id) == -1) { // accept only non duplicate id
								var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
								if (filter.test(csvcols[0])) {
									email_html += '<span class="chosen-choices fl ml5">&nbsp;<span>'+csvcols[0]+'</span>&nbsp;&nbsp;<span class="email-id-close crp" email_id="'+csvcols[0]+'" ><strong> X </strong></span>&nbsp;</span>';
									manual_emails += email_id;
								}
							}
						}  
					}             
					$("#csvemails").html(email_html);
					if(manual_emails.length > 0){
						$('#manual_emails').val(manual_emails);
					}
				}
				$('#csvemails').removeClass('error');
				reader.readAsText($("#csvfile")[0].files[0]);  
			}  
			else {  
                bootbox.alert("Sorry! Your browser does not support HTML5!");  
			}  
		} else {  
            bootbox.alert("Please upload a valid CSV file!");  
		}
		return;
		e.preventDefault(); 
   });
   
   /* 
	| -----------------------------------------------------
	| Remove manual email row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".email-id-close","click",function(e){
		$(this).parent('span').remove();
		var email_id = $(this).attr('email_id');
		var manual_emails = $('#manual_emails').val();
		if(email_id != '') {
			manual_emails = manual_emails.replace(email_id+',','');
			$('#manual_emails').val(manual_emails);
		}
    });
</script>

