<style>
 .chosen-choices
  {
    background-color: #ffffff;
    border: 1px solid #cccccc;
    border-top-right-radius: 4px;
    border-top-left-radius: 4px;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    cursor: text;
    height: auto !important;
    height: 1%;
    overflow: hidden;
    padding: 0;
    position: relative;
    width: auto;
    }
.dropdown {
  position: relative;
      margin: 0;
    padding: 0;
    margin-bottom: 15px;
 }


.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: -1px 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #fff;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
     background-color: #95979829;
    display: block;
    min-height: 30px;
    line-height: 30PX;
    border: 0;
    width: 272px;
    padding: 7px 5px;
    color: black;
    margin-top:30px;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
    padding: 0 3px 2px 0;
    color: #3e3d3d;
    display: list-item;
    list-style-type: decimal;
    list-style-position: inside;
    line-height: 20px;
}

.dropdown dd ul {
     display: none;
     background-color: #e1e2e2;
    color: #3a3b36;
    left: 0px;
    padding: 2px 15px 15px 5px;
    top: 2px;
    width: 272px;
    list-style: none;
    height:90px;
    overflow: auto;
    font-size: 12px;
    border: 1px solid #ccc7c7;
    margin-bottom: 10px;
        overflow-y: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #fff;
}
.p-relative{ position:relative;}
.p-relative label.error {
    position: absolute;
    top: -12px;
    min-width: 300px;
}
.rewardcard-value {
    position: relative;
    padding-bottom: 20px;
}
.rewardcard-value label.error{ 
	position: absolute;
    bottom: 0px;
    min-width: 300px;
}

</style>  
<link href="<?php echo Template::theme_url('edm-dashboard/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" media="all"/>

<?php
$edm_id 	   			=  !empty($edm_data['id'])?$edm_data['id']:'';
$edm_title  			=  !empty($edm_data['edm_title'])?$edm_data['edm_title']:'';
$start_date 			=  !empty($edm_data['start_date'])?$edm_data['start_date']:'';
$end_date  				=  !empty($edm_data['end_date'])?$edm_data['end_date']:'';
$product_top  			=  !empty($edm_data['product_top'])?json_decode($edm_data['product_top']):array();
$product_bottom  		=  !empty($edm_data['product_bottom'])?json_decode($edm_data['product_bottom']):array();
$product_other  		=  !empty($edm_data['product_other'])?$edm_data['product_other']:'';
$occurance_type  		=  !empty($edm_data['occurance_type'])?$edm_data['occurance_type']:'';
$occurance_data  		=  !empty($edm_data['occurance_data'])?json_decode($edm_data['occurance_data']):'';
$rec_home_store_id  	=  !empty($edm_data['rec_home_store_id'])?json_decode($edm_data['rec_home_store_id']):array();
$rec_previous_purchase  =  !empty($edm_data['rec_previous_purchase'])?$edm_data['rec_previous_purchase']:'';
$rec_all  				=  !empty($edm_data['rec_all'])?$edm_data['rec_all']:'';
$rec_type			  	=  !empty($edm_data['rec_type'])?json_decode($edm_data['rec_type']):array();
$rec_reward_card_type  	=  !empty($edm_data['rec_reward_card_type'])?json_decode($edm_data['rec_reward_card_type']):array();
$no_products  			=  !empty($edm_data['no_products'])?$edm_data['no_products']:'';
$no_of_top_products  	=  !empty($edm_data['no_of_top_products'])?$edm_data['no_of_top_products']: $this->config->item('no_of_top_products');
$no_of_bottom_products  =  !empty($edm_data['no_of_bottom_products'])?$edm_data['no_of_bottom_products']: $this->config->item('no_of_bottom_products');
$is_subscription  		=  !empty($edm_data['is_subscription'])?$edm_data['is_subscription']:'';
$created_date  			=  !empty($edm_data['created_date'])?$edm_data['created_date']:'';
$product_top_val  		=  !empty($edm_data['product_top'])?$edm_data['product_top']:'';
$product_bottom_val  	=  !empty($edm_data['product_bottom'])?$edm_data['product_bottom']:'';
$email_header_text  	=  !empty($edm_data['email_header_text'])?$edm_data['email_header_text']:'';
$email_description  	=  !empty($edm_data['email_description'])?$edm_data['email_description']:'';
$product_mapping  	    =  !empty($edm_data['product_mapping'])?$edm_data['product_mapping']:'';
$email_banner_image  	=  !empty($edm_data['email_banner_image'])?$edm_data['email_banner_image']:'';
// convert offer json to array 
$selected_offers	    = (!empty($edm_data['instore_offers'])) ? (array) json_decode($edm_data['instore_offers']) : array();
//$SelectTop = (empty($product_top))?"<span class='hida'>Select top selling product</span>":"";
$SelectTop = "";
$TopHtml   = "";
$TopName   = "";
$isTop_dn  = "dn";
if(!empty($top)){	
	foreach($top as $topData){
		$prId = $topData['JNAD_PRODUCT'];
		$prName = $topData['PROD_DESC'];
		$isTopchecked = "";
		if(in_array($topData["JNAD_PRODUCT"],$product_top)){
			$selectoPtion  = $prName.',';
			$TopName      .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isTopchecked  = "checked";	 
			$isTop_dn 	    = "";			 
		}
		$TopHtml .= "<li><input type='checkbox' class='topSellingProducts' name='product_top[]' ".$isTopchecked." value='$prId' data-name='$prName' />$prName</li>";
	 }
	 $SelectTop .= "<ul class='multiSel'>".$TopName."</ul>";
}

//$SelectBottom = (empty($product_bottom))?"<span class='hida'>Select bottom selling product</span>":"";
$SelectBottom = '';
$BottomHtml   = "";
$BottomName   = "";
$isBottom_dn  = "dn";
if(!empty($bottom)){
	foreach($bottom as $bottomData){
		$prId   = $bottomData['JNAD_PRODUCT'];
		$prName = $bottomData['PROD_DESC'];
		$isBottomchecked = "";
		if(in_array($bottomData["JNAD_PRODUCT"],$product_bottom)){
			$selectoPtion = $prName.',';
			$BottomName  .= "<span title='$selectoPtion'>$selectoPtion</span>";
			$isBottomchecked = "checked";
			$isBottom_dn = "";
		}
		$BottomHtml .= "<li><input type='checkbox' class='bottomSellingProducts' name='product_bottom[]'  ".$isBottomchecked."   value='$prId' data-name='$prName' />$prName</li>";
	}
	$SelectBottom .= "<ul class='multiSel'>".$BottomName."</ul>";
}

$SelectStore  = (empty($rec_home_store_id))?"<span class='hida'>Select Store</span>":"";
$StoreHtml   = "";
$StoreName   = "";
$isStore_dn  = "dn";
if(!empty($storeList)){
	 foreach($storeList as $key=>$value){
		   $prId   = $key;
		   $prName = $value;
           $isStorechecked = "";
         if(in_array($prId,$rec_home_store_id)){
			 $selectoPtion = $value.',';
			 $StoreName   .= "<span title='$selectoPtion'>$selectoPtion</span>";
			 $isStorechecked = "checked";
			 $isStore_dn = "";
		 }
         $StoreHtml     .= "<li><input type='checkbox' name='rec_home_store_id[]'  ".$isStorechecked."   value='$prId' data-name='$prName' />$prName</li>";
	 }
 }

$days = array(
		'sunday'   =>'Sunday',
		'monday'   =>'Monday',
		'tuesday'  =>'Tuesday',
		'wednesday'=>'Wednesday',
		'thursday' =>'Thursday',
		'friday'   =>'Friday',
		'saturday' =>'Saturday',
	);
?>
<!-- Title input start here -->
<!-- new tab Start Content panel -->
 <div class="col-md-6" style="padding:0px 15px 0px 0px; margin:0px;">
	<div class="tabbable "> 
		<?php echo form_open($this->uri->uri_string(),array('name'=>'frm_edm','id'=>'frm_edm','enctype'=>'multipart/form-data')); ?>
		
			<ul class="nav nav-tabs edm-tabs">
				<li class="active" data-tab="info_tab"><a <?php echo (!empty($edm_id))?"href='#info_tab'  data-toggle='tab'":""; ?>><?php echo lang('nav_tab1');?></a></li>
				<li data-tab="edm_email_tab"><a <?php echo (!empty($edm_id))?"href='#edm_email_tab'  data-toggle='tab'":""; ?>><?php echo lang('nav_tab5');?></a></li>
				<li data-tab="frequency_tab" data-ntab="receiver_tab"><a <?php echo (!empty($edm_id))?"href='#frequency_tab'  data-toggle='tab'":""; ?>><?php echo lang('nav_tab3');?></a></li>
				<li data-tab="receiver_tab" data-ntab="other_tab"><a <?php echo (!empty($edm_id))?"href='#receiver_tab'  data-toggle='tab'":""; ?>><?php echo lang('nav_tab4');?></a></li>
			</ul>
			<div class="tab-content scroll ">
				<!-- Start edm general tab panel -->
				<div class="tab-pane active" id="info_tab">
					<div class="form-wrapper clearboth">
						<!-- invite points input start here -->
						<div class="form-box">
							<label for="edm-other-other_tab"><?php echo lang('edm_title_labl');?></label>						
								<?php
								echo form_hidden('edm_id',$edm_id);
								$data = array('name'=>'edm_title','id'=>'edm_title', 'value'=>$edm_title,'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_title_labl'));
								echo form_input($data);
							 ?>
							</div>
							<!-- start_date input start here -->
							<div class="form-box">
								<div class="iline-view">
									<label for="co_start_date"><?php echo lang('edm_start_date');?><i style="color:red;">*</i></label>
									<div class="half_date">
									   <div class="form-group input-append date" id="start_date_div1">
										  <?php
											 $data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$start_date,'id'=>'start_date','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
											 echo form_input($data);
											 ?>
										  <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
									   </div>
									</div>
								</div>
								
								 <div class="iline-view">
									<label for="co_end_date"><?php echo lang('edm_end_date');?><i style="color:red;">*</i></label>
									<div class="half_date">
									   <div class="form-group input-append date" id="end_date_div1">
										  <?php
											 $data = array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'value'=>$end_date,'id'=>'end_date','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_end_date'), 'readonly'=>'readonly');
											 echo form_input($data);?>
										  <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
									   </div>
									</div>
								</div>	
							</div>
							<!-- end_date input end here -->
						</div>
						<!-- submit button here -->
						 <div class="form-box ">
							<button type="submit" name="next"   class="btn btn-primary btn-margin"><?php echo 'Next';?></button>
						 </div>
						<!-- submit button here -->
						<div class="clearboth"></div>
					</div>
				<!-- End edm general tab panel -->	
				
				<!-- Start edm email setting tab panel -->
				<div class="tab-pane co_products_tab min-loader" id="edm_email_tab">
					<?php 
					$template_sugession_display = (!empty($edm_template)) ? 'display_none' : '';
					$hide_template = (!empty($edm_template)) ? '' : 'display_none';?>
					<!-- Template layout suggession box start here -->
					<div id="choose-template" class="text-center <?php echo $template_sugession_display ;?>">
						<button class="choose" type="button" id="grid_view"><img src="<?php echo site_url().'uploads/edm_images/edm-grid-view.jpg'?>" class="img-responsive" alt=""><span>Grid Template</span></button>
						<button class="choose" type="button" id="list_view"><img src="<?php echo site_url().'uploads/edm_images/edm-list-view.jpg'?>" class="img-responsive" alt=""><span>List Template</span></button>
					</div>
					<!-- Template layout suggession box end here -->
					
					<div class="row-fluid custom-email">
						<!-- Template input fields box start here -->
						<div class="form-wrapper span6 email-template-box <?php echo $hide_template;?>">
									
							<!-- Banner image box start here -->
							<div class="form-box">
								<label for="title" class="img-title"><?php echo lang('email_banner_image');?></label> 	
								<div class="imge-wrap">
									<div class="img-box">
										<?php
										// set banner image action
										$banner_image_val = (isset($email_banner_image) && !empty($email_banner_image)) ? $email_banner_image :'';
										$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
										if(!empty($banner_image_val)) {
											$banner_image_src = base_url('uploads/edm_images/'.$banner_image_val);
											$title = lang('tooltip_offer_image');
									
											$banner_image_val_doc = $this->config->item('document_path').'uploads/edm_images/'.$email_banner_image; 
											if(!file_exists($banner_image_val_doc)){
												// $promo_image_1 = base_url('uploads/No_Image_Available.png');
												$banner_image_src = base_url('uploads/No_Image_Available.png');
												$title = "No Image Available";
											}
										} else {
											$title = lang('tooltip_image');
											$banner_image_src = base_url('uploads/upload.png');
										} ?>
										<div class="cell-img" id="banner_image_cell">
											<img id="banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
										</div>
										<?php
										$data	= array('name'=>'email_banner_image', 'id'=>'banner_image_val', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
										echo form_upload($data);
										// set banner image value in hidden type
										$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
										echo form_input($data);?>
									</div>
								</div>
							</div>
							<!-- Banner image box end here -->
							<div class="clearboth"></div>
							<!-- Email description text start here -->
							<div>
								<label><?php echo lang('email_description');?></label>
								<div class="control-group">	
									<?php
									$data	= array('name'=>'email_description', 'class'=>'edmmceEditor redactor span8', 'value'=>isset($email_description) ? $email_description : '');
									echo form_textarea($data);
									?>
								</div>
							</div>		
							<!-- Email description text end here -->
						
							<!-- Product mapping availability radio options start here -->
							<div class="form-box">
								<label><?php echo lang('product_mapping');?>
									<span class="fa fa-info-circle" title="<?php echo lang('product_mapping_help_txt');?>"></span>
								</label>
								<div class="control-group ">
									<input type="checkbox" name="product_mapping" id="product_mapping" <?php echo (!empty($product_mapping)) ? 'checked="checked"' : '';?>  >
								</div>
							</div>
							<?php
							$data = array('type'=>'hidden','name'=>'edm_products_json','id'=>'edm_products_json','value'=> !empty($edm_data['edm_products_json'])?$edm_data['edm_products_json']:'');
							echo form_input($data);
							?>
							<!-- Product mapping availability radio options end here -->
						
							<!-- Product mapping box start here-->
							<div class="<?php echo (!empty($product_mapping)) ? '' : 'dn' ?>" id="product_mapping_div">
								<!-- Product box title input start here -->
								<div class="form-box">
									<label><?php echo lang('product_grid_title');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'product_grid_title','id'=>'product_grid_title','value'=>set_value('product_grid_title', isset($edm_data['product_grid_title'])?$edm_data['product_grid_title']: ''), 'class'=>'span8 required');
									echo form_input($data);?>
								</div>	
								<!-- Product box title input end here -->
								
								<div class="form-box">
									<!-- number of top selling product input start here -->
									<div class="iline-view">
										<label for="co_start_date"><?php echo lang('no_of_top_selling_products');?><i style="color:red;">*</i>
											<span class="fa fa-info-circle" title="<?php echo lang('no_of_top_selling_products_help_txt');?>"></span>
										</label>
										<div class="half_date">
											<div class="form-group input-append" >
												<?php
												$data = array('name'=>'no_of_top_products','id'=>'no_of_top_products', 'value'=> $no_of_top_products ,
												'class'=>'span8 required product_selling_fetch_limit','product_selling_type'=>1, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_top_selling_products'));
												echo form_input($data);
												$data = array('type'=>'hidden','id'=>'no_of_top_products_hidden','value'=> $no_of_top_products);
												echo form_input($data);
												?>		
											</div>
										</div>
									</div>
									<!-- number of top selling product input end here -->
									
									<!-- number of bottom selling product input start here -->
									 <div class="iline-view">
										<label for="co_end_date"><?php echo lang('no_of_bottom_selling_products');?><i style="color:red;">*</i>
											<span class="fa fa-info-circle" title="<?php echo lang('no_of_bottom_selling_products_help_txt');?>"></span>
										</label>
										<div class="half_date">
										   <div class="form-group input-append">
												<?php
												$data = array('name'=>'no_of_bottom_products','id'=>'no_of_bottom_products', 'value'=> $no_of_bottom_products ,
												'class'=>'span8 required product_selling_fetch_limit','product_selling_type'=>2, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('no_of_bottom_selling_products'));
												echo form_input($data);
												$data = array('type'=>'hidden','id'=>'no_of_bottom_products_hidden','value'=> $no_of_bottom_products);
												echo form_input($data);
												?>
										   </div>
										</div>
									</div>	
								</div>
								<!-- number of bottom selling product input end here -->
								
								<!-- number of top selling product input start here -->
								<div class="form-box mt10 indispaly">
									<label>
										<span class="fl mr5">
											<input  type="checkbox" onchange="cbchange(this)" data-show="topsell-pr" id="topSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_top)?"checked":""); ?>>
										</span>
										<span class="fl mr5">
											<?php echo lang('edm_top_selling_product');?>
										</span> 
									</label>  
									<span class="topsell-pr <?php echo $isTop_dn; ?>">			 						 
										<dl class="dropdown" id="top"> 
											<dt>
												<a href="#" class="selectProducts_1">
													<?php echo $SelectTop;?>
												</a>
											</dt>
											<dd>
												<div class="mutliSelect" name="top">
													<ul class="ulProductHtml_1">
														<?php echo $TopHtml; ?>
													</ul>
												</div>
											</dd>
										</dl>
									</span>
								</div>
								<!-- number of top selling product input end here -->
								
								<!-- bottom selling products dropdown start here -->
								<div class="form-box mt10 indispaly">
									<label>
										<span class="fl mr5">
											<input  type="checkbox" onchange="cbchange(this)" data-show="bottomsell-pr" id="bottomSellingProducts" class="product" data-type='bottom' <?php echo (!empty($product_bottom)?"checked":""); ?>>
										</span>
										<span class="fl mr5">	
											<?php echo lang('edm_bottom_selling_product');?>
										</span>	
									</label>
									<span class="bottomsell-pr <?php echo $isBottom_dn; ?>">
										<dl class="dropdown" id="bottom"> 
											<dt>
												<a href="#" class="selectProducts_2">
													<?php echo $SelectBottom; ?>
												</a>
											</dt>
											<dd>
												<div class="mutliSelect"  name="bottom">
													<ul class="ulProductHtml_2">
														<?php echo $BottomHtml; ?>
													</ul>
												</div>
											</dd>
										</dl>
									</span>
								</div>
								<!-- bottom selling products dropdown end here -->
								
								<!-- other product div start here -->
								<div class="clear-both">
									<div class="form-box mt10 indispaly">
										<label>
											<span class="fl mr5">
												<input type="checkbox" name="is_product_group" class="otherPr__" value="1" id="is_product_group"  <?php echo (!empty($product_other)?"checked":""); ?>>
											</span>
											<span class="fl mt1"><?php echo lang('edm_other_product');?></span>
										</label>
										<div id="productgropuShowHide" style=" clear: both; padding-top: 10px;" class="<?php echo (!empty($product_other) ? "" : "dn"); ?>">
											<div class="mr10" id="searchProductButton">
												&nbsp;&nbsp;
												<button type="button" name="product_search" class="btn btn-primary otherPr" ><?php echo lang('search_product');?></button>
											</div>
											<div class="mt10 "  id="showproductDiv"></div>
											<input type="hidden" id="product_other" name="product_other" value="">
										</div>					
									</div><br>
								</div>
								<!-- other product div end here -->
									
								<!-- number of product show on email input start here -->
								<div class="form-box">
									<label for="edm-other-quantity"><?php echo lang('no_of_product_in_email');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'no_products','id'=>'no_products', 'value'=> $no_products ,
									'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('edm_top_selling_product'));
									echo form_input($data);?>
								</div>
								<!-- number of product show on email input end here -->	
							</div>
							<!-- Product mapping box end here-->
							
							<!-- Email footer content box start here -->
							<div>
								<label><?php echo lang('email_footer_content');?></label>
								<div class="control-group">	
									<?php 
									$data	= array('name'=>'email_footer_text', 'class'=>'footermceEditor redactor span8', 'value'=>(!empty($edm_data['email_footer_text'])?$edm_data['email_footer_text']:''));
									echo form_textarea($data);
									?>
								</div>
							</div>		
							<!-- Email footer content box end here -->
							
							
							<!-- Offer mapping availability radio options start here -->
							<!--
							<div class="form-box">
								<label><?php echo lang('instore_offers_mapping');?>
									<span class="fa fa-info-circle" title="<?php echo lang('offers_mapping_help_txt');?>"></span>
								</label>
								<div class="control-group ">
									<input type="checkbox" name="offers_mapping" id="offers_mapping" <?php echo (!empty($selected_offers)) ? 'checked="checked"' : '';?>  >
								</div>
								<?php
								$data = array('type'=>'hidden','name'=>'instore_offers_json','id'=>'instore_offers_json','value'=> !empty($edm_data['instore_offers'])?$edm_data['instore_offers']:'');
								echo form_input($data);
								?>
							</div>
							<div class="clearboth"></div>
							-->
							<!-- Offer mapping availability radio options end here -->
							
							<!-- Offers mapping box start here-->
							<div class="<?php echo (!empty($selected_offers)) ? '' : 'dn' ?>" id="offers_mapping_div">
								<!-- In-store offers input start here -->
								<div>
									<?php
									$instore_offers = $offers['instore_offers'];
									if(!empty($instore_offers)) {
										for($i=0;$i<count($instore_offers);$i++){
											$default_image = $instore_offers[$i]['is_default_image'];
											$offer_image = $instore_offers[$i]['offer_image_'.$default_image];
											$offer_image = (!empty($offer_image)) ? $offer_image : '';
											$offer_image = base_url('uploads/offer_images/'.$offer_image);
											$offer_id = $instore_offers[$i]['offer_id'];
											$checked_offer = '';
											if (in_array($offer_image,$selected_offers)) {
												$checked_offer = 'checked="checked"';
											}?>
											<span class="instore-offer-box">
												<div>
													<img id="offer_img_<?php echo $offer_id;?>" src="<?php echo $offer_image; ?>" >
												</div>
												<div>
													<span>
														<input type="checkbox" class="instore_offers" value="<?php echo $instore_offers[$i]['offer_id']?>" name="instore_offers" id="instore_offers" <?php echo $checked_offer;?>>
													</span>
													<span>
														<?php echo $instore_offers[$i]['offer_name'];?>
													</span>
												</div>
											</span>	
										<?php	
										}
									}
									?>
								</div>	
								<!-- In-store offers input end here -->
							</div>
							<!-- Offers mapping box start here-->
						</div>
						<!-- Template input fields box end here -->
						
						<!-- Email template box start here -->
						<div class="form-wrapper span6 email-template-box <?php echo $hide_template;?>">
							<?php echo $this->load->view('edm_email_template_form2'); ?>
						</div>
						<!-- Email template box end here -->
					</div>
					
					<!-- submit button here -->
					 <div class="form-box email-template-box <?php echo $hide_template;?>">
						<?php
						$data = array('type'=>'hidden','name'=>'template_type','id'=>'template_type','value'=>(!empty($edm_data['template_type']))?$edm_data['template_type']:'');
						echo form_input($data);?>
						<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()" ><?php echo 'Back';?></button> 
						<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
						<!--
						<button type="button" name="test"   class="btn btn-primary get_template"><?php echo 'Get Template';?></button>
						-->
					 </div>
					<!-- submit button here -->
				</div>						
				<div class="clearboth"></div>
				<!-- End edm email tab panel -->
					
				<!-- Start schedule tab panel -->
				<div class="tab-pane" id="frequency_tab">
					<div class="">
						<!-- edm_occurance input start here -->
						 <div class="form-box">
							<label for="co_end_date"><?php echo lang('edm_occurance_type');?><i style="color:red;">*</i></label>
							 <div class="app">
								<div class="appointment">
									<div class="col-md-12">
										<div class="menu-Recurrence">
											<ul class = "freq-edm">
											  <li>	
												  <div class="valuedm">
											  <input class="edm_occurance" data-div="daily_div" type="radio" name="occurance_type"  id="daily" value="1" <?php echo ($occurance_type == 1 || $occurance_type == '')?"checked":""; ?> />			
											  <label for="recurring_type"><?php echo lang('edm_Daily');?></label>	
											  </div>
											  
											  </li>
											   <li>
												  <input class="edm_occurance" data-div="weekly_div" type="radio" name="occurance_type"  id="weekly" value="2" <?php echo ($occurance_type == 2)?"checked":""; ?> />											
												  <label for="recurring_type"><?php echo lang('edm_Weekly');?></label>
																											
											   </li>
											   <li>
												  <input class="edm_occurance" data-div="monthly_div" type="radio" name="occurance_type"  id="monthly" value="3" <?php echo ($occurance_type == 3)?"checked":""; ?> />
												  <label for="recurring_type"><?php echo lang('edm_Monthly');?></label>															
											   </li>
											   <li>
												  <input class="edm_occurance" data-div="yearly_div" type="radio" name="occurance_type"  id="yearly" value="4" <?php echo ($occurance_type == 4)?"checked":""; ?>/>
												  <label for="recurring_type"><?php echo lang('edm_Yearly');?></label>															
											   </li>
											</ul>
										</div>
										<?php 
										$daily = $weeks = $monthly = $yearly = array()	;
										switch($occurance_type){
											case 1:
											$daily = $occurance_data;
											break;
											case 2:
											$weeks = $occurance_data;
											break;
											case 3:
											$monthly = $occurance_data;
											break;
											case 4:
											$yearly = $occurance_data;
											break;
										}														
										?>
										<!-- daily_div -->
										<div class="values-seen">
											<div class="col-md-9 occurance-div <?php echo ($occurance_type == 1 || $occurance_type == '') ? "" : "dn";?>" id="daily_div"  >
												<div class="pull-left">
													<h5>Promotional Messages will repeat on every day.</h5>
												</div>
											</div>
											<!-- weekly_div -->	
											<div class="col-md-10 col-xs-12 occurance-div  <?php echo ($occurance_type == 2 && !empty($weeks))?"":"dn"; ?>" id="weekly_div">
												<div class="pull-left">
													<h5>Promotional Messages will repeat every week on.</h5>
													<ul class="list-inline ul_week custcheck-box" id="recurring_day_ul" >
														<p> Select your occurrence for week</p>
														<?php
														$dayCount = 1;			
														foreach($days as $daykey => $dayvalue){ ?>							
															<li>
																<input id="box<?php echo $dayCount; ?>" name="weekly_days[]" value="<?php echo $daykey; ?>" <?php echo (in_array($daykey,$weeks))?'checked':'';?> type="checkbox" />
																<label for="box<?php echo $dayCount;?>"><?php echo $dayvalue; ?></label>
															</li>
														<?php
															$dayCount++;
														}?> 	 
													</ul>
												</div>
											</div>
											<!-- monthly_div -->
											<div class="col-md-10 col-xs-12 occurance-div <?php echo ($occurance_type == 3 && !empty($monthly))?"":"dn"; ?>" id="monthly_div" >
												<h5>Promotional Messages will repeat every month of</h5>
												<p> Select your occurrence for week</p> 
												<ul class="list-inline ul_month custcheck-box" id="recurring_date_ul">
													 <?php 
													 for($i=1; $i<=31; $i++){ ?>
														<li>
															<input id="<?php echo $i;?>" name="monthly_dates[]" value="<?php echo $i;?>" <?php echo (in_array($i,$monthly))?'checked':''; ?> type="checkbox" />
															<label for="<?php echo $i;?>"><?php echo $i;?></label>
														</li>
													  <?php } ?>
												</ul>
											</div>
											<!-- yearly_div -->
											<div class="col-md-9 occurance-div <?php echo ($occurance_type == 4 && !empty($yearly))?"":"dn"; ?>" id="yearly_div" >
												<h5>Promotional Messages will repeat every year of.</h5>												
												<div class="input-group" style="margin:10px 0;">							
													<div class="half_date">
														<div class="form-group input-append date" id="yearly_time_div">
															<?php
															$data	= array('name'=>'yearly_time', 'data-format'=>'dd-MM-yyyy', 'value'=>isset($yearly_time)?$yearly_time:'','id'=>'yearly_time','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('edm_start_date'), 'readonly'=>'readonly');
															 echo form_input($data);?>
															<span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
														</div>																   
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
										</div>
										<!-- appointment border -->
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<!-- edm_occurance input end here --> 
							<!-- submit button here -->
							<div class="form-box">
								<button type="button" name="cancel" class="btn btn-info back-btn" onclick="backnext()"><?php echo 'Back';?></button> 
								<button type="submit" name="next"   class="btn btn-primary"><?php echo 'Next';?></button>
							</div>
							<!-- submit button here -->	 
						</div>
						<div class="clearboth"></div>
					</div>
					<!-- End earning points panel -->
				</div>
				<!-- End schedule tab panel -->
				
				<!-- Start receiver tab panel -->
				<div class="tab-pane co_products_tab" id="receiver_tab">
					<div class="">
						
						<!-- invite points input start here -->
						<div class="form-box edm-receiver radio-homerec ">
							<input  type="radio" class="rec2" name="rec_type[]" data-type='bottom' onchange="cdradio(this)"  data-show="rec-home" value="2"  <?php echo (empty($edm_id))?"checked":((in_array(2,$rec_type))?"checked":"");?> >
							<label for="enable_referral" >
							 <?php echo 'All User';?>
							</label>
						</div>
						<!-- invite points input end here -->	
						
						<!-- referral status dropdown start here -->
						<div class="form-box edm-receiver radio-homerec">
							<div class="p-relative">
								<input  type="radio" class="rec1" name="rec_type[]" data-type='bottom' onchange="cdradio(this)" data-show="rec-home" value="1" <?php echo (in_array(1,$rec_type))?"checked":"";?> >
								<label for="enable_referral"><?php echo 'Home Store';?></label>
								<span class="rec-home  <?php echo (!empty($rec_home_store_id))?"":"dn"; ?>">
										<dl class="dropdown" id="store"> 
											<dt>
											<a href="#">
											   <?php echo $SelectStore; ?>      
											  <p class="multiSel"><?php echo $StoreName; ?></p>  
											</a>
											</dt>
											<dd>
												<div class="mutliSelect"  name="store">
													<ul>
														<?php echo $StoreHtml; ?>
													</ul>
												</div>
											</dd>
										</dl>
								<?php
								 //$data = array('name'=>'rec_home_store_id','id'=>'rec_home_store_id');
								 //echo form_dropdown($data,$storeList,set_value('rec_home_store_id',$rec_home_store_id));
								 ?>
								 </span>
							</div>	
						</div>
						<!-- referral status dropdown end here -->
						<!-- invite points limit input start here -->
						<div class="form-box edm-receiver">
							<input  type="checkbox" class="rec3" name="rec_type[]" data-type='bottom' value="3" <?php echo (in_array(3,$rec_type))?"checked":"";?>>
							<label for="enable_referral" ><?php echo 'Previously Purchased';?></label>
						</div>
						<!-- invite points limit input end here -->
						<!-- invite points limit input start here -->
						<div class="form-box edm-receiver">
							<input  class="main-rec" name="rec_type[]" onchange="cbchange(this)" data-show="sub-rec" type="checkbox" class="rec4" data-show="" value="4" <?php echo (in_array(4,$rec_type))?"checked":"";?>>
							<label for="enable_referral"><?php echo 'Reward Card Type';?></label>
						<span class="sub-rec  <?php echo (!empty($rec_reward_card_type))?"":"dn"; ?> ">
						<ul class="rewardcard-value" >
						   <?php 
						   if(!empty($rewardCard_data)){
						   foreach($rewardCard_data as $cards){ 
						   ?>
						   <li><input  name ="rec_rc[]" type="checkbox" class="rec4_<?php echo $cards->id;?>" data-type='bottom' <?php echo (in_array($cards->id,$rec_reward_card_type))?'checked':''; ?> data-show="" value="<?php echo $cards->id; ?>" > <?php echo ($cards->card_name)?$cards->card_name:''; ?></li>
						   <?php 
						    } 
						   }	 
						  ?>
						</ul> 
						</span>
						</div>
						<!-- invite points limit input end here -->
					</div>
						<!-- submit button here -->
						<div class="form-box" style="padding:10px 0;">
							<button type="button" onclick="backnext()" name="cancel" class="btn btn-info back-btn"><?php echo 'Back';?></button> 
							<button type="submit" name="submit"   class="btn btn-primary"><?php echo 'Submit';?></button>
						 </div>
						<!-- submit button here -->
					<div class="clearboth"></div>
				</div>
				<!-- End receiver tab panel -->
				</div>
		<?php echo form_close(); ?>
	</div>
</div>	
	
<!-- start the bootstrap modal where the other product appear -->
<div class="modal fade dn" id="productmodal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Product Search</h4>
				</div>
				<div class="modal-body">
                  <div class="tab-pane" id="product_tab">
                    <button onclick="product_search()" class="btn default add-product-btn fr"  type="button product_direct_search_button" style="margin-top:0px">Product Search</button>		
                    <input type="text" placeholder="Product Id / Product Name" name="product_direct_search" id="product_direct_search" style="width:200px;margin-right:10px" class="fr" >
                    <select id="productType" name="productType" style="width:100px;margin-right:10px" class="fr">
                        <option value="id">Number</option>
                        <option value="name">Name </option>
                    </select>
                        <div class="clearboth"></div>
                        <div id="error_message" style="font-size:11px;color:red"></div> 
                   
						<!-- Product list / form  start here -->
						<div class="clearboth product-grid-form" id="appendProductTable">
							<table class="table table-bordered" id="product_tables"> 
								<thead class="bg_ccc">
									<tr>
										<th class="width:20px"><input type="checkbox" name="all_product_check" value="1" id="all_product_check"  ></th>
										<th class='product_table_th'><?php echo lang('product_number');?></th>
										<th class='product_table_th'><?php echo lang('product_name');?></th>
										<th class='width45'></th>
									</tr>
								</thead>
							  
							</table>
							<!-- Product fields end here -->
						</div>    
					</div> 
				</div>
				<div class="modal-footer">
                    <button onclick="add_into_filter()" class="btn default add-on-filter"  type="button">Add Into Filter</button>		
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end the bootstrap modal where other product will appear -->
<script src="<?php echo Template::theme_url('edm-dashboard/js/jquery.multi-select.js'); ?>"></script> 
<script>
	var productIDArray	= new Array();
	var productNAMEArray = new Array();
	var EmailProducts = {};
   
	$(document).ready(function() {
		if(jQuery('#start_date_div1').length > 0)  { 
           jQuery('#start_date_div1').datetimepicker({
               pickTime: false,
               startDate: new Date(), 
           });
       }
   
       if(jQuery('#end_date_div1').length > 0)  { 
           jQuery('#end_date_div1').datetimepicker({
               pickTime: false,
               startDate: new Date(), 
           });
       }
       
       if(jQuery('#yearly_time_div').length > 0)  {
           jQuery('#yearly_time_div').datetimepicker({
               pickTime: false,
               startDate: new Date(), 
           });
       }
   });
	
	function show_loader(action,className){
		var div = "<div class='loader loaderdiv'><i class='fa fa-refresh fa-spin' style='font-size: 28px;color: #111;position: absolute;left: 50%;bottom: 50%;transform: translate(-50%,-50%);'></i></div>";
		if(action){
			$('.'+className).css({"position": "relative","padding": "0px"});
			$('.'+className).prepend(div);
		}else{
			$('.'+className).css({"position":"","padding": ""});
			$('.loaderdiv').remove();
		}
	}	
	
	$(document).delegate(".otherPr", "click", function(e){
	 $('#productmodal').modal('show');
	});
	
	/* Hide and Show manual product box*/     
    $(document).on('click','#is_product_group',function(){
        if($(this).is(':checked')){
		$('#productgropuShowHide').show();
        }else{
			$('#productgropuShowHide').hide();
			$('#product_tab_link').hide(); // Hide Product Groups
			$('#filter_tab_link').show(); // Hide Product Groups
        } 
    });
	
   /* 
	| -----------------------------------------------------
	| Manage add quantity row 
	| -----------------------------------------------------
	*/ 
	function product_search() {
		// get last product row number
		var product_direct_search = $('#product_direct_search').val();
		var productType = $('#productType').val();
        
        if(product_direct_search == ""){
			//bootbox.alert("Empty search is not valid");
			$('#error_message').show();
			$('#error_message').html("Empty search is not valid");
			$('#error_message').hide(4000);
			$('#product_direct_search').val('');
			return false;
        }
        
        if(productType == 'id'){
            if(isNaN(product_direct_search)){
                //bootbox.alert("Invalid product number");
                $('#error_message').show();
                $('#error_message').html("Invalid product number");
                $('#error_message').hide(4000);
                $('#product_direct_search').val('');
                return false;
            }else{
               //alert('Input OK');
            }
        }
        $('#loader1').fadeIn();	
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {product_direct_search:product_direct_search,productType:productType},
			url  : BASEURL+"admin/settings/targetpush/search_product_direct",
			success: function(data){
				$('#appendProductTable').html(data.html);
				$('#loader1').fadeOut();
			}
		});
	}
	
   /* 
	| -----------------------------------------------------
	| Manage add filter product row 
	| -----------------------------------------------------
	*/ 
	function add_into_filter() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
		var row;
		var product_id;
		var product_ids_val = '';
		var product_name;
		var product_qty;
		var html = '';
		var j = productIDArray.length;
		$('#loader1').fadeIn();	
		$( ".single_product_check:checkbox:checked" ).each(function( index ) {
			row = parseInt($(this).attr('qty_row'));
			product_name = $(this).attr('product_name_'+row);
			product_qty = $(this).attr('product_qty_'+row);
			product_id = parseInt($(this).val());
			if(jQuery.inArray(product_id,productIDArray) < 0){
				productIDArray[j] = product_id;
				productNAMEArray[product_id] = product_name;
				j++;
			}
		});
		for(var i=0;i<productIDArray.length;i++){
			product_ids_val += productIDArray[i]+',';
			html += '<span class="chosen-choices fl ml5">&nbsp;<span>'+productNAMEArray[productIDArray[i]]+'</span>&nbsp;&nbsp;<span class="search-choice-close crp" qty_row="'+i+'" prod_id="'+productIDArray[i]+'" ><strong> X </strong></span>&nbsp;</span>';
		}
		if(productIDArray != ""){
			$('#product_other').val(product_ids_val);
		}
		if(html != ""){
			 $('#showproductDiv').html(html);  
			 $('#filter_tab_link').show(); // Hide Product Groups
		  //   $('#product_tab_link').hide();
			$('#filter_tab_link').trigger('click'); // Hide Product Group
			$('#productmodal').modal('hide');
		}
		$('#loader1').fadeOut();
   }
 
   /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".search-choice-close","click",function(e){
		$(this).parent('span').remove();
		var prod_id = $(this).attr('prod_id');
		var product_ids = $('#product_other').val();
		if(prod_id != '') {
			product_ids = product_ids.replace(prod_id+',','');
			$('#product_other').val(product_ids);
		}
    });
    
   	$( "#daily").click(function() {
		$('#daily_div').css("display","block");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","none");  
	});

	$("#weekly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","block");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","none");
	});

	$("#monthly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","block");
		$('#yearly_div').css("display","none");
	});

	$("#yearly").click(function() {
		$('#daily_div').css("display","none");
		$('#weekly_div').css("display","none");
		$('#monthly_div').css("display","none");
		$('#yearly_div').css("display","block");
	});
	
	function cdradio(res){
		var showdiv = $(res).attr('data-show');
		var res_val = parseInt($(res).val());
		if(showdiv!= ''){
			if(res.checked){
				if(res_val === 1){
					$('.'+showdiv).fadeIn('slow');	
				}else{
					$('.'+showdiv).fadeOut('slow');	
				}
			}
		}
	}
	
	function cbchange(res){
		var showdiv = $(res).attr('data-show');
		if(showdiv!= ''){
			if(res.checked){
				$('.'+showdiv).fadeIn('slow');	
			}else{
				$('.'+showdiv).fadeOut('slow');	
			}
		}
	}
	$(document).ready(function(){
		$("#frm_edm").validate({
			rules: {
				"rec_type[]": { 
						required: true, 
						minlength: 1 
				},
				"rec_rc[]": { 
						required: true, 
						minlength: 1 
				},
				no_products: { 
					min: 1
				} 
			}, 
			messages: { 
					"rec_type[]": "Please select at least one types of receiver.",
					"rec_rc[]": "Please select at least one reward card type."
			},
			submitHandler: function() {
				var activeTab = $("ul.nav-tabs").find(".active").attr('data-tab');
				switch(activeTab) {
					case 'receiver_tab':
						  savedata();
						break;
					case 'product_tab':
						var topSellingProducts = $('input.topSellingProducts:checked').length;
						var bottomSellingProducts = $('input.bottomSellingProducts:checked').length;
						var product_other = $('#product_other').val();
						if( (topSellingProducts > 0 && $('#topSellingProducts').is(':checked')) || (bottomSellingProducts  > 0 && $('#bottomSellingProducts').is(':checked')) || product_other != '' ) {
							//show_loader(1,'min-loader');	
							//createEdmTemplate();
						} else {
							bootbox.alert("Please select product(s) first!");
							return false;
						}
						break;
					default:
						backnext(true);
				}
			}
		});
		return false;
	});	
	
	// Manage form store procedure
	function savedata(){
		var formData = new FormData($('#frm_edm')[0]);
		$('#loader1').fadeIn();
		$.ajax({
			url: BASEURL+"admin/settings/edm/edm_configration",
			type: 'POST',
			data: formData,
			async: false,
			success: function (data) {
				$('#loader1').fadeOut();
				window.location.href = BASEURL+"admin/settings/edm";
			},
			cache: false,
			contentType: false,
			processData: false
		}, "json");
		return false;
	}
	
	function createEdmTemplate(){
		backnext(true);
		var fromData=$("#frm_edm").serialize();	
		$.post(BASEURL+"admin/settings/edm/edm_template_config",fromData, function(data){
			if(data.result){
				//$('#body').val(data.view);
				CKEDITOR.instances['body'].setData(data.view);
				show_loader(0,'min-loader');
			}
		}, "json");
	}

	function backnext(type){
		var activeTab = $("ul.nav-tabs").find(".active");
		//remove from current
		$(activeTab).removeClass("active");
		$("#"+$(activeTab).attr('data-tab')).removeClass("active");
		//add next 
		if(type){
			$(activeTab).next().addClass('active');
			$("#"+$(activeTab).next().attr('data-tab')).addClass('active');	
		}else{
		//add next 
			$(activeTab).prev().addClass('active');
			$("#"+$(activeTab).prev().attr('data-tab')).addClass('active');
		}
	}

	$(".dropdown dt a").on('click', function() {
		//console.log($(this).parent().next().find('.mutliSelect')..slideToggle('fast'));
		$(this).parent().next().find('.mutliSelect ul').slideToggle('fast');
		//$(".dropdown dd ul").slideToggle('fast');
		//$(this).parent().next("dd ul").slideToggle('fast');
	});
	
	$(".dropdown dd ul li a").on('click', function() {
		$(this).parent().next().find('.mutliSelect ul').hide();
		//$(".dropdown dd ul").hide();
	});

	function getSelectedValue(id) {
		return $("#" + id).find("dt a span.value").html();
	}

	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
	});

	$('.mutliSelect input[type="checkbox"]').on('click', function() {
		var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').attr('data-name'),
		title = $(this).attr('data-name') + ",";
		if ($(this).is(':checked')) {
			var html = '<span title="' + title + '">' + title + '</span>';
			// $(this).parent().find('.multiSel').append(html);
			//$('.multiSel').append(html);
			$(this).closest('.dropdown').find('.multiSel').append(html);
			$(".hida").hide();
		} else {
			$('span[title="' + title + '"]').remove();
			var ret = $(".hida");
			$(this).closest('.dropdown').find('dt a').append(ret);
			//$('.dropdown dt a').append(ret);
		}
	});
	
	// display top or bottom selling products in dropdown 
	$(".product_selling_fetch_limit").on("blur", function(){
		var product_limit =  $(this).val();
		var product_selling_type = $(this).attr('product_selling_type');
		var field_id = $(this).attr('id');
		var prev_limit = $('#'+field_id+'_hidden').val();
		if(prev_limit != product_limit) {
			$('#'+field_id+'_hidden').val(product_limit);
			var existing_products = '';
			if(product_selling_type == 1) {
				<?php if(!empty($product_top_val)) { ?>
					existing_products = JSON.parse('<?php echo $product_top_val;?>');
				<?php } ?>
			} else {
				<?php if(!empty($product_bottom_val)) { ?>
					existing_products = JSON.parse('<?php echo $product_bottom_val;?>');
				<?php } ?>
			}
			if(product_limit !== 0 || product_limit !== ''){
				manage_selling_product(product_limit,product_selling_type,existing_products);
			}
		}
	});
	
	// manage dynamic product dropdown appearance
	function manage_selling_product(product_limit,selling_product_type,existing_products) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: '<?php echo site_url("admin/settings/edm/get_products_selling_dropdown"); ?>',
			data: {product_limit:product_limit, selling_product_type:selling_product_type, existing_products : existing_products},
			beforeSend: function() {
				$('#loader1').fadeIn();	
			},
			success: function(data){
				if(data.result){
					$('.selectProducts_'+selling_product_type).html(data.selectedProducts);
					$('.ulProductHtml_'+selling_product_type).html(data.ulProductHtml);
				}
			},
			complete:function(){
				$('#loader1').fadeOut();	
			}
		});
	}
	
	$("#banner_image_picker").click(function() {
		$("input[name='email_banner_image']").click();
	});

	function read_url_banner_image(input) {
		
		var pickerId= '#'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 1.0955135) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
						$('.banner-img').show();
						var banner_image = '<img id="banner-image" src="'+e.target.result+'" border="0" style="width:525px;">'
						$('.banner-img').html(banner_image);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 1MB!");
			}
		}
	}
	
	// Manage product mapping availabily of box
    $('#product_mapping').click(function() {
        if($("#product_mapping").is(':checked')) {
            $('#product_mapping_div').fadeIn('slow');
            manage_product_grid();
            $('.products').show();
        } else {
            $('#product_mapping_div').fadeOut('slow');
            $('.products').hide();
        }
    });
    
    // Make a clone of header text on email template
    $("#header_text").keyup(function(){
		var header_text =  $("#header_text").val();
		$('.header_text').show();
		$('.header_text').html(header_text);
	});
	
	// Initialize description mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "edmmceEditor",
		height: '220px',
		width: '450px',
		theme_advanced_resizing: true,
		plugins: ['link'],
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				$('.edm_description').show();
				$('.edm_description').html(edm_description);
			});
		}
	});
	
	// Initialize footer content mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "footermceEditor",
		height: '220px',
		width: '450px',
		theme_advanced_resizing: true,
		plugins: ['link'],
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				$('.email_footer_text').show();
				$('.email_footer_text').html(edm_description);
			});
		}
	});
	
	// Manage products grid on template
	$("#no_products").on("blur", function(){
		manage_product_grid();
	});
	
	// Prepare products box inside email template
	function manage_product_grid() {
		var no_products = $('#no_products').val();
		var product_ids = new Array();
		var product_names = new Array();
		$('input.topSellingProducts[type=checkbox]').each(function () {
			if(this.checked) {
				product_ids.push($(this).val());
				product_names.push($(this).attr('data-name'));
			}
		});
		$('input.bottomSellingProducts[type=checkbox]').each(function () {
			if(this.checked) {
				product_ids.push($(this).val());
				product_names.push($(this).attr('data-name'));
			}
		});
		// append other product in template 
		for(var i=0;i<productIDArray.length;i++){
			product_ids.push(productIDArray[i]);
			product_names.push(productNAMEArray[productIDArray[i]]);
		}
		
		var product_count = product_ids.length;
		if(no_products <= product_ids.length){
			product_count = no_products;
		}
		var product_html = set_template_html(product_count,product_ids,product_names);
		$('.products').show();
		$('.products_grid').html(product_html);
		// convert products array to json
		var EmailProductsJson = JSON.stringify(EmailProducts);
		$('#edm_products_json').val(EmailProductsJson);
	}
	
	// Prepare html templates base on type
	function set_template_html(product_count,product_ids,product_names) {
		var template_type = $('#template_type').val();
		var product_html ='';
		if(template_type == 'grid_view') {
			var counter = 1;
			for(var i=0;i<product_count;i++) {
				if(counter == 1) {
					product_html += '<tr><td><table border="0" width="100%" cellpadding="0" cellspacing="0" class="style" style="text-align: center;"><tr>';
				}
				product_html += '<td class="brd border-bottom" style="width:32%;"><p style="text-align: center;"> <img width="131" height="71" src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"></p>';
				product_html += '<h4 style="text-align: center;"> <strong>'+product_names[i]+'</strong> </h4><div style="text-align: center;" class="price-grid">';
				product_html += '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="50%" style="text-align: right;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;">List Price</td><td width="50%" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;font-family: "Courier New", Courier, monospace;text-align: left;font-weight: 600;padding-left: 4%;letter-spacing: 0px;">$15</td></tr>';
				product_html += '<tr><td width="50%" style="text-align: right;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;" class="text-right">Price</td><td width="50%" class="price-new">$10</td></tr></table></div>';
				product_html += '<div class="clearfix"></div><div class="explore"><a href="{#VIEW_PRODUCT#}'+product_ids[i]+'">Explore More</a> </div></td>';
				if(counter == 1 || counter == 2) {
					product_html += '<td style="width:10px;">&nbsp;</td>';
				}	
				if(counter == 3) {
					product_html += '</tr></table></td></tr>';
					product_html += '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
					counter = 1;
				} else {
					counter++;
				}
				var ProductArray = {};
				ProductArray['product_id'] = product_ids[i];
				ProductArray['product_name'] = product_names[i];
				EmailProducts[i] = ProductArray;
			}
		} else {
			for(var i=0;i<product_count;i++) {
				product_html += '<tr><td><table class="style list" width="100%" style="text-align: center;"><tr>';
				product_html += '<td width="20%" class="vt"><img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"></td>';
				product_html += '<td width="60%" class="text-left vm"><h4 class="list-h" style="font-size: 14px;line-height: 15px!important; margin: 2px 0px 0px 0px;"> <strong>'+product_names[i]+'</strong> </h4>';
				product_html += '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="40%" style="text-align: left;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">List Price</td><td width="" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;font-family: "Courier New", Courier, monospace;text-align: left;font-weight: 600;padding-left: 0;letter-spacing: 0px;">$15</td></tr>';
				product_html += '<tr><td width="40%" style="text-align: left;font-family: Georgia, "Times New Roman", Times, serif;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">Price</td><td width="" class="price-new2">$10</td></tr></table></td>';
				product_html += '<td width="20%" class="pr2" style="vertical-align: middle;"><div class="explore"> <a href="{#VIEW_PRODUCT#}'+product_ids[i]+'">Explore More</a> </div></td></tr></table></td></tr><tr><td height="20" style="font-size:20px; line-height:20px;"></td></tr>';
				var ProductArray = {};
				ProductArray['product_id'] = product_ids[i];
				ProductArray['product_name'] = product_names[i];
				EmailProducts[i] = ProductArray;
			}
		}
		return product_html;
	}
	
	// Manage offers mapping selection
	var offer_images = <?php echo json_encode($selected_offers); ?>;
	$('#offers_mapping').click(function() {
        if($("#offers_mapping").is(':checked')) {
            $('#offers_mapping_div').fadeIn('slow');
            // prepare instore offers html
			prepare_offer_html();
        } else {
            $('#offers_mapping_div').fadeOut('slow');
            //$('.offers').hide();
        }
    });
    
    // Manage offer template widget
    $('.instore_offers').click(function() {
		var offer_id =  $(this).val();
        if($(this).is(':checked')) {
			// set offer image 
			var offer_img = $('#offer_img_'+offer_id).attr('src');
			offer_images[offer_id]  = offer_img;
        } else {
            delete offer_images[offer_id];
        }
        // prepare instore offers html
		prepare_offer_html();
		
    });
    
    // Prepare offer template html
    function prepare_offer_html() {
		// prepare instore offer html
        var offer_html = '';
        var counter = 1;
        var offer_image_count = 0;
        var selected_offers = {};
        jQuery.each( offer_images, function( i, val ) {
			if(val != undefined) {
				if(counter == 1) {
					offer_html += '<tr>';
				}
				offer_html += '<td align="left" style="width:40%;" class="brd"><p><img src="'+val+'"></p></td>';
				if(counter == 2) {
					offer_html += '</tr>';
				} else {
					counter++;
				}
				offer_image_count++;
				selected_offers[i] = val;
			}
		});
		// set visibility of offers template widget
		//~ if(offer_image_count > 0) {
			 //~ $('.offers').show();
		//~ } else {
			//~ $('.offers').hide();
		//~ }
		// append offer image html under template area
		$('#offers_images').html(offer_html);
		// convert offers array to json
		var instore_offer_json = JSON.stringify(selected_offers);
		$('#instore_offers_json').val(instore_offer_json);
	}
    
    // Manage email template from iFrame
	$('.get_template').click(function() {
		var iFrameDOM = $("iframe#templateIframe").contents();
		
		iFrameDOM.find('img[alt]').each(function() { $(this).removeAttr('alt'); });
		var data = iFrameDOM.find("#mail-template").html();
		data = data.replace(/(<button.*?>.*?<\/button>)/g,'');
		//console.log(data);
	});
	
	// Manage template selection
	$(document).delegate(".choose","click",function(e){
		var id = $(this).attr('id');
		$('#'+id+'_template').removeClass('dn');
		$('#choose-template').addClass('display_none');
		$('.email-template-box').removeClass('display_none');
		$('#template_type').val(id);
	});
    
    // Make a clone of product widget title on email template
    $("#product_grid_title").keyup(function(){
		var product_grid_title =  $("#product_grid_title").val();
		$('.product_widget_title').html('<h4>'+product_grid_title+'</h4>');
	});
	
</script>

