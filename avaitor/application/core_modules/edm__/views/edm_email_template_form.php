<div class="tab-pane active" id="edm_content_tab">
	<div class="form-wrapper clearboth">
		<div class="form-box">
			<label for="body">
				<?php //echo lang('email_template');?>
			</label>
			<div class="template-table">
				<div class="bgcolor ptb10 pull-left font12 theme-input formnew lh38">
					<div class="col-xs-2"><?php echo lang('from_email');?><i style="color:red;">*</i></div>
					<div class="col-xs-10 mpl0">
						<input id="email_from_name" name="email_from_name" placeholder="Nightowl" value="<?php echo isset($edm_template['email_from_name'])?$edm_template['email_from_name']: '';?>" class="form-control input-md required" type="text">
					</div>
					<div class="clearfix"></div>
					<div class="col-xs-2"><?php echo lang('email_subject');?><i style="color:red;">*</i></div>
					<div class="col-xs-10 mpl0">
						<input id="subject" name="subject" placeholder="Subject" value="<?php echo isset($edm_template['subject'])?$edm_template['subject']: '';?>" class="form-control input-md required" type="text">
						<?php
						echo form_hidden('email_id',(isset($edm_template['id'])?$edm_template['id']: ''));
						?>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="graycolor p20">
					<div class="mt-4 bgcolor3 dotted-view" style="width:100%; margin:0px auto;">	
						<?php
						//if(!empty($edm_template['body'])) {
							//echo $edm_template['body'];
						//} else {
							$grid_template_display  	=  (!empty($edm_data['template_type']) && $edm_data['template_type'] == 'grid_view') ? '' : 'dn';
							$list_template_display  	=  (!empty($edm_data['template_type']) && $edm_data['template_type'] == 'list_view') ? '' : 'dn';
							echo '<div id="grid_view_template" class="'.$grid_template_display.'">'.$grid_template_view.'</div>';
							echo '<div id="list_view_template" class="'.$list_template_display.'">'.$list_template_view.'</div>';
						//} ?>
					</div>
				</div>  
			</div>
		</div>					
	</div>
</div>
