<?php
$product_html = '';
// decode json into array
$products = (!empty($edm_products_json)) ? json_decode($edm_products_json) : '';
// prepare products grid html
if(!empty($products) && count($products)>0 && $product_mapping == 1) {
	$counter = 1;
	foreach($products as $key=>$val) {
		if($counter == 1) {
			$product_html .= '<table border="0" cellpadding="0" cellspacing="0" style="width:auto;border-spacing: 5px; border-collapse: separate; "><tr>';
		}
		$product_html .= '<td class="brd" style="width:33%; vertical-align:top; text-align:center; border: solid 1px #a3a3a3;"><h4 style="color: #54667a; line-height:17px;font-size: 13px;padding:4px 6px; font-weight:500; margin:0;min-height: 60px;">'.$val->product_name.'</h4>';
		$product_html .= '<p style="margin:0 ;"><img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"></p>';
		$product_html .= '<p style="color: #54667a; margin:0 0 10px;font-size: 12px;">Cock, Sprit, Fanta</p><div style="font-size:13px;"><span class="price-old" style="color: #a3a3a3;font-size: 11px;text-decoration: line-through;margin-right: 8px;">$80.00</span><span class="price-new" style="color: #758393;font-size: 13px"> $50.00</span></div>';
		$product_html .= '<a href="{#VIEW_PRODUCT#}'.encode($val->product_id).'"><button style="background: #ffed00;border: none;font-size: 12px;color: #54667a;line-height: 12px;margin-bottom: 8px;margin-top: 8px;padding: 6px 12px;border-radius: 4px;">More Details</button></a></td>';
		if($counter == 3) {
			$product_html .= '</tr></table>';
			$counter = 1;
		} else {
			$counter++;
		}
	}
}
// prepare offer html
$instoreoffers_html = '';
// decode json into array
$instore_offers = (!empty($instore_offers)) ? json_decode($instore_offers) : '';
// prepare products grid html
if(!empty($instore_offers) && count($instore_offers)>0) {
	$counter = 1;
	foreach($instore_offers as $key=>$val) {
		if($counter == 1) {
			$instoreoffers_html .= '<tr>';
		}
		$instoreoffers_html .= '<td align="left" style="width:40%;" class="brd"><p><img src="'.$val.'"></p></td>';
		if($counter == 2) {
			$product_html .= '</tr>';
			$counter = 1;
		} else {
			$counter++;
		}
	}
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<style type="text/css">
		body { font-family: arial; font-size: 14px;
		margin:0; padding:0; }
		table td table td { border-bottom: 1px solid
		#e7e7e7; padding:6px 12px; }
		</style>
	</head>
	<body>
		<table border="0" align="center" cellpadding="0"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; border:1px solid #ccc; "  cellspacing="0"  bgcolor="ffffff" class="bg_color">
			<tbody>
				<tr class="bgyellow p10" style="text-align:left">
					<td style="height:52px; vertical-align:middle; padding-left:10px; padding-bottom:10px; background: #ffed00">
						<img src="<?php echo site_url();?>uploads/edm_images/logo.png">
					</td>
				</tr>
				<tr>
					<td align="left"><table border="0" align="center" cellpadding="0" cellspacing="0" class="">
						<tbody>
							<tr>
								<td align="center" class="banner-img" style="<?php echo (!empty($email_banner_image)) ? '' : 'display: none;';?> border-style:none !important; border:0 !important;">
									<?php if(!empty($email_banner_image)) {?>
										<img id="banner-image" src="<?php echo site_url().'uploads/edm_images/'.$email_banner_image?>" border="0" style="width:525px;">
									<?php }?>
								</td>
							</tr>
							<tr>
								<td class='header_text' style="<?php echo (!empty($email_header_text)) ? '' : 'display: none;';?>padding:5px ; text-align:center;font-size: 16px;font-weight:bold;color:#000000;" >
									<?php echo (!empty($email_header_text)) ? $email_header_text : '&nbsp;';?>
								</td>
							</tr>
							<tr>
								<td class='edm_description' style="<?php echo (!empty($email_description)) ? '' : 'display: none;';?> padding:5px 0 0 5px;"><?php echo (!empty($email_description)) ? $email_description : '&nbsp;';?></td>
							</tr>
							<tr class="products" style="<?php echo (!empty($product_html)) ? '' : 'display: none;';?>">
								<td height="40" style="font-size:18px; font-weight:800; padding-left:2%; line-height:30px; background:#e9921b; font-family:'Courier New', Courier, monospace; color:#fff; ">Hot Deals&nbsp;</td>		
							</tr>
							<tr class="products" style="<?php echo (!empty($product_html)) ? '' : 'display: none;';?>">
								<td class="products_grid">
									<?php echo $product_html;?>
								</td>
							</tr>
							
							<tr class="offers" style="<?php echo (!empty($instoreoffers_html)) ? '' : 'display: none;';?>">
								<td height="40" style="font-size:18px; font-weight:800; padding-left:2%; line-height:30px; background:#758393; font-family:'Courier New', Courier, monospace; color:#fff; ">Special Offers&nbsp;</td>
							</tr>
							<tr class="offers" style="<?php echo (!empty($instoreoffers_html)) ? '' : 'display: none;';?>">
								<td>
									<table border="0" width="100%" cellpadding="0" cellspacing="0" class="style">
										<tbody id="offers_images">
											<?php echo $instoreoffers_html;?>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td height="15" style="font-size:15px; line-height:15px;">&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>         
				<td align="center" class="" style="border-style:none !important; display: block; border:0 !important; color:#fff; background:#656464; padding:10px 0; font-size:12px; line-height:24px;">     
					<p style="margin:0 0 5px;"> Copyright 2018 Nightowl </p>
					<p style="margin:0;">
						<a style="margin:0 5px;display: inline-block; text-align:center;background: #3e3d3d; border-radius: 100%;width: 30px;height: 30px;line-height: 30px;padding: 5px 0;">
							<img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/facebook.png" alt="" style="width:20px;margin:0 auto;" />
						</a> 
						<a style="margin:0 5px;display: inline-block; text-align:center;background: #3e3d3d; border-radius: 100%;width: 30px;height: 30px;line-height: 30px;padding: 5px 0;">
							<img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/instagram.png" style="width:20px;margin:0 auto;" alt="" />
						</a> 
						<a style="margin:0 5px;display: inline-block; text-align:center;background: #3e3d3d;border-radius: 100%;width: 30px;height: 30px;line-height: 30px;padding: 5px 0;">
							<img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/twitter.png" style="width:20px; margin:0 auto;" alt="" />
						</a>
					</p>
				</td>
			</tr>
		</tbody>
		</table>
	</body>
</html>	
