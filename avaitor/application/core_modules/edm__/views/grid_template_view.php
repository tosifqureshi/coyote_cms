<?php
$product_html = '';
// decode json into array
$product_json = '';
if(!empty($product_add_type) && $product_add_type == 1) {
	$product_json = json_decode($edm_products_json);
} else if(!empty($product_add_type) && $product_add_type == 2) {
	$product_json = json_decode($edm_sell_base_products_json);
}
$products = (!empty($product_json)) ? $product_json : '';
$no_products = (!empty($no_products)) ? $no_products : 0;
// prepare products grid html
if(!empty($products) && count($products)>0) {
	$counter = 1;
	$product_counts = 1;
	$total_product_count = count($products);
	foreach($products as $key=>$val) {
		if($no_products >= $product_counts) {
			if($counter == 1) {
				$product_html .= '<tr><td><table border="0" width="100%" cellpadding="0" cellspacing="0" style="text-align: center;" class="style"><tr>';
			}
			$product_html .= '<td class="border-bottom" style="width:32%;background-color: #fefefe !important;border: 1px solid #ccc;border-radius: 4px;padding: 0px -1px 10px 12px;vertical-align: top;text-align: left;"><p style="text-align: center;"> <img width="160" height="71" src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"> </p>';
			$product_html .= '<h4 style="text-align: center;color: #54667a;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;font-weight: 300;"> <strong>'.$val->product_name.'</strong> </h4><div style="text-align: center;" class="price-grid">';
			$product_html .= '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="50%" style="text-align: right;font-size: 12px;letter-spacing: 1px;">List Price</td><td width="50%" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;text-align: left;font-weight: 600;padding-left: 4%;letter-spacing: 0px;">$'.$val->product_price.'</td></tr>';
			$product_html .= '<tr><td width="50%" style="text-align: right;font-size: 12px;letter-spacing: 1px;" class="text-right">Price</td><td width="50%" style="color: #758393;font-size:15px;margin-right: 8px;text-align: left;font-weight: 600;padding-left: 4%;letter-spacing: 0px;">$'.$val->special_price.'</td></tr></table></div>';
			$product_html .= '<div class="clearfix"></div><div style="text-align: center;height: 20px;"><a style="color: #e78b28;font-size: 11px;" href=""{VIEW_PRODUCT}'.encode($val->product_id).'"">Explore More</a> </div></td>';
			if($counter == 1 || $counter == 2) {
				$product_html .= '<td style="width:10px;">&nbsp;</td>';
			}
			// setup additional coloumn in case of less then 3 TD
			if($total_product_count == 1 && $counter < 3) {
				$remain_td = 1;
				if($counter == 1) {
					$remain_td = 2;
				}
				for($k=0;$k<$remain_td;$k++) {
					$product_html .= '<td class="border-bottom" style="width:32%;">&nbsp;</td>';
					$product_html .= '<td style="width:10px;">&nbsp;</td>';
				}
				$product_html .= '</tr></table></td></tr>';
				$product_html .= '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
			}									
			if($counter == 3) {
				$product_html .= '</tr></table></td></tr>';
				$product_html .= '<tr><td style="height:10px; line-height:10px;">&nbsp;</td></tr>';
				$counter = 1;
			} else {
				$counter++;
			}
		}
		$product_counts++;
		$total_product_count--;
	}
	// add view more option
	$product_html .= '<tr style="border-bottom: 1px solid #f5eeee;"><td colspan="4"><a style="text-align: right;float: right;padding: 5px;color:#e78b28;" href="{VIEW_MORE_PRODUCT}">View more</a></td></tr>';
} ?>
<table align="center" border="0" width="550" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="border: 1px #ccc solid;">
	<tr style="background: #FFCB00;padding: 10px;">
	  <td style="height:52px; padding-left:10px;"><img src="<?php echo site_url();?>uploads/edm_images/logo.png" style="padding-top:10px;"></td>
	</tr>
	<tr>
		<td style="width:auto;">
			<table border="0" style="width:100%;" align="center" cellpadding="0" cellspacing="0" class=" ">
				<?php if((isset($action) && !empty($email_banner_image)) || (!isset($action))) { ?>
					<tr class="banner-img <?php (!empty($email_banner_image)) ? '' : 'dn';?>">
						<td align="center" class="section-img" style="border-style:none !important; display: block; border:0 !important;">
							<span class="h120">
								<?php
								if(!empty($email_banner_image)) { ?>
									<img id="banner-image" src="<?php echo site_url().'uploads/edm_images/'.$email_banner_image?>" border="0" style="max-width:550px;">
								<?php
								}?>
							</span>
						</td>
					</tr>
				<?php }?>
				<tr>
					<td height="20" style="font-size:20px; line-height:20px;"></td>
				</tr>
				<tr>
					<td class="header-text">
						<p style="font-size:16px;margin-left: 5px;">Dear <strong>{RECIPIENT_NAME}</strong>,</p>
					</td>
				</tr>
				<?php if((isset($action) && !empty($email_description)) || (!isset($action))) { ?>
					<tr class="edm_description_tr <?php echo !empty($email_description) ? '' : 'dn';?>">
						<td class="edm_description" style="border-style:none !important; display: block; border:0 !important;padding-top:10px;margin-left:5px;">
								<span class="h85">
									<?php echo (!empty($email_description)) ? $email_description : '';?>
								</span>
							
						</td>
					</tr>
				<?php }?>
				<tr>
					<td height="20" style="font-size:20px; line-height:20px;"></td>
				</tr>
				<?php
				if((isset($action) && !empty($product_grid_title) && !empty($product_html)) || (!isset($action))) {?>
					<tr class="product_widget_title_tr <?php echo !empty($product_grid_title) ? '' : 'dn';?>">
						<td class="product_widget_title" style="font-size:18px; font-weight:800;padding:15px; background:#e9921b; font-family:'Courier New', Courier, monospace; color:#fff;background-color:#ffcb00;text-align: center;">
							<?php 
							if(!empty($product_grid_title)) {
								echo '<h4 style="margin:0px;">'.$product_grid_title.'</h4>';
							} ?>
						</td>
					</tr>
					<tr>
						<td height="10" style="font-size: 10px; line-height: 10px;"></td>
					</tr>
				<?php } 
				
				if((isset($action) && !empty($product_html)) || (!isset($action))) { ?>
					<tr class="product_html_tr <?php echo (!empty($product_html)) ? '' : 'dn';?>">
						<td>
							<table border="0" width="100%" cellpadding="0" cellspacing="0" class="style products_grid">
								<?php
								if(!empty($product_html) && isset($action)) {
									echo $product_html;
								} ?>
							</table>
						</td>
					</tr>
					<tr class="product_html_tr <?php echo (!empty($product_html)) ? '' : 'dn';?>">
						<td height="40" style="font-size:40px; line-height:40px;"></td>
					</tr>
					<tr>
						<td style="width:100%; background:#ffcb00; height:1px;"></td>
					</tr>
				<?php 
				}?>
			<tr>
				<td height="20" style="font-size:20px; line-height:20px;"></td>
			</tr>
			<tr>
				<td style="width:100%; padding:10px 5px;">
					<p> <strong>Warm Regards,</strong></p>
					<p>Nightowl Team</p>
				</td>
			</tr>
			<?php if((isset($action) && !empty($email_footer_text)) || (!isset($action))) { ?>
				<tr class="email_footer_text_tr <?php echo !empty($email_footer_text) ? '' : 'dn';?>">
					<td class="email_footer_text" style="border-style:none !important; display: block; border:0 !important;text-align: center;">
						<span class="h85">
							<?php echo (!empty($email_footer_text)) ? $email_footer_text : '';?>
						</span>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td height="20" style="font-size:20px; line-height:20px;"></td>
			</tr>
			<tr>
				<td align="center" class="" style="border-style:none !important; display: block; padding:6px; border:0 !important; color:#fff; background:#989898; font-size:12px; line-height:24px;">Copyright 2018 Nightowl</td>
				<img style="vertical-align: middle; position: absolute; opacity:0; left:0;" src="{GIF_IMAGEURL}"/>
			</tr>
		</table>
	</td>
</tr>
</table>
