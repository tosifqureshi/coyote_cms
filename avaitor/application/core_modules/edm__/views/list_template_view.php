<?php
$product_html = '';
// decode json into array
$product_json = '';
if(!empty($product_add_type) && $product_add_type == 1) {
	$product_json = json_decode($edm_products_json);
} else if(!empty($product_add_type) && $product_add_type == 2) {
	$product_json = json_decode($edm_sell_base_products_json);
}
$products = (!empty($product_json)) ? $product_json : '';
$no_products = (!empty($no_products)) ? $no_products : 0;
// prepare products grid html
if(!empty($products) && count($products)>0) {
	$product_counts = 1;
	foreach($products as $key=>$val) {
		if($no_products >= $product_counts) {
			$product_html .= '<tr><td><table class="style list" width="100%" style="text-align: center;"><tr>';
			$product_html .= '<td width="20%"><img src="http://mst.cdnsolutionsgroup.com:84/coyotev3/uploads/edm_images/img2.jpg"></td>';
			$product_html .= '<td width="60%" style="vertical-align: top;"><h4 style="font-size: 14px;line-height: 15px!important; margin: 2px 0px 0px 0px;padding-left: 5%;color: #54667a;font-weight: 300;float: left;"> <strong>'.$val->product_name.'</strong> </h4>';
			$product_html .= '<table cellpadding="0" cellspacing="0" align="center" width="100%"><tr><td width="40%" style="text-align: left;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">List Price</td><td width="" style="color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;text-align: left;font-weight: 600;padding-left: 0;letter-spacing: 0px;">$'.$val->product_price.'</td></tr>';
			$product_html .= '<tr><td width="40%" style="text-align: left;font-size: 12px;letter-spacing: 1px;padding-left: 5%;">Price</td><td width="" style="color: #ffcb00;font-size: 32px;font-weight: 600;line-height: 30px;letter-spacing: -3px;padding-left: 0;text-align: left;">$'.$val->special_price.'</td></tr></table></td>';
			$product_html .= '<td width="20%" style="vertical-align: middle;"><div style="text-align: center;"> <a href="{VIEW_PRODUCT}'.encode($val->product_id).'" style="color: #e78b28;font-size: 11px;">Explore More</a> </div></td></tr></table></td></tr><tr><td height="20" style="font-size:20px; line-height:20px;"></td></tr>';
		}
		$product_counts++;
	}
	// add view more option
	$product_html .= '<tr style="border-bottom: 1px solid #f5eeee;"><td colspan="4"><a style="text-align: right;float: right;padding: 5px;color:#e78b28;" href="{VIEW_MORE_PRODUCT}">View more</a></td></tr>';
} ?>

<table align="center" border="0" width="550" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="border: 1px #ccc solid;">
	<tr style="background: #FFCB00;padding: 10px;">
	  <td style="height:52px; padding-left:10px;"><img src="<?php echo site_url();?>uploads/edm_images/logo.png" style="padding-top:10px;"></td>
	</tr>
	<tr>
		<td style="width:auto; padding:10px;">
			<table border="0" style="width:100%;" align="center" cellpadding="0" cellspacing="0" class=" ">
				<?php if((isset($action) && !empty($email_banner_image)) || (!isset($action))) { ?>
					<tr class="banner-img <?php (!empty($email_banner_image)) ? '' : 'dn';?>">
						<td align="center" class="section-img" style="border-style:none !important; display: block; border:0 !important;">
							<span class="h120">
								<?php
								if(!empty($email_banner_image)) { ?>
									<img id="banner-image" src="<?php echo site_url().'uploads/edm_images/'.$email_banner_image?>" border="0" style="max-width:550px;">
								<?php
								}?>
							</span>
						</td>
					</tr>
				<?php }?>	
				<tr>
					<td height="20" style="font-size:20px; line-height:20px;"></td>
				</tr>
				<tr>
					<td class="header-text">
						<p style="font-size:16px;margin-left: 5px;">Dear <strong>{RECIPIENT_NAME}</strong>,</p>
					</td>
				</tr>
				<?php if((isset($action) && !empty($email_description)) || (!isset($action))) { ?>
					<tr class="edm_description_tr <?php echo !empty($email_description) ? '' : 'dn';?>">
						<td class="edm_description" style="border-style:none !important; display: block; border:0 !important;padding-top:10px;margin-left:5px;">
								<span class="h85">
									<?php echo (!empty($email_description)) ? $email_description : '';?>
								</span>
							
						</td>
					</tr>
				<?php }?>
				<tr>
					<td height="20" style="font-size:20px; line-height:20px;"></td>
				</tr>
				<?php
				if((isset($action) && !empty($product_grid_title) && !empty($product_html)) || (!isset($action))) { ?>
					<tr class="product_widget_title_tr <?php echo !empty($product_grid_title) ? '' : 'dn';?>">
						<td class="product_widget_title" style="font-size:18px; font-weight:800;padding:15px; background:#e9921b; font-family:'Courier New', Courier, monospace; color:#fff;background-color:#ffcb00;text-align: center;">
							<?php 
							if(!empty($product_grid_title)) {
								echo '<h4 style="margin:0px;">'.$product_grid_title.'</h4>';
							} ?>
						</td>
					</tr>
					<tr>
						<td height="20" style="font-size:20px; line-height:20px;"> </td>
					</tr>
				<?php }
				if((isset($action) && !empty($product_html)) || (!isset($action))) { ?>
					<tr class="product_html_tr <?php echo (!empty($product_html)) ? '' : 'dn';?>">
						<td>
							<table class="products_grid" width="100%">
								<?php
								if(!empty($product_html) && isset($action)) {
									echo $product_html;
								} ?>
							</table>
						</td>
					</tr>
				<?php 
				} ?>	
				<tr>
					<td style="width:100%; background:#ffcb00; height:1px;"></td>
				</tr>
				<tr>
					<td height="20" style="font-size:20px; line-height:20px;"></td>
				</tr>
				<tr>
					<td style="width:100%; padding:10px 5px;">
						<p> <strong>Warm Regards,</strong></p>
						<p>Nightowl Team</p>
					</td>
				</tr>
				<?php if((isset($action) && !empty($email_footer_text)) || (!isset($action))) { ?>
					<tr class="email_footer_text_tr <?php echo !empty($email_footer_text) ? '' : 'dn';?>">
						<td class="email_footer_text" style="border-style:none !important; display: block; border:0 !important;text-align: center;">
							<span class="h85">
								<?php echo (!empty($email_footer_text)) ? $email_footer_text : '';?>
							</span>
						</td>
					</tr>
				<?php } ?>
				<tr>
					<td height="20" style="font-size:20px; line-height:20px;"></td>
				</tr>
				<tr>
					<td align="center" class="" style="border-style:none !important; display: block; padding:6px; border:0 !important; color:#fff; background:#989898; font-size:12px; line-height:24px;">Copyright 2018 Nightowl</td>
					<img style="vertical-align: middle; position: absolute; opacity:0; left:0;" src="{GIF_IMAGEURL}"/>
				</tr>
			</table>
		</td>
	</tr>
</table>
