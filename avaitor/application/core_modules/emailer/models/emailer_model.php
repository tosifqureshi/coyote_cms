<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Avaitor
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Avaitor
 * @author    Avaitor Dev Team
 * @copyright Copyright (c) 2011 - 2012, Avaitor Dev Team
 * @license   http://guides.ciAvaitor.com/license.html
 * @link      http://ciAvaitor.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Emailer Model
 *
 * @package    Avaitor
 * @subpackage Modules_Emailer
 * @category   Model
 * @author     Avaitor Dev Team
 * @link       http://guides.ciAvaitor.com/helpers/file_helpers.html
 *
 */
class Emailer_model extends BF_Model
{


	/**
	 * Name of the table
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $table = 'email_queue';

	/**
	 * Name of the primary key
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $key = 'id';

	/**
	 * Use soft deletes or not
	 *
	 * @access protected
	 *
	 * @var bool
	 */
	protected $soft_deletes = FALSE;

	/**
	 * The date format to use
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $date_format = 'datetime';

	/**
	 * Set the created time automatically on a new record
	 *
	 * @access protected
	 *
	 * @var bool
	 */
	protected $set_created = FALSE;

	/**
	 * Set the modified time automatically on editing a record
	 *
	 * @access protected
	 *
	 * @var bool
	 */
	protected $set_modified = FALSE;

}//end class
