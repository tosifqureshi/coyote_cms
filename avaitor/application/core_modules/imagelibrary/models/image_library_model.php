<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Competition Model
 *
 * The central way to access and perform CRUD on Competition.
 *
 * @package    Avaitor
 * @subpackage Image_library_model
 * @category   Models
 * @author     CDN Team
 * @link       http://www.cdnsolutionsgroup.com
 */
class Image_library_model extends BF_Model
{

	function __construct() {
		
        parent::__construct();    
        $this->read_db  = $this->load->database('read',TRUE); //Loading read database.
		$this->mssql_db = $this->load->database('mssql',TRUE); //Loading read database.
       // define tables
		$this->image_library_table = 'ava_image_library';
	}
    
	public function save_image($data) {
		$data['created_at'] = date('Y-m-d H:i:s');
		$this->read_db->insert($this->image_library_table, $data);
		return $this->read_db->insert_id();
	}

	public function get_all_image() {

		$this -> read_db -> select('*');
		$result = $this -> read_db -> get($this->image_library_table);
		return $result -> result_array();
	}

	
	
}//end User_model
