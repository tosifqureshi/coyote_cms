
 <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="<?=base_url();?>avaitor/public/js/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="<?=base_url();?>avaitor/public/js/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>avaitor/public/js/plupload/jquery.ui.plupload/jquery.ui.plupload.js"></script>


<div class="row">
    <div class="col-12 chit-chat-layer1">
      <div class="card">
        <div class="card-body">

<div class="model-section">
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#uploadFilesModel">Upload Files</button>
</div>
<br>

<div class="image-section">
<?php
  $image_path = base_url().'uploads/image_library';

  if(!empty($get_all_images)){
    echo "<table id='image_table' class='table table-striped'><thead><tr><th>Sr. No</th><th>Name</th><th>View</th></tr></thead>"; 
    $i = 1;
    foreach ($get_all_images as $value) {

      $url = $image_path.'/'.$value['name'];
        echo "<tr><td>" . $i . "</td>
        <td>" . $value['name'] . "</td>
        <td><img id='image_src' data-toggle='modal' data-target='#showImageModel' data-name=" . $value['name'] . " src=". $url ." width='50'></td>
        </tr>"; 
        $i++;
    }

    echo "</table>"; 

  }

?>
</div>
            <!-- Modal -->
            <div id="uploadFilesModel" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title">Upload File</h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                      <div id="uploader">
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal close-->


             <!-- Modal -->
            <div id="showImageModel" class="modal fade" role="dialog">
              <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title"></h5>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                      <div id="image-body">
                      <img src="">
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal close-->


      </div>
    </div>
  </div>
</div>

<script>
  jQuery(document).ready(function() {
    jQuery('#image_table').DataTable();

  $('#uploadFilesModel').on('hidden.bs.modal', function () {
    location.reload();
  });

  $("#image_table").on("click", "img#image_src", function(){
      var image_src = $(this).attr('src');
      var image_name = $(this).attr('data-name');
      $('#showImageModel .modal-body #image-body img').attr("src" , image_src);
      $('#showImageModel h5.modal-title').text(image_name);
  });

  });

  function show_image_library(){
    jQuery('#image_library_form').toggle();
  }

// Initialize the widget when the DOM is ready

  $("#uploader").plupload({
    // General settings
    runtimes : 'html5,flash,silverlight,html4',
    url : "<?=base_url();?>admin/settings/imagelibrary/uploadtoserver",
    // User can upload no more then 20 files in one go (sets multiple_queues to false)
    max_file_count: 20,
    
    chunk_size: '1mb',
    // Resize images on clientside if we can
    resize : {
      width : 200, 
      height : 200, 
      quality : 90,
      crop: true // crop to exact dimensions
    },
    
    filters : {
      // Maximum file size
      max_file_size : '1000mb',
      // Specify what files to browse for
      mime_types: [
        {title : "Image files", extensions : "jpg,gif,png"},
        {title : "Zip files", extensions : "zip"}
      ]
    },
    // Rename files by clicking on their titles
    rename: true,
    
    // Sort files
    sortable: true,
    // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
    dragdrop: true,
    // Views to activate
    views: {
      list: true,
      thumbs: true, // Show thumbs
      active: 'thumbs'
    },
    // Flash settings
    flash_swf_url : '/plupload/js/Moxie.swf',
    // Silverlight settings
    silverlight_xap_url : '/plupload/js/Moxie.xap'
  });

  // Handle the case when form was submitted before uploading has finished
/*  $('#image_library_form').submit(function(e) {
    // Files in queue upload them first
    if ($('#uploader').plupload('getFiles').length > 0) {
      // When all files are uploaded submit form
      $('#uploader').on('complete', function() {
        $('#image_library_form')[0].submit();
      });
      $('#uploader').plupload('start');
    } else {
           alert("You must have at least one file in the queue.");
    }
    return false; // Keep the form from submitting
  });*/


</script>



