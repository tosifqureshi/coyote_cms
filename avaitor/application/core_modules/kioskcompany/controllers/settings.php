<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kioskcompany Controller
 *
 * Manages the kiosk company functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Kioskcompany
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('kioskcompany');
		$this->load->library('form_validation');
		$this->load->model('kioskcompany/Kioskcompany_model', 'kioskcompany_model');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the kiosk company listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
	
		// get listing
		$companies = $this->kioskcompany_model->get_records();
		Template::set('companies',$companies);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage competition form
	 * @input : competition_id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_data()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/kioskcompany');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_data($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/kioskcompany');
			}
		}
		$data['company_title']	= lang('add_company');
		if($id) {
			$company = $this->kioskcompany_model->get_records($id);	
			if(empty($company)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/kioskcompany');
			}
			$data['company_title']	= lang('edit_company');
			$data['id']				= encode($id);
			$data['name']	  		= $company->name;		
			$data['email']  		= $company->email;		
			$data['phone_number']   = $company->phone_number;	
			$data['status']		    = $company->status;
			$data['created_at']	    = $company->created_at;	
		}
		
		Template::set('data',$data);
		Template::set_view('company_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store data in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_data($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
		}
		// apply validation rules for input params	
		$this->form_validation->set_rules('name', 'lang:name', 'trim|required');
		$this->form_validation->set_rules('email', 'lang:email', 'trim|required');
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		// set post values in store bundle
		$data['name']    = $this->input->post('name');
		$data['email']  = $this->input->post('email');
		$data['phone_number']   = $this->input->post('phone_number');
		$data['created_by']    = $this -> session -> userdata('user_id');
		// add or update data into db
		$id = $this->kioskcompany_model->save_data($data);
		return $id;
	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Update status of company
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->kioskcompany_model->save_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/kioskcompany');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove company from db
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$id = decode($id);
		$id = $this->kioskcompany_model->remove_record($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/kioskcompany');
	}

}//end Settings

// End of Admin kiosk company Controller
/* Location: ./application/core_modules/kioskcompany/controllers/settings.php */
