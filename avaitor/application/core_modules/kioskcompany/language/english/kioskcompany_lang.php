<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['kioskcompany']		= 'Company Management';
$lang['add_company']		= 'Add New Company';
$lang['edit_company']		= 'Edit Company';
$lang['sno']				= 'S.No.';
$lang['name']				= 'Company Name';
$lang['email']				= 'Company Email';
$lang['phone_no']		    = 'Phone Number';
$lang['created_at']			= 'Created At';
$lang['status']				= 'Status';
$lang['delete']				= 'Delete';
$lang['insert_msg']			= 'Successfully inserted';
$lang['update_msg']			= 'Successfully updated';
$lang['update_status']		= 'successfully change status';
$lang['delete_msg']			= 'Successfully removed';
$lang['form_save']			= 'Save';
$lang['form_cancel']		= 'Cancel';

