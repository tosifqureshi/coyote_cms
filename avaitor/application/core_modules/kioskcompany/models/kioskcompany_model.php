<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Kioskcompany Model
 *
 * The central way to access and perform CRUD on kiosk company.
 *
 * @package    Avaitor
 * @subpackage Modules_Kioskcompany
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Kioskcompany_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        // define tables
		$this->kiosk_companies_table = 'kiosk_companies';
    }

	/**
	 * Get listing of company log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_records($id = 0) {
		
		$this -> read_db -> select('*');
		if ($id) {
			$this -> read_db -> where("id", $id);
			$result = $this -> read_db -> get($this->kiosk_companies_table);
			return $result -> row();
		}
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->kiosk_companies_table);
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation of data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->kiosk_companies_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->kiosk_companies_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Function to remove entry from db
	 * @input : id
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	public function remove_record($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->kiosk_companies_table); 
			$return = true;
		}
		return $return;
	}
	

}//end Kioskcompany_model
