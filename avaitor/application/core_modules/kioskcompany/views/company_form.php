<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $company_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					$id = isset($id) ? $id : '';
					echo form_open_multipart('admin/settings/kioskcompany/create/'.$id,'id="add_company_form" class="form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Company name input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('name');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'name','id'=>'name', 'value'=> isset($name) ? $name : '' , 'class'=>'form-control form-control-line required');
									echo form_input($data);
									?>
								</div>
								<!-- Company name input end here -->
								
								<!-- Company email input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('email');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'email','id'=>'email', 'value'=> isset($email) ? $email : '' , 'class'=>'required email form-control form-control-line');
									echo form_input($data);
									?>
								</div>
								<!-- Company email input end here -->
								
								<!-- Company phone number input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('phone_no');?></label>
									<?php
									$data = array('name'=>'phone_number','id'=>'phone_number', 'value'=> isset($phone_number) ? $phone_number : '' , 'class'=>'form-control form-control-line','maxlength'=>15);
									echo form_input($data);
									?>
								</div>
								<!-- Company phone number input end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/kioskcompany' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
							
<script type="text/javascript">
	$(document).ready(function() {
		$("#add_company_form").validate({
			
		});
	});
	
	$( "#add_company_form" ).submit(function( event ) {
		
		return;
		event.preventDefault();
	});
	
	
</script>


