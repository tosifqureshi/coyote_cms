<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('kioskcompany') ?></h4>
				<a href="<?php echo site_url("admin/settings/kioskcompany/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_company'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<div class="table-responsive m-t-40">
					<table id="company_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('name'); ?></th>
								<th><?php echo lang('email'); ?></th>
								<th><?php echo lang('phone_no'); ?></th>
								<th><?php echo lang('created_at'); ?></th>
								<th><?php echo lang('status'); ?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($companies) && is_array($companies) && count($companies)>0):
								foreach($companies as $company) :
									// set encoded id
									$id = encode($company['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
											<a href="<?php echo site_url("admin/settings/kioskcompany/create/".$id);?>">
												<?php echo (strlen($company['name']) > 20) ? substr($company['name'],0,20).'...' :  $company['name'];?>
											</a>
										</td>
										<td><?php echo $company['email'];?></td>
										<td><?php echo $company['phone_number'];?></td>
										<td><?php echo date('F j, Y',strtotime($company['created_at']));?></td>
										<td>
											<?php
											// set change status url
											$status_url = site_url("admin/settings/kioskcompany/change_status/".$id.DIRECTORY_SEPARATOR.encode($company['status']));
											if($company['status'] == '0') { ?>
												<div style="display:none"><?php echo $company['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
												<?php
											} else {
												?>
												<div style="display:none"><?php echo $company['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
											} ?>
										</td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/kioskcompany/delete/".$id);?>')"><i class="fa fa-trash"></i></a>
										</td>
										
									</tr>
									
								<?php 
								$i++;
								endforeach ; 
							endif; ?>
						</tbody>
					</table>
				</div>		
			</div>	
		</div>	
	</div>	
</div>	

<script>
	$(document).ready(function() {
		$('#company_table').DataTable();
	});
</script>

