<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kiosklicence Controller
 *
 * Manages the kiosk licensing functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Kiosklicence
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('kiosklicence');
		$this->load->library('form_validation');
		$this->load->model('kiosklicence/Kiosklicence_model', 'kiosklicence_model');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the kiosk licence requests listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index($request_type=0)
	{
		// get listing
		$requests = $this->kiosklicence_model->get_requests(0,decode($request_type));
		Template::set('requests',$requests);
		Template::set('request_type',$request_type);
		Template::set_view('listing');
		Template::render();

	}//end index()
	
	//--------------------------------------------------------------------

	/**
	 * Update status of device address
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->deviceaddress_model->save_device_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/deviceaddress');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Generate licence key for request
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function generate_licence($id='') {
		if(!empty($id)) {
			$id = decode($id);
			// get licence key
			$license_key = $this->prepare_license_key();
			// set update data
			$data['id'] = $id;
			$data['licence_key'] = $license_key;
			$data['status'] = 2;
			$id = $this->kiosklicence_model->update_request($data);
			Template::set_message(lang('key_create_msg'),'success');
		}
		redirect('admin/settings/kiosklicence');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove licence key request
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$id = decode($id);
		$id = $this->kiosklicence_model->remove_request($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/kiosklicence');
	}
	
	/**
	* Generate a License Key.
	* Optional Suffix can be an integer or valid IPv4, either of which is converted to Base36 equivalent
	* If Suffix is neither Numeric or IPv4, the string itself is appended
	*
	* @param   string  $suffix Append this to generated Key.
	* @return  string
	*/
	function prepare_license_key($suffix = null) {
		// Default tokens contain no "ambiguous" characters: 1,i,0,o
		if(isset($suffix)){
			// Fewer segments if appending suffix
			$num_segments = 3;
			$segment_chars = 6;
		}else{
			$num_segments = 4;
			$segment_chars = 5;
		}
		$tokens = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
		$license_string = '';
		// Build Default License String
		for ($i = 0; $i < $num_segments; $i++) {
			$segment = '';
			for ($j = 0; $j < $segment_chars; $j++) {
				$segment .= $tokens[rand(0, strlen($tokens)-1)];
			}
			$license_string .= $segment;
			if ($i < ($num_segments - 1)) {
				$license_string .= '-';
			}
		}
		// If provided, convert Suffix
		if(isset($suffix)){
			if(is_numeric($suffix)) {   // Userid provided
				$license_string .= '-'.strtoupper(base_convert($suffix,10,36));
			}else{
				$long = sprintf("%u\n", ip2long($suffix),true);
				if($suffix === long2ip($long) ) {
					$license_string .= '-'.strtoupper(base_convert($long,10,36));
				}else{
					$license_string .= '-'.strtoupper(str_ireplace(' ','-',$suffix));
				}
			}
		}
		return $license_string;
	}
	
	//------------------------------------------------------------------

	/**
	 * Function to check existance of licence key
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	public function validateKey() {
		$data  = $this->input->post();
		$licence_key = $data['licence_key'];
		$response = false;
		if(!empty($licence_key)) {
			$is_exist = $this->kiosklicence_model->check_licence_existance($licence_key);
			$response = (!empty($is_exist)) ? true : false;
		}
		echo json_encode(array('return'=>$response,'msg'=>''));die; 
	}
	
	//------------------------------------------------------------------

	/**
	 * Function to manage licence creation
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	public function create() {
		// get companies
		$data['companies']	= get_kiosk_companies();
		// set template params
		Template::set('data',$data);
		Template::set_view('licence_form');
		Template::render();	
	}
	
	//------------------------------------------------------------------

	/**
	 * Function used to insert generated licence in db
	 * @input : null
	 * @return: void
	 * @access: public
	 */
	function add_licence(){
		
        //set form post params
		$licence_key = $this->input->post('licence_key');
        $licence_expire = $this->input->post('licence_expire');
        $success = false;
        $msg = 'licence_add_error';
        // manage licence store procedure
        if (!empty($licence_key)) {
            foreach ($licence_key as $keys => $licence) {

                if (!empty($licence)) {
                    $licenceKey = $licence_key[$keys];
                    $licenceExpire = $licence_expire[$keys];
					if(!empty($licenceExpire)){
						$license_data = array(
							'licence_key' => $licenceKey,
							'expired_in_year' => $licenceExpire,
							'created_at' => date('Y-m-d H:i:s')
						);
						$result = $this->kiosklicence_model->save_licence_data($license_data);
					}
                }
            }
            $success = true;
            $msg = lang('added_licence_success');
        }
        // return with response message
        $success = ($success == false) ? 'error' : 'success';
		Template::set_message( $msg,$success);
		redirect('admin/settings/kiosklicence');
    }
    
    //------------------------------------------------------------------

	/**
	 * Function used to fetch company's tills
	 * @input : null
	 * @return: void
	 * @access: public
	 */
    function get_company_tills() {
		$company_id = $this->input->post('company_id');
		$kiosk_type = $this->input->post('kiosk_type');
		// get only those tills whos licence not created yet
		$till_records = $this->kiosklicence_model->get_non_licence_tills($company_id,$kiosk_type);
		// set expiry year array
		$expire_time = array('1'=>'1 Year','2'=>'2 Year','3'=>'3 Year','4'=>'4 Year');
		// prepare licence suggession html
		$till_html = '';
		foreach($till_records as $val) {
			// get licence key
			$license_key = $this->prepare_license_key();
			$till_html .= '<tr>';
			$till_html .= '<td><input type="text" readonly name="till_name" value="'.$val->till_name.'" class="form-control readonly_input width100"></td>';
			$till_html .= '<td><input type="text" name="mac_id" id="mac_id_'.$val->id.'" class="form-control required licence_input" row_id="'.$val->id.'"></td>';
			$till_html .= '<td>';		
			$till_html .= '<div class="encoded_licence_textarea_'.$val->id.'">NA</div>';	
			$till_html .= '<input type="hidden" id="encoded_licence_'.$val->id.'">';	
			$till_html .= '</td>';
			$till_html .= '<td><select  name="licence_expire" id="licence_expire_'.$val->id.'" class="form-control required width80">';
			foreach($expire_time  as $key => $value){
				$till_html .= '<option value='.$key.'>'.$value.'</option>';
			} 
			$till_html .= '</select></td>';
			$till_html .= '<td><span class="generateTokenSpan_'.$val->id.'"><button id="generate_token_btn_'.$val->id.'" class="btn default" row_id = "'.$val->id.'" type="button" onclick="generate_licence('.$val->id.')">Generate Licence</button></span>';
			$till_html .= '<span class=" dn decodeTokenSpan_'.$val->id.'"><button id="decode_token_btn_'.$val->id.'" class="btn default" row_id = "'.$val->id.'" type="button" onclick="decode_licence('.$val->id.')">Decode Licence</button></span></td>';
			$till_html .= '</tr>';
		}			
		echo json_encode(array('result'=>true,'tills'=>$till_html));die;
	}
	
	//------------------------------------------------------------------

	/**
	 * Function used to generate secure JWT licence code
	 * @input : null
	 * @return: void
	 * @access: public
	 */
    function generate_licence_token() {
		
		// set input post params
		$till_id = $this->input->post('till_id');
		$mac_id  = $this->input->post('mac_id');
		$expired_in_year  = $this->input->post('expire_at');
		$kiosk_type  = $this->input->post('kiosk_type');
		$result  = FALSE;
		$action  = 'encode';
		$msg = lang('licence_add_error');
		if(!empty($till_id) && !empty($mac_id)) {
			// fetch till details
			$till_data = $this->kiosklicence_model->get_till_data($till_id);
			// set till data
			$till_id = (!empty($till_data->id)) ? $till_data->id : 0;
			$company_name = (!empty($till_data->company_name)) ? $till_data->company_name : 0;
			$outlet_name = (!empty($till_data->outlet_name)) ? $till_data->outlet_name : 0;
			$expired_in_year = (!empty($expired_in_year)) ? $expired_in_year : 1;
			$expire_at = date('Y-m-d',strtotime('+ '.$expired_in_year.'year'));
			$kiosk_type_name = lang('app_type_'.$kiosk_type);
			// prepare token json
			$token_json = '{"till_id":"'.$till_id.'","company_name":"'.$company_name.'","outlet_name":"'.$outlet_name.'","mac_id":"'.$mac_id.'","expire_at":"'.$expire_at.'","kiosk_type":"'.$kiosk_type.'","kiosk_type_name":"'.$kiosk_type_name.'"}';
			// include JWT library
			require_once($_SERVER['DOCUMENT_ROOT'].'/coyotev3/jwt_token/token_encryption.php');
			if(!empty($encoded_licence)) {
				$encoded_licence = (string) $encoded_licence;
				$encoded_licence_textarea = '<textarea id="access_code_'.$mac_id.'" licence_code = "'.$mac_id.'" rows="12" class="form-control licence-textarea crp" readonly>'.$encoded_licence.'</textarea>';
				$license_data = array(
					'till_id' => $till_id,
					'mac_id' => $mac_id,
					'access_code' => $encoded_licence,
					'expired_in_year' => $expired_in_year,
					'kiosk_type' => $kiosk_type,
					'expire_at' => $expire_at,
					'created_at' => date('Y-m-d H:i:s')
				);
				$result = $this->kiosklicence_model->save_licence_data($license_data);
				$result  = TRUE;
				$msg = lang('added_licence_success');
			} 
		}
		echo json_encode(array('result'=>$result,'encoded_licence'=>(isset($encoded_licence) ? $encoded_licence : ''),'encoded_licence_textarea'=>(isset($encoded_licence_textarea) ? $encoded_licence_textarea : ''),'msg'=>$msg));die;
	}
	
	/**
	 * Function used to decode encrypted licence key
	 * @input : null
	 * @return: void
	 * @access: public
	 */
    function decode_licence() {
		// set input post params
		$encoded_token = $this->input->post('encoded_token');
		$result  = FALSE;
		$action  = 'decode';
		if(!empty($encoded_token)) {
			// include JWT library
			require_once($_SERVER['DOCUMENT_ROOT'].'/coyotev3/jwt_token/token_encryption.php');
			$decoded_licence = (string) $decoded_licence;
			$result  = TRUE;
		}
		echo json_encode(array('result'=>$result,'decoded_licence'=>(isset($decoded_licence) ? $decoded_licence : '')));die;
	}
}//end Settings

// End of Admin Kiosk Licence Controller
/* Location: ./application/core_modules/kiosklicence/controllers/settings.php */
