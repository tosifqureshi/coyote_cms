<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Deviceaddress Model
 *
 * The central way to access and perform CRUD on Kiosklicence.
 *
 * @package    Avaitor
 * @subpackage Modules_Kiosklicence
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Kiosklicence_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        // define tables
		$this->kiosk_licence_requests_table = 'kiosk_licence_keys';
		$this->kiosk_till_log_table  = 'kiosk_till_log';
		$this->kiosk_companies_table = 'kiosk_companies';
		$this->kiosk_outlets_table   = 'kiosk_outlets';
		
    }

	/**
	 * Get listing of kiosk licence log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_requests($id = 0,$request_type = 0) {
		
		$this -> read_db -> select('req.*,till.till_name,cmp.name as company_name,outl.outlet_name');
		$this -> read_db ->from($this->kiosk_licence_requests_table.' req');
		$this -> read_db ->join($this->kiosk_till_log_table.' till','till.id = req.till_id');
		$this -> read_db -> join($this->kiosk_companies_table.' cmp','cmp.id = till.company_id');
		$this -> read_db -> join($this->kiosk_outlets_table.' outl','outl.id = till.outlet_id');
		if($request_type == 1) {
			// fetch pending requests
			$this -> read_db -> where("req.kiosk_type", $request_type);
		} else if($request_type == 2) {
			// fetch completed requests
			$this -> read_db -> where("req.kiosk_type", $request_type);
		}
		$this -> read_db -> order_by('req.created_at', 'desc');
		$result = $this -> read_db -> get();
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage updation of request data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function update_request($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->kiosk_licence_requests_table, $data);
			return $data['id'];
		}
		return false;
	}
	
	/**
	 * Function to remove kiosk licence entry from db
	 * @input : id
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	public function remove_request($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->kiosk_licence_requests_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Check existance of licence key
	 * @input : licence_key
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function check_licence_existance($licence_key = 0) {
		
		$this -> read_db -> select('id');
		$this -> read_db -> where("licence_key", $licence_key);
		$result = $this -> read_db -> get($this->kiosk_licence_requests_table);
		return $result -> row();
		
	}
	
	/**
	 * Function to manage insertion and updation of licence data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_licence_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->kiosk_licence_requests_table, $data);
			return $data['id'];
		} else { // add data
			$this->read_db->insert($this->kiosk_licence_requests_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	
	/**
	 * Function to fetch those tills whos licence not created yet
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function get_non_licence_tills($company_id=0,$kiosk_type=1) {
		// fetch records
		$sql 	= "select till.* from ava_kiosk_till_log till where till.company_id = '$company_id'  AND NOT EXISTS (select id from ava_kiosk_licence_keys where till_id=till.id AND kiosk_type = $kiosk_type )";
		$query 	= $this->read_db->query($sql);
		return $query->result();
		
	}
	
	/**
	 * Get listing of till log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_till_data($id = 0) {
		if(!empty($id)) {
			$this -> read_db -> select('till.*,cmp.name as company_name,cmp.id as company_id,outl.id as outlet_id,outl.outlet_name');
			$this -> read_db ->from($this->kiosk_till_log_table.' till');
			$this -> read_db -> join($this->kiosk_companies_table.' cmp','cmp.id = till.company_id');
			$this -> read_db -> join($this->kiosk_outlets_table.' outl','outl.id = till.outlet_id');
			$this -> read_db -> where("till.id", $id);
			$result = $this -> read_db -> get();
			return $result -> row();
		}
		return false;
	}

}//end User_model
