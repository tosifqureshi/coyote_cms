<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo lang('add_licence');?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php 
					$expire_time = array('1'=>'1 Year','2'=>'2 Year','3'=>'3 Year','4'=>'4 Year'); ?>
					<div class="row">
						<div class="col-md-6">
							<!-- Application types start here -->
							<div class=" form-group">
								<label><?php echo lang('app_type');?></label>
								<div class="demo-radio-button">
									<input name="kiosk_type" id="app_type_1" type="radio" value="1" class="with-gap radio-col-light-blue" checked>
									<label for="app_type_1"><?php echo lang('app_type_1');?></label> 
									
									<input type="radio" id="app_type_2" name="kiosk_type" value="2" class="with-gap radio-col-light-blue">
									<label for="app_type_2"><?php echo lang('app_type_2');?></label>
								 </div>
							</div>
							<!-- Application types end here -->
							<!-- Company dropdown start here -->
							<div class="form-group row">
								<label class="control-label col-md-12"><?php echo lang('company');?></label>
								<div class="col-md-6">
									<?php
									$company_id = (!empty($company_id)) ? $company_id : '';
									$data	= array('name'=>'company_id','id'=>'company_id', 'class'=>'required form-control');
									echo form_dropdown($data,$companies,set_value('company_id',$company_id));?>
								</div>
								<div class="col-md-12 alert-info m-t-10">
									<i class="icon fa fa-info"></i> 
									<?php echo lang('licence_add_help_txt');?> 
								</div>
							</div>
							<!-- Company dropdown end here -->
						</div>	
					</div>	
					<div class="row">
						<!-- Till licence options start here -->
						<div class="form-group">   				
							<div class="col-md-12">
								<div class="box">
								  <div class="col-md-12">
										<div class="box-body">		
											<div class="clearboth product-grid-form">
												<table class="table table-bordered dn" id="licence-table"> 
													<thead class="bg_ccc"> 
														<th><?php echo lang('tillname')?></th>
														<th><?php echo lang('macid')?></th>
														<th><?php echo lang('licence_key')?></th>
														<th><?php echo lang('licence_expired_in')?></th>
														<th><?php echo lang('action')?></th>                          
													</thead>
													<tbody id="addMore_licence" class="product_table_tbody">
													</tbody>	
												</table>
											</div>		
										</div>	
									</div>	
								</div>	
							</div>
						</div>				
						<!-- Till licence options end here -->
					</div>
					<div class="row pb-3 float-right">
						 <div class="mx-1">
							<a href='<?php echo base_url().'admin/settings/kiosklicence' ?>'>
								<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
							</a>
						</div>
					</div>	
				</div>	
			</div>	
		</div>	
	</div>	
</div>	
	

	
	
<script type="text/javascript">
	
	/* 
    | -----------------------------------------------------
    | Handle form posting validation
    | -----------------------------------------------------
    */
	$(document).ready(function() {
        $("#add_licence_form").validate({
        });
    });
    
    $( "#add_licence_form" ).submit(function( event ) {
		var is_validate = true;
		$( ".licence_input" ).each(function( e ) {
			var row_id = $(this).attr('row_id');
			var mac_id = $('#mac_id_'+row_id).val();
			if(mac_id == '') {
				$('#mac_id_'+row_id).addClass('error');
				is_validate =false;
			}
		});
		if(is_validate == false) {
            bootbox.alert("Please add required licence(s) details!");
            return false;
        }
        return;
        event.preventDefault();
	});
	
	// Delete till licence row
	function remove_licence(element) {
		$(element).closest('tr').remove();
    }
	
	// Manage companies till records
	$( "#company_id" ).change(function() {
		get_tills();
	});
	
	// Get tills whos licence not created yet based on application type
	function get_tills() {
		var kiosk_type = $("input[name='kiosk_type']:checked").val();
		var company_id = $('#company_id').val();
		if(company_id !==0 && company_id !== ''){
			$.ajax({
				type: "POST",
				dataType: "json",
				url: '<?php echo site_url("admin/settings/kiosklicence/get_company_tills"); ?>',
				data: {company_id:company_id,kiosk_type:kiosk_type},
				beforeSend: function() {
					$('#loader1').fadeIn();	
				},
				success: function(data){
				if(data.result){
					var tills = (data.tills) ? data.tills : '';
					if(tills != '') {
						$('#licence-table').show();
					} else {
						bootbox.alert("No Till available for licence allotment!");
					}
					$('#addMore_licence').html(tills);
				  }
				},
				complete:function(){
					$('#loader1').fadeOut();	
				}
			});
		}
	}
	// Manage licence generation
	function generate_licence(till_id) {
		var mac_id = $('#mac_id_'+till_id).val();
		var expire_at = $('#licence_expire_'+till_id).val();
		var kiosk_type = $("input[name='kiosk_type']:checked").val();
		if(mac_id == '') {
			bootbox.alert("Please mention Mac Id to generate licence token!");
			return false;
		}
		if(till_id !==0 || till_id !== ''){
			$.ajax({
				type: "POST",
				dataType: "json",
				url: '<?php echo site_url("admin/settings/kiosklicence/generate_licence_token"); ?>',
				data: {till_id:till_id,mac_id:mac_id,expire_at:expire_at,kiosk_type:kiosk_type},
				beforeSend: function() {
					$('#loader1').fadeIn();	
				},
				success: function(data){
					if(data.result){
						$('.encoded_licence_textarea_'+till_id).html(data.encoded_licence_textarea);
						$('#encoded_licence_'+till_id).val(data.encoded_licence);
						$('.generateTokenSpan_'+till_id).hide();
						$('.decodeTokenSpan_'+till_id).show();
						$('#mac_id_'+till_id).prop('readonly', true);
						$('#mac_id_'+till_id).addClass('readonly_input');
						$('#licence_expire_'+till_id).prop('disabled', 'disabled');
						$('#licence_expire_'+till_id).addClass('readonly_input');
						$('.encoded_licence_textarea_'+till_id).addClass('licence_token');
					} else {
						bootbox.alert(data.msg);
						return false;
					}
				},
				complete:function(){
					$('#loader1').fadeOut();	
				}
			});
		}
	}
	
	// Manage licence decoding
	function decode_licence(till_id) {
		var encoded_token = $('#encoded_licence_'+till_id).val();
		if(encoded_token == '') {
			bootbox.alert("Invalid encoded token key!");
			return false;
		}
		if(till_id !==0 || till_id !== ''){
			$.ajax({
				type: "POST",
				dataType: "json",
				url: '<?php echo site_url("admin/settings/kiosklicence/decode_licence"); ?>',
				data: {encoded_token:encoded_token},
				beforeSend: function() {
					$('#loader1').fadeIn();	
				},
				success: function(data){
					if(data.result){
						$('#decodedtokenModal').modal('show');
						$('.modal-body').html(data.decoded_licence);
					} else {
						bootbox.alert('Error occured!');
						return false;
					}
				},
				complete:function(){
					$('#loader1').fadeOut();	
				}
			});
		}
	}
	
	// Manage licence tills fetching on the basis of kiosk type
	$("input[name='kiosk_type']").change(function(){
		get_tills();
	});
	
	// Function used to copy licnce on clipboard
	$(document).delegate('.licence-textarea','click',function(){
		var mac_id = $(this).attr('licence_code');
		var copyText = document.getElementById("access_code_"+mac_id);
		copyText.select();
		document.execCommand("Copy");
	});
</script>


