<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('kiosklicence') ?></h4>
				<a href="<?php echo site_url("admin/settings/kiosklicence/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_licence'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<?php 
				// define request types
				$request_types[] = lang('all_requests');
				$request_types[encode(1)] = lang('app_type_1');
				$request_types[encode(2)] = lang('app_type_2');
				$data = array('name'=>'request_types','id'=>'request_types', 'class'=>'request_types');
				echo form_dropdown($data,$request_types,set_value('request_type',$request_type));
				?>
				<div  style="clear:both;padding-bottom:10px"></div>
				<div class="table-responsive m-t-40">
					<table id="kiosklicence_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('till_name'); ?></th>
								<th><?php echo lang('mac_id'); ?></th>
								<th><?php echo lang('licence_token'); ?></th>
								<th><?php echo lang('app_type'); ?></th>
								<th><?php echo lang('licence_expired_in'); ?></th>
								<th class="no-sort"><?php echo lang('action'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($requests) && is_array($requests) && count($requests)>0):
								foreach($requests as $request) :
									// set encoded id
									$id = encode($request['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $request['till_name'];?></td>
										<td><?php echo $request['mac_id'];?></td>
										<td class="list_licence_token">
											<textarea id="access_code_<?php echo $request['id'];?>" rows="12" class="form-control licence-textarea crp" readonly><?php echo trim($request['access_code']);?></textarea>
										</td>
										<td><?php echo lang('app_type_'.$request['kiosk_type']);?></td>
										<td><?php echo (!empty($request['expire_at'])) ? date('d-m-Y',strtotime($request['expire_at'])) : 'NA';?></td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/kiosklicence/delete/".$id);?>')" title="<?php echo lang('remove_token_title');?>"><i class="fa fa-trash"></i></a>
											<a class="common-btn remove-btn" onclick="decode_licence('<?php echo $request['access_code'];?>')" title='<?php echo lang('view_decoded_token');?>'><i class="fa fa-eye"></i></a>
											<a class="common-btn copy-btn crp" onclick="copy_licence('<?php echo $request['id'];?>')" title="<?php echo lang('copy_token_title');?>"><i class="fa fa-copy"></i></a> 
										</td>	
									</tr>
								<?php 
								$i++;
								endforeach ; 
							endif; ?>
						</tbody>
					</table>
				</div>		
			</div>	
		</div>	
	</div>	
</div>

<!-- start the bootstrap modal where the decoded token will appear -->
<div class="modal fade dn" id="decodedtokenModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Decoded Licence Token</h5>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default save_entries" row_id = '<?php echo $row_id;?>' data-dismiss="modal">Ok</button>
			</div>
		</div>
	</div>
</div>
<!-- end the bootstrap modal where the decoded token will appear -->

<script>
	$(document).ready(function() {
		$('#kiosklicence_table').DataTable();
	});
	
	// Manage request listing filters
	$( ".request_types" ).change(function() {
		// get selected campaign type 
		var selected_request_type  = $(this).val();
		window.location.href = BASEURL+'admin/settings/kiosklicence/index/'+selected_request_type;
	});
	
	$('#view_token').click(function() {
		var token = $(this).attr('token');
		$('#decodedtokenModal').modal('show');
		$('#myModalLabel').html('<?php echo lang('encoded_token');?>');
		$('.modal-body').html(token);
	});
	
	// Manage licence decoding
	function decode_licence(encoded_token) {
		
		if(encoded_token == '') {
			alert("Invalid encoded token key!");
			return false;
		}
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: '<?php echo site_url("admin/settings/kiosklicence/decode_licence"); ?>',
			data: {encoded_token:encoded_token},
			beforeSend: function() {
				$('#loader1').fadeIn();	
			},
			success: function(data){
				if(data.result){
					$('#decodedtokenModal').modal('show');
					$('#myModalLabel').html('<?php echo lang('decoded_token');?>');
					$('.modal-body').html(data.decoded_licence);
				} else {
					alert('Error occured!');
					return false;
				}
			},
			complete:function(){
				$('#loader1').fadeOut();	
			}
		});	
	}
	
	// Function used to copy licnce on clipboard
	function copy_licence(licence_id='') {
		var copyText = document.getElementById("access_code_"+licence_id);
		copyText.select();
		document.execCommand("Copy");
		//alert("Copied the text: " + copyText.value);
	}
	
</script>

