<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kioskoutlet Controller
 *
 * Manages the kiosk outlet functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Kioskoutlet
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('kioskoutlet');
		$this->load->library('form_validation');
		$this->load->model('kioskoutlet/Kioskoutlet_model', 'kioskoutlet_model');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the kiosk outlets listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
		// get listing
		$outlets = $this->kioskoutlet_model->get_records();
		Template::set('outlets',$outlets);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_data()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/kioskoutlet');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_data($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/kioskoutlet');
			}
		}
		// set form values
		$data['outlet_title']	= lang('add_outlet');
		if($id) {
			$till = $this->kioskoutlet_model->get_records($id);	
			if(empty($till)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/kioskoutlet');
			}
			$data['outlet_title']	= lang('edit_outlet');
			$data['id']				= encode($id);
			$data['outlet_name']	= $till->outlet_name;		
			$data['outlet_email']  	= $till->outlet_email;		
			$data['phone_number']  	= $till->phone_number;		
			$data['company_id']   	= $till->company_id;	
			$data['status']		    = $till->status;
			$data['created_at']	    = $till->created_at;
		}
		// get companies
		$data['companies']	= get_kiosk_companies();
		// set template params
		Template::set('data',$data);
		Template::set_view('outlet_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store data in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_data($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
		}
		// apply validation rules for input params	
		$this->form_validation->set_rules('outlet_name', 'lang:outlet_name', 'trim|required');
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		// set post values in store bundle
		$data['outlet_name']    = $this->input->post('outlet_name');
		$data['outlet_email']  = $this->input->post('outlet_email');
		$data['phone_number']  = $this->input->post('phone_number');
		$data['company_id']   = $this->input->post('company_id');
		$data['created_by']   = $this -> session -> userdata('user_id');
		// add or update data into db
		$id = $this->kioskoutlet_model->save_data($data);
		return $id;
	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Update status of outlets
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->kioskoutlet_model->save_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/kioskoutlet');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove outlets from db
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$id = decode($id);
		$id = $this->kioskoutlet_model->remove_record($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/kioskoutlet');
	}

}//end Settings

// End of Admin kiosk company Controller
/* Location: ./application/core_modules/kiosktill/controllers/settings.php */
