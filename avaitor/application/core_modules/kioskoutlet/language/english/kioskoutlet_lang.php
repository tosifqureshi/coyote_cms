<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['kioskoutlet']		= 'Outlet Management';
$lang['add_outlet']			= 'Add New Outlet';
$lang['edit_outlet']		= 'Edit Outlet';
$lang['sno']				= 'S.No.';
$lang['outlet_name']		= 'Outlet Name';
$lang['outlet_email']		= 'Outlet Email';
$lang['outlet_number']		= 'Phone Number';
$lang['company']		    = 'Company';
$lang['created_at']			= 'Created At';
$lang['status']				= 'Status';
$lang['delete']				= 'Delete';
$lang['insert_msg']			= 'Successfully inserted';
$lang['update_msg']			= 'Successfully updated';
$lang['update_status']		= 'successfully change status';
$lang['delete_msg']			= 'Successfully removed';
$lang['form_save']			= 'Save';
$lang['form_cancel']		= 'Cancel';

