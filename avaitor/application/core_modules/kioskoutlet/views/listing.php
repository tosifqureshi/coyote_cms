<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('kioskoutlet') ?></h4>
				<a href="<?php echo site_url("admin/settings/kioskoutlet/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_outlet'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<div class="table-responsive m-t-40">
					<table id="outlet_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('outlet_name'); ?></th>
								<th><?php echo lang('company'); ?></th>
								<th><?php echo lang('outlet_email'); ?></th>
								<th><?php echo lang('outlet_number'); ?></th>
								<th><?php echo lang('created_at'); ?></th>
								<th><?php echo lang('status'); ?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($outlets) && is_array($outlets) && count($outlets)>0):
								foreach($outlets as $outlet) :
									// set encoded id
									$id = encode($outlet['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
											<a href="<?php echo site_url("admin/settings/kioskoutlet/create/".$id);?>">
												<?php echo (strlen($outlet['outlet_name']) > 20) ? substr($outlet['outlet_name'],0,20).'...' :  $outlet['outlet_name'];?>
											</a>
										</td>
										<td><?php echo $outlet['company_name'];?></td>
										<td><?php echo $outlet['outlet_email'];?></td>
										<td><?php echo $outlet['phone_number'];?></td>
										<td><?php echo date('F j, Y',strtotime($outlet['created_at']));?></td>
										<td>
											<?php
											// set change status url
											$status_url = site_url("admin/settings/kioskoutlet/change_status/".$id.DIRECTORY_SEPARATOR.encode($outlet['status']));
											if($outlet['status'] == '0') { ?>
												<div style="display:none"><?php echo $outlet['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
												<?php
											} else {
												?>
												<div style="display:none"><?php echo $outlet['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
											} ?>
										</td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/kioskoutlet/delete/".$id);?>')"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php 
								$i++;
								endforeach ; 
							endif; ?>
						</tbody>
					</table>
				</div>		
			</div>	
		</div>	
	</div>	
</div>	

<script>
	$(document).ready(function() {
		$('#outlet_table').DataTable();
	});
</script>

