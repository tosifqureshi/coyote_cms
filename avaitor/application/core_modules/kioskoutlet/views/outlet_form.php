<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $outlet_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					$id = isset($id) ? $id : '';
					echo form_open_multipart('admin/settings/kioskoutlet/create/'.$id,'id="add_outlet_form" class="form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Company dropdown start here -->
								<div class="form-group row">
									<label class="control-label col-md-12">
										<?php echo lang('company');?><span class="red">*</span>
									</label>
									<div class="col-md-6">
										<?php
										$company_id = (!empty($company_id)) ? $company_id : '';
										$data	= array('name'=>'company_id','id'=>'company_id', 'class'=>'required form-control');
										echo form_dropdown($data,$companies,set_value('company_id',$company_id));?>
									</div>
								</div>
								<!-- Company dropdown end here -->
								
								<!-- Name input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('outlet_name');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'outlet_name','id'=>'outlet_name', 'value'=> isset($outlet_name) ? $outlet_name : '' , 'class'=>'form-control form-control-line required');
									echo form_input($data);?>
								</div>
								<!-- Name input end here -->
								
								<!-- Email input start here -->
								<div class="form-group ">
									<label><?php echo lang('outlet_email');?></label>
									<?php
									$data = array('name'=>'outlet_email','id'=>'outlet_email', 'value'=> isset($outlet_email) ? $outlet_email : '' , 'class'=>'form-control form-control-line email');
									echo form_input($data);
									?>
								</div>
								<!-- Email input end here -->
								
								<!-- Phone number input start here -->
								<div class="form-group ">
									<label><?php echo lang('outlet_number');?></label>
									<?php
									$data = array('name'=>'phone_number','id'=>'phone_number', 'value'=> isset($phone_number) ? $phone_number : '' , 'class'=>'form-control form-control-line','maxlength'=>20);
									echo form_input($data);
									?>
								</div>
								<!-- Phone number input end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/kioskoutlet' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>			
					<?php echo form_close();?>
				</div>
			</div>		
		</div>		
	</div>		
</div>		

<script type="text/javascript">
	$(document).ready(function() {
		$("#add_outlet_form").validate({
			
		});
	});
	
	$( "#add_outlet_form" ).submit(function( event ) {
		return;
		event.preventDefault();
	});
	
	
</script>


