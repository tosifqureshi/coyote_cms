<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kiosktill Controller
 *
 * Manages the kiosk till functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Kiosktill
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('kiosktill');
		$this->load->library('form_validation');
		$this->load->model('kiosktill/Kiosktill_model', 'kiosktill_model');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the kiosk till listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
	
		// get listing
		$tills = $this->kiosktill_model->get_records();
		Template::set('tills',$tills);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_data()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/kiosktill');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_data($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/kiosktill');
			}
		}
		// set form values
		$data['till_title']	= lang('add_till');
		if($id) {
			$till = $this->kiosktill_model->get_records($id);	
			if(empty($till)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/kiosktill');
			}
			$data['till_title']	    = lang('edit_till');
			$data['id']				= encode($id);
			$data['till_name']	  	= $till->till_name;		
			$data['till_number']  	= $till->till_number;		
			$data['company_id']   	= $till->company_id;	
			$data['outlet_id']   	= $till->outlet_id;	
			$data['status']		    = $till->status;
			$data['created_at']	    = $till->created_at;
		}
		$company_id = (isset($data['company_id'])) ? $data['company_id'] : 0;
		// get companies
		$data['companies']	= get_kiosk_companies();
		// get company's outlets
		$data['outlets']	= get_kiosk_outlets($company_id);
		// set template params
		Template::set('data',$data);
		Template::set_view('till_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store data in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_data($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
		}
		// apply validation rules for input params	
		$this->form_validation->set_rules('till_name', 'lang:till_name', 'trim|required');
		$this->form_validation->set_rules('till_number', 'lang:till_number', 'trim|required');
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		$till_number = (!empty($this->input->post('till_number'))) ? $this->input->post('till_number') : 0;
		$till_id     = (!empty($id)) ? $id : 0;
		// get kiosk till record from number
		$is_exist	= $this->kiosktill_model->get_till_from_number($till_number,$till_id);
		if($is_exist == TRUE) {
			Template::set_message(lang('till_exists'),'error');
			redirect('admin/settings/kiosktill/create');
		}
		// set post values in store bundle
		$data['till_name']    = $this->input->post('till_name');
		$data['till_number']  = $this->input->post('till_number');
		$data['company_id']   = $this->input->post('company_id');
		$data['outlet_id']    = $this->input->post('outlet_id');
		$data['created_by']   = $this -> session -> userdata('user_id');
		// add or update data into db
		$id = $this->kiosktill_model->save_data($data);
		return $id;
	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Update status of company
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->kiosktill_model->save_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/kiosktill');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove company from db
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$id = decode($id);
		$id = $this->kiosktill_model->remove_record($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/kiosktill');
	}
	
	/**
	 * Function used to fetch company's outlets dropdown
	 * @input : null
	 * @return: void
	 * @access: public
	 */
    function get_outlets() {
		// set input post params
		$company_id = $this->input->post('company_id');
		$company_id = (!empty($company_id)) ? $company_id : 0;
		// get company's outlets
		$outlets	= get_kiosk_outlets($company_id);
		$data	= array('name'=>'outlet_id','id'=>'outlet_id', 'class'=>'required form-control');
		$outlet_selectbox = form_dropdown($data,$outlets,'');
		echo json_encode(array('outlets'=>$outlet_selectbox));die;
	}
	
	/**
	 * Function used to check till existance
	 * @input : null
	 * @return: void
	 * @access: public
	 */
    function check_kiosk_till_existance() {
		// set input post params
		$till_number = trim($this->input->post('till_number'));
		$till_id 	 = trim($this->input->post('till_id'));
		$till_number = (!empty($till_number)) ? $till_number : 0;
		$till_id     = (!empty($till_id)) ? decode($till_id) : 0;
		// get kiosk till record from number
		$is_exist	= $this->kiosktill_model->get_till_from_number($till_number,$till_id);
		echo json_encode(array('is_exist'=>$is_exist));die;
	}

}//end Settings

// End of Admin kiosk till Controller
/* Location: ./application/core_modules/kiosktill/controllers/settings.php */
