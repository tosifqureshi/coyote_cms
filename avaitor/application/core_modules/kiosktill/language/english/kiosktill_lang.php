<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['kiosktill']			= 'Till Management';
$lang['add_till']			= 'Add New Till';
$lang['edit_till']			= 'Edit Till';
$lang['sno']				= 'S.No.';
$lang['till_name']			= 'Till Name';
$lang['till_number']		= 'Till Number';
$lang['company']		    = 'Company';
$lang['outlet']		        = 'Outlet';
$lang['created_at']			= 'Created At';
$lang['status']				= 'Status';
$lang['delete']				= 'Delete';
$lang['insert_msg']			= 'Successfully inserted';
$lang['update_msg']			= 'Successfully updated';
$lang['update_status']		= 'successfully change status';
$lang['delete_msg']			= 'Successfully removed';
$lang['till_exists']		= 'Till number already exists!';
$lang['form_save']			= 'Save';
$lang['form_cancel']		= 'Cancel';

