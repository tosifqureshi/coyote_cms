<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Kiosktill Model
 *
 * The central way to access and perform CRUD on kiosk till.
 *
 * @package    Avaitor
 * @subpackage Modules_Kiosktill
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Kiosktill_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        // define tables
		$this->kiosk_till_log_table = 'kiosk_till_log';
		$this->kiosk_companies_table = 'kiosk_companies';
		$this->kiosk_outlets_table = 'kiosk_outlets';
    }

	/**
	 * Get listing of till log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_records($id = 0) {
		
		$this -> read_db -> select('till.*,cmp.name as company_name,outl.outlet_name');
		$this-> read_db ->from($this->kiosk_till_log_table.' till');
		if ($id) {
			$this -> read_db -> join($this->kiosk_companies_table.' cmp','cmp.id = till.company_id');
			$this -> read_db -> join($this->kiosk_outlets_table.' outl','outl.id = till.outlet_id');
			$this -> read_db -> where("till.id", $id);
			$result = $this -> read_db -> get($this->kiosk_till_log_table);
			return $result -> row();
		}
		$this -> read_db -> join($this->kiosk_companies_table.' cmp','cmp.id = till.company_id');
		$this -> read_db -> join($this->kiosk_outlets_table.' outl','outl.id = till.outlet_id');
		$this -> read_db -> order_by('till.created_at', 'desc');
		$result = $this -> read_db -> get();
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation of data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->kiosk_till_log_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->kiosk_till_log_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Function to remove entry from db
	 * @input : id
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	public function remove_record($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->kiosk_till_log_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Get till details from number
	 * @input : till_number
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_till_from_number($till_number = 0,$till_id=0) {
		$is_exist = false;
		if ($till_number) {
			$this -> read_db -> select('id');
			$this -> read_db -> where("till_number", $till_number);
			$result = $this -> read_db -> get($this->kiosk_till_log_table);
			$row = $result -> row();
			$is_exist = (!empty($row) && ($row->id != $till_id)) ? true : false;
		}
		return $is_exist;
	}

}//end Kiosktill_model
