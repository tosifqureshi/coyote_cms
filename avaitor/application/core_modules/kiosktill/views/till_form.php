<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $till_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					$id = isset($id) ? $id : '';
					echo form_open_multipart('admin/settings/kiosktill/create/'.$id,'id="add_till_form" class="form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Company dropdown start here -->
								<div class="form-group row">
									<label class="control-label col-md-12"><?php echo lang('company');?><span class="red">*</span></label>
									<div class="col-md-6">
										<?php
										$company_id = (!empty($company_id)) ? $company_id : '';
										$data	= array('name'=>'company_id','id'=>'company_id', 'class'=>'required form-control');
										echo form_dropdown($data,$companies,set_value('company_id',$company_id));?>
									</div>
								</div>
								<!-- Company dropdown end here -->
								
								<!-- Outlet dropdown start here -->
								<div class="form-group row">
									<label class="control-label col-md-12"><?php echo lang('outlet');?><span class="red">*</span></label>
									<div class="col-md-6 outlets_div">
										<?php
										$outlet_id = (!empty($outlet_id)) ? $outlet_id : '';
										$data	= array('name'=>'outlet_id','id'=>'outlet_id', 'class'=>'required form-control');
										echo form_dropdown($data,$outlets,set_value('outlet_id',$outlet_id));
										?>
									</div>
								</div>
								<!-- Outlet dropdown end here -->
								
								<!-- Till name input start here -->
								<div class="form-group ">
									<label><?php echo lang('till_name');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'till_name','id'=>'till_name', 'value'=> isset($till_name) ? $till_name : '' , 'class'=>'form-control form-control-line required');
									echo form_input($data);
									?>
								</div>
								<!-- Till name input end here -->
								
								<!-- Till number input start here -->
								<div class="form-group ">
									<label><?php echo lang('till_number');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'till_number','id'=>'till_number', 'value'=> isset($till_number) ? $till_number : '' , 'class'=>'required form-control form-control-line','maxlength'=>20);
									echo form_input($data);
									?>
								</div>
								<!-- Till number input end here -->
							</div>	
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/kiosktill' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>	
					<?php echo form_close();?>	
				</div>
			</div>
		</div>
	</div>
</div>			

<script type="text/javascript">
	$(document).ready(function() {
		$("#add_till_form").validate({
			
		});
	});
	
	$( "#add_till_form" ).submit(function( event ) {
		return;
		event.preventDefault();
	});
	
	// Manage companies till records
	$( "#company_id" ).change(function() {
		var company_id =  $(this).val();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: '<?php echo site_url("admin/settings/kiosktill/get_outlets"); ?>',
			data: {company_id:company_id},
			beforeSend: function() {
				$('#loader1').fadeIn();	
			},
			success: function(data){
				var outlets = (data.outlets) ? data.outlets : '';
				$('.outlets_div').html(outlets);
			},
			complete:function(){
				$('#loader1').fadeOut();	
			}
		});
	});
	
	// Check till number existance
	//~ $(document).delegate('#till_number','blur',function(e){
		//~ var till_number =  $(this).val();
		//~ $.ajax({
			//~ type: "POST",
			//~ dataType: "json",
			//~ url: '<?php echo site_url("admin/settings/kiosktill/check_kiosk_till_existance"); ?>',
			//~ data: {till_number:till_number,till_id:'<?php echo $id;?>'},
			//~ beforeSend: function() {
				//~ $('#loader1').fadeIn();	
			//~ },
			//~ success: function(data){
				//~ if(data.is_exist) {
					//~ $('#till_number').val('');
					//~ bootbox.alert("Till number already exists!");
					//~ return false;
				//~ }
			//~ },
			//~ complete:function(){
				//~ $('#loader1').fadeOut();	
			//~ }
		//~ });
	//~ });
	
	
</script>


