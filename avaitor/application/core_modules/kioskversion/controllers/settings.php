<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kioskversion Controller
 *
 * Manages the kiosk exe versions functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Kioskversion
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('kioskversion');
		$this->load->library('form_validation');
		$this->load->model('kioskversion/Kioskversion_model', 'kioskversion_model');
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the kiosk versions listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
		// get listing
		$versions = $this->kioskversion_model->get_records();
		Template::set('versions',$versions);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_data()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/kioskversion');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_data($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/kioskversion');
			}
		}
		// set form values
		$data['version_title']	= lang('add_version');
		if($id) {
			$version = $this->kioskversion_model->get_records($id);	
			if(empty($version)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/kioskversion');
			}
			$data['version_title']	    = lang('edit_version');
			$data['id']					= encode($id);
			$data['exe_version']		= $version->exe_version;			
			$data['exe_download_url']	= $version->exe_download_url;			
			$data['release_date']		= $version->release_date;			
			$data['created_at']	    	= $version->created_at;
		}
		// set template params
		Template::set('data',$data);
		Template::set_view('version_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store data in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_data($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
		}
		// apply validation rules for input params	
		$this->form_validation->set_rules('exe_version', 'lang:exe_version', 'trim|required');
		$this->form_validation->set_rules('exe_download_url', 'lang:exe_download_url', 'trim|required');
		$this->form_validation->set_rules('release_date', 'lang:release_date', 'trim|required');
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		// set post values in store bundle
		$data['exe_version']       = $this->input->post('exe_version');
		$data['exe_download_url']  = $this->input->post('exe_download_url');
		$data['release_date'] 	   = date('Y-m-d',strtotime($this->input->post('release_date')));
		$data['created_by']        = $this -> session -> userdata('user_id');
		// add or update data into db
		$id = $this->kioskversion_model->save_data($data);
		return $id;
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove record from db
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$id = decode($id);
		$id = $this->kioskversion_model->remove_record($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/kioskversion');
	}

}//end Settings

// End of Admin kiosk exe version Controller
/* Location: ./application/core_modules/kioskversion/controllers/settings.php */
