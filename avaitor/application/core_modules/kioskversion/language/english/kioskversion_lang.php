<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['kioskversion']		= 'EXE Version Management';
$lang['add_version']		= 'Add New Version';
$lang['edit_version']		= 'Edit Version';
$lang['sno']				= 'S.No.';
$lang['exe_version']		= 'EXE Version';
$lang['exe_download_url']	= 'Download Url';
$lang['created_at']			= 'Created At';
$lang['delete']				= 'Delete';
$lang['insert_msg']			= 'Successfully inserted';
$lang['update_msg']			= 'Successfully updated';
$lang['update_status']		= 'successfully change status';
$lang['delete_msg']			= 'Successfully removed';
$lang['form_save']			= 'Save';
$lang['form_cancel']		= 'Cancel';
$lang['release_date']		= 'Release Date';

