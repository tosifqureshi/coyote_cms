<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('kioskversion') ?></h4>
				<a href="<?php echo site_url("admin/settings/kioskversion/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_version'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<div class="table-responsive m-t-40">
					<table id="version_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('exe_version'); ?></th>
								<th><?php echo lang('exe_download_url'); ?></th>
								<th><?php echo lang('release_date'); ?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($versions) && is_array($versions) && count($versions)>0):
								foreach($versions as $version) :
									// set encoded id
									$id = encode($version['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
											<a href="<?php echo site_url("admin/settings/kioskversion/create/".$id);?>">
												<?php echo $version['exe_version'];?>
											</a>
										</td>
										<td><?php echo $version['exe_download_url'];?></td>
										<td><?php echo date('F j, Y',strtotime($version['release_date']));?></td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/kioskversion/delete/".$id);?>')"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php 
								$i++;
								endforeach; 
							endif; ?>
						</tbody>
					</table>
				</div>		
			</div>	
		</div>	
	</div>	
</div>

<script>
	$(document).ready(function() {
		$('#version_table').DataTable();
	});
</script>

