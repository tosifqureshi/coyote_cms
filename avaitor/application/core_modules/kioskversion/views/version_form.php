<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $version_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					$id = isset($id) ? $id : '';
					echo form_open('admin/settings/kioskversion/create/'.$id,'id="add_version_form" class="form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Exe version input start here -->
								<div class="form-group ">
									<label><?php echo lang('exe_version');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'exe_version','id'=>'exe_version', 'value'=> isset($exe_version) ? $exe_version : '' , 'class'=>'form-control form-control-line  required number','maxlength'=>5);
									echo form_input($data);
									?>
								</div>
								<!-- Exe version input end here -->
								
								<!-- Exe download url input start here -->
								<div class="form-group ">
									<label><?php echo lang('exe_download_url');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'exe_download_url','id'=>'exe_download_url', 'value'=> isset($exe_download_url) ? $exe_download_url : '' , 'class'=>'form-control form-control-line required');
									echo form_input($data);?>
								</div>
								<!-- Exe download url input end here -->
								
								<!-- Release date fields start here -->
								<div class="row form-group">
									<div class="col-md-6">
										<label for="co_start_date"><?php echo lang('release_date');?><span class="red">*</span></label>
										<input type="text" name="release_date" class="form-control required" value="<?php echo !empty($release_date) ? $release_date : '';?>" id="release_date" title="<?php echo lang('release_date');?>" readonly="readonly">
									</div>
								</div>
								<!-- Release date fields end here -->
							</div>	
						</div>	
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/kioskversion' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	// MAterial Date picker    
	$('#release_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
	
	$(document).ready(function() {
		$("#add_version_form").validate({
			
		});
	});
		
</script>


