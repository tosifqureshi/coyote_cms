<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Leaderboard Controller
 *
 * Manages the leader boards functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Leaderboard
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('leaderboard');
		$this->load->library('form_validation');
		$this->load->model('leaderboard/Leaderboard_model', 'leaderboard_model');
		// set ordering images path
		$this->image_path = FCPATH.'uploads/competition_images/'; 
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display the leader board list
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index()
	{
		// get leader boards listing
		$leader_boards = $this->leaderboard_model->get_leader_boards();
		Template::set('leader_boards',$leader_boards);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage category form
	 * @input : category_id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($category_id=0) {
		
		$category_id = decode($category_id); // decode category id
		$data = array();
		
		if((isset($_POST['save'])) && $category_id=='') {
			if($this->save_category()) {       
					Template::set_message(lang('co_insert_msg'),'success');
					redirect('admin/settings/competition');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_category($category_id)) {
					Template::set_message(lang('co_update_msg'),'success');
					redirect('admin/settings/competition');
			}
		}
		$data['competition_title']	= lang('add_competition');
		if($category_id) {
			$category_data = $this->ordering_model->get_categories($category_id);	
			if(empty($category_data)) {
				Template::set_message(lang('co_invalid_msg'),'error');
				redirect('admin/settings/competition');
			}
			$data['competition_title']	= lang('edit_competition');
			$data['competition_id']	= encode($category_id);
			$data['title']			= $category_data->title;		
			$data['short_description']= $category_data->short_description;		
			$data['description']	= $category_data->description;		
			$data['start_date']		= date('d-m-Y H:i:s',strtotime($category_data->start_date));
			$data['end_date']		= date('d-m-Y H:i:s',strtotime($category_data->end_date));
			$data['product_ids']	= $category_data->product_ids;
			$data['status']			= $category_data->status;
		}	
		
		Template::set('data',$data);
		Template::set_view('category_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store competition in database
	 * @input : type , competition_id
	 * @return: void
	 * @access: public
	 */
	public function save_competition($competition_id=0) {
		$data = array();
		if(!empty($competition_id)) {
			$data['competition_id']  = $competition_id;
			// get competition details
			$competition = $this->competition_model->get_competitions($competition_id);
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('title', 'lang:title', 'trim|required|max_length[80]');			
		$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');	
		$this->form_validation->set_rules('end_date', 'lang:end_date', 'trim|required');	
	
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		
		// Set input field values
		if (isset($_FILES['image_1']) && ($_FILES['image_1']['name']) != '' ) {
			// remove old image
			(isset($competition->image_1) && !empty($competition->image_1)) ? unlink($this->image_path.$competition->image_1) : '';
			// upload image
			$data['image_1'] = $this->to_upload_image('image_1');
		}
		if (isset($_FILES['image_2']) && ($_FILES['image_2']['name']) != '' ) {
			// remove old image
			(isset($competition->image_2) && !empty($competition->image_2)) ? unlink($this->image_path.$competition->image_2) : '';
			// upload image
			$data['image_2'] = $this->to_upload_image('image_2');
		}
		if (isset($_FILES['image_3']) && ($_FILES['image_3']['name']) != '' ) {
			// remove old image
			(isset($competition->image_3) && !empty($competition->image_3)) ? unlink($this->image_path.$competition->image_3) : '';
			// upload image
			$data['image_3'] = $this->to_upload_image('image_3');
		}
		
		// set post values in store bundle
		$data['title']   			= $this->input->post('title');
		$data['product_ids']  		= $this->input->post('product_ids');
		$data['short_description']  = $this->input->post('short_description');
		$data['description']    	= $this->input->post('description');
		$data['created_by'] 		= $this -> session -> userdata('user_id');
		$data['start_date']			= date("Y-m-d H:i:s",strtotime($this->input->post('start_date')));
		$data['end_date']  			= date("Y-m-d H:i:s",strtotime($this->input->post('end_date')));
		$data['default_image_type'] = (!empty($this->input->post('default_image_type'))) ? $this->input->post('default_image_type') : 1;
		
		// add or update loyalty data into db
		$competition_id = $this->competition_model->save_competition_data($data);
		return $competition_id;

	}

	//--------------------------------------------------------------------

	/**
	 * Upload competition image
	 * @input : image
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	private function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'competition_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('admin/settings/competition');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	//--------------------------------------------------------------------

	/**
	 * Update status of competition
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['competition_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->competition_model->save_competition_data($data);
		Template::set_message(lang('co_update_status'),'success');
		redirect('admin/settings/competition');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove competition
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($id='') {
		$data['competition_id'] = decode($id);
		$data['is_deleted'] = 1;
		$id = $this->competition_model->save_competition_data($data);
		Template::set_message(lang('co_delete_msg'),'success');
		redirect('admin/settings/competition');
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to remove image from list
	 * @input  : offer_id ,image_type
	 * @output : string
	 * @access : public
	 */
	public function remove_image() {
		// get data from post
		$id = $this->input->post('id');
		$image_type = $this->input->post('image_type');
		$file_name = $this->input->post('file_name');
		$return = false;
		if(!empty($image_type) && !empty($id)) {
			// update image value as blank
			$data['competition_id'] = decode($id);
			$data['image_'.$image_type] = "";
			// delete image from folder
			unlink($this->image_path.$file_name);
			// update image value in db
			$this->competition_model->save_competition_data($data);
			$return = true;
		} else if(!empty($image_type)) {
			$return = true;
		}
		echo $return;die;
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to get members purchase data in competition duration
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function competition_members($competition_id=0) {
		// get data from post
		//$competition_id = $this->input->post('competition_id');
		if(!empty($competition_id)) {
			$competition_id = decode($competition_id);
			// get competition details
			$competition = $this->competition_model->get_competitions($competition_id);
			if(!empty($competition)) {
				// set competition params for fetch purchase data
				$product_ids = (!empty($competition->product_ids)) ? $competition->product_ids : '';
				$start_date = (!empty($competition->start_date)) ? date('Ymd',strtotime($competition->start_date)) : '';
				$end_date = (!empty($competition->end_date)) ? date('Ymd',strtotime($competition->end_date)) : '';
				// get members purchase data in competition duration
				$co_members = $this->competition_model->competitions_members_data($product_ids,$start_date,$end_date);
			}
		}
		// set competition title
		$competition_title = (isset($competition->title) && !empty($competition->title)) ? $competition->title : '';
		// set members result if exists
		$co_members = (isset($co_members) && !empty($co_members)) ? $co_members : '';
		Template::set('competition_title',$competition_title);
		Template::set('competition_members',$co_members);
		Template::set_view('competition_members');
		Template::render();
	}
	

}//end Settings

// End of Admin Competition Controller
/* Location: ./application/core_modules/competition/controllers/settings.php */
