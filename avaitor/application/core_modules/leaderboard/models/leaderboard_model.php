<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Boards Model
 *
 * The central way to access and perform CRUD on leader boards.
 *
 * @package    Avaitor
 * @subpackage Modules_Boards
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Boards_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
		$this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // define tables
		$this->product_table = 'ProdTbl';
		$this->outp_table    = 'OutpTbl';
		$this->store_table   = 'STORTBL';
		$this->competitions_table  = 'competitions';
    }

	/**
	 * Function to get product data from MSSQL database
	 * @input : 
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	public function get_ms_products($limit=0,$offset=0) { 
		$result= mssql_query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM PRODTBL where Prod_Number != "") a WHERE row > '.$limit.' and row <= '.$offset);
		$result= $this->mssql_db->query('SELECT ACC_NUMBER,ACC_FIRST_NAME,ACC_SURNAME,ACC_ADDR_1,ACC_ADDR_2,ACC_POST_CODE,ACC_ACCOUNT_NAME,ACC_CONTACT,ACC_MOBILE,ACC_EMAIL,ACC_GENDER,ACC_STATUS,ACC_PASSWORD,ACC_DATE_OF_BIRTH,ACC_DATE_ADDED FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY ACC_NUMBER) as row FROM ACCOUNT where ACC_EMAIL != "") a WHERE row > '.$limit.' and row <= '.$offset);
		//echo $this -> mssql_db -> last_query();die;
		return $result -> result_array();
		
	}

	/**
	 * Get listing of leader boards
	 * @input : leaderboard_id
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_leader_boards($competition_id = 0,$co_expired=0) {
		
		$this -> read_db -> select('competitions.*');
		if ($competition_id) {
			$this -> read_db -> where("competition_id", $competition_id);
			$result = $this -> read_db -> get($this->competitions_table);
			return $result -> row();
		}
		$this -> read_db -> where(array('competitions.is_deleted'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		if($co_expired == 1) {
			// set current date
			$current_date = date('Y-m-d H:i:s');
			$this -> read_db -> where('competitions.end_date <',$current_date);
			//$this -> read_db -> where_or('competitions.is_expired','1');
		}
		$this -> read_db -> order_by('competitions.competition_id', 'desc');
		$result = $this -> read_db -> get($this->competitions_table);
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation of competition data
	 * @input : data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function save_competition_data($data) {
		
		if(isset($data['competition_id']) && !empty($data['competition_id'])) { // update data
			$this->db->where('competition_id', $data['competition_id']);
			$this->db->update($this->competitions_table, $data);
			return $data['competition_id'];
		} else { // add data
			$this->db->insert($this->competitions_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Function to get members purchase log in competition duration
	 * @input : string $product_ids , string $start_date , string $end_date
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	public function competitions_members_data($product_ids,$start_date,$end_date) {
		$result = $this->mssql_db->query("SELECT A.ACC_FIRST_NAME as member_name , A.ACC_EMAIL as member_email , ATX.ATRX_ACCOUNT as member_id, SUM(ATX.ATRX_QTY) as purchased_qty  FROM ACCOUNT_TRXTBL ATX LEFT JOIN ACCOUNT A on ATX.ATRX_ACCOUNT = A.ACC_NUMBER WHERE  ATX.ATRX_PRODUCT in (".$product_ids.") and ATX.ATRX_DATE between '".$start_date."' and '".$end_date."' and ATX.ATRX_TYPE = 'ITEMSALE' GROUP BY ATX.ATRX_ACCOUNT,A.ACC_FIRST_NAME , A.ACC_EMAIL order by purchased_qty DESC");
		return $result -> result_array();
	}
	
	/**
	 * Function to get competition winner log in competition duration
	 * @input : data
	 * @output: obj array
	 * @auther: Tosif Qureshi
	 */
	public function competitions_winner_data($product_ids,$start_date,$end_date) {
		$result = $this->mssql_db->query("SELECT top 1 A.ACC_FIRST_NAME as member_name , A.ACC_EMAIL as member_email , ATX.ATRX_ACCOUNT as member_id, SUM(ATX.ATRX_QTY) as purchased_qty  FROM ACCOUNT_TRXTBL ATX LEFT JOIN ACCOUNT A on ATX.ATRX_ACCOUNT = A.ACC_NUMBER WHERE  ATX.ATRX_PRODUCT in (".$product_ids.") and ATX.ATRX_DATE between '".$start_date."' and '".$end_date."' and ATX.ATRX_TYPE = 'ITEMSALE' GROUP BY ATX.ATRX_ACCOUNT,A.ACC_FIRST_NAME , A.ACC_EMAIL order by purchased_qty DESC");
		return (array) $result -> row();
	}

}//end User_model
