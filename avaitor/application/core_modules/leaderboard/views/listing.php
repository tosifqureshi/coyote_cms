<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="span9 dash-board">
	<div class="well-white">
		<div class="page-header">
			<h1><?php echo lang('competitions') ?></h1>
			<a href="<?php echo site_url("admin/settings/competition/create"); ?>" role="button" class="btn btn-primary pull-right"><?php echo lang('add_competition'); ?></a>
		</div>
		<div>
<?php			
if (isset($message) && !empty($message)){
	echo '<div class="alert alert-success">' . $message	 . '</div>';
} ?>
	<!-- start the bootstrap modal where the image will appear -->
	<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end the bootstrap modal where the image will appear -->


	<div role="grid" class="dataTables_wrapper" id="dyntable_wrapper">
		<table id="competition_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">    
			<thead>
				<tr role="row">
					<th><?php echo lang('co_sno'); ?></th>
					<th class="no-sort"><?php echo lang('co_image'); ?></th>
					<th><?php echo lang('co_title'); ?></th>
					<th><?php echo lang('co_created_at'); ?></th>
					<th><?php echo lang('co_status'); ?></th>
					<th class="no-sort"><?php echo lang('co_delete'); ?></th>
					<th class="no-sort view-btn"><?php echo lang('co_view_members'); ?></th>
				</tr>
			</thead>
                    
			<tbody role="alert" aria-live="polite" aria-relevant="all">
					<?php
					$i=1;
					if(isset($competitions) && is_array($competitions) && count($competitions)>0):
					foreach($competitions as $competition) :
						// set encoded competition id
						$competition_id = encode($competition['competition_id']); ?>
						<tr>
							<td><?php echo $i;?></td>
							<td>
								<?php if(isset($competition['default_image_type']) && !empty($competition['default_image_type'])) {
									$co_image = $competition['image_'.$competition['default_image_type']] ;
									$co_image = (!empty($co_image)) ? base_url('uploads/competition_images/'.$co_image) : base_url('uploads/No_Image_Available.png');?>
									<img  id="imageresource" src="<?php echo $co_image;?>" width="110" height="90" title="<?php echo $competition['title'];?>">
								<?php } ?>
							</td>
							<td>
								<div class="offer_info">
									<?php 
									$start_date_time = date('Y-m-d H:i:s',strtotime($competition['start_date']));
									$end_date_time = date('Y-m-d H:i:s',strtotime($competition['end_date']));
									$diff =  get_date_diffrence($start_date_time, $end_date_time);
									echo '<b>'.lang('co_title').' : </b>'.$competition['title'] ;
									echo '<br>';
									echo '<b>'.lang('co_start_date').' : </b>'.date('F j, Y, g:i a',strtotime($competition['start_date'])) ;
									echo '<br>';
									echo '<b>'.lang('co_end_date').'  : </b>'.date('F j, Y, g:i a',strtotime($competition['end_date'])) ;
									echo '<br>';
									echo $diff;
									?>
								</div>
								<a href="<?php echo site_url("admin/settings/competition/create/".$competition_id);?>"><?php echo (strlen($competition['title']) > 20) ? substr($competition['title'],0,20).'...' :  $competition['title'];?></a>
							</td>
							<td><?php echo date('F j, Y',strtotime($competition['created_at']));?></td>
							<td>
								<?php
								// set change status url
								$status_url = site_url("admin/settings/competition/change_status/".$competition_id.DIRECTORY_SEPARATOR.encode($competition['status']));
								if($competition['status'] == '0') { ?>
									<a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
									<?php
								} else {
									?>
									<a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
								<?php
								} ?>
							</td>
							<td>
								<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/competition/delete/".$competition_id);?>')"><i class="fa fa-trash"></i></a>
							</td>
							<td><a class="common-btn" href="<?php echo site_url("admin/settings/competition/competition_members/".$competition_id);?>"><i class="fa fa-eye"></i></td>
						</tr>
					<?php 
					$i++;
					endforeach ; endif; ?>
                </tbody>
            </table>                 
		</div>
	</div>
</div>

<!----------- Start competition members listing ------------->
<div id="modal_members_list"></div>


<script>
	jQuery('#competition_table').dataTable({
		"aoColumnDefs": [{ "targets": 'no-sort',"bSortable": false}],
		"bJQueryUI": true,
		"aaSorting": [[ 0, "asc" ]],
		"sPaginationType": "full_numbers"
	});
/* 
| ------------------------------------------------------------
| Show competition members listing on popup 
| ------------------------------------------------------------
*/ 
function competition_members(competition_id) {
	$.ajax ({
		type: "POST",
		data : {competition_id:competition_id},
		url: BASEURL+'admin/settings/competition/competition_members',
		async: false,
		success: function(data) {
			if(data) {
				$('#modal_members_list').html(data);
				$('#competitionmodal').modal('show');
			}
		}, 
		error: function(error){
			bootbox.alert(error);
		}
	});
		
}
</script>

