<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin support controller
 *
 * Provides Admin functions for page listing,view,add,edit,delete 
 *
 * @package    Coyote
 * @subpackage Modules_In store page
 * @category   Controllers
 * @author     Pritesh Gami 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Settings extends Admin_Controller {
	/**
	 * Controller constructor sets the Title and Permissions
	 *
	 */
	 
	/*pages : Start*/
	public function __construct() {
		parent::__construct();
		$this->load->model('pages/pages_model');
		$this->lang->load('pages');
	}//end __construct()

	//--------------------------------------------------------------------
	/**
	 * Displays the Reports context homepage
	 *
	 * @return void
	 */
	public function index() {								// show the list of active and not deleted page
		$pages = $this->pages_model->getPageList();		
		Template::set('pages',$pages);
		Template::set_view('pages/listing');
		Template::render();
	}//end index
	
	public function create($id="") {				// controller will call in both the cases on add and edit
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode page id
		$data = array();
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_page()) {       
					Template::set_message(lang('message_saved_page'),'success');
					redirect('admin/settings/pages');
			}
		} else if(isset($_POST['save'])) { 
			if($this->save_page('update',$id)) {
					Template::set_message(lang('message_update_page'),'success');
					redirect('admin/settings/pages');
			}
		}
		if($id) {
			$data['page_title']	= lang('edit_page');
			$data['records']	= $this->pages_model->getPageList($id);
		} else {
			$data['page_title']= lang('add_new_page');
		}
		Template::set('data',$data);
		Template::set_view('pages/pages');
		Template::render();	
	}
	
	public function save_page($type = 'insert',$id=0) {
		
		$data = array();
		if($id != 0) {
			$data['id'] = $id;
		} 
		$this->form_validation->set_rules('page_title', 'lang:page_title', 'trim|required|max_length[80]');	
		$this->form_validation->set_rules('page_long_description', 'lang:page_long_description', 'trim');	
		$this->form_validation->set_rules('page_slug', 'lang:page_slug', 'trim|required');	
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("Some error occured while saving form.",'error');
			return FALSE;
		}
		$data['title']     = $this->input->post('page_title');
		$data['slug']	= $this->input->post('page_slug');
		$data['content'] = $this->input->post('page_long_description');
		$data['status'] 		= 1;
		$data['section_id'] 	= 0;
		$data['menu_title'] 	= '';

		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->pages_model->add_page($data);
		}
		else if ($type == 'update') {
			$id = $this->pages_model->add_page($data);
		}
		return $id;
	}

	
	public function page_delete($id='') {
		$data['id'] = decode($id);
		$id = $this->pages_model->delete_page($data);
		Template::set_message(lang('message_delete_page'),'success');
		redirect('admin/settings/pages');
	}

	public function page_status($id='',$status='') {
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->pages_model->add_page($data);
		Template::set_message(lang('message_status_page'),'success');
		redirect('admin/settings/pages');
	}
	
	/*Pages : End*/
}//end class


