<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['page_title']			= 'Title';
$lang['pages']			= 'Pages';
$lang['page_slug']			= 'Slug';
$lang['tooltip_page_name'] 	= 'Enter the page name.';
$lang['page_long_description'] 	= 'Long Description';
$lang['form_save'] 	= 'Save';
$lang['form_cancel'] 	= 'Cancel';
$lang['add_new_page'] 	= 'Add Page';
$lang['edit_page'] 	= 'Edit Page';
$lang['page_sno'] 	= 'S.No.';
$lang['page_name'] 	= 'Name';
$lang['page_content'] 	= 'Content';
$lang['page_status'] 	= 'Status';
$lang['page_action'] 	= 'Action';
$lang['message_saved_page'] 	= 'Page save sucessfully';
$lang['message_update_page'] 	= 'Page update sucessfully';
$lang['message_delete_page'] 	= 'Page deleted sucessfully';
$lang['message_status_page'] 	= 'Page status change sucessfully';
$lang['page_created_at'] = 'Created at';







