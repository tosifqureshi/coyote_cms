<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Competition Model
 *
 * The central way to access and perform CRUD on Competition.
 *
 * @package    Avaitor
 * @subpackage Pages_model
 * @category   Models
 * @author     CDN Team
 * @link       http://www.cdnsolutionsgroup.com
 */
class Pages_model extends BF_Model
{

	function __construct() {
		
        parent::__construct();    
        $this->pages_table = 'ava_pages';
	}
	

	function add_page($data) {    //function is used in both case edit and update page
		if($data['id']) {											// edit data w.r.t. to id		
			$this->db->where('id', $data['id']);
			$this->db->update($this->pages_table, $data);
			return $data['id'];
		} else {														// add data
			$this->db->insert($this->pages_table, $data);
			return $this->db->insert_id();
		}
	}


	function getPageList($id = '') {    // get the list of Page to show in Page view list
		$this->db->select('*');
		if ($id) {
			$this->db->where("id", $id);
			$result = $this->db->get($this->pages_table);
			return $result->row();
		}
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->pages_table);
		//echo $this->db->last_query();die;
		return $result->result_array();
	}
	
	function delete_page($data) { 
		$id = $data['id'];
		$this->db->delete( $this->pages_table , array('id' => $id)); 
	}

}//end User_model
