<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('pages') ?></h4>
				<a href="<?php echo site_url("admin/settings/pages/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_page'); ?></button>
				</a>

<?php			
if (isset($message) && !empty($message)){
	echo '<div class="alert alert-success">' . $message	 . '</div>';
} ?>
	
	<div class="table-responsive m-t-40">
		
			<table id="pages_table" class="table table-bordered table-striped">
                    <thead>
                        <tr role="row">
							<th><?php echo lang('page_sno'); ?></th>
							<th><?php echo lang('page_name'); ?></th>
							<th><?php echo lang('page_slug'); ?></th>
							<th><?php echo lang('page_status'); ?></th>
							<th><?php echo lang('page_action'); ?></th>
						</tr>
						
                    </thead>
                    
                	<tbody role="alert" aria-live="polite" aria-relevant="all">
					<?php
					$i=1;
					if(isset($pages) && is_array($pages) && count($pages)>0):
					foreach($pages as $page) : 
						// set event id
						$id = encode($page['id']);?>
						
						<tr>
						<td><?php echo $i;?></td>
						<td class="mytooltip tooltip-effect-1 desc-tooltip">
						<span class="tooltip-content clearfix">
							<span class="tooltip-text">
							<?php 
							echo '<b>'.lang('page_name').' : </b>'.$page['title'] ;
							echo '<br>';
							echo '<b>'.lang('page_created_at').' : </b>'.date('F j, Y, g:i a',strtotime($page['created_at'])) ;
							echo '<br>';
							?>
							</span> 
						</span>
						<span class="text-ttip">
						<a href="<?php echo site_url("admin/settings/pages/create"); echo '/' . ($id);?>"><?php if(strlen($page['title']) > 20) { echo substr($page['title'],0,20).'...'; } else { echo $page['title']; }?></a>
						</span> 
						</td>
					
						<td>
							<p><?php if(strlen($page['slug']) > 20) { echo substr($page['slug'],0,20).'...'; } else { echo $page['slug']; }?></p>
						</td>
						<td><?php 
							if($page['status'] == '0') {
								?>
								<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo encode($page['id']); ?>','<?php echo encode($page['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
								<?php
							} else {
								?>
								<a class="common-btn active-btn" onclick="changeStatus('<?php echo encode($page['id']); ?>','<?php echo encode($page['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
								<?php
							}
						?></td>
						<td>
							<a class="common-btn delete-btn" href="javascript:;" onclick="deletePage('<?php echo encode($page['id']); ?>')"><i class="fa fa-trash"></i></a>
						</td>
						</tr>
					<?php 
					$i++;
					endforeach ; endif; ?>
                		</tbody>
            		</table>                 
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});
	function deletePage(id) {
		bootbox.confirm("Are you sure want to delete the page?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("admin/settings/pages/page_delete/"); ?>/"+id;
			}
		});
	}

	function changeStatus(id,status) {
		bootbox.confirm("Are you sure want to change the page status?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("admin/settings/pages/page_status/"); ?>/"+id+"/"+status;
			}
		});
	}
	
</script>