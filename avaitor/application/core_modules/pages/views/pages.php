<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">          
  <div class="col-md-12">
    <div class="card">
      <div class="card-body p-b-0">
<?php echo form_open_multipart('admin/settings/pages/create/'.(isset($records->id) ? encode($records->id) : ''),'id="add_pages_form"'); ?>
    <div class="p-t-20">
    <div class="row">
      <div class="col-md-6">
      <div class="form-group form-material">
      <label class=""><?php echo lang('page_title');?><i style="color:red;">*</i></label>
          <?php
          $data = array('name'=>'page_title', 'value'=>set_value('page_title', isset($records->title) ? $records->title : ''), 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_page_name'));
          echo form_input($data);
          ?>
      </div>
      
      <div class="form-group form-material">
        <label for="page_slug">
          <?php echo lang('page_slug');?>
          <i style="color:red;">*</i>
        </label>
        <div class="control-group ">
          <?php
          $data = array('name'=>'page_slug','id'=>'page_slug', 'value'=>set_value('slug', isset($records->slug) ? $records->slug : '') , 'class'=>'form-control form-control-line required');
          echo form_input($data);
          ?>
        </div>  
      </div>

      
      <div class="form-group form-material">
        <label for="page_long_description">
          <?php echo lang('page_long_description');?>
        </label>
        <div class="control-group"> 
          <?php
          $data = array('name'=>'page_long_description', 'class'=>'mceEditor redactor form-control form-control-line', 'value'=>set_value('content', isset($records->content) ? $records->content : ''), 'cols'=>'40', 'rows'=>'10');
          echo form_textarea($data);
          ?>
          </div>
        </div> 
               
      </div>       
    </div>
  </div>   
              <div class="row pb-3 float-right">
              <button type="submit" name="save" class="btn btn-primary"> <?php echo lang('form_save');?></button>
              &nbsp;
              <a href='<?php echo base_url().'admin/settings/pages' ?>'><button type="button" name="save" class="btn default"><?php echo lang('form_cancel');?></button></a>
              </div> 
          </div> 
      </div> 
    </div>
</div>




<script type="text/javascript">
  $(document).ready(function() {
    $("#add_pages_form").validate({
    });
  });

  $( "#add_pages_form" ).submit(function( event ) {
  });
  
</script>
