<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<p class="intro"><?php e(lang('permissions_intro')) ?></p>
				<h4 class="card-title float-left"><?php echo $toolbar_title ?></h4>
				<?php echo form_open($this->uri->uri_string()); ?>
					<div class="table-responsive m-t-40">
						<table id="permission_table" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="column-check">
										<input type="checkbox" id="check_all" class="check-all filled-in chk-col-light-blue" />
										<label for="check_all"></label>
									</th>
									<th><?php echo lang('permissions_id'); ?></th>
									<th><?php echo lang('permissions_name'); ?></th>
									<th><?php echo lang('permissions_description'); ?></th>
									<th><?php echo lang('permissions_status'); ?></th>
								</tr>
							</thead>
							<tfoot>
								<?php if (isset($results) && is_array($results) && count($results)) : ?>
								<tr>
									<td colspan="5">
										<?php echo lang('bf_with_selected') ?>
										<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('permissions_delete_confirm'); ?>')">
									</td>
								</tr>
								<?php endif;?>
							</tfoot>
							<tbody>
								<?php 
								if (isset($results) && is_array($results) && count($results)) :
									foreach ($results as $record) : 
										$record = (array)$record; ?>
										<tr>
											<td>
												<div class="demo-checkbox">
													<input type="checkbox" name="checked[]" id="check_<?php echo $record['permission_id'] ?>" value="<?php echo $record['permission_id'] ?>" class="filled-in chk-col-light-blue" />
													<label for="check_<?php echo $record['permission_id'] ?>"></label>
												</div>
											</td>
											<td><?php echo $record['permission_id'] ?></td>
											<td>
												<a href="<?php echo site_url(SITE_AREA .'/settings/permissions/edit/'. $record['permission_id']) ?>">
													<?php echo $record['name'] ?>
												</a>
											</td>
											<td><?php e($record['description']) ?></td>
											<td><?php e(ucfirst($record['status'])) ?></td>
										</tr>
									<?php endforeach; 
								else: ?>
									<tr>
										<td colspan="6">No permissions found.</td>
									</tr>
								<?php endif; ?>
							</tbody>
						</table>
					</div>	
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		jQuery('#permission_table').DataTable();
	});
</script>
