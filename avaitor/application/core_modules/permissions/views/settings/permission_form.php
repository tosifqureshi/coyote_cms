<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php // Change the css classes to suit your needs
	if( isset($permissions) ) {
		$permissions = (array)$permissions;
	}
	$id = isset($permissions['permission_id']) ? "/".$permissions['permission_id'] : '';
?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $toolbar_title ?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Permissions name input start here -->
								<div class="form-group ">
									<label><?php echo lang('permissions_name');?><span class="red">*</span></label>
									<input id="name" type="text" name="name" class="input-large form-control form-control-line" maxlength="30" value="<?php echo set_value('name', isset($permissions['name']) ? $permissions['name'] : ''); ?>"  />
								</div>
								<!-- Permissions name input end here -->
								
								<!-- Permissions description input start here -->
								<div class="form-group ">
									<label><?php echo lang('permissions_description');?></label>
									<input id="description" type="text" name="description" class="form-control form-control-line" maxlength="100" value="<?php echo set_value('description', isset($permissions['description']) ? $permissions['description'] : ''); ?>"  />
								</div>
								<!-- Permissions description input end here -->
								
								<!-- Permissions status start here -->
								<div class="form-group row">
									<label class="control-label col-md-12">
										<h4> <?php echo lang('permissions_status');?><span class="red">*</span></h4>
									</label>
									<div class="col-md-6">
										<select name="status" id="status" class="form-control">
											<option value="active" <?php echo set_select('status', lang('permissions_active')) ?>><?php echo lang('permissions_active') ?></option>
											<option value="inactive" <?php echo set_select('status', lang('permissions_inactive')) ?>><?php echo lang('permissions_inactive') ?></option>
											<option value="deleted" <?php echo set_select('status', lang('permissions_deleted')) ?>><?php echo lang('permissions_deleted') ?></option>
										</select>
									</div>	
								</div>
								<!-- Permissions status end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="submit" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('permissions_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/permissions' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('bf_action_cancel');?></button>
								</a>
							</div>
						</div>
					<?php echo form_close();?>
				</div>	
			</div>
		</div>
	</div>
</div>
