<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Reward Controller
 *
 * Manages the Reward functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Reward
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('reward');
		$this->load->library('form_validation');
		$this->load->model('reward/Reward_model', 'reward_model');
		// set banner images path
		$this->reward_path = FCPATH.'uploads/reward_images/';
		$this->earnrate_type = 1;
		$this->spendrate_type = 2;
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display result listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index() {
	
		// get listing
		$results = $this->reward_model->get_reward_rule_results();
		Template::set('results',$results);
		Template::set_view('listing');
		Template::render();

	}//end index()
	
	//--------------------------------------------------------------------

	/**
	 * Function to manage form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_rule()) {
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/reward');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_rule($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/reward');
			}
		}
		$data['form_title']	= lang('add_rule');
		if($id) {
			$result = $this->reward_model->get_reward_rule_results($id);	
			if(empty($result)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/reward');
			}
			$data['form_title']		= lang('edit_rule');
			$data['id']	 			= encode($id);
			$data['rule_name']		= $result->rule_name;		
			$data['start_date']		= date('Y-m-d',strtotime($result->start_date));
			$data['end_date']		= (!empty($result->end_date)) ? date('Y-m-d',strtotime($result->end_date)) : '';
			$data['reward_points']  = $result->reward_points;
			$data['status']			= $result->status;
			$data['created_at']		= $result->created_at;
			$data['description']	= $result->description;
			$data['banner_image']	= $result->banner_image;
			$data['rule_conditions']= $this->reward_model->get_conditions($id);
		}
		// set condition entity form
		$data['condition_form'] = $this->load->view('condition_form',array('row_id'=>1),true);
		Template::set('data',$data);
		Template::set_view('reward_rule_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store save reward rules in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_rule($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
			// get reward rule details
			$scratchnwin = $this->reward_model->get_reward_rule_results($id);
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('rule_name', 'lang:rule_name', 'trim|required|max_length[80]');			
		$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');
		$this->form_validation->set_rules('reward_points', 'lang:reward_points', 'trim|required|numeric');
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}

		if (isset($_FILES['banner_image']) && ($_FILES['banner_image']['name']) != '' ) {
			// remove old image
			(isset($cat_data->banner_image) && !empty($cat_data->banner_image)) ? unlink($this->reward_path.$cat_data->banner_image) : '';
			// upload image
			$data['banner_image'] = $this->to_upload_banner_image('banner_image');
            if($data['banner_image'] == false){
                return false;
            }
		}
		
		// set post values in store bundle
		$data['rule_name']   	= $this->input->post('rule_name');
		$data['reward_points']  = $this->input->post('reward_points');
		$data['description']    = $this->input->post('description');
		$data['created_by'] 	= $this -> session -> userdata('user_id');
		$data['start_date']		= date("Y-m-d",strtotime($this->input->post('start_date')));
		if(!empty($this->input->post('end_date'))) {
			$data['end_date']  	= date("Y-m-d",strtotime($this->input->post('end_date')));
		}
		
		if(empty($id)) {
			$current_date =  date("Y-m-d H:i:s");
			// set default result date as start date
			$data['created_at'] = $current_date;
		}
		// add or update reward rule data into db
		$id = $this->reward_model->save_data($data);
		// manage rule condition data
		$this->manage_conditions($id);
		return $id;
	}
	
	/**
	 * Add rule's conditions
	 * @input : id
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	private function manage_conditions($rule_id=0) {
		// manage products log
		$post = $this->input->post();
		//~ $reward_entity   = $this->input->post('reward_entity');
		//~ $entity_value    = $this->input->post('entity_value');
		$product_id   = $this->input->post('product_id');
		$product_qty    = $this->input->post('product_qty');
		$product_name    = $this->input->post('product_name');
		$condition_ids   = $this->input->post('condition_ids');
		
		// setup product array values
		//~ $reward_entity   = (!empty($reward_entity)) ? array_filter($reward_entity) : '';
		//~ $entity_value    = (!empty($entity_value)) ? array_filter($entity_value) : '';
		$product_id   = (!empty($product_id)) ? array_filter($product_id) : '';
		$product_qty    = (!empty($product_qty)) ? array_filter($product_qty) : '';
		$product_name    = (!empty($product_name)) ? array_filter($product_name) : '';
		$condition_ids   = (!empty($condition_ids)) ? array_filter($condition_ids) : '';
		
		if(!empty($product_id)) {
			foreach($product_id as $key=>$id) {
				$condition_data = array();
				$condition_data['product_id'] = $id;
				$condition_data['rule_id'] = $rule_id;
				$condition_data['product_qty'] = (isset($product_qty[$key])) ? $product_qty[$key] : '';
				$condition_data['product_name'] = (isset($product_name[$key])) ? $product_name[$key] : '';
				if(isset($condition_ids[$key]) && !empty($condition_ids[$key])) {
					$condition_data['id'] = $condition_ids[$key];
				}
				// add product data
				$this->reward_model->add_conditions($condition_data);
			}
		}
		return true;
	}
	
	/**
	 * Update status of reward rule
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_rule_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->reward_model->save_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/reward');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove reward rule
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete_rule($id='') {
		$id = decode($id);
		// delete reward rule entry
		$this->reward_model->remove_reward_rule($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/reward');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to view reward configuration
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function config() {
	
		// get saved configurations
		$results = $this->reward_model->get_reward_config();
        $result_reward_card = $this->reward_model->get_reward_cards();
		Template::set('results',$results);
		Template::set('result_reward_card',$result_reward_card);
		Template::set_view('reward_configuration');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to save reward configurations
	 * @input : id
	 * @return: void
	 * @access: public
	 */
	public function storeconfig() {
		$post = $this->input->post();
		$data = array();
		// set post values in store bundle
		$data['minimum_points_for_redeem']  = (!empty($post['minimum_points_for_redeem'])) ? $post['minimum_points_for_redeem'] : 0;
		$data['maximum_points_balance']  	= (!empty($post['maximum_points_balance'])) ? $post['maximum_points_balance'] : 0;
		$data['points_expire_after_days']   = (!empty($post['points_expire_after_days'])) ? $post['points_expire_after_days'] : 0;
		$data['registration_points']    	= (!empty($post['registration_points'])) ? $post['registration_points'] : 0;
		$data['birthday_points']    		= (!empty($post['birthday_points'])) ? $post['birthday_points'] : 0;
		$data['social_sharing_points']    	= (!empty($post['social_sharing_points'])) ? $post['social_sharing_points'] : 0;
		$data['rate_application_points']    = (!empty($post['rate_application_points'])) ? $post['rate_application_points'] : 0;
		$data['make_purchase_points']    	= (!empty($post['make_purchase_points'])) ? $post['make_purchase_points'] : 0;
		$data['view_vip_deals_points']    	= (!empty($post['view_vip_deals_points'])) ? $post['view_vip_deals_points'] : 0;
		$data['view_last_min_deal_points']  = (!empty($post['view_last_min_deal_points'])) ? $post['view_last_min_deal_points'] : 0;
		$data['points_per_day']    			= (!empty($post['points_per_day'])) ? $post['points_per_day'] : 0;
		$data['referral_status']    		= (!empty($post['referral_status'])) ? $post['referral_status'] : 1;
		$data['referral_invitation_points'] = (!empty($post['referral_invitation_points'])) ? $post['referral_invitation_points'] : 0;
		$data['referral_invitation_daily_limit'] = (!empty($post['referral_invitation_daily_limit'])) ? $post['referral_invitation_daily_limit'] : 0;
		$data['referral_purchase_type']    	= (!empty($post['referral_purchase_type'])) ? $post['referral_purchase_type'] : 1;
		$data['referral_purchase_points']   = (!empty($post['referral_purchase_points'])) ? $post['referral_purchase_points'] : 0;
		$data['notification_status']    	= (!empty($post['notification_status'])) ? $post['notification_status'] : 1;
		$data['points_expiry_notification'] = (!empty($post['points_expiry_notification'])) ? $post['points_expiry_notification'] : 1;
		$data['birthday_notification']    	= (!empty($post['birthday_notification'])) ? $post['birthday_notification'] : 1;
		$data['invite_notification']    	= (!empty($post['invite_notification'])) ? $post['invite_notification'] : 1;
		$data['before_points_expiry_days']  = (!empty($post['before_points_expiry_days'])) ? $post['before_points_expiry_days'] : 0;
		$data['referral_invitation_description']  = (!empty($post['referral_invitation_description'])) ? $post['referral_invitation_description'] : 0;
		$data['referral_terms_n_condition'] = (!empty($post['referral_terms_n_condition'])) ? $post['referral_terms_n_condition'] : 0;
		$reward_point_rates = (!empty($post['reward_point_rate'])) ? $post['reward_point_rate'] : 0;
		$reward_spend_rates = (!empty($post['reward_spend_rate'])) ? $post['reward_spend_rate'] : 0;
       
        // store reward earning and spend rates
        if(!empty($reward_point_rates)) {
            foreach($reward_point_rates as $key=>$value) {
                $records['id'] = $key;
                $records['reward_point_rate'] = (!empty($value)) ? $value : 0;
                $records['reward_spend_rate'] = (!empty($reward_spend_rates[$key])) ? $reward_spend_rates[$key] : 0;
                $this->reward_model->save_reward_card_data($records);
            }
        }
		// get config id
		$id = (!empty($post['config_id'])) ? $post['config_id'] : 0;
		if(!empty($id)) {
			$data['id']  = decode($id);
		}
		// add or update reward config data into db
		$config_id = $this->reward_model->save_config_data($data);
		if(!empty($config_id) && !empty($id)) {
			Template::set_message(lang('update_config_msg'),'success');
		} else if(!empty($config_id)) {
			Template::set_message(lang('insert_config_msg'),'success');
		} else {
			Template::set_message(lang('error_msg'),'error');
		}
		redirect('admin/settings/reward/config');
	}
	
	//------------------------------------------------------------------

	/**
	 * Function to display reward catalogues
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function catalogue() {
	
		// get listing
		$results = $this->reward_model->get_reward_catalogue();
		Template::set('results',$results);
		Template::set_view('reward_catalogues');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to manage catalogue form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create_catalogue() {
		
		//$id = decode($id); // decode id
		$id = 1;
		$data = array();
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_catalogue()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/reward/catalogue');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_catalogue($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/reward/create_catalogue/'.encode($id));
			}
		}
		$data['form_title']	= lang('add_catalogue');
		
        if($id) {
            $result = $this->reward_model->get_reward_catalogue($id);
            $data['form_title']		= lang('edit_catalogue');
            //$data['id']	 			= encode($id);
            $data['id']			= (!empty($result->id)) ? encode($result->id) : 0;
            $data['catalogue_id']			= (!empty($result->id)) ? encode($result->id) : 0;
            $data['title']			= (!empty($result->title)) ? $result->title : '';
            $data['banner_image']	= (!empty($result->banner_image)) ? $result->banner_image : '';
            $data['status']			= (!empty($result->status)) ? $result->status : 0;
            $data['created_at']		= (!empty($result->created_at)) ? $result->created_at : '';
            // get catalog product 
            $results = $this->reward_model->get_reward_catalogue_product(decode($data['id']),0);
            $data['results']	 			= $results;
		}
        
		Template::set('data',$data);
		Template::set_view('catalogue_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store save reward catalogue in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_catalogue($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
			// get reward catalogue details
			$cat_data = $this->reward_model->get_reward_catalogue($id);
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('title', 'lang:title', 'trim|required|max_length[80]');
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		// Set input field values
		if (isset($_FILES['banner_image']) && ($_FILES['banner_image']['name']) != '' ) {
			// remove old image
			(isset($cat_data->banner_image) && !empty($cat_data->banner_image)) ? unlink($this->reward_path.$cat_data->banner_image) : '';
			// upload image
			$data['banner_image'] = $this->to_upload_banner_image('banner_image');
            if($data['banner_image'] == false){
                return false;
            }
		}
		// set post values in store bundle
		$data['title']   		= $this->input->post('title');
		$data['short_description'] = $this->input->post('short_description');
		$data['description']    = $this->input->post('description');
		$data['created_by'] 	= $this -> session -> userdata('user_id');
		
		if(empty($id)) {
			$current_date =  date("Y-m-d H:i:s");
			// set default result date as start date
			$data['created_at'] = $current_date;
		}
		// add or update reward catalogue data into db
		$id = $this->reward_model->save_catalogue_data($data);
		return $id;
	}
    
    /*
     * @catalogueProduct
     * 
     **/
    
    public function catalogueProduct($catalogue_id=0,$id=0) {
        if(!empty($catalogue_id)) {
            // get listing
            $results = $this->reward_model->get_reward_catalogue_product(decode($catalogue_id),$id);
            Template::set('catalogue_id',$catalogue_id);
            Template::set('results',$results);
            Template::set_view('reward_catalogues_product');
            Template::render();
        } else {
            redirect('admin/settings/reward/catalogue');
        }
    }//catalogueProduct
    
    /*
     * @addCatalogueProduct
     *  
     **/
    public function addCatalogueProduct($catalogue_id=0,$id=0) {
		
		//echo '<pre>';print_r($this->input->post());die;
        if(!empty($catalogue_id)) {
            $id = decode($id); // decode id
            $data = array();
            if((isset($_POST['save'])) && $id=='') {
                if($this->save_catalogue_Products()) {       
                        Template::set_message(lang('insert_msg'),'success');
                        redirect('admin/settings/reward/create_catalogue/'.$catalogue_id);
                }
            } else if(isset($_POST['save'])) {
                if($this->save_catalogue_Products($id)) {
                        Template::set_message(lang('update_msg'),'success');
                        redirect('admin/settings/reward/create_catalogue/'.$catalogue_id);
                }
            }
            $data['form_title']	= lang('add_catalogue_products');
            $data['departments'] = array();
            $data['stores'] = array();
            if($id) {
                $result = $this->reward_model->get_reward_catalogue_product(decode($catalogue_id),$id);
                if(empty($result)) {
                    Template::set_message(lang('invalid_msg'),'error');
                    redirect('admin/settings/reward/catalogue');
                }
                $data['form_title']		= lang('edit_catalogue_products');
                $data['id']	 			= encode($id);
                //$data['product_title']			= $result->product_title;
                $data['ms_store_id']			= $result->ms_store_id;
                $data['department_id']			= $result->department_id;
                $data['department_name']			= $result->department_name;
                $data['product_id']			= $result->product_id;
                $data['product_name']			= $result->product_name;
                $data['redeem_points']			= $result->redeem_points;
                $data['add_extra_price']			= $result->add_extra_price;
                $data['extra_price']			= $result->extra_price;
                $data['redeem_points_with_extra_price']			= $result->redeem_points_with_extra_price;
                $data['banner_image']	= $result->banner_image;
                $data['start_date']	= $result->start_date;
                $data['end_date']	= $result->end_date;
                $data['status']			= $result->status;
                $data['created_at']		= $result->created_at;
                $data['short_description'] = $result->short_description;
                $data['description']	= $result->long_description;
                $getproductdata = $this->getproductdata($data['product_id']);
                $stores = (!empty($getproductdata['stores'])) ? $getproductdata['stores'] : array();
                $departments = (!empty($getproductdata['departments'])) ? $getproductdata['departments'] : array();
                $data['departments_array'] = $departments;
                $data['stores_array'] = $stores;
                
            }
            
            $data['catalogue_id'] = $catalogue_id;
            Template::set('data',$data);
            Template::set_view('catalogue_product_form');
            Template::render();
        } else {
            redirect('admin/settings/reward/catalogueProduct');
        }
    } //addCatalogueProduct
	
	//--------------------------------------------------------------------
    
    /*
     * @save_catalogue_Products
     * 
     **/
    public function save_catalogue_Products($id=0) {
        $post = $this->input->post();
        $data = array();
        if(!empty($id)) {
			$data['id']  = $id;
		}
        // apply validation rules for input params
		//$this->form_validation->set_rules('product_title', 'lang:product_title', 'trim|required|max_length[80]');
		//$this->form_validation->set_rules('store_id', 'lang:store_id', 'trim|required');
		//$this->form_validation->set_rules('department_id', 'lang:department_id', 'trim|required');
		$this->form_validation->set_rules('product_name', 'lang:product_name', 'trim|required');
		$this->form_validation->set_rules('product_id', 'lang:product_id', 'trim|required');
        
        if($this->form_validation->run() === FALSE) { //echo '1';die;
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		//echo '2';die;
		// Set input field values
		if (isset($_FILES['banner_image']) && ($_FILES['banner_image']['name']) != '' ) {

			// upload image
			$data['banner_image'] = $this->to_upload_banner_image('banner_image');
            if($data['banner_image'] == false){
                return false;
            }
		}
		// set post values in store bundle
		//$data['product_title']   		= (!empty($this->input->post('product_title'))) ? $this->input->post('product_title') : '';
		$data['catalogue_id'] = (!empty($this->input->post('catalogue_id'))) ? decode($this->input->post('catalogue_id')) : '';
		$data['ms_store_id'] = (!empty($this->input->post('store_id'))) ? implode(",", $this->input->post('store_id')) : '';
		$data['department_id']    = (!empty($this->input->post('department_id'))) ? implode(",", $this->input->post('department_id')) : '';
		$data['department_name']    = (!empty($this->input->post('department_name'))) ? $this->input->post('department_name') : '';
		$data['product_name'] 	= (!empty($this->input->post('product_name'))) ? $this->input->post('product_name') : '';
		$data['product_id'] 	= (!empty($this->input->post('product_id'))) ? $this->input->post('product_id') : '';
		$data['start_date'] 	= (!empty($this->input->post('start_date'))) ? date('Y-m-d',strtotime($this->input->post('start_date'))) : '';
		$data['end_date'] 	= (!empty($this->input->post('end_date'))) ? date('Y-m-d',strtotime($this->input->post('end_date'))) : '';
		$data['redeem_points'] 	= (!empty($this->input->post('redeem_points'))) ? $this->input->post('redeem_points') : 0;
		$data['add_extra_price'] 	= (!empty($this->input->post('add_extra_price'))) ? $this->input->post('add_extra_price') : 0;
		$data['redeem_points_with_extra_price'] = 0;
		$data['extra_price'] = 0;
        if(!empty($data['add_extra_price'])) {
            $data['redeem_points_with_extra_price'] 	= (!empty($this->input->post('redeem_points_with_extra_price'))) ? $this->input->post('redeem_points_with_extra_price') : 0;
            $data['extra_price'] 	= (!empty($this->input->post('extra_price'))) ? $this->input->post('extra_price') : 0;
        }
        
        $data['short_description'] 	= (!empty($this->input->post('short_description'))) ? $this->input->post('short_description') : '';
        $data['long_description'] 	= (!empty($this->input->post('description'))) ? $this->input->post('description') : '';
        $data['created_by'] 	= $this -> session -> userdata('user_id');
		// add or update reward catalogue data into db
		$id = $this->reward_model->save_catalogue_product_data($data);
		return $id;
    }
    //--------------------------------------------------------------------
    
    public function getStoreDepartment() {
        $store_id = (!empty($this->input->post('store_id'))) ? (int) $this->input->post('store_id') : 0;
        $departments = $this->reward_model->get_department_list($store_id);
        $dropdown = "<option value=''>Select department</option>";
        if(!empty($departments)) {
            foreach($departments as $department){
                 $dropdown .= "<option value=".$department['CODE_KEY_NUM'].">".$department['CODE_DESC']."</option>";
            }
        }
        echo $dropdown;die;
    }
    
    public function getDepartmentProducts() {
        $keyword = (!empty($this->input->post('keyword'))) ? $this->input->post('keyword') : 0;
        $keyword1 = str_replace("'","''",$keyword);
        $department_id = (!empty($this->input->post('department_id'))) ? $this->input->post('department_id') : 0;
        $store_id = (!empty($this->input->post('store_id'))) ? $this->input->post('store_id') : 0;
        $products = $this->reward_model->get_department_products($department_id,$store_id,$keyword1);
        $dropdown = "<ul id='country-list'>";
        if(!empty($products)) {
            foreach($products as $product) {
                $Prod_Number = (!empty($product['Prod_Number'])) ? $product['Prod_Number'] : '';
                $Prod_Desc = (!empty($product['Prod_Desc'])) ? html_entity_decode($product['Prod_Desc']) : '';
                //$onclick = "selectCountry('$Prod_Number','$Prod_Desc')";
                $onclick = "\"javascript: selectCountry('$Prod_Number','$Prod_Desc')";
                $dropdown .= "<li onClick=\"javascript: selectCountry('$Prod_Number','$Prod_Desc');\">".$Prod_Desc."</li>";
            }
        }
        echo $dropdown .= "</ul>";die;
    }
    
   /**
	* Upload scratch&win banner image
	* @input : image
	* @return: void
	* @access: private
	*/
	private function to_upload_banner_image($image='',$upload_at='reward_images/') {
		
		$dirPath = IMAGEUPLOADPATH.$upload_at;
		
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){				
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
            return false;
			//redirect('admin/settings/reward/catalogue');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to get product data
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function getproductdata($productid=0) {
		// get data from post
		$product_id = (int) $this->input->post('product_id');
        $product_id = (!empty($product_id)) ? $product_id : $productid;
		$response = array();
		if(!empty($product_id)) {
			// get product details from product id
			$product_data = $this->reward_model->get_product_data($product_id);
            
            $product_array = array();
            $departments_array = array();
            $CODE_KEY_NUM_array = array();
            $OUTL_OUTLET_array = array();
            $stores_array = array();
            
            if(!empty($product_data)) {
                $i = 0;
                foreach($product_data as $data){
                    $product_array['Prod_Number'] = (!empty($data['Prod_Number'])) ? $data['Prod_Number'] : '';
                    $product_array['Prod_Desc'] = (!empty($data['Prod_Desc'])) ? $data['Prod_Desc'] : '';
                    
                    $CODE_KEY_NUM = (!empty($data['CODE_KEY_NUM'])) ? $data['CODE_KEY_NUM'] : '';
                    $CODE_DESC = (!empty($data['CODE_DESC'])) ? $data['CODE_DESC'] : '';
                    $OUTL_OUTLET = (!empty($data['OUTL_OUTLET'])) ? $data['OUTL_OUTLET'] : '';                    
                    $OUTL_Name_On_App = (!empty($data['OUTL_Name_On_App'])) ? $data['OUTL_Name_On_App'] : '';
                    
                    if(!empty($CODE_DESC) && !in_array($CODE_KEY_NUM,$CODE_KEY_NUM_array)) {
                        $CODE_KEY_NUM_array[] = $CODE_KEY_NUM;
                        $departments_array[$i]['CODE_KEY_NUM'] = $CODE_KEY_NUM;
                        $departments_array[$i]['CODE_DESC'] = $CODE_DESC;
                    }
                    if(!empty($OUTL_Name_On_App) && !in_array($OUTL_OUTLET,$OUTL_OUTLET_array)){
                        $OUTL_OUTLET_array[] = $OUTL_OUTLET;
                        $stores_array[$i]['OUTL_OUTLET'] = $OUTL_OUTLET;
                        $stores_array[$i]['OUTL_Name_On_App'] = $OUTL_Name_On_App;                   
                    }
                    $i++;
                }
            }
		} 
        
        if (!$this->input->is_ajax_request()) {
            $product_array['stores'] = $stores_array;
            $product_array['departments'] = $departments_array;
            return $product_array;
        } else {
            //$departments_dropdown = "<option value=''>Select Departments</option>";
            $departments_dropdown = "";
            if(!empty($departments_array)) {
                foreach($departments_array as $department){
                     $departments_dropdown .= "<option value=".$department['CODE_KEY_NUM'].">".$department['CODE_DESC']."</option>";
                }
            }
            //$stores_dropdown = "<option value=''>Select Stores</option>";
            $stores_dropdown = "";
            if(!empty($stores_array)) {
                foreach($stores_array as $store) {
                     $stores_dropdown .= "<option value=".$store['OUTL_OUTLET'].">".$store['OUTL_Name_On_App']."</option>";
                }
            }
            
            $product_array['stores'] = $stores_dropdown;
            $product_array['departments'] = $departments_dropdown;
            $response = $product_array;
            // set return json response
            echo json_encode($response);die;
        }
		
	}
	
	//--------------------------------------------------------------------

	/**
	 * Update status of reward catalogue
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_catalogue_product_status($catalogue_id='',$id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$this->reward_model->save_catalogue_product_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/reward/catalogueProduct/'.$catalogue_id);
	}
	//--------------------------------------------------------------------

	/**
	 * Update status of reward catalogue
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_catalogue_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$this->reward_model->save_catalogue_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/reward/catalogue');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove reward catalogue Product
	 * @input : id
	 * @return: void
	 * @access: public
	 */
	public function delete_catalogue_product($catalogue_id=0,$id='') {
		$id = decode($id);
		// delete catalogue entry
		$this->reward_model->remove_reward_catalogue_product($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/reward/catalogueProduct/'.$catalogue_id);
	}
    
	//--------------------------------------------------------------------

	/**
	 * Remove reward catalogue
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete_catalogue($id='') {
		$id = decode($id);
		// delete catalogue entry
		$this->reward_model->remove_reward_catalogue($id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/reward/catalogue');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to render condition form 
	 * @input : row_id
	 * @return  void
	 * @access public
	 */
	public function condition_form()
	{
		// get data from post
		$row_id = $this->input->post('row_id');
		$data['row_id'] = $row_id;
		$tpl_data = $this->load->view('condition_form',$data,true);
		echo json_encode(array('status'=>true,'condition_form'=>$tpl_data));die;

	}//end product_form()
	
	//--------------------------------------------------------------------

	/**
	 * Remove rule condition entry
	 * @input : id
	 * @return: void
	 * @access: public
	 */
	public function remove_rule_condition() {
		// get data from post
		$id = $this->input->post('rule_condition_id');
		$rule_id = $this->input->post('rule_id');
		$status = false;
		if(!empty($id) && !empty($rule_id)) {
			$id = $this->reward_model->remove_rule_condition($id);
			$status = true;
		}
		// set return json response
		echo json_encode(array('status'=>$status));die;
	}
	
	//------------------------------------------------------------------

	/**
	 * Function to display reward cards
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function loyaltycards() {
	
		// get listing
		$results = $this->reward_model->get_reward_cards();
		Template::set('results',$results);
		Template::set_view('loyalty_cards');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to manage reward card form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create_card($id=0) {
		
		$id = decode($id); // decode id
		$data = array();
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_loyalty_card()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/reward/loyaltycards');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_loyalty_card($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/reward/loyaltycards');
			}
		}
		$data['form_title']	= lang('add_card');
        $data['min_purchase_amount'] = $this->reward_model->get_min_purchase_amount();
		if($id) {
			$result = $this->reward_model->get_reward_cards($id);
			if(empty($result)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/reward/loyaltycards');
			}
			$data['form_title']		= lang('edit_card');
			$data['id']	 			= encode($id);
			$data['card_name']		= $result->card_name;
			$data['background_color']	 = $result->background_color;		
			$data['font_color']	 = $result->font_color;	
			$data['logo_color']	 = $result->logo_color;	
			$data['font_color_2']	 = $result->font_color_2;	
			$data['min_purchase_amount'] = $result->min_purchase_amount;
			$data['max_purchase_amount'] = $result->max_purchase_amount;
			$data['card_image']		= $result->card_image;
			$data['status']			= $result->status;
			$data['created_at']		= $result->created_at;
			//$data['start_date']		= date('d-m-Y',strtotime($result->start_date));
			$data['end_date']		= (!empty($result->end_date)) ? date('m-Y',strtotime($result->end_date)) : '';
			$data['description']	= $result->description;
			$data['card_conditions']= $this->reward_model->get_card_conditions($id);
		}
		// set condition entity form
		$data['condition_form'] = $this->load->view('card_condition_form',array('row_id'=>1),true);
		Template::set('data',$data);
		Template::set_view('loyalty_card_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store save reward card in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_loyalty_card($id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
			// get reward catalogue details
			$card_data = $this->reward_model->get_reward_catalogue($id);
		}
        
        if(!empty($this->check_card_name())){
            Template::set_message("Card name is already selected",'error');
			return FALSE;
        }
		// apply validation rules for input params
		$this->form_validation->set_rules('card_name', 'lang:card_name', 'trim|required|max_length[80]');	
		//$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');		
		$this->form_validation->set_rules('min_purchase_amount', 'lang:min_purchase_amount', 'trim|required');
		$this->form_validation->set_rules('max_purchase_amount', 'lang:max_purchase_amount', 'trim|required');
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		// Set input field values
		if (isset($_FILES['card_image']) && ($_FILES['card_image']['name']) != '' ) {
			// remove old image
			(isset($card_data->card_image) && !empty($card_data->card_image)) ? unlink($this->reward_path.$card_data->card_image) : '';
			// upload image
			$data['card_image'] = $this->to_upload_banner_image('card_image');
            if($data['card_image'] == false){
                return false;
            }
		}
		// set post values in store bundle
		$data['card_name']   	= $this->input->post('card_name');
		$data['description'] 	= $this->input->post('description');
		$data['background_color']	= $this->input->post('background_color');
		$data['font_color']	= $this->input->post('font_color');
		$data['logo_color']	= $this->input->post('logo_color');
		$data['font_color_2']	= $this->input->post('font_color_2');
		$data['min_purchase_amount']	= $this->input->post('min_purchase_amount');
		$data['max_purchase_amount']	= $this->input->post('max_purchase_amount');
		$data['created_by'] 	= $this -> session -> userdata('user_id');
		$data['end_date']  	= $this->input->post('end_date');
		$data['card_name']  = $this->input->post('card_name');
        /*$data['start_date']		= date("Y-m-d",strtotime($this->input->post('start_date')));*/
		if(!empty($this->input->post('end_date'))) {
            $end_date = '01-'.$this->input->post('end_date');
			$data['end_date']  	= date("Y-m-d",strtotime($end_date));
		}
		// add or update reward card data into db
		$id = $this->reward_model->save_reward_card_data($data);
		return $id;
	}
	
	/**
	 * Add loyalty card conditions
	 * @input : id
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	private function manage_card_conditions($card_id=0) {
		// manage products log
		$post = $this->input->post();
		$condition_entity   = $this->input->post('condition_entity');
		$entity_value    = $this->input->post('entity_value');
		$condition_ids   = $this->input->post('condition_ids');
		
		// setup condition array values
		$condition_entity= (!empty($condition_entity)) ? array_filter($condition_entity) : '';
		$entity_value    = (!empty($entity_value)) ? array_filter($entity_value) : '';
		$condition_ids   = (!empty($condition_ids)) ? array_filter($condition_ids) : '';
		
		if(!empty($condition_entity)) {
			foreach($condition_entity as $key=>$id) {
				$condition_data = array();
				$condition_data['condition_entity'] = $id;
				$condition_data['card_id'] = $card_id;
				$condition_data['entity_value'] = (isset($entity_value[$key])) ? $entity_value[$key] : '';
				if(isset($condition_ids[$key]) && !empty($condition_ids[$key])) {
					$condition_data['id'] = $condition_ids[$key];
				}
				// add condition data
				$this->reward_model->add_card_conditions($condition_data);
			}
		}
		return true;
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to render card condition form 
	 * @input : row_id
	 * @return  void
	 * @access public
	 */
	public function card_condition_form() {
		// get data from post
		$row_id = $this->input->post('row_id');
		$data['row_id'] = $row_id;
		$tpl_data = $this->load->view('card_condition_form',$data,true);
		echo json_encode(array('status'=>true,'card_condition_form'=>$tpl_data));die;

	}//end product_form()
	
	//--------------------------------------------------------------------

	/**
	 * Remove card condition entry
	 * @input : id
	 * @return: void
	 * @access: public
	 */
	public function remove_card_condition() {
		// get data from post
		$id = $this->input->post('rule_condition_id');
		$card_id = $this->input->post('card_id');
		$status = false;
		if(!empty($id) && !empty($card_id)) {
			$id = $this->reward_model->remove_card_condition($id);
			$status = true;
		}
		// set return json response
		echo json_encode(array('status'=>$status));die;
	}
	
	/**
	 * Update status of reward card
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_card_status($id='',$status='') {
		
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$this->reward_model->save_reward_card_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/reward/loyaltycards');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove reward card
	 * @input : id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete_card($id='') {
		$data['id'] = decode($id);
		$data['is_deleted'] = 1;
		$this->reward_model->save_reward_card_data($data);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/reward/loyaltycards');
	}
	
    //------------------------------------------------------------------
    
    /**
     * @check_card_name check the card name already exists or not
     * @input card_name(string) card_id(int)
     * @output boolean
     */
    public function check_card_name() {
        $data['card_name']  = (!empty($this->input->post('card_name'))) ? $this->input->post('card_name') : '';
        $data['card_id']  = (!empty($this->input->post('card_id'))) ? decode($this->input->post('card_id')) : 0;
        if (!$this->input->is_ajax_request()) {
            return $this->reward_model->check_card_name($data);
        } else {
            echo $this->reward_model->check_card_name($data);die;
        }
    } //end check_card_name
    
    //------------------------------------------------------------------
    
    /**
     * @check_card_purchase_amount check the card total purchace amount already assigne to any other card or not
     * @input purchase_amount(float) card_id(int)
     * @output boolean
     */
    public function check_card_purchase_amount() {
        $data['max_purchase_amount']  = (!empty($this->input->post('max_purchase_amount'))) ? $this->input->post('max_purchase_amount') : 0;
        $data['min_purchase_amount']  = (!empty($this->input->post('min_purchase_amount'))) ? $this->input->post('min_purchase_amount') : 0;
        $data['card_id']  = (!empty($this->input->post('card_id'))) ? decode($this->input->post('card_id')) : 0;
        if (!$this->input->is_ajax_request()) {
            return $this->reward_model->check_card_purchase_amount($data);
        } else {
            echo $this->reward_model->check_card_purchase_amount($data);die;
        }
    } //end check_card_name

    //------------------------------------------------------------------

	/**
	 * Function to display reward earn rates
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function earnrate() {
		// get listing
		$results = $this->reward_model->get_reward_pointrates($this->earnrate_type);
		$type = $this->earnrate_type;
		Template::set('type',$type);
		Template::set('results',$results);
		Template::set_view('reward_point_rates');
		Template::render();
	}
	
	//------------------------------------------------------------------

	/**
	 * Function to display reward spend rates
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function spendrate() {
		// get listing
		$results = $this->reward_model->get_reward_pointrates($this->spendrate_type);
		$type = $this->spendrate_type;
		Template::set('type',$type);
		Template::set('results',$results);
		Template::set_view('reward_point_rates');
		Template::render();
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to manage reward card form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create_rate($type=0,$id=0) {
		
		$type = decode($type); // decode rate type
		if(empty($type) || $type > 2)  {
			Template::set_message(lang('invalid_rate_msg'),'error');
			redirect('admin/settings/reward/earnrate');
		}
		$id = decode($id); // decode id
		$data = array();
		$rate_section = ($type == 1) ? 'earnrate' : 'spendrate';
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_rates($type)) {    
				Template::set_message(lang('insert_msg'),'success');
				redirect('admin/settings/reward/'.$rate_section);
			}
		} else if(isset($_POST['save'])) {
			if($this->save_rates($type,$id)) {
				Template::set_message(lang('update_msg'),'success');
				redirect('admin/settings/reward/'.$rate_section);
			}
		}
		$data['form_title']	= lang('add_rate'.$type);
		$data['rate_type']	= $type;
		if($id) {
			$result = $this->reward_model->get_reward_pointrates($type,$id);
			if(empty($result)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/reward/'.$rate_section);
			}
			$data['form_title']		= lang('edit_rate_'.$type);
			$data['id']	 			= encode($id);		
			$data['reward_point']	= $result->reward_point;
			$data['amount_rate']	= $result->amount_rate;
			$data['card_id']		= $result->card_id;
			$data['created_at']		= $result->created_at;
		}
		Template::set('data',$data);
		Template::set_view('reward_rate_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store save reward card in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	private function save_rates($type=0,$id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($id)) {
			$data['id']  = $id;
			// get reward rate details
			$card_data = $this->reward_model->get_reward_pointrates($id);
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('card_id', 'lang:card_type', 'trim|required');	
		$this->form_validation->set_rules('amount_rate', 'lang:rate_amount', 'trim|required');	
		$this->form_validation->set_rules('reward_point', 'lang:reward_ponts', 'trim|required');		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		}
		
		// set post values in store bundle
		$data['rate_type']   	= $type;
		$data['card_id']   		= $this->input->post('card_id');
		$data['amount_rate']    = $this->input->post('amount_rate');
		$data['reward_point']   = $this->input->post('reward_point');
		$data['created_by'] 	= $this -> session -> userdata('user_id');
		// add or update reward rate data into db
		$id = $this->reward_model->save_reward_rate_data($data);
		return $id;
	}
	
	/**
	 * Update status of reward rate point
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_rate_status($type=0,$id='',$status='') {
		$type = decode($type); // decode rate type
		$rate_section = ($type == 1) ? 'earnrate' : 'spendrate';
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$this->reward_model->save_reward_rate_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/reward/'.$rate_section);
	}
    
	/**
	 * Function to display result listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function reward_pool() {
        $reward_pool_filter = $this->input->post('reward_pool_filter');
        $filters = [];
        if(isset($reward_pool_filter)) {
            $filters = [
                'status_type' => (!empty($this->input->post('status_type'))) ? $this->input->post('status_type') : 0,
                'from_date' => (!empty($this->input->post('from_date'))) ? $this->input->post('from_date') : '',
                'to_date' => (!empty($this->input->post('to_date'))) ? $this->input->post('to_date') : '',
                'reward_type' => (!empty($this->input->post('reward_type'))) ? $this->input->post('reward_type') : '',
            ];
        }
        // get listing
        $results = $this->reward_model->get_reward_transaction($filters);
        Template::set('filters',$filters);
        Template::set('results',$results);
        Template::set_view('reward_pool');
        Template::render();
    }//end reward_pool()
    
    /**
     * @reward_customers function use to display all customer list with reward poits
     * @input : null
     * @output : void
     * 
     */
    public function reward_customers() {
        $results = $this->reward_model->get_customers_rewards();
		Template::set('results',$results);
		Template::set_view('reward_customers');
		Template::render();
    }
    
    /*
     * @reward_customers_details function is use to view customer reward detail
     * @input : user_id(int)
     * @output : void
     * 
     **/
    public function reward_customers_details($user_id=0) {
        $filters['user_id'] = $user_id;
        $reward_pool_filter = $this->input->post('reward_pool_filter');
        if(isset($reward_pool_filter)) {
            $filters = [
                'user_id' => $user_id,
                'status_type' => (!empty($this->input->post('status_type'))) ? $this->input->post('status_type') : 0,
                'from_date' => (!empty($this->input->post('from_date'))) ? $this->input->post('from_date') : '',
                'to_date' => (!empty($this->input->post('to_date'))) ? $this->input->post('to_date') : '',
                'reward_type' => (!empty($this->input->post('reward_type'))) ? $this->input->post('reward_type') : '',
            ];
        }
        
        // get listing
        $results = $this->reward_model->get_reward_transaction($filters);
        Template::set('filters',$filters);
		Template::set('results',$results);
		Template::set_view('reward_customers_details');
		Template::render();
    }
	//--------------------------------------------------------------------

    /*
     * @rewardCampaign 
     * */
    public function rewardCampaign() {
        $results = $this->reward_model->get_reward_campaign();
        Template::set('results',$results);
		Template::set_view('reward_campaign');
		Template::render();
    }
    //--------------------------------------------------------------------
    
    /*
     * @addRewardc 
     * */
    public function addRewardCampaign($id="") {
        //echo '<pre>'; print_r($_POST);die;
        if((isset($_POST['save'])) && empty($id)) {
			if($this->saveCampaign()) {    
				Template::set_message(lang('campaign_insert_msg'),'success');
				redirect('admin/settings/reward/rewardCampaign');
			}
		} else if(isset($_POST['save'])) {
			if($this->saveCampaign('update',$id)) {
				Template::set_message(lang('campaign_update_msg'),'success');
				redirect('admin/settings/reward/rewardCampaign');
			}
		}
        $campaign_id = (!empty($id)) ? decode($id) : 0; // decode Campaign id
		
        $data = array();        
        $data['campaign_title'] = lang('add_reward_campaign');
        $data['campaign_id']    = $campaign_id;
        $data['name']   = '';
        $data['reward_points']  = '';
        $data['short_description']  = '';
        $data['long_description']   = '';
        $data['start_date'] = '';
        $data['end_date']   = '';
        $data['action_type']    = 0;
        $data['action_sub_type']    = 0;
        $data['product_id']    = '';
        $data['action_recurrence']    = 0;
        $data['image']  = '';
        $data['banner_image']  = '';
        if(!empty($campaign_id)) {
            $data['campaign_title'] = lang('edit_reward_campaign');
            $campaign = $this->reward_model->get_reward_campaign($campaign_id);
            $data['name']   = (!empty($campaign->name)) ? $campaign->name : '';
            $data['reward_points']  = (!empty($campaign->reward_points)) ? $campaign->reward_points : 0;
            $data['short_description']  = (!empty($campaign->short_description)) ? $campaign->short_description : '';
            $data['long_description']   = (!empty($campaign->long_description)) ? $campaign->long_description : '';
            $data['start_date'] = (!empty($campaign->start_date)) ? $campaign->start_date : '';
            $data['end_date']   = (!empty($campaign->end_date)) ? $campaign->end_date : '';
            $data['action_type']    = (!empty($campaign->action_type)) ? $campaign->action_type : 0;
            $data['action_sub_type']    = (!empty($campaign->action_sub_type)) ? $campaign->action_sub_type : 0;
            $data['product_id']    = (!empty($campaign->product_id)) ? $campaign->product_id : '';
            $data['action_recurrence']    = (!empty($campaign->action_recurrence)) ? $campaign->action_recurrence : 0;
            $data['image']  = (!empty($campaign->image)) ? $campaign->image : '';
            $data['banner_image']  = (!empty($campaign->banner_image)) ? $campaign->banner_image : '';
        }
        
        Template::set('data',$data);
		Template::set_view('add_reward_campaign');
		Template::render();
    }
    //--------------------------------------------------------------------
    
    /**
     * @saveCampaign
     * 
     */
	public function saveCampaign($type = 'insert',$campaign_id=0) {
        //apply validation
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('reward_points', 'lang:reward_points', 'trim|required|numeric');
		$this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
		//$this->form_validation->set_rules('long_description', 'lang:long_description', 'trim|required');	
		$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');
		$this->form_validation->set_rules('end_date', 'lang:end_date', 'trim|required');
		$this->form_validation->set_rules('action_type', 'lang:action_type', 'trim');        
		$this->form_validation->set_rules('action_sub_type', 'lang:action_sub_type', 'trim');        
		$this->form_validation->set_rules('action_recurrence', 'lang:action_recurrence', 'trim');        
        //check the validation 
        if($this->form_validation->run() === FALSE) {
			Template::set_message(validation_errors(),'error');
			return FALSE;
		}
        
        // upload the campaign image
        if (isset($_FILES['image']) && ($_FILES['image']['name']) != '' ) {
            $data['image'] = $this->to_upload_banner_image('image');
        }
        // upload the campaign image
        if (isset($_FILES['banner_image']) && ($_FILES['banner_image']['name']) != '' ) {
            $data['banner_image'] = $this->to_upload_banner_image('banner_image');
        }
        
        //get post parameter
        $data['id'] = $campaign_id;
        $data['name']     = (!empty($this->input->post('name'))) ? $this->input->post('name') : '';
        $data['reward_points']     = (!empty($this->input->post('reward_points'))) ? $this->input->post('reward_points') : 0;
        
        $data['start_date'] = (!empty($this->input->post('start_date'))) ? date("Y-m-d",strtotime($this->input->post('start_date'))) : '';
        $data['end_date'] = (!empty($this->input->post('end_date'))) ? date("Y-m-d",strtotime($this->input->post('end_date'))) : '';
        
        $data['action_type']    = (!empty($this->input->post('action_type'))) ? $this->input->post('action_type') : 0;
        $data['action_sub_type']    = (!empty($this->input->post('action_sub_type'))) ? $this->input->post('action_sub_type') : 0;
        $data['product_id']    = (!empty($this->input->post('product_id'))) ? $this->input->post('product_id') : 0;
        
        $data['action_recurrence']    = (!empty($this->input->post('action_recurrence'))) ? $this->input->post('action_recurrence') : 0;
        $data['short_description']  = (!empty($this->input->post('short_description'))) ? $this->input->post('short_description') : '';
        $data['long_description']   = (!empty($this->input->post('long_description'))) ? $this->input->post('long_description') : '';
        $data['created_by'] = 1;
        if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
            //print_r($data);die;
			$campaign_id = $this->reward_model->add_campaign($data);
		}
		else if ($type == 'update') {
			$campaign_id = $this->reward_model->add_campaign($data);
		}
		return $campaign_id;
    
    }
    //--------------------------------------------------------------------
    
    /**
     * @campaignStatus
     *
     */
    public function campaignStatus($id='',$status='') {
		$data['id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->reward_model->add_campaign($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('admin/settings/reward/rewardCampaign');
	}
    //--------------------------------------------------------------------
    
    /**
     * @deleteCampaign
     *
     */
	public function deleteCampaign($id='') {
		$data['id'] = decode($id);
		$data['is_deleted'] = 1;
        //print_r($data);die;
		$id = $this->reward_model->add_campaign($data);
		Template::set_message(lang('message_status_offer'),'success');
		redirect('admin/settings/reward/rewardCampaign');
	}
    //--------------------------------------------------------------------

    /**
     * @checkexistCampaign
     *
     */
	public function checkexistCampaign() {
        $data['action_type']    = (!empty($this->input->post('action_type'))) ? $this->input->post('action_type') : 0;
        $data['campaign_id']    = (!empty($this->input->post('campaign_id'))) ? $this->input->post('campaign_id') : 0;
        $data['action_sub_type']    = (!empty($this->input->post('action_sub_type'))) ? $this->input->post('action_sub_type') : 0;
        echo $this->reward_model->check_campaign($data);die;
	}
    //--------------------------------------------------------------------

	/**
	 * Function to store save reward catalogue in database
	 * @input : id
	 * @return: void
	 * @access: private
	 */
	public function save_catalogue_test() {
	
	}
	
}//end Settings

// End of Admin Reward Controller
/* Location: ./application/core_modules/reward/controllers/settings.php */
