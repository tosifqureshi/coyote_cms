<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Reward Model
 *
 * The central way to access and perform CRUD on Reward.
 *
 * @package    Avaitor
 * @subpackage Modules_Reward
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Reward_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        $this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // define tables
		$this->reward_rules_table = 'reward_rules';
		$this->reward_config_table = 'reward_config';
		$this->reward_catalogue_table = 'reward_catalogue';
		$this->reward_catalogue_products_table = 'reward_catalogue_products';
		$this->reward_condition_table = 'reward_rule_conditions';
		$this->reward_card_table = 'reward_cards';
		$this->reward_card_condition_table = 'reward_card_conditions';
		$this->reward_point_rate_table = 'reward_point_rates';
		$this->reward_transaction = 'reward_transaction';
		$this->reward_campaign_table = 'reward_campaign';
		$this->users = 'users';
        $this->stores_table = 'ava_stores_log';
        $this->OUTPTBL = 'OUTPTBL';
        $this->OutlTbl = 'OutlTbl';
        $this->PRODTBL = 'PRODTBL';
        $this->CODETBL = 'CODETBL';
    }

	/**
	 * Get listing of reward rules log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_reward_rule_results($id = 0) {
		$this -> read_db -> select('*');
		if ($id) {
			$this -> read_db -> where("id", $id);
			$result = $this -> read_db -> get($this->reward_rules_table);
			return $result -> row();
		}
		$this -> read_db -> where("is_deleted", 0);
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->reward_rules_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to manage insertion and updation data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->reward_rules_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->reward_rules_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Get saved reward configuration details
	 * @input : null
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_reward_config() {
		$this -> read_db -> select('*');
		$result = $this -> read_db -> get($this->reward_config_table);
		return $result -> row();
	}
	
	/**
	 * Function to manage reward configuration insertion and updation
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_config_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->reward_config_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->reward_config_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Get listing of reward catalogue
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_reward_catalogue($id = 0) {
		$this -> read_db -> select('*');
		if ($id) {
			$this -> read_db -> where("id", $id);
			$result = $this -> read_db -> get($this->reward_catalogue_table);
			return $result -> row();
		}
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->reward_catalogue_table);
		return $result -> result_array();
	}
    
	/**
	 * Get listing of reward catalogue product
	 * @input : id
	 * @output: array
	 */
	function get_reward_catalogue_product($catalogue_id,$id = 0) {
		$this -> read_db -> select('*');
        $this -> read_db -> where("catalogue_id", $catalogue_id);
		if ($id) {
			$this -> read_db -> where("id", $id);
			$result = $this -> read_db -> get($this->reward_catalogue_products_table);
			return $result -> row();
		}
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->reward_catalogue_products_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to manage reward catalogue insertion and updation
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_catalogue_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->reward_catalogue_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->reward_catalogue_table, $data);
			return $this->read_db->insert_id();
		}
	}
    
	/**
	 * Function to manage reward catalogue products insertion and updation
	 * @input : data
	 * @output: int
	 */
	public function save_catalogue_product_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->reward_catalogue_products_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->reward_catalogue_products_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Function to remove catalogue entry from db
	 * @input : id
	 * @output: void
	 */
	public function remove_reward_catalogue($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->reward_catalogue_table); 
			$return = true;
		}
		return $return;
	}
	/**
	 * Function to remove catalogue product entry from db
	 * @input : id
	 * @output: void
	 */
	public function remove_reward_catalogue_product($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->reward_catalogue_products_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Function to remove reward rule from db
	 * @input : id
	 * @output: void
	 */
	public function remove_reward_rule($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->reward_rules_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Function to manage conditions insertion
	 * @input : data
	 * @output: int(id)
	 * @access: public
	 */
	public function add_conditions($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->db->where('id', $data['id']);
			$this->db->update($this->reward_condition_table, $data);
			return $data['id'];
		} else { // add data
			$this->db->insert($this->reward_condition_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Function to get condition list
	 * @input : int rule_id
	 * @output: array
	 * @access: public
	 */
	public function get_conditions($rule_id=0,$id=0) {
		if(!empty($rule_id) || !empty($id)) {
			$this -> read_db -> select('*');
			if(!empty($id)) {
				$this -> read_db -> where("id", $id);
				$result = $this -> read_db -> get($this->reward_condition_table);
				return $result -> row();
			} else {
				$this -> read_db -> order_by('id', 'asc');
				$this -> read_db -> where(array('rule_id'=>$rule_id));
				$result = $this -> read_db -> get($this->reward_condition_table);
				return $result -> result_array();
			}
		}	else {
			return false;
		}
	}
	
	/**
	 * Function to remove condition
	 * @input : int id
	 * @output: bool
	 * @access: public
	 */
	public function remove_rule_condition($id=0) {
		$return = false;
		if(!empty($id)) {
			$this->db->where('id', $id);
			$this->db->delete($this->reward_condition_table);
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Get listing of reward loyalty cards
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_reward_cards($id = 0) {
		$this -> read_db -> select('*');
		$this->read_db->where('is_deleted', 0);
		if ($id) {
			$this -> read_db -> where("id", $id);
			$result = $this -> read_db -> get($this->reward_card_table);
			return $result -> row();
		}
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->reward_card_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to manage reward card insertion and updation
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_reward_card_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->reward_card_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->reward_card_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
    /**
     * @check_card_name check the card name already exists or not
     * @input : array
     * @output: int
     */
    public function check_card_name($data) {
        $id = (!empty($data['card_id'])) ? $data['card_id'] : 0;
        $card_name = (!empty($data['card_name'])) ? $data['card_name'] : '';
        $this->db->select('*');
        $this->db->from($this->reward_card_table);
        $this->db->like('card_name', $card_name);
        $this->db->where('id !=', $id);
        $this->db->where('is_deleted', 0);
        $recordset=$this->db->get();
		$data=$recordset->result();
		$count=count($data);
		if(!empty($count)){
			return true;
		}else{
			return false;
		}
    }
    
    /**
     * @check_card_purchase_amount check the card purchase amount
     * @input : array
     * @output: int
     */
    public function check_card_purchase_amount($data) {
        $id = (!empty($data['card_id'])) ? $data['card_id'] : 0;
        $max_purchase_amount = (!empty($data['max_purchase_amount'])) ? $data['max_purchase_amount'] : 0;
        $min_purchase_amount = (!empty($data['min_purchase_amount'])) ? $data['min_purchase_amount'] : 0;
        $sql 		= "select * from ava_".$this->reward_card_table." where id != '".$id."' AND is_deleted = '0' AND ((min_purchase_amount BETWEEN ".$min_purchase_amount." AND ".$max_purchase_amount.") OR (max_purchase_amount BETWEEN ".$min_purchase_amount." AND ".$max_purchase_amount."))";
		$query 		= $this->db->query($sql);
		return $query->num_rows();
    }
    
    /**
     * 
     * 
     **/
    public function get_min_purchase_amount() {
        $this->db->select('max_purchase_amount');
        $this->db->order_by('max_purchase_amount', 'desc');
        $result_set = $this->db->get($this->reward_card_table);
        $result = $result_set -> row();
        $max_purchase_amount = (!empty($result->max_purchase_amount)) ? $result->max_purchase_amount : 0;
        return $max_purchase_amount+1;
    }
    
	
	/**
	 * Function to manage card conditions insertion
	 * @input : data
	 * @output: int(id)
	 * @access: public
	 */
	public function add_card_conditions($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->db->where('id', $data['id']);
			$this->db->update($this->reward_card_condition_table, $data);
			return $data['id'];
		} else { // add data
			$this->db->insert($this->reward_card_condition_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Function to get condition list
	 * @input : int card_id
	 * @output: array
	 * @access: public
	 */
	public function get_card_conditions($card_id=0,$id=0) {
		if(!empty($card_id) || !empty($id)) {
			$this -> read_db -> select('*');
			if(!empty($id)) {
				$this -> read_db -> where("id", $id);
				$result = $this -> read_db -> get($this->reward_card_condition_table);
				return $result -> row();
			} else {
				$this -> read_db -> order_by('id', 'asc');
				$this -> read_db -> where(array('card_id'=>$card_id));
				$result = $this -> read_db -> get($this->reward_card_condition_table);
				return $result -> result_array();
			}
		}	else {
			return false;
		}
	}
	
	/**
	 * Function to remove card condition
	 * @input : int id
	 * @output: bool
	 * @access: public
	 */
	public function remove_card_condition($id=0) {
		$return = false;
		if(!empty($id)) {
			$this->db->where('id', $id);
			$this->db->delete($this->reward_card_condition_table);
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Function to get condition list
	 * @input : int rate_type
	 * @output: array
	 * @access: public
	 */
	public function get_reward_pointrates($rate_type=0,$id=0) {
		if(!empty($rate_type) || !empty($id)) {
			$this -> read_db -> select($this->reward_point_rate_table.'.*');
			if(!empty($id)) {
				$this -> read_db -> where($this->reward_point_rate_table.'.id',$id);
				$result = $this -> read_db -> get($this->reward_point_rate_table);
				return $result -> row();
			} else {
				$this -> read_db -> select($this->reward_card_table.'.card_name');
				$this -> read_db -> where($this->reward_point_rate_table.'.rate_type',$rate_type);
				$this -> read_db -> join($this->reward_card_table, $this->reward_card_table.'.id = '.$this->reward_point_rate_table.'.card_id','left');
				$this -> read_db -> order_by($this->reward_point_rate_table.'.id', 'asc');
				$result = $this -> read_db -> get($this->reward_point_rate_table);
				return $result -> result_array();
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Function to manage reward rate point insertion and updation
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_reward_rate_data($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->read_db->where('id', $data['id']);
			$this->read_db->update($this->reward_point_rate_table, $data);
			return $data['id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->reward_point_rate_table, $data);
			return $this->read_db->insert_id(); 
		}
	}
    
    /**
     * @get_reward_transaction is use to list all type the rewarded
     * @input : array
     * @ouput : array
     */
    public function get_reward_transaction($filters=array()) {
        $user_id = (!empty($filters['user_id'])) ? $filters['user_id'] : 0;
        $status = (!empty($filters['status_type'])) ? $filters['status_type'] : '';
        $reward_type = (!empty($filters['reward_type'])) ? $filters['reward_type'] : '';
        $from_date = (!empty($filters['from_date'])) ? date('Y-m-d 00:00:00',strtotime($filters['from_date'])) : '';
        $to_date = (!empty($filters['to_date'])) ? date('Y-m-d 23:59:59',strtotime($filters['to_date'])) : '';
        
        $this -> read_db -> select($this->reward_transaction.'.*'); 
        $this -> read_db -> select($this->users.'.email,firstname,lastname');
        if(!empty($status)){
            $this -> read_db -> where($this->reward_transaction.'.status',$status);
        } 
        if(!empty($reward_type)){
            $this -> read_db -> where($this->reward_transaction.'.transaction_type',$reward_type);
        } 
        if(!empty($from_date) && !empty($to_date)) {
            $this-> read_db ->where($this->reward_transaction.'.transaction_date >=', $from_date);
            $this-> read_db ->where($this->reward_transaction.'.transaction_date <=', $to_date);
        }
        if(!empty($user_id)) {
            $this -> read_db -> where($this->reward_transaction.'.user_id',$user_id);
        }
        $this -> read_db -> join($this->users, $this->users.'.id = '.$this->reward_transaction.'.user_id','left');
        $result = $this -> read_db -> get($this->reward_transaction);
        return $result -> result_array();
    }
    
    /**
     * @get_customers_rewards is use to fetch customers rewards and details
     * @intput user_id(int)
     * @output array
     */
    public function get_customers_rewards() {
        
        $this -> read_db -> select($this->reward_transaction.'.id, SUM(balance_point) AS total_point'); 
        $this -> read_db -> select($this->users.'.id as user_id,email,firstname,lastname');
        $this -> read_db -> where($this->reward_transaction.'.status',2);
        $this -> read_db -> join($this->users, $this->users.'.id = '.$this->reward_transaction.'.user_id','left');
        $this -> read_db -> group_by($this->users.'.id');
        $result = $this -> read_db -> get($this->reward_transaction);
        return $result -> result_array();
    }
    
    /**
     * @get_reward_campaign is use to fetch get all the campaign list
     * @intput null
     * @output array
     */
    public function get_reward_campaign($id = '') {
        $this -> read_db -> select($this->reward_campaign_table.'.*'); 
        if ($id) {
			$this -> read_db -> where($this->reward_campaign_table.'.id', $id);
			$result = $this -> read_db -> get($this->reward_campaign_table);
			return $result -> row();
		}
        $this -> read_db -> where($this->reward_campaign_table.'.is_deleted',0);
        $result = $this -> read_db -> get($this->reward_campaign_table);
        return $result -> result_array();
    }
    
    /**
     * @add_campaign
     *
     */
    function add_campaign($data=array()) {
		if(!empty($data['id'])) {
			$this->db->where('id', $data['id']);
			$this->db->update($this->reward_campaign_table, $data);
			return $data['id'];
		} else {
			$this->db->insert($this->reward_campaign_table, $data);
			return $this->db->insert_id();
		}
	}
    
    /**
     * @check_campaign
     *
     */
    function check_campaign($data=array()) {
		$action_type = (!empty($data['action_type'])) ? $data['action_type'] : 0;
		$action_sub_type = (!empty($data['action_sub_type'])) ? $data['action_sub_type'] : 0;
        $campaign_id = (!empty($data['campaign_id'])) ? $data['campaign_id'] : '';
        $this->db->select('*');
        $this->db->from($this->reward_campaign_table);
        $this->db->where('action_type', $action_type);
        $this->db->where('action_sub_type', $action_sub_type);
        $this->db->where('is_deleted', 0);
        $this->db->where('id !=', $campaign_id);
        $recordset=$this->db->get();
		$data=$recordset->result();
        //echo $this->db->last_query();die;
		$count=count($data);
		if(!empty($count)){
			return false;
		}else{
			return true;
		}
	}
    
    /**
     * 
     */
    function get_store_list() {
        $this->read_db->select('*');
        $this->read_db->from($this->stores_table);
        $this->read_db->where(array('status'=>'1','display_on_app'=>'1'));
        $this->read_db->order_by('store_name', 'asc');
        $result = $this->read_db->get();
        $return = array();
		if($result->num_rows() > 0){
            $return[''] = 'Select store';	
			foreach($result -> result_array() as $row){
				$return[$row['ms_store_id']] = $row['store_name'];			
			}
		}
		return $return;
    }
    
    /**
     * 
     */
    function get_department_list($store_id = 0) {
        $department_query = "SELECT CODE_KEY_NUM, CODE_DESC FROM " . $this->OUTPTBL . " join " . $this->PRODTBL . "  on Outp_Product = Prod_Number join " . $this->CODETBL . "  on CODE_KEY_NUM = PROD_DEPARTMENT where  OUTP_OUTLET = $store_id and OUTP_STATUS = 'Active' and CODE_KEY_TYPE = 'DEPARTMENT' and PROD_NATIONAL = 0 GROUP BY CODE_KEY_NUM, CODE_DESC ORDER BY CODE_DESC";
        $department_result = $this->mssql_db->query($department_query);
		return $department_result -> result_array();
    }
    
    /**
     * 
     */
    public function get_department_products($department_id = 0,$store_id= 0,$keyword1 = '') {
        $product_query = "SELECT Prod_Number, Prod_Desc,PROD_POS_DESC FROM  " .$this->OUTPTBL. " JOIN " .$this->PRODTBL. " on Outp_Product = Prod_Number JOIN " .$this->CODETBL. " on CODE_KEY_NUM = PROD_DEPARTMENT WHERE  OUTP_OUTLET = $store_id AND CODE_KEY_NUM = $department_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND CODE_KEY_TYPE = 'DEPARTMENT' AND PROD_NATIONAL = 0 AND Prod_Desc like '%$keyword1%'";
        $product_result = $this->mssql_db->query($product_query);
		return $product_result -> result_array();
    }
    
	/**
	 * Function to get product details from product id
	 * @input : product_id
	 * @output: obj array
	 */
	public function get_product_data($product_id) {
		// get product's result
        $query = "SELECT Prod_Number, Prod_Desc, CODE_KEY_NUM, CODE_DESC, OUTL_OUTLET, OUTL_Name_On_App FROM  " .$this->OUTPTBL. " JOIN " .$this->OutlTbl. " on OUTL_OUTLET = OUTP_OUTLET JOIN " .$this->PRODTBL. " on Outp_Product = Prod_Number JOIN " .$this->CODETBL. " on CODE_KEY_NUM = PROD_DEPARTMENT WHERE  OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND CODE_KEY_TYPE = 'DEPARTMENT' AND Prod_Number = $product_id";
		$result = $this->mssql_db->query($query);
		return $result -> result_array();
	}
}//end Reward_model
