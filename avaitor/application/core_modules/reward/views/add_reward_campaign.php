<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $campaign_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php echo form_open_multipart('admin/settings/reward/addRewardCampaign/'.$campaign_id,'id="add_offer_form" class=" form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Campaign name input start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('campaign_name');?><span class="red">*</span></label>
									<?php
									$data	= array('name'=>'name', 'value'=>set_value('name',$name), 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_campaign_name'));
									echo form_input($data);
									?>
								</div>
								<!-- Campaign name input end here -->
								
								<!-- Campaign reward points input start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('reward_points');?><span class="red">*</span></label>
									<?php
									$data	= array('name'=>'reward_points' ,'id'=>'reward_points', 'value'=>set_value('reward_points',$reward_points), 'class'=>'number required form-control form-control-line', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_price'));
									echo form_input($data);
									?>
								</div>
								<!-- Campaign reward points input end here -->
								
								<!-- Start / end date fields start here -->
								<div class="row form-group form-material">
									<div class="col-md-6">
										<label for="co_start_date"><?php echo lang('start_date');?><span class="red">*</span></label>
										<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date" title="<?php echo lang('tooltip_offer_start_time');?>" readonly="readonly">
									</div>
										
									<div class="col-md-6">
										<label for="co_end_date"><?php echo lang('end_date');?><i style="color:red;">*</i></label>
										<input type="text" name="end_date" class="form-control required" value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date" title="<?php echo lang('tooltip_offer_end_time');?>" readonly="readonly">
									</div>
								</div>
								<!-- Start / end date fields end here -->
								
								<!-- Action type dropdown start here -->
								<div class="form-group form-material row">
									<label class="control-label col-md-12">
										<h4> <?php echo lang('action_type');?></h4>
									</label>
									<div class="col-md-6">
										<?php
										$data	= array('name'=>'action_type','id'=>'action_type', 'class'=>'required form-control', 'data-toggle'=>'tooltip', 'data-placement'=>'right');
										echo form_dropdown($data,['0'=>'None','1'=>'Invite Friends','2'=>'Share','3'=>'Sync contact'],set_value('action_type',$action_type));?>
									</div>
								</div>
								<!-- Action type dropdown end here -->
								
								<!-- Action sub type dropdown start here -->
								<div class="form-group form-material row" id="action_sub_type_div">
									<label class="control-label col-md-12">
										<h4> <?php echo lang('action_sub_type');?></h4>
									</label>
									<div class="col-md-6">
										<?php
										$data	= array('name'=>'action_sub_type','id'=>'action_sub_type', 'class'=>'required form-control', 'data-toggle'=>'tooltip', 'data-placement'=>'right');
										echo form_dropdown($data,['0'=>'Select action sub type','1'=>'Application','2'=>'Product'],set_value('action_sub_type',$action_sub_type));
										?>
									</div>
								</div>
								<!-- Action sub type dropdown end here -->
								
								<!-- Reward product id input start here -->
								<div class="form-group form-material"  id="product_id_div">
									<label class=""><?php echo lang('product_id');?><span class="red">*</span></label>
									  <?php
										$data = array('name'=>'product_id','id'=>'product_id', 'value'=> isset($product_id) ? $product_id : '' , 'class'=>'form-control form-control-line required digits');
										echo form_input($data);?>
								</div>
								<!-- Reward product id input end here -->
								
								<!-- Action recurring time input start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('action_recurring_times');?><span class="red">*</span></label>
									<?php
									$data	= array('name'=>'action_recurrence' ,'id'=>'action_recurrence', 'value'=>set_value('action_recurrence',$action_recurrence), 'class'=>'digits required form-control form-control-line', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_price'));
									echo form_input($data);
									?>
								</div>
								<!-- Action recurring time input end here -->
								
								<!-- Short description start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('short_description');?><span class="red">*</span></label>
									<?php
									$data	= array('name'=>'short_description', 'value'=>isset($short_description) ? $short_description : '','class'=> 'form-control form-control-line required','rows'=>3);
									echo form_textarea($data);
									?>
								</div>
								<!-- Short description end here -->
								
								<!-- Long description start here -->	
								<div class="form-group html-editor">
									<label><?php echo lang('long_description');?></label>
									<?php
									$data = array('name'=>'long_description', 'class'=>'mceEditor redactor form-control', 'value'=>isset($long_description) ? $long_description : '');
									echo form_textarea($data);?>
								</div>
								<!-- Long description end here -->
							</div>	
							<div class="col-md-6">
								<div class="form-group ">
									<label><?php echo lang('campaign_image');?> (only 200 X 200 allowed)</label>
									<div class="row">
										<div class="col-md-4 ">
											<div class="img-box">
												<?php
												// set banner image action
												$image_val = (isset($image) && !empty($image)) ? $image :'';
												$img_1_display = (isset($image_val) && !empty($image_val)) ? '' :'dn';
												if(!empty($image_val)) {
													$image_src = base_url('uploads/reward_images/'.$image);
													$image_src_doc = $this->config->item('document_path').'uploads/reward_images/'.$image; 
													if(!file_exists($image_src_doc)){
														$image_src = base_url('uploads/No_Image_Available.png');
													}
												} else {
													$image_src = base_url('uploads/No_Image_Available.png');
												} ?>
												<div class="cell-img" id="image_cell">
													<img id="snw_image_picker" src="<?php echo $image_src; ?>"/>
												</div>
												<?php
												$data	= array('name'=>'image', 'id'=>'image', 'value'=>$image_val, 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
												echo form_upload($data);

												// set banner image value in hidden type
										$data = array('name'=>'image_val', 'value'=>$image_val,'id'=>'image_val','type'=>'hidden');
										echo form_input($data);
												?>
											</div>	
										</div>	
									</div>
									<label><?php echo lang('banner_image');?> (only 4:3 allowed)</label>
									<div class="row">
										<div class="col-md-4 ">
											<div class="img-box">
												<?php
												// set banner image action
												$banner_image_val = (isset($banner_image) && !empty($banner_image)) ? $banner_image :'';
												$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
												if(!empty($banner_image_val)) {
													$banner_image_src = base_url('uploads/reward_images/'.$banner_image);
													$banner_image_src_doc = $this->config->item('document_path').'uploads/reward_images/'.$banner_image; 
													if(!file_exists($banner_image_src_doc)){
														$banner_image_src = base_url('uploads/No_Image_Available.png');
													}
												} else {
													$banner_image_src = base_url('uploads/No_Image_Available.png');
												} ?>
												<div class="cell-img" id="banner_image_cell">
													<img id="banner_image_picker" src="<?php echo $banner_image_src; ?>"/>
												</div>
												<?php
												$data	= array('name'=>'banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val, 'style'=>'display:none', 'onchange'=>'banner_url_image(this)');
												echo form_upload($data);

												// set banner image value in hidden type
										$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
										echo form_input($data);
												?>
											</div>	
										</div>	
									</div>	
									<div class="row">
										<div class="col-md-8">
											<div class="alert red-alert alert-danger alert-dismissible ">
												<i class="icon fa fa-warning"></i>
												Upload up to 3MB images. 
											</div>	
										</div>	
									</div>
									<?php 
									// set banner image value in hidden type
									$data = array('name'=>'image_val', 'value'=>'','id'=>'image_val','type'=>'hidden');
									echo form_input($data);
									// set banner image value in hidden type
									$data = array('name'=>'campaign_id', 'value'=>$campaign_id,'id'=>'campaign_id','type'=>'hidden');
									echo form_input($data);?>
								</div>
							</div>	
						</div>	
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/reward/rewardCampaign' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	// MAterial Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});
	
	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});
	
	$(document).ready(function() {
        
        <?php if($action_type == 2) { ?>
            $('#action_sub_type_div').show();
        <?php } else { ?>
            $('#action_sub_type_div').hide();
        <?php } ?>
        
        <?php if($action_sub_type == 2) { ?>
            $('#product_id_div').show();
        <?php } else { ?>
            $('#product_id_div').hide();
        <?php } ?>
        
            //add rules for decimal
            $.validator.addMethod("decimalpoints", function(value, element) {
                return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
            }, "You must include two decimal places.");
        
		$("#add_offer_form").validate({
            rules: {
                reward_points: {
                    required: true,
                    decimalpoints: true
                }
            }            
		});




			/* 
	| -----------------------------------------------------
	| Handle form posting validation
	| -----------------------------------------------------
	*/
	$("#add_offer_form").submit(function( event ) {
		
		// set start date for calculate day diffrence
		var start_date	=  $( "#start_date" ).val().split(' ');
		var startdateSplit = start_date[0].split('-');
		var new_start_date = startdateSplit[0] + '-' + startdateSplit[1] + '-' + startdateSplit[2];
		// set end date for calculate day diffrence
		var end_date	 =  $( "#end_date" ).val().split(' ');
		var enddateSplit = end_date[0].split('-');
		var new_end_date = enddateSplit[0] + '-' + enddateSplit[1] + '-' + enddateSplit[2];
		// show error if start date less then end date
		if(start_date  != ''&& end_date  != '' && (new_start_date >= new_end_date) ) {
			bootbox.alert("End date must be greater than start date.");
			return false;
		}

		var banner_image_val = $('#banner_image_val').val();
        if(banner_image_val == '') {
            $('#banner_image_cell').addClass('error');
            bootbox.alert("Please upload banner image.");
            return false;
        }

        var image_val = $('#image_val').val();
        if(image_val == '') {
            $('#image_cell').addClass('error');
            bootbox.alert("Please upload campaign image.");
            return false;
        }

		var is_validate = true; 
     	if(is_validate == false) {
            bootbox.alert("Please fill all required condition attributes.");
            return false;
        }
        return;
		event.preventDefault();
	});



        
	});
	
    $("#action_type, #action_sub_type").change(function() {
        //get action type
        var action_type = $('#action_type').val();
        action_type = parseInt(action_type);
        
        //get action subtype
        var action_sub_type = $('#action_sub_type').val();
        action_sub_type = parseInt(action_sub_type);
        
        if(action_type != 0){
            var campaign_id = $('#campaign_id').val();
            $('#loader1').fadeIn();	
            $.ajax ({
                type: "POST",
                data : {action_type:action_type,action_sub_type:action_sub_type,campaign_id:campaign_id},
                url: "<?php echo base_url() ?>admin/settings/reward/checkexistCampaign",
                async: false,
                success: function(data){
                    $('#loader1').fadeOut();
                    if(action_type == 2) {
                        $('#action_sub_type_div').show();
                    } else {
                        $('#action_sub_type_div').hide();
                        $("select#action_sub_type").prop('selectedIndex', 0);
                    }
                    if(action_sub_type == 2) {
                        $('#product_id_div').show();
                    } else {
                        if(data == false) {
                            bootbox.alert('Campaign of selected action type is already exist.');
                            $("select#action_type").prop('selectedIndex', 0);
                            $('#action_sub_type_div').hide();
                            $("select#action_sub_type").prop('selectedIndex', 0);
                            $('#action_sub_type').trigger("change");
                        }
                    }
                },
                error: function(error){
                    $('#loader1').fadeOut();	
                    bootbox.alert(error);
                }
            });
        }
    });
    
    $(document).delegate('#product_id','blur',function(e){
		var product_id = $(this).val();
		$('#loader1').fadeIn();	
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {product_id:product_id},
			url: "<?php echo base_url() ?>admin/settings/reward/getproductdata",
			success: function(data){
				$('#loader1').fadeOut();	
				if(data.product_name != '') {
					//$('#product_name').val(data.product_name);
				} else {
					//$('#product_name').val('');
					$('#product_id').val('');
                    $('#product_id').addClass('error');
                    bootbox.alert('Please enter valid product id');
				}
			}
		});
	});
    
    $("#snw_image_picker").click(function() {
        $("input[name='image']").click();
	});
    
    $("#banner_image_picker").click(function() {
        $("input[name='banner_image']").click();
	});
    function read_url_image(input) {
	
		var pickerId= '#snw_'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();
         
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
						image.onload = function () {
                            var height = parseInt(this.height);
                            var width = parseInt(this.width);

                            if (height != 200 || width != 200) {
                               // $(fieldId).val("");
                                bootbox.alert("Image should be 200 X 200");
                                return false;
                            }
                        };
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
    
    function banner_url_image(input) {
	
		var pickerId= '#'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();
         
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
						image.onload = function () {
                            var height = parseInt(this.height);
                            var width = parseInt(this.width);
                            var r = gcd (width, height);
                            var w = width/r;
                            var h = height/r;
                            //alert(w+':'+h);
                            if (h != 3 || w != 4) {
                                //$(fieldId).val("");
                                bootbox.alert("Image aspect be 4:3");
                                return false;
                            }
                        };
						
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
    function gcd (a, b) {
        return (b == 0) ? a : gcd (b, a%b);
    }
    </script>
