<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<tr class="snw-product-half-side" id="qty_row_<?php echo $row_id;?>">
	<td>
		<?php
		$condition_entity = card_condition_entity();
		$data	= array('name'=>'condition_entity[]','id'=>'condition_entity_'.$row_id, 'class'=>'required condition_input','row_id'=>$row_id);
		echo form_dropdown($data,$condition_entity,set_value('reward_entity',''));
		?>
	</td>
	<td>
		<?php
		$data = array('name'=>'entity_value[]', 'value'=>'','class'=>'required condition_input','id'=>'entity_value_'.$row_id,'row_id'=>$row_id);
		echo form_input($data);?>
	</td>
	<td>
		<span class="romove_row new-prod-row-icon" row_id=<?php echo $row_id;?> snw_product_id='' title="<?php echo lang('product_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
	</td>
</tr>


