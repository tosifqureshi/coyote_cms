<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $form_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php 
					$id = isset($id) ? $id : '';
					echo form_open_multipart('admin/settings/reward/addCatalogueProduct/'.$catalogue_id.'/'.$id,'id="add_catalogue_product_form" class=" form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Product Id input start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('product_title');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'product_id','id'=>'product_id', 'value'=> isset($product_id) ? $product_id : '' , 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top');
									echo form_input($data);?>
								</div>
								<!-- Product Id input end here -->
								
								<!-- Product name redonly input start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('product_name');?><span class="red">*</span></label>
									<?php
									$data = array('name'=>'product_name','id'=>'product_name', 'readonly'=>true, 'value'=> isset($product_name) ? $product_name : '' , 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'To select product name, first select product id.', 'Placeholder'=>'To select product name, first select product id.');
									echo form_input($data);?>
									<div id="suggesstion-box"></div>
								</div>
								<!-- Product name redonly input end here -->
								
								<!-- Store dropdown start here -->
								<div class="form-group row ">
									<label class="control-label col-md-12">
										<?php echo lang('store');?><span class="red">*</span>
									 </label>
									<div class="col-md-6">
										<?php
										// set store id value
										$store_id = (!empty($ms_store_id)) ? explode(',',$ms_store_id) : '';
										//$stores = [''=>'Select Stores'];  
										if(!empty($stores_array)) {
											foreach($stores_array as $store) {
												$OUTL_OUTLET = (!empty($store['OUTL_OUTLET'])) ? $store['OUTL_OUTLET'] : '';
												$OUTL_Name_On_App = (!empty($store['OUTL_Name_On_App'])) ? $store['OUTL_Name_On_App'] : '';
												$stores[$OUTL_OUTLET] = $OUTL_Name_On_App;
											}
										}
										$data	= array('name'=>'store_id[]','id'=>'store_id','class'=>'required form-control','multiple'=>'multiple', 'data-toggle'=>'tooltip', 'data-placement'=>'right');
										echo form_dropdown($data,$stores,set_value('store_id',$store_id));?>
									</div>	
								</div>	
								<!-- Store dropdown end here -->
								
								<!-- Department dropdown start here -->
								<div class="form-group row " id="department_div">
									<label class="control-label col-md-12">
										<?php echo lang('department');?><span class="red">*</span>
									 </label>
									<div class="col-md-6">
										<?php
										// set store id value
										$department_id = (!empty($department_id)) ? explode(',',$department_id) : '';
										if(!empty($departments_array)) {
											foreach($departments_array as $depart) {
												$CODE_KEY_NUM = (!empty($depart['CODE_KEY_NUM'])) ? $depart['CODE_KEY_NUM'] : '';
												$CODE_DESC = (!empty($depart['CODE_DESC'])) ? $depart['CODE_DESC'] : '';
												$departments[$CODE_KEY_NUM] = $CODE_DESC;
											}
										}
										//
										$data	= array('name'=>'department_id[]','multiple'=>'multiple','id'=>'department_id','class'=>'required form-control','data-toggle'=>'tooltip', 'data-placement'=>'right');
										echo form_dropdown($data,$departments,set_value('department_id',$department_id));?>
									</div>
									<input type="hidden" name="department_name" id="department_name" value="<?php echo isset($department_name) ? $department_name : ''?>">
								</div>	
								<!-- Department dropdown end here -->
								
								<!-- Start / end date fields start here -->
								<div class="row form-group form-material">
									<div class="col-md-6">
										<label for="co_start_date"><?php echo lang('start_date');?><span class="red">*</span></label>
										<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date" title="<?php echo lang('tooltip_offer_start_time');?>" readonly="readonly">
									</div>
										
									<div class="col-md-6">
										<label for="co_end_date"><?php echo lang('end_date');?><i style="color:red;">*</i></label>
										<input type="text" name="end_date" class="form-control required" value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date" title="<?php echo lang('tooltip_offer_end_time');?>" readonly="readonly">
									</div>
								</div>
								<!-- Start / end date fields end here -->
								
								<!-- Reward points input start here -->
								<div class="row form-group form-material">
									<div class="col-md-6">
										<label class=""><?php echo lang('catalogue_points');?><span class="red">*</span></label>
										<?php
										$data = array('name'=>'redeem_points','id'=>'redeem_points', 'value'=> isset($redeem_points) ? $redeem_points : '1' , 'class'=>'form-control form-control-line required decimalpoints', 'min'=>'1');
										echo form_input($data);?>
									</div>
								</div>
								<!-- Reward points input end here -->
								
								<!-- Add extra price start here -->
								<div class="form-group form-material">
									<label class="control-label">
										<?php echo lang('is_extra_price');?><span class="red">*</span>
									</label>
									<div class="demo-checkbox">
										<?php
										$data = array( 'name' => 'add_extra_price', 'id' => 'add_extra_price','class' =>'filled-in chk-col-light-blue','value' => '1', 'checked'=> !empty($add_extra_price) ? 'checked' : ''  );
										echo form_checkbox($data);?>
										<label for="add_extra_price"></label>
									</div>	
								</div>
								<!-- Add extra price start here -->
								
								<div class="form-group row form-material" id='extra_price_div'>
									<!-- Extra redeeem points input start here -->
									<div class="col-md-6">
										<label class=""><?php echo lang('points_with_redeem_point');?><span class="red">*</span></label>
										 <?php
										$data = array('name'=>'redeem_points_with_extra_price','id'=>'redeem_points_with_extra_price', 'value'=> isset($redeem_points_with_extra_price) ? $redeem_points_with_extra_price : '1' , 'class'=>'form-control form-control-line required decimalpoints', 'min'=>'1');
										echo form_input($data);?>
									</div>
									<!-- Extra redeeem points input end here -->
									
									<!-- Extra price input start here -->
									<div class="col-md-6">
										<label class=""><?php echo lang('extra_price');?><span class="red">*</span></label>
										 <?php
										$data = array('name'=>'extra_price','id'=>'extra_price', 'value'=> isset($extra_price) ? $extra_price : '1' , 'class'=>'form-control form-control-line required', 'min'=>'1');
										echo form_input($data);?>
									</div>
									 <!-- Extra price input end here -->
								</div> 
								
								<!-- Short description start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('short_description');?></label>
									<?php
									$data	= array('name'=>'short_description', 'value'=>isset($short_description) ? $short_description : '','class'=> 'form-control form-control-line','rows'=>3);
									echo form_textarea($data);
									?>
								</div>
								<!-- Short description end here -->
								
								<!-- Long description start here -->	
								<div class="form-group html-editor">
									<label><?php echo lang('description');?></label>
									<?php
									$data = array('name'=>'description', 'class'=>'mceEditor redactor form-control', 'value'=>isset($description) ? $description : '');
									echo form_textarea($data);?>
								</div>
								<!-- Long description end here -->
							</div>
							
							<div class="col-md-6">
								<div class="form-group ">
									<label><?php echo lang('product_image');?> (only 200 X 200 allowed)</label>
									<div class="row">
										<div class="col-md-4 ">
											<div class="img-box">
												<?php
												// set banner image action
												$banner_image_val = (isset($banner_image) && !empty($banner_image)) ? $banner_image :'';
												$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
												if(!empty($banner_image_val)) {
													$banner_image_src = base_url('uploads/reward_images/'.$banner_image);
													$title = lang('tooltip_offer_image');
													$banner_image_src_doc = $this->config->item('document_path').'uploads/reward_images/'.$banner_image; 
													if(!file_exists($banner_image_src_doc)){
														$banner_image_src = base_url('uploads/No_Image_Available.png');
														$title = "No Image Available";
													}
												} else {
													$title = lang('tooltip_image');
													$banner_image_src = base_url('uploads/No_Image_Available.png');
												} ?>
												<div class="cell-img" id="banner_image_cell">
													<img id="snw_banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
												</div>
												<?php
												$data	= array('name'=>'banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
												echo form_upload($data);
												// set banner image value in hidden type
												$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
												echo form_input($data);?>
											</div>		
										</div>		
									</div>		
									<div class="row">
										<div class="col-md-8">
											<div class="alert red-alert alert-danger alert-dismissible ">
												<i class="icon fa fa-warning"></i>
												Upload up to 3MB images. 
											</div>	
										</div>	
									</div>
								</div>		
							</div>		
						</div>
						<div class="row pb-3 float-right">
							  <input type="hidden" name="catalogue_id" id="catalogue_id" value="<?php echo isset($catalogue_id) ? $catalogue_id : 0?>">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/reward/create_catalogue' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>		
					<?php echo form_close();?>
				</div>	
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    // MAterial Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
	
	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
    
	$(document).ready(function() {
        
        // extra redeem pay amount hide
        $('#extra_price_div').hide();
        
        $('#department_id').change(function() {
            var department_id = $(this).val();
            var old_department_name = $('#department_name').val();
            if(department_id != '') {
                var department_name = $("#department_id option:selected").text();
                if(old_department_name != '') {
                    old_department_name = old_department_name+','+department_name;
                } else {
                    old_department_name = department_name;
                }
            } 
            $("#department_name").val(old_department_name);
        });
        
        <?php if((!empty($add_extra_price)) && $add_extra_price == 1) { ?>
            $('#extra_price_div').show();
        <?php } else { ?>
            $('#extra_price_div').hide();
        <?php } ?>
        
        $('#add_extra_price').change(function() {
            if ($('#add_extra_price').is(":checked")) {
                $('#extra_price_div').show();
            } else {
                $('#extra_price_div').hide();
            }
        });
            
        //add rules for decimal
        $.validator.addMethod("decimalpoints", function(value, element) {
            return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
        }, "You must include two decimal places.");
            
		$("#add_catalogue_product_form").validate({
            rules: {
                redeem_points: {
                    required: true,
                    decimalpoints: true
                }
            }
		});
	});
	
	/* 
	| -----------------------------------------------------
	| Handle form posting validation
	| -----------------------------------------------------
	*/
	$( "#add_catalogue_product_form" ).submit(function( event ) {
        // set start date for calculate day diffrence
		var start_date	=  $( "#start_date" ).val().split(' ');
		var startdateSplit = start_date[0].split('-');
		var new_start_date = startdateSplit[1] + '/' + startdateSplit[0] + '/' + startdateSplit[2];
		// set end date for calculate day diffrence
		var end_date	 =  $( "#end_date" ).val().split(' ');
		var enddateSplit = end_date[0].split('-');
		var new_end_date = enddateSplit[1] + '/' + enddateSplit[0] + '/' + enddateSplit[2];
		// show error if start date less then end date
		if(start_date  != ''&& end_date  != '' && (new_start_date >= new_end_date) ) {
			bootbox.alert("End date must be greater than start date.");
			return false;
		}
        
		// show error if banner image not exist
		var banner_image_val = $('#banner_image_val').val();
		if(banner_image_val == '') {
			$('#banner_image_cell').addClass('error');
			bootbox.alert("Please upload banner image.");
			return false;
		}
        var product_id = $("#product_id").val();
        if(product_id == '') {
			bootbox.alert("Please select proper product name.");
			return false;
        }
		return;
		event.preventDefault();
	});
	
	$("#snw_banner_image_picker").click(function() {
		$("input[name='banner_image']").click();
	});
	
	function read_url_banner_image(input) {
	
		var pickerId= '#snw_'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();
         
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
						image.onload = function () {
                            var height = parseInt(this.height);
                            var width = parseInt(this.width);

                            if (height != 200 || width != 200) {
                                //$(fieldId).val("");
                                bootbox.alert("Image should be 200 X 200");
                                return false;
                            }
                        };
                        //alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	
	$(document).delegate('#product_id','change',function(e){
		var product_id = $(this).val();
		get_product_data(product_id);
	});
	
	function get_product_data(product_id) {
        if(product_id != '') {
	        $.ajax ({
				type: "POST",
				dataType:'jSon',
				data : {product_id:product_id},
				url: "<?php echo base_url() ?>admin/settings/reward/getproductdata/0",
                beforeSend: function(){
                    $('#loader1').fadeIn();
                    $('#product_name').val('');
                    $('#department_id').html('');
                    $('#department_name').val('');
                    $('#store_id').html('');
                },
				success: function(data){
                    $('#loader1').fadeOut();
					if(data.Prod_Desc == '' || data.Prod_Desc == undefined) {
                        $('#product_id').addClass('error');
                        $('#product_name').val('');
                        $('#department_id').html('');
                        $('#department_name').val('');
                        $('#store_id').html('');
					} else {
						$('#product_name').val(data.Prod_Desc);
                        $("#department_id").html(data.departments);
                        $("#store_id").html(data.stores);
					}
				}
			});
		}
	}

</script>

