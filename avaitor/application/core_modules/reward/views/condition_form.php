<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<tr class="snw-product-half-side" id="qty_row_<?php echo $row_id;?>">
	<td>
		<?php
		$product_id = array('name'=>'product_id[]', 'value'=>'','class'=>'product_input required condition_input','id'=>'product_id_'.$row_id,'row_id'=>$row_id,'lable_name'=>'product_id');
		echo form_input($product_id);
		?>
	</td>
	<td>
			<?php
			$product_name = array('name'=>'product_name[]', 'value'=>'','readonly'=>true,'id'=>'product_name_'.$row_id,'row_id'=>$row_id);
			echo form_input($product_name);?>
		</td>
	<td>
		<?php
		$product_qty = array('name'=>'product_qty[]', 'value'=>'','class'=>'required condition_input','id'=>'product_qty_'.$row_id,'row_id'=>$row_id,'lable_name'=>'product_qty');
		echo form_input($product_qty);
		?>
	</td>
	<td>
		<span class="romove_row new-prod-row-icon" row_id=<?php echo $row_id;?> snw_product_id='' title="<?php echo lang('product_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
	</td>
</tr>


