<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('point&reward') ?></h4>
				<a href="<?php echo site_url("admin/settings/reward/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_rule'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="max-width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				
				<div class="table-responsive m-t-40">
					<table id="reward_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('rule_name'); ?></th>
								<th><?php echo lang('start_date'); ?></th>
								<th><?php echo lang('end_date'); ?></th>
								<th><?php echo lang('reward_points'); ?></th>
								<th><?php echo lang('status'); ?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($results) && is_array($results) && count($results)>0):
								foreach($results as $result) :
									// set encoded id
									$id = encode($result['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td class="mytooltip tooltip-effect-1 desc-tooltip">
											<span class="tooltip-content clearfix">
												<span class="tooltip-text">
													<?php 
													$start_date_time = date('Y-m-d',strtotime($result['start_date']));
													$expire_at = lang('NA');
													if(!empty($result['end_date'])) {
														$end_date_time = date('Y-m-d',strtotime($result['end_date'].' +1 days'));
														$diff = get_date_diffrence($start_date_time, $end_date_time);
														$expire_at = date('F j, Y',strtotime($result['end_date']));
													}
													
													echo '<b>'.lang('rule_name').' : </b>'.$result['rule_name'];
													echo '<br>';
													echo '<b>'.lang('start_date').' : </b>'.date('F j, Y',strtotime($result['start_date'])) ;
													echo '<br>';
													echo '<b>'.lang('end_date').'  : </b>'.$expire_at;
													echo '<br>';
													echo (isset($diff)) ? $diff : '';?>
												</span>	
											</span>	
											<span class="text-ttip">
												<a href="<?php echo site_url("admin/settings/reward/create/".$id);?>"><?php echo (strlen($result['rule_name']) > 20) ? substr($result['rule_name'],0,20).'...' :  $result['rule_name'];?></a>
											</span>
										</td>
										<td><?php echo date('F j, Y',strtotime($result['start_date']));?></td>
										<td><?php echo $expire_at;?></td>
										<td><?php echo (!empty($result['reward_points'])) ? $result['reward_points'] : 0;?></td>
										<td>
											<?php
											// set change status url
											$status_url = site_url("admin/settings/reward/change_rule_status/".$id.'/'.encode($result['status']));
											if($result['status'] == '0') { ?>
												<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
												<?php
											} else {
												?>
												<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
											} ?>
										</td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/reward/delete_rule/".$id);?>')"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php 
								$i++;
								endforeach ; 
							endif; ?>
						</tbody>
					</table>
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

<script>
	$(document).ready(function() {
		jQuery('#reward_table').DataTable();
	});
</script>

