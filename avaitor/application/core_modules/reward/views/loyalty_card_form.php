<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
 <div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $form_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php 
					$card_id = isset($id) ? $id : '';$card_image_val;
					echo form_open_multipart('admin/settings/reward/create_card/'.$card_id,'id="add_card_form" class="  form-with-label"');?>
						<div class="row">
							<div class="col-md-6">
								<!-- Title input start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('card_name');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'card_name','id'=>'card_name', 'value'=> isset($card_name) ? $card_name : '' , 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('beacon_code'));
									echo form_input($data);?>
									<input type="hidden" name="card_id" value="<?php echo isset($card_id) ? $card_id : '';?>" id="card_id" >
								</div>
								<!-- Title input end here -->
								
								<div class="row form-group form-material">
									<!-- Card discount input start here -->
									<div class="col-md-6">
										<label class="m-t-20"><?php echo lang('card_discount');?></label>
										<?php 
										// define campaign types
										$discount_amounts = array();
										for($i=0;$i<=100;$i++) {
											$discount_amounts[$i] = $i;
										}
										$data = array('name'=>'card_discount','id'=>'card_discount','class'=>'form-control');
										echo form_dropdown($data,$discount_amounts,set_value('card_discount',(!empty($card_discount) ? $card_discount : 0)));?>
									</div>
									<!-- Card discount input end here -->
									<!-- Expiry date field start here -->
									<?php 
									if(decode($card_id) != 1){ ?>
										<div class="col-md-6">
											<label class="m-t-20"><?php echo lang('expire_date');?><i style="color:red;">*</i></label>
											<input type="text" name="end_date" class="form-control required" value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date">
										</div>
									<?php } ?>	
									<!-- Expiry date field end here -->
								</div>
								
								<div class="form-group row form-material">
									<div class="col-md-6">
										<label class=""><?php echo lang('min_purchase_amt');?><i style="color:red;">*</i></label>
										<?php
										$data = array('name'=>'min_purchase_amount','id'=>'min_purchase_amount', 'value'=> isset($min_purchase_amount) ? $min_purchase_amount : 0, 'class'=>'form-control form-control-line required number', 'readonly'=>true);
										echo form_input($data);?>
									</div>
									<div class="col-md-6 form-material">
										<label class=""><?php echo lang('max_purchase_amt');?><i style="color:red;">*</i></label>
										<?php
										$data = array('name'=>'max_purchase_amount','id'=>'max_purchase_amount', 'value'=> isset($max_purchase_amount) ? $max_purchase_amount : 0, 'class'=>'form-control form-control-line required number');
										echo form_input($data);?>
									</div>
								</div>		
								
								<div class="form-group row form-material">
									<div class="col-md-6">
										<label class=""><?php echo 'Click here to choose logo color';?><i style="color:red;">*</i></label>
										<?php
										$data = array('name'=>'logo_color','id'=>'logo_color', 'value'=> isset($logo_color)?$logo_color:'', 'class'=>'form-control jscolor');
										echo form_input($data);?>
									</div>
									<div class="col-md-6 form-material">
										<label class=""><?php echo 'Click here to choose font color';?><i style="color:red;">*</i></label>
										<?php
										$data = array('name'=>'font_color','id'=>'font_color', 'value'=> isset($font_color)?$font_color:'', 'class'=>'form-control jscolor');
										echo form_input($data);?>
									</div>
								</div>
								
								<div class="form-group row form-material">
									<div class="col-md-6">
										<label class=""><?php echo 'Click here to choose card name font color';?><i style="color:red;">*</i></label>
										<?php
										$data = array('name'=>'font_color_2','id'=>'font_color_2', 'value'=> isset($font_color_2)?$font_color_2:'', 'class'=>'form-control jscolor');
										echo form_input($data);?>
									</div>
									<div class="col-md-6">
										<label class=""><?php echo 'Click here to choose background color';?><i style="color:red;">*</i></label>
										<?php
										$data = array('name'=>'background_color','id'=>'background_color', 'value'=> isset($background_color)?$background_color:'', 'class'=>'form-control jscolor');
										echo form_input($data);?>
									</div>
								</div>
								
								<!-- Description start here -->
								<div class="form-group  html-editor">
									<label><?php echo lang('description');?></label>
									<?php
									$data	= array('name'=>'description','class'=>'mceEditor redactor form-control', 'value'=>isset($description) ? $description : '');
									echo form_textarea($data);?>
								</div>
								<!-- Description end here -->
								
							</div>	
							
							<div class="col-md-6">
								<div class="form-group ">
									<label><?php echo lang('card_image');?> (only 200 X 200 allowed)</label>
									<div class="row">
										<div class="col-md-4 ">
											<div class="img-box">
												<?php
												// set banner image action
												$card_image_val = (isset($card_image) && !empty($card_image)) ? $card_image :'';
												$img_1_display = (isset($card_image_val) && !empty($card_image_val)) ? '' :'dn';
												if(!empty($card_image_val)) {
													$card_image_src = base_url('uploads/reward_images/'.$card_image);
													$card_image_src_doc = $this->config->item('document_path').'uploads/reward_images/'.$card_image; 
													if(!file_exists($card_image_src_doc)){
														$card_image_src = base_url('uploads/No_Image_Available.png');
													}
												} else {
													$card_image_src = base_url('uploads/upload.png');
												} ?>
												<div class="cell-img" id="card_image_cell">
													<img id="snw_card_image_picker" src="<?php echo $card_image_src; ?>"/>
												</div>
												<?php
												$data	= array('name'=>'card_image', 'id'=>'card_image', 'value'=>$card_image_val, 'style'=>'display:none', 'onchange'=>'read_url_card_image(this)');
												echo form_upload($data);
												// set banner image value in hidden type
												$data = array('name'=>'card_image_val', 'value'=>$card_image_val,'id'=>'banner_image_val','type'=>'hidden');
												echo form_input($data);
												?>
											</div>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-8">
											<div class="alert red-alert alert-danger alert-dismissible ">
												<i class="icon fa fa-warning"></i>
												Upload up to 3MB images. 
											</div>	
										</div>	
									</div>	
								</div>	
							</div>	 
						</div>	
						<div class="row pb-3 float-right">
							<?php
							// set current date in hidden field 
							$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
							echo form_input($data);?>
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/reward/loyaltycards' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/jscolor/jscolor.min.js'); ?>"></script>
<script type="text/javascript">
	// Initialize material Date picker    
	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format: "MM-YYYY",
		viewMode: "months", 
		minViewMode: "months",
	});
	
	$(document).ready(function() {
        //add rules for decimal
        $.validator.addMethod("decimalpoints", function(value, element) {
            return this.optional(element) || /^\d{0,10}(\.\d{0,2})?$/i.test(value);
        }, "Please enter a valid number.");
        
        
        $("#add_card_form").validate({
            rules: {
                max_purchase_amount: {
                    required: true,
                    decimalpoints: true
                }
            }
        });
    });
	
    /*
        check the uniques of purchase amount
    */
    
    $(document).on('blur', '#max_purchase_amount', function() {
        comparepurchasedamount();
    });
    
    function comparepurchasedamount() {
        var max_purchase_amount = parseInt($('#max_purchase_amount').val());
        var min_purchase_amount = parseInt($('#min_purchase_amount').val());
        var card_id = '<?php echo $card_id; ?>';
        if(max_purchase_amount <= min_purchase_amount) {
            bootbox.alert('Maximum purchased amount should be greater then minimum purchased amount.');
            $("#max_purchase_amount").val('');
            return false;
        } else {
            $.ajax({
                type:'POST',
                dataType: 'json',
                url: "<?php echo site_url('/admin/settings/reward/check_card_purchase_amount');?>",
                data:{ max_purchase_amount: $.trim(max_purchase_amount),min_purchase_amount: $.trim(min_purchase_amount),card_id: card_id },
                beforeSend: function(){
                    $('#loader1').fadeIn();
                },
                success:function(data){
                    $('#loader1').fadeOut();
                    if(data) {
                        bootbox.alert('Amount range ('+min_purchase_amount+' - '+max_purchase_amount+') is not valid.');
                        $("#max_purchase_amount").val('0');
                        return false;
                    }
                }
            });
        }
    }
    
    /*
        check reward point uniqueness 
    */
    
    $(document).on('change', '#card_name', function() {
		checkcardname();
	});
    function checkcardname() {
        var card_name = $("#card_name").val();
        if(card_name) {
        var card_id = '<?php echo $card_id; ?>';
            $.ajax({
                type:'POST',
                dataType: 'json',
                url: "<?php echo site_url('/admin/settings/reward/check_card_name');?>",
                data:{ card_name: $.trim(card_name),card_id: card_id },
                beforeSend: function(){
                 $('#loader1').fadeIn();
                },
                success:function(data){
                    $('#loader1').fadeOut();
                    if(data) {
                        bootbox.alert('Card name "'+card_name+'" is already exist.');
                        $("#card_name").val('');
                    }
                }
            });
        }
    }
    
	/* 
	| -----------------------------------------------------
	| Handle form posting validation
	| -----------------------------------------------------
	*/
	$( "#add_card_form" ).submit(function( event ) {
        checkcardname();
        comparepurchasedamount();
		// show error if banner image not exist
		var card_image_val = $('#card_image_val').val();
		if(card_image_val == '') {
			$('#card_image_cell').addClass('error');
			bootbox.alert("Please upload card image.");
			return false;
		}
		return;
		event.preventDefault();
	});
	
	$("#snw_card_image_picker").click(function() {
		$("input[name='card_image']").click();
	});
	
	function read_url_card_image(input) {
		var pickerId= '#snw_'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
    
    /* 
	| -----------------------------------------------------
	| Init start and end date
	| -----------------------------------------------------
	*/
	$(document).ready(function() {
		if(jQuery('#start_date_div1').length > 0)  { 
			jQuery('#start_date_div1').datetimepicker({
				pickTime: false,
				startDate: new Date(), 
			});
		}

		if(jQuery('#end_date_div1').length > 0)  { 
			jQuery('#end_date_div1').datetimepicker({
                format: "MM-yyyy",
                viewMode: "months", 
                minViewMode: "months",
				pickTime: false,
				startDate: new Date(), 
			});
		}
	});
</script>


