<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('reward_campaign') ?></h4>
				<a href="<?php echo site_url("admin/settings/reward/addRewardCampaign"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_reward_campaign'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="max-width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="reward_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('campaign_image'); ?></th>
								<th><?php echo lang('name'); ?></th>
								<th><?php echo lang('reward_points'); ?></th>
								<th><?php echo lang('offer_status'); ?></th>
								<th><?php echo lang('action'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(!empty($results)) {
								foreach($results as $result){
									// set encoded id
									$id = (!empty($result['id'])) ? encode($result['id']) : ''; ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
											<?php 
											$image = (!empty($result['image'])) ? $result['image'] : '';
											if(!empty($image)) {
											$image = base_url('uploads/reward_images/'.$image); 							
											?>
												<img  id="imageresource" src="<?php echo $image;?>" width="110" height="90" offer_name="<?php echo $image;?>">
											<?php } else { ?>
												No Image Available
											<?php }?>
										</td>
										<td>
										<?php
											$name = (!empty($result['name'])) ? ucfirst($result['name']) : "";
										?>
											<a href="<?php echo site_url("admin/settings/reward/addRewardCampaign"); echo '/' . ($id);?>"><?php if(strlen($name) > 20) { echo substr($name,0,20).'...'; } else { echo $name; }?></a>
										</td>
										<td><?php echo (!empty($result['reward_points'])) ? $result['reward_points'] : 0;?></td>
										<td>
										<?php 
										$status = (!empty($result['status'])) ? $result['status'] : 0;
										if(!empty($status)) {
											?>
											<a class="common-btn active-btn" onclick="changeStatus('<?php echo $id; ?>','<?php echo encode($status); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
										} else {
											?>
											<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo $id; ?>','<?php echo encode($status); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>                                    
											<?php
										} ?>
										</td>
										<td>
											<a class="common-btn delete-btn" href="javascript:;" onclick="deleteCampaign('<?php echo $id; ?>')"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php 
								$i++;
								} 
							} ?>
						</tbody>
					</table>
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

<script>
    
    $(document).ready(function() {
		jQuery('#reward_table').DataTable();
	});
	
    function changeStatus(id,status) {
		bootbox.confirm("Are you sure want to change the offer status?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("admin/settings/reward/campaignStatus"); ?>/"+id+"/"+status;
			}
		});
	}
    
    function deleteCampaign(id) {
		bootbox.confirm("Are you sure want to delete the offer?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("admin/settings/reward/deleteCampaign"); ?>/"+id;
			}
		});
	}
</script>

