<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('reward_campaign') ?></h4>
				<a href="<?php echo site_url("admin/settings/reward/addRewardCampaign"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_reward_campaign'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="max-width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="reward_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th class="no-sort"><?php echo lang('banner_image');?></th>
								<th><?php echo lang('title');?></th>
								<th><?php echo lang('status');?></th>
								<th class="no-sort"><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
						
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($results) && is_array($results) && count($results)>0):
								foreach($results as $result) :
									// set encoded id
									$id = encode($result['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
											<?php if(isset($result['banner_image']) && !empty($result['banner_image'])) {
												$banner_image = $result['banner_image'];
												$banner_image = (!empty($banner_image)) ? base_url('uploads/reward_images/'.$banner_image) : base_url('uploads/No_Image_Available.png');?>
												<img  id="imageresource" src="<?php echo $banner_image;?>" width="110" height="90" title="<?php echo $result['title'];?>">
											<?php } ?>
										</td>
										<td>
											<a href="<?php echo site_url("admin/settings/reward/create_catalogue/".$id);?>"><?php echo (strlen($result['title']) > 20) ? substr($result['title'],0,20).'...' :  $result['title'];?></a>
										</td>

										<td>
											<?php
											// set change status url
											$status_url = site_url("admin/settings/reward/change_catalogue_status/".$id.'/'.encode($result['status']));
											if($result['status'] == '0') { ?>
												<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
												<?php
											} else { ?>
												<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
											} ?>
										</td>
										<td>
											<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/reward/delete_catalogue/".$id);?>')"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php 
								$i++;
								endforeach ;
							endif; ?>
						</tbody>
					</table>	
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

<script>
	$(document).ready(function() {
		jQuery('#reward_table').DataTable();
	});
</script>

