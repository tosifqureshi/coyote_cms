<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo lang('configuration');?></h4>
				<?php 
				$id = isset($id) ? $id : '';
				echo form_open_multipart('admin/settings/reward/storeconfig/'.$id,'id="config_form" class="add_form"'); ?>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#earning_points_tab" role="tab"> <span class=""><?php echo lang('activity_earning_points');?></span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#purchase_earning_points_tab" role="tab"> <span class=""><?php echo lang('purchase_earning_points');?></span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#purchase_spending_points_tab" role="tab"> <span class=""><?php echo lang('purchase_spending_points');?></span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#notifications_tab" role="tab"> <span class=""><?php echo lang('notifications');?></span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<!-- Start general configuration panel -->
						<div class="tab-pane active my-5" id="earning_points_tab" role="tabpanel">
							<!-- registration input start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('registration');?></label>
									<?php
									$data = array('name'=>'registration_points','id'=>'registration_points', 'value'=> isset($results->registration_points) ? $results->registration_points : '' , 'class'=>'form-control form-control-line digits');
									echo form_input($data);
									?>
								</div>
							</div>
							<!-- registration input end here -->
							<!-- birthday input start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('birthday');?></label>
									<?php
									$data = array('name'=>'birthday_points','id'=>'birthday_points', 'value'=> isset($results->birthday_points) ? $results->birthday_points : '' , 'class'=>'form-control form-control-line digits');
									echo form_input($data);?>
								</div>
							</div>
							<!-- birthday input end here -->
							<!-- view vip deals input start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('view_vip_deals');?></label>
									<?php
									$data = array('name'=>'view_vip_deals_points','id'=>'view_vip_deals_points', 'value'=> isset($results->view_vip_deals_points) ? $results->view_vip_deals_points : '' , 'class'=>'form-control form-control-line digits');
									echo form_input($data);?>
								</div>
							</div>
							<!-- view vip deals input end here -->
							<!-- view last min deal input start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('view_last_min_deal');?></label>
									<?php
									$data = array('name'=>'view_last_min_deal_points','id'=>'view_last_min_deal_points', 'value'=> isset($results->view_last_min_deal_points) ? $results->view_last_min_deal_points : '' , 'class'=>'form-control form-control-line digits');
									echo form_input($data);?>
								</div>
							</div>
							<!-- view last min deal input end here -->
							<!-- make purchase input start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('points_per_day');?></label>
									<?php
									$data = array('name'=>'points_per_day','id'=>'points_per_day', 'value'=> isset($results->points_per_day) ? $results->points_per_day : '' , 'class'=>'form-control form-control-line digits');
									echo form_input($data);?>
								</div>
							</div>
							<!-- make purchase input end here -->  
						</div>	
						<!-- End general configuration panel -->
						
						<!-- Start earning points panel -->
						<div class="tab-pane  p-20" id="purchase_earning_points_tab" role="tabpanel">
							 <table class="table table-bordered th-border">
								<?php if(!empty($result_reward_card)) { ?>
									<tr>
										<th><?php echo lang('card_name');?></th>
										<th><?php echo lang('rate');?></th>
										<th><?php echo lang('reward_points');?></th>
									</tr>
									<?php
									foreach($result_reward_card as $reward_card) {
										$reward_card_id = (!empty($reward_card['id'])) ? $reward_card['id'] : 0;?>
										<tr>
											<td><?php echo (!empty($reward_card['card_name'])) ? $reward_card['card_name'] : ''?></td>
											<td><?php echo $this->config->item('currency_sign');?>1</td>
											<td>
												<?php $reward_point_rate = (!empty($reward_card['reward_point_rate'])) ? $reward_card['reward_point_rate'] : 0;?>
												<?php
												$data = array('name'=>'reward_point_rate['.$reward_card_id.']','id'=>'reward_point_rate'.$reward_card_id, 'value'=> $reward_point_rate , 'class'=>'number','maxlength'=>5);
												echo form_input($data);?>	
											</td>
										</tr>
										<?php 
									}
								}?>
							 </table>
						</div>
						<!-- End earning points panel -->
						
						 <!-- Start earning points panel -->
						<div class="tab-pane  p-20" id="purchase_spending_points_tab" role="tabpanel">
							<table class="table table-bordered th-border">
								<?php
								if(!empty($result_reward_card)) { ?>
									<tr>
										<th><?php echo lang('card_name');?></th>
										<th><?php echo lang('reward_point');?></th>
										<th><?php echo sprintf(lang('spend_rate'), $this->config->item('currency_sign'));?></th>
									</tr>
									<?php
									foreach($result_reward_card as $reward_card) {
										$reward_card_id = (!empty($reward_card['id'])) ? $reward_card['id'] : 0;?>
										<tr>
											<td><?php echo (!empty($reward_card['card_name'])) ? $reward_card['card_name'] : ''?></td>
											<td>1</td>
											<td>
												<?php 
												$reward_spend_rate = (!empty($reward_card['reward_spend_rate'])) ? $reward_card['reward_spend_rate'] : 0;
												$data = array('name'=>'reward_spend_rate['.$reward_card_id.']','id'=>'reward_spend_rate'.$reward_card_id, 'value'=> $reward_spend_rate , 'class'=>'number','maxlength'=>5);
												echo form_input($data);
												?>
											</td>
										</tr>
									<?php 
									}
								} ?>
							</table>
						</div>
						 <!-- End earning points panel -->
						 
						 <!-- Start notifications panel -->
						<div class="tab-pane  p-20" id="notifications_tab" role="tabpanel">
							<!-- referral status dropdown start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('notification_status');?></label>
									<?php
									$data = array('name'=>'notification_status','id'=>'notification_status','class'=>'form-control form-control-line');
									echo form_dropdown($data,array('1'=>'Yes','2'=>'No'),set_value('notification_status',(isset($results->notification_status)) ? $results->referral_status : ''));?>
								</div>
							</div>
							<!-- referral status dropdown end here -->
							<!-- notification expiry dropdown start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('notification_expiry');?></label>
									<?php
									$data = array('name'=>'points_expiry_notification','id'=>'points_expiry_notification','class'=>'form-control form-control-line');
									echo form_dropdown($data,array('1'=>'Yes','2'=>'No'),set_value('notification_expiry',(isset($results->points_expiry_notification)) ? $results->points_expiry_notification : ''));?>
								</div>
							</div>
							<!-- notification expiry dropdown end here -->
							<!-- notification birthday dropdown start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('notification_birthday');?></label>
									<?php
									$data = array('name'=>'birthday_notification','id'=>'birthday_notification','class'=>'form-control form-control-line');
									echo form_dropdown($data,array('1'=>'Yes','2'=>'No'),set_value('birthday_notification',(isset($results->birthday_notification)) ? $results->birthday_notification : ''));?>
								</div>
							</div>
							<!-- notification birthday dropdown end here -->
							<!-- notification invite dropdown start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('notification_invite');?></label>
									<?php
									$data = array('name'=>'invite_notification','id'=>'invite_notification','class'=>'form-control form-control-line');
									echo form_dropdown($data,array('1'=>'Yes','2'=>'No'),set_value('invite_notification',(isset($results->invite_notification)) ? $results->invite_notification : ''));?>
								</div>
							</div>
							<!-- notification invite dropdown end here -->
							<!-- notification expiry limit input start here -->
							<div class="col-md-6 form-material">
								<div class="form-group">
									<label><?php echo lang('notification_expiry_limit');?></label>
									<?php
									$data = array('name'=>'before_points_expiry_days','id'=>'before_points_expiry_days', 'value'=> isset($results->before_points_expiry_days) ? $results->before_points_expiry_days : '' , 'class'=>'form-control form-control-line digits','min'=>1);
									echo form_input($data);?>
								</div>
							</div>
							<!-- notification expiry limit input end here -->
						</div>
						<!-- End notifications panel -->		 		   
					</div>
					<div class="row pb-3 float-right">
						 <div class="mx-1">
							<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							<?php
							// set config id in hidden field 
							$data = array('name'=>'config_id','type'=>'hidden','value'=>(isset($results->id)) ? encode($results->id) : '');
							echo form_input($data);?>
						</div>
					</div>	
				<?php echo form_close();?>	   
			</div> 
		</div> 
	</div> 
</div> 

<script type="text/javascript">
	
	$(document).ready(function() {
		$("#config_form").validate({
		});
	});

</script>


