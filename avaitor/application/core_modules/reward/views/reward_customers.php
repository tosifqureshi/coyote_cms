<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$reward_config = [
    '' => 'Select reward type',
    'referral_invitation_points' => 'Referral Invitation Points',
    'birthday' => 'Birthday Points',      
    'registration_points' => 'Registration Points',
    'social_sharing_points' => 'Social Sharing Points',
    'rate_application_points' => 'Rate Application Points',
    'make_purchase_points' => 'Make Purchase Points',
    'view_vip_deals_points' => 'View Vip Deals Points',
    'view_last_min_deal_points' => 'View Last Min Deal Points',
];?>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('acart_menu_customers') ?></h4>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="max-width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="reward_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('name'); ?></th>
								<th><?php echo lang('email'); ?></th>
								<th><?php echo lang('total_points'); ?></th>
								<th><?php echo lang('action'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
								<?php
								$i=1;
								if(isset($results) && is_array($results) && count($results)>0):
								foreach($results as $result) :
									// set encoded id
									$user_id = (!empty($result['user_id'])) ? $result['user_id'] : "";
									if(!empty($user_id)) {
									$id = encode($result['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
										<?php
											  $firstname = (!empty($result['firstname'])) ? ucfirst($result['firstname']) : "";
											  $lastname = (!empty($result['lastname'])) ? ucfirst($result['lastname']) : "";
										echo $firstname.' '.$lastname;
										?></td>
										<td><?php echo (!empty($result['email'])) ? $result['email'] : "NA";?></td>
										<td><?php echo (!empty($result['total_point'])) ? $result['total_point'] : 0;?></td>
										<td><a class="common-btn" href="<?php echo site_url('admin/settings/reward/reward_customers_details/'.$user_id);?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
									</tr>
								<?php 
								$i++;
									}
								endforeach ; 
							endif; ?>
						</tbody>
					</table>
				</div>	
			</div>	
		</div>	
	</div>	
</div>

<script>
	$(document).ready(function() {
		jQuery('#reward_table').DataTable();
	});
    
    $( "#get_record" ).click(function() {
        // get selected campaign type 
        var data = {}
        data['status_type']  = $('#status_type').val();
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('/language/remove_key');?>",
            data: {"data" : charges},
            dataType : "json",
        });
	});
    
    $( "#reward_pool_filter" ).submit(function( event ) {
        var start_date	=  $( "#from_date" ).val();
		var split_start_date	= 	start_date.split("-");
		var new_start_date	= split_start_date[2]+'/'+split_start_date[1]+'/'+split_start_date[0];
        
        var end_date	=  $( "#to_date" ).val();
		var split_end_date	= 	end_date.split("-");
		var new_end_date	= split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0];

        var sDate 		=  new Date(new_start_date);
		var eDate 		=  new Date(new_end_date);
        if((start_date != '' && end_date == '') ||  (start_date == '' && end_date != '')) {
            bootbox.alert("Please enter both the dates.");
            return false;
        } else if(sDate > eDate) {
            bootbox.alert("Start date should be greater then end date.");
            return false;
        }
        return true;
        event.preventDefault();
    });
</script>

