<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$reward_config = [
    '' => 'Select reward type',
    'referral_invitation_points' => 'Referral Invitation Points',
    'birthday' => 'Birthday Points',      
    'registration_points' => 'Registration Points',
    'social_sharing_points' => 'Social Sharing Points',
    'rate_application_points' => 'Rate Application Points',
    'make_purchase_points' => 'Make Purchase Points',
    'view_vip_deals_points' => 'View Vip Deals Points',
    'view_last_min_deal_points' => 'View Last Min Deal Points',
];
$user_id = (!empty($filters['user_id'])) ? $filters['user_id'] : 0;

?>
<div class="well-white">
    <div class="page-header">
        <h1><?php echo lang('customer_details') ?></h1>
    </div>
<div>
<div class="admin-box admin-height">
    <div class="col-md-12 pad-10">
        <div class="control-group half-width">
            <label class="control-label" for="Name">Name :</label>
            <div class="controls">
                <span class="help-inline">
                    <?php
                        echo (!empty($results[0]['firstname'])) ? $results[0]['firstname'] : '';
                    ?>
                </span>
            </div>
        </div>
        <div class="control-group half-width">
            <label class="control-label" for="Name">Total Points :</label>
            <div class="controls">
                <span class="help-inline">5000</span>
            </div>
        </div>
        <div class="control-group half-width">
            <label class="control-label" for="Name">Email :</label>
            <div class="controls">
                <span class="help-inline">
                <?php
                    echo (!empty($results[0]['email'])) ? $results[0]['email'] : '';
                ?>
                </span>
            </div>
        </div>
        <div class="control-group half-width">
            <label class="control-label" for="Name">Total Points Earned:</label>
            <div class="controls">
                <span class="help-inline">3000</span>
            </div>
        </div>
        <div class="control-group half-width">
            <label class="control-label" for="Name">Reward Card Category :</label>
            <div class="controls">
                <span class="help-inline">Gold Card</span>
            </div>
        </div>
        <div class="control-group half-width">
            <label class="control-label" for="Name">Total Points Spend:</label>
            <div class="controls">
                <span class="help-inline">2000</span>
            </div>
        </div>
    </div>
</div>
<div class="admin-box">
    <h3><?php echo lang('transaction_detail') ?>:</h3>
        <div role="grid" class="dataTables_wrapper mtb-8" id="dyntable_wrapper">
            <?php echo form_open_multipart('admin/settings/reward/reward_customers_details/'.$user_id,'id="reward_pool_filter"'); ?>
            <button type="submit" name="reward_pool_filter" class="btn btn-primary pull_right">GO</button>
            <a href="<?php echo site_url('admin/settings/reward/reward_customers_details/'.$user_id);?>" class="btn btn-default pull_right">Reset</a>
            <?php
                $status = (!empty($filters['status_type'])) ? $filters['status_type'] : 0;
                $reward_type = (!empty($filters['reward_type'])) ? $filters['reward_type'] : '';
                $from_date = (!empty($filters['from_date'])) ? $filters['from_date'] : '';
                $to_date = (!empty($filters['to_date'])) ? $filters['to_date'] : '';
            ?>
            <div class="half_date label-sec date_width">
                <label for="start_date">From:<i style="color:red;">*</i></label>
                <div class="form-group input-append date" id="start_date_div">
                    <?php
                    $data	= array('name'=>'from_date', 'data-format'=>'dd-MM-yyyy', 'value'=>set_value('from_date', $from_date),'id'=>'from_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'readonly'=>'readonly', 'class'=>'required w100');
                    echo form_input($data);
                    ?>
                    <span id="start_time_add_on " class="add-on calender-icon"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>
                </div>
            </div>
            <div class="half_date label-sec date_width">
                <label for="start_date">To:<i style="color:red;">*</i></label>
                <div class="form-group input-append date" id="end_date_div">
                    <?php
                    $data	= array('name'=>'to_date', 'data-format'=>'dd-MM-yyyy', 'value'=>set_value('to_date', $to_date),'id'=>'to_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'readonly'=>'readonly', 'class'=>'required w100');
                    echo form_input($data);
                    ?>
                    <span id="start_time_add_on " class="add-on calender-icon"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>
                </div>
            </div>
            <div class="select-status label-sec reward-type">
            <?php
                $data = ['name'=>'reward_type','id'=>'reward_type', 'class'=>'campaign_type date'];
                echo form_dropdown($data,$reward_config,set_value('reward_type',$reward_type),'Reward Type:');
            ?>
            </div>
        </div>
        </form>

    <table class="table table-striped" id="reward_table">
        <thead>
            <tr role="row">
                <th><?php echo lang('sno'); ?></th>
                <th><?php echo lang('reward_type'); ?></th>
                <th><?php echo lang('spend_points'); ?></th>
                <th><?php echo lang('earn_points'); ?></th>
                <th><?php echo lang('total_points'); ?></th>
                <th><?php echo lang('balance_points'); ?></th>
                <th><?php echo lang('transation_date'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i=1;
                if(isset($results) && is_array($results) && count($results)>0):
                    foreach($results as $result):
                        // set encoded id
                        $id = encode($result['id']); ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td>
                            <?php $transaction_type = (!empty($result['transaction_type'])) ? $result['transaction_type'] : "";
                            echo (!empty($reward_config[$transaction_type])) ? $reward_config[$transaction_type] : 'NA';
                            ?></td>
                            <td><?php echo 0;?></td>
                            <td><?php echo (!empty($result['balance_point'])) ? $result['balance_point'] : 0;?></td>
                            <td><?php echo 0;?></td>
                            <td><?php echo (!empty($result['balance_point'])) ? $result['balance_point'] : 0;?></td>
                            <td><?php echo  date('F j, Y',strtotime($result['transaction_date']));?></td>
                        </tr>
                        <?php 
                        $i++;
                    endforeach ; 
                endif; 
            ?>
        </tbody>
    </table>
</div>


<script>
	jQuery('#reward_table').dataTable({
		"aoColumnDefs": [{ "targets": 'no-sort',"bSortable": false}],
		"bJQueryUI": true,
		"aaSorting": [[ 0, "asc" ]],
		"sPaginationType": "full_numbers"
	});
    
    $( "#reward_pool_filter" ).submit(function( event ) {
        var start_date	=  $( "#from_date" ).val();
		var split_start_date	= 	start_date.split("-");
		var new_start_date	= split_start_date[2]+'/'+split_start_date[1]+'/'+split_start_date[0];
        
        var end_date	=  $( "#to_date" ).val();
		var split_end_date	= 	end_date.split("-");
		var new_end_date	= split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0];

        var sDate 		=  new Date(new_start_date);
		var eDate 		=  new Date(new_end_date);
        if((start_date != '' && end_date == '') ||  (start_date == '' && end_date != '')) {
            bootbox.alert("Please enter both the dates.");
            return false;
        } else if(sDate > eDate) {
            bootbox.alert("Start date should be greater then end date.");
            return false;
        }
        return true;
        event.preventDefault();
    });
</script>

