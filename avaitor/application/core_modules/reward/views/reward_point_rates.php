<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="span9 dash-board">
	<div class="well-white">
		<div class="page-header">
			<h1><?php 
			 echo lang('reward_point_rate_'.$type) ?></h1>
			<a href="<?php echo site_url("admin/settings/reward/create_rate/".encode($type)); ?>" role="button" class="btn btn-primary pull-right"><?php echo lang('add_rate_'.$type); ?></a>
		</div>
		<?php			
		if (isset($message) && !empty($message)){
			echo '<div class="alert alert-success">' . $message	 . '</div>';
		} ?>
		<!-- start the bootstrap modal where the image will appear -->
		<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"></h4>
					</div>
					<div class="modal-body">
						<img src="" id="imagepreview" style="max-width: 400px; height: 264px;" >
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end the bootstrap modal where the image will appear -->
		<div role="grid" class="dataTables_wrapper" id="dyntable_wrapper">
			<table id="reward_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">    
				<thead>
					<tr role="row">
						<th><?php echo lang('sno'); ?></th>
						<th><?php echo lang('card_type');?></th>
						<th><?php echo lang('rate');?></th>
						<th><?php echo lang('status');?></th>
					</tr>
				</thead>
				
				<tbody role="alert" aria-live="polite" aria-relevant="all">
					<?php
					$i=1;
					if(isset($results) && is_array($results) && count($results)>0) :
						foreach($results as $result) :
							// set encoded id
							$id = encode($result['id']); ?>
							<tr>
								<td><?php echo $i;?></td>
								<td>
									<a href="<?php echo site_url("admin/settings/reward/create_rate/".encode($type).'/'.$id);?>"><?php echo (!empty($result['card_name'])) ? $result['card_name'] : 'N/A';?></a>
								</td>
								<td>
									<?php 
									if( $result['rate_type'] == 1 ) {
										echo $result['amount_rate'].' $ = '.$result['reward_point'].' '.lang('reward_ponts');
									} else {
										echo $result['reward_point'].' '.lang('reward_ponts').' = '.$result['amount_rate'].' $';
									} ?>
								</td>
								<td>
									<?php
									// set change status url
									$status_url = site_url("admin/settings/reward/change_rate_status/".encode($type).'/'.$id.'/'.encode($result['status']));
									if($result['status'] == '0') { ?>
										<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
									<?php
									} else { ?>
										<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
									<?php
									} ?>
								</td>
							</tr>
						<?php 
						$i++;
						endforeach ;
					endif; ?>
				</tbody>
			</table>                 
		</div>
	</div>
</div>

<script>
	jQuery('#reward_table').dataTable({
		"aoColumnDefs": [{ "targets": 'no-sort',"bSortable": false}],
		"bJQueryUI": true,
		"aaSorting": [[ 0, "asc" ]],
		"sPaginationType": "full_numbers"
	});
</script>

