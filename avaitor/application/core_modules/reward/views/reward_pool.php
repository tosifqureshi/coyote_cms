<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php
$reward_config = [
    '' => 'Select reward type',
    'referral_invitation_points' => 'Referral Invitation Points',
    'birthday' => 'Birthday Points',      
    'registration_points' => 'Registration Points',
    'social_sharing_points' => 'Social Sharing Points',
    'rate_application_points' => 'Rate Application Points',
    'make_purchase_points' => 'Make Purchase Points',
    'view_vip_deals_points' => 'View Vip Deals Points',
    'view_last_min_deal_points' => 'View Last Min Deal Points',
];?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('reward_pool') ?></h4>
				<?php
				echo form_open('admin/settings/reward/reward_pool/','id="reward_pool_filter"'); 
					$status = (!empty($filters['status_type'])) ? $filters['status_type'] : 0;
					$reward_type = (!empty($filters['reward_type'])) ? $filters['reward_type'] : '';
					$from_date = (!empty($filters['from_date'])) ? $filters['from_date'] : '';
					$to_date = (!empty($filters['to_date'])) ? $filters['to_date'] : '';?>
					<div class="flex-box row">
						<div class="form-group col-md-2">
							<label>From:<span class="red">*</span></label>
							<input type="text" name="from_date" class="form-control required" value="<?php echo !empty($from_date) ? $from_date : '';?>" id="from_date">
						</div>
						<div class="form-group col-md-2 ">
							<label>To:<span class="red">*</span></label>
							<input type="text" name="to_date" class="form-control required" value="<?php echo !empty($to_date) ? $to_date : '';?>" id="to_date">
						</div>
						<div class="form-group col-md-3">
							<?php
							$status_types = ['0'=>'Select Status','1'=>'Pending','3'=>'Processing','2'=>'Received'];
							$data = ['name'=>'status_type','id'=>'status_type', 'class'=>'campaign_type date form-control'];
							echo form_dropdown($data,$status_types,set_value('status_type',$status),'Status:');?>
						</div>
						<div class="form-group col-md-3">
							<?php
							$data = ['name'=>'reward_type','id'=>'reward_type', 'class'=>'campaign_type date form-control'];
							echo form_dropdown($data,$reward_config,set_value('reward_type',$reward_type),'Reward Type:');?>
						</div>
						<div class="form-group col-md-1">
							<label></label>
							<button type="submit" name="reward_pool_filter" class="btn waves-effect waves-light btn-block btn-info">Go</button>
						</div>
						<div class="form-group col-md-1">
							<label></label>
							<a href="<?php echo site_url('admin/settings/reward/reward_pool');?>">
								<button  class="btn waves-effect waves-light btn-block btn-info">Reset</button>
							</a>
						</div>
					</div>	  	
				<?php echo form_close();?>	
				
				
				<div class="table-responsive">  
					<table id="reward_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('comment'); ?></th>
								<th><?php echo lang('customer_email'); ?></th>
								<th><?php echo lang('balance_points'); ?></th>
								<th><?php echo lang('transation_date'); ?></th>
								<th><?php echo lang('status'); ?></th>
							</tr>
						</thead>
								
						<tbody role="alert" aria-live="polite" aria-relevant="all">
								<?php
								$i=1;
								if(isset($results) && is_array($results) && count($results)>0):
								foreach($results as $result) :
									// set encoded id
									$id = encode($result['id']); ?>
									<tr>
										<td><?php echo $i;?></td>
										<td>
										<?php $transaction_type = (!empty($result['transaction_type'])) ? $result['transaction_type'] : "";
										echo (!empty($reward_config[$transaction_type])) ? $reward_config[$transaction_type] : 'NA';
										?></td>
										<td><?php echo (!empty($result['email'])) ? $result['email'] : "NA";?></td>
										<td><?php echo (!empty($result['balance_point'])) ? $result['balance_point'] : 0;?></td>
										<td><?php echo  date('F j, Y',strtotime($result['transaction_date']));?></td>
										<td><?php
											if($result['status'] == 2) {
												echo 'Received';
											} else if($result['status'] == 3){
												echo 'Processing';
											} else if($result['status'] == 1) {
												echo 'Pending';
											} ?>
										</td>
									</tr>
								<?php 
								$i++;
								endforeach ;
							 endif; ?>
						</tbody>
					</table>
				</div>		
			</div>	
		</div>	
	</div>	
</div>	

<script>
	// Initialize material Date picker    
	$('#from_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});

	$('#to_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
	
	$(document).ready(function() {
		jQuery('#reward_table').DataTable();
	});
    
    $( "#get_record" ).click(function() {
        // get selected campaign type 
        var data = {}
        data['status_type']  = $('#status_type').val();
        $.ajax({
            type: 'POST',
            url: "<?php echo site_url('/language/remove_key');?>",
            data: {"data" : charges},
            dataType : "json",
        });
	});
    
    $( "#reward_pool_filter" ).submit(function( event ) {
        var start_date	=  $( "#from_date" ).val();
		var split_start_date	= 	start_date.split("-");
		var new_start_date	= split_start_date[2]+'/'+split_start_date[1]+'/'+split_start_date[0];
        var end_date	=  $( "#to_date" ).val();
		var split_end_date	= 	end_date.split("-");
		var new_end_date	= split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0];

        var sDate 		=  new Date(new_start_date);
		var eDate 		=  new Date(new_end_date);
        if((start_date != '' && end_date == '') ||  (start_date == '' && end_date != '')) {
            bootbox.alert("Please enter both the dates.");
            return false;
        } else if(sDate > eDate) {
            bootbox.alert("Start date should be greater then end date.");
            return false;
        }
        return true;
        event.preventDefault();
    });
</script>

