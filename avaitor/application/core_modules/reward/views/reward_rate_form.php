<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="page-header">
    <h1><?php echo $form_title;?></h1>
</div>
<?php 
$rate_id = isset($id) ? $id : '';
echo form_open_multipart('admin/settings/reward/create_rate/'.encode($rate_type).'/'.$rate_id,'id="add_rate_form" class="add_form"'); ?>
	<div class="tabbable ">
		<ul class="nav nav-tabs">
			<li class="active">&nbsp;</li>
			<li class="active"><a href="#content_tab" data-toggle="tab"><?php echo lang('rate_information');?></a></li>
		</ul>
		<div class="tab-content scroll ">
			<!-- Start Content panel -->
			<div class="tab-pane active" id="content_tab">
				<div class="CompetitionS form-wrapper clearboth">
					<!-- Loyalty card input start here -->
					<div class="form-box1 form-box">
						<label for="card_type"><?php echo lang('card_type');?><i style="color:red;">*</i></label>
						<?php
						$loyalty_cards = loyalty_cards();
						$card_id = (!empty($card_id)) ? $card_id : '';
						$data	= array('name'=>'card_id','id'=>'card_id', 'class'=>'required rate-select');
						echo form_dropdown($data,$loyalty_cards,set_value('card_id',$card_id));
						?>
					</div>
					<!-- Loyalty card input end here -->
					
					<!-- Point rate fields start here -->
					<?php
					// set type wise input field name and value
					$rate_first_field_name  = ( $rate_type == 1) ? 'amount_rate' : 'reward_point'; 
					$rate_first_field_val   = ( $rate_type == 1) ? (!empty($amount_rate)) ? $amount_rate : '' : (!empty($reward_point)) ? $reward_point : ''; 
					$rate_second_field_name = ( $rate_type == 1) ? 'reward_point' : 'amount_rate'; 
					$rate_second_field_val  = ( $rate_type == 1) ? (!empty($reward_point)) ? $reward_point : '' : (!empty($amount_rate)) ? $amount_rate : ''; 
					?>
					<div class="full-wrap mb10">
                        <div class="form-box half-wrap mr-36">
                            <label><?php echo lang('rate');?><i style="color:red;">*</i></label>
                            <div class="half_rate">
								<?php
								$data = array('name'=>$rate_first_field_name, 'value'=> isset($rate_first_field_val) ? $rate_first_field_val : '' , 'class'=>'span8 required digits width_100');
								echo form_input($data);?>
								<span class="width_100"><?php echo ( $rate_type == 1 ) ? '$  >>' : lang('reward_ponts').' >>'; ?></span> 
							</div>
                        </div>
                            
                        <div class="form-box half-wrap mr-36">
                            <label>&nbsp;</label>
                            <div class="half_rate">
								<?php
								$data = array('name'=>$rate_second_field_name, 'value'=> isset($rate_second_field_val) ? $rate_second_field_val : '' , 'class'=>'span8 required digits width_100');
								echo form_input($data);?>
								<span class="width_100"><?php echo ( $rate_type == 1 ) ? lang('reward_ponts') : '$'; ?></span>
                            </div>
                        </div>
                    </div>
					<!-- Point rate fields end here -->	
				</div>	
			</div>
			<!-- End Content panel -->
			
		</div>
	</div>
	<div class="text-right mt10 mb10">
		<?php
		// set current date in hidden field 
		$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
		echo form_input($data);?>
		<button type="submit" name="save" class="btn btn-primary"><?php echo lang('form_save');?></button>
		&nbsp;
		<a href='<?php echo base_url().'admin/settings/reward/loyaltycards' ?>'>
			<button type="button" name="cancel" class="btn default"><?php echo lang('form_cancel');?></button>
		</a>
	</div>			
<?php form_close();?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#add_rate_form").validate({		
		});
	});
	
</script>


