<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">          
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $form_title;?></h4>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs customtab2" role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#content_tab" role="tab"> <span class=""><?php echo lang('content');?></span></a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attribute_tab" role="tab"> <span class=""><?php echo lang('conditions');?></span></a> </li>
				</ul>
				<!-- Tab panes -->
				<?php 
				$rule_id = isset($id) ? $id : '';
				echo form_open_multipart('admin/settings/reward/create/'.$rule_id,'id="add_rule_form" class="form-material form-with-label"'); ?>
					<div class="tab-content">
						<!-- Start Content panel -->
						<div class="tab-pane active" id="content_tab" role="tabpanel">
							<div class="p-t-20">
								<div class="row">
									<div class="col-md-6">
										<!-- Rule name input start here -->
										<div class="form-group ">
											<label for="title"><?php echo lang('rule_name');?><span class="red">*</span></label>
											<?php
											$data = array('name'=>'rule_name','id'=>'rule_name', 'value'=> isset($rule_name) ? $rule_name : '' , 'class'=>'form-control form-control-line required');
											echo form_input($data);?>
										</div>
										<!-- Rule name input end here -->
										
										<!-- Start / end date fields start here -->
										<div class="row form-group">
											 <div class="col-md-6">
												<label class="m-t-20"><?php echo lang('start_date');?><span class="red">*</span></label>
												<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date">
											</div>	
										
											<div class="col-md-6">
												<label class="m-t-20"><?php echo lang('end_date');?><span class="red">*</span></label>
												<input type="text" name="end_date" class="form-control required"  value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date">
											</div>
										</div>
										<!-- Start / end date fields end here -->
										
										<!-- Reward points input start here -->
										<div class="form-group ">
											<label for="title"><?php echo lang('reward_points');?><span class="red">*</span></label>
											<?php
											$data = array('name'=>'reward_points','id'=>'reward_points', 'value'=> isset($reward_points) ? $reward_points : '' , 'class'=>'form-control form-control-line required number','min'=>1);
											echo form_input($data);?>
										</div>
										<!-- Reward points input end here -->
										
										<!-- Description input start here -->
										<div class="form-group">
											<label class=""><?php echo lang('description');?></label>
											<?php
											$data	= array('name'=>'description', 'value'=>isset($description) ? $description : '','class'=> 'form-control form-control-line','rows'=>3);
											echo form_textarea($data);
											?>
										</div>
										<!-- Description input end here -->
									</div>
									<div class="col-md-6">
										<!-- Images right side block start here -->
										<div class="imge-wrap img-wrapper">
										<label for="title" class="img-title"><?php echo lang('banner_image');?></label> 	
										<div class="img-box">
										<?php
										// set banner image action
										$banner_image_val = (isset($banner_image) && !empty($banner_image)) ? $banner_image :'';
										$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
										if(!empty($banner_image_val)) {
										$banner_image_src = base_url('uploads/reward_images/'.$banner_image);
										$title = lang('tooltip_offer_image');

										$banner_image_val_doc = $this->config->item('document_path').'uploads/reward_images/'.$banner_image; 
										if(!file_exists($banner_image_val_doc)){
										// $promo_image_1 = base_url('uploads/No_Image_Available.png');
										$banner_image_src = base_url('uploads/No_Image_Available.png');
										$title = "No Image Available";
										}
										} else {
										$title = lang('tooltip_image');
										$banner_image_src = base_url('uploads/upload.png');
										} ?>
										<div class="cell-img" id="banner_image_cell">
										<img id="snw_banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
										</div>
										<?php
										$data	= array('name'=>'banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_reward_url_banner_image(this)');
										echo form_upload($data);
										// set banner image value in hidden type
										$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
										echo form_input($data);
										?>
										</div>
										<div style="clear:both;"></div>
										<div class="alert red-alert alert-danger alert-dismissible ">
										<i class="icon fa fa-warning"></i>
										Upload up to 3MB images. 
										</div>
										</div>
										<!-- Images right side block end here -->
									</div>
								</div>	
							</div>	
						</div>	
						<!-- End Content panel -->
						<!-- Start Attributes panel -->
						<div class="tab-pane p-20" id="attribute_tab" role="tabpanel">
							<div class="float-left"><?php echo lang('condition_header');?></div>	
							<!-- Add product button -->
							<button onclick="add_row()" class="btn waves-effect waves-light btn-info add-product-btn float-right"  type="button"><?php echo lang('add_condition');?></button>
							<div class="table-responsive">
								<table class="table table-bordered th-border" id="product_tables">
									<thead class="bg_ccc">
										<tr>
											<th class='snw_product_table_th'><?php echo lang('condition_entity');?></th>
											<th class='snw_product_table_th'><?php echo lang('condition_name');?></th>
											<th class='snw_product_table_th'><?php echo lang('condition_value');?></th>
											<th><?php echo lang('product_actions');?></th>
										</tr>
									</thead>
									<tbody>
										<?php
										$qty_row = 1;
										if(isset($rule_conditions) && !empty($rule_conditions) && count($rule_conditions) > 0) {
											foreach($rule_conditions as $condition) { ?>
												<tr class="snw-product-half-side" id="qty_row_<?php echo $qty_row;?>">
													<td>
														<?php
														$condition_ids = array('type'=>'hidden','name'=>'condition_ids[]','value'=>$condition['id']);
														$product_id = array('name'=>'product_id[]', 'value'=>$condition['product_id'],'class'=>'product_input required condition_input form-control','id'=>'product_id_'.$qty_row,'row_id'=>$qty_row,'exist_id'=>$condition['product_id'],'lable_name'=>'product_id');
														echo form_input($condition_ids);
														echo form_input($product_id);
														?>
													</td>
													<td>
														<?php
														$product_name = array('name'=>'product_name[]', 'value'=>isset($condition['product_name']) ? $condition['product_name'] : '','readonly'=>true,'id'=>'product_name_'.$qty_row,'class'=>'form-control','row_id'=>$qty_row);
														echo form_input($product_name);?>
													</td>
													<td>
														<?php
														$product_qty = array('name'=>'product_qty[]', 'value'=>$condition['product_qty'],'class'=>'required condition_input form-control','id'=>'product_qty_'.$qty_row,'row_id'=>$qty_row,'lable_name'=>'product_qty');
														echo form_input($product_qty);
														?>
													</td>
													  <td>
														<span class="romove_row scratch-prod-icon"  row_id=<?php echo $qty_row;?> condition_id='<?php echo $condition['id'];?>' title="<?php echo lang('product_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
													</td>
												</tr>	
											<?php 	
												$qty_row++;		
											}
										} else {
											// render blank condition entities 
											echo $condition_form;
											$qty_row++;
										} ?>
									</tbody>
								</table>
								<input type="hidden" value="<?php echo $qty_row;?>" name='last_qty_row' id='last_qty_row'>
							</div>	
						</div>	
						<!-- End Attributes panel -->
					</div>
					<div class="row pb-3 float-right">
						 <div class="mx-1">
							<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
						</div>
						 <div class="mx-1">
							<a href='<?php echo base_url().'admin/settings/reward' ?>'>
								<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
							</a>
						</div>
						<?php
						// set current date in hidden field 
						$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
						echo form_input($data);?>
					</div>
				<?php echo form_close();?>
			</div>	
		</div>	
	</div>	
</div>

<script type="text/javascript">
	// Initialize material Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});

	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});
	
	$(document).ready(function() {
        
        //add rules for decimal
        $.validator.addMethod("decimalpoints", function(value, element) {
            return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
        }, "You must include two decimal places.");
        
		$("#add_rule_form").validate({
            rules: {
                reward_points: {
                    required: true,
                    decimalpoints: true
                }
            }
		});
	});
	
	/* 
	| -----------------------------------------------------
	| Handle form posting validation
	| -----------------------------------------------------
	*/
	$( "#add_rule_form" ).submit(function( event ) {
		
		// set start date for calculate day diffrence
		var start_date	=  $( "#start_date" ).val().split(' ');
		var startdateSplit = start_date[0].split('-');
		var new_start_date = startdateSplit[0] + '-' + startdateSplit[1] + '-' + startdateSplit[2];
		// set end date for calculate day diffrence
		var end_date	 =  $( "#end_date" ).val().split(' ');
		var enddateSplit = end_date[0].split('-');
		var new_end_date = enddateSplit[0] + '-' + enddateSplit[1] + '-' + enddateSplit[2];
		// show error if start date less then end date
		if(start_date  != ''&& end_date  != '' && (new_start_date >= new_end_date) ) {
			bootbox.alert("End date must be greater than start date.");
			return false;
		}

		var banner_image_val = $('#banner_image_val').val();
        if(banner_image_val == '') {
            $('#banner_image_cell').addClass('error');
            bootbox.alert("Please upload banner image.");
            return false;
        }

		var is_validate = true;
        var product_qty_array = []
        var product_id_array = []
        var checkProductId=1; 
        $( ".condition_input" ).each(function( e ) { 
            var row_id = $(this).attr('row_id');
            var lable_name = $(this).attr('lable_name');
            var product_id = $('#product_id_'+row_id).val();
            var product_qty = $('#product_qty_'+row_id).val();
			
            if(product_id == '') {
                $('#product_id_'+row_id).addClass('error');
                is_validate = false;
            }
            if(product_qty == '') {
                $('#product_qty_'+row_id).addClass('error');
                is_validate = false;
            }
        });
         
     	if(is_validate == false) {
            bootbox.alert("Please fill all required condition attributes.");
            return false;
        } else if(is_validate == 'check') {
            bootbox.alert('There are two same conditions for this rule, Please remove one of them.');
            return false;
        } 
        return;
		event.preventDefault();
	});
	
	
	/* 
	| -----------------------------------------------------
	| Function used to remove image
	| -----------------------------------------------------
	*/
	$(document).delegate('.product_input','blur',function(e){
		var product_id = $(this).val();
		var row_id = $(this).attr('row_id');
		var exist_id = $(this).attr('exist_id');
		get_product_data(product_id,row_id,exist_id);
	});
	
	function get_product_data(product_id,row_id,exist_id) {
		
        var product_ids = new Array();
        var currentRowId = '';
        var j =0;
        $.each( $( ".product_input" ), function() {
            currentRowId = $(this).attr('row_id');
            if(row_id != currentRowId){
                product_ids[j] = $(this).val();
            j++;
            }
        });
        
		// check existing product id
		if(exist_id == product_id) {
			return false;
		} else if(exist_id != '' && exist_id == product_id) {
			bootbox.alert("Product already exist.");
			$('#product_id_'+row_id).val(exist_id);
			return false;
		} 
		
		if ($.inArray(product_id, product_ids) > -1) {
			bootbox.alert("Product already exist.");
			$('#qty_row_'+row_id).html('');
			return false;
		} else {
			$.ajax ({
				type: "POST",
				dataType:'jSon',
				data : {product_id:product_id},
				url: "<?php echo base_url() ?>admin/settings/scratchnwin/getproductdata",
				success: function(data){
					if(data.product_name != '') {
						$('#product_name_'+row_id).val(data.product_name);
					} else {
						$('#product_name_'+row_id).val('');
						$('#product_id_'+row_id).val('');
					}
				}
			});
		}
	}
	
	/* 
    | -----------------------------------------------------
    | Remove condition row
    | -----------------------------------------------------
    */ 
    $(document).delegate(".romove_row","click",function(e){
        var row_id = $(this).attr('row_id');
        var rule_condition_id = $(this).attr('condition_id');
        var rule_id = '<?php echo $rule_id;?>';
        var rowCount = $('#product_tables >tbody >tr').length;
        if(rowCount == 1) {
            bootbox.alert("You have to keep at least one condition in the rule!");
            return false;
        }
        
        if(rule_condition_id != undefined && rule_condition_id != '') {
            bootbox.confirm("Are you sure want to delete this?", function(result) {
                if(result==true) {
                    $.ajax ({
                        type: "POST",
                        dataType:'jSon',
                        data : {rule_condition_id:rule_condition_id,rule_id:rule_id},
                        url: "<?php echo base_url() ?>admin/settings/reward/remove_rule_condition",
                        success: function(data){
                            if(data.status) {
                                // hide the selected row
                                $('#qty_row_'+row_id).remove();
                            }
                        }
                    });
                }
            });
        } else {
            // hide the selected row
            $('#qty_row_'+row_id).remove();
        }
    });
	
	/* 
	| -----------------------------------------------------
	| Init start and end date
	| -----------------------------------------------------
	*/
	$(document).ready(function() {
		if(jQuery('#start_date_div1').length > 0)  { 
			jQuery('#start_date_div1').datetimepicker({
				pickTime: false,
				startDate: new Date(), 
			});
		}

		if(jQuery('#end_date_div1').length > 0)  { 
			jQuery('#end_date_div1').datetimepicker({
				pickTime: false,
				startDate: new Date(), 
			});
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Manage add new condition row 
	| -----------------------------------------------------
	*/ 
	function add_row() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
		last_row = parseInt(last_row)+1;
		$('#last_qty_row').val(last_row);
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {row_id:last_row},
			url: "<?php echo base_url() ?>admin/settings/reward/condition_form",
			success: function(data){
				if(data.status) {
					$('#product_tables tr:last').after(data.condition_form)
				}
				
			}
		});
	}


	 $("#snw_banner_image_picker").click(function() {
        $("input[name='banner_image']").click();
    });

	function read_reward_url_banner_image(input) {
    
        var pickerId= '#snw_'+input.id+'_picker';
        var fieldId	= '#'+input.id;
        $(fieldId+'_val').val(input.value);
        
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var size = file.size/1000000; // get image size in mb;
            if(size <= 3.0955135) {
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //alert(pickerId);
                        $(pickerId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $(fieldId).val("");
                    bootbox.alert("File not supported!");
                }
            } else {
                $(fieldId).val("");
                bootbox.alert("File size should be less then 3MB!");
            }
        }
    }
	
	
</script>


