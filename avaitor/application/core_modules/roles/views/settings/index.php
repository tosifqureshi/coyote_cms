<div class="row">
	<div class="col-12">                 
		<div class="card">
			<div class="card-body">
				<p class="intro"><?php e(lang('role_intro')) ?></p>
				<?php if (isset($role_counts) && is_array($role_counts) && count($role_counts)) : ?>
					<h4 class="card-title float-left"><?php echo lang('role_manage') ?></h4>
					<div class="float-right">
						<a href="<?php echo site_url(SITE_AREA .'/settings/roles') ?>" class="btn waves-effect waves-light btn-info  mx-1">Role</a>
						<a href="<?php echo site_url(SITE_AREA .'/settings/roles/permission_matrix') ?>" class="btn waves-effect waves-light btn-info  mx-1">Permissions Matrix </a>
					</div>
					<div class="table-responsive m-t-40">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th><?php echo lang('role_account_type'); ?></th>
									<th># <?php echo lang('bf_users'); ?></th>
									<th><?php echo lang('role_description') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php 
								foreach ($roles as $role) : ?>
									<tr>
										<td><?php echo anchor(SITE_AREA .'/settings/roles/edit/'. $role->role_id, $role->role_name) ?></td>
										<td><?php
											$count = 0;
											foreach ($role_counts as $r)
											{
												if ($role->role_name == $r->role_name)
												{
													$count = $r->count;
												}
											}
											echo $count;?>
										</td>
										<td><?php echo $role->description ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>	
					</div>	
				<?php endif; ?>	
			</div>	
		</div>	
	</div>	
</div>	
