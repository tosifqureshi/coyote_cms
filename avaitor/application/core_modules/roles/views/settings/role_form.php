<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $toolbar_title ?> <?php e(isset($role) ? ': '. $role->role_name : ''); ?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Role name input start here -->
								<div class="form-group ">
									<label><?php echo lang('role_name');?></label>
									<?php
									$data = array('name'=>'role_name','id'=>'role_name', 'value'=> isset($role) ? $role->role_name : '' , 'class'=>'form-control form-control-line');
									echo form_input($data);
									?>
								</div>
								<!-- Role name input end here -->
								
								<!-- Description input start here -->
								<div class="form-group">
									<label><?php echo lang('role_login_destination');?></label>
									<?php
									$data	= array('name'=>'description', 'value'=>isset($role) ? $role->description : '','class'=> 'form-control form-control-line','rows'=>3);
									echo form_textarea($data);
									?>
									<span class="help-inline"><?php echo form_error('description') ? form_error('description') : lang('role_max_desc_length'); ?></span>
								</div>
								<!-- Description input end here -->
								
								<!-- Role login destination input start here -->
								<div class="form-group ">
									<label><?php echo lang('role_login_destination');?></label>
									<?php
									$data = array('name'=>'login_destination','id'=>'login_destination', 'value'=> isset($role) ? $role->login_destination : '' , 'class'=>'form-control form-control-line');
									echo form_input($data);
									?>
									<span class="help-inline"><?php echo form_error('login_destination') ? form_error('login_destination') : lang('role_destination_note'); ?></span>
								</div>
								<!-- Role login destination input end here -->
								
								<!-- Role default context input start here -->
								<div class="form-group row">
									<label class="control-label col-md-12">
										<h4> <?php echo lang('role_default_context');?></h4>
									</label>
									<div class="col-md-6">
										 <select name="default_context" id="default_context" class="form-control">
											<?php if (isset($contexts) && is_array($contexts) && count($contexts)):?>
											<?php foreach($contexts as $context):?>
												<option value="<?php echo $context;?>" <?php echo set_select('default_context', $context, (isset($role) && $role->default_context == $context) ? TRUE : FALSE) ?>><?php echo ucfirst($context) ?></option>
												<?php endforeach;?>
											<?php endif;?>
										</select>
									</div>
									<div class="col-md-12">
										<?php echo form_error('default_context') ? form_error('default_context') : lang('role_default_context_note'); ?>
									</div>	
								</div>
								<!-- Role default context input end here -->
								
								<!-- Role default role checkbox start here -->
								<div class="form-group">
									<label><?php echo lang('role_default_role');?></label>
									<div class="demo-checkbox">
										<input type="checkbox" name="default" id="default" value="1" class="filled-in chk-col-light-blue" <?php echo set_checkbox('default', 1, isset($role) && $role->default == 1 ? TRUE : FALSE) ?> />
										<label for="default">
											<?php echo lang('role_default_note'); ?>
										</label>
									</div>
								</div>
								<!-- Role default role checkbox end here -->
								
								<!-- Role delete radio options start here -->
								<div class=" form-group">
									<label><?php echo lang('role_can_delete_role');?></label>
									<div class="demo-radio-button">
										<input type="radio" name="can_delete" id="can_delete_yes" class="with-gap radio-col-light-blue" value="1" <?php echo set_radio('can_delete', 1, isset($role) && $role->can_delete == 1 ? TRUE : FALSE) ?> />
										<label for="can_delete_yes">Yes</label> 
										<input type="radio" name="can_delete" id="can_delete_no" class="with-gap radio-col-light-blue" value="0" <?php echo set_radio('can_delete', 0, isset($role) && $role->can_delete == 0 ? TRUE : FALSE) ?> />
										<label for="can_delete_no">No</label>
									 </div>
									 <span class="help-inline" style="display: inline"><?php echo lang('role_can_delete_note'); ?></span>
								</div>
								<!-- Role delete radio options start here -->
								
							</div>	
						</div>
						<!-- Permissions -->
						<?php if (has_permission('Avaitor.Permissions.Manage')) : ?>
							<div class="row">
								<legend><?php echo lang('role_permissions'); ?></legend>
								<br/>
								<p class="intro"><?php echo lang('role_permissions_check_note'); ?></p>
								<?php echo modules::run('roles/settings/matrix'); ?>
							</div>
						<?php endif; ?>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('role_save_role');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/roles' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('bf_action_cancel');?></button>
								</a>
							</div>
							<?php if(isset($role) && $role->can_delete == 1 && has_permission('Avaitor.Roles.Delete')):?>
								<div class="mx-1">
									<button type="submit" name="delete" class="btn btn-danger btn waves-effect waves-light btn-block btn-info" onclick="return confirm('<?php echo lang('role_delete_confirm').' '.lang('role_delete_note') ?>')"><i class="icon-trash icon-white">&nbsp;</i>&nbsp;<?php echo lang('role_delete_role'); ?></button>
								</div>
							<?php endif;?>
						</div>
					<?php echo form_close();?>
				</div>	
			</div>	
		</div>	
	</div>	
</div>	

