<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Scratchnwin Controller
 *
 * Manages the Scratch n Win functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Scratchnwin
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
		$this->lang->load('scratchnwin');
		$this->load->library('form_validation');
		$this->load->model('scratchnwin/Scratchnwin_model', 'scratchnwin_model');
		// set banner images path
		$this->scratchnwin_path = FCPATH.'uploads/scratchnwin_images/'; 
	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Function to display result listing
	 * @input : null
	 * @return  void
	 * @access public
	 */
	public function index() {
	
		// get listing
		$results = $this->scratchnwin_model->get_scratchnwin_results();
		Template::set('results',$results);
		Template::set_view('listing');
		Template::render();

	}//end index()

	//--------------------------------------------------------------------

	/**
	 * Function to manage form
	 * @input : id(optional)
	 * @return: void
	 * @access: public
	 */
	public function create($id=0) {
		
		$id = decode($id); // decode competition id
		$data = array();
		
		if((isset($_POST['save'])) && $id=='') {
			if($this->save_scratchnwin()) {       
					Template::set_message(lang('insert_msg'),'success');
					redirect('admin/settings/scratchnwin');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_scratchnwin($id)) {
					Template::set_message(lang('update_msg'),'success');
					redirect('admin/settings/scratchnwin');
			}
		}
		$data['scratchnwin_title']	= lang('add_scratchnwin');
		if($id) {
			$result = $this->scratchnwin_model->get_scratchnwin_results($id);	
			if(empty($result)) {
				Template::set_message(lang('invalid_msg'),'error');
				redirect('admin/settings/scratchnwin');
			}
			$data['scratchnwin_title']	= lang('edit_scratchnwin');
			$data['snw_id']	 		= encode($id);
			$data['title']			= $result->title;		
			$data['start_date']		= date('d-m-Y',strtotime($result->start_date));
			$data['end_date']		= date('d-m-Y',strtotime($result->end_date));
			$data['notification_time'] = $result->notification_time;
			$data['second_chance_count'] = $result->second_chance_count;
			$data['second_chance_draw'] = $result->second_chance_draw;
			$data['prize_frequency']  = $result->prize_frequency;
			$data['beacon_promotion'] = $result->beacon_promotion;
			$data['status']			= $result->status;
			$data['created_at']		= $result->created_at;
			$data['banner_image']	= $result->banner_image;
			$data['snw_products']	= $this->scratchnwin_model->get_snw_products($id);
			// get beacon offer details
			$beacon_offer_result = $this->scratchnwin_model->get_scratchnwin_beacon_offer($id);
			$data['beacon_offer_id'] = (isset($beacon_offer_result->beacon_offer_id)) ? encode($beacon_offer_result->beacon_offer_id) : 0;
			$data['offer_name']		 = (isset($beacon_offer_result->offer_name)) ? $beacon_offer_result->offer_name : '';
			$data['offer_short_description'] = (isset($beacon_offer_result->offer_short_description)) ? $beacon_offer_result->offer_short_description : '';
			$data['offer_long_description'] = (isset($beacon_offer_result->offer_long_description)) ? $beacon_offer_result->offer_long_description : '';
			$data['offer_image'] = (isset($beacon_offer_result->offer_image)) ? $beacon_offer_result->offer_image : '';
			$data['store_id'] = (isset($beacon_offer_result->store_id)) ? $beacon_offer_result->store_id : '';
		}
		// get beacon associated store list
		$data['stores'] = $this->scratchnwin_model->get_store_list('beacon');	
		// set product emtity form
		$data['product_form'] = $this->load->view('product_form',array('row_id'=>1),true);
		Template::set('data',$data);
		Template::set_view('scratchnwin_form');
		Template::render();	
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to store save scratch n win in database
	 * @input : snw_id
	 * @return: void
	 * @access: private
	 */
	private function save_scratchnwin($snw_id=0) {
		$post = $this->input->post();
		$data = array();
		if(!empty($snw_id)) {
			$data['snw_id']  = $snw_id;
			// get scratchnwin details
			$scratchnwin = $this->scratchnwin_model->get_scratchnwin_results($snw_id);
		}
		$product_ids     = $this->input->post('product_ids');
		$beacon_promotion = $this->input->post('beacon_promotion');
		// apply validation rules for input params
		$this->form_validation->set_rules('title', 'lang:title', 'trim|required|max_length[80]');			
		$this->form_validation->set_rules('start_date', 'lang:start_date', 'trim|required');	
		$this->form_validation->set_rules('end_date', 'lang:end_date', 'trim|required');
		if(!empty($beacon_promotion)) {
			$this->form_validation->set_rules('offer_name', 'lang:offer_name', 'trim|required|max_length[80]');		
			$this->form_validation->set_rules('store_id[]', 'lang:offer_store', 'trim|required');
		}
		
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			return FALSE;
		} else if($product_ids == '') {
			Template::set_message("Please add products ",'error');
			return FALSE;
		}
		
		// Set input field values
		if (isset($_FILES['banner_image']) && ($_FILES['banner_image']['name']) != '' ) {
			// remove old image
			(isset($scratchnwin->banner_image) && !empty($scratchnwin->banner_image)) ? unlink($this->scratchnwin_path.$scratchnwin->banner_image) : '';
			// upload image
			$data['banner_image'] = $this->to_upload_banner_image('banner_image');
		}
		
		// set post values in store bundle
		$data['title']   			= $this->input->post('title');
		$data['notification_time']  = $this->input->post('notification_time');
		$data['second_chance_count']= $this->input->post('second_chance_count');
		$data['prize_frequency']    = (!empty($this->input->post('prize_frequency'))) ? $this->input->post('prize_frequency') : 7;
		$data['beacon_promotion']   = $this->input->post('beacon_promotion');
		$data['created_by'] 		= $this -> session -> userdata('user_id');
		$data['start_date']			= date("Y-m-d",strtotime($this->input->post('start_date')));
		$data['end_date']  			= date("Y-m-d",strtotime($this->input->post('end_date')));
		$data['second_chance_draw'] = (!empty($this->input->post('second_chance_draw'))) ? $this->input->post('second_chance_draw') : 0;
		$ms_snw_id = (isset($scratchnwin->scrwin_id) && !empty($scratchnwin->scrwin_id)) ? $scratchnwin->scrwin_id : 0;
		
		if(empty($snw_id)) {
			$current_date =  date("Y-m-d H:i:s");
			// set default result date as start date
			$data['created_at'] = $current_date;
			// insert scratch&win entry in mssql table
			$ms_snw_id = $this->manage_ms_scratch_n_win_entry($data); 
			$data['scrwin_id'] = (!empty($ms_snw_id)) ? $ms_snw_id : 0;
		}
		// add or update loyalty data into db
		$snw_id = $this->scratchnwin_model->save_data($data);
		if(!empty($beacon_promotion)) {
			// manage beacon offer data
			$this->manage_beacon_offer($snw_id);
		}
		// manage products data
		$this->manage_products($snw_id,$ms_snw_id);
		return $snw_id;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Add competition's product data
	 * @input : competition_id
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	private function manage_products($snw_id=0,$ms_snw_id=0) {
		// manage products log
		$post = $this->input->post();
		$product_ids     = $this->input->post('product_ids');
		$product_name    = $this->input->post('product_name');
		$promo_code      = $this->input->post('promo_code');
		$snw_product_ids = $this->input->post('snw_product_ids');
		$prizes_per_day	 = $this->input->post('prizes_per_day');
		$total_prizes	 = $this->input->post('total_prizes');
		$prod_image_val	 = $this->input->post('existing_product_image');
		
		// setup product array values
		$product_ids     = (!empty($product_ids)) ? array_filter($product_ids) : '';
		$product_name    = (!empty($product_name)) ? array_filter($product_name) : '';
		$promo_code      = (!empty($promo_code)) ? array_filter($promo_code) : '';
		$snw_product_ids = (!empty($snw_product_ids)) ? array_filter($snw_product_ids) : '';
		$prizes_per_day	 = (!empty($prizes_per_day)) ? array_filter($prizes_per_day) : '';
		$total_prizes	 = (!empty($total_prizes)) ? array_filter($total_prizes) : '';
		$prod_image_val	 = (!empty($prod_image_val)) ? array_filter($prod_image_val) : '';
		$product_images  = $_FILES['product_image'];
	
		if(!empty($product_ids)) {
			foreach($product_ids as $key=>$id) {
				$product_data = array();
				
				if(isset($product_images['name'][$key]) && !empty($product_images['name'][$key])) {
					
					// set image post values
					$_FILES['product_image_'.$key]['name'] = $product_images['name'][$key];
					$_FILES['product_image_'.$key]['type'] = $product_images['type'][$key];
					$_FILES['product_image_'.$key]['tmp_name'] = $product_images['tmp_name'][$key];
					$_FILES['product_image_'.$key]['error'] = $product_images['error'][$key];
					$_FILES['product_image_'.$key]['size'] = $product_images['size'][$key];
					// upload product image
					$product_image = $this->to_upload_banner_image('product_image_'.$key);
					$product_data['product_image'] = (isset($product_image)) ? $product_image : '';
					// remove existing product image
					(isset($prod_image_val[$key]) && !empty($prod_image_val[$key])) ? unlink($this->scratchnwin_path.$prod_image_val[$key]) : '';
				}
				// set availability for of product
				$days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
				$days_availability = '';
				$i = 0;
				foreach($days as $day) {
					if(isset($post[$day][$key]) && $post[$day][$key] == 1) {
						$days_availability .= ($i == 0) ? ','.$day.',' : $day.',';
						$i++;
					}
				}
				
				$product_data['product_id'] = $id;
				$product_data['snw_id'] = $snw_id;
				$product_data['product_name'] = (isset($product_name[$key])) ? $product_name[$key] : '';
				$product_data['promo_code'] = (isset($promo_code[$key])) ? $promo_code[$key] : '';
				$product_data['prizes_per_day'] = (isset($prizes_per_day[$key])) ? intval($prizes_per_day[$key]) : 0;
				$product_data['total_prizes'] = (isset($total_prizes[$key])) ? intval($total_prizes[$key]) : 0;
				$product_data['days_availability'] = $days_availability;
				if(isset($snw_product_ids[$key]) && !empty($snw_product_ids[$key])) {
					$product_data['id'] = $snw_product_ids[$key];
				}
				
				// add product data in mssql table
				if(!empty($ms_snw_id) && !isset($product_data['id'])) {
					//$ms_snw_prod_id = $this->manage_ms_scratch_n_win_product_entry($product_data,$ms_snw_id);
					$product_data['ms_snw_prod_id'] = (!empty($ms_snw_prod_id)) ? $ms_snw_prod_id : 0;
				}
				
				// add product data
				$this->scratchnwin_model->add_product($product_data);
			}
		}
		return true;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function used to manage beacon offer store procedure
	 * @input : int snw_id , int beacon_offer_id
	 * @return: void
	 * @access: private
	 * @author: Tosif Qureshi
	 */
	 private function manage_beacon_offer($snw_id) {
		if (isset($_FILES['offer_image']) && ($_FILES['offer_image']['name']) != '' ) {
			$existing_offer_image = $this->input->post('existing_offer_image');
			// remove old image
			(!empty($existing_offer_image)) ? unlink($this->scratchnwin_path.$existing_offer_image) : '';
			// upload image
			$beacon_data['offer_image'] = $this->to_upload_banner_image('offer_image');
		}
		$beacon_data['offer_name']   			= $this->input->post('offer_name');
		$beacon_data['offer_short_description'] = $this->input->post('offer_short_description');
		$beacon_data['offer_long_description']  = $this->input->post('offer_long_description');
		$beacon_offer_id  = $this->input->post('beacon_offer_id');
		$beacon_data['snw_id']  = $snw_id;
		if(!empty($beacon_offer_id)) {
			$beacon_data['beacon_offer_id']  = decode($beacon_offer_id);
		}
		// manage store ids
		$store_data = '';
		$store_ids = $this->input->post('store_id');
		if(is_array($store_ids) && !empty($store_ids)) {
			$store_data = implode(',',$store_ids);
		}
		$beacon_data['store_id']  = ",".$store_data.",";
		$beacon_data['modified_at'] = date('Y-m-d H:i:s');
		// manage beacon offer store procedure
		$beacon_offer_id = $this->scratchnwin_model->save_beacon_data($beacon_data);
		return true;
	 }
	
	//--------------------------------------------------------------------

	/**
	 * Upload scratch&win image
	 * @input : image
	 * @return: void
	 * @access: private
	 */
	private function to_upload_image($image='',$upload_at='product_images/') {
		
		$dirPath = IMAGEUPLOADPATH.$upload_at;
		
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){					
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('admin/settings/scratchnwin');					
		}
	
		$imageData = $this -> upload -> data();
		
		if (is_array($imageData)) {
		
			return $imageData['file_name'];
		}
	}
	
	//--------------------------------------------------------------------

	/**
	 * Upload scratch&win banner image
	 * @input : image
	 * @return: void
	 * @access: private
	 */
	
	private function to_upload_banner_image($image='',$upload_at='scratchnwin_images/') {
		
		$dirPath = IMAGEUPLOADPATH.$upload_at;
		
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '3000';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){					
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('admin/settings/scratchnwin');					
		}
		
		$imageData = $this -> upload -> data();
		
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Update status of scratch&win
	 * @input : id , status
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function change_status($id='',$status='') {
		
		$data['snw_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$id = $this->scratchnwin_model->save_data($data);
		Template::set_message(lang('update_status'),'success');
		redirect('admin/settings/scratchnwin');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove scatch&win
	 * @input : snw_id
	 * @return: void
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function delete($snw_id='') {
		$snw_id = decode($snw_id);
		// get data for remove image data
		$result = $this->scratchnwin_model->get_scratchnwin_results($snw_id);	
		if(!empty($result)) {
			// remove banner image
			(isset($result->banner_image) && !empty($result->banner_image)) ? unlink($this->scratchnwin_path.$result->banner_image) : '';
			// remove products images
			$snw_products = $this->scratchnwin_model->get_snw_products($snw_id);
			if(!empty($snw_products)) {
				foreach($snw_products as $snw_product) {
					// remove existing product image
					(isset($snw_product['product_image']) && !empty($snw_product['product_image'])) ? unlink($this->scratchnwin_path.$snw_product['product_image']) : '';
				}
			}
		}
		// delete scratch&win entry
		$snw_id = $this->scratchnwin_model->remove_snw($snw_id);
		Template::set_message(lang('delete_msg'),'success');
		redirect('admin/settings/scratchnwin');
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to render product form 
	 * @input : row_id
	 * @return  void
	 * @access public
	 */
	public function product_form()
	{
		// get data from post
		$row_id = $this->input->post('row_id');
		$data['row_id'] = $row_id;
		$tpl_data = $this->load->view('product_form',$data,true);
		echo json_encode(array('status'=>true,'product_form'=>$tpl_data));die;

	}//end product_form()
	
	 //--------------------------------------------------------------------
	
	/**
	 * Function to get product data
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function getproductdata() {
		// get data from post
		$product_id = $this->input->post('product_id');
		$product_name = '';
		if(!empty($product_id)) {
			// get product details from product id
			$product_data = $this->scratchnwin_model->get_product_data($product_id);
			// set product name
			$product_name = (isset($product_data->Prod_Desc) && !empty(isset($product_data->Prod_Desc))) ? $product_data->Prod_Desc : '';
		} 
		// set return json response
		echo json_encode(array('product_name'=>$product_name));die;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to reset product's prize sent value as 0
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function reset_prize_sent_log() {
		$return_res = false;
		// get data from post
		$snw_product_id = $this->input->post('snw_product_id');
		if(!empty($snw_product_id)) {
			// update product prize sent value as 0
			$this->scratchnwin_model->update_product_val($snw_product_id,array('prizes_sent_this_week'=>0));
			$return_res = true;
		}
		// set return json response
		echo json_encode(array('status'=>$return_res));die;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to update products status
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function change_product_status() {
		$return_res = 0;
		// get data from post
		$snw_product_id = $this->input->post('snw_product_id');
		$row_id = $this->input->post('row_id');
		$snw_id = $this->input->post('snw_id');
		if(!empty($snw_product_id)) {
			// get product details
			$result = $this->scratchnwin_model->get_snw_products(0,$snw_product_id);	
			if(!empty($result)) {
				// set status value
				$status = (isset($result->status) && !empty($result->status)) ? 0 : 1;
				$is_update = true;
				if($status == 0) {
					// fetch campaign's active product count
					$product_count = $this->scratchnwin_model->active_products_count(decode($snw_id));
					$return_res = 2; // return response in case of single active record
					if($product_count <= 1) {
						$is_update = false;
					}
				}
				
				if($is_update){
					// update product status
					$this->scratchnwin_model->update_product_val($snw_product_id,array('status'=>$status));
					$return_res = 1;
				}
			}
		}
		// set class
		$btn_class = (isset($status) && $status == '0') ? 'deactive-btn' : 'active-btn';
		$icon_class = (isset($status) && $status == '0') ? 'fa-toggle-off' : 'fa-toggle-on';
		// set return json response
		echo json_encode(array('status'=>$return_res,'btn_class'=>$btn_class,'icon_class'=>$icon_class));die;
	}
	
	//--------------------------------------------------------------------

	/**
	 * Remove scracth&win product entry
	 * @input : id
	 * @return: void
	 * @access: public
	 */
	public function remove_snw_products() {
		// get data from post
		$id = $this->input->post('snw_product_id');
		$snw_id = $this->input->post('snw_id');
		$status = false;
		if(!empty($id) && !empty($snw_id)) {
			// check product's existance in campaing prize list
			$is_not_exists = $this->scratchnwin_model->product_prize_existance($id,decode($snw_id));
			if($is_not_exists) {
				$id = $this->scratchnwin_model->remove_snw_product($id);
				$status = true;
			}
		}
		// set return json response
		echo json_encode(array('status'=>$status));die;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function to get product promo code
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function get_product_promo() {
		// get data from post
		$promo_code = $this->input->post('promo_code');
		$product_id = '';
		if(!empty($promo_code)) {
			// get product details from product promo code
			$product_data = $this->scratchnwin_model->get_product_promo($promo_code);
			// set product id
			$product_id = (isset($product_data->product_id) && !empty(isset($product_data->product_id))) ? $product_data->product_id : '';
		} 
		// set return json response
		echo json_encode(array('product_id'=>$product_id));die;
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function used to manage scratch n win entries in MSSQL db 
	 * @param array snw_data
	 * @return void
	 * @access public
	 * 
	 */
	private function manage_ms_scratch_n_win_entry($snw_data) {
		$current_date =  date("Y-m-d H:i:s");
		$inserted_id = 0;
		if(is_array($snw_data) && count($snw_data) > 0) {
			// prepare scratch&Win data
			$snw_ms_data = array('ScrWin_Title'=>$snw_data['title'],
				'ScrWin_Banner_Image'=>$snw_data['banner_image'],
				'ScrWin_Start_Date'=>$snw_data['start_date'],
				'ScrWin_End_Date'=>$snw_data['end_date'],
				'ScrWin_Second_Chance_Draw'=>$snw_data['second_chance_draw'],
				'ScrWin_Second_Chance_Avail_Count'=>$snw_data['second_chance_count'],
				'ScrWin_Status'=>1,
				'ScrWin_Created_Date'=>$current_date,
				'ScrWin_Modified_Date'=>$current_date
			);
			// insert data in mssql scratch table
			$inserted_id = $this->scratchnwin_model->ms_scratch_n_win_entry($snw_ms_data);
		}
		return $inserted_id;			
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Function used to manage scratch n win product entries in MSSQL db 
	 * @param array snw_product_data
	 * @return void
	 * @access public
	 * 
	 */
	private function manage_ms_scratch_n_win_product_entry($snw_product_data,$scrwin_id=0) {
		$current_date =  date("Y-m-d H:i:s");
		$inserted_id = 0;
		if(!empty($scrwin_id) && is_array($snw_product_data) && count($snw_product_data) > 0) {
			// prepare scratch&Win data
			$ms_snw_product_data = array('ScrWinProd_Snw_Id'=>$scrwin_id,
				'ScrWinProd_Product_Number'=>$snw_product_data['product_id'],
				'ScrWinProd_Image'=>$snw_product_data['product_image'],
				'ScrWinProd_Prize_Per_Day'=>$snw_product_data['prizes_per_day'],
				'ScrWinProd_Total_Prizes'=>$snw_product_data['total_prizes'],
				'ScrWinProd_Total_Claimed'=>0,
				'ScrWinProd_Promo_Code'=>$snw_product_data['promo_code'],
				'ScrWinProd_Status'=>1,
				'ScrWinProd_Created_Date'=>$current_date,
				'ScrWinProd_Modified_Date'=>$current_date
			);
			// insert data in mssql scratch table
			$inserted_id = $this->scratchnwin_model->ms_scratch_n_win_product_entry($ms_snw_product_data);
		}
		return $inserted_id;			
	}

}//end Settings

// End of Admin Device address Controller
/* Location: ./application/core_modules/scratchnwin/controllers/settings.php */
