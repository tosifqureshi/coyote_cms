<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['scratchnwin']		= 'Scratch&Win';
$lang['add_scratchnwin']	= 'Add New Scratch&Win';
$lang['edit_scratchnwin']	= 'Edit Scratch&Win';
$lang['sno']				= 'S.No.';
$lang['title']				= 'Title';
$lang['image']				= 'Image';
$lang['description']		= 'Long Description';
$lang['short_description']= 'Short Description';
$lang['second_chance_draw']= 'Second Chance Draw';
$lang['notification_time']= 'Notification Time';
$lang['created_at']		= 'Created At';
$lang['status']			= 'Status';
$lang['start_date']		= 'Start date';
$lang['end_date']		= 'End date';
$lang['products']		= 'Products';
$lang['delete']			= 'Delete';
$lang['insert_msg']			= 'Successfully inserted';
$lang['update_msg']			= 'Successfully updated';
$lang['update_status']		= 'successfully change status';
$lang['delete_msg']			= 'Successfully removed';
$lang['please_fill_address']= 'Please fill at least IMEI or MAC number detail';
$lang['form_save']			= 'Save';
$lang['form_cancel']		= 'Cancel';
$lang['tab_wifi_note']		= 'Use Mac address for tablet Wi-Fi compatibility';
$lang['tab_wifi_gsm_note']  = 'Use Mac and IMEI address for tablet Wi-Fi & GSM compatibility';
$lang['default_image']	= 'Default Image';
$lang['tooltip_image']	= 'click to upload new Scratch&Win image';
$lang['beacon_tooltip_image']	= 'click to upload Beacon Offer image';
$lang['content']			= 'Content';
$lang['attributes']			= 'Attributes';
$lang['product_number']		= 'Product no.';
$lang['product_name']		= 'Product name';
$lang['product_promo_code']	= 'Promo code';
$lang['prizes_per_day']		= 'Prizes per day';
$lang['total_prizes']		= 'Total prizes';
$lang['prizes_sent_this_week']	= 'Prizes sent this week';
$lang['total_number_claimed']	= 'Total claimed';
$lang['product_avail_days']		= 'Day(s) availability';
$lang['beacon_promotion']		= 'Campaign Beacon Promotion';
$lang['product_image']			= 'Product image';
$lang['product_actions']		= 'Status / Delete';
$lang['banner_image']			= 'Banner Image';
$lang['product_status_btn_note_0']= 'Click here to active this product in campaign';
$lang['product_status_btn_note_1']= 'Click here to de-active this product from campaign';
$lang['product_delete_btn_note']= 'Click here to remove this product from campaing';
$lang['prize_frequency'] = 'Prize Frequency (One in)';
$lang['offer_short_description'] = 'Offer Short Description';
$lang['beacon_offer_name'] = 'Beacon Promotion Name';
$lang['offer_long_description'] = 'Beacon Promotion Description';
$lang['beacon_offer_image'] = 'Beacon Promotion Image';
$lang['offer_store'] = 'Beacon Promotion Store(s)';
$lang['number_of_second_chance']= 'Number of Second Chance Entries to Send';
