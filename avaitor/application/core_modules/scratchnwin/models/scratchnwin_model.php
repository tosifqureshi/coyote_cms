<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Scratchnwin Model
 *
 * The central way to access and perform CRUD on Scratchnwin.
 *
 * @package    Avaitor
 * @subpackage Modules_Scratchnwin
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Scratchnwin_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        $this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // define tables
		$this->scratch_n_win_table = 'scratch_n_win';
		$this->scratch_n_win_products_table = 'scratch_n_win_products';
		$this->scratch_n_win_prize_winners_table = 'scratch_n_win_prize_winners';
		$this->scratch_n_win_beacon_offers_table = 'scratch_n_win_beacon_offers';
		$this->stores_log_table = 'stores_log';
		$this->beacons_table = 'beacons';
		$this->ms_scratch_win_table = 'SCRATCH_WIN_TBL';
		$this->ms_scratch_win_product_table = 'SCRATCH_WIN_PRODUCT_TBL';
    }

	/**
	 * Get listing of scratch n win log
	 * @input : id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_scratchnwin_results($id = 0) {
		
		$this -> read_db -> select('*');
		if ($id) {
			$this -> read_db -> where("snw_id", $id);
			$result = $this -> read_db -> get($this->scratch_n_win_table);
			return $result -> row();
		}
		$this -> read_db -> where("is_deleted", 0);
		$this -> read_db -> order_by('created_at', 'desc');
		$result = $this -> read_db -> get($this->scratch_n_win_table);
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_data($data) {
		
		if(isset($data['snw_id']) && !empty($data['snw_id'])) { // update data
			$this->read_db->where('snw_id', $data['snw_id']);
			$this->read_db->update($this->scratch_n_win_table, $data);
			return $data['snw_id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->scratch_n_win_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
	/**
	 * Function to remove entry from db
	 * @input : id
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	public function remove_data($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('snw_id', $id);
			$this->read_db->delete($this->scratch_n_win_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Function to get product details from product id
	 * @input : product_id
	 * @output: obj array
	 * @author: Tosif Qureshi
	 */
	public function get_product_data($product_id) {
		// get product's result
		$product_result = $this->mssql_db->query("select Prod_Desc, Prod_Number,APN_NUMBER from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT join APNTBL on Prod_Number = APN_PRODUCT where OUTP_STATUS = 'active' and Prod_Number = $product_id and APN_PRODUCT = $product_id");
		return $product_result -> row();
	}
	
	/**
	 * Function to get products list
	 * @input : int snw_id
	 * @output: array
	 * @access: public
	 */
	public function get_snw_products($snw_id=0,$id=0) {
		if(!empty($snw_id) || !empty($id)) {
			$this -> read_db -> select('*');
			if(!empty($id)) {
				$this -> read_db -> where("id", $id);
				$result = $this -> read_db -> get($this->scratch_n_win_products_table);
				return $result -> row();
			} else {
				$this -> read_db -> order_by('id', 'asc');
				$this -> read_db -> where(array('snw_id'=>$snw_id,'is_deleted'=>0));
				$result = $this -> read_db -> get($this->scratch_n_win_products_table);
				return $result -> result_array();
			}
		}	else {
			return false;
		}
	}
	
	/**
	 * Function to manage product insertion
	 * @input : data
	 * @output: int(id)
	 * @access: public
	 */
	public function add_product($data) {
		
		if(isset($data['id']) && !empty($data['id'])) { // update data
			$this->db->where('id', $data['id']);
			$this->db->update($this->scratch_n_win_products_table, $data);
			return $data['id'];
		} else { // add data
			$this->db->insert($this->scratch_n_win_products_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Function to update product's data
	 * @input : int snw_product_id
	 * @output: void
	 */
	public function update_product_val($snw_product_id,$data) {
		
		if(!empty($snw_product_id)) {
			$this->read_db->where('id', $snw_product_id);
			$this->read_db->update($this->scratch_n_win_products_table,$data);
			return $snw_product_id;
		}
		return false;
	}
	
	/**
	 * Function to remove product entry from db
	 * @input : id
	 * @output: void
	 */
	public function remove_snw_product($id) {
		$return = false;
		if(!empty($id)) {
			$this->read_db->where('id', $id);
			$this->read_db->delete($this->scratch_n_win_products_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Function to remove scatch&win entry from db
	 * @input : snw_id
	 * @output: void
	 * @author: Tosif Qureshi
	 */
	public function remove_snw($snw_id) {
		$return = false;
		if(!empty($snw_id)) {
			// remove 
			$this->read_db->where('snw_id', $snw_id);
			$this->read_db->delete($this->scratch_n_win_table); 
			
			$this->read_db->where('snw_id', $snw_id);
			$this->read_db->delete($this->scratch_n_win_products_table); 
			$return = true;
		}
		return $return;
	}
	
	/**
	 * Function to get products list
	 * @input : string promo_code
	 * @output: array
	 * @access: public
	 */
	public function get_product_promo($promo_code='') {
		if(!empty($promo_code)) {
			$this -> read_db -> select('id,product_id');
			$this -> read_db -> where("promo_code", $promo_code);
			$result = $this -> read_db -> get($this->scratch_n_win_products_table);
			return $result -> row();
		}
		return false;
	}
	
	/**
	 * Function to manage insertion of scratch n win mssql data
	 * @input : data
	 * @output: int
	 *
	 */
	public function ms_scratch_n_win_entry($data) {
		$this->mssql_db->insert($this->ms_scratch_win_table, $data);
		return $this->mssql_db->insert_id();
	}
	
	/**
	 * Function to manage insertion of scratch n win product mssql data
	 * @input : data
	 * @output: int
	 *
	 */
	public function ms_scratch_n_win_product_entry($data) {
		$this->mssql_db->insert($this->ms_scratch_win_product_table, $data);
		return $this->mssql_db->insert_id();
	}
	
	/**
	 * Function used to check product's existance in prize
	 * @input : string product_id
	 * @output: bool
	 * @access: public
	 */
	public function product_prize_existance($product_id='',$snw_id='') {
		$return_response = true;
		if(!empty($product_id) && !empty($snw_id)) {
			$date = date('Y-m-d');
			$this -> read_db -> select('snw_winner_id');
			$this -> read_db -> where("snw_product_id", $product_id);
			$this -> read_db -> where("snw_id", $snw_id);
			$this -> read_db -> where("prize_declare_at", $date);
			$result = $this -> read_db -> get($this->scratch_n_win_prize_winners_table);
			$result_row = $result -> row();
			if(!empty($result_row)) {
				$return_response = false;
			}
		}
		return $return_response;
	}
	
	/**
	 * Function used to fetch campaign's active product count
	 * @input : string snw_id
	 * @output: int
	 * @access: public
	 */
	public function active_products_count($snw_id='') {
		$return_response = 0;
		if(!empty($snw_id)) {
			$this -> read_db -> select('count(id) as product_count');
			$this -> read_db -> where("status", 1);
			$this -> read_db -> where("snw_id", $snw_id);
			$result = $this -> read_db -> get($this->scratch_n_win_products_table);
			$result_row = $result -> row();
			if(!empty($result_row) && isset($result_row->product_count)) {
				$return_response = $result_row->product_count;
			}
		}
		return $return_response;
	}
	
	/**
	 * Get scratch beacon offer details
	 * @input : snw_id
	 * @output: array
	 * @author: Tosif Qureshi
	 */
	function get_scratchnwin_beacon_offer($snw_id = 0) {
		
		$this -> read_db -> select('*');
		if ($snw_id) {
			$this -> read_db -> where("snw_id", $snw_id);
			$result = $this -> read_db -> get($this->scratch_n_win_beacon_offers_table);
			return $result -> row();
		}
		return false;
	}
	
	/**
	 * Get all store log listing
	 * @access: public
	 * 
	 */
	public function get_store_list($check= '') {
		
		$beaconStores = $this->get_active_beacon_store();// get active store associated with beacon which is to show in dropdown
		$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> where('store_name !=','');
		if($check == 'beacon') {
			$this -> read_db -> where_in('store_id', $beaconStores);
		}
		$this -> read_db -> order_by('store_name', 'ASC');
		$result = $this -> read_db -> get($this->stores_log_table);
		$return = array();
		if($result->num_rows() > 0){
			foreach($result -> result_array() as $row){
				$return[$row['store_id']] = $row['store_name'];			
			}
		}
		return $return;
	}
	
	/**
	 * Get all active store associated with beacon
	 * @access: public
	 * 
	 */
	public function get_active_beacon_store() {
		$this -> read_db -> select('store_id');
		$this -> read_db -> where(array('status'=>1,'is_delete'=>0));
		$result = $this -> read_db -> get($this->beacons_table);
		$return = array();
		if($result->num_rows() > 0){
			foreach($result -> result_array() as $row){
				$return[] = $row['store_id'];			
			}
		}
		return $return;
	}
	
	/**
	 * Function to manage insertion and updation beacon offer data
	 * @input : data
	 * @output: int
	 * @author: Tosif Qureshi
	 */
	public function save_beacon_data($data) {
		
		if(!empty($data['snw_id']) && isset($data['beacon_offer_id']) && !empty($data['beacon_offer_id'])) { // update data
			$this->read_db->where('snw_id', $data['snw_id']);
			$this->read_db->where('beacon_offer_id', $data['beacon_offer_id']);
			$this->read_db->update($this->scratch_n_win_beacon_offers_table, $data);
			return $data['beacon_offer_id'];
		} else { // add data
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->read_db->insert($this->scratch_n_win_beacon_offers_table, $data);
			return $this->read_db->insert_id();
		}
	}
	
}//end User_model
