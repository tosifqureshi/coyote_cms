<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="span9 dash-board">
	<div class="well-white">
		<div class="page-header">
			<h1><?php echo lang('scratchnwin') ?></h1>
			<a href="<?php echo site_url("admin/settings/scratchnwin/create"); ?>" role="button" class="btn btn-primary pull-right"><?php echo lang('add_scratchnwin'); ?></a>
		</div>
		<div>
<?php			
if (isset($message) && !empty($message)){
	echo '<div class="alert alert-success">' . $message	 . '</div>';
} ?>
	<!-- start the bootstrap modal where the image will appear -->
	<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<img src="" id="imagepreview" style="max-width: 400px; height: 264px;" >
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end the bootstrap modal where the image will appear -->


	<div role="grid" class="dataTables_wrapper" id="dyntable_wrapper">
		<table id="competition_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">    
			<thead>
				<tr role="row">
					<th><?php echo lang('sno'); ?></th>
					<th class="no-sort"><?php echo lang('image'); ?></th>
					<th><?php echo lang('title'); ?></th>
					<th><?php echo lang('start_date'); ?></th>
					<th><?php echo lang('end_date'); ?></th>
					<th><?php echo lang('status'); ?></th>
					<th class="no-sort"><?php echo lang('delete'); ?></th>
				</tr>
			</thead>
                    
			<tbody role="alert" aria-live="polite" aria-relevant="all">
					<?php
					$i=1;
					if(isset($results) && is_array($results) && count($results)>0):
					foreach($results as $result) :
						// set encoded id
						$snw_id = encode($result['snw_id']); ?>
						<tr>
							<td><?php echo $i;?></td>
							<td>
								<?php if(isset($result['banner_image']) && !empty($result['banner_image'])) {
									$snw_image = $result['banner_image'];
									$snw_image = (!empty($snw_image)) ? base_url('uploads/scratchnwin_images/'.$snw_image) : base_url('uploads/No_Image_Available.png');?>
									<img  id="imageresource" src="<?php echo $snw_image;?>" width="110" height="90" title="<?php echo $result['title'];?>">
								<?php } ?>
							</td>
							<td>
								<div class="offer_info">
									<?php 
									$start_date_time = date('Y-m-d',strtotime($result['start_date']));
									$end_date_time = date('Y-m-d',strtotime($result['end_date'].' +1 days'));
									$diff =  get_date_diffrence($start_date_time, $end_date_time);
									echo '<b>'.lang('title').' : </b>'.$result['title'];
									echo '<br>';
									echo '<b>'.lang('start_date').' : </b>'.date('F j, Y',strtotime($result['start_date'])) ;
									echo '<br>';
									echo '<b>'.lang('end_date').'  : </b>'.date('F j, Y',strtotime($result['end_date'])) ;
									echo '<br>';
									echo $diff;
									?>
								</div>
								<a href="<?php echo site_url("admin/settings/scratchnwin/create/".$snw_id);?>"><?php echo (strlen($result['title']) > 20) ? substr($result['title'],0,20).'...' :  $result['title'];?></a>
							</td>
							<td><?php echo date('F j, Y',strtotime($result['start_date']));?></td>
							<td><?php echo date('F j, Y',strtotime($result['end_date']));?></td>
							<td>
								<?php
								// set change status url
								$status_url = site_url("admin/settings/scratchnwin/change_status/".$snw_id.'/'.encode($result['status']));
								if($result['status'] == '0') { ?>
									<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn deactive-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
									<?php
								} else {
									?>
									<div style="display:none"><?php echo $result['status']; ?></div><a class="common-btn active-btn" onclick="updateStatus('<?php echo $status_url;?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
								<?php
								} ?>
							</td>
							<td>
								<a class="common-btn remove-btn" onclick="removeData('<?php echo site_url("admin/settings/scratchnwin/delete/".$snw_id);?>')"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					<?php 
					$i++;
					endforeach ; endif; ?>
                </tbody>
            </table>                 
		</div>
	</div>
</div>

<!----------- Start competition members listing ------------->
<div id="modal_members_list"></div>


<script>
	jQuery('#competition_table').dataTable({
		"aoColumnDefs": [{ "targets": 'no-sort',"bSortable": false}],
		"bJQueryUI": true,
		"aaSorting": [[ 0, "asc" ]],
		"sPaginationType": "full_numbers"
	});
/* 
| ------------------------------------------------------------
| Show competition members listing on popup 
| ------------------------------------------------------------
*/ 
function competition_members(competition_id) {
	$.ajax ({
		type: "POST",
		data : {competition_id:competition_id},
		url: BASEURL+'admin/settings/competition/competition_members',
		async: false,
		success: function(data) {
			if(data) {
				$('#modal_members_list').html(data);
				$('#competitionmodal').modal('show');
			}
		}, 
		error: function(error){
			bootbox.alert(error);
		}
	});
		
}
</script>

