<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="page-header">
    <h1><?php echo $scratchnwin_title;?></h1>
</div>
<?php 
$snw_id = isset($snw_id) ? $snw_id : '';
echo form_open_multipart('admin/settings/scratchnwin/create/'.$snw_id,'id="add_scratchnwin_form" class="add_form"'); ?>
	<div class="tabbable ">
		<ul class="nav nav-tabs">
			<li class="active">&nbsp;</li>
			<li class="active"><a href="#content_tab" data-toggle="tab"><?php echo lang('content');?></a></li>
			<li><a href="#attributes_tab" data-toggle="tab"><?php echo lang('attributes');?></a></li>
		</ul>
		<div class="tab-content scroll ">
			<!-- Start Content panel -->
			<div class="tab-pane active" id="content_tab">
				<div class="CompetitionS form-wrapper clearboth">
					<!-- Title input start here -->
					<div class="form-box1 form-box">
						<label for="title"><?php echo lang('title');?><i style="color:red;">*</i></label>
						<?php
						$data = array('name'=>'title','id'=>'title', 'value'=> isset($title) ? $title : '' , 'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('beacon_code'));
						echo form_input($data);
						?>
					</div>
					<!-- Title input end here -->
					
					<!-- Start / end date fields start here -->
					<div class="inptbx full-wrap mb10">
                        <div class="form-box half-wrap">
                            <label for="co_start_date"><?php echo lang('start_date');?><i style="color:red;">*</i></label>
                            <div class="half_date">
                                <div class="form-group input-append date" id="start_date_div1">
                                    <?php
                                    $data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'value'=>isset($start_date) ? $start_date : '','id'=>'start_date','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_time'), 'readonly'=>'readonly');
                                    echo form_input($data);
                                    ?>
                                    <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-box1 form-box half-wrap">
                            <label for="co_end_date"><?php echo lang('end_date');?><i style="color:red;">*</i></label>
                            <div class="half_date">
                                <div class="form-group input-append date" id="end_date_div1">
                                    <?php
                                    $data = array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'value'=>isset($end_date) ? $end_date : '','id'=>'end_date','class'=>'required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_end_time'), 'readonly'=>'readonly');
                                    echo form_input($data);?>
                                    <span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
					<!-- Start / end date fields end here -->
					
					<!-- Notification time input start here -->
					<div class="form-box1 form-box">
						<label for="notification_time"><?php echo lang('notification_time');?><i style="color:red;">*</i></label>
						<div class="half_date" style="margin-bottom:10px">
							<div class="form-group input-append date" id="hour_min_time">
								<?php
								$data	= array('name'=>'notification_time', 'data-format'=>'hh:mm', 'value'=>isset($notification_time) ? $notification_time : '','id'=>'notification_time','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_time'), 'readonly'=>'readonly', 'class'=>'required');
								echo form_input($data);
								?>
								<span class="add-on" id="notification_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-time"> </i> </span>
							</div>
						</div>
					</div>
					<!-- Notification time input end here -->
					
					<!-- Prize Frequency input start here -->
					<div class="form-box1 form-box">
						<label for="prize_frequency"><?php echo lang('prize_frequency');?><i style="color:red;">*</i></label>
						<div class="half_date">
							<?php
							$data = array('name'=>'prize_frequency','id'=>'prize_frequency', 'value'=> isset($prize_frequency) ? $prize_frequency : 7 , 'class'=>'span8 required digits','min'=>1);
							echo form_input($data);
							?>
						</div>
					</div>
					<!-- Prize Frequency input end here -->
					
					<!-- Second Chance Draw radio options start here -->
					<div class="form-box1 form-box">
						<label for="notification_time"><?php echo lang('number_of_second_chance');?></label>
						<div class="half_date" style="width:10px">
							<input type="checkbox" name="second_chance_draw" value="1" id="second_chance_draw" <?php echo (isset($second_chance_draw) && !empty($second_chance_draw)) ? 'checked="checked"' : '';?>  >
						</div>
						<div class="half_date" style="margin: 0 20px 10px">
							<?php
							$display_none = (isset($second_chance_draw) && !empty($second_chance_draw)) ? '' : 'display:none;';
							$data = array('name'=>'second_chance_count','id'=>'second_chance_count', 'value'=> isset($second_chance_count) ? $second_chance_count : 0 , 'class'=>'span8 required digits','style'=>$display_none);
							echo form_input($data);
							?>
						</div>
					</div>
					<!-- Second Chance Draw radio options end here -->
					
					<!-- Campaign beacon promotion radio options start here -->
					<div class="form-box1 form-box">
						<label for="beacon_promotion"><?php echo lang('beacon_promotion');?></label>
						<div class="control-group ">
							<input type="checkbox" name="beacon_promotion" value="1" id="beacon_promotion" <?php echo (isset($beacon_promotion) && !empty($beacon_promotion)) ? 'checked="checked"' : '';?>  >
						</div>
					</div>
					<!-- Campaign beacon promotion radio options end here -->
					
				</div>
				<!-- Images right side block start here -->
				<div class="imge-wrap img-wrapper">
					<label for="title" class="img-title"><?php echo lang('banner_image');?></label> 	
					<div class="img-box">
						<?php
						// set banner image action
						$banner_image_val = (isset($banner_image) && !empty($banner_image)) ? $banner_image :'';
						$img_1_display = (isset($banner_image_val) && !empty($banner_image_val)) ? '' :'dn';
						if(!empty($banner_image_val)) {
						$banner_image_src = base_url('uploads/scratchnwin_images/'.$banner_image);
						$title = lang('tooltip_offer_image');
							 $banner_image_src_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$banner_image; 
                             if(!file_exists($banner_image_src_doc)){
                                $promo_image_1 = base_url('uploads/No_Image_Available.png');
								$title = "No Image Available";
                             }
                            
						} else {
							$title = lang('tooltip_image');
							$banner_image_src = base_url('uploads/upload.png');
						} ?>
						<div class="cell-img" id="banner_image_cell">
							<img id="snw_banner_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
						</div>
						<?php
						$data	= array('name'=>'banner_image', 'id'=>'banner_image', 'value'=>$banner_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
						echo form_upload($data);
						// set banner image value in hidden type
						$data = array('name'=>'banner_image_val', 'value'=>$banner_image_val,'id'=>'banner_image_val','type'=>'hidden');
						echo form_input($data);
						?>
					</div>
					<div style="clear:both;"></div>
					<div class="alert red-alert alert-danger alert-dismissible ">
						<i class="icon fa fa-warning"></i>
						Upload up to 3MB images. 
					</div>
				</div>
				<!-- Images right side block end here -->	
				
				<!-- Beacon Offer details div start here -->
				<?php 
				$beacon_dn = (isset($beacon_promotion) && !empty($beacon_promotion)) ? '' : 'dn';
				// set beacon offer value in hidden type
				$data = array('name'=>'beacon_offer_id', 'value'=>(isset($beacon_offer_id) && !empty($beacon_offer_id)) ? $beacon_offer_id : 0,'id'=>'beacon_offer_id','type'=>'hidden');
				echo form_input($data);
				?>
				<div id="beacon_promotion_div" class="<?php echo $beacon_dn;?>">
					<div class="CompetitionS form-wrapper clearboth">
						<!-- Beacon offer stores dropdown start here -->
						<div class="form-box">
							<label for="content"><?php echo lang('offer_store');?><i style="color:red;">*</i></label>
							<div class="control-group ">
								<input type="checkbox" id="select_all"><span class="select-all"><?php echo lang('select_all');?></span>
								<?php
								$store_id_array = (isset($store_id) && !empty($store_id)) ? explode(',',$store_id) : '';
								$data	= array('name'=>'store_id[]','multiple'=>'multiple','id'=>'beacon_store_id', 'class'=>'required snw_beacon_store_select', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_store'));
								echo form_dropdown($data,$stores,set_value('store_id',$store_id_array));
								?>
							</div>
						</div>
						<!-- Beacon offer stores dropdown end here -->
						
						<!-- Beacon offer title start here -->
						<div class="form-box">
							<label for="title"><?php echo lang('beacon_offer_name');?><i style="color:red;">*</i></label>
							<?php
							$data	= array('name'=>'offer_name', 'value'=>set_value('offer_name',isset($offer_name) ? $offer_name : ''), 'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_name'));
							echo form_input($data);
							?>
						</div>
						<!-- Beacon offer title end here -->
						
						<!-- Beacon offer short description start here -->
						<?php /*?>
						<div class="form-box">
							<label for="offer_short_description"><?php echo lang('offer_short_description');?></label>
							<div class="control-group ">	
								<?php
								$data	= array('name'=>'offer_short_description', 'value'=>isset($offer_short_description) ? $offer_short_description : '');
								echo form_textarea($data);
								?>
							</div>	
						</div>
						<?php */
						$data = array('name'=>'offer_short_description', 'value'=>'','type'=>'hidden');
						echo form_input($data);
						?>
						<!-- Beacon offer short description end here -->
						
						<!-- Beacon offer long description start here -->
						<div class="form-box">
							<label for="offer_long_description"><?php echo lang('offer_long_description');?></label>
							<div class="control-group ">	
								<?php
								$data	= array('name'=>'offer_long_description', 'class'=>'mceEditor redactor span8', 'value'=>isset($offer_long_description) ? $offer_long_description : '');
								echo form_textarea($data);
								?>
							</div>
						</div>		
						<!-- Beacon offer long description end here -->
					</div>
					<!-- Beacon Offer details div end here -->
					
					<!-- Beacon Offer Image right side block start here -->	
					<div class="imge-wrap img-wrapper">
						<label for="title" class="img-title"><?php echo lang('beacon_offer_image');?></label> 	
						<div class="img-box">
							<?php
							// set banner image action
							$offer_image_val = (isset($offer_image) && !empty($offer_image)) ? $offer_image :'';
							$img_1_display = (isset($offer_image_val) && !empty($offer_image_val)) ? '' :'dn';
							if(!empty($offer_image_val)) {
							$offer_image_src = base_url('uploads/scratchnwin_images/'.$offer_image);
							$title = lang('beacon_tooltip_image');
                             $offer_image_src_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$offer_image; 
                             if(!file_exists($offer_image_src_doc)){
                                $promo_image_1 = base_url('uploads/No_Image_Available.png');
                                $title = "No Image Available";
                             }
							} else {
								$title = lang('beacon_tooltip_image');
								$offer_image_src = base_url('uploads/upload.png');
							} ?>
							<div class="cell-img" id="offer_image_cell">
								<img id="snw_offer_image_picker" src="<?php echo $offer_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
							</div>
							<?php
							$data	= array('name'=>'offer_image', 'id'=>'offer_image', 'value'=>$offer_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_banner_image(this)');
							echo form_upload($data);
							// set banner image value in hidden type
							$data = array('name'=>'offer_image_val', 'value'=>$offer_image_val,'id'=>'offer_image_val','type'=>'hidden');
							echo form_input($data);
							$data = array('name'=>'existing_offer_image', 'value'=>$offer_image_val,'type'=>'hidden');
							echo form_input($data);
							?>
							
						</div>
						<div style="clear:both;"></div>
						<div class="alert red-alert alert-danger alert-dismissible ">
							<i class="icon fa fa-warning"></i>
							Upload up to 3MB images. 
						</div>
						
					</div>
				</div>	
				<!-- Beacon Offer Image right side block end here -->	
				
			</div>
			<!-- End Content panel -->
			
			<!-- Start Attributes panel -->
			<div class="tab-pane co_products_tab" id="attributes_tab">
				<!-- Add product button -->
			<button onclick="add_row()" class="btn default add-product-btn"  type="button">Add Product</button>		
            <div class="clearboth"></div>
				<!-- Product list / form  start here -->
				<div class="clearboth product-grid-form">
					<table class="table table-bordered" id="product_tables"> 
						<thead class="bg_ccc">
							<tr>
								<th class='snw_product_table_th'><?php echo lang('product_number');?></th>
								<th class='snw_product_table_th'><?php echo lang('product_name');?></th>
								<th class='snw_product_table_th'><?php echo lang('product_promo_code');?></th>
								<th class='snw_product_table_th'><?php echo lang('prizes_per_day');?></th>
								<th class='snw_product_table_th'><?php echo lang('total_prizes');?></th>
								<!--<th class='snw_product_table_th'><?php //echo lang('prizes_sent_this_week');?></th>-->
								<th class='snw_product_table_th'><?php echo lang('total_number_claimed');?></th>
								<th class='snw_product_table_th'><?php echo lang('product_image');?></th>
								<th class='snw_product_table_th width155'><?php echo lang('product_avail_days');?></th>
								<th><?php echo lang('product_actions');?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$qty_row = 1;
							if(isset($snw_products) && !empty($snw_products) && count($snw_products) > 0) {
								foreach($snw_products as $snw_product) { ?>
									<tr class="snw-product-half-side" id="qty_row_<?php echo $qty_row;?>">
										<td>
											<?php
											$data = array('name'=>'product_ids[]', 'value'=>$snw_product['product_id'],'id'=>'product_id_'.$qty_row,'class'=>'product_input digits required','row_id'=>$qty_row,'exist_id'=>$snw_product['product_id']);
											echo form_input($data);
											$data = array('type'=>'hidden','name'=>'snw_product_ids[]','value'=>$snw_product['id']);
											echo form_input($data);
											?>
										</td>
										<td>
											<?php
											$data = array('name'=>'product_name[]', 'value'=>$snw_product['product_name'],'readonly'=>true,'id'=>'product_name_'.$qty_row,'row_id'=>$qty_row);
											echo form_input($data);?>
										</td>
										<td>
											<?php
											$data = array('name'=>'promo_code[]', 'value'=>$snw_product['promo_code'],'id'=>'promo_code_'.$qty_row,'row_id'=>$qty_row,'class'=>'promo_code required');
											echo form_input($data);?>
										</td>
										<td>
											<?php
											$data = array('name'=>'prizes_per_day[]','value'=>$snw_product['prizes_per_day'],'id'=>'prizes_per_day_'.$qty_row,'class'=>'prizes_count digits required','min'=>"1",'row_id'=>$qty_row,'is_per_day_count'=>1);
											echo form_input($data);?>
										</td>
										<td>
											<?php
											$data = array('name'=>'total_prizes[]', 'value'=>$snw_product['total_prizes'],'id'=>'total_prizes_'.$qty_row,'class'=>'prizes_count digits required','min'=>"1",'row_id'=>$qty_row);
											echo form_input($data);?>
										</td>
										<!--<td>
											<?php
											//$data = array('name'=>'prizes_sent_this_week[]', 'value'=>$snw_product['prizes_sent_this_week'],'readonly'=>true,'id'=>'prizes_sent_this_week_'.$qty_row,'row_id'=>$qty_row);
											//echo form_input($data);?>
										</td>-->
										<td>
											<?php
											// get product claimed total count
											$total_number_claimed = get_prise_claimed_count($snw_id,$snw_product['product_id']);
											$data = array('name'=>'total_number_claimed[]', 'value'=>$total_number_claimed,'readonly'=>true,'id'=>'total_number_claimed_'.$qty_row,'row_id'=>$qty_row);
											echo form_input($data);?>
										</td>
										<td>
											<?php
											$product_image_val = (!empty($snw_product['product_image'])) ? $snw_product['product_image'] :'';
											$img_display = (!empty($product_image_val)) ? '' :'dn';
											$title = lang('tooltip_promo_image');
											$product_image = base_url('uploads/upload.png');
											if(isset($product_image_val) && $product_image_val != '') {
												$product_image = base_url('uploads/scratchnwin_images/'.$product_image_val);
												$title = lang('tooltip_offer_image');
												$product_image_doc = $this->config->item('document_path').'uploads/scratchnwin_images/'.$product_image_val; 
                                                 if(!file_exists($product_image_doc)){
                                                    $product_image = base_url('uploads/No_Image_Available.png');
													$title = "No Image Available";
                                                 }   
											} ?>
											<div class="cell-img" id="product_image_cell_<?php echo $qty_row;?>"><img id="picker_product_image_<?php echo $qty_row;?>" class="product_img_picker" src="<?php echo $product_image; ?>"  data-toggle='tooltip', data-placement='right', title='' , row_id='<?php echo $qty_row;?>' /></div>
											<?php
											$data = array('name'=>'product_image[]', 'id'=>'product_image_'.$qty_row, 'value'=>$product_image_val, 'class'=>'product_input', 'data-toggle'=>'tooltip', 'data-placement'=>'right','row_id'=>$qty_row, 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_product_image(this)');
											echo form_upload($data);
											// set image value in hidden type
											$data = array('name'=>'product_image_val[]', 'value'=>$product_image_val,'id'=>'hidden_product_image_'.$qty_row,'type'=>'hidden');
											echo form_input($data);
											// set existing image value in hidden type
											$data = array('name'=>'existing_product_image[]', 'value'=>$product_image_val,'id'=>'existing_product_image_'.$qty_row,'type'=>'hidden');
											echo form_input($data);
											?>
										</td>
										
										<td>	
											<?php
											// set day array
											$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
											$days = (isset($snw_product['days_availability']) && !empty($snw_product['days_availability'])) ? explode(',',$snw_product['days_availability']) : $week_days;
											$days = array_filter($days);
											$day_btn_txt = '';
											$day_title_txt = '';
											foreach($days as $key=>$val) {
												// set button text
												$day_btn_txt .= ucfirst(substr($val,0,1));
												$day_btn_txt .= (count($days)>($key)) ? ' | ' : '';
												// set btn title text
												$day_title_txt .= ucfirst(substr($val,0,3));
												$day_title_txt .= (count($days)>($key)) ? ' | ' : '';
											}
											echo '<button id="daysavailability_btn_'.$qty_row.'" class="btn default daysavailability" snw_product_id = "'.$snw_product['id'].'"  row_id = "'.$qty_row.'" type="button" title="'.$day_title_txt.'">'.$day_btn_txt.'</button>';?>
											<!-- start the bootstrap modal where the image will appear -->
											<div class="modal fade" id="daysavailabilitymodal_<?php echo $qty_row;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title modal_title_<?php echo $qty_row;?>" id="myModalLabel">&nbsp;</h4>
														</div>
														<div class="modal-body modal-days-avail">
															<?php
															$week_days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
															foreach($week_days as $day) {
																$checked = (in_array($day,$days)) ? 'checked' : '';
																$is_day_checked = (in_array($day,$days)) ? 1 : 0;
																$short_day_name = ucfirst(substr($day,0,3));
																?>
																<input type="checkbox" name='<?php echo $short_day_name;?>[]' id='<?php echo $day.'_'.$qty_row;?>' value="<?php echo $day;?>" class='days_avail_check day_checkbox_cls_<?php echo $qty_row;?>'  <?php echo $checked;?> row_id = '<?php echo $qty_row;?>' short_day_name = '<?php echo $short_day_name;?>'>
																<input type="hidden" name='<?php echo $day;?>[]' value="<?php echo $is_day_checked;?>" id='day_checked_<?php echo $short_day_name.'_'.$qty_row;?>'>
																<?php
																echo ucfirst($day);
															 }
															 ?>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
														</div>
													</div>
												</div>
											</div>
											<!-- end the bootstrap modal where the image will appear -->
										</td>
										<td>
											<?php
											$btn_class = ($snw_product['status'] == '0') ? 'deactive-btn' : 'active-btn';
											$icon_class = ($snw_product['status'] == '0') ? 'fa-toggle-off' : 'fa-toggle-on';?>
											<span class="product_status_td_<?php echo $qty_row;?>">
												<a class="scratch-prod-icon <?php echo $btn_class;?>" id="product_status" href="javascript:;" snw_product_id = '<?php echo $snw_product['id'];?>'  row_id = '<?php echo $qty_row;?>' title="<?php echo lang('product_status_btn_note_'.$snw_product['status']);?>"><i class="fa <?php echo $icon_class;?>"></i></a>
											</span>
											<?php //if($qty_row != 1) { ?>
												<span class="romove_row scratch-prod-icon"  row_id=<?php echo $qty_row;?> snw_product_id='<?php echo $snw_product['id'];?>' title="<?php echo lang('product_delete_btn_note');?>"><i class="fa fa-trash"></i></span>
											<?php //} ?>
										</td>
										
									</tr>
									<?php
									$qty_row++;
									
								}
							} else {
								// render blank product entities 
								echo $product_form;
								$qty_row++;
							} ?>
						</tbody>	
					</table>
					<input type="hidden" value="<?php echo $qty_row;?>" name='last_qty_row' id='last_qty_row'>
					<!-- Product fields end here -->
				</div>
			</div>
			<!-- End Attributes panel -->
		</div>
	</div>
	<div class="text-right mt10">
		<?php
		// set current date in hidden field 
		$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
		echo form_input($data);?>
		<button type="submit" name="save" class="btn btn-primary"><?php echo lang('form_save');?></button>
		&nbsp;
		<a href='<?php echo base_url().'admin/settings/scratchnwin' ?>'>
			<button type="button" name="cancel" class="btn default"><?php echo lang('form_cancel');?></button>
		</a>
	</div>			
<?php form_close();?>

<script type="text/javascript">
	
	
	$(document).ready(function() {
		$("#add_scratchnwin_form").validate({
			
		});
	});
	
	/* 
	| -----------------------------------------------------
	| Handle form posting validation
	| -----------------------------------------------------
	*/
	$( "#add_scratchnwin_form" ).submit(function( event ) {
		
		// set start date for calculate day diffrence
		var start_date	=  $( "#start_date" ).val().split(' ');
		var startdateSplit = start_date[0].split('-');
		var new_start_date = startdateSplit[1] + '/' + startdateSplit[0] + '/' + startdateSplit[2];
		// set end date for calculate day diffrence
		var end_date	 =  $( "#end_date" ).val().split(' ');
		var enddateSplit = end_date[0].split('-');
		var new_end_date = enddateSplit[1] + '/' + enddateSplit[0] + '/' + enddateSplit[2];
		// show error if start date less then end date
		if(new_start_date >= new_end_date ) {
			bootbox.alert("End date must be greater than start date.");
			return false;
		}
		// show error if banner image not exist
		var banner_image_val = $('#banner_image_val').val();
		if(banner_image_val == '') {
			$('#banner_image_cell').addClass('error');
			bootbox.alert("Please upload banner image.");
			return false;
		}
		// show error if beacon offer image not exist
		var offer_image_val = $('#offer_image_val').val();
		if($("#beacon_promotion").is(':checked') && offer_image_val == ''){
			$('#offer_image_cell').addClass('error');
			return false;
		}
		
		var is_validate = true;
		$( ".product_input" ).each(function( e ) {
			var row_id = $(this).attr('row_id');
			var product_id = $('#product_id_'+row_id).val();
			var promo_code = $('#promo_code_'+row_id).val();
			var product_image = $('#hidden_product_image_'+row_id).val();
			
			var prizes_per_day = $('#prizes_per_day_'+row_id).val();
			var total_prizes_per_week = $('#total_prizes_'+row_id).val();
			
			if(product_id == '') {
				$('#product_id_'+row_id).addClass('error');
				is_validate =false;
			}
			if(promo_code == '') {
				$('#promo_code_'+row_id).addClass('error');
				is_validate =false;
			}
			if(product_image == '') {
				$('#product_image_cell_'+row_id).addClass('error');
				is_validate =false;
			}
			if(prizes_per_day == '') {
				$('#prizes_per_day_'+row_id).addClass('error');
				is_validate =false;
			}
			if(total_prizes_per_week == '') {
				$('#total_prizes_'+row_id).addClass('error');
				is_validate =false;
			}
		});
		
		if(is_validate == false) {
			bootbox.alert("Please fill all required product attributes.");
			return false;
		}
		return;
		event.preventDefault();
	});
	
	/* 
	| -----------------------------------------------------
	| Function used to remove image
	| -----------------------------------------------------
	*/
	$(document).delegate('.product_input','blur',function(e){
		var product_id = $(this).val();
		var row_id = $(this).attr('row_id');
		var exist_id = $(this).attr('exist_id');
		get_product_data(product_id,row_id,exist_id);
	});
	
	function get_product_data(product_id,row_id,exist_id) {
		
        var product_ids = new Array();
        var currentRowId = '';
        var j =0;
        $.each( $( ".product_input" ), function() {
            currentRowId = $(this).attr('row_id');
            if(row_id != currentRowId){
                product_ids[j] = $(this).val();
            j++;
            }
        });
        
		// check existing product id
		if(exist_id == product_id) {
			return false;
		} else if(exist_id != '' && exist_id == product_id) {
			bootbox.alert("Product already exist.");
			$('#product_id_'+row_id).val(exist_id);
			return false;
		} 
		
		if ($.inArray(product_id, product_ids) > -1) {
			bootbox.alert("Product already exist.");
			$('#qty_row_'+row_id).html('');
			return false;
		} else {
			$.ajax ({
				type: "POST",
				dataType:'jSon',
				data : {product_id:product_id},
				url: "<?php echo base_url() ?>admin/settings/scratchnwin/getproductdata",
				success: function(data){
					if(data.product_name != '') {
						$('#product_name_'+row_id).val(data.product_name);
					} else {
						$('#product_name_'+row_id).val('');
						$('#product_id_'+row_id).val('');
					}
				}
			});
		}
	}
	
	function read_url_product_image(input) {
		
		var pickerId = '#picker_'+input.id;
		var fieldId	= '#'+input.id;
		$('#hidden_'+input.id).val(input.value);
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	$(document).delegate('.product_img_picker','click',function(e){
		var row_id = $(this).attr('row_id');
		$("input[id='product_image_"+row_id+"']").click();
	});
	
	$("#snw_banner_image_picker").click(function() {
		$("input[name='banner_image']").click();
	});
	
	$("#snw_offer_image_picker").click(function() {
		$("input[name='offer_image']").click();
	});
	
	function read_url_banner_image(input) {
	
		var pickerId= '#snw_'+input.id+'_picker';
		var fieldId	= '#'+input.id;
		$(fieldId+'_val').val(input.value);
		
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	/* 
	| -----------------------------------------------------
	| Manage add quantity row 
	| -----------------------------------------------------
	*/ 
	function add_row() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
		last_row = parseInt(last_row)+1;
		$('#last_qty_row').val(last_row);
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {row_id:last_row},
			url: "<?php echo base_url() ?>admin/settings/scratchnwin/product_form",
			success: function(data){
				if(data.status) {
					$('#product_tables tr:last').after(data.product_form)
					// prepare qunatity row html
					//$("#multiple_qty").append(data.product_form);
				}
				
			}
		});
	}
	
	/* 
	| -----------------------------------------------------
	| Remove product row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".romove_row","click",function(e){
		var row_id = $(this).attr('row_id');
		var snw_product_id = $(this).attr('snw_product_id');
		var snw_id = '<?php echo $snw_id;?>';
		var rowCount = $('#product_tables >tbody >tr').length;
		if(rowCount == 1) {
			bootbox.alert("You have to keep at least one product in the campaign!");
			return false;
		}
		
		if(snw_product_id != undefined && snw_product_id != '') {
			bootbox.confirm("Are you sure want to delete this?", function(result) {
				if(result==true) {
					$.ajax ({
						type: "POST",
						dataType:'jSon',
						data : {snw_product_id:snw_product_id,snw_id:snw_id},
						url: "<?php echo base_url() ?>admin/settings/scratchnwin/remove_snw_products",
						success: function(data){
							if(data.status) {
								// hide the selected row
								$('#qty_row_'+row_id).remove();
							} else {
								bootbox.alert("You can't remove this product until it's in active price list!");
							}
						}
					});
				}
			});
		} else {
			// hide the selected row
			$('#qty_row_'+row_id).html('');
		}
	});
	
	
	/* 
	| -----------------------------------------------------
	| Get day diffrance betweem start and end date
	| -----------------------------------------------------
	*/
	function parseDate(str) {
		var mdy = str.split('/');
		return new Date(mdy[2], mdy[0]-1, mdy[1]);
	}

	function daydiff(first, second) {
		return (second-first)/(1000*60*60*24);
	}
	
	/* 
	| -----------------------------------------------------
	| Reset product's prizes sent count as 0
	| -----------------------------------------------------
	*/
	$(document).delegate('.reset_price_sent','click',function(e){
		var row_id = $(this).attr('row_id');
		var snw_product_id = $(this).attr('snw_product_id');
		var prizes_sent_this_week = $('#prizes_sent_this_week_'+row_id).val();
		if(prizes_sent_this_week == 0 || prizes_sent_this_week == '') {
			bootbox.alert("Prizes sent count must be greater then 0");
			return false;
		}
		if(snw_product_id != undefined && snw_product_id != '') {
			bootbox.confirm("Are you want to reset the prizes sent count?", function(result) {
				if(result==true) {
					$.ajax ({
						type: "POST",
						dataType:'jSon',
						data : {snw_product_id:snw_product_id},
						url: "<?php echo base_url() ?>admin/settings/scratchnwin/reset_prize_sent_log",
						success: function(data) {
							if(data.status) {
								// hide the selected row
								$('#prizes_sent_this_week_'+row_id).val(0);
							}
						}
					});
				}
			});
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Change product's status active or inactive
	| -----------------------------------------------------
	*/
	$(document).delegate('#product_status','click',function(e){
		var row_id = $(this).attr('row_id');
		var snw_product_id = $(this).attr('snw_product_id');
		var snw_id = '<?php echo $snw_id;?>';
		
		if(snw_product_id != undefined && snw_product_id != '') {
			bootbox.confirm("Are you want to change this status?", function(result) {
				if(result==true) {
					$.ajax ({
						type: "POST",
						dataType:'jSon',
						data : {snw_product_id:snw_product_id,row_id:row_id,snw_id:snw_id},
						url: "<?php echo base_url() ?>admin/settings/scratchnwin/change_product_status",
						success: function(data) {
							if(data.status == 1) {
								var status_html = '<a row_id='+row_id+' snw_product_id= '+snw_product_id+' href="javascript:;" id="product_status" class="scratch-prod-icon '+data.btn_class+'"><i class="fa '+data.icon_class+'"></i></a>';
								$('.product_status_td_'+row_id).html(status_html);
							} else if(data.status == 2) {
								bootbox.alert("You have to take atleast one active product!");
							}
						}
					});
				}
			});
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Validate Prizes counts
	| -----------------------------------------------------
	*/
	$(document).delegate('.prizes_count','blur',function(e){
		var row_id = $(this).attr('row_id');
		var is_per_day_count = $(this).attr('is_per_day_count');
		var prizes_per_day = $('#prizes_per_day_'+row_id).val();
		var total_prizes_per_week = $('#total_prizes_'+row_id).val();
		//console.log(prizes_per_day+'=='+total_prizes_per_week+'=='+is_per_day_count);
		prizes_per_day = parseInt(prizes_per_day);
		total_prizes_per_week = parseInt(total_prizes_per_week);
		if(prizes_per_day != '' && total_prizes_per_week != '' && prizes_per_day >= total_prizes_per_week) {
			if(is_per_day_count == 1 && is_per_day_count != undefined) {
				$('#prizes_per_day_'+row_id).val('');
			} else{
				$('#total_prizes_'+row_id).val('');
			}
			bootbox.alert("Total prizes should be greater than Prizes per day!");
			return false;
		}
		
	});
	
	$(document).ready(function() {
		if(jQuery('#start_date_div1').length > 0)  { 
			jQuery('#start_date_div1').datetimepicker({
				pickTime: false,
				startDate: new Date(), 
			});
		}

		if(jQuery('#end_date_div1').length > 0)  { 
			jQuery('#end_date_div1').datetimepicker({
				pickTime: false,
				startDate: new Date(), 
			});
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Manage availabily of second draw count
	| -----------------------------------------------------
	*/
	$('#second_chance_draw').click(function() {
		if($("#second_chance_draw").is(':checked')){
			$('#second_chance_count').show();
		} else {
			$('#second_chance_count').hide();
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Manage beacon offer availabily of promotion
	| -----------------------------------------------------
	*/
	$('#beacon_promotion').click(function() {
		if($("#beacon_promotion").is(':checked')){
			$('#beacon_promotion_div').fadeIn('slow');
		} else {
			$('#beacon_promotion_div').fadeOut('slow');
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Manage select all stores
	| -----------------------------------------------------
	*/
	$('#select_all').click(function() {
		if($("#select_all").is(':checked')) {
			$('#beacon_store_id option').prop('selected', true);
		} else {
			$('#beacon_store_id option').prop('selected', false);
		}
	});
	
	/*
	| ------------------------------------------------------------
	| Manage available day's hidden value
	| ------------------------------------------------------------
	*/ 
	$(document).delegate('.days_avail_check','click',function(e){
		
		var row_id = $(this).attr('row_id');
		var value = $(this).attr('value');
		var short_day_name = $(this).attr('short_day_name');
		var id = value+'_'+row_id;
		if($("#"+id).is(':checked')){
			$('#day_checked_'+short_day_name+'_'+row_id).val('1');
		} else {
			$('#day_checked_'+short_day_name+'_'+row_id).val('0');
		}
		
		// set week button text
		var day_btn_val = '';
		var day_btn_title = '';
		var is_day_avail = false;
		$('input.day_checkbox_cls_'+row_id+'[type=checkbox]').each(function () {
			if(this.checked) {
				var day_val = $(this).val();
				// set button html text
				day_btn_val += day_val[0].toUpperCase();
				day_btn_val += ' | ';
				// set button title text
				day_btn_title += day_val[0].toUpperCase() + day_val.substring(1,3);
				day_btn_title += ' | ';
				is_day_avail = true;
			}
		});
		
		if(is_day_avail) {
			day_btn_val = $.trim(day_btn_val);
			day_btn_title = $.trim(day_btn_title);
			day_btn_val = (day_btn_val != '') ? day_btn_val.slice(0, -1) : '';
			day_btn_title = (day_btn_title != '') ? day_btn_title.slice(0, -1) : '';
			$('#daysavailability_btn_'+row_id).attr('title',day_btn_title);
			$('#daysavailability_btn_'+row_id).html(day_btn_val);
			$('.modal_title_'+row_id).html('&nbsp;');
		} else {
			$('.modal_title_'+row_id).addClass('error');
			$('.modal_title_'+row_id).html('Please select atleast one day!');
			$("#"+id).parents('span').addClass("checked");
			$("#"+id).prop('checked', 'checked');
			$('#day_checked_'+short_day_name+'_'+row_id).val('1');
		}
	});
	
	
	/*
	| ------------------------------------------------------------
	| Show bootstrap image popup on listing view
	| ------------------------------------------------------------
	*/ 
	$(document).delegate( ".daysavailability", "click", function(e) {
		var row_id = $(this).attr('row_id');
		$('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
		$('#daysavailabilitymodal_'+row_id).modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
		$('.modal_title_'+row_id).html('&nbsp;');
	});
</script>


