<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Avaitor
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Avaitor
 * @author    Avaitor Dev Team
 * @copyright Copyright (c) 2011 - 2012, Avaitor Dev Team
 * @license   http://guides.ciAvaitor.com/license.html
 * @link      http://ciAvaitor.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Settings Module Model
 *
 * Provides methods to retrieve and update settings in the database
 *
 * @package    Avaitor
 * @subpackage Modules_Settings
 * @category   Models
 * @author     Avaitor Dev Team
 * @link       http://guides.ciAvaitor.com/helpers/file_helpers.html
 *
 */
class Settings_model extends BF_Model
{

	/**
	 * Name of the table
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $table		= 'settings';

	/**
	 * Name of the primary key
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $key			= 'name';

	/**
	 * Use soft deletes or not
	 *
	 * @access protected
	 *
	 * @var bool
	 */
	protected $soft_deletes	= FALSE;

	/**
	 * The date format to use
	 *
	 * @access protected
	 *
	 * @var string
	 */
	protected $date_format = 'datetime';

	/**
	 * Set the created time automatically on a new record
	 *
	 * @access protected
	 *
	 * @var bool
	 */
	protected $set_created = FALSE;

	/**
	 * Set the modified time automatically on editing a record
	 *
	 * @access protected
	 *
	 * @var bool
	 */
	protected $set_modified = FALSE;

	//--------------------------------------------------------------------


	/**
	 * A convenience method that combines a where() and find_all()
	 * call into a single call.
	 *
	 * @access public
	 *
	 * @param string $field The table field to search in.
	 * @param string $value The value that field should be.
	 *
	 * @return array
	 */
	public function find_all_by($field=NULL, $value=NULL)
	{
		if (empty($field)) return FALSE;

		// Setup our field/value check
		$this->db->where($field, $value);

		$results = $this->find_all();

		$return_array = array();

		if (is_array($results) && count($results))
		{
			foreach ($results as $record)
			{
				$return_array[$record->name] = $record->value;
			}
		}

		return $return_array;

	}//end find_all_by()
	
	/**
	 * Method to check existance of custom settings(S3)
	 * @access public
	 * @param 
	 * @param 
	 * @return bool
	 */
	public function get_s3_settings()
	{
		$this->db->where('module','files');
		$s3_settings = $this->db->get($this->table);
		$s3_res = $s3_settings->result();
		if(!empty($s3_res))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to save custom settings(S3)
	 * @access public
	 * @param array
	 * @return bool
	 */
	public function save_s3_settings($s3_settings)
	{
		if(!empty($s3_settings)){
			$data = array();
			$i=0;
			foreach($s3_settings as $key=>$value)
			{
				$data[$i]['name'] = $key;
				$data[$i]['value'] = $value;
				$data[$i]['module'] = 'files';
			$i++;}
			
			return $status = $this->db->insert_batch($this->table,$data);
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to add new allowed extension
	 * @access public
	 * @param array
	 * @return bool
	 */
	function add_new_ext($ext='')
	{
		if(!empty($ext))
		{
			$get_ext = $this->db->get_where('allowed_file_ext',array('ext'=>$ext))->row();
			if(empty($get_ext))
			{
				$this->db->select('value');
				$where = array('name'=>'allowed_ext');
				$config_ext = $this->db->get_where($this->table,$where)->row();
				$this->db->where($where);
				$this->db->update($this->table,array('value'=>$config_ext->value.','.$ext));
				
				$res = $this->db->insert('allowed_file_ext',array('ext'=>$ext,'is_enabled'=>1));
				return array('status'=>1);
			}
			else
			{
				return array('status'=>0,'message'=>lang('ext_already_exists')); 
			}
		}
	}
	
	/**
	* Method to get user's information
	* @access public
	* @param 
	* @return bool
	*/
	function get_users($search_key='')
	{
		$role_id = $this->session->userdata['role_id'];
		if(!empty($search_key))
		{
			$this->db->select('username,id,role_id,display_name,email');
			$this->db->like('username',$search_key,'after');
			$this->db->where('role_id !=',$role_id);
			$this->db->order_by('username');
			return $user_info = $this->db->get('users')->result();
		}
		else
		{
			return false;
		}
	}
	

}//end Settings_model
