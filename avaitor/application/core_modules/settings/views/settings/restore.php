<div class="form-horizontal">
	<div class="admin-box">
		<fieldset>
		<legend><?php echo lang('restore_users_file_system'); ?></legend>
			<div class="control-group">
				<label for="bucketName" class="control-label"><?php echo lang('search_user'); ?></label>
				<div class="controls">
					<div class="search_bar">
						<div class="fleft span6" style="margin-left:0;">
							<input type="text" autocomplete="off" class="span6" style="" value="" id="user_search_bar" name="user_search_bar" data-toggle="tooltip" title="<?php echo lang('select_user_to_restore');?>">
						</div>
						<div class="clear"></div>
						<div class="autocomplete" style="min-width: 570px !important;"></div> 
					</div>
				</div>
			</div>
		</fieldset>
		<div class="restore_confirm">
			<div class="restore_confirm_txt" style="font-size: 20px;"><span class="warning_custom_sign"><i class="fa fa-warning"></i></span><?php echo lang('restore_alert1');?> <b class="restore_username"></b>'<?php echo lang('restore_alert2');?></div>
			<div class="" style="font-size: 14px;margin: 10px 0;"><?php echo lang('restore_alert3');?></div>
			<div class="restore_buttons">
				<button name="restore button" id="restore" class="btn btn-primary" value="Restore"><?php echo lang('restore');?></button>
				<button name="cancel button" value="Cancel" class="cancel_restore btn" onclick="$('.restore_confirm').hide();"><?php echo lang('cancel');?></button>
			</div>
			<div class="demo-wrapper progress_bar html5-progress-bar">
				<div class="progress-bar-wrapper">
					<progress id="progressbar" value="0" max="100"></progress>
					<span class="progress-value">0%</span>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var xmlHttpObject = null;
		$("#user_search_bar").keyup(function(){
			search_key = $(this).val();
			xmlHttpObject = $.ajax({
				url : "<?php echo site_url('settings/search_users') ?>",
				type: "POST",
				dataType:"jSon",
				beforeSend:function(){
					if(xmlHttpObject!=null)
					{
						xmlHttpObject.abort();
					}
				},
				data:{search_key:search_key},
				success:function(resp)
				{
					if(resp.status=='ok')
					{
						$('.autocomplete').html(resp.data);
						$('.autocomplete').fadeIn('slow');
						$(".ff").each(function() {
							var letters = '0123456789'.split('');
							var color = '#';
							for (var i = 0; i < 6; i++) {
								letter = letters[Math.round(Math.random() * 10)];
								if(letter==undefined){letter = 9;}
								color += letter;
							}
							$(this).css("background-color", color);
						});
					}
					else
					{
						$('.autocomplete').html(resp.message);		
						$('.autocomplete').fadeIn('slow');				
					}
				}
			});
		});
	});
	
	
	$(document).delegate('.user_search_content','click',function()
	{
		var user_id = $(this).attr('user_id');
		restore_username  = $(this).attr('user_name');
		$('.restore_username').html(restore_username);
		$('#restore').attr('user_id',user_id);
		$('.restore_confirm').show();
		$('.restore_buttons').show();
		reset_progress();
		$('.progress_bar').hide();
		$('.autocomplete').html('');
	});
	
	
	$(document).delegate('#restore','click',function()
	{
		user_id = $(this).attr('user_id');
		$('.restore_buttons').hide();
		$('.progress_bar').show();
		$.ajax({
			url : "<?php echo site_url('settings/update_filesystem') ?>",
			type: "POST",
			dataType:"jSon",
			data:{user_id:user_id},
			beforeSend:function()
			{
				showProgress(90);
			},
			success:function(resp)
			{
				if(resp.status=='Success')
				{
					showProgress(100);
					$('.restore_confirm').append('<div class="success_restore custom_msg_suc"><?php echo lang('system_restore_success');?></div>');
					$('.restore_confirm').fadeOut(5000);
				}
				else
				{
					$('.restore_confirm').append('<div class="success_restore">'+resp.message+'</div>');
				}
			}
		});
	});
	
	//else where click hide div
	$(document).click(function(e){
		$('.autocomplete').hide();
	});
	
	$(document).delegate(".autocomplete,#user_search_bar",'click',function(e){
			e.stopPropagation();
		});
	
	function reset_progress()
	{
		$('#progressbar').val(0);
		$('#progressbar').attr('max',100);
		$('.success_restore').hide();
	}
	
	function showProgress(limit)
	{
		if(!Modernizr.meter)
		{
			alert('Sorry your brower does not support HTML5 progress bar');
		}
		else
		{
			var progressbar = $('#progressbar'),
				max		= limit,
				time	= (100/max)*5,	
				value	= progressbar.val();

			var loading = function() 
			{
				value += 1;
				addValue = progressbar.val(value);
				
				$('.progress-value').html(value + '%');

				if(value == max) 
				{
					clearInterval(animate);			           
				}
			};
			var animate = setInterval(function() {
				loading();
			}, time);
		};
	};
	
	
</script>
