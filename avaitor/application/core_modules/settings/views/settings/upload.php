<div class="tab-pane" id="uploader">
		<fieldset>
			<legend><?php echo lang('upload_setting') ?></legend>
			<div class="control-group">
				<label class="control-label" for="bucketName"><?php echo lang('max_upload_size') ?></label>
				<div class="controls">
					<input type="text" name="s3[max_upload_size]" id="max_upload_size" value="<?php echo set_value('s3[max_upload_size]', isset($max_upload_size) ? $max_upload_size : '')  ?>" class="span1" /> MB
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="bucketName"><?php echo lang('allowed_ext') ?></label>
				<div class="controls">
					<?php 
					$extension_options = get_file_ext();
					if(!empty($allowed_ext)){$extensions = explode(',',$allowed_ext);}?>
						<?php foreach($extension_options as $opt):?>
						<div class="ext_checkbox_container">
							<div class="ext_checkbox">
							<input type="checkbox" name="s3[allowed_ext][]" <?php if(!empty($extensions)){if(in_array($opt->ext,$extensions)){echo "checked='checked'";}}?> value="<?php echo $opt->ext;?>">
							</div>
							<div class="ext_checkbox_label"><?php echo $opt->ext;?></div>
						</div>
						<?php endforeach;?>
				</div>
			</div>
			
			
			<div class="control-group">
				<label class="control-label" for="add_new_ext"><?php echo lang('add_new_ext') ?></label>
				<div class="controls">
					<input type="text" name="s3[add_new_ext]" id="ext_name" class="span4" value="<?php echo set_value('s3[add_new_ext]', '') ?>" />
					<input type="button" value="<?php echo lang('add_ext') ?>" class="btn btn-primary" name="add_ext" id="add_ext" >
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="allowed_upload_limit"><?php echo lang('allowed_upload_limit') ?></label>
				<div class="controls">
					<input type="text" name="s3[allowed_upload_limit]" id="allowed_upload_limit" class="span1" value="<?php echo set_value('s3[allowed_upload_limit]', isset($allowed_upload_limit) ? $allowed_upload_limit : '') ?>" /> GB
				</div>
			</div>
			
			<legend><?php echo lang('s3_setting') ?></legend>
			<div class="control-group">
				<label class="control-label" for="is_on_s3"><?php echo lang('upload_on_s3') ?></label>
				<div class="controls">
					<input type="radio" value="1" name='s3[is_on_s3]' <?php if(!empty($is_on_s3)){echo "checked='checked'";}?> >
					<span>Yes</span>
					<input type="radio" value="0" <?php if(empty($is_on_s3)){echo "checked='checked'";}?> name='s3[is_on_s3]'>
					<span>No</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="title"><?php echo lang('access_key') ?></label>
				<div class="controls">
					<input type="text" name="s3[access_key]" id="access_key" class="span4" value="<?php echo set_value('s3[access_key]', isset($access_key) ? $access_key : '') ?>" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="secret_key"><?php echo lang('secret_key') ?></label>
				<div class="controls">
					<input type="text" name="s3[secret_key]" id="secret_key" class="span4" value="<?php echo set_value('s3[secret_key]', isset($secret_key) ? $secret_key : '') ?>" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="amazon_host"><?php echo lang('amazon_host') ?></label>
				<div class="controls">
					<input type="text" name="s3[amazon_host]" id="amazon_host" class="span4" value="<?php echo set_value('s3[amazon_host]', isset($amazon_host) ? $amazon_host : '') ?>" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="aws_host"><?php echo lang('aws_host') ?></label>
				<div class="controls">
					<input type="text" name="s3[aws_host]" id="aws_host" value="<?php echo set_value('s3[aws_host]', isset($aws_host) ? $aws_host : '')  ?>" class="span4" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="bucketName"><?php echo lang('bucketName') ?></label>
				<div class="controls">
					<input type="text" name="s3[bucketName]" id="bucketName" value="<?php echo set_value('s3[bucketName]', isset($bucketName) ? $bucketName : '')  ?>" class="span4" />
				</div>
			</div>
			<input type="hidden" value="" id="is_uploader" name="s3[is_uploader]">
		</fieldset>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.nav-tabs > li > a').click(function(){
		hash = $(this).attr('href');
		window.location.hash = hash;
		if(hash=="#uploader")
		{
			$('#is_uploader').val(1);
		}
		else
		{
			$('#is_uploader').val(0);
		}
	});
	
	if(window.location.hash=='#uploader')
	{
		$('.nav-tabs > li').removeClass('active');
		$('.tab-pane').removeClass('active');
		$('#upload_button').addClass('active');
		$('#uploader').addClass('active');
		$('#is_uploader').val(1);
	}
	
	$('#add_ext').click(function(){
		var ext_name = $('#ext_name').val();
		obj	= $(this); 
		$.ajax({
			url : BASEURL+"admin/settings/settings/save_extensions",
			type: "POST",
			dataType:"jSon",
			data:{ext_name:ext_name},
			success:function(resp){
				if(resp.status)
				{
					window.location.reload();
				}	
				else
				{
					obj.parent().append('<p class="ext_message">'+resp.message+'</p>');
					$('.ext_message').fadeOut(3000);
				}
			},
		});
	});
});
</script>	

