<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Competition Controller
 *
 * Manages the competition functionality on the admin pages.
 *
 * @package    Avaitor
 * @subpackage Modules_Competition
 * @category   Controllers
 * @author     CDN Team
 *
 */
class Settings extends Admin_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required permissions
	 *
	 * @return void
	 */
	public function __construct()
    {
		parent::__construct();

		$this->auth->restrict('Core.Users.View');
        $this->lang->load('beaconoffers_admin');
		$this->load->library('form_validation');
		$this->load->model('targetpush/Targetpush_model', 'target_model');
	}//end __construct()

    
   

 
    /* ----------- Changes For Target Notification ---------
     * Amit Neema
     * 2016-07-15 
     * @access: public
	 * @return: void
	 */
	public function targetnotification() {
		// fetch target notification type  data
		$offers = $this->target_model->get_offer_listing(0,2); 
		// set params in view loading
		Template::set('targetpush',$offers);
		Template::set_view('target_notification_listing');
		Template::render();
        
	}
    
    
        
	/**
	 * Manege Target Notification form
	 * @access: public
	 * @auther: Amit Neema
	 * @return: void
	 */
	public function filter_members($id="") {
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode offer id
		$data = array();
		
        $data['offer_title']  = lang('add_filter_target_notification');
        // get simple offer data	
		$data['start_date']   = date('d-m-Y');
		$data['end_date']     = date('d-m-Y');
		$data['start_time']   = '00:00:00';
		$data['end_time']     = '23:23:59';
		
        $data['basket_size']     = '';
		$data['product_group']   = '';
		Template::set('data',$data);
		Template::set_view('filter_member_form');
		Template::render();	
	}
		
        
    
     
    /* ----------- Use for create Target Notification Message ---------
     * Amit Neema
     * 2016-07-15 
     * @access: public
	 * @return: void
	 */
	public function create_member() {
		// Redirect to create target notification messge */
        	redirect('admin/settings/targetpush/create_target_notification/');
		
	}
    
    
		
        /**
	 * Function to render competition product form 
	 * @input : row_id
	 * @return  void
	 * @access public
	 */
	public function filter_product()
	{
		// get data from post
		$row_id = $this->input->post('row_id');
		$data['row_id'] = $row_id;
		$tpl_data = $this->load->view('target_product_form',$data,true);
		echo json_encode(array('status'=>true,'product_form'=>$tpl_data));die;

	}//end product_form()
    
    /** 
     * Function to filter member action 
     * Amit Neema
     * */
    public function filter_members_action(){
        $post_data = $this->input->post();
        $is_datesearch = $this->input->post('is_datesearch');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $is_basketsize = $this->input->post('is_basketsize');
        $basketEG = !empty($this->input->post('basketEG'))?$this->input->post('basketEG'):0;
        $basketEL = !empty($this->input->post('basketEL'))?$this->input->post('basketEL'):0;
        $is_productcount = $this->input->post('is_productcount');
        $productcountEG = !empty($this->input->post('productcountEG'))?$this->input->post('productcountEG'):0;
        $productcountEL = !empty($this->input->post('productcountEL'))?$this->input->post('productcountEL'):0;
        $is_product_group = $this->input->post('is_product_group');
        $product_ids = $this->input->post('product_ids_val');
        $product_ids = (!empty($product_ids)) ? explode(',',$product_ids) : '';
        $member_data = '';
        $is_result = false;
        if(!empty($is_datesearch) && !empty($start_date) && !empty($end_date)) {
			 // set purchase start date
            $start_date = date('Ymd',strtotime($start_date)); // like 20150120
            // set purchase end date
            $end_date = date('Ymd',strtotime($end_date)); // like 20160428
            // set purchase start time
            $start_time = date('His',strtotime($start_time)); // like 20150120
            // set purchase end time
            $end_time = date('His',strtotime($end_time)); // like 20160428
		} else {
			// set date time values as blank
			$start_date	 = '';
			$end_date	 = '';
			$start_time	 = '';
			$end_time 	 = '';
		}
        
		if(empty($is_product_group)){
			$product_ids = array(); 
		}
		if(!empty($is_datesearch) || !empty($basketEG) || !empty($basketEL) || !empty($product_ids) || !empty($productcountEG) || !empty($productcountEL)) {
			$data['memberListing'] = $this->target_model->member_search_filter($start_date,$end_date,$start_time,$end_time,$basketEG,$basketEL,$product_ids,$productcountEG,$productcountEL);
			if(!empty($data['memberListing'])){
			   $member_data = $this->load->view('member_listing_view',$data,true);
			   $is_result = true;
			}else{
				$is_result = true;
				$member_data = $this->load->view('member_listing_view',$data,true);
			}
		}
        
        echo json_encode(array('html'=>$member_data,'is_result'=>$is_result));
        die;
    }
    
    
    /**
	 * Function to get product data
	 * @input  : product_id
	 * @output : string
	 * @access : public
	 */
	public function getproductdata() {
		// get data from post
		$product_id = $this->input->post('product_id');
		$product_name = '';
		if(!empty($product_id)) {
			// get product details from product id
			$product_data = $this->target_model->get_product_data($product_id);
			// set product name
			$product_name = (isset($product_data->Prod_Desc) && !empty(isset($product_data->Prod_Desc))) ? $product_data->Prod_Desc : '';
		} 
		// set return json response
		echo json_encode(array('product_name'=>$product_name));die;
	}


    /**
     * Function to Member Direct Filter
     * @input  : product_id
     * @output : string
     * @access : public
     */
    public function member_direct_search() {
        $post_data = $this->input->post();
        $member_id = $this->input->post('membersearchId');
        $is_result = false;
        if(!empty($member_id)){
            $data['memberListing'] = $this->target_model->member_direct_search($member_id);
            if(!empty($data['memberListing'])){
               $member_data = $this->load->view('member_direct_view',$data,true);
               $is_result = true;
            }else{
                 $is_result = true;
                $member_data = $this->load->view('member_direct_view',$data,true);
            }            
            echo json_encode(array('html'=>$member_data,'is_result'=>$is_result));
            die;
        }
    }

    
    
    //--------------------------------------------------------------------

	/**
	 * Manege Target Notification form
	 * @access: public
	 * @auther: Amit Neema
	 * @return: void
	 */
    public function create_target_notification($id="") {
        $data = array();
        
        /* for member filter */
        $memberId = '';
        $memberName = '';
        $data['memberName'] = $memberName;  
        if(empty($id)){
              if($this->input->post()){
                    $post_data = $this->input->post(); 
                    $member_ids_val = $this->input->post('member_ids_val');
                    $member_name_val = $this->input->post('member_name_val');
                    if(!empty($member_name_val)){
                        $memberName = $member_name_val;
                    }             
                    if(!empty($member_ids_val)){
                        $members_val = explode(',',$member_ids_val);
                        foreach($members_val as $val){
                          if(!empty($val)){
                                $memberId .= '-'.$val.'-,';
							}                                       
                        }
                        $memberId = rtrim($memberId,',');
                    }
                    $data['memberId'] = $memberId;  
                    $data['memberName'] = $memberName;  
                }else{
                    redirect('admin/settings/targetpush/filter_members/'.$id);
                }
              
        }  
        
        
        
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$id = decode($id); // decode offer id

		if((isset($_POST['save'])) && $id=='') {
			if($this->save_simple_targetpush()) {       
					Template::set_message(lang('message_saved_targetpush'),'success');
					redirect('admin/settings/targetpush/targetnotification');
			}
		} else if(isset($_POST['save'])) {
			if($this->save_simple_targetpush('update',$id)) {
					Template::set_message(lang('message_update_targetpush'),'success');
					redirect('admin/settings/targetpush/targetnotification');
			}
		}
		$data['offer_title']	= lang('add_new_target_notification');
		$offer = array();
		if($id) {
			$data['offer_title']		= lang('edit_target_notification');
			$targetpush_id = $id;
			// get simple offer data
			$offer = $this->target_model->get_offer_listing($targetpush_id);
            if(empty($offer)) {
				Template::set_message(lang('message_invalid_targetpush'),'error');
				redirect('admin/settings/targetpush/targetnotification');
			}else{
                $offer = $offer[0];
            }	
		}
		
		// set input form values
		$data['targetpush_id']		        = (!empty($targetpush_id)) ? encode($targetpush_id) : '';
		$data['targetpush_name']			= isset($offer['targetpush_name']) ? $offer['targetpush_name'] : '';		
		$data['targetpush_price']			= isset($offer['targetpush_price']) ? $offer['targetpush_price'] : '';		
		$data['targetpush_short_description']= isset($offer['targetpush_short_description']) ? $offer['targetpush_short_description'] : '';	
		$data['targetpush_long_description']	= isset($offer['targetpush_long_description']) ? $offer['targetpush_long_description'] : '';	
		$data['targetpush_image_1']			= isset($offer['targetpush_image_1']) ? $offer['targetpush_image_1'] : '';	
		$data['targetpush_image_2']			= isset($offer['targetpush_image_2']) ? $offer['targetpush_image_2'] : '';
		$data['targetpush_image_3']			= isset($offer['targetpush_image_3']) ? $offer['targetpush_image_3'] : '';
		$data['start_date']				= (isset($offer['notification_start_date'])) ? date("d-m-Y",strtotime($offer['notification_start_date'])): date('d-m-Y');
		$data['end_date']				= (isset($offer['notification_end_date'])) ?  date("d-m-Y",strtotime($offer['notification_end_date'])) : date('d-m-Y');
		$data['is_banner_image']		= isset($offer['is_banner_image']) ? $offer['is_banner_image'] : '';	
		$data['barcode_image']			= isset($offer['barcode_image']) ? $offer['barcode_image'] : '';
		$data['member_ids']	       		= (isset($offer['member_ids']) && !empty($offer['member_ids'])) ?  $offer['member_ids'] : $memberId;	
        $data['barcode']	       		= isset($offer['barcode']) ? $offer['barcode'] : '';	
		$data['filters']	       		= isset($offer['filters']) ? $offer['filters'] : '';	
        
        
		Template::set('data',$data);
		Template::set_view('target_notification_form');
		Template::render();	
	}



	/**
	 * Manege simple target notification form
	 * @access: public
	 * @auther: Tosif Qureshi
	 * @return: void
	 */
	public function save_simple_targetpush($type = 'insert',$id=0) {
		$data = array();
		if($id != 0) {
			$_POST['id'] = $id;
			$data['targetpush_id'] = $id;
		}
		// apply validation rules for input params
		$this->form_validation->set_rules('targetpush_name', 'lang:targetpush_name', 'trim|required');		
		$this->form_validation->set_rules('targetpush_short_description', 'lang:targetpush_short_description', 'trim');	
		$this->form_validation->set_rules('targetpush_long_description', 'lang:targetpush_long_description', 'trim');	
		//$this->form_validation->set_rules('barcode', 'lang:offer_barcode', 'trim|numeric|required|max_length[12]');	
		if($this->form_validation->run() === FALSE) {
			Template::set_message("* Marked fields are required",'error');
			
			return FALSE;
		}
		// upload banner images
		if (isset($_FILES['targetpush_image_1']) && ($_FILES['targetpush_image_1']['name']) != '' ) {
			$data['targetpush_image_1'] = $this->to_upload_image('targetpush_image_1');
		}
		if (isset($_FILES['targetpush_image_2']) && ($_FILES['targetpush_image_2']['name']) != '' ) {
			$data['targetpush_image_2'] = $this->to_upload_image('targetpush_image_2');
		}
		if (isset($_FILES['targetpush_image_3']) && ($_FILES['targetpush_image_3']['name']) != '' ) {
			$data['targetpush_image_3'] = $this->to_upload_image('targetpush_image_3');
		}
		
		// set post values in store bundle		
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$start_date = date("Y-m-d",strtotime($start_date));
		$end_date = date("Y-m-d",strtotime($end_date));
		$data['targetpush_name']    	= ($this->input->post('targetpush_name'));
		$data['targetpush_price']    = $this->input->post('targetpush_price');
		$data['targetpush_short_description']    = ($this->input->post('targetpush_short_description'));
		$data['targetpush_long_description']    	= $this->input->post('targetpush_long_description');
		$data['member_ids']    	= $this->input->post('member_ids');
		$data['notification_start_date']    	= $start_date;
		$data['notification_end_date']    	= $end_date;
		$data['updated_at'] 	= date('Y-m-d H:i:s');
		$data['status'] 		= 1;
		$data['is_simple_offer']= 2;
		if($this->input->post('is_banner_image')) {
			$data['is_banner_image']     = $this->input->post('is_banner_image');
		} else {
			$data['is_banner_image']     = '0';
		}

		// manage barcode
		$barcode = $this->input->post('barcode');
		if(!empty($barcode)) {
			// get barcode data
			$barcode_data = $this->target_model->get_barcode_data($barcode,2,$id);
			if($barcode_data == 0) {
				// generate barcode image
				$barcode_image = $this->generate_zend_barcode($barcode);
			}
		}
		// set barcode values
		$data['barcode'] = (!empty($barcode)) ? $barcode : '';
		$data['barcode_image']   = (isset($barcode_image) && !empty($barcode_image)) ? $barcode_image : '';
		// manage storing offer data
		if($type == 'insert') {
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->target_model->add_targetpush($data);
		}
		else if ($type == 'update') {
			// update offer data
			$id = $this->target_model->add_targetpush($data);
		}
		return $id;

	}
	
    
    
    
	function to_upload_image($image='') {
		$dirPath = IMAGEUPLOADPATH.'offer_images/';
		$config['upload_path'] = $dirPath;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = '5120';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload($image)){						
			$error = $this -> upload -> display_errors();
			Template::set_message($error,'error');
			redirect('admin/settings/targetpush/targetnotification');					
		}
		$imageData = $this -> upload -> data();
		if (is_array($imageData)) {
			return $imageData['file_name'];
		}
	} //to_upload_image
	/*Offers : End*/
		
/**
	 * Function to generate barcode from zend library
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	private function generate_zend_barcode($barcode='')
	{
	
		// load zend library
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		// prepare barcode from zend library
		$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
		// set barcode image path
		$dir_path = IMAGEUPLOADPATH.'barcodes/';
		// set barcode image name
		//$image_name = time().$barcode;
		$image_name = $barcode.'.png';
		// create image from barcode
		ImagePNG($file, $dir_path.$image_name);
		ImageDestroy($file);
		return $image_name;
		
	}


    public function offer_delete($id='') {
        $data['targetpush_id'] = decode($id);
        $data['is_delete'] = 1;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $id = $this->target_model->add_targetpush($data);
        Template::set_message(lang('message_delete_targetpush'),'success');
        redirect('admin/settings/targetpush/targetnotification');
    }
    
    /**
	 * Function to update simple offer's status
	 * @input  : id , status
	 * @output : void
	 * @access : public
	 */
	public function update_offer_status($id='',$status='') {
		$data['targetpush_id'] = decode($id);
		$status = decode($status);
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$id = $this->target_model->add_targetpush($data);
		Template::set_message(lang('message_status_targetpush'),'success');
		redirect('admin/settings/targetpush/targetnotification');
	}
	
    
    /**
     * Function to Product Direct Filter
     * @input  : product_id
     * @output : string
     * @access : public
     */
    public function search_product_direct() {
        $post_data = $this->input->post();
        $product_direct_search = trim($this->input->post('product_direct_search'));
        $productType = trim($this->input->post('productType'));
        $is_result = false;
        $member_data = lang('no_result_found');
        if(!empty($product_direct_search)){
            $data['productListing'] = $this->target_model->product_direct_search($product_direct_search,$productType);
            if(!empty($data['productListing'])){
                $responseArray = array();
                $i = 0;
                foreach($data['productListing'] as $key => $val){
                    $Prod_Desc = $val['Prod_Desc'];
                    $Prod_Desc = trim($Prod_Desc);
                    $Prod_Desc = utf8_encode($Prod_Desc);
                    $responseArray[$i]['Prod_Desc'] =  $Prod_Desc;
                    $Prod_Number = $val['Prod_Number'];
                    $Prod_Number = trim($Prod_Number);
                    $Prod_Number = utf8_encode($Prod_Number);
                    $responseArray[$i]['Prod_Number'] =  $Prod_Number;
                    $i++;
                }                
                   $data['productListing'] = $responseArray; 
                   $member_data = $this->load->view('product_listing_view',$data,true);
                   $is_result = true;
            }else{
                 $is_result = true;
                $member_data = $this->load->view('product_listing_view',$data,true);
            }            
        }
		echo json_encode(array('html'=>$member_data,'is_result'=>$is_result));die;
    }

    public function notificationAlert() {
       	Template::set_view('notification_alert_listing');
		Template::render();
    }
    
    public function send_notification_alert() {
        $data['categories'] = $this->target_model->get_category_list();
        Template::set('data',$data);
        Template::set_view('send_notification_alert');
        Template::render();	
    }

}//end Settings

/* Location: ./application/core_modules/targetpush/controllers/targetpush.php */
