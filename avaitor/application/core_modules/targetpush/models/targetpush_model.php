<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Competition Model
 *
 * The central way to access and perform CRUD on Competition.
 *
 * @package    Avaitor
 * @subpackage Modules_Competition
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Targetpush_model extends BF_Model
{

	function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
		$this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
    
		$this->ava_targetpush_notificaiton = 'ava_targetpush_notificaiton';
        $this->categories_table = 'categories';
        // define tables
	}
    
    /**
	 * Get listing of offers basis of type
	 * @input : offer_id , offer_type
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_offer_listing($targetpush_id = 0,$offer_type=2,$is_default=0) { 
		$this -> read_db -> select('ava_targetpush_notificaiton.*');
		if (!empty($targetpush_id)) {
			$this -> read_db -> where("ava_targetpush_notificaiton.targetpush_id", $targetpush_id);
			$result = $this -> read_db -> get('ava_targetpush_notificaiton');
			return $result -> result_array();
		}
	
		if($is_default == 1) {
			$this -> read_db -> where(array('ava_targetpush_notificaiton.is_delete'=>'0','ava_targetpush_notificaiton.is_default'=>$is_default));   // status=>1 : active and is_delete=>0 : data is not delete
		} else {
			$this -> read_db -> where(array('ava_targetpush_notificaiton.is_delete'=>'0','ava_targetpush_notificaiton.is_default'=>'0','ava_targetpush_notificaiton.is_simple_offer'=>$offer_type));   // status=>1 : active and is_delete=>0 : data is not delete
		}
	
		$this -> read_db -> order_by('ava_targetpush_notificaiton.targetpush_id', 'desc');
		$result = $this -> read_db -> get('ava_targetpush_notificaiton');
		return $result -> result_array();
	}
	
    
    /* Function user to Search Memver from filters */
    function member_search_filter_old($start_date = '',$end_date = '',$start_time='',$end_time='',$basketEG='',$basketEL='',$product_ids = array()){
            $whereCondition = "ATRX_TYPE = 'ITEMSALE' ";
            // Purchase date range
            if(!empty($start_date) && !empty($end_date)){
                // AND ATRX_TIMESTAMP) >= '".$start_time."' AND ATRX_TIMESTAMP <= '".$end_time."'))";
                $whereCondition .= "  AND (ATRX_TIMESTAMP >= '".$start_date."' AND ATRX_TIMESTAMP <= '".$end_date."') ";
            }
            // Basket size filter
            $havingClause = '';
            if(!empty($basketEG) || !empty($basketEL)){
                $havingClause .= " HAVING SUM(ATRX_QTY) >= ".$basketEG."";
               if(!empty($basketEL)){
                     $havingClause .= " AND SUM(ATRX_QTY) <= ".$basketEL."";
                } 
            }
            // Filter with respect to group // ?
            $whereIn = '';
            if(!empty($product_ids)){
                $productIdList = implode("', '",$product_ids);
                $whereIn .= "AND ATRX_PRODUCT IN ('".$productIdList."') ";
            }
            
            $result= $this->mssql_db->query("SELECT SUM(ATRX_QTY) as purchase_count, ATRX_ACCOUNT as member_id,ATRX_TIMESTAMP as last_order_date  FROM ACCOUNT_TRXTBL  WHERE  ".$whereCondition." ".$whereIn."  GROUP BY ATRX_ACCOUNT,ATRX_TIMESTAMP ".$havingClause. " ORDER BY ATRX_TIMESTAMP DESC ");
            //return $result->result_array();
            /* To get single result with respect to member */
            $memberList = $result->result_array();
            $finalMemberList = array();
            $memberIdA = array();
            foreach($memberList as $memberAVal){
                if(!in_array($memberAVal['member_id'],$memberIdA)){
                $finalMemberList[] = $memberAVal;
                $memberIdA[] = $memberAVal['member_id'];
                }
            }
            return $finalMemberList;
    }
    

    /* Function user to Search Memver from filters */
    function member_search_filter($start_date = '',$end_date = '',$start_time='',$end_time='',$basketEG='',$basketEL='',$product_ids = array(),$productcountEG='',$productcountEL=''){
            $whereCondition = "JNLD_TYPE = 'SALE' ";
            // Purchase date range
            if(!empty($start_date) && !empty($end_date)){
                $whereCondition .= "  AND (JNLD_YYYYMMDD >= ".$start_date." AND JNLD_YYYYMMDD <= ".$end_date.") ";
            }
            // Purchase time range
            if(!empty($start_time) && !empty($end_time)){
                $whereCondition .= "  AND (JNLD_HHMMSS >= ".$start_time." AND JNLD_HHMMSS <= ".$end_time.") ";
            }
            
            // Basket size filter , Amount Filter
            $havingClause = '';
            if(!empty($basketEG) || !empty($basketEL) || !empty($productcountEG) || !empty($productcountEL)){
                   $havingClause .= " HAVING ( ";
                   if(!empty($basketEG) || !empty($basketEL)) {
                       $havingClause .=  "( SUM(JNLD_AMT) >= ".$basketEG."";
                       if(!empty($basketEL)){
                             $havingClause .= " AND SUM(JNLD_AMT) <= ".$basketEL."";
                        }
                        $havingClause .= " ) ";
                   } 
                   if(!empty($productcountEG) || !empty($productcountEL)) {
                        if(!empty($basketEG) || !empty($basketEL)) {
                       $havingClause .=  " AND "; 
                        }     
                       $havingClause .=  " ( SUM(JNLD_QTY) >= ".$productcountEG."";
                       if(!empty($productcountEL)){
                             $havingClause .= " AND SUM(JNLD_QTY) <= ".$productcountEL."";
                        }
                        $havingClause .= " ) "; 
                   } 
                   $havingClause .= ")";
            }
            // Filter with respect to group // ?
            
            $whereIn = '';
            
            if(!empty($product_ids)){
				$product_ids = array_filter($product_ids);
                $productIdList = implode("', '",$product_ids);
                $whereIn .= "AND JNLD_PRODUCT IN ('".$productIdList."') ";
            }
            $result= $this->mssql_db->query("SELECT SUM(JNLD_QTY) as product_count,SUM(JNLD_AMT) as purchase_count,JNLD_TRX_NO,JNLD_YYYYMMDD as last_order_date,JNLD_HHMMSS as last_order_time   FROM JNLDTBL  WHERE  ".$whereCondition." ".$whereIn." AND JNLD_STATUS = 1  GROUP BY JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS ".$havingClause." ORDER BY JNLD_TRX_NO DESC ");
            /* To get single result with respect to member */
            $orderList = $result->result_array();
            $memberList = array();
            foreach($orderList as $orderKey => $orderVal){
                $TRX_NO = $orderVal['JNLD_TRX_NO'];
                $last_order_date = $orderVal['last_order_date'];
                $member_query= $this->mssql_db->query("select top 1 JNLD_PRODUCT from JNLDTBL WHERE JNLD_YYYYMMDD = ".$last_order_date." AND  JNLD_TRX_NO=".$TRX_NO." AND JNLD_TYPE = 'MEMBER'");
                /* To get single result with respect to member */
                $memberListRow = $member_query->result_array();
                if(!empty($memberListRow)){
                    $memberList[$orderKey] = $orderVal;
                    $memberList[$orderKey]['member_id'] = $memberListRow[0]['JNLD_PRODUCT'];   
                }
            }
            
            $finalMemberList = array();
            $memberIdA = array();
            foreach($memberList as $memberAVal){
                if(!in_array($memberAVal['member_id'],$memberIdA)){
                $finalMemberList[] = $memberAVal;
                $memberIdA[] = $memberAVal['member_id'];
                }
            }
            return $finalMemberList;
    }
    
    
    
    
    /**
	 * Function to get product details from product id
	 * @input : product_id
	 * @output: obj array
	 * @auther: Tosif Qureshi
	 */
	public function get_product_data($product_id) {
		$product_result = $this->mssql_db->query("select Prod_Desc, Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT where OUTP_STATUS = 'active' and Prod_Number = $product_id");
		return $product_result -> row();
	}
    
    
    
    
    
    
    /* Function user to Search Memver from filters */
    function member_direct_search($member_id = ''){
        
            $finalMemberList = array();
            $memberList = array();
            if($member_id){
                $member_id = trim($member_id);
                if(is_numeric($member_id)){
                    $member_result = $this->read_db->query("select acc_number,firstname,lastname from ava_users where ( acc_number = $member_id) AND acc_number != '' ORDER BY acc_number = $member_id DESC");
                }else{
                    $member_result = $this->read_db->query("select acc_number,firstname,lastname from ava_users where ( firstname LIKE '%$member_id%' OR lastname LIKE '%$member_id%' ) AND acc_number != '' ORDER BY acc_number DESC");
                }
                
                $memberRow = $member_result->result_array();
                if(!empty($memberRow)){
                   $i = 0;
                   foreach($memberRow as $memberData){                        
                        $memberList[$i]['member_id'] = $memberData['acc_number'];   
                        $memberList[$i]['member_name'] = $memberData['firstname'].' '.$memberData['lastname'];   
                        $memberList[$i]['purchase_count'] = '';   
                        $memberList[$i]['last_order_date'] = '';   
                        $memberList[$i]['last_order_time'] = '';   
                        $memberList[$i]['JNLD_TRX_NO'] = '';       
                   $i++;
                   }
                }
                $finalMemberList = $memberList;
            }
            return $finalMemberList;       
    }
    
    
    
    /**
	 * Get barcode data
	 * @access: public
	 * 
	 */
	public function get_barcode_data($barcode=0,$is_simple_offer=2,$id) {
		
		$this -> read_db -> select('targetpush_id');
		$this -> read_db -> where('barcode',$barcode); 
		$this -> read_db -> where('is_simple_offer',$is_simple_offer); 
		$this -> read_db -> where_not_in('targetpush_id',$id);
		$result = $this -> read_db -> get('ava_targetpush_notificaiton');
		return $result->num_rows();
	}
    
    
    /**
	 * Function to generate barcode from zend library
	 * @input  : barcode
	 * @output : string
	 * @access : public
	 */
	private function generate_zend_barcode($barcode='')
	{
	
		// load zend library
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		// prepare barcode from zend library
		$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
		// set barcode image path
		$dir_path = IMAGEUPLOADPATH.'barcodes/';
		// set barcode image name
		//$image_name = time().$barcode;
		$image_name = $barcode.'.png';
		// create image from barcode
		ImagePNG($file, $dir_path.$image_name);
		ImageDestroy($file);
		return $image_name;
		
	}
    
    function add_targetpush($data) {    //function is used in both case edit and update
		if($data['targetpush_id']) {											// edit data w.r.t. to beacon_offer_id		
			$this->db->where('targetpush_id', $data['targetpush_id']);
			$this->db->update('ava_targetpush_notificaiton', $data);
			return $data['targetpush_id'];
		} else {														// add data
			$this->db->insert('ava_targetpush_notificaiton', $data);
			return $this->db->insert_id();
		}
	}
    
       /* Function user to Search Memver from filters */
    function product_direct_search($product_direct_search = '',$productType=''){
        if($productType == 'id'){
            $product_result = $this->mssql_db->query("select Prod_Desc,Prod_Number from ProdTbl where (Prod_Number = $product_direct_search)");
        }else{
            $product_direct_search = str_replace("'","''",$product_direct_search);
            $product_result = $this->mssql_db->query("select Prod_Desc,Prod_Number from ProdTbl where ( Prod_Desc LIKE '%$product_direct_search%' )");
        }   
        return $product_result->result_array();
    }
    
    	/**
	 * Get all category listing
	 * @access: public
	 * 
	 */
	public function get_category_list() {
		
		$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> where('is_deleted',0);
		$result = $this -> read_db -> get($this->categories_table);
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Category';
			foreach($result -> result_array() as $row){
				$return[$row['category_id']] = $row['category_name'];			
			}
		}
		return $return;
	}
	

	
}//end User_model
