<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"> 
					<?php echo $offer_title;?>
				</h4>	
				 <ul class="nav nav-tabs customtab2" role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#filter_tab_link" role="tab"> <span class=""><?php echo lang('Filters');?></span></a> </li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
					<?php echo form_open('admin/settings/targetpush/create_target_notification','id="filter_member", class="form-material form-with-label"'); ?>
						<div class="tab-pane active py-5" id="filter_tab_link" role="tabpanel">
							<div class="form-group">
								<input type="checkbox" name="is_datesearch" value="1" id="is_datesearch" class="filled-in chk-col-light-blue" checked />
								<label for="is_datesearch"><?php echo lang('purchasedaterangesearch');?></label>
								<div class="row form-material col-md-6" id="daterange">
									<div class="col-md-6">
										<label class="m-t-20"><?php echo lang('purchase_date_range');?></label>
										<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date">
						
										<label class="m-t-40"><?php echo lang('purchase_time_range');?></label>
										<input type="text" name="start_time" class="form-control required" value="<?php echo !empty($start_time) ? $start_time : '';?>" id="start_time">
									</div>
									<div class="col-md-6">
										<label class="m-t-20">&nbsp;</label>
										<input type="text" name="end_date" class="form-control required"  value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date">
										<label class="m-t-40">&nbsp;</label>
										<input type="text" name="end_time" class="form-control required"  value="<?php echo !empty($end_time) ? $end_time : '';?>" id="end_time">
									</div>
								</div>
							</div>	
							
							<div class="form-group">
								<label><?php echo lang('basketsizerange');?></label>
								<div id="basketShowHide" class="form-group ">
									<input type="checkbox" name="is_basketsize_eg" value="1" id="is_basketsize_eg" class="filled-in chk-col-light-blue" />
									<label for="is_basketsize_eg"> <?php echo lang('equalorgreater');?></label>
									<div id="basketShowHide_eg">
									  <?php
										$basketEG = 0;
										$data	= array('name'=>'basketEG','value'=>set_value('basketEG', $basketEG),'id'=>'basketEG', 'class'=>'form-control form-control-line required numeric','style'=>"margin-top:5px");
										echo form_input($data);?>
									</div>
								</div>
								<div id="basketShowHide" class="form-group ">
									<input type="checkbox" name="is_basketsize_eg" value="1" id="is_basketsize_el" class="filled-in chk-col-light-blue" />
									<label for="is_basketsize_el"><?php echo lang('equalorless');?></label>
									<div id="basketShowHide_el">
										<?php
										$basketEL = 0;
										$data	= array('name'=>'basketEL','value'=>set_value('basketEL', $basketEL),'id'=>'basketEL', 'class'=>'form-control form-control-line required numeric','style'=>"margin-top:5px");
										echo form_input($data);?>
									</div>
								</div>
							</div>		
			
							<div class="form-group">
								<label><?php echo lang('productcountrange');?></label>
								<div id="productcountShowHide" class="form-group ">
									<input type="checkbox" name="is_productcount_eg" value="1" id="is_productcount_eg" class="filled-in chk-col-light-blue" />
									<label for="is_productcount_eg"> <?php echo lang('equalorgreater');?></label>
									<div id="productcountShowHide_eg">
										<?php
										$productcountEG = 0;
										$data	= array('name'=>'productcountEG','value'=>set_value('productcountEG', $productcountEG),'id'=>'productcountEG', 'class'=>'form-control form-control-line required numeric','style'=>"margin-top:5px" );
										echo form_input($data);?>
									</div>
								</div>
								<div id="productcountShowHide" class="form-group ">
									<input type="checkbox" name="is_productcount_el" value="1" id="is_productcount_el" class="filled-in chk-col-light-blue" />
									<label for="is_productcount_el"><?php echo lang('equalorless');?></label>
									<div id="productcountShowHide_el">
										<?php
										$productcountEL = 0;
										$data	= array('name'=>'productcountEL','value'=>set_value('productcountEL', $productcountEL),'id'=>'productcountEL', 'class'=>'form-control form-control-line required numeric','style'=>"margin-top:5px");
										echo form_input($data);?>
									</div>
								</div>
							</div>		
							
							<div class="form-group">
								<input type="checkbox" name="is_product_group" value="1" id="is_product_group"  class="filled-in chk-col-light-blue" />
								<label for="is_product_group"><?php echo lang('product_group');?></label>
								<div id="productgropuShowHide">
									<div id="searchProductButton">
										<button type="button" id="imageresource" name="product_search"  class="col-sm-4 btn waves-effect waves-light btn-block btn-info"><?php echo lang('search_product');?></button>
									</div>
									<div class=" "  id="showproductDiv"> </div>
									<input type="hidden" id="product_ids_val" name="product_ids_val" value="">
									<input type="hidden" id="member_ids_val" name="member_ids_val" value="">
									<input type="hidden" id="member_name_val" name="member_name_val" value="">
								</div>
							</div>
							
							<div class="form-group">
								<input type="checkbox" name="is_membersearch" value="1" id="is_membersearch" class="filled-in chk-col-light-blue" />
								<label for="is_membersearch"> <?php echo lang('membersearch');?></label>
								<div id="membersearchShowHide" class="col-md-6">
									<?php
									$membersearch = '';
									$data = array('name'=>'membersearchId','value'=>set_value('membersearchId', $membersearch),'id'=>'membersearchId','placeholder'=>'Member Id or Member Name','class'=>'form-control form-control-line required');
									echo form_input($data);?>
							
									<div id="memberSearchButton">
										<button type="button" name="member_search" class="col-sm-4 my-3 btn waves-effect waves-light btn-block btn-info member_search_Action"><?php echo lang('memberdirectsearch');?></button>
									</div>	 
								</div>	
							</div>
							
							<div class="form-group text-right">
								<button type="button" name="filter" class="btn btn-primary filterAction"><?php echo lang('show_member_filtered');?></button>
							</div>
							<hr>
							<h3 id="selectedMember"> Selected member </h3>
							   <div class="mt10 "  id="showmemberDiv"></div>
							<br>
						</div>   
						
						<div class="tab-pane active" id="member_listing"></div>
						
						<div class="row pb-3 float-right">
							<div class="col-lg-6 col-md-4">
								<button type="submit" name="save" id="AddTargetNotification" class="submit_form add_target_notification btn waves-effect waves-light btn-block btn-info"><?php echo lang('add_target_notification');?></button>
							</div>
							<div class="col-lg-6 col-md-4">
								<a href='<?php echo base_url().'admin/settings/targetpush/targetnotification' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					<?php echo form_close();?>	
				</div>   
				
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Product Search</h4>
							</div>
							<div class="modal-body">
							  <div class="tab-pane" id="product_tab">
								<button onclick="product_search()" class="btn default add-product-btn fr"  type="button product_direct_search_button" style="margin-top:0px">Product Search</button>		
								<input type="text" placeholder="Product Id / Product Name" name="product_direct_search" id="product_direct_search" style="width:200px;margin-right:10px" class="fr" >
								<select id="productType" name="productType" style="width:100px;margin-right:10px"  class="fr">
									<option value="id">Number</option>
									<option value="name">Name </option>
								</select>
								<div class="clearboth"></div>
								<div id="error_message" style="font-size:11px;color:red"></div> 
								<!-- Product list / form  start here -->
								<div class="clearboth product-grid-form" id="appendProductTable">
									<table class="table table-bordered" id="product_tables"> 
										<thead class="bg_ccc">
											<tr>
												<th class="width:20px"><input type="checkbox" name="all_product_check" value="1" id="all_product_check"  ></th>
												<th class='product_table_th'><?php echo lang('product_number');?></th>
												<th class='product_table_th'><?php echo lang('product_name');?></th>
												<th class='width45'></th>
											</tr>
										</thead>
										<?php
										$qty_row = 1;?>
									</table>
									<input type="hidden" value="<?php echo $qty_row;?>" name='last_qty_row' id='last_qty_row'>
									<!-- Product fields end here -->
								</div>	
							</div>    
			   
							</div>
							<div class="modal-footer">
								<button onclick="add_into_filter()" class="btn default add-on-filter"  type="button">Add Into Filter</button>		
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
			</div>	 
		</div>	 
	</div>	 
</div>

<script type="text/javascript">
	// Initialize material Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});

	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});
	
	$('#start_time').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		date: false,
		format : 'H:m:s'
	});

	$('#end_time').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		date: false,
		format : 'H:m:s'
	});
	
	/**
	 *  display offer image in popup
	 */
	$(document).delegate( "#imageresource", "click", function(e) {
		$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
	});


	var productIDArray = new Array();
	var productNAMEArray = new Array();


$(document).ready(function(){
        $("#filter_member").validate();
    
    $(document).on('click','#AddTargetNotification',function(){
        /*
        var memberChecked = new Array();    
        $( ".allMemberChecked:checkbox:checked" ).each(function( index ) {
            memberChecked.push($(this).val());
        });
        if(memberChecked.length > 0){
            $('#filter_member').submit();
        }
        */
        var member_ids_val = $('#member_ids_val').val();
        if(member_ids_val != ""){
            $('#filter_member').submit();
        }else{
        	bootbox.alert("Please add member first using Add Member button");
        }
        
    });    
    
    $(document).on('click','.check-all',function(){
        $(".allMemberChecked").prop('checked', $(this).prop("checked"));
    });
    
    
    /* To hide member filter */
    $('#member_listing').hide();// Hide member listing 
    $('.add_target_notification').hide();// Hide targetNotification button
    $('#membersearchShowHide').hide(); // Hide Basket dropdown
    $('#productgropuShowHide').hide(); // Hide Product Group
    $('#selectedMember').hide(); // Hide Product Group
    $('#productcountShowHide_eg').hide(); // Hide Product Group
    $('#productcountShowHide_el').hide(); // Hide Product Group
    $('#basketShowHide_eg').hide(); // Hide Product Group
    $('#basketShowHide_el').hide(); // Hide Product Group
    $('#product_tab_link').hide(); // Hide Product Groups
});

    $(document).on('click','.filterAction',function(){
        $('#member_listing').show();
         $('#loader1').fadeIn();
        var fromData = $("#filter_member").serialize();
        $.ajax ({
				type: "POST",
				dataType:'jSon',
				data : fromData,
				url: "<?php echo base_url() ?>admin/settings/targetpush/filter_members_action",
				success: function(data){
                    if(data.is_result == true){
                    $('#member_listing').html(data.html);
                    $('.add_target_notification').show();
                    }
                    $('#loader1').fadeOut(); 
				}
			});
         
    });  
    
	/* Hide and Show basket Size */
    $(document).on('click','#is_basketsize_el',function(){
        if($(this).is(':checked')){
            $('#basketShowHide_el').show();
        }else{
            $('#basketShowHide_el').hide();
              $('#basketEL').val(0);
        } 
    });  
    $(document).on('click','#is_basketsize_eg',function(){
        if($(this).is(':checked')){
            $('#basketShowHide_eg').show();
        }else{
            $('#basketShowHide_eg').hide();
            $('#basketEG').val(0);
        } 
    });


/* Hide and Show Product Count */     
    
    $(document).on('click','#is_productcount',function(){
        if($(this).is(':checked')){
            $('#productcountShowHide').show();
        }else{
            $('#productcountShowHide').hide();
        } 
    }); 
    

/* Hide and Show Product Count */     
    
    $(document).on('click','#is_productcount_eg',function(){
        if($(this).is(':checked')){
            $('#productcountShowHide_eg').show();
        }else{
            $('#productcountShowHide_eg').hide();
             $('#productcountEG').val(0);
        } 
    });  
         
    
/* Hide and Show Product Count */     
    
    $(document).on('click','#is_productcount_el',function(){
        if($(this).is(':checked')){
            $('#productcountShowHide_el').show();
        }else{
            $('#productcountShowHide_el').hide();
            $('#productcountEL').val(0);
        } 
    });
    
    /* Hide and Show Product Count */     
    
    $(document).on('click','#is_membersearch',function(){
        if($(this).is(':checked')){
            $('#membersearchShowHide').show();
        }else{
            $('#membersearchShowHide').hide();
           
        } 
    });
    
	/* Hide and Show Purchase date range */     
    
    $(document).on('click','#is_datesearch',function(){
        if($(this).is(':checked')){
            $('#daterange').show();
        }else{
            $('#daterange').hide();
        } 
    });  
        
        
    
    

	/* Hide and Show Product Group */     
    
    $(document).on('click','#is_product_group',function(){
        if($(this).is(':checked')){
            $('#productgropuShowHide').show();
        }else{
            $('#productgropuShowHide').hide();
             $('#product_tab_link').hide(); // Hide Product Groups
             $('#filter_tab_link').show(); // Hide Product Groups
        } 
    });  
    
	/* Hide and Show Product Button */     
    
     
    $(document).on('click','.productSearchAction',function(){
         $('#filter_tab_link').hide(); // Hide Product Groups
         //$('#product_tab_link').show();
         $('#product_tab_link').trigger('click'); // Hide Product Group
    });
    
     
    $(document).on('click','.filterSearchAction',function(){
         $('#filter_tab_link').show(); // Hide Product Groups
        // $('#product_tab_link').hide();
         $('#filter_tab_link').trigger('click'); // Hide Product Group
    });
    
    /* 
	| -----------------------------------------------------
	| Manage add quantity row 
	| -----------------------------------------------------
	*/ 
	function product_search() {
		// get last product row number
		var product_direct_search = $('#product_direct_search').val();
		var productType = $('#productType').val();
        
        if(product_direct_search == ""){
                //bootbox.alert("Empty search is not valid");
                  $('#error_message').show();
                $('#error_message').html("Empty search is not valid");
                $('#error_message').hide(4000);
                $('#product_direct_search').val('');
                
                return false;
                
        }
        
        if(productType == 'id'){
            if (isNaN(product_direct_search)) {
                //bootbox.alert("Invalid product number");
                $('#error_message').show();
                $('#error_message').html("Invalid product number");
                $('#error_message').hide(4000);
                $('#product_direct_search').val('');
                return false;
                
            } else {
                //alert('Input OK');
            }
        }
        
        $('#loader1').fadeIn();	
		$.ajax ({
			type: "POST",
			dataType:'jSon',
			data : {product_direct_search:product_direct_search,productType:productType},
			url: "<?php echo base_url() ?>admin/settings/targetpush/search_product_direct",
			success: function(data){
				$('#appendProductTable').html(data.html);
				$('#loader1').fadeOut();
			}
		});                
	}
    
    /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".romove_row","click",function(e){
		var row_id = $(this).attr('row_id');
		var product_img_id = $(this).attr('product_img_id');
		var product_image = $(this).attr('product_image');
		
		if(product_img_id != undefined && product_img_id != '') {
			bootbox.confirm("Are you sure want to delete this?", function(result) {
				if(result==true) {
                    $('#qty_row_'+row_id).html('');
     			}
			});
		} else {
			// hide the selected row
			$('#qty_row_'+row_id).html('');
		}
	});

	
    
      /* 
	| -----------------------------------------------------
	| Manage add filter product row 
	| -----------------------------------------------------
	*/ 
	function add_into_filter() {
		// get last product row number
		var last_row = $('#last_qty_row').val();
        var row;
        var product_id;
        var product_ids_val = '';
        var product_name;
        var product_qty;
        var html = '';
        var j = productIDArray.length;
        $('#loader1').fadeIn();	
        
        $( ".single_product_check:checkbox:checked" ).each(function( index ) {
            row = parseInt($(this).attr('qty_row'));
            product_name = $(this).attr('product_name_'+row);
            product_qty = $(this).attr('product_qty_'+row);
            product_id = parseInt($(this).val());
            if(jQuery.inArray(product_id,productIDArray) < 0){
                productIDArray[j] = product_id;
                productNAMEArray[product_id] = product_name;
                j++;
            }
            
        });
        
        
        for(var i=0;i<productIDArray.length;i++){
            product_ids_val += productIDArray[i]+',';
            html += '<span class="chosen-choices fl ml5">&nbsp;<span>'+productNAMEArray[productIDArray[i]]+'</span>&nbsp;&nbsp;<span class="search-choice-close crp" qty_row="'+i+'" ><strong> X </strong></span>&nbsp;</span>';
        }
        
        if(productIDArray != ""){
            $('#product_ids_val').val(product_ids_val);
        }
        
        if(html != ""){
             $('#showproductDiv').html(html);  
             $('#filter_tab_link').show(); // Hide Product Groups
          //   $('#product_tab_link').hide();
            $('#filter_tab_link').trigger('click'); // Hide Product Group
            $('#imagemodal').modal('hide');
            	
        }
        
        $('#loader1').fadeOut();
        
	}
    
    /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".search-choice-close","click",function(e){
		alert(41234);
		return false;
		$(this).parent('span').remove();
		
		var qty_row = $(this).attr('qty_row');
        
         var product_ids_val = '';
        for(var i=0;i<productIDArray.length;i++){
            if(qty_row != productIDArray[i]){
                product_ids_val += productIDArray[i]+',';
            }
        }
        if(product_ids_val != ""){
            $('#product_ids_val').val(product_ids_val);
        }
        
        //alert(qty_row);
	});


    /* Get Product Data */
    
    /* 
	| -----------------------------------------------------
	| Function used to remove image
	| -----------------------------------------------------
	*/
	$(document).delegate('.product_input','blur',function(e){
		var product_id = $(this).val();
		var row_id = $(this).attr('row_id');
		var exist_id = $(this).attr('exist_id');
		get_product_data(product_id,row_id,exist_id);
	});
	
	function get_product_data(product_id,row_id,exist_id) {
		var product_ids = new Array();
        var currentRowId = '';
        var j =0;
        $.each( $( ".product_input" ), function() {
            currentRowId = $(this).attr('row_id');
            if(row_id != currentRowId){
                product_ids[j] = $(this).val();
            j++;
            }
        });
        
        
		// check existing product id
		if(exist_id == product_id) {
			return false;
		} else if(exist_id != '' && exist_id != product_id) {
			bootbox.alert("Product already associated with this competition.");
			$('#product_id_'+row_id).val(exist_id);
			return false;
		} 
		
		if ($.inArray(product_id, product_ids) > -1) {
			bootbox.alert("Product already associated with this competition.");
			$('#qty_row_'+row_id).html('');
			return false;
		} else {
			$.ajax ({
				type: "POST",
				dataType:'jSon',
				data : {product_id:product_id},
				url: "<?php echo base_url() ?>admin/settings/targetpush/getproductdata",
				success: function(data){
					if(data.product_name != '') {
						$('#product_name_'+row_id).val(data.product_name);
					} else {
						$('#product_name_'+row_id).val('');
						$('#product_id_'+row_id).val('');
					}
				}
			});
		}
	}
    
    
    
    $(document).on('click','.member_search_Action',function(){
        $('#member_listing').show();
       
        var member_id = $("#membersearchId").val();
        var fromData = $("#filter_member").serialize();
        $('#loader1').fadeIn();
        if(member_id != ""){
            $.ajax ({
				type: "POST",
				dataType:'jSon',
				data : fromData,
				url: "<?php echo base_url() ?>admin/settings/targetpush/member_direct_search",
				success: function(data){
                    if(data.is_result == true)
                    $('#member_listing').html(data.html);
                     $('.add_target_notification').show();
                        $('#loader1').fadeOut();
				}
			});
        }else{
            bootbox.alert("Please insert Member Id or Member Name");
			$('#membersearch').val('');
            $('#loader1').fadeOut();
        }
       
        
    }); 
    
    
	/* Add Member  */
   
var memberIDArray = new Array();
var memberNAMEArray = new Array();
var member_ids_val = '';
 
$(document).on('click','#memberAddtoBox',function(){
var member_ids_val = '';
var member_name_val = '';

        var html = '';
        var j = memberIDArray.length;
        $( ".allMemberChecked:checkbox:checked" ).each(function( index ) {
            memberId = parseInt($(this).val());
            member_name = $(this).attr('member_name');
            
            if(jQuery.inArray(memberId,memberIDArray) < 0){
                memberIDArray[j] = memberId;
                memberNAMEArray[memberId] = member_name;
                j++;
            }
            
        });
        
        
        for(var i=0;i<memberIDArray.length;i++){
            if(memberIDArray[i] != ""){
                member_ids_val += memberIDArray[i]+',';
                member_name_val += memberNAMEArray[memberIDArray[i]]+'-BREKAER-';
                html += '<span class="chosen-choices fl ml5">&nbsp;<span>'+memberNAMEArray[memberIDArray[i]]+'</span>&nbsp;&nbsp;<span class="member-search-choice-close crp" qty_row="'+memberIDArray[i]+'" ><strong> X </strong></span>&nbsp;</span>';
            }            
        }
        
        if(member_ids_val != ""){
            $('#member_ids_val').val(member_ids_val);
            $('#member_name_val').val(member_name_val);
        }
        
        if(html != ""){
             $('#selectedMember').show();
             $('#showmemberDiv').html(html);  
        }
        
      
   });  
   
   
     /* 
	| -----------------------------------------------------
	| Remove quantity row
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".member-search-choice-close","click",function(e){
		$(this).parent('span').remove();
		var qty_row = $(this).attr('qty_row');
        var member_ids_val = '';
        var member_name_val = '';
        for(var i=0;i<memberIDArray.length;i++){
            if(qty_row != memberIDArray[i]){
                member_ids_val += memberIDArray[i]+',';
                member_name_val += memberNAMEArray[memberIDArray[i]]+'-BREKAER-';
            }else{
                memberIDArray[i] = '';
                memberNAMEArray[memberIDArray[i]] = '';
            }
        }
        if(member_ids_val != ""){
            $('#member_ids_val').val(member_ids_val);
            $('#member_name_val').val(member_name_val);
        }
        if(memberIDArray.length == 0){
          $('#selectedMember').hide();
        }
        
	});
     

</script>
