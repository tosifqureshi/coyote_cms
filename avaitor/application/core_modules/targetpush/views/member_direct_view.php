<table id="pages_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">
                    <thead>
                    <tr role="row">
                        <th><input class="check-all" name="check-all" value="1"  type="checkbox" /></th>
                        <th><?php echo lang('member_no'); ?></th>
                        <th><?php echo lang('member_name'); ?></th>
                    </tr>
                </thead>
            
            <tbody role="alert" aria-live="polite" aria-relevant="all">
   
<?php
    if(!empty($memberListing)){
         $i=0;
        if(isset($memberListing) && is_array($memberListing) && count($memberListing)>0):
        foreach($memberListing as $member) : 
        // set offer id
        if(!empty($member['member_id'])){
        $member_id = $member['member_id'];
        $member_name = $member['member_name'];
        ?>
            <tr>
            <td><input type="checkbox" value="<?php echo $member_id; ?>" member_name="<?php echo $member_name;?>" name="allMemberChecked[]" class="allMemberChecked" /></td>
            <td>
                <?php echo $member_id;?>
            </td>
            <td>
                <?php echo $member_name;?>
            </td>
            </tr>
          <?php 
        $i++;
        }
        endforeach ; endif;
    
    if($i == 0){
    ?>
    <tr role="row">
        <th colspan="5" > No Member Record Available.</th>
    </tr>
    <?php
    }
    
    ?>
<script>
jQuery('#pages_table').dataTable({
        "bJQueryUI": true,
        "aaSorting": [[ 0, "asc" ]],
        "aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 1 ] } ],
        "sPaginationType": "full_numbers"
});
</script>    
    <?php
    }else{ // If member listing data is null
    ?>    
     <tr role="row">
        <th colspan="5" > No Member Record Available.</th>
    </tr>
    <?php    
    }
?>    
    </tbody>
</table>   
 
<div style="clear:both"></div> 
<div class="mt10" >
    <button type="button" id="memberAddtoBox" name="memberAddtoBox" class="btn btn-primary" >Add Member</button>
</div>
              

