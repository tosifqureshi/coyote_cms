<table id="pages_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">
                    <thead>
                    <tr role="row">
                        <th><input class="check-all" name="check-all" value="1" type="checkbox" /></th>
                        <th><?php echo lang('member_no'); ?></th>
                        <th><?php echo lang('member_name'); ?></th>
                        <th><?php echo lang('last_order_date'); ?></th>
                        <th><?php echo lang('last_basket_size'); ?></th>
                    </tr>
                </thead>
            
            <tbody role="alert" aria-live="polite" aria-relevant="all">
   
<?php
    if(!empty($memberListing)){
         $i=1;
        if(isset($memberListing) && is_array($memberListing) && count($memberListing)>0):
        foreach($memberListing as $member) : 
        // set offer id
        $member_id = $member['member_id'];
        $purchase_count = $member['purchase_count'];
        $last_order_date = $member['last_order_date'];
        $last_order_date = $member['last_order_date'];
        $last_order_time = $member['last_order_time'];
        
        $last_datetime = substr($last_order_date,0,4).'-'.substr($last_order_date,4,2).'-'.substr($last_order_date,6,2).' '.substr($last_order_time,0,2).':'.substr($last_order_time,2,2).':'.substr($last_order_time,4,2);
        $last_datetime = date('d M Y',strtotime($last_datetime));
        $member_name = get_ava_user_name($member_id);
        if(!empty($member_name)){
        ?>
            <tr>
            <td><input type="checkbox" value="<?php echo $member_id; ?>" member_name="<?php echo get_user_name($member_id); ?>" name="allMemberChecked[]"  class="allMemberChecked" /></td>
            <td>
                <?php echo $member_id;?>
            </td>
            <td>
                <?php echo get_ava_user_name($member_id);?>
            </td>
            <td>
                <?php echo $last_datetime;?>
            </td>
            <td>
                <?php echo $purchase_count;?>
            </td>
            </tr>
        <?php 
        
        }
        $i++;
        endforeach ; endif;
    
    ?>
<script>
jQuery('#pages_table').dataTable({
        "bJQueryUI": true,
        "aaSorting": [[ 0, "asc" ]],
        "aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 1 ] } ],
        "sPaginationType": "full_numbers"
});
</script>    
    <?php
    }else{ // If member listing data is null
    ?>    
     <tr role="row">
        <th colspan="5" > No Member Record Available.</th>
    </tr>
    <?php    
    }
?>    
    </tbody>
</table>   
              
<div style="clear:both"></div> 
<div class="mt10" >
    <button type="button" id="memberAddtoBox" name="memberAddtoBox" class="btn btn-primary" >Add Member</button>
</div>
              

