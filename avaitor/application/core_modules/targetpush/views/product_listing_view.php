  <table class="table table-bordered" id="product_tables"> 
                                    <thead class="bg_ccc">
                                        <tr>
                                            <th class="width:20px"><input type="checkbox" name="all_product_check" value="1" id="all_product_check"  >
                                            <label for="all_product_check"></label></th>
                                            <th class='product_table_th'><?php echo lang('product_number');?></th>
                                            <th class='product_table_th'><?php echo lang('product_name');?></th>
                                           <!--<th class='width45'></th>-->
                                        </tr>
                                    </thead>
            <tbody >
                                        
<?php

    if(!empty($productListing)){
         $qty_row=1;
        if(isset($productListing) && count($productListing)>0):
        foreach($productListing as $product) : 
        // set offer id
        $product_id = $product['Prod_Number'];
        $Prod_Desc = $product['Prod_Desc'];        
        ?>
                  <tr class="product-half-side" id="qty_row_<?php echo $qty_row;?>">
                                                    <td  >
                                                       <input style="width:20px !important" qty_row="<?php echo $qty_row;?>" product_qty_<?php echo $qty_row;?> = "0" product_name_<?php echo $qty_row;?> = "<?php echo $Prod_Desc; ?>" type="checkbox" name="single_product_check" value="<?php echo $product_id; ?>" id="single_product_check" class="single_product_check"  > 
                                                       <label for="single_product_check"></label>                         
                                                    </td>
                                                    <td>
                                                        <?php echo $product_id; ?> 
                                                    </td>
                                                  
                                                    <td>
                                                        <?php echo $Prod_Desc; ?> 
                                                    </td>
                                                <!--<td>
                                                        <span class="romove_row" row_id=<?php echo $qty_row;?> product_img_id='' product_image=''><i class="fa fa-trash"></i></span>
                                                    </td>
                                                 -->   
                                                </tr>
                                         
        <?php 
        $qty_row++;
        endforeach ; endif;
    
    ?>
<script>
jQuery('#product_tables').dataTable({
        "bJQueryUI": true,
        "aaSorting": [[ 0, "asc" ]],
        "aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 1 ] } ],
        "sPaginationType": "full_numbers"
});

 $(document).on('click','#all_product_check',function(){
        $(".single_product_check").prop('checked', $(this).prop("checked"));
    });
    
    

</script>    
    <?php
    }else{ // If member listing data is null
    ?>    
     <tr role="row">
        <th colspan="3"> No Product Record Available.</th>
    </tr>
    <?php    
    }
?>    

    </tbody>	
</table>   

