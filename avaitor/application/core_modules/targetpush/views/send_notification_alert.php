<div class="page-header">
  	<h1>
        <i class="fa fa-plus-square-o"></i>
		Notification Alert
	</h1>
</div>

<?php echo form_open_multipart('backend/vip_offer/create/','id="add_offer_form"'); ?>
    <div class="tabbable">
        <div class="tab-content">
            <div class="tab-pane active" id="content_tab">
                <div class="form-wrapper clearboth">
                    <div class="form-box">
                        <label for="title">Offer Name / Subject<i style="color:red;">*</i>:</label>
                        <?php
                        $data	= array('name'=>'offer_name', 'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_name'));
                        echo form_input($data);
                        ?>
                    </div>
                    <div class=" full-wrap mb10">
                        <!--Start date-->
                        <div class="form-box half-wrap">
                            <label for="start_date"><?php echo lang('start_date');?><i style="color:red;">*</i>:</label>
                            <div class="half_date">
                                <div class="form-group input-append date" id="start_date_div">
                                    <?php
                                    $data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'id'=>'start_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_date'), 'readonly'=>'readonly', 'class'=>'required');
                                    echo form_input($data);
                                    ?>
                                    <span id="start_time_add_on" class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>
                                </div>
                            </div>
                        </div>
                        <!--End date-->
                        <div class="form-box half-wrap">
                            <label for="end_date"><?php echo lang('end_date');?><i style="color:red;">*</i>:</label>
                            <div class="half_date">
                                <div class="form-group input-append date" id="end_date_div">
                                    <?php
                                    $data	= array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'id'=>'end_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_end_date'), 'readonly'=>'readonly', 'class'=>'required');
                                    echo form_input($data);
                                    ?>
                                    <span id="start_time_add_on" class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>
                                </div>	
                            </div>	
                        </div>
                    </div>
                    
                    <div class="form-box every-time mb10">
                        <label for="title">Notification Frequency<i style="color:red;">*</i>:</label>
                        Every   <select>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                        Times In Every
                                <select>
                                    <option>Day</option>
                                    <option>Week</option>
                                    <option>Month</option>
                                </select>
                    </div>

                    <div class="form-box">
                        <label for="offer_long_description">Email Body<i style="color:red;">*</i>:</label>
                        <div class="control-group ">	
                            <?php
                            $data	= array('name'=>'offer_long_description', 'class'=>'mceEditor redactor span8', 'value'=>'');
                            echo form_textarea($data);
                            ?>
                        </div>
                    </div>		
                    <div class=" full-wrap mt5">
                        <label for="offer_long_description">Target Users<i style="color:red;">*</i>:</label>
                        <div class="form-box  mt10 pl20">
                            <label for="is_category">
                                <span class="fl mr5"><input type="checkbox" name="is_category" value="1" id="is_category" ></span>
                                <span class="fl mt1">On Product Purchase Category</span>
                            </label>
                        </div>
                            <div class="form-box pl20" id="category_div">
                                <label for="content"><?php echo lang('category');?><i style="color:red;">*</i></label>
                                <?php
                                $data	= array('name'=>'category_id','multiple'=>'multiple','id'=>'category_id', 'class'=>'required', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_category'));
                                echo form_dropdown($data,$categories);
                                ?>
                            </div>
                            <div class="clearfix"></div>
                        <div class="form-box half-wrap mt10 pl20">
                            <label for="is_purchase">
                                <span class="fl mr5"><input type="checkbox" name="is_purchase" value="1" id="is_purchase" ></span>
                                <span class="fl mt1">On Purchase Price Above</span>
                            </label>
                        </div>
                        <div class="form-box price-box pl20" id="product_price_div"> 
                            <div class="fl "><label for="title"><?php echo lang('offer_price');?><i style="color:red;">*</i>:</label>
                                <span class="dolor_img"><?php echo $this->config->item('currency_sign');?></span>
                                <?php
                                $data	= array('name'=>'offer_price' ,'id'=>'offer_price', 'class'=>'span8 numeric required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_price'));
                                echo form_input($data);
                                ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="text-right btn_wrapZ mt10">
                <button type="submit" name="save" class="btn btn-primary"><?php echo lang('form_save');?></button>
                &nbsp;
                <a href='<?php echo base_url().'backend/vip_offer/beacon' ?>'><button type="button" name="cancel" class="btn default"><?php echo lang('form_cancel');?></button></a>
            </div>
        </div>	
    </div>
</form>
<script>
    $(document).ready(function() {
        $("#category_div").hide();
        $("#product_price_div").hide();
        
        $('#is_category').change(function () {
            if($('input[name=is_category]').is(':checked')){
                $("#category_div").show();
            } else {
                $("#category_div").hide();                
            }
        });
        
        $('#is_purchase').change(function () {
            if($('input[name=is_purchase]').is(':checked')){
                $("#product_price_div").show();
            } else {
                $("#product_price_div").hide();                
            }
        });
    });
</script>
