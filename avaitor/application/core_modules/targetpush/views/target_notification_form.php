<div class="page-header">
	<h1>
		<?php 
		if(!empty($targetpush_id)) { ?>
			 <i class="fa fa-edit"></i>
		<?php 
		} else { ?>
			 <i class="fa fa-plus-square-o"></i>

		<?php } ?>
		 
		<?php echo $offer_title;?>
	</h1>
</div>

<?php echo form_open_multipart('admin/settings/targetpush/create_target_notification/'.$targetpush_id,'id="add_targetpush_form"'); ?>
<div class="tabbable">
	
	<div class="tab-content">
		<div class="tab-pane active" id="content_tab">
			
				<div class="form-wrapper clearboth">
                    <!-- member view -->
                    <div class="form-box">
                    <label for="title"><?php echo 'Member List';?><i style="color:red;">*</i></label>	
                    <?php
                    if(!empty($memberName)){
                    
                    
					 $memberNameA = explode('-BREKAER-',$memberName);  
                     foreach($memberNameA as $memberNameVal){
                        if(!empty($memberNameVal)){
                         echo '<span class="chosen-choices fl ml5">&nbsp;<span><b>'.$memberNameVal.'</b></span>,</span>';    
                        }
                     }  
					
                    
                    }else{
                    
					 $member_ids = str_ireplace('-','',$member_ids);
                     $member_idA = explode(',',$member_ids);  
                     foreach($member_idA as $memberVal){
                         if(!empty($memberVal)){
                            echo '<span class="chosen-choices fl ml5">&nbsp;<span><b>'.get_ava_user_name($memberVal).'</b></span>,</span>';    
                        }
                     }  
					    
                    }
                    ?>
                    </div>
            
            
            		<div class="form-box">
						<label for="title"><?php echo lang('offer_name');?><i style="color:red;">*</i></label>
						<input type="hidden" name="member_ids" id="member_ids" value="<?php echo $member_ids; ?>">
                        <?php
						$data	= array('name'=>'targetpush_name', 'value'=>set_value('targetpush_name',$targetpush_name), 'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_name'));
						echo form_input($data);
						?>
                        
					</div>
				
                 <div class="form-box half-wrap">
                        <label for="start_time"><?php echo lang('notification_start_date');?></label>
                        <div class="half_date">
                           	<div class="form-group input-append date" id="start_date_div">
                                <?php
                                $data	= array('name'=>'start_date', 'data-format'=>'dd-MM-yyyy', 'value'=>set_value('start_date', $start_date),'id'=>'start_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_date'), 'readonly'=>'readonly');
                                echo form_input($data);
                                ?>
                                <span id="start_time_add_on" class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>
                        </div>
                           
                        </div>
                    </div>	
                    
                    <div class="form-box half-wrap">
                        <label for="end_time"><?php echo lang('notification_end_date');?></label>
                        <div class="half_date">
                            <div class="form-group input-append date" id="end_date_div">
                                <?php
                                $data	= array('name'=>'end_date', 'data-format'=>'dd-MM-yyyy', 'value'=>set_value('end_date', $end_date),'id'=>'end_date','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_end_date'), 'readonly'=>'readonly');
                                echo form_input($data);
                                ?>
                                <span id="start_time_add_on" class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>
                            </div>	
                        </div>	
                    </div>
                    
                
					<div class="form-box price-box"> 
						<div class="fl half_w"><label for="title"><?php echo lang('offer_price');?></label>
							<span class="dolor_img"><?php echo $this->config->item('currency_sign');?></span>
							<?php
							$data	= array('name'=>'targetpush_price' ,'id'=>'targetpush_price', 'value'=>set_value('targetpush_price',$targetpush_price), 'class'=>'span8 numeric', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_price'));
							echo form_input($data);
							?>
						</div>
					</div>
					
					<div class="form-box">
					<div class="fl half_w">	
						<label for="barcode"><?php echo lang('offer_barcode');?></label>
						<?php
						$barcode_data	= array('name'=>'barcode', 'value'=>set_value('barcode',$barcode), 'class'=>'span8 numeric ','maxlength'=>12, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_barcode'));
						echo form_input($barcode_data);
						?>
					</div>
					<!--- QR code upload field starts here --> 
					<?php if(isset($barcode_image) && !empty($barcode_image)) { ?>
						<div class="  barcode_wrap fl ml10" style="float:left !important">
							<label for="title" class="img-title"><?php echo lang('qrcode_image');?></label> 
							<div class="bar-img" >
								<img id="qr-image-picker" style="width: 100%;" src="<?php echo base_url('uploads/barcodes/'.$barcode_image) ?>" data-toggle='tooltip', data-placement='right', title='<?php echo lang('tooltip_barcode_image'); ?>' />
							</div>
						</div>	
					<?php } ?>
					<!--- QR code upload field end here --> 
					</div>
                    <div class="form-box">
						<label for="offer_short_description"><?php echo lang('offer_short_description');?></label>
						<div class="control-group ">	
							<?php
							$data	= array('name'=>'targetpush_short_description', 'value'=>$targetpush_short_description,'rows'=>2,'cols'=>20);
							echo form_textarea($data);
							?>
						</div>	
					</div>
				
					<div class="form-box">
						<label for="offer_long_description"><?php echo lang('offer_long_description');?></label>
						<div class="control-group ">	
							<?php
							$data	= array('name'=>'targetpush_long_description', 'class'=>'mceEditor redactor span8', 'value'=>$targetpush_long_description);
							echo form_textarea($data);
							?>
						</div>
					</div>				
				</div>
			
			
			<div class="img-wrapper">
				<label for="title" class="img-title"><?php echo lang('offer_image');?></label> 
				<div class="img-box">
					<?php
					$targetpush_image_val_1 = (isset($targetpush_image_1) && !empty($targetpush_image_1)) ? $targetpush_image_1 :'';
					$img_1_display = (isset($targetpush_image_1) && !empty($targetpush_image_1)) ? '' :'dn';
					if(isset($targetpush_image_1) && $targetpush_image_1 != '') {
					$targetpush_image_1 = base_url('uploads/offer_images/'.$targetpush_image_val_1);
					$title = lang('tooltip_offer_image');                    
                     $targetpush_image_1_doc = $this->config->item('document_path').'uploads/offer_images/'.$targetpush_image_val_1; 
                     if(!file_exists($targetpush_image_1_doc)){
                        $targetpush_image_1 = base_url('uploads/No_Image_Available.png');
                        $title = "No Image Available";
                     }
                    
					} else {
						$title = lang('tooltip_offer_image');
						$targetpush_image_1 = base_url('uploads/upload.png');
					} ?>
					<a id="remove_btn_1" class="<?php echo $img_1_display;?>" href="javascript:void(0);" onclick="remove_image(1)">×</a>
					<div class="cell-img" ><img id="image-picker_1" src="<?php echo $targetpush_image_1; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
					
					<?php
						$data	= array('name'=>'targetpush_image_1', 'id'=>'targetpush_image_1', 'value'=>set_value('targetpush_image_1',$targetpush_image_val_1),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
						echo form_upload($data);
					?>
					<label for="offer_default_image"><?php echo lang('offer_default_image');?></label>
						<?php	
						$data	= array('name'=>'is_banner_image', 'value'=>'1','id'=>'is_banner_image_1','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image')); // fist image is alway the default image
						echo form_radio($data);
						?>
				</div>
                
				<div class="img-box">
					<?php
					$targetpush_image_val_2 = (isset($targetpush_image_2) && !empty($targetpush_image_2)) ? $targetpush_image_2 :'';
					$img_2_display = (isset($targetpush_image_2) && !empty($targetpush_image_2)) ? '' :'dn';
					if(isset($targetpush_image_2) && $targetpush_image_2 != '') {
					$targetpush_image_2 = base_url('uploads/offer_images/'.$targetpush_image_val_2);
					$title = lang('tooltip_offer_image');
                     $targetpush_image_2_doc = $this->config->item('document_path').'uploads/offer_images/'.$targetpush_image_val_2; 
                     if(!file_exists($targetpush_image_2_doc)){
                      	$targetpush_image_2 = base_url('uploads/No_Image_Available.png');
                        $title = "No Image Available";
                     }
					} else {
						$title = lang('tooltip_offer_image');
						$targetpush_image_2 = base_url('uploads/upload.png');
					} ?>
					<a id="remove_btn_2" class="<?php echo $img_2_display;?>" href="javascript:void(0);" onclick="remove_image(2)">×</a>
					<div class="cell-img" ><img id="image-picker_2" src="<?php echo $targetpush_image_2; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>					
					
					<?php
						$data	= array('name'=>'targetpush_image_2', 'id'=>'targetpush_image_2', 'value'=>set_value('targetpush_image_2',$targetpush_image_val_2),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
						echo form_upload($data);
					?>
					<label for="offer_default_image"><?php echo lang('offer_default_image');?></label>
						<?php
						if(isset($is_banner_image) && $is_banner_image == 2) { 
							$data	= array('name'=>'is_banner_image', 'value'=>'2','id'=>'is_banner_image_2','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_banner_image'));
						} else {
							$data	= array('name'=>'is_banner_image', 'value'=>'2','id'=>'is_banner_image_2','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_banner_image'));
						}
						echo form_radio($data);
						?>
				</div>
				<div class="img-box">
					<?php
					$targetpush_image_val_3 = (isset($targetpush_image_3) && !empty($targetpush_image_3)) ? $targetpush_image_3 :'';
					$img_3_display = (isset($targetpush_image_3) && !empty($targetpush_image_3)) ? '' :'dn';
					if(isset($targetpush_image_3) && $targetpush_image_3 != '') {
					$targetpush_image_3 = base_url('uploads/offer_images/'.$targetpush_image_val_3);
					$title = lang('tooltip_offer_image');
                     $targetpush_image_3_doc = $this->config->item('document_path').'uploads/offer_images/'.$targetpush_image_val_3; 
                     if(!file_exists($targetpush_image_3_doc)){
                        $targetpush_image_3 = base_url('uploads/No_Image_Available.png');
                        $title = "No Image Available";
                     }
					} else {
						$title = lang('tooltip_offer_image');
						$targetpush_image_3 = base_url('uploads/upload.png');
					}?>
					<a id="remove_btn_3" class="<?php echo $img_3_display;?>" href="javascript:void(0);" onclick="remove_image(3)">×</a>
					<div class="cell-img" ><img id="image-picker_3" src="<?php echo $targetpush_image_3; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
					<?php
						$data	= array('name'=>'targetpush_image_3', 'id'=>'targetpush_image_3', 'value'=>set_value('targetpush_image_3',$targetpush_image_val_3),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
						echo form_upload($data);
					?>
					<label for="offer_default_image"><?php echo lang('offer_default_image');?></label>
						<?php
						if(isset($is_banner_image) && $is_banner_image == 3) { 
							$data	= array('name'=>'is_banner_image', 'value'=>'3','id'=>'is_banner_image_3','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_banner_image'));
						} else {
							$data	= array('name'=>'is_banner_image', 'value'=>'3','id'=>'is_banner_image_3','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_banner_image'));
						}
						echo form_radio($data);
						?>
				</div>
						
			<div class="alert red-alert alert-danger alert-dismissible ">
               <i class="icon fa fa-warning"></i>
			  Upload up to 3MB images. 
              </div>
			</div>
			
			
			</div>	
<div class="text-right  btn_wrapZ mt10">
	<button type="submit" name="save" class="btn btn-primary submit_form"><?php echo lang('form_send');?></button>
    &nbsp;
	<!--
    <a href='<?php echo base_url().'admin/settings/targetpush/filter_members' ?>'><button type="button" name="back" class="btn default"><?php echo lang('back');?></button></a>
	-->
    &nbsp;
	<a href='<?php echo base_url().'admin/settings/targetpush/targetnotification' ?>'><button type="button" name="cancel" class="btn default"><?php echo lang('form_cancel');?></button></a>
</div>			
</div>	
		</div>
	
		
	</div>
	
</div>
</form>
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#add_targetpush_form").validate({
		// post form to action url
	});
    
    
      if(jQuery('#start_date_div').length > 0)  { 
                jQuery('#start_date_div').datetimepicker({
                    pickTime: false,
                    startDate: new Date(), 
                });
            }

            if(jQuery('#end_date_div').length > 0)  { 
                jQuery('#end_date_div').datetimepicker({
                    pickTime: false,
                     startDate: new Date(), 
                });
            }

    
});
	
	
tinymce.init({
    mode: "specific_textareas",
    editor_selector: "mceEditor",
    height: '300px',
    width: '770px',
    theme_advanced_resizing: true
  });
  	
function checkCheckBox(checkbox){
	//alert(checkbox);
	for(var i=1;i<=3;i++){
		if(checkbox != 'check'+i) {
			$('#check'+i).attr('checked', false);
		}
	}
}
	$("#image-picker_1").click(function() {
		$("input[name='targetpush_image_1']").click();
	});
	$("#image-picker_2").click(function() {
		$("input[name='targetpush_image_2']").click();
	});
	$("#image-picker_3").click(function() {
		$("input[name='targetpush_image_3']").click();
	});
	$("#qr-image-picker").click(function() {
		$("input[name='barcode_image']").click();
	});
	function read_url_image(input) {
		//alert(input.id);
		if(input.id == 'targetpush_image_1') {
			var pickerId	= '#image-picker_1';
			var fieldId	= '#targetpush_image_1';
			$("#remove_btn_1").show();
		} else if(input.id == 'targetpush_image_2') {
			var pickerId	= '#image-picker_2';
			var fieldId	= '#targetpush_image_2';
			$("#remove_btn_2").show();
		} else if(input.id == 'targetpush_image_3') {
			var pickerId	= '#image-picker_3';
			var fieldId	= '#targetpush_image_3';
			$("#remove_btn_3").show();
		}
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	
	function read_qr_image(input) {
		if(input.id == 'barcode_image') {
			var pickerId	= '#qr-image-picker';
			var fieldId	= '#barcode_images';
		} 
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var imageType = /image.*/;
			if (file.type.match(imageType)) {
				var reader = new FileReader();
				reader.onload = function (e) {
					//alert(pickerId);
					$(pickerId).attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				$(fieldId).val("");
				bootbox.alert("File not supported!");
			}
		}
	}
	
	
	
	$("#targetpush_price").on("keyup", function(){
			var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
			val = this.value;
		
		if(!valid){
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});
	
	$( "#add_targetpush_form" ).submit(function( event ) {
		checkBeacon();
		return;
	  event.preventDefault();
	});
	
	
	function checkBeacon() {
		
	}
	
	/**
	* Function used to remove image
	*/
	function remove_image(image_type) {
		bootbox.confirm("Are you sure want to delete this image?", function(result) {
			if(result==true) {
				// unset image data
				$("#image-picker_"+image_type).attr('src', '<?php echo base_url('uploads/upload.png');?>');
				var offer_id = $('#targetpush_id').val();
				$.ajax ({
					type: "POST",
					data : {offer_id:offer_id,image_type:image_type},
					url: "<?php echo base_url() ?>backend/vip_offer/remove_offer_image",
					async: false,
					success: function(data){ 
						if(data) {
							var file = $('#image-picker_'+image_type).attr('src','<?php echo base_url('uploads/upload.png');?>');
							$("#remove_btn_"+image_type).hide();
						}
					}, 
					error: function(error){
						bootbox.alert(error);
					}
				});
			}
		});
	}
    
    
    
    
          
               
    
    </script>
