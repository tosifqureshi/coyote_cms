<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('target_notification') ?></h4>
				<a href="<?php echo site_url("admin/settings/targetpush/filter_members"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_target_notification'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="pages_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('offer_sno'); ?></th>
								<th><?php echo lang('offer_list_image'); ?></th>
								<th><?php echo lang('offer_name'); ?></th>
								<th><?php echo lang('offer_status'); ?></th>
								<th><?php echo lang('offer_delete'); ?></th>
							</tr>
						</thead>
                    
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($targetpush) && is_array($targetpush) && count($targetpush)>0):
							foreach($targetpush as $offer) : 
								// set offer id
								$offer_id = encode($offer['targetpush_id']);?>	
								<tr>
								<td><?php echo $i;?></td>
								<td>
									<?php if(isset($offer['is_banner_image']) && !empty($offer['is_banner_image'])) {
										$offer_image = $offer['targetpush_image_'.$offer['is_banner_image']] ;
										$offer_image_check = $offer_image = (!empty($offer_image)) ? $offer_image : '';
										$offer_image = base_url('uploads/offer_images/'.$offer_image); 
										if(!empty($offer_image_check)){
										?>
										<img  id="imageresource" src="<?php echo $offer_image;?>" width="110" height="90" offer_name="<?php echo $offer['targetpush_name'];?>">
									<?php } 
										}
										?>
								</td>
								<td class="mytooltip tooltip-effect-1 desc-tooltip">
									<span class="tooltip-content clearfix">
										<span class="tooltip-text">
											<?php 
											$start_date_time = date('Y-m-d',strtotime($offer['notification_start_date']));
											$end_date_time = date('Y-m-d',strtotime($offer['notification_end_date']));
											$diff =  get_date_diffrence($start_date_time, $end_date_time." 23:59:59");
											
											echo '<b>'.lang('offer_name').' : </b>'.$offer['targetpush_name'] ;
											echo '<br>';
											echo '<b>'.lang('start_at').' : </b>'.date('F j, Y, g:i a',strtotime($offer['notification_start_date'])) ;
											echo '<br>';
											echo '<b>'.lang('end_at').'  : </b>'.date('F j, Y, g:i a',strtotime($offer['notification_end_date']." 23:59:59")) ;
											echo '<br>';
											echo $diff;?>
										</span> 
									</span>
									<span class="text-ttip">
										<a href="<?php echo site_url("admin/settings/targetpush/create_target_notification/".$offer_id);?>"><?php if(strlen($offer['targetpush_name']) > 20) { echo substr($offer['targetpush_name'],0,20).'...'; } else { echo $offer['targetpush_name']; }?></a>
									</span>
								</td>
								<td><?php 
									if($offer['status'] == '0') {
										?>
										<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo encode($offer['targetpush_id']); ?>','<?php echo encode($offer['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
										<?php
									} else {
										?>
										<a class="common-btn active-btn" onclick="changeStatus('<?php echo encode($offer['targetpush_id']); ?>','<?php echo encode($offer['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
									}
								?></td>
								<td>
									<a class="common-btn delete-btn" href="javascript:;" onclick="myFunction('<?php echo encode($offer['targetpush_id']); ?>')"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							<?php 
							$i++;
							endforeach ; endif; ?>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
</div>			

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});
	
    function myFunction(id) {
        bootbox.confirm("Are you sure want to delete the target push notification?", function(result) {
            if(result==true) {
                $('#loader1').fadeIn();	
                location.href="<?php echo site_url("admin/settings/targetpush/offer_delete/"); ?>/"+id;
            }
        });
    }

    function changeStatus(id,status,store_id) {
        bootbox.confirm("Are you sure want to change the target push notification status?", function(result) {
            if(result==true) {
                $('#loader1').fadeIn();
                location.href="<?php echo site_url("admin/settings/targetpush/update_offer_status/"); ?>/"+id+"/"+status+"/"+store_id;
            }
        });
    }


</script>

