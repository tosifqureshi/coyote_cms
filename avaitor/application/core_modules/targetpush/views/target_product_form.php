<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

	<tr class="product-half-side" id="qty_row_<?php echo $row_id;?>">
		<td>
			<?php
			$data = array('name'=>'product_ids[]', 'value'=>'','id'=>'product_id_'.$row_id,'class'=>'product_input digits required','row_id'=>$row_id,'exist_id'=>'');
			echo form_input($data);
			$data = array('type'=>'hidden','name'=>'product_img_ids[]','value'=>'');
			echo form_input($data);
			?>
		</td>
		<td>
			<?php
			$data = array('name'=>'product_name[]', 'value'=>'','id'=>'product_name_'.$row_id,'row_id'=>$row_id,'readonly'=>"readonly");
			echo form_input($data);?>
		</td>
		
		<td>
<?php 
/* For First Product Remove Button condition */ 
if($row_id != 1){
?>
        <span class="romove_row" row_id=<?php echo $row_id;?> product_img_id='' product_image=''><i class="fa fa-trash"></i></span>
<?php
}
?>       
</td>
	</tr>


