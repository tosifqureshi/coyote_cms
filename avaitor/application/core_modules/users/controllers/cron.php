<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Manage cron methods
 *
 * Setup the cron methods
 *
 * @package    Coyote
 * @subpackage Cron
 * @category   Controllers
 * @author     Tosif Qureshi 
 * @link       http://www.cdnsolutionsgroup.com
 *
 */

class Cron extends Front_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Setup the required libraries etc
	 *
	 * @retun void
	 */
	public function __construct()
	{
		parent::__construct();
		
		if (!class_exists('Cron_model'))
		{
			$this->load->model('users/Cron_model', 'cron_model');			
		}
		
		$this->load->database();
		
	}
	
	//--------------------------------------------------------------------

	/**
	 * Function to sync user account data
	 *
	 * @return void
	 */
	public function index() {
		
		// get local mysql users listing 
		$users = $this->cron_model->get_users();
				
		// add user in mssql database
		if(!empty($users) && count($users) > 0) {
			foreach($users as $user) {
				// get last user number from mssql
				$ms_users = $this->cron_model->get_last_ms_user();
				// set account number
				$ms_users_number = $ms_users->ACC_NUMBER; 
				
				if(!empty($ms_users_number)) {
					$ms_users_number = $ms_users_number+1; // increment
					// prepare account data
					$account_data = array('ACC_NUMBER'=>$ms_users_number,
						'ACC_FIRST_NAME'=>$user['firstname'],
						'ACC_SURNAME'=>$user['lastname'],
						'ACC_ADDR_1'=>$user['address'],
						'ACC_EMAIL'=>$user['email'],
						'ACC_POST_CODE'=>$user['post_code'],
						'ACC_MOBILE'=>$user['mobile'],
						'ACC_GENDER'=>$user['gender'],
						'ACC_STATUS'=>$user['active'],
						'ACC_DATE_OF_BIRTH'=>(isset($user['date_of_birth']) && !empty($user['date_of_birth']))?$user['date_of_birth']:'',
						'ACC_DATE_ADDED'=>date('Y-m-d'),
						'ACC_IS_APP_USER'=>1,
						//'ACC_PASSWORD'=>$user['password_hash'],
					);
					// prepare member data
					$member_data = array('MEMB_NUMBER'=>$ms_users_number,'MEMB_LOYALTY_TYPE'=>0,'MEMB_ACCUM_POINTS_IND'=>1,'MEMB_POINTS_BALANCE'=>0);
					// check user's email in mssql account table
					$is_exists = $this->cron_model->check_user_email($user['email']);
					if($is_exists) {
						// update account is exist status in mysql user table
						$this->cron_model->update_user_number($user['id'] ,array('is_already_exist'=>1));
					} else {
						// insert user in mssql account table
						$inserted_id = $this->cron_model->manage_users_inserton($account_data);
						// update account number in mysql user table
						$this->cron_model->update_user_number($user['id'] ,array('acc_number'=>$ms_users_number)); 
						// check member number is exist or not
						$member_exist = $this->cron_model->check_member_number($ms_users_number);
						if($member_exist) {
							// insert member data
							$this->cron_model->add_member_data($member_data);
						}
					}
				}
			}
		}
		return true;
		
	}
	
	/**
	 * Function to manage mssql and mysql store data
	 * @access  : public
	 */
	 public function manage_stores() {
		// get mssql stores data
		$ms_stores = $this->cron_model->get_ms_stores();
		
		if(!empty($ms_stores)) {
			foreach($ms_stores as $ms_store) {
				// set store values
				$store_name = $ms_store['STOR_DESC'];
				$store_address_1 = $ms_store['STOR_ADDR_1'];
				$ms_store_id = $ms_store['STOR_STORE'];
				// check store id existance
				if(!empty($store_name) && !empty($store_address_1)) {
					$is_store_exist = $this->cron_model->check_store_exist($store_name,$ms_store_id);
					if($is_store_exist) {
						// prepare store data
						$store_data = array('ms_store_id'=>$ms_store['STOR_STORE'],
							'store_name'=>$ms_store['STOR_DESC'],
							'store_abn'=>$ms_store['STOR_ABN'],
							'store_address_1'=>$ms_store['STOR_ADDR_1'],
							'store_address_2'=>$ms_store['STOR_ADDR_2'],
							'store_post_code'=>$ms_store['STOR_POST_CODE'],
							'store_phone'=>$ms_store['STOR_PHONE'],
							'store_fax'=>$ms_store['STOR_FAX'],
							'status'=>($ms_store['STOR_STATUS'] == 'Active') ? 1 : 0,
						);
						// insert store data
						$store_id = $this->cron_model->add_store_data($store_data);
					}
				}
			}
		}
	 }
	 
	 /**
	 * Function to manage mssql and mysql user data synchrinization
	 * input : limit, offset
	 * output: void
	 * @access  : public
	 */
	 public function sync_users_log($limit=0,$offset=0) {
		// $name = $this->mssql_real_escape_string('æ??è¾°');
		// echo $name;die;
		 // get mssql users
		 $ms_users = $this->cron_model->get_ms_users($limit,$offset);
		 //echo '<pre>';
		// print_r($ms_users);die;
		 if(!empty($ms_users)) {
			 foreach($ms_users as $ms_user) {
				if(!empty($ms_user['ACC_EMAIL']) && !empty($ms_user['ACC_NUMBER'])) {
					if (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $ms_user['ACC_EMAIL'])) {
						// execute nothing
					} else {
						// check user exist in user log
						$is_user_not_exist = $this->cron_model->check_user_exist($ms_user['ACC_EMAIL'],$ms_user['ACC_NUMBER']);

						if($is_user_not_exist) {
							// assign blank value if first name contains special charecter
							if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $ms_user['ACC_FIRST_NAME']))
							{
								$ms_user['ACC_FIRST_NAME'] = '';
							}
							// assign blank value if last name contains special charecter
							if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $ms_user['ACC_SURNAME']))
							{
								$ms_user['ACC_SURNAME'] = '';
							}
							// prepare user data
							$user_data = array('email'=>$ms_user['ACC_EMAIL'],
								'display_name' => $ms_user['ACC_FIRST_NAME'],
								'username'     => $ms_user['ACC_FIRST_NAME'],
								'password_hash'=> '',
								'created_on'   => $ms_user['ACC_ADDR_1'],
								'firstname'    => $ms_user['ACC_FIRST_NAME'],
								'lastname'     => $ms_user['ACC_SURNAME'],
								'date_of_birth'=> $ms_user['ACC_DATE_OF_BIRTH'],
								'address'      => $ms_user['ACC_ADDR_1'],
								'post_code'    => $ms_user['ACC_POST_CODE'],
								'mobile'       => $ms_user['ACC_MOBILE'],
								'gender'       => ($ms_user['ACC_GENDER'] == 3) ? 1 : $ms_user['ACC_GENDER'],
								'status'       => $ms_user['ACC_STATUS'],
								'acc_number'   => $ms_user['ACC_NUMBER'],
								'created_on'   => date('Y-m-d h:i:g'),
							);
							// update user in mysql user table
							$this->cron_model->manage_users_mysql_inserton($user_data);
						}
					}
				}
			}
		 } else {
			echo 'No record found.';
		 }
	 }
	 
	function mssql_real_escape_string($s) {
		if(get_magic_quotes_gpc()) {
			$s = stripslashes($s);
		}
		$s = str_replace("'","''",$s);
		return $s;
	}

	//--------------------------------------------------------------------

	function test_ms_add($limit,$offset) {
		
		$ms_last_users = $this->cron_model->get_ms_users($limit,$offset);
		echo '<pre>';
		print_r($ms_last_users);die;
		//$member_data = array('MEMB_NUMBER'=>'2100000007','MEMB_LOYALTY_TYPE'=>0,'MEMB_ACCUM_POINTS_IND'=>1,'MEMB_POINTS_BALANCE'=>0);
		//$this->cron_model->add_member_data($member_data);
		//$is_emai_exist = $this->cron_model->check_user_email('subhashshrivastava@cdnsol.com'); 
		//echo $is_emai_exist;die;
		
		//$ms_users = $this->cron_model->add_member_data();
		//echo $ms_last_users->ACC_NUMBER;
		echo '<pre>';
		print_r($ms_last_users);
		die;
		
	}
	
	/*
	 * @syn ProdTbl and OutpTbl
	 * */
	function getProduct($limit,$offset) {
		$products = $this->cron_model->get_ms_product($limit,$offset);
		//$products2 = $this->cron_model->get_ms_product2($limit,$offset);
		//echo '<pre>';
		print_r($products);die;
	}
	
	/*
	 * @syn ProdTbl and OutpTbl
	 * */
	function getOutp($limit,$offset) {
		$products = $this->cron_model->get_ms_outp($limit,$offset);
		echo '<pre>';
		print_r($products);die;
	}
	
	/**
	 * @create_table()
	 */
	function create_table() {
		$result = $this->cron_model->creatTable();
		echo '<pre>';
		print_r($result);die;
	}
	
}//end class
