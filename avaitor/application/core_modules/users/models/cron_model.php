<?php 

/**
 * Cron Model
 *
 * The central way to access and perform CRUD on cron.
 *
 * @package    Avaitor
 * @subpackage Modules_Users
 * @category   Models
 * @author     Tosif Qureshi
 * @link       http://www.cdnsolutionsgroup.com
 */
class Cron_model extends BF_Model
{
    
    function __construct() {
        parent::__construct();    
        $this->read_db  = $this->load->database('read', TRUE); //Loading read database.
        $this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // define tables
		$this->users_table   = 'users';
		$this->account_table = 'ACCOUNT';
		$this->member_table  = 'MEMBER';
		$this->store_table  = 'STORTBL';
		$this->store_log_table  = 'stores_log';
    }    
	
	
	/**
	 * Function to get mysql users listing of coyote who's not a mssql user
	 * @input : 
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	public function get_users() {
		$this -> read_db -> select('id,acc_number,email,password_hash,firstname,lastname,mobile,gender,active,date_of_birth,address,post_code');
		$this -> read_db -> where(array('acc_number'=>null));
		//$this -> read_db ->limit(1);
		$this -> read_db ->order_by('id','desc');
		$result = $this -> read_db -> get($this->users_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to get users data from MSSQL database
	 * @input : 
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	public function get_ms_users($limit=0,$offset=0) {  
		
		//$this -> mssql_db -> select('ACC_NUMBER,ACC_FIRST_NAME,ACC_SURNAME,ACC_ADDR_1,ACC_ADDR_2,ACC_POST_CODE,ACC_ACCOUNT_NAME,ACC_CONTACT,ACC_MOBILE,ACC_EMAIL,ACC_GENDER,ACC_STATUS,ACC_PASSWORD,ACC_DATE_OF_BIRTH,ACC_DATE_ADDED');
		//$this -> mssql_db -> where(array('ACC_EMAIL !='=>""));	// status=>1 : active and is_delete=>0 : data is not delete
		//$this -> mssql_db -> limit($limit,$offet);
		//$this -> mssql_db -> order_by('ACC_NUMBER','asc');
		//$result = $this -> mssql_db -> get('ACCOUNT');
		
		$result= $this->mssql_db->query('SELECT ACC_NUMBER,ACC_FIRST_NAME,ACC_SURNAME,ACC_ADDR_1,ACC_ADDR_2,ACC_POST_CODE,ACC_ACCOUNT_NAME,ACC_CONTACT,ACC_MOBILE,ACC_EMAIL,ACC_GENDER,ACC_STATUS,ACC_PASSWORD,ACC_DATE_OF_BIRTH,ACC_DATE_ADDED FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY ACC_NUMBER) as row FROM ACCOUNT where ACC_EMAIL != "") a WHERE row > '.$limit.' and row <= '.$offset);
		//echo $this -> mssql_db -> last_query();die;
		return $result -> result_array();
		
	
	}
	
	/**
	 * Function to get last mssql user id
	 * @input : 
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	public function get_last_ms_user() {  
		
		$this -> mssql_db -> select('ACC_NUMBER');
		//$this -> mssql_db -> where(array('ACC_NUMBER'=>2000000664));	// status=>1 : active and is_delete=>0 : data is not delete
		$this -> mssql_db -> limit(1);
		$this -> mssql_db -> order_by('ACC_NUMBER', 'desc');
		$result = $this -> mssql_db -> get('ACCOUNT');
		return  $result -> row();
		
	
	}
	
	/**
	 * Function to manage insertion of account mssql data
	 * @input : data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function manage_users_inserton($data) {
		$this->mssql_db->insert($this->account_table, $data);
		return $this->mssql_db->insert_id();
	}
	
	/**
	 * Function to manage insertion of member data
	 * @input : data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function add_member_data($data) {
		$this->mssql_db->insert($this->member_table, $data);
		return true;
	}
	
	/**
	 * Function to update user account number
	 * @input : user_id , data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function update_user_number($user_id=0,$data) {
		
		if($user_id) { // update data
			$this->read_db->where('id', $user_id);
			$this->read_db->update($this->users_table, $data);
			return true;
		}
	}
	
	/**
	 * Function to get user's email in mssql 
	 * @input : email
	 * @output: void
	 * @auther: Tosif Qureshi
	 */
	public function check_user_email($email='') { 
		
		if($email) { 
			// get email from account table
			$this -> mssql_db -> select('ACC_NUMBER');
			$this -> mssql_db -> where('LOWER(ACC_EMAIL)=', strtolower($email));
			$this -> mssql_db -> limit(1);
			$result = $this -> mssql_db -> get('ACCOUNT');
			$result_data = $result -> row();
			if(!empty($result_data) && isset($result_data->ACC_NUMBER)) {
				return  true;
			}	
		}
		return false;
	}
	
	/**
	 * Function to get user's email in mssql 
	 * @input : email
	 * @output: void
	 * @auther: Tosif Qureshi
	 */
	public function check_member_number($memb_number='') { 
		
		if(!empty($memb_number)) {
			// get email from account table
			$this -> mssql_db -> select('MEMB_LOYALTY_TYPE');
			$this -> mssql_db -> where('MEMB_NUMBER', $memb_number);
			$this -> mssql_db -> limit(1);
			$result = $this -> mssql_db -> get($this->member_table);
			$result_data = $result -> row();
			if(!empty($result_data) && isset($result_data->MEMB_LOYALTY_TYPE)) {
				return  false;
			}	
		}
		return true;
	}
	
	/**
	 * Function to get stores data 
	 * @input : 
	 * @output: void
	 * @auther: Tosif Qureshi
	 */
	public function get_ms_stores() { 
		
		// get email from account table
		$this -> mssql_db -> select('*');
		$result = $this -> mssql_db -> get($this->store_table);
		$result_data = $result -> result_array();
		return $result_data;
	}
	
	/**
	 * Function to check store in mysql 
	 * @input : store_name
	 * @output: void
	 * @auther: Tosif Qureshi
	 */
	public function check_store_exist($store_name='',$ms_store_id=0) {
		
		if(!empty($store_name)) {
			// get email from account table
			$this -> read_db -> select('store_id');
			$this -> read_db -> where('store_name', $store_name);
			$this -> read_db -> or_where('ms_store_id', $ms_store_id);
			$this -> read_db -> limit(1);
			$result = $this -> read_db -> get($this->store_log_table);
			$result_data = $result -> row();
			if(!empty($result_data) && isset($result_data->store_id)) {
				return  false;
			}	
		}
		return true;
	}
	
	/**
	 * Function to manage insertion of user mysql data
	 * @input : data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function manage_users_mysql_inserton($data) {
		$this->read_db->insert($this->users_table, $data);
		return $this->read_db->insert_id();
	}
	
	/**
	 * Function to check user in mysql 
	 * @input : store_name
	 * @output: void
	 * @auther: Tosif Qureshi
	 */
	public function check_user_exist($email='',$acc_number='') {
		
		if(!empty($email)) {
			// get email from account table
			$this -> read_db -> select('id');
			$this -> read_db -> where('lower(email)', $email);
			$this -> read_db -> or_where('acc_number', $acc_number);
			$this -> read_db -> limit(1);
			$result = $this -> read_db -> get($this->users_table);
			$result_data = $result -> row();
			if(!empty($result_data) && isset($result_data->id)) {
				return  false;
			}	
		}
		return true;
	}
	
	/**
	 * Function to manage insertion of store data
	 * @input : data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function add_store_data($data) {
		$this->read_db->insert($this->store_log_table, $data);
		return true;
	}
	
	public function get_member_data($data) {
		$this -> mssql_db -> select('*');
		$this -> mssql_db -> limit(10);
		$result = $this -> mssql_db -> get($this->member_table);
		$result_data = $result -> result_array();
		return $result_data;
	}
	

	/*
	 * @get mssql product
	 * */
	public function get_ms_product($limit=0,$offset=0) {
		$table = 'ava_ms_products';
		$priKey = 'PROD_NUMBER';
		$insertCol = "`".$priKey."`,";
		$result= mssql_query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM ProdTbl where Prod_Number != "") a WHERE row > '.$limit.' and row <= '.$offset);
		$fields = mssql_num_fields($result);
		//echo $numRow = mssql_num_rows($result);die;
		/*$query = "CREATE TABLE IF NOT EXISTS `".$table."` ( `".$priKey."` int(11) NOT NULL AUTO_INCREMENT,";
		for ($i=1; $i < $fields; $i++) {
			$type  = mssql_field_type($result, $i); 
			$len   = mssql_field_length($result, $i);
			$temp = "NOT NULL";
			if($type == 'real') {
				$type = 'bigint';
				$len = '20';
				$temp = "NOT NULL DEFAULT '0'";
			} else if($type == 'char') { 
				$type = 'varchar';
				$len = '225';
			}
			 $type.'***'.$len.'***';		
			 $name  = mssql_field_name($result, $i); 
			//echo $i."---".$fields.'--<br/>';
			if($type == 'datetime') { 
				$query .= "`".$name."`".$type." ".$temp.",";
			} else {
				$query .= "`".$name."`".$type."(".$len.") ".$temp.",";
			}
			if($fields == ($i+1)) {
				$insertCol .= "`".$name."`";
			} else {
				$insertCol .= "`".$name."`, ";
			}
		}
			$query .= "PRIMARY KEY (`PROD_NUMBER`)
						) ENGINE=InnoDB AUTO_INCREMENT=".$numRow." DEFAULT CHARSET=utf8;";
		//$this->db->create_table($query);			
		echo $query;
		die;	*/
		$insertQuery = '';
		//$insertQuery .= "INSERT INTO `ava_tblproduct` (".$insertCol.") VALUES ";
		$data = array();		
		
		$j = 0;
		while($row = mssql_fetch_array($result)) { 
			//$insertQuery .= "(";
			$insertQuery = array();		
			for ($i=0; $i < $fields; $i++) {
				$name  = mssql_field_name($result, $i);
				//$insertQuery .= $name." => ";
				
				$type  = mssql_field_type($result, $i); 
				$value = $row[$i];
				if($value == NULL) {
					$value = ' ';
				}
				if($type == 'datetime') { 
					$value = date("Y-m-d H:i:s",strtotime($value));
				}
				if($type == 'real' && $row[$i] == '') {
					$value = 0;
				}
					$insertQuery[$name] = utf8_encode($value);
				
				/*
				if($fields == ($i+1)){
					$insertQuery .= "'".mysql_real_escape_string($value)."'";
				} else {
					$insertQuery .= "'".mysql_real_escape_string($value)."', " ;
				}*/
			}
			$data[] = $insertQuery;
			$j++;
			//echo $j." == ".$numRow.'<br/>';
			/*if($j == $numRow) {
				$insertQuery .= ")";
			} else {
				$insertQuery .= "),";	
			}*/
			
		}
		//echo '<pre>';
		//print_r($data);
		//die;
		foreach($data as $singleData) {
			echo $this->db->insert($table,$singleData);
		}
		die;
		return $query;
		
		
		/*
		$result2 = mssql_query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM PRODTBL where Prod_Number != "") a WHERE row > '.$limit.' and row <= '.$offset);
		//$result2= $this->mssql_db->query("select distinct Prod_Desc, Prod_Number from PRODTBL join OUTPTBL on Prod_Number = OUTP_PRODUCT where OUTP_STATUS = 'active'");
		return $result2 -> result_array(); */
	}
	
	public function get_ms_product2($limit=0,$offset=0) {
		$result= $this->mssql_db->query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM PRODTBL where Prod_Number != "") a WHERE row > '.$limit.' and row <= '.$offset);
		//$result= $this->mssql_db->query("select distinct Prod_Desc, Prod_Number from PRODTBL join OUTPTBL on Prod_Number = OUTP_PRODUCT where OUTP_STATUS = 'active'");
		//$result -> result_array();
		echo $result->num_rows();
	}
	
	
	
	/*
	 * @get mssql product
	 * */
	public function get_ms_outp($limit=0,$offset=0) {
		$table = 'ava_outptbl';
		$priKey = 'OUTP_PRODUCT';
		$insertCol = "`".$priKey."`,";
		
		$insertQuery = '';
		
		$result= mssql_query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY OUTP_PRODUCT) as row FROM OUTPTBL where OUTP_PRODUCT != "") a WHERE row > '.$limit.' and row <= '.$offset);
		//$result= $this->mssql_db->query("select distinct Prod_Desc, Prod_Number from PRODTBL join OUTPTBL on Prod_Number = OUTP_PRODUCT where OUTP_STATUS = 'active'");
		$data = array();		
		$fields = mssql_num_fields($result);
		$j = 0;
		while($row = mssql_fetch_array($result)) { 
			//$insertQuery .= "(";
			$insertQuery = array();		
			for ($i=0; $i < $fields; $i++) {
				$name  = mssql_field_name($result, $i);
				//$insertQuery .= $name." => ";
				
				$type  = mssql_field_type($result, $i); 
				$value = $row[$i];
				if($value == NULL) {
					$value = ' ';
				}
				if($type == 'datetime') { 
					$value = date("Y-m-d H:i:s",strtotime($value));
				}
				if($type == 'real' && $row[$i] == '') {
					$value = 0;
				}
					$insertQuery[$name] = utf8_encode($value);
				
				/*
				if($fields == ($i+1)){
					$insertQuery .= "'".mysql_real_escape_string($value)."'";
				} else {
					$insertQuery .= "'".mysql_real_escape_string($value)."', " ;
				}*/
			}
			$data[] = $insertQuery;
			$j++;
			//echo $j." == ".$numRow.'<br/>';
			/*if($j == $numRow) {
				$insertQuery .= ")";
			} else {
				$insertQuery .= "),";	
			}*/
			
		}
		echo '<pre>';
		print_r($data);
		//die;
		foreach($data as $singleData) {
			echo $this->db->insert($table,$singleData);
		}
		die;
		return $query;
	}
	
	/**
	 * @create table 
	 * */
	public function creatTable($limit=0,$offset=0) {
		$table = 'ava_outptbl';
		$priKey = 'OUTP_PRODUCT';
		$insertCol = "`".$priKey."`,";
		$result= mssql_query('SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY OUTP_PRODUCT) as row FROM OUTPTBL where OUTP_PRODUCT != "") a WHERE row > '.$limit.' and row <= '.$offset);
		$fields = mssql_num_fields($result);
		$numRow = mssql_num_rows($result);
		$query = "CREATE TABLE IF NOT EXISTS `".$table."` ( `".$priKey."` int(11) NOT NULL AUTO_INCREMENT,";
		for ($i=1; $i < $fields; $i++) {
			$type  = mssql_field_type($result, $i); 
			$len   = mssql_field_length($result, $i);
			$temp = "NOT NULL";
			if($type == 'real') {
				$type = 'bigint';
				$len = '20';
				$temp = "NOT NULL DEFAULT '0'";
			} else if($type == 'char') { 
				$type = 'varchar';
				$len = '225';
			}
			 $type.'***'.$len.'***';		
			 $name  = mssql_field_name($result, $i); 
			//echo $i."---".$fields.'--<br/>';
			if($type == 'datetime') { 
				$query .= "`".$name."`".$type." ".$temp.",";
			} else {
				$query .= "`".$name."`".$type."(".$len.") ".$temp.",";
			}
			if($fields == ($i+1)) {
				$insertCol .= "`".$name."`";
			} else {
				$insertCol .= "`".$name."`, ";
			}
		}
			$query .= "PRIMARY KEY (`".$priKey."`)
						) ENGINE=InnoDB AUTO_INCREMENT=".$numRow." DEFAULT CHARSET=utf8;";
		//$this->db->create_table($query);			
		echo $query;
		die;
	}
}
