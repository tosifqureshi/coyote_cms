<?php if (has_permission('Core.Users.Manage')):?>
<ul class="nav nav-pills">
	<?php /* ?> 
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/users') ?>"><?php echo lang('bf_users'); ?></a>
	</li>
	<?php */ ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/users/create') ?>" class ="btn btn-primary" id="create_new"><?php echo lang('bf_add_new_admin'); ?></a>
	</li>
</ul>
<?php endif;?>
