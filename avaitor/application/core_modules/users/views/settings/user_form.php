<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php 
if (isset($user) && $user->banned) : ?>
<div class="alert alert-warning fade in">
	<h4 class="alert-heading"><?php echo lang('us_banned_admin_note'); ?></h4>
</div>
<?php endif; ?>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $toolbar_title;?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal form-material form-with-label" autocomplete="off"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- email input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('bf_email');?><span class="red">*</span></label>
									<input type="email" name="email" id="email" class="form-control form-control-line" value="<?php echo set_value('email', isset($user) ? $user->email : '') ?>">
								</div>
								<!-- email input end here -->
								
								<!-- username input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('bf_username');?><span class="red">*</span></label>
									<input type="text" name="username" id="username" class="form-control form-control-line" value="<?php echo set_value('username', isset($user) ? $user->username : '') ?>">
								</div>
								<!-- username input end here -->
								
								<!-- password input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('bf_password');?><span class="red">*</span></label>
									<input type="password" id="password" class="form-control form-control-line" name="password" value="">
								</div>
								<!-- password input end here -->
								
								<!-- confirm password input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('bf_password_confirm');?><span class="red">*</span></label>
									<input type="password" id="pass_confirm" class="form-control form-control-line" name="pass_confirm" value="">
								</div>
								<!-- confirm password input end here -->
								
								<!-- role type dropdown start here -->
								<div class="form-group row">
									<label class="control-label col-md-12">
										<h4> <?php echo lang('us_role');?></h4>
									</label>
									<div class="col-md-6">
										<select name="role_id" id="role_id" class="chzn-select form-control">
											<?php 
											if (isset($roles) && is_array($roles) && count($roles)) : 
												foreach ($roles as $role) : 
													if (has_permission('Permissions.'. ucfirst($role->role_name) .'.Manage')) : 
														// check if it should be the default
														$default_role = FALSE;
														if ((isset($user) && $user->role_id == $role->role_id) || (!isset($user) && $role->default == 1))
														{
															$default_role = TRUE;
														} ?>
													<option value="<?php echo $role->role_id ?>" <?php echo set_select('role_id', $role->role_id, $default_role) ?>>
														<?php e(ucfirst($role->role_name)) ?>
													</option>
													<?php endif; ?>
												<?php endforeach; ?>
											<?php endif; ?>
										</select>
									</div>
								</div>
								<!-- role type dropdown end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="submit" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('bf_action_save');?></button>
							</div>
							 <div class="mx-1">
								<a href='<?php echo base_url().'admin/settings/users' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('bf_action_cancel');?></button>
								</a>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>	
			</div>	
		</div>	
	</div>	
</div>

