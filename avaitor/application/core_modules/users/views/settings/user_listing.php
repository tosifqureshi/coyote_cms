<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/dataTables.buttons.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/jszip.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/buttons.html5.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/buttons.print.min.js'); ?>"></script>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left">Users</h4>
				<?php if (has_permission('Core.Users.Manage')):?>
				<a href="<?php echo site_url(SITE_AREA .'/settings/users/create') ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('bf_add_new_admin'); ?></button>
				</a>
				<?php endif;?>
				<div class="flex-box row">
					<div class="form-group col-md-3">
						<label>User role</label>
						<select data-placeholder="user roll" class="form-control bt_select" tabindex="3" name="role" id="role">
							<option value="4" selected="selected" >User</option>
							<option value="1">Administrator</option>
							<option value="7">Marketing</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Status</label>
						<select data-placeholder="user type" class="form-control bt_select" tabindex="3" name="status" id="status">
							<option value="active" selected="selected">Active</option>
							<option value="inactive">Inactive</option>
							<option value="banned">Banned</option>
							<option value="deleted">Deleted</option>
						</select>
					</div>
					<div class="form-group col-md-2">
							<label></label>
							<button class="btn waves-effect waves-light btn-block btn-info resetfilter">Clear Filtter</button>
					</div>
					<div class="form-group col-md-2">
						<label></label>
						<button class="btn waves-effect waves-light btn-block btn-info reportBtn">Generate Report</button>
					</div>
				</div>	
				
				<div class="table-responsive">  
					<table id="user_table" class="table table-bordered table-striped dataTable">
						<thead>
							<tr role="row">
								<th><?php echo lang('bf_id'); ?></th>
								<th><?php echo lang('bf_firstname'); ?></th>
								<th><?php echo lang('bf_email'); ?></th>
								<th><?php echo lang('us_devicetype'); ?></th>
								<th><?php echo lang('us_store'); ?></th>
								<th><?php echo lang('us_role'); ?></th>
								<th><?php echo lang('us_last_login'); ?></th>				
								<th><?php echo lang('us_status'); ?></th>
							</tr>
						</thead>
					</table>
				</div>		
				
				<!-- start the bootstrap modal where the download report will appear -->
				<div class="modal fade" id="userreportmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"><?php echo lang('download_user_report');?></h4>
							</div>
							<div class="modal-body modal-days-avail">
								<div class="form-wrapper clearboth">
									<!-- Start / end date fields start here -->
									<div class="row form-group">
										<div class="col-md-6">
											<label for="co_start_date"><?php echo lang('start_date');?></label>
											<?php
											$min_date = '01-07-2017';?>
											<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($min_date) ? $min_date : '';?>" id="start_date" readonly="readonly">
										</div>
											
										<div class="col-md-6">
											<label for="co_end_date"><?php echo lang('end_date');?></label>
											<input type="text" name="end_date" class="form-control required" value="<?php echo !empty($min_date) ? $min_date : '';?>" id="end_date" readonly="readonly">
										</div>
									</div>
									<!-- Start / end date fields end here -->
									
								</div>	
								<div class="form-wrapper clearboth">
									<a href="#"><button type="button" class="btn btn-info reportExport">Export Excel</button></a>
								</div>				
							</div>
							<!--
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
							-->
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
			</div>	
		</div>	
	</div>	
</div>

<script>
	// MAterial Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
	
	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'DD-MM-YYYY'
	});
		
	$(document).ready(function(){			
		$('#user_table').dataTable({			  
		 "bPaginate": true,
         "bLengthChange": true,
         "bFilter": true,
         "bSort": true,
         "bInfo": false,
         "bAutoWidth": false,
         "processing": false,
         "serverSide": true,
         "searching": true,
         "destroy":true,
        "aoColumns":[
 			{"bSortable": false},
 			{"bSortable": true},
 			{"bSortable": true},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			],
 			dom: 'lBfrtip',
			
			buttons: [{
						extend: 'excel',
						text:'export to excel',
						title: 'current record excel' 
					 }],
			"ajax": {
				"url":"<?php echo base_url('admin/settings/users/userlist'); ?>",
				"data": function(d){ 
				   d.status = $('#status').val();
				   d.role   = $('#role').val(); 
				 }
			},
			"aoColumnDefs":[{ 'bSortable': false,'aTargets':[]}],
			"columnDefs":[{"targets": 0,"bSortable":false,"orderable":false,"visible":true}],
			lengthMenu: [
				[10, 25, 50, 100, 200, 300],
				[10, 25, 50, 100, 200, 300]
			],
			
		});		
	    var oTable;
        oTable = $('#user_table').dataTable();
		$("#status").change(function(){		
		 oTable.fnFilter(); 
		});
		$("#role").change(function(){		
		 oTable.fnFilter(); 
		});
	
		//reset datatable filter 
		$(document).on('click','.resetfilter',function(){			
			 $('.bt_select').prop('selectedIndex',0);
			 $('#user_table').DataTable().search('').draw();
			 oTable.fnFilterClear();
        });	
		
	});
	
   /*
    | ------------------------------------------------------------
    | Show bootstrap report popup
    | ------------------------------------------------------------
    */ 
    $(document).delegate( ".reportBtn", "click", function(e) {
        $('#userreportmodal').modal('show');		  
    });
    
	$(document).delegate( ".reportExport", "click", function(e) {
        var start_date = $('#start_date').val();	  
        var end_date = $('#end_date').val();
        window.location.href = '<?php echo site_url(SITE_AREA .'/settings/users/userExportxls'); ?>'+'/'+start_date+'/'+end_date;
    });
	</script>
	
	
 
