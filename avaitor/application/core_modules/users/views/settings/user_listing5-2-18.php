<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/dataTables.buttons.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/jszip.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/buttons.html5.min.js'); ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo Template::theme_url('js/bootexport/buttons.print.min.js'); ?>"></script>

<div class="form-above">


<select data-placeholder="user roll" class="form-control bt_select" tabindex="3" name="role" id="role">
	<option value="4" selected="selected" >User</option>
	<option value="1">Administrator</option>
	<option value="7">Marketing</option>
</select>
<select data-placeholder="user type" class="form-control bt_select" tabindex="3" name="status" id="status">
	<option value="active" selected="selected">Active</option>
	<option value="inactive">Inactive</option>
	<option value="banned">Banned</option>
	<option value="deleted">Deleted</option>
</select>
<a href="<?php echo site_url(SITE_AREA .'/settings/users/userExportxls'); ?>"><button type="button" class="btn btn-info">Export Excel</button></a>
<button type="button" class="btn btn-danger resetfilter">Clear Filtter</button>
</div>
	
	<div role="grid" class="dataTables_wrapper user_table" id="dyntable_wrapper">

		<table id="user_table" class="table table-bordered dataTable" aria-describedby="dyntable_info">    
			<thead>
				<tr role="row">
					<th><?php echo lang('bf_id'); ?></th>
					<th><?php echo lang('bf_firstname'); ?></th>
					<th><?php echo lang('bf_email'); ?></th>
					<th><?php echo lang('us_devicetype'); ?></th>
					<th><?php echo lang('us_store'); ?></th>
					<th><?php echo lang('us_role'); ?></th>
					<th><?php echo lang('us_last_login'); ?></th>				
					<th><?php echo lang('us_status'); ?></th>
				</tr>
			</thead>
		</table>                 
	</div>

	<script>
	$(document).ready(function(){			
		$('#user_table').dataTable({			  
		 "bPaginate": true,
         "bLengthChange": true,
         "bFilter": true,
         "bSort": true,
         "bInfo": false,
         "bAutoWidth": false,
         "processing": false,
         "serverSide": true,
         "searching": true,
         "destroy":true,
        "aoColumns":[
 			{"bSortable": false},
 			{"bSortable": true},
 			{"bSortable": true},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			{"bSortable": false},
 			],
 			dom: 'lBfrtip',
			
			buttons: [{
						extend: 'excel',
						text:'export to excel',
						title: 'current record excel' 
					 }],
			"ajax": {
				"url":"<?php echo base_url('admin/settings/users/userlist'); ?>",
				"data": function(d){ 
				   d.status = $('#status').val();
				   d.role   = $('#role').val(); 
				 }
			},
			"aoColumnDefs":[{ 'bSortable': false,'aTargets':[]}],
			"columnDefs":[{"targets": 0,"bSortable":false,"orderable":false,"visible":true}],
			lengthMenu: [
				[10, 25, 50, 100, 200, 300],
				[10, 25, 50, 100, 200, 300]
			],
			
		});		
	    var oTable;
        oTable = $('#user_table').dataTable();
		$("#status").change(function(){		
		 oTable.fnFilter(); 
		});
		$("#role").change(function(){		
		 oTable.fnFilter(); 
		});
	
		//reset datatable filter 
		$(document).on('click','.resetfilter',function(){			
			 $('.bt_select').prop('selectedIndex',0);
			 $('#user_table').DataTable().search('').draw();
			 oTable.fnFilterClear();
        });	
		
	});
	</script>
	
	
 
