<div class="login-box card">
	<div class="card-body">	
		<?php echo form_open($this->uri->uri_string(), array('class' => 'form-horizontal form-material','id'=>'forgotform', 'autocomplete' => 'off')); ?>
			<div class="form-group ">
				<div class="col-xs-12">
					<h3><?php echo lang('us_reset_password'); ?></h3>
					<p class="text-muted"><?php echo lang('us_reset_note'); ?></p>
				</div>
			</div>
			<div class="form-group ">
				<div class="col-xs-12">
					<div class="control-group <?php echo iif( form_error('email') , 'error'); ?>">
						<input class="form-control" type="text" name="email" id="email" value="<?php echo set_value('email') ?>" placeholder="<?php echo lang('us_reset_placeholder'); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group text-center m-t-20">
				<div class="col-xs-12">
					<input class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="submit" id="submit" value="<?php echo lang('us_reset_button');?>" />
				</div>
			</div>
		<?php echo form_close(); ?>
		
		<div class="form-group">
			<div class="col-md-12">	
				<a href="<?php echo site_url('login'); ?>" class="text-dark pull-right"><?php echo lang('us_return_login');?></a> 
			</div>
		</div>
	</div>
</div>
</section>

	

