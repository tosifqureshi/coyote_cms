<div class="login-box card">
	<div class="card-body">	
		<?php
		$site_open = $this->settings_lib->item('auth.allow_register');

		if ( !$site_open ) : ?>
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-danger fade in span6" >
					  <a data-dismiss="alert" class="close">&times;</a>
						<h4 class="alert-heading">Sorry this is invite only site.</h4>
					</div>
				</div>
			</div>
		<?php
		endif;
		if (auth_errors() || validation_errors()) :?>
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-error fade in">
					  <a data-dismiss="alert" class="close">&times;</a>
						<?php echo auth_errors() . validation_errors(); ?>
					</div>
				</div>
			</div>
		<?php endif; 

		echo form_open('login', array('class' => 'form-horizontal form-material','id'=>'loginform', 'autocomplete' => 'off')); ?>
			<a href="javascript:void(0)" class="text-center db"><img src="<?php echo Template::theme_url('assets/images/logo.png'); ?>" alt="Home" /></a>
			<div class="form-group m-t-40">
				<div class="col-xs-12">
					<div class="control-group <?php echo iif( form_error('login') , 'error') ;?>">
						<input class="form-control" type="text" name="login" id="login_value" value="<?php echo set_value('login'); ?>" tabindex="1" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-12">
					<div class="control-group <?php echo iif( form_error('password') , 'error') ;?>">
						<input class="form-control" type="password" name="password" id="password" value="" tabindex="2" placeholder="<?php echo lang('bf_password'); ?>" />
					</div>
				</div>
			</div>
			
			<?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
				<div class="form-group">
					<div class="col-md-12">
						<div class="checkbox checkbox-primary pull-left p-t-0">
							<input id="checkbox-signup" type="checkbox">
							<label for="checkbox-signup"><?php echo lang('us_remember_note'); ?></label>
						</div>
						<a href="<?php echo site_url('forgot_password'); ?>" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> <?php echo lang('us_forgot_your_password');?></a> </div>
				</div>
			<?php endif; ?>
			
			<div class="form-group text-center m-t-20">
				<div class="col-xs-12">
					<input class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="submit" id="submit" value="<?php echo lang('us_login');?>" />
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>
</section>






