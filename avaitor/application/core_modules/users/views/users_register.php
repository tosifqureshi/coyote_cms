<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Coyote</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo Assets::css(null, true); ?>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico"> 
	<link rel="stylesheet" href="<?php echo Template::theme_url('css/bootstrap.min.css'); ?>" type="text/css" /> 
	<link rel="stylesheet" href="<?php echo Template::theme_url('css/bootstrap-responsive.min.css'); ?>" type="text/css" /> 
	<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-114x114.png">
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script type="text/javascript" src="<?php echo base_url(); ?>avaitor/themes/admin/js/js/jquery-1.9.1.min.js"></script>
    
<script type="text/javascript" src="<?php echo base_url(); ?>avaitor/themes/admin/js/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>avaitor/themes/admin/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>avaitor/themes/admin/js/bootstrap-datetimepicker.pt-BR.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>avaitor/themes/admin/css/bootstrap-datetimepicker.min.css" type="text/css" /> 
<link type="text/css" href="<?php echo base_url(); ?>avaitor/themes/admin/css/parsley.css" rel="stylesheet" media="all" />
<link type="text/css" href="<?php echo base_url(); ?>avaitor/themes/admin/css/css/pulse.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="<?php echo base_url(); ?>avaitor/themes/admin/js/parsley.min.js"></script>

  </head>
<?php
    $Attribute = array(
    'id'=>'login-form',
    'method'=>'post',
    'data-parsley-validate'=>'',
    'class' => "form-horizontal "
    );

?>
        
<!-- form action="<?php echo base_url('services/users/register_user'); ?>" method="post" accept-charset="utf-8"  autocomplete="off">-->
    <div class="loader1" id="loader1"><div class="hexdots-loader"> Loading… </div></div>

<?php echo form_open('',$Attribute);?>

<div class="container_main body login-wrapper"> <!-- Start of Main Container -->
<section id="login " class="registeration-page">    
<div class="row-fluid inner-wrap ">
    <div class="errorMsg" style="color:red;font-size:12px;display:none; padding: 5px 21px;"></div>
    <div class="successMsg" style="color:green;font-size:14px;font-weight:bold;display:none; padding: 5px 21px;"></div>
	<div class="control-group w10" >
          <input  class="form-control" type="text"  data-parsley-required="true" autocomplete="off"  data-parsley-required-message="First Name is required" name="ACC_FIRST_NAME" id="ACC_FIRST_NAME" value="" tabindex="1" placeholder="First Name *"  />		
          <label class="w10">First Name *</label>  
    </div>
    <div class="control-group w20">
        <input class=" form-control"  type="text" data-parsley-required="true" autocomplete="off"  data-parsley-required-message="last Name is required" name="ACC_SURNAME" id="ACC_SURNAME" value="" tabindex="1" placeholder="Last Name *"  />
        <label  class="w20">Last Name *</label> 
    </div>
    <div class="control-group w10">
        <input class=" form-control" type="email" data-parsley-type="email" data-parsley-type-message="Invalid EmailId"  type="text" name="ACC_EMAIL" data-parsley-required="true" autocomplete="off"  data-parsley-required-message="Email is required" id="ACC_EMAIL" value="" tabindex="1" placeholder="Email *"/>
        <label  class="w10" >Email *</label> 
    </div>
    
     <div class="control-group w30" id="start_date_div" style="position:relative">
        <input class="form-control" readonly="readonly" data-format="dd-MM-yyyy" data-parsley-required="true"  data-parsley-required-message="DOB is required" type="text" name="ACC_DATE_OF_BIRTH" id="ACC_DATE_OF_BIRTH" value="" tabindex="1" placeholder="Date Of Birth *"/>
        <label class="w30">Date Of Birth *</label> 
        <span id="ACC_DATE_OF_BIRTH_add_on" class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"> </i> </span>    
    </div>
    
    <div class="control-group w10">
        <input class=" form-control" type="password" name="ACC_PASSWORD" id="ACC_PASSWORD" data-parsley-required="true" autocomplete="off"  data-parsley-required-message="Password is required" value="" tabindex="1" placeholder="Password *"/>
        <label >Password *</label> 
    </div>
 
 
    <div class="control-group w20">
			<input class=" form-control" type="password" name="ACC_V_PASSWORD" data-parsley-equalto="#ACC_PASSWORD" data-parsley-equalto-message="Verify password not match" id="ACC_V_PASSWORD" data-parsley-required="true" autocomplete="off"  data-parsley-required-message="Varification password is required" value="" tabindex="1" placeholder="Verify Password *"/>
            <label >Verify Password *</label> 
	</div>
	<div class="control-group w10">
		<input class=" form-control" type="text"  data-parsley-type="number" data-parsley-type-message="Invalid Number" data-parsley-length="[10,12]" maxlength="12"  data-parsley-required-message="Invalid Number" name="ACC_MOBILE" data-parsley-required="true" autocomplete="off"  data-parsley-required-message="Mobile is required" id="ACC_MOBILE" value="" tabindex="1" placeholder="Contact Number *"/>
        <label >Contact Number *</label> 
    </div>
    <div class="control-group w20">
        <select class=" form-control text-control " name="country" data-parsley-required="true" autocomplete="off"  data-parsley-required-message="Country is required">
            <option value="">Select Country *</option>
            <?php foreach($country as $val){ ?>
                <option value="<?php echo $val->ccode; ?>"   <?php if($val->ccode == 'AS'){ echo 'selected="selected"'; } ?>  ><?php echo $val->country_name; ?></option>
            <?php } ?>
        </select>
        
	</div>
   <div style="clear:both;"></div>
   <div class="control-group ">
        <div class="">
            <!--<input id="user_terms_condition" style="float:left;margin-right:5px"  name="user_terms_condition" type="checkbox" value="1"> -->
            <div>*I accept the <a href="http://www.nightowl.com.au/terms/">Term & conditions</a> and <a href="http://www.nightowl.com.au/privacy/">Privacy policy</a> It is important that you read and understand these Terms and Conditions.
            </div>
        </div>
    </div>
	<div class="btn-group">
        <center><button class="sign_in_button" type="button" id="loginAction"><span class="button_text">Sign up</span> </button></center>
    </div>

	
</div>

</section>
</div>

</form>
    

<!--/.container-->
<script src="<?php echo Template::theme_url('js/bootstrap.min.js'); ?>" type="text/css" ></script> 

<style>
body{font-family:"Helvetica Neue",Helvetica,Helvetica,Arial,sans-serif;}
body.login-wrapper {
    margin-top:50px !important;
}

body {
    background: url(' ') center center/cover no-repeat fixed !important;
    background : #f2f2f2 !important;
}
.form-horizontal .control-group {
    margin-bottom: 30px !important;
}
.w10{
    width: 47%; float: left; margin: 0 39px 0 0;
    
}
.w20{
    width: 47%; margin-left: 05px; float: left;
}

.login-wrapper {
    max-width:66% !important;
}
input{
    width:98.7%;
	padding: 0.5em .3em;
}

select{
    width:104% !important;
    color: #555;
	height: 34px;
	font-size: .875rem;
	color: rgba(51, 51, 51, 0.7);
	font-family: "Noto Sans", sans-serif;
	padding: .5rem;
}
.width_80{
    width:80% !important;
}

input[readonly] {
    cursor: not-allowed;
    background-color: #fff;
	font-size: .875rem;
	color: rgba(51, 51, 51, 0.7);
	font-family: "Noto Sans", sans-serif;
	
}
input:focus {
    border-width: 1px;
   	box-shadow: 0 0 5px #999;
    background: #fafafa;
    border-color: #999;
    outline: 0;	
	padding: 0.5em .3em;
}

.registeration-page  h1 {
    color: #000;
    font-size: 24px;
    font-weight: normal;
    padding: 32px 0px 0px 0px;
    text-shadow: 0 0 1px #888;;
    text-align:center;
}
.sign_in_button {
    border-radius: 50px !important;
    -moz-border-radius: 7px;
    -webkit-border-radius: 7px;
    background-color: #ffffff;
    cursor: pointer;
    margin: 20px 0;
    border: 2px solid #f78f1e;
    width: 52%;
    padding: 10px 10px 10px;
    font-size: 21px;
}
.main_page {
    width: 100%;
    height: 100%;
    background: #ffffff;
    border-radius: 10px;
    margin-top: 50px;
    display: table;
    padding: 19px;
}
.icon-calendar{
    position: absolute;
    height: 34px;
    width: 44px;
    z-index: 1;
    left: 96.5%;
	top: 6px;
    background: url("../uploads/bloguser/calender_outuser.png") no-repeat;

}


.select-arrow{
	background: url(../uploads/bloguser/down-arrow_outuser.png) no-repeat;
	position: absolute;
	height: 24px;
	width: 44px;
	z-index: 1;
	left: 86%;
	top: 65.70%;
}

.form-control1{
    position:relative;
}


.form-control1{
    position:relative;
}

.form-control1+ul>li, .phonecode_error, .chosen-container+ul>li {
    background: #f5f5f5;
    border: none;
    border-radius: 5px;
    -webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.30);
    -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.30);
    box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.30);
    display: inline-block;
    padding: 10px;
    position: absolute;
    right: 0;
    line-height: 10px;
    z-index: 999;
}

input.parsley-success, select.parsley-success, textarea.parsley-success, select.parsley-success + .chosen-container {
    color: #555;;
    background-color: #fff;
}



.text_control1 {
    width: 95%;
    padding : 0 10px 0 0;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 18px;
}

.text_control1 label {
    font-size: 12px;
    font-weight: normal;
}






</style>
<script>

(function($){
    $('#loginAction').click(function(){	
		var formvalidate  = $('#login-form');
        if(formvalidate.parsley().validate()){
        
            /*
            if(!$("#user_terms_condition").is(':checked')){
                $('.errorMsg').html('Please check terms & condition before sign up');
                $('.errorMsg').show();
                $('.errorMsg').fadeOut(10000);
                return false;
            }
            */
            
            
            
            var post_url =  '<?php echo site_url('services/users/register_user') ?>';
			$('#loader1').fadeIn();
            
            $.ajax({
					url: post_url,  
					type: 'POST',
					dataType: "json",
					data: $(formvalidate).serialize(),	
					success: function(output) {
                        console.log(output);
                        if(output.result_code == 1){
                            $('#loader1').fadeOut();
                            window.top.location.href = "http://www.nightowl.com.au/app-confirmation/";
                            //$('.successMsg').html("You are registered successfully!");
                            //$('.successMsg').show();
                            //$('.successMsg').fadeOut(10000);
                           
                        }else{
                            $('#loader1').fadeOut();
                            $('.errorMsg').html(output.error_msg);
                            $('.errorMsg').show();
                            $('.errorMsg').fadeOut(10000);
                        }						
						//console.log(output.result_code);		
					}
				});
        }

    });
    
        if(jQuery('#start_date_div').length > 0)  { 
                jQuery('#start_date_div').datetimepicker({
                    pickTime: false,
                    endDate: new Date(), 
                    autoclose: true,
                });
            }
    
})(jQuery);
</script>

<style>
 
.control-group  > input {
  position: relative;
  font-size: .875rem;
  color: rgba(51, 51, 51, 0.7);
  font-family: "Noto Sans", sans-serif;
  padding: .5rem;
}
.control-group > label {
  position: absolute;
  display:none;  
}
.control-group > input:focus + ul + label {
    position: absolute;
    display: block;
    background-color: #f78f1e;
    width: 92.8%;
    color: #fff;
    padding: 0 5px;
    margin-top:-50px;
}
.w10 > input:focus + ul + label {
    width: 43.5% !important;
}

.w20 > input:focus + ul + label {
    width: 43.7% !important;
    margin-left:0;
}
.w30 > input:focus + ul + label {
    width: 101% !important;
    margin-left:0;
}
.w30{
    width: 47%; margin-left: 05px; float: left;
}





/*responsive*/

@media screen and (max-width:1024px){
   .form-control+ul>li{
       padding:6px !important;
       right:-10px !important;
   } 
    
.login-wrapper {
    max-width: 92% !important;
}
select {
    width: 104% !important;
}
.icon-calendar{
    left: 96%;
}
.w20 {
    width:47%;
    margin-left:0;
}
.w10 {
    width: 47%;
}

.w10 > input:focus + ul + label {
    width: 43.6% !important;
}

.w20 > input:focus + ul + label {
    width: 43.6% !important;
    margin-left:0;
}

.w30 > input:focus + ul + label {
    width: 101% !important;
    margin-left:0;
}
.w30{
    width: 47%; margin-left: 05px; float: left;
}





	
}
@media screen and (max-width:800px){
    select {
    width: 102% !important;
}
.w20 > input:focus + ul + label {
    width: 89.5% !important;
    margin-left:0;
}
.w10 > input:focus + ul + label {
    width: 89.5% !important;
    margin-left:0;
}
    
.w20 {
    width: 100%;
    margin-left:0;
}
.w10 {
    width: 100%;
}

.w30 > input:focus + ul + label {
    width: 101% !important;
    margin-left:0;
}
.w30{
    width: 100%; margin-left: 0;
}


}

@media screen and (max-width:640px){
    input[readonly] {
    width: 98.6%;
}
select {
    width: 104% !important;
}

.login-wrapper {
    max-width: 90% !important;
}

.icon-calendar{
    left: 95%;
}
.w20 {
    width: 100%;
    margin-left:0;
}
.w10 {
    width: 100%;
}


.w10 > input:focus + ul + label {
    width: 86.8% !important;
}

.w20 > input:focus + ul + label {
    width: 86.8% !important;
    margin-left:0;
}

.w30 > input:focus + ul + label {
    width: 101% !important;
    margin-left:0;
}
.w30{
    width: 100%; margin-left: 0;
}




	
	
}
@media screen and (max-width:480px){
    
.w10 > input:focus + ul + label {
    width: 82.8% !important;
}

.w20 > input:focus + ul + label {
    width:82.8% !important;
    margin-left:0;
}

.sign_in_button {
    width:90%;
}

    
select {
    width: 104% !important;
}
.login-wrapper {
    max-width:100% !important;
}

.icon-calendar{
    left: 93%;
}
.w20 {
    width: 100%;
    margin-left:0;
}
.w10 {
    width: 100%;
}

.w30 > input:focus + ul + label {
    width: 101% !important;
    margin-left:0;
}
.w30{
    width: 100%; margin-left: 0;
}


}

.body.login-wrapper {
    margin-top: 22px;
}
.inner-wrap {
    padding: 47px 38px 20px 30px;
}
input[readonly]{
	width:99%;
}

.loader1{ position:fixed; width:100%; height:100%; z-index:99999; background:rgba(146, 144, 144, 0.4); display:none;}
.loader1 img{
	width:10%;
	position: absolute;
   /*element can move on the screen (only screen, not page)*/
   left:50%;top:50%;
   /*set the top left corner of the element on the center of the screen*/
   transform:translate(-50%,-50%);
}

.DateOfBirthClass{
    width:100% !important;
}

</style>

<script>
    $(document).ready(function(){
        $('#ACC_DATE_OF_BIRTH').click(function(event){
            event.preventDefault();
            $('.icon-calendar').trigger('click');
        });
    });
</script>    

</html>

