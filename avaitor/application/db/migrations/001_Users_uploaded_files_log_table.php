<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Users_uploaded_files_log_table extends Migration {
	
	public function up() 
	{
		$this->dbforge->add_field(array(
              "`file_id` bigint(11) NOT NULL AUTO_INCREMENT",
              "`user_id` int(11) NOT NULL",
              "`updated_by` int(11) NOT NULL",
              "`file_name` varchar(255) NOT NULL",
              "`file_size` varchar(255) NOT NULL",
              "`file_type` varchar(255) NOT NULL",
              "`file_path` text NOT NULL",
              "`is_deleted` tinyint NOT NULL DEFAULT 0",
              "`level` int(11) NOT NULL DEFAULT 0",
              "`parent_id` int(11) NOT NULL DEFAULT 0",
              "`uploaded_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
              "`updated_on` TIMESTAMP NOT NULL DEFAULT 0"
        ));
        $this->dbforge->add_key('file_id', TRUE);
        $this->dbforge->create_table('users_file_log');
	}
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$this->dbforge->drop_table('users_file_log');
	}
	
	//--------------------------------------------------------------------
}
