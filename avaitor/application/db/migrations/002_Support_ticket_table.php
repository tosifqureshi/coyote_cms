<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Support_ticket_table extends Migration {

    /**
     * Create Ticket table
     * @id primary key 
     **/
	public function up() {
		$this->dbforge->add_field(array(
			"`id` int(10) NOT NULL AUTO_INCREMENT",
			"`subject` varchar(200) NOT NULL",
			"`body` text NOT NULL",
			"`status` varchar(200) DEFAULT NULL",
			"`department` int(11) DEFAULT NULL",
			"`clientid` int(10) NOT NULL",
			"`priority` varchar(50) DEFAULT NULL",
			"`additional` text",
			"`attachment` varchar(200) DEFAULT NULL",
			"`created` timestamp NULL DEFAULT CURRENT_TIMESTAMP",
			"`is_read` tinyint(4) NOT NULL DEFAULT '0'"              
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('tbltickets');
		
		/**
		* Create Ticket Reply table
		* @id primary key 
		**/		
		$this->dbforge->add_field(array(
			"`id` int(10) NOT NULL AUTO_INCREMENT",
			"`ticketid` int(10) NOT NULL",
			"`body` text NOT NULL",
			"`replier` varchar(200) DEFAULT NULL",
			"`replierid` int(10) DEFAULT NULL",
			"`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
			"`status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Unread;1:Read'"                          
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('tblticketreplies');
		
		/**
		* Create Department table
		* @deptid primary key 
		* default values (Technical,General) inserted on table creation 
		**/
		$this->dbforge->add_field(array(
			"`deptid` int(10) NOT NULL AUTO_INCREMENT",
			"`deptname` varchar(200) DEFAULT NULL",
			"`depthidden` varchar(10) DEFAULT NULL"                           
		));
		$this->dbforge->add_key('deptid', TRUE);
		$this->dbforge->create_table('tbldepartments');
		$this->db->query("INSERT INTO `ava_tbldepartments` (`deptid`, `deptname`, `depthidden`) VALUES(1, 'Technical', NULL),(2, 'General', NULL)");
		
		/**
		* Create Status table
		* @id primary key 
		* default values (answered,closed....) inserted on table creation 
		**/
		$this->dbforge->add_field(array(
			"`id` int(10) NOT NULL AUTO_INCREMENT",
			"`status` varchar(200) NOT NULL",                          
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('tblstatus');
		$this->db->query("INSERT INTO `ava_tblstatus` (`id`, `status`) VALUES
						(1, 'answered'),
						(2, 'closed'),
						(3, 'open'),
						(4, 'customer reply'),
						(5, 'in progress');
					");
					
		/**
		* Create Priorities table
		* @id primary key 
		* default values (low,medium,high) inserted on table creation 
		**/
		$this->dbforge->add_field(array(
			"`id` int(10) NOT NULL AUTO_INCREMENT",
			"`priority` varchar(200) DEFAULT NULL",                          
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('tblpriorities');
		$this->db->query("INSERT INTO `ava_tblpriorities` (`id`, `priority`) VALUES
						(1, 'Low'),
						(2, 'Medium'),
						(3, 'High')
					");
		
	}
	
		
	public function down(){
	$this->dbforge->drop_table('tbltickets');

	$this->dbforge->drop_table('tblticketreplies');

	$this->dbforge->drop_table('tbldepartments');

	$this->dbforge->drop_table('tblstatus');

	$this->dbforge->drop_table('tblpriorities');
	}
	
	//--------------------------------------------------------------------
}
