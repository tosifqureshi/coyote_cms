<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_File_upload_allowed_extensions_table extends Migration {
	
	public function up() 
	{
		$this->dbforge->add_field(array(
              "`ext_id` int(11) NOT NULL AUTO_INCREMENT",
              "`ext` varchar(20) NOT NULL",
			  "`is_enabled` tinyint NOT NULL DEFAULT 0"
        ));
        $this->dbforge->add_key('ext_id', TRUE);
        $this->dbforge->create_table('allowed_file_ext');
        
        $extension_options = array(array('ext'=>'odt','is_enabled'=>1),array('ext'=>'pdf','is_enabled'=>1),array('ext'=>'docx','is_enabled'=>1),array('ext'=>'doc','is_enabled'=>1),array('ext'=>'ppt','is_enabled'=>1),array('ext'=>'xls','is_enabled'=>1),array('ext'=>'xl','is_enabled'=>1),array('ext'=>'ods','is_enabled'=>1),array('ext'=>'csv','is_enabled'=>1),array('ext'=>'jpg','is_enabled'=>1),array('ext'=>'gif','is_enabled'=>1),array('ext'=>'png','is_enabled'=>1),array('ext'=>'zip','is_enabled'=>1),array('ext'=>'mov','is_enabled'=>1),array('ext'=>'mp4','is_enabled'=>1),array('ext'=>'mpg','is_enabled'=>1),array('ext'=>'3gp','is_enabled'=>1));
        $this->db->insert_batch('allowed_file_ext',$extension_options);
	}
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$this->dbforge->drop_table('allowed_file_ext');
	}
	
	//--------------------------------------------------------------------
}
