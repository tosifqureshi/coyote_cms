<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_File_version_table extends Migration {
	
	public function up() 
	{
		$this->dbforge->add_field(array(
              "`version_id` bigint(11) NOT NULL AUTO_INCREMENT",
              "`file_id` int(11) NOT NULL",
              "`version` varchar(255) NOT NULL",
              "`vcs_version` varchar(255) NOT NULL",
              "`version_dir` varchar(255) DEFAULT NULL",
              "`uploaded_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ));
        $this->dbforge->add_key('version_id', TRUE);
        $this->dbforge->create_table('file_version');
    }
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$this->dbforge->drop_table('file_version');
	}
	
	//--------------------------------------------------------------------
}
