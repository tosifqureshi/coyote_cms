<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_table_language_keys extends Migration {

    public function up()
    {
        // bubbles settings table
        $this->dbforge->add_field(array(
				"`key` varchar(255) NOT NULL ",
				"`filename` varchar(255) NOT NULL ",
				"`comment` VARCHAR(255) DEFAULT NULL"
        ));
        $this->dbforge->create_table('language_keys');
    }

    public function down()
    {
		$this->dbforge->drop_table('language_keys');
    }
}
