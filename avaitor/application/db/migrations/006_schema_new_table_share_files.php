<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_table_share_files extends Migration {

    public function up()
    {
        // share file status
        $this->dbforge->add_field(array(
			"`id` bigint(11) NOT NULL AUTO_INCREMENT",
			"`user_id` int(11) NOT NULL",
			"`shared_user_id` int(11) NOT NULL",
			"`short_url` varchar(255) NOT NULL",
			"`is_read` tinyint NOT NULL DEFAULT 0",
			"`shared_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('shared_files_status');
        
        // share file table
        $this->dbforge->add_field(array(
			"`id` bigint(11) NOT NULL AUTO_INCREMENT",
			"`user_id` int(11) NOT NULL",
			"`file_id` varchar(255) NOT NULL",
			"`short_url` varchar(255) NOT NULL",
			"`url` varchar(255) NOT NULL",
			"`share_user_ids` varchar(255) NOT NULL",
			"`is_password_set` tinyint NOT NULL DEFAULT 0",
			"`is_expiry_set` tinyint NOT NULL DEFAULT 0",
			"`expiry_date` text",
			"`shared_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('share_files');
    }

    public function down()
    {
		$this->dbforge->drop_table('shared_files_status');
		$this->dbforge->drop_table('share_files');
    }
}
