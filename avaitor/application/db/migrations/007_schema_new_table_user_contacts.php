<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_table_user_contacts extends Migration {

    public function up()
    {
        // user contacts
        $this->dbforge->add_field(array(
			"`id` bigint(11) NOT NULL AUTO_INCREMENT",
			"`user_id` int(11) NOT NULL",
			"`user_email` varchar(255) NOT NULL",
			"`invited_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('user_contacts',TRUE);
    }

    public function down()
    {
		$this->dbforge->drop_table('user_contacts');
    }
}
