<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_table_language extends Migration {

    public function up()
    {
        // user contacts
        $this->dbforge->add_field(array(
			"`lang_id` int(11) NOT NULL AUTO_INCREMENT",
			"`language` varchar(255) NOT NULL",
			"`lang_code` varchar(255) NOT NULL"
        ));
        $this->dbforge->add_key('lang_id', TRUE);
        $this->dbforge->create_table('language');
    }

    public function down()
    {
		$this->dbforge->drop_table('language');
    }
}
