<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_field_user_table extends Migration {

    public function up()
    {
        $fields = array(
                        'airdoc_email' => array('type' => 'varchar(255)', 'null' => TRUE)
		);
		$this->dbforge->add_column('users', $fields);
    }

    public function down()
    {
		$this->dbforge->drop_column('users', 'airdoc_email');
    }
}
