<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_alter_table_invite_friends extends Migration {

    public function up()
    {
        $fields = array(
                        'invite_user_ids' => array('type' => 'int')
		);
        if(!$this->db->field_exists('invite_user_ids', 'invite_friends')){
            $this->dbforge->add_column('invite_friends', $fields);
        }
        if($this->db->field_exists('url', 'share_files')){
            $this->dbforge->drop_column('share_files', 'url');
        }
    }

    public function down()
    {
		$this->dbforge->drop_column('invite_friends', 'invite_user_ids');
		$fields = array(
                        'url' => array('type' => 'varchar(255)')
		);
		$this->dbforge->add_column('share_files', $fields);
    }
}
