<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_alter_table_files_log extends Migration {

    public function up()
    {
        $fields = array(
                        'is_shared' => array('type' => 'int', 'null' => TRUE)
		);
		$this->dbforge->add_column('users_file_log', $fields);
    }

    public function down()
    {
		$this->dbforge->drop_column('users_file_log', 'is_shared');
    }
}
