<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_table_share_collaborate_log extends Migration {

    public function up()
    {
       $this->dbforge->add_field(array(
              "`collaboration_id` bigint(11) NOT NULL AUTO_INCREMENT",
              "`owner_id` int(11) NOT NULL",
              "`share_user_id` int(11) NOT NULL",
              "`file_id` int(11) NOT NULL",
              "`is_accepted` tinyint NOT NULL DEFAULT 0",
              "`shared_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"
        ));
        $this->dbforge->add_key('collaboration_id', TRUE);
        $this->dbforge->create_table('share_collaborate_members');
    }

    public function down()
    {
		$this->dbforge->drop_table('share_collaborate_members');
    }
}
