<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_schema_new_section_id_in_pages_table extends Migration {

    public function up()
    {
        $fields = array(
            'section_id' => array('type' => 'int')
		);
        if(!$this->db->field_exists('section_id', 'pages')){
            $this->dbforge->add_column('pages', $fields);
        }
    }

    public function down()
    {
        if($this->db->field_exists('section_id', 'pages')){
            $this->dbforge->drop_column('pages', 'section_id');
        }
    }
}
