<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Rename_modulebuilder extends Migration {
	
	public function up() 
	{
		$this->db->where('name', 'Core.Modulebuilder.View')
				 ->set('name', 'Core.Builder.View')
				 ->update('permissions');
	}
	
	//--------------------------------------------------------------------
	
	public function down() 
	{
		$this->db->where('name', 'Core.Builder.View')
				 ->set('name', 'Core.Modulebuilder.View')
				 ->update('permissions');
	}
	
	//--------------------------------------------------------------------
}