<?php 
function check_login(){
	$CI = &get_instance();
	// Auth setup
	$CI->load->model('users/User_model', 'user_model');
    if(!$CI->auth->is_logged_in()){
		if (!$CI->input->is_ajax_request()) {
         redirect(base_url());   
        }
        else
        {
			echo json_encode(array('logout_url'=>base_url(),'logout_message'=>lang('session_expired'),'btn'=>lang('bf_action_login')));die;
		}
    }
	else
	{
	 return TRUE;
	}
}

function check_packages($dealer_id='',$package_id=''){
	$CI = &get_instance();
	$CI->load->model('dealer_model');
	return $CI->dealer_model->check_package($dealer_id, $package_id);
}

function isSuperAdmin(){
	$CI = &get_instance();
	// Auth setup
	$CI->load->model('users/User_model', 'user_model');
	if(check_login()){
		$CI->current_user = $CI->user_model->find($CI->auth->user_id());
		if((int)$CI->current_user->role_id==1)
		{
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}	
}
 function isAdmin(){
		$CI = &get_instance();
		// Auth setup
		$CI->load->model('users/User_model', 'user_model');
		if(check_login()){
			$CI->current_user = $CI->user_model->find($CI->auth->user_id());
			if((int)$CI->current_user->role_id==2)
			{
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}	
}

/* Get all uread messages for logged in user */
if(!function_exists('get_unreadmails')){
function get_unreadmails($clientid=0){	
	$CI = & get_instance();    
	$CI->load->model('tickets/Tickets_model', 'tickets_model');
	$result = $CI->tickets_model->get_unreadmails($clientid);
	  if(!empty($result)){
		 return $result; 	 
    }else {
		    return false;
		 }
	}
 }
/* Get all uread messages for admin */
if(!function_exists('get_admin_messages')){
function get_admin_messages($user_id=0){	
	$CI = & get_instance();    
	$CI->load->model('tickets/Tickets_model', 'tickets_model');
	$new_mails='';
	$result = $CI -> tickets_model -> admin_mail($user_id);
	$reply =  $CI -> tickets_model -> get_admin_unreadmails($user_id);
	if( (!empty($result) && is_array($result)) && (!empty($reply) && is_array($reply) )){
		$new_mails = array_merge($result,$reply);		
		}else if( !empty($result) && is_array($result)) {
			$new_mails = $result;		
		}else if( !empty($reply) && is_array($reply)) {
			$new_mails = $reply;
		}
		 		
	 if(!empty($new_mails)){
		 asort($new_mails);	
		 return $new_mails; 	 
    }else {
		    return false;
		 }
	}		
}
/* Time Ago */
if(!function_exists('get_timeago')){
 function get_timeago($ptime,$type=''){
		$etime = time() - $ptime;
		if( $etime < 1 ){
			return 'less than 1 second ago';
		}
		$a = array(	12 * 30 * 24 * 60 * 60	=>  'year',
					30 * 24 * 60 * 60		=>  'month',
					24 * 60 * 60			=>  'day',
					60 * 60				=>  'hour',
					60					=>  'minute',
					1					=>  'second'
		);
						
		foreach( $a as $secs => $str ){
			$d = $etime / $secs;
			
			if($type==''){				
				if( $d >= 1 ){
					$r = round( $d );
					
					if($str=='day' && $r == 1 ){
						return 'Yesterday at '.date('h:i',$ptime);
					}elseif($str=='hour'||$str=='minute'||$str=='second'){
						return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
					}else{					
						return date('F j , ',$ptime).' '.date(' g:i a',$ptime);						
					}				
				}
			}else{
				if( $d >= 1 ){
					$r = round( $d );					
					if($str=='hour'){
						return true;
					}else{					
						return false;
					}				
				}				
			}
		}
	}
 }
 
 if(!function_exists('format_text')){
  function format_text($field='',$size=20){
	 if(!empty($field)){ 
	  if(strlen($field)>$size){		
			return substr($field,0,$size).'.....';
		}else{
			return $field;
			 }
	  }else{
		  return false;
	      } 
	  }
}

if(!function_exists('getSmtpDetails')){
	function getSmtpDetails(){
	$CI = & get_instance();    
	$CI->load->model('tickets/Tickets_model', 'tickets_model');
	$result = $CI->tickets_model->getMailSettings();
	
	if(isset($result) && is_array($result) && count($result)>0){		
		$emailinfo = array();
		foreach ($result as $email){			  
		$emailinfo[$email->name] = $email->value;
		}
		return $emailinfo;		
		}
	return false; 
	
	}	
}

	if(!function_exists('is_memcache_on')){
		function is_memcache_on()
		{
			$CI = & get_instance();
			$memcache_conf = $CI->config->item('memcached');
			if($memcache_conf['is_on'])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	if(!function_exists('change_status')){
		function change_status($id){
		if($id){
		$CI = & get_instance();    
		$CI->load->model('tickets_model');
		$result = $CI->tickets_model->change_status($id);		
		}else {
				return false;
			 }
		}
	}
	
	/*
	* @function : To get upload log of a user(total upload size,files,directories)
	* @param	: user_id(int)
	* @output	: array() 
	*/
	if(!function_exists('get_user_upload_log')){
		function get_user_upload_log($user_id){
			if($user_id)
			{
				$CI = & get_instance();    
				$CI->load->model('files/files_model');
				$result = $CI->files_model->get_user_upload_log($user_id); // uploaded files by user
				$result1 = $CI->files_model->get_user_folder_count($user_id); // created folder of user
				$result2 = $CI->files_model->get_user_delete_files($user_id); // deleted files by user 
				if(!empty($result))
				{
					$total_upload_size =	format__size_mb($result->total_upload_size);					
					$total_folders =	(!empty($result1)) ? $result1->total_folders: 0 ;
					$total_deleted_files =	(!empty($result2)) ? $result2->total_deleted_files: 0 ;
										
					return array('total_upload_size'=>$total_upload_size,'total_uploaded_files'=>$result->total_uploaded_files,'created_folders'=>$total_folders,'deleted_files'=>$total_deleted_files);
				}
			}
			else 
			{
					return false;
			}
		}
	}
	
	/*
	* Function to format file size
	* @input  : numeric(size)
	* @output : string
	*/
	function format_file_size($size)
	{
		$file_size = ($size/1000);
		if($file_size>128)
		{
			$file_size = number_format(($file_size/1024),2).' MB';
		}
		else
		{
			$file_size = $file_size.' KB';
		}
		return $file_size;
	}
	
	/*
	* @function : Get all added pages from admin area
	* @output	: array() 
	*/
	if(!function_exists('get_pages')){
		function get_pages(){			
			$CI = & get_instance();    
			$CI->load->model('page_model');
			$result = $CI->page_model->get_pages();
			if(!empty($result))
			{					
				return $result;
			}			
			return false;
		}
	}
	
	/*
	* Function to format date
	* @input  : date,format
	* @output : date
	*/
	if ( ! function_exists('dateFormatView')){
		function dateFormatView($time = '',$fmt = 'Y/m/d')
		{	
			if ( ! isset($time))
			{
				return FALSE;
			}
			return date($fmt, strtotime($time));
		}
	}
	
	/*
	* Function to get percentage
	* @input  : numeric(total,filesize)
	* @output : string
	*/
	if ( ! function_exists('get_percentage')){
		function get_percentage($total, $number)
		{
			if ( $total > 0 ) {
				return round($number / ($total / 100),2);
			} else {
				return 0;
			}
		}
   }
	
	/*
	* Function to format file size in MB
	* @input  : numeric(size)
	* @output : string
	*/
	function format__size_mb($size)
	{
		$file_size = ($size/1000);
		if($file_size>128)
		{
			$file_size = number_format(($file_size/1024),2).' MB';
		}
		else
		{
			$file_size = '0.2 KB';
		}
		return $file_size;
	}
	
	
	function get_user_id($email=''){
	$CI = &get_instance();	
	$CI->load->model('users/User_model', 'user_model');	
	$user = $CI->user_model->find_by('email',$email);		
		if((int)$user->id!='')
		{
			return $user->id;
		}else{
			return false;
		}
	}	

	function get_timeago($ptime,$type=''){
		$etime = time() - $ptime;
		if( $etime < 1 ){
			return 'less than 1 second ago';
		}
		$a = array(	12 * 30 * 24 * 60 * 60	=>  'year',
					30 * 24 * 60 * 60		=>  'month',
					24 * 60 * 60			=>  'day',
					60 * 60				=>  'hour',
					60					=>  'minute',
					1					=>  'second'
		);
						
		foreach( $a as $secs => $str ){
			$d = $etime / $secs;
			
			if($type==''){				
				if( $d >= 1 ){
					$r = round( $d );
					
					if($str=='day' && $r == 1 ){
						return 'Yesterday at '.date('h:i',$ptime);
					}elseif($str=='hour'||$str=='minute'||$str=='second'){
						return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
					}else{					
						return date('F j , ',$ptime).' '.date(' g:i a',$ptime);						
					}				
				}
			}else{
				if( $d >= 1 ){
					$r = round( $d );					
					if($str=='hour'){
						return true;
					}else{					
						return false;
					}				
				}				
			}
		}
	}
	
/* Get all uread messages for logged in user */
if(!function_exists('get_unread_shared_file')){
function get_unread_shared_file($userId=0){	
	$CI = & get_instance();    
	$CI->load->model('share/url_model', 'share_model');
	$result = $CI->share_model->get_unread_shared_file($userId);
	  if(!empty($result)){
		 return $result; 	 
    }else {
		    return false;
		 }
	}
 }

/*-------------------------------------
| @Function - To set user's selected language
| @Input  	: string  
| @Output 	: bool
-------------------------------------*/
if(!function_exists('set_user_language')){
	function set_user_language($language){
		$CI = & get_instance();		
		$language		=	explode("#",$language);
		$CI->session->set_userdata('language_code',$language[2]);
		$CI->session->set_userdata('language',$language[1]);
		$CI->session->set_userdata('language_id',$language[0]);
		$CI->config->set_item('language', $language[1]);
		return true;
	}
}


/*-------------------------------------
| @Function - To get all languages
| @Input  	:   
| @Output 	:   array/bool
-------------------------------------*/
if(!function_exists('get_all_languages')){
	function get_all_languages()
	{ 
		$CI = & get_instance();
		$CI->load->model('user_model');
		$result = $CI->user_model->get_all_language();
		if(!empty($result))
		{
			return $result; 	 
		}
		else
		{
			return false;
		}
	}
}


/*-------------------------------------
| @Function : To get base path/remote path(upload directory)  
| @Input  	:   
| @Output 	: array/bool
-------------------------------------*/
if(!function_exists('get_upload_path')){
	function get_upload_path($path_type='',$user_id=0)
	{ 
		if($path_type=='remote')
		{
			return base_url().'uploads/'.encode($user_id).'/';
		}
		else
		{
			return FCPATH.'uploads/'.encode($user_id).'/';
		}
	}
}

/*-------------------------------------
| @Function : Get user id by email
| @Input  	: string 
| @Output 	: user_id
-------------------------------------*/
if(!function_exists('get_id_by_email')){
	function get_id_by_email($email='')
	{ 
		$CI = & get_instance();
		$CI->load->model('user_model');
		return $result = $CI->user_model->get_id_by_email($email);
	}
}

/*-------------------------------------
| @Function : Get user name by id
| @Input  	: string 
| @Output 	: username
-------------------------------------*/
if(!function_exists('get_name_by_id')){
	function get_name_by_id($id='')
	{ 
		$CI = & get_instance();
		$CI->load->model('user_model');
		return $result = $CI->user_model->get_name_by_id($id);
	}
}


/*-------------------------------------
| @Function : Get user shared file count
| @Input  	: user id 
| @Output 	: array
-------------------------------------*/
if(!function_exists('shared_pending_file')){
function shared_pending_file($userId=0){	
	$CI = & get_instance();    
	$CI->load->model('share/url_model', 'share_model');
	$result = $CI->share_model->shared_pending_file($userId);
	//print_r($result);
	  if(!empty($result)){
		 return $result; 	 
    }else {
		    return false;
		 }
	}
 }


/*-------------------------------------
| @Function : Get user unread mail count
| @Input  	: user name,password,host name (webmail) 
| @Output 	: count
-------------------------------------*/
if(!function_exists('user_unread_count')){
function user_unread_count($username='',$password='',$host="airdoc2.cdnsolutionsgroup.com"){
	
	if (!extension_loaded('imap'))
	{		
		return false;
		
	}else{
	
			$mbox = imap_open("{" . $host . ":143/novalidate-cert}", $username, decode($password), OP_HALFOPEN);
			//or die("can't connect: " . imap_last_error());			
			
			$status = imap_status($mbox, "{". $host ."}INBOX", SA_ALL);
			if (!empty($status)) {
				return $status->unseen;
			} else {
			//echo "imap_status failed: " . imap_last_error() . "\n";
				return false;
			}
				imap_close($mbox);	
		}
	}
 }


/*-------------------------------------
| @Function : Get user webmail email & password
| @Input  	: user id 
| @Output 	: array
-------------------------------------*/
if(!function_exists('get_webmail_info')){
	function get_webmail_info($userId=0){	
	$CI = & get_instance();    
	$CI->load->model('users/user_model', 'user_model');
	$result = $CI->user_model->get_webmail_user_info($userId);
	//print_r($result);
	  if(!empty($result)){
		 return $result; 	 
    }else {
		    return false;
		 }
	}
 }
 
 /*-------------------------------------
| @Function : Get user shared members list
| @Input  	: fileId
| @Output 	: array
-------------------------------------*/
if(!function_exists('get_file_owner')){
	function get_file_owner($fileId=0){	
	$CI = & get_instance();    
	$CI->load->model('share/url_model', 'url_model');
	$result = $CI->url_model->get_file_owner($fileId);	
	  if(!empty($result)){
		 $name = get_name_by_id($result); 	 
		 return $name;		 
    }else {
		    return false;
		 }
	}
 }
 
/*-------------------------------------
| @Function : Get store name from store id
| @Input  	: store_id
| @Output 	: array
-------------------------------------*/
if(!function_exists('get_store_name')){
	function get_store_name($store_id=0){	
		$CI = & get_instance();    
		$CI->load->model('offers_model');
		$result = $CI->offers_model->get_store_name($store_id);	
		if(!empty($result)){ 
			return $result;		 
		}else {
		    return false;
		 }
	}
 }
 
/*-------------------------------------
| @Function : Get date diffrence
| @Input  	: start_date, end_date
| @Output 	: array
-------------------------------------*/
if(!function_exists('get_date_diffrence')) {
	function get_date_diffrence($time1,$time2 ,$precision = 6,$segment_count=0) {
		
		$current_date = date('Y-m-d H:i:s'); // set current date time
		$diffrence_type = 'Start from';
		
		if(strtotime($current_date) > strtotime($time1) ) {
			$diffrence_type = 'Remaining Time';
			$time1 = $current_date;
		} else {
			$time2 = $time1;
			$time1 = $current_date;
			
		}
		// If not numeric then convert texts to unix timestamps
		if (!is_int($time1)) {
		  $time1 = strtotime($time1);
		}
		if (!is_int($time2)) {
		  $time2 = strtotime($time2);
		}
	 
		// If time1 is bigger than time2
		// Then swap time1 and time2
		if ($time1 > $time2) {
		  $ttime = $time1;
		  $time1 = $time2;
		  $time2 = $ttime;
		}
	 
		// Set up intervals and diffs arrays
		$intervals = array('year','month','day','hour','minute','second');
		$diffs = array();
	 
		// Loop thru all intervals
		foreach ($intervals as $interval) {
		  // Create temp time from time1 and interval
		  $ttime = strtotime('+1 ' . $interval, $time1);
		  // Set initial values
		  $add = 1;
		  $looped = 0;
		  // Loop until temp time is smaller than time2
		  while ($time2 >= $ttime) {
			// Create new temp time from time1 and interval
			$add++;
			$ttime = strtotime("+" . $add . " " . $interval, $time1);
			$looped++;
		  }
	 
		  $time1 = strtotime("+" . $looped . " " . $interval, $time1);
		  $diffs[$interval] = $looped;
		}
		
		$count = 0;
		$times = array();
		// Loop thru all diffs
		foreach ($diffs as $interval => $value) {
		  // Break if we have needed precission
		  if ($count >= $precision) {
			break;
		  }
		  // Add value and interval 
		  // if value is bigger than 0
		  if ($value > 0) {
			// Add s if value is not 1
			if ($value != 1) {
			  $interval .= "s";
			}
			// Add value and interval to times array
			$times[] = $value . " " . $interval;
			$count++;
		  }
		}
		// set time duration formate
		$time_duration = '<b>'.$diffrence_type .'</b> '.implode(", ", $times);
		if(!empty($segment_count)) {
			$time_duration = $times[0].' Left';
		}
		// Return string with times
		return $time_duration ;
	  }
 }
