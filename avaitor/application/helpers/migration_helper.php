<?php
//if(! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('import_data'))
{
	function import_data($schema)
	{
        $query = '';
        $handle = fopen($schema, "r");
        if ($handle) {
            $CI = & get_instance();
            while (!feof($handle)) {
                $query.= fgets($handle, 4096);
                if (substr(rtrim($query), -1) == ';') {
                    $CI->db->query($query);
                    $query = '';
                }
            }
            fclose($handle);
        }
	}
}

if(!function_exists('import_data'))
{
    function get_file_data($file_path='')
    {
       if($file_path!='' && file_exists($file_path)){
          $content = file_get_contents($file_path);       
          return $content;
       }
    }
}
