<?php

function inbox_count($id){
	$CI = &get_instance();
	$CI->load->model('author/package_model');
	return $CI->package_model->inbox_count($id);
}
function get_sections(){
	
	$CI = &get_instance();
	$CI->load->model('Page_model');
	//$CI->load->model('pages');
	return $CI->Page_model->get_all_sections();
}
function get_user_by_email($email){	
	$CI = &get_instance();
	$CI->load->model('Page_model');
	//$CI->load->model('pages');
	return $CI->package_model->get_user_by_email($email);
}

function sidebar_notification_forbooks($type)
{
	$CI = &get_instance();
	$CI->load->model('customer_model');
	// for  books type is books,for total books type is total,for  book_review type is book_review,,for  book_published type is book_published,for enabled books type is enabled,for disabled books type is disabled
	if($type!='book_review' &&
	 $type!='book_published')
	{	
		$data['books']= $CI->customer_model->get_books($type);
		return $data['books'];
	}
	if($type=='book_review')
	{
		$data['book_review']= $CI->customer_model->get_books('review',3);
		return $data['book_review'];
	}
	if($type=='book_published')
	{
		$data['book_published']= $CI->customer_model->get_books('review',4);
		return $data['book_published'];
	}
	
	
}

function sidebar_notification_user_authors($role_id,$active)
{
	$CI = &get_instance();
	$CI->load->model('customer_model');
	//for  books type is books,for total books type is total,for  book_review type is book_review,,for  book_published type is book_published,for enabled books type is enabled,for disabled books type is disabled
	return  $CI->customer_model->get_customers_dash($role_id,$active);
}

function get_message_count($type='')
{
	
	$CI = &get_instance();
	$CI->load->model('author/package_model');
	$id = $CI->session->userdata('user_id');
	$count="";
	if($type=="sent")
	{
		$count=	$CI->package_model->outbox_count($id);
	}
	if($type=="recevied")
	{
		$count=	$CI->package_model->inbox_count($id);
	}
	return $count;
	
}

function get_pending_author_book($id='')
{
	$CI = &get_instance();
	$CI->load->model('author/author_model');
	return $CI->author_model->get_pending_author_book($id='');
}

function get_zone_name($id)
{
	$CI = &get_instance();
	$CI->load->model('location_model');
	return $CI->location_model->get_zone($id)->name;
}
function get_order_books($id)
{
	$CI = &get_instance();
	$CI->load->model('order_model');
    $book_name = array();
	foreach($CI->order_model->get_order($id)->contents as $details){
       $book_name[] = $details['name'];
    }
    $book_name = implode(',', $book_name);
    return $book_name;
}
function get_order_isbn($id)
{
	$CI = &get_instance();
	$CI->load->model('order_model');
    $isbn = array();
	foreach($CI->order_model->get_order($id)->contents as $details){
       $isbn[] = (!empty($details['isbn']) && $details['isbn']!=0) ? $details['isbn'] : '';
    }
    $isbn = implode(',', $isbn);
    return $isbn;
}
?>
