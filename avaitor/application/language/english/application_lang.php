<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	Copyright (c) 2011 Lonnie Ezell

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

//--------------------------------------------------------------------
// ! GENERAL SETTINGS
//--------------------------------------------------------------------

$lang['bf_site_name']			= 'Site Name';
$lang['bf_site_email']			= 'Site Email';
$lang['bf_site_email_help']		= 'The default email that system-generated emails are sent from.';
$lang['bf_site_status']			= 'Site Status';
$lang['bf_online']				= 'Online';
$lang['bf_offline']				= 'Offline';
$lang['bf_top_number']			= 'Items <em>per</em> page:';
$lang['bf_top_number_help']		= 'When viewing reports, how many items should be listed at a time?';
$lang['bf_home']				= 'Home';
$lang['bf_site_information']	= 'Site Information';
$lang['bf_timezone']			= 'Timezone';
$lang['bf_language']			= 'Language';
$lang['bf_language_help']		= 'Choose the languages available to the user.';

//--------------------------------------------------------------------
// ! AUTH SETTINGS
//--------------------------------------------------------------------

$lang['bf_security']			= 'Security';
$lang['bf_login_type']			= 'Login Type';
$lang['bf_login_type_email']	= 'Email Only';
$lang['bf_login_type_username']	= 'Username Only';
$lang['bf_allow_register']		= 'Allow User Registrations?';
$lang['bf_login_type_both']		= 'Email or Username';
$lang['bf_use_usernames']		= 'User display across Avaitor:';
$lang['bf_use_own_name']		= 'Use Own Name';
$lang['bf_allow_remember']		= 'Allow \'Remember Me\'?';
$lang['bf_remember_time']		= 'Remember Users For';
$lang['bf_week']				= 'Week';
$lang['bf_weeks']				= 'Weeks';
$lang['bf_days']				= 'Days';
$lang['bf_username']			= 'Username';
$lang['bf_password']			= 'Password';
$lang['bf_password_confirm']	= 'Confirm Password';
$lang['bf_display_name']		= 'Display Name';
$lang['bf_firstname']			= 'First Name';
$lang['bf_lastname']            = 'Last Name';
//--------------------------------------------------------------------
// ! CRUD SETTINGS
//--------------------------------------------------------------------

$lang['bf_home_page']			= 'Home Page';
$lang['bf_pages']				= 'Pages';
$lang['bf_enable_rte']			= 'Enable RTE for pages?';
$lang['bf_rte_type']			= 'RTE Type';
$lang['bf_searchable_default']	= 'Searchable by default?';
$lang['bf_cacheable_default']	= 'Cacheable by default?';
$lang['bf_track_hits']			= 'Track Page Hits?';

$lang['bf_action_save']			= 'Save';
$lang['bf_action_delete']		= 'Delete';
$lang['bf_action_cancel']		= 'Cancel';
$lang['bf_action_download']		= 'Download';
$lang['bf_action_preview']		= 'Preview';
$lang['bf_action_search']		= 'Search';
$lang['bf_action_purge']		= 'Purge';
$lang['bf_action_restore']		= 'Restore';
$lang['bf_action_show']			= 'Show';
$lang['bf_action_login']		= 'Sign In';
$lang['bf_action_logout']		= 'Sign Out';
$lang['bf_actions']				= 'Actions';
$lang['bf_clear']				= 'Clear';
$lang['bf_action_list']			= 'List';
$lang['bf_action_create']		= 'Create';
$lang['bf_action_ban']			= 'Ban';

//--------------------------------------------------------------------
// ! SETTINGS LIB
//--------------------------------------------------------------------

$lang['bf_do_check']			= 'Check for updates?';
$lang['bf_do_check_edge']		= 'Must be enabled to see bleeding edge updates as well.';

$lang['bf_update_show_edge']	= 'View bleeding edge updates?';
$lang['bf_update_info_edge']	= 'Leave unchecked to only check for new tagged updates. Check to see any new commits to the official repository.';

$lang['bf_ext_profile_show']	= 'Does User accounts have extended profile?';
$lang['bf_ext_profile_info']	= 'Check "Extended Profiles" to have extra profile meta-data available option(wip), omiting some default Avaitor fields (eg: address).';

$lang['bf_yes']					= 'Yes';
$lang['bf_no']					= 'No';
$lang['bf_none']				= 'None';
$lang['bf_id']					= 'ID';

$lang['bf_or']					= 'or';
$lang['bf_size']				= 'Size';
$lang['bf_files']				= 'Files';
$lang['bf_file']				= 'File';

$lang['bf_with_selected']		= 'With selected';

$lang['bf_env_dev']				= 'Development';
$lang['bf_env_test']			= 'Testing';
$lang['bf_env_prod']			= 'Production';

$lang['bf_show_profiler']		= 'Show Admin Profiler?';
$lang['bf_show_front_profiler']	= 'Show Front End Profiler?';

$lang['bf_cache_not_writable']  = 'The application cache folder is not writable';

$lang['bf_password_strength']			= 'Password Strength Settings';
$lang['bf_password_length_help']		= 'Minimum password length e.g. 8';
$lang['bf_password_force_numbers']		= 'Should password force numbers?';
$lang['bf_password_force_symbols']		= 'Should password force symbols?';
$lang['bf_password_force_mixed_case']	= 'Should password force mixed case?';
$lang['bf_password_show_labels']	    = 'Display password validation labels';

//--------------------------------------------------------------------
// ! USER/PROFILE
//--------------------------------------------------------------------

$lang['bf_user']				= 'User';
$lang['bf_users']				= 'Users';
$lang['bf_description']			= 'Description';
$lang['bf_email']				= 'Email';
$lang['bf_user_settings']		= 'My Profile';
$lang['bf_admin']               = 'Admin';

//--------------------------------------------------------------------
// !
//--------------------------------------------------------------------

$lang['bf_both']				= 'both';
$lang['bf_go_back']				= 'Go Back';
$lang['bf_new']					= 'New';
$lang['bf_required_note']		= 'Required fields are in <b>bold</b>.';
$lang['bf_form_label_required'] = '<span class="required">*</span>';

//--------------------------------------------------------------------
// MY_Model
//--------------------------------------------------------------------
$lang['bf_model_db_error']		= 'DB Error: ';
$lang['bf_model_no_data']		= 'No data available.';
$lang['bf_model_invalid_id']	= 'Invalid ID passed to model.';
$lang['bf_model_no_table']		= 'Model has unspecified database table.';
$lang['bf_model_fetch_error']	= 'Not enough information to fetch field.';
$lang['bf_model_count_error']	= 'Not enough information to count results.';
$lang['bf_model_unique_error']	= 'Not enough information to check uniqueness.';
$lang['bf_model_find_error']	= 'Not enough information to find by.';
$lang['bf_model_bad_select']	= 'Invalid selection.';

//--------------------------------------------------------------------
// Contexts
//--------------------------------------------------------------------
$lang['bf_no_contexts']			= 'The contexts array is not properly setup. Check your application config file.';
$lang['bf_context_content']		= 'Content';
$lang['bf_context_reports']		= 'Reports';
$lang['bf_context_settings']	= 'Settings';
$lang['bf_context_developer']	= 'Developer';
$lang['bf_context_acart']	    = 'Acart';

//--------------------------------------------------------------------
// Activities
//--------------------------------------------------------------------
$lang['bf_act_settings_saved']	= 'App settings saved from';
$lang['bf_unauthorized_attempt']= 'unsuccessfully attempted to access a page which required the following permission "%s" from ';

$lang['bf_keyboard_shortcuts']		= 'Available keyboard shortcuts:';
$lang['bf_keyboard_shortcuts_none']	= 'There are no keyboard shortcuts assigned.';
$lang['bf_keyboard_shortcuts_edit']	= 'Update the keyboard shortcuts';

//--------------------------------------------------------------------
// Common
//--------------------------------------------------------------------
$lang['bf_question_mark']	      = '?';
$lang['bf_language_direction']	= 'ltr';
$lang['log_intro']              = 'These are your log messages';

//--------------------------------------------------------------------
// Login
//--------------------------------------------------------------------
$lang['bf_action_register']		= 'Sign Up';
$lang['bf_forgot_password']		= 'Forgot your password?';
$lang['bf_remember_me']			= 'Remember me';

//--------------------------------------------------------------------
// Password Help Fields to be used as a warning on register
//--------------------------------------------------------------------
$lang['bf_password_number_required_help']  = 'Password must contain at least 1 punctuation mark.';
$lang['bf_password_caps_required_help']    = 'Password must contain at least 1 capital letter.';
$lang['bf_password_symbols_required_help'] = 'Password must contain at least 1 symbol.';

$lang['bf_password_min_length_help']       = 'Password must be at least %s characters long.';
$lang['bf_password_length']                = 'Password Length';

//--------------------------------------------------------------------
// User Meta examples
//--------------------------------------------------------------------

$lang['user_meta_street_name']	= 'Street Name';
$lang['user_meta_type']			= 'Type';
$lang['user_meta_country']		= 'Country';
$lang['user_meta_state']		= 'State';

// Activation
//--------------------------------------------------------------------
$lang['bf_activate_method']			= 'Activation Method';
$lang['bf_activate_none']			= 'None';
$lang['bf_activate_email']			= 'Email';
$lang['bf_activate_admin']			= 'Admin';
$lang['bf_activate']				= 'Activate';
$lang['bf_activate_resend']			= 'Resend Activation';
$lang['bf_username_exists']			= 'User name is already in use';
$lang['bf_email_registered']			= 'Email already in use ';

$lang['bf_reg_complete_error']		= 'An error occurred completing your registration. Please try again or contact the site administrator for help.';
$lang['bf_reg_activate_email'] 		= 'An email containing your activation code has been sent to [EMAIL].';
$lang['bf_reg_activate_admin'] 		= 'You will be notified when the site administrator has approved your membership.';
$lang['bf_reg_activate_none'] 		= 'Please login to begin using the site.';
$lang['bf_user_not_active'] 		= 'User account is not active.';
$lang['bf_login_activate_title']	= 'Need to activate your account?';
$lang['bf_login_activate_email'] 	= '<b>Have an activation code to enter to activate your membership?</b> Enter it on the [ACCOUNT_ACTIVATE_URL] page.<br /><br />    <b>Need your code again?</b> Request it again on the [ACTIVATE_RESEND_URL] page.';
$lang['acart_menu_main_navigation'] = 'Main navigation';
$lang['acart_menu_dashboard'] = 'Dashboard';
$lang['acart_menu_messages'] = 'Message Communication';
$lang['acart_menu_reports'] = 'Reports';
$lang['acart_menu_activities'] = 'Activities';
$lang['acart_menu_cart_admin'] = 'Cart Admin';
$lang['acart_menu_settings'] = 'Settings';
$lang['acart_menu_beacon_settings'] = 'Beacon Settings';
$lang['acart_menu_cart_settings'] = 'Cart Settings';
$lang['acart_menu_tablet_settings'] = 'Tablet Settings';
$lang['acart_menu_kiosk_settings'] = 'Kiosk Settings';
$lang['acart_menu_kiosk_licence_request'] = 'Kiosk Licence Management';
$lang['acart_menu_keyboardshortcut'] = 'Keyboard Shortcut';
$lang['acart_menu_permissions'] = 'Permissions';
$lang['acart_menu_roles'] = 'Roles';
$lang['acart_menu_users'] = 'Users';
$lang['acart_menu_emailqueue'] = 'E-mail Queue';
$lang['acart_menu_templates'] = 'Templates';
$lang['acart_menu_viewqueue'] = 'View Queue';
$lang['acart_menu_developer'] = 'Developer';
$lang['acart_menu_code_builder'] = 'Code Builder';
$lang['acart_menu_database_tools'] = 'Database Tools';
$lang['acart_menu_maintenance'] = 'Maintenance';
$lang['acart_menu_backups'] = 'Backups';
$lang['acart_menu_migrations'] = 'Migrations';
$lang['acart_menu_logs'] = 'Logs';
$lang['acart_menu_sysinfo'] = 'System Information';
$lang['acart_menu_translate'] = 'Translate';
$lang['acart_menu_acart'] = 'Acart';
$lang['acart_menu_sales'] = 'Sales';
$lang['acart_menu_sales_report'] = 'Sales Report';
$lang['acart_menu_sales_details'] = 'Sales Details';
$lang['acart_menu_publish_reports'] = 'Book Publish Report';
$lang['acart_details'] = 'Get Details';
$lang['acart_menu_customers'] = 'Customers';
$lang['acart_menu_reports'] = 'Reports';
$lang['acart_menu_orders'] = 'Orders';
$lang['acart_menu_coupons'] = 'Coupons';
$lang['acat_menu_giftcards'] = 'Gift Cards';
$lang['acart_menu_book_cover'] = 'Book Cover';
$lang['acart_menu_catalog'] = 'Catalog';
$lang['acart_menu_categories'] = 'Categories';
$lang['acart_menu_products'] = 'Products';
$lang['acart_menu_content'] = 'Content';
$lang['acart_menu_pages'] = 'Pages';
$lang['acart_menu_blog'] = 'Blog';
$lang['acart_menu_slider'] = 'Manage Slider';
$lang['acart_manage_slider'] = 'Manage Slider';
$lang['acart_menu_frontend'] = 'Visit Website';
$lang['acart_menu_bookpricing_calculator'] = 'Book Pricing Calculator';
$lang['acart_book_calculator'] = 'Book Calculator';
$lang['acart_menu_locations'] = 'Locations';
$lang['acart_menu_manage_adv'] = 'Manage Advertisements';
$lang['acart_menu_emailsignup'] = 'E-mail Signup';
$lang['acart_menu_footer_links'] = 'Footer Links';
$lang['acart_menu_author_admin'] = 'Author Admin';
$lang['acart_menu_author'] = 'Registered Authors';
$lang['acart_menu_packcat'] = 'Package Category';
$lang['acart_menu_packages'] = 'Package';
$lang['acart_menu_packorder'] = 'Package Orders';
$lang['acart_menu_sendmsg'] = 'Send Message';
$lang['acart_menu_recmsg'] = 'Recieved Messages';
$lang['acart_menu_sentmsg'] = 'Sent Messages';
$lang['acart_book_cover'] = 'Book Cover'; 
$lang['acart_menu_publish_book'] = 'Publish Book';
$lang['bf_action_logout_header'] = 'Logout';
$lang['bf_user_settings_header'] = 'Profile';
$lang['upload_settings'] = 'Upload Settings';

$lang['restore_filesystem'] = 'Restore Filesystem';
$lang['upload'] = 'Upload';
$lang['s3_setting'] = 'Amazon S3 Settings';
$lang['access_key'] = 'Access Key';
$lang['secret_key'] = 'Secret Key';
$lang['amazon_host'] = 'Amazon Host';
$lang['bucketName'] = 'Bucket Name';
$lang['upload_on_s3'] = 'Upload On S3';
$lang['profile'] = 'My Profile';
$lang['folder_doesnt_exists'] = 'Folder does not exists!';
$lang['folder_empty'] = 'Folder is empty!';
$lang['session_expired'] = 'Your session has expired, Please Log-In again.';
$lang['restore_users_file_system'] = 'Restore Users File System';
$lang['search_user'] = 'Search User';
$lang['no_record_found'] = 'No Record Found!';
$lang['language_create_lang'] = 'Add New Language';
$lang['manage_language'] = 'Manage Language';

/*added by P : Start*/
$lang['acart_menu_campaign'] = 'Campaigns';
$lang['acart_menu_offers'] = 'Offers';
$lang['acart_menu_in_store_offers'] = 'In Stores Offers';
$lang['acart_menu_beacon_offers'] = 'Beacon Offers';
$lang['acart_menu_beacon_management'] = 'Beacon';
$lang['acart_menu_beacon_normal_offers'] = 'Normal Beacon Offers';
$lang['acart_menu_beacon_fallback_offers'] = 'Fallback Beacon Offers';
$lang['acart_menu_vip_offers'] = 'VIP Offers';
$lang['acart_menu_simple_offers'] = 'VIP Push Offers';
$lang['target_notification'] = 'Target Notification';
$lang['acart_menu_beacons'] = 'Beacons';
$lang['acart_menu_promotion'] = 'Promotions';
$lang['acart_menu_app_promotion'] = 'App Promotions';
$lang['acart_menu_kiosk_promotion'] = 'Kiosk Promotions';
$lang['acart_menu_web_promotion'] = 'Web Promotions';
$lang['acart_menu_event'] = 'Events';
$lang['acart_menu_loyalty'] = 'Produt Loyalty Program';
$lang['acart_menu_competition_management'] = 'Competition';
$lang['acart_menu_competition'] = 'Competitions';
$lang['acart_menu_edm'] = 'Email Direct Marketing';
$lang['acart_menu_lucky_draw'] = 'Lucky Draw Winners';
$lang['acart_menu_leader_board_winners'] = 'Leader Board Winners';
$lang['acart_menu_deviceaddress'] = 'Device Management';
$lang['acart_menu_scratchnwin'] = 'Scratch&Win';
$lang['acart_menu_campaigns'] = 'Campaigns';
$lang['menu_points_n_reward'] = 'Reward';
$lang['menu_reward_rule'] = 'Product Reward Rules';
$lang['menu_reward_transaction'] = 'Reward Pool';
$lang['menu_reward_point_rates'] = 'Points Rates';
$lang['menu_reward_earn_rate'] = 'Earn Rate';
$lang['menu_reward_spend_rate'] = 'Spend Rate';
$lang['menu_reward_config'] = 'Reward Configuration';
$lang['menu_reward_catalogue'] = 'Reward Catalogue';
$lang['menu_reward_campaign'] = 'Reward Campaign';
$lang['menu_reward_cards'] = 'Loyalty Cards';
$lang['menu_kiosk_management'] = 'Kiosk';
$lang['menu_kiosk_companies'] = 'Kiosk Companies';
$lang['menu_kiosk_outlets'] = 'Kiosk Outlets';
$lang['menu_kiosk_tills'] = 'Kiosk Tills';
$lang['menu_kiosk_licences'] = 'Kiosk Licences';
$lang['menu_kiosk_exe_versions'] = 'Kiosk EXE Versions';
$lang['menu_kiosk_settings'] = 'Kiosk Settings';


$lang['offer_sno'] = 'S.No.';
$lang['offer_name'] = 'Offer Name';
$lang['offer_start_date'] = 'Start Date';
$lang['offer_end_date'] = 'End Date';
//$lang['offer_store'] = 'Store';
$lang['offer_status'] = 'Status';
$lang['offer_delete'] = 'Delete';
$lang['bf_add_new_admin'] = 'Add New User';
$lang['select_all'] = 'Select All';
$lang['selectConditionEntity'] = 'Select Condition Entity';
$lang['selectCards'] = 'Select Loyalty Card';
$lang['filter_by_year']		  = 'Filter By Last 5 Years : ';
$lang['scratch_n_win_chart']  = 'SCRATCH AND WIN';
$lang['selectCompanies'] 	  = '-- Select Company --';
$lang['selectOutlets'] 	  = '-- Select Outlet --';


/*added by P : End*/

