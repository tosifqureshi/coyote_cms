<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['beacons']				= 'Beacons';
$lang['add_new_beacon']		= 'Add New Beacon';
$lang['beacon_code']		= 'Beacon Code';
$lang['created_at']		    = 'Created At';
$lang['sno']				= 'S.No.';
$lang['store']				= 'Store';
$lang['status']				= 'Status';
$lang['delete']				= 'Delete';
$lang['description']		= 'Description';
$lang['message_saved_beacon']	= 'Beacon Successfully added.';
$lang['message_update_beacon']	= 'Beacon Updated added.';
$lang['message_status_beacon']	= 'Beacon Status updated successfully.';
$lang['message_delete_beacon']	= 'Beacon deleted successfully.';
$lang['add_new_beacon']			= 'Add New Beacon';
$lang['edit_beacon']			= 'Edit Beacon';
$lang['beacon_store']           = 'Store';
$lang['tooltip_beacon_store'] 	= 'Select the store for the beacon.';
$lang['tooltip_beacon_code']	= 'Enter beacon code here';
$lang['tooltip_description']	= 'Enter beacon description here';


$lang['content']				= 'Content';
$lang['attributes']				= 'Attributes';
$lang['offer_name']				= 'Name';
$lang['offer_price']			= 'Price';
$lang['offer_image']			= 'Image';
$lang['start_date']				= 'Start Date';
$lang['start_time']				= 'Start Time';
$lang['end_date']				= 'End Date';
$lang['end_time']				= 'End Time';
$lang['offer_short_description']= 'Short Description';
$lang['offer_long_description']	= 'Long Description';
$lang['offer_beacon']			= 'Beacon';

$lang['offer_banner_image']		= 'Banner Image';
$lang['form_save']				= 'Save';
$lang['form_cancel']			= 'Cancel';

$lang['subject']				= 'Subject';

$lang['offer_status_active']	= 'Active';
$lang['offer_status_deactive']	= 'Deactive';
$lang['cannot_place_offer']		= 'Can not place Order on give time.';
$lang['start_date_is_greater']	= 'Please ensure that the End Date is greater than or equal to the Start Date.';

$lang['tooltip_offer_name'] 	= 'Enter the offer name.';
$lang['tooltip_offer_price'] 	= 'Enter the offer Price.';
$lang['tooltip_offer_image'] 	= 'Click image to upload new offer image';
$lang['tooltip_offer_beacon'] 	= 'Select the beacon for the offer.';
$lang['tooltip_offer_store'] 	= 'Select the store for the offer.';
$lang['tooltip_offer_start_date'] 	= 'Select the offer\'s start date.';
$lang['tooltip_offer_end_date'] 	= 'Select the offer\'s end date.';
$lang['tooltip_offer_start_time'] 	= 'Select the offer\'s start time.';
$lang['tooltip_offer_end_time'] 	= 'Select the offer\'s end time.';
$lang['tooltip_offer_banner_image'] = 'Select offer image for banner';

$lang['add_department']		= 'Add Department';
$lang['department_name']	= 'Department Name';
$lang['save_changes']		= 'Save changes';
$lang['processing']			= 'Processing...';
$lang['close']				= 'Close';
$lang['edit_department']	= 'Edit Department';
$lang['staff_name']			= 'Staff Name';
$lang['name']				= 'Name';
$lang['action']				= 'Action';
$lang['edit']				= 'Edit';
$lang['delete']				= 'Delete';
$lang['ticket_status']		= 'Ticket Status';

$lang['customer']			= 'Customer';
$lang['department']			= 'Department';
$lang['priority']			= 'Priority';
$lang['view_ticket']		= 'Viewing Ticket #';
$lang['ticket_info']		= 'Ticket Info';
$lang['customer_email']		= 'Customer Email';
$lang['ticket_status']		= 'Ticket Status';
$lang['created_on']			= 'Created On';
$lang['department_assigned']= 'Department Assigned';
$lang['attachment']			= 'Attachment';
$lang['download']			= 'Download';
$lang['client']				= 'Client';
$lang['post_reply']			= 'Post a reply';
$lang['set_to']				= 'Set to ';


$lang['has_been_responded_by_the_client_click_follow_link'] = 'has been responded by the client. You may view the ticket by clicking on the following link';
$lang['regards']				= 'Regards';
//$lang['airdoc_team']			= 'Airdoc Team'; // comment and add by P
$lang['airdoc_team']			= 'Coyote Team';

$lang['client_email']			= 'Client Email';
$lang['hello']					= 'Hello';
$lang['your_ticket_has_been_opened_with_us']	= 'Your ticket has been opened with us.';
$lang['click_on_the_below_link_to_see_the_ticket_details']	= 'Click on the below link to see the ticket details and post additional comments.You must be logged in to view ticket ';
$lang['you_have'] = 'You have'; 
$lang['you_have_no_new_message'] = 'You have no new message'; 
$lang['see_all_messages'] = 'See All Messages'; 
$lang['file_types'] = 'File Types'; 
$lang['simple_offers']	= 'Simple Offers';
$lang['add_new_simple_offer']		= 'Add New Simple Offer';
$lang['edit_simple_offer']			= 'Edit Simple Offer';
$lang['qrcode_image'] = 'QR Code';
$lang['tooltip_barcode_image'] 	= 'Click image to upload new barcode';
$lang['message_invalid_beacon'] = 'Invalid Beacon';

$lang['beacon_setting_heading'] = 'Beacon Setting';
$lang['beacon_notification_time'] = 'Beacon Notification Time(Hour(s))';
$lang['beacon_setting_success_msg'] = 'Successfully configured';
$lang['beacon_notification_help_note'] = 'Beacon popup appears on the basis of this time duration';

$lang['cart_setting_heading'] = 'Cart Setting';
$lang['cart_setting_success_msg'] = 'Successfully configured';
$lang['cart_setting'] = 'Product Limit';
$lang['cart_notification_help_note'] = 'Product add into cart in the basis of these limit';

$lang['tablet_setting_heading'] = 'Tablet Settings';
$lang['unlock_password'] = 'Unlock Password';
$lang['app_version'] = 'Current App Version';
$lang['os_version'] = 'Current OS Version';

$lang['kiosk_setting_heading'] = 'Kiosk Settings';
$lang['kiosk_download_url'] = 'Upload EXE';
$lang['import_new_exe'] = 'Import New EXE';

$lang['employee_discount'] = 'Employee Discount(%)';
$lang['normal_discount'] = 'Normal Discount(%)';
$lang['exclude_category'] = 'Exclude Category';

