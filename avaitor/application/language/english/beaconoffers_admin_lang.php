<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['offers']					= 'Beacon\'s Offers';
$lang['content']				= 'Content';
$lang['attributes']				= 'Attributes';
$lang['offer_name']				= 'Name';
$lang['offer_price']			= 'Price';
$lang['offer_image']			= 'Image(s)';
$lang['start_date']				= 'Start Date';
$lang['start_time']				= 'Start Time';
$lang['end_date']				= 'End Date';
$lang['end_time']				= 'End Time';
$lang['offer_short_description']		= 'Short Description';
$lang['offer_long_description']			= 'Long Description';
$lang['offer_beacon']			= 'Beacon';
$lang['offer_store']			= 'Store';
$lang['offer_banner_image']		= 'Banner Image';
$lang['form_save']				= 'Save';
$lang['form_cancel']			= 'Cancel';
$lang['message_saved_offer']	= 'Offer successfully added.';
$lang['message_update_offer']	= 'Offer successfully updated.';
$lang['message_status_offer']	= 'Offer status updated successfully.';
$lang['message_delete_offer']	= 'Offer deleted successfully.';
$lang['subject']				= 'Subject';
$lang['status']					= 'Status';
$lang['offer_status_active']	= 'Active';
$lang['offer_status_deactive']	= 'Deactive';
$lang['cannot_place_offer']		= 'is already assign for given time duration.';
$lang['start_date_is_greater']	= 'Please ensure that the end date is greater than or equal to the start date.';
$lang['end_date_is_greater']	= 'End date should be greater than today\'s date.';
$lang['start_time_is_greater']	= 'Please ensure that the end time is greater than the start time.';

$lang['tooltip_offer_name'] 	= 'Enter the offer name.';
$lang['tooltip_offer_price'] 	= 'Enter the offer Price.';
$lang['tooltip_offer_image'] 	= 'Click image to upload new offer image';
$lang['tooltip_offer_beacon'] 	= 'Select the beacon for the offer.';
$lang['tooltip_offer_store'] 	= 'Select the store for the offer.';
$lang['tooltip_offer_start_date'] 	= 'Select the offer\'s start date.';
$lang['tooltip_offer_end_date'] 	= 'Select the offer\'s end date.';
$lang['tooltip_offer_start_time'] 	= 'Select the offer\'s start time.';
$lang['tooltip_offer_end_time'] 	= 'Select the offer\'s end time.';
$lang['tooltip_offer_banner_image'] = 'Select offer image for banner';


$lang['add']		= 'Add';
$lang['edit']			= 'Edit';
$lang['add_department']		= 'Add Department';
$lang['department_name']	= 'Department Name';
$lang['save_changes']		= 'Save changes';
$lang['processing']			= 'Processing...';
$lang['close']				= 'Close';
$lang['edit_department']	= 'Edit Department';
$lang['staff_name']			= 'Staff Name';
$lang['name']				= 'Name';
$lang['action']				= 'Action';
$lang['edit']				= 'Edit';
$lang['delete']				= 'Delete';
$lang['ticket_status']		= 'Ticket Status';
$lang['customer']			= 'Customer';
$lang['department']			= 'Department';
$lang['priority']			= 'Priority';
$lang['view_ticket']		= 'Viewing Ticket #';
$lang['ticket_info']		= 'Ticket Info';
$lang['customer_email']		= 'Customer Email';
$lang['ticket_status']		= 'Ticket Status';
$lang['created_on']			= 'Created On';
$lang['department_assigned']= 'Department Assigned';
$lang['attachment']			= 'Attachment';
$lang['download']			= 'Download';
$lang['client']				= 'Client';
$lang['post_reply']			= 'Post a reply';
$lang['set_to']				= 'Set to ';


$lang['has_been_responded_by_the_client_click_follow_link'] = 'has been responded by the client. You may view the ticket by clicking on the following link';
$lang['regards']				= 'Regards';
//$lang['airdoc_team']			= 'Airdoc Team'; // comment and add by P
$lang['airdoc_team']			= 'Coyote Team';

$lang['client_email']			= 'Client Email';
$lang['hello']					= 'Hello';
$lang['your_ticket_has_been_opened_with_us']	= 'Your ticket has been opened with us.';
$lang['click_on_the_below_link_to_see_the_ticket_details']	= 'Click on the below link to see the ticket details and post additional comments.You must be logged in to view ticket ';
$lang['you_have'] = 'You have'; 
$lang['you_have_no_new_message'] = 'You have no new message'; 
$lang['see_all_messages'] = 'See All Messages'; 
$lang['file_types'] = 'File Types'; 
$lang['simple_offers']	= 'VIP Push Offers';
$lang['qrcode_image'] = 'Barcode Image';
$lang['tooltip_barcode_image'] 	= 'Barcode Image';
$lang['category']			= 'Category';
$lang['tooltip_offer_category'] 	= 'Select the category for the offer.';
$lang['offer_default_image']		= 'Default';
$lang['tooltip_default_image'] 	= 'Click image to upload new default image';
$lang['date']					= 'Offer Date';
$lang['offer_day_diffrence']	= 'Availabilty';
$lang['start_at'] = 'Start at';
$lang['end_at'] = 'End at';
$lang['offer_image'] = 'Image(s)';
$lang['offer_category'] = 'Category';
$lang['closed'] = 'Closed';
$lang['offer_barcode'] = 'Barcode';
$lang['tooltip_barcode'] 	= 'Enter the offer barcode';
$lang['price']			= 'Original Price';
$lang['tooltip_original_price'] 	= 'Enter the Original Price.';
$lang['message_invalid_beacon_offer'] = 'Invalid Beacon Offer';
$lang['message_invalid_simple_offer'] = 'Invalid VIP Simple Push Offer';
$lang['add_new_beacon_offer']		= 'Add Beacon Offer';
$lang['edit_beacon_offer']			= 'Edit Beacon Offer';
$lang['add_new_simple_offer']		= 'Add VIP Offer';
$lang['edit_simple_offer']			= 'Edit VIP Offer';
$lang['beacon_already_occupied']	= 'Beacon is associated with other active beacon offer.';
$lang['beacons_already_occupied']	= 'Beacon(s) associated with another active beacon offer.';
$lang['offer_list_image']	= 'Image';
/*--- Add fallback beacon offer lang ---*/
$lang['fallback_offers']		= 'Fallback Beacon Offers';
$lang['add_new_fallback_offer']	= 'Add Fallback Offer';
$lang['edit_fallback_offer']	= 'Edit Fallback Offer';
$lang['cannot_place_offer_for_store']		= 'is already assign for given store.';


/* Target Notification */
$lang['add_new_target_notification']		= 'Add Target Notification';
$lang['edit_target_notification']			= 'Edit Target Notification'; 
$lang['target_notification']	= 'Target Notification';
$lang['add_filter_target_notification']	= 'Filter Members';
$lang['select_basket_size'] 	= 'Select the Basket Size';
$lang['basketsize'] 	= 'Basket Size';
$lang['productcount'] 	= 'Product Count';
$lang['basketsizerange'] 	= 'Basket Size Range';
$lang['productcountrange'] 	= 'Product Count';
$lang['membersearch'] 	= 'Member Search';
$lang['memberdirectsearch'] = 'Member Direct Search';
$lang['equalorgreater'] 	= 'Equal to or greater than';
$lang['equalorless'] 	= 'Equal to or less than';
$lang['product_group'] 	= 'Product Group';
$lang['select_product_group'] 	= 'Select Product Group';
$lang['show_member_filtered'] 	= 'Show Filtered Member';
$lang['form_send'] 	= 'Send';
$lang['back'] 	= 'Back';
$lang['purchase_start_time'] 	= 'Purchase start time';
$lang['purchase_end_time'] 	= 'Purchase end time';
$lang['notification_start_date'] 	= 'Notification start date';
$lang['notification_end_date'] 	= 'Notification end date';
$lang['message_saved_targetpush'] 	= 'Save target notification message';
$lang['message_update_targetpush'] 	= 'Update target notification message';
$lang['message_invalid_targetpush'] 	= 'Target Push Notification Invalid';
$lang['message_status_targetpush']	= 'Target notification status updated successfully.';
$lang['message_delete_targetpush']	= 'Target notification deleted successfully.';
$lang['no_result_found']	= 'No record found';

/* Member Listing view */
$lang['member_no']   = 'Member Id';
$lang['member_name'] = 'Member Name';
$lang['last_order_date'] = 'Last Order Date';
$lang['last_basket_size'] = 'Last Basket Size';
$lang['add_target_notification'] = 'Add Target Notification';
$lang['purchase_date_range'] = 'Purchase Date Range';
$lang['purchase_time_range'] = 'Purchase Time Range';
$lang['Filters'] = 'Filters';
$lang['Products'] = 'Products';
$lang['search_product'] = 'Product Search';
$lang['product_number'] = 'Product Number';
$lang['product_name'] = 'Product Name';
$lang['product_qty'] = 'Product Quantity';
$lang['product_level'] = 'Level';
$lang['purchasedaterangesearch'] = 'Date Range';

/* Notification Alert */
$lang['notification_alert'] = 'Notification Alert';
$lang['add_notification_alert'] = 'Add Notification Alert';

$lang['offer_detail_page_image']	= 'Detail Page Image';
$lang['is_slider_image']	= 'Is Banner Image?';


