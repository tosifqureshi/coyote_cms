<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['email_template']				= 'Email Template';
$lang['add_new_email_template']		= 'Add Email Template';
$lang['edit_email_template']		= 'Edit Email Template';
$lang['email_template_sno']			= 'S.No.';
$lang['email_template_purpose']		= 'Email Purpose';
$lang['email_template_subject']		= 'Email Subject';
$lang['email_template_template']	= 'Template';

$lang['form_save']				= 'Save';

$lang['tooltip_email_template_subject'] = 'Enter the Email subject.';
$lang['tooltip_email_template_body'] 	= 'Edit the Email template.';
$lang['tooltip_event_image'] 	= 'Click image to upload new offer image';


$lang['message_saved_event']	= 'Email Template successfully added.';
$lang['message_update_event']	= 'Email Template successfully updated.';
$lang['message_status_event']	= 'Email Template status updated successfully.';
$lang['message_delete_event']	= 'Email Template deleted successfully.';






