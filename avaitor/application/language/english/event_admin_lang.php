<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['events']				= 'Events';
$lang['add_new_event']		= 'Add Event';
$lang['edit_event']			= 'Edit Event';
$lang['event_sno']			= 'S.No.';
$lang['event_image']		= 'Image(s)';
$lang['event_banner_image']	= 'Banner Image';
$lang['event_name']			= 'Name';
$lang['event_day_diffrence']	= 'Availabilty';
$lang['event_status']		= 'Status';
$lang['action']				= 'Action';
$lang['event_at'] 			= 'Event at';
$lang['start_at'] 			= 'Start at';
$lang['end_at']				= 'End at';
$lang['event_date']			= 'Event Date';
$lang['start_date']			= 'Start Date';
$lang['end_date']			= 'End Date';
$lang['event_address']		= 'Location';
$lang['event_latitude']		= 'Latitude';
$lang['event_longitude']	= 'Longitude';

$lang['closed'] 			= 'Closed';
$lang['content']			= 'Content';
$lang['gallary_tab']			= 'Image Gallary';
$lang['event_short_description']		= 'Short Description';
$lang['event_long_description']			= 'Long Description';
$lang['form_save']				= 'Save';
$lang['form_cancel']			= 'Cancel';

$lang['tooltip_event_name'] 	= 'Enter the event name.';
$lang['tooltip_event_image'] 	= 'Click image to upload new event image';
$lang['tooltip_event_date'] 	= 'Select the event  date.';
$lang['tooltip_event_start_date'] 	= 'Select the event\'s start date.';
$lang['tooltip_event_end_date'] 	= 'Select the event\'s end date.';
$lang['tooltip_event_default_image'] = 'Select event default image';

$lang['message_saved_event']	= 'Event successfully added.';
$lang['message_update_event']	= 'Event successfully updated.';
$lang['message_status_event']	= 'Event status updated successfully.';
$lang['message_delete_event']	= 'Event deleted successfully.';






