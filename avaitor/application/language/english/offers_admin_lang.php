<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['offers']					= 'In stores Offers';
$lang['content']				= 'Content';
$lang['attributes']				= 'Attributes';
$lang['offer_name']				= 'Name';
$lang['offer_price']			= 'Price';
$lang['offer_image']			= 'Image(s)';
$lang['start_date']				= 'Start Date';
$lang['start_time']				= 'Start Time';
$lang['end_date']				= 'End Date';
$lang['end_time']				= 'End Time';

$lang['offer_short_description']		= 'Short Description';
$lang['offer_long_description']			= 'Long Description';
$lang['offer_beacon']			= 'Beacon';
$lang['offer_store']			= 'Store';
$lang['offer_default_image']	= 'Default Image';
$lang['offer_banner_image']		= 'Banner Image';
$lang['form_save']				= 'Save';
$lang['form_cancel']			= 'Cancel';
$lang['message_saved_offer']	= 'Offer successfully added.';
$lang['message_update_offer']	= 'Offer successfully updated.';
$lang['message_status_offer']	= 'Offer status updated successfully.';
$lang['message_delete_offer']	= 'Offer deleted successfully.';
$lang['message_copy_offer']		= 'Offer successfully copied.';
$lang['error_message_copy_offer']		= 'There is some error to copy offer. Please try again';
$lang['subject']				= 'Subject';
$lang['status']					= 'Status';
$lang['offer_status_active']	= 'Active';
$lang['offer_status_deactive']	= 'Deactive';
$lang['cannot_place_offer']		= 'Can not place Order on give time.';
$lang['start_date_is_greater']	= 'Please ensure that the End Date is greater than or equal to the Start Date.';

$lang['tooltip_offer_name'] 	= 'Enter the offer name.';
$lang['tooltip_offer_price'] 	= 'Enter the offer Price.';
$lang['tooltip_offer_image'] 	= 'Click image to upload new offer image';
$lang['tooltip_offer_beacon'] 	= 'Select the beacon for the offer.';
$lang['tooltip_offer_store'] 	= 'Select the store for the offer.';
$lang['tooltip_offer_start_date'] 	= 'Select the offer\'s start date.';
$lang['tooltip_offer_end_date'] 	= 'Select the offer\'s end date.';
$lang['tooltip_offer_start_time'] 	= 'Select the offer\'s start time.';
$lang['tooltip_offer_end_time'] 	= 'Select the offer\'s end time.';
$lang['tooltip_offer_default_image'] = 'Select offer default image';
$lang['tooltip_offer_banner_image'] = 'Select offer image for banner';


$lang['add']		= 'Add';
$lang['edit']			= 'Edit';
$lang['add_department']		= 'Add Department';
$lang['department_name']	= 'Department Name';
$lang['save_changes']		= 'Save changes';
$lang['processing']			= 'Processing...';
$lang['close']				= 'Close';
$lang['edit_department']	= 'Edit Department';
$lang['staff_name']			= 'Staff Name';
$lang['name']				= 'Name';
$lang['action']				= 'Action';
$lang['edit']				= 'Edit';
$lang['delete']				= 'Delete';

$lang['customer']			= 'Customer';
$lang['department']			= 'Department';
$lang['priority']			= 'Priority';
$lang['view_ticket']		= 'Viewing Ticket #';
$lang['ticket_info']		= 'Ticket Info';
$lang['customer_email']		= 'Customer Email';
$lang['ticket_status']		= 'Ticket Status';
$lang['created_on']			= 'Created On';
$lang['department_assigned']= 'Department Assigned';
$lang['attachment']			= 'Attachment';
$lang['download']			= 'Download';
$lang['client']				= 'Client';
$lang['post_reply']			= 'Post a reply';
$lang['set_to']				= 'Set to ';


$lang['has_been_responded_by_the_client_click_follow_link'] = 'has been responded by the client. You may view the ticket by clicking on the following link';
$lang['regards']				= 'Regards';
//$lang['airdoc_team']			= 'Airdoc Team'; // comment and add by P
$lang['airdoc_team']			= 'Coyote Team';

$lang['client_email']			= 'Client Email';
$lang['hello']					= 'Hello';
$lang['your_ticket_has_been_opened_with_us']	= 'Your ticket has been opened with us.';
$lang['click_on_the_below_link_to_see_the_ticket_details']	= 'Click on the below link to see the ticket details and post additional comments.You must be logged in to view ticket ';
$lang['you_have'] = 'You have'; 
$lang['you_have_no_new_message'] = 'You have no new message'; 
$lang['see_all_messages'] = 'See All Messages'; 
$lang['file_types'] = 'File Types'; 
$lang['select_all'] = 'Select All';
$lang['category'] = 'Category';
$lang['date']					= 'Offer Date';
$lang['offer_day_diffrence']	= 'Availabilty';
$lang['start_at'] = 'Start at';
$lang['end_at'] = 'End at';
$lang['offer_image'] = 'Image(s)';
$lang['offer_category'] = 'Category';
$lang['closed'] = 'Closed';
$lang['price']			= 'Original Price';
$lang['tooltip_original_price'] 	= 'Enter the Original Price.';
$lang['message_invalid_offer'] = 'Invalid In Store Offer';
$lang['add_new_offer']		= 'Add In Store Offer';
$lang['edit_offer']			= 'Edit In Store Offer';
$lang['offer_list_image']	= 'Image';
$lang['offer_detail_page_image']	= 'Detail Page Image';
$lang['is_slider_image']	= 'Is Banner Image?';


