<?php
/******************************************
US English
Admin Common Language
******************************************/

//header
$lang['common_sales']				= 'فروش';
$lang['common_orders']				= 'سفارشات';
$lang['common_customers']			= 'مشتریان';
$lang['common_groups']				= 'گروه';
$lang['common_reports']				= 'گزارش ها';
$lang['common_coupons']				= 'کوپن';
$lang['common_giftcards']			= 'کارت های هدیه';
$lang['common_catalog']				= 'کاتالوگ';
$lang['common_categories']			= 'دسته بندی ها';
$lang['common_products']			= 'محصولات';
$lang['common_digital_products']	= 'محصولات دیجیتال';
$lang['common_content']				= 'مقدار';
$lang['common_author']				= 'نویسنده';
$lang['common_banners']				= 'آگهی ها';
$lang['common_boxes']				= 'جعبه';
$lang['common_pages']				= 'صفحات';
$lang['common_administrative']		= 'اداری';
$lang['common_settings']			= 'تنظیمات';
$lang['common_locations']			= 'مکان';
$lang['common_administrators']		= 'مدیران';
$lang['common_note']				= 'نکته';
$lang['common_alert']				= 'هوشیار';
$lang['common_log_out']				= 'خروج از سیستم';
$lang['common_front_end']			= 'پایان جلو';
$lang['common_dashboard']			= 'داشبورد';
$lang['common_home']				= 'خانه';
$lang['common_actions']				= 'عملیات';

//buttons
$lang['save']						= 'ذخیره';
$lang['edit']						= 'ویرایش';
$lang['delete']						= 'حذف کردن';
$lang['search']						= 'جستجو';
$lang['toggle_wysiwyg']				= 'تعویض WYSIWYG';


/*For DashBoard*/
$lang['authors'] = 'نویسنده';
$lang['package_category'] = 'بسته بندی رده';
$lang['packages'] = 'بسته';
$lang['packages_orders'] = 'بسته سفارشات';
$lang['send_message'] = 'پیام های ارسال شده';
$lang['inbox'] = 'صندوق';
$lang['acart_blog'] = 'وبلاگ';
$lang['manage_slider'] = 'مدیریت لغزان';
$lang['front_end'] = 'پایان جلو';
$lang['adverts'] = 'مدیریت تبلیغات';
$lang['cover_page'] = 'مدیریت جلد کتاب';
$lang['add_cover_page'] = 'اضافه کردن جلد کتاب';
$lang['update_cover_page'] = 'جلد کتاب با موفقیت به روز شده';
$lang['add_adverts'] = 'اضافه کردن تبلیغات';
$lang['box_ads'] = 'مدیریت آگهی در محتوا';
$lang['advertise_name'] = 'نام';
$lang['advertise_image'] = 'جلد تصویر';
$lang['advertise_cover_upload'] = 'لطفا آپلود جلد تصویر';
$lang['advertise_front_upload'] = 'لطفا تصویر مقابل آپلود';
$lang['advertise_back_upload'] = 'لطفا آپلود بازگشت تصویر';
$lang['advertise_back'] = 'برای تصویر بازگشت';
$lang['advertise_name_error'] = 'لطفا نام برای تبلیغات وارد کنید';
$lang['advertise_success'] = 'جلد کتاب با موفقیت اضافه شده است';
$lang['advertise_cover'] = 'برای تصویر پوشش';
$lang['advertise_cover'] = 'برای تصویر مقابل';
$lang['advertise_front'] = 'تصویر جبهه';
$lang['advertise_back'] = 'بازگشت تصویر';
$lang['bookcover_confirm_delete'] = 'آیا شما مطمئن هستید که میخواهید جلد کتاب را حذف کنید ';
$lang['add_book_cover'] = 'اضافه کردن جلد کتاب جدید';
$lang['delete_book_cover'] = 'جلد کتاب با موفقیت حذف';
$lang['disable_book_cover'] = 'جلد کتاب با موفقیت غیرفعال';
$lang['enable_book_cover'] = 'جلد کتاب با موفقیت فعال';
$lang['advertise_description'] = 'شرح';
$lang['type'] = 'تبلیغات نوع';
$lang['box1'] = 'جعبه1';
$lang['box2'] = 'جعبه2';
$lang['box_content'] = 'مدیریت آگهی در محتوا';
$lang['email_signup'] = 'ثبت نام ایمیل';
$lang['sidebar'] = 'نوار کناری';
$lang['registration_block'] = 'بلوک ثبت نام';
$lang['advertise_links'] = 'لینک به تبلیغات';
$lang['publish'] = 'کتاب انتشار';
$lang['register'] = 'ثبت نام';
$lang['footer_links'] = 'تماس با ما لینک';
$lang['acart_calculator']              = 'ماشین حساب کتاب قیمت گذاری';  
$lang['acart_customers']              = 'مشتریان';  
$lang['acart_customer_groups']        = 'گروه های مشتری';  
$lang['acart_orders']                 = 'سفارشات';  
$lang['acart_coupons']                = 'کوپن';  
$lang['acart_giftcards']              = 'کارت های هدیه';  
$lang['acart_categories']             = 'دسته بندی ها';  
$lang['acart_products']               = 'محصولات'; 
$lang['acart_digital_products']       = 'محصولات دیجیتال';  
$lang['acart_banners']                = 'آگهی ها';  
$lang['acart_reports']                = 'گزارش ها';  
$lang['acart_boxes']                  = 'جعبه';  
$lang['acart_pages']                  = 'صفحات';  
$lang['acart_settings']               = 'تنظیمات';  
$lang['acart_locations']              = 'مکان';  
$lang['enable']              = 'فعال';  
$lang['disable']              = 'غیر فعال';  
$lang['blog_added_msg']              = 'وبلاگ موفقیت انجام شد..';  
$lang['acart_menu_main_navigation_copy']              = 'صفحه اصلی';  
$lang['acart_menu_dashboard_copy']              = 'داشبورد';  
$lang['acart_menu_cart_admin_copy']              = 'سبد خرید مدیریت';  
$lang['acart_menu_emailqueue_copy']              = 'پیام ارتباطات';  
$lang['acart_menu_settings_copy']              = 'محیط';  

/*For DashBoard*/
$lang['authors'] = 'نویسنده';
$lang['package_category'] = 'بسته بندی رده';
$lang['packages'] = 'بسته';
$lang['packages_orders'] = 'بسته سفارشات';
$lang['send_message'] = 'پیام های ارسال شده';
$lang['inbox'] = 'صندوق';
$lang['acart_blog'] = 'وبلاگ';
$lang['manage_slider'] = 'مدیریت لغزان';
$lang['front_end'] = 'پایان جلو';
$lang['adverts'] = 'مدیریت تبلیغات';
$lang['cover_page'] = 'مدیریت جلد کتاب';
$lang['add_cover_page'] = 'اضافه کردن جلد کتاب';
$lang['update_cover_page'] = 'جلد کتاب با موفقیت به روز شده';
$lang['add_adverts'] = 'اضافه کردن تبلیغات';
$lang['box_ads'] = 'مدیریت آگهی در محتوا';
$lang['advertise_name'] = 'نام';
$lang['advertise_image'] = 'جلد تصویر';
$lang['advertise_cover_upload'] = 'لطفا آپلود جلد تصویر';
$lang['advertise_front_upload'] = 'لطفا تصویر مقابل آپلود';
$lang['advertise_back_upload'] = 'لطفا آپلود بازگشت تصویر';
$lang['advertise_back'] = 'برای تصویر بازگشت';
$lang['advertise_name'] = 'نام';
$lang['advertise_success'] = 'جلد کتاب با موفقیت اضافه شده است';
$lang['advertise_cover'] = 'برای تصویر پوشش';
$lang['advertise_cover'] = 'برای تصویر مقابل';
$lang['advertise_front'] = 'تصویر جبهه';
$lang['advertise_back'] = 'بازگشت تصویر';
$lang['bookcover_confirm_delete'] = 'آیا شما مطمئن هستید که میخواهید جلد کتاب را حذف کنید';
$lang['add_book_cover'] = 'اضافه کردن جلد کتاب جدید';
$lang['delete_book_cover'] = 'جلد کتاب با موفقیت حذف';
$lang['advertise_description'] = 'شرح';
$lang['manage_advertise_description'] = 'شرح';
$lang['type'] = 'نوع';
$lang['manage_cart_type'] = 'نوع';
$lang['box1'] = 'جعبه';
$lang['box2'] = 'جعبه2';
$lang['box_content'] = 'مدیریت آگهی در محتوا';
$lang['email_signup'] = 'ثبت نام ایمیل';
$lang['sidebar'] = 'نوار کناری';
$lang['footer_links'] = 'تماس با ما لینک';
$lang['acart_calculator']              = 'ماشین حساب کتاب قیمت گذاری';  
$lang['acart_customers']              = 'مشتریان';  
$lang['acart_customer_groups']        = 'گروه های مشتری';  
$lang['acart_orders']                 = 'سفارشات';  
$lang['acart_coupons']                = 'کوپن';  
$lang['acart_giftcards']              = 'کارت های هدیه';  
$lang['acart_categories']             = 'دسته بندی ها';  
$lang['acart_products']               = 'محصولات'; 
$lang['acart_digital_products']       = 'محصولات دیجیتال';  
$lang['acart_banners']                = 'آگهی ها';  
$lang['acart_reports']                = 'گزارش ها';  
$lang['acart_boxes']                  = 'جعبه';  
$lang['acart_pages']                  = 'صفحات';  
$lang['acart_settings']               = 'تنظیمات';  
$lang['acart_locations']              = 'مکان';  
$lang['enable']              = 'فعال';  
$lang['disable']              = 'غیر فعال';  
$lang['language_create_lang'] = 'اضافه کردن زبان های جدید';
/*For DashBoard*/


