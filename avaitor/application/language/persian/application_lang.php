<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	Copyright (c) 2011 Lonnie Ezell

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

//--------------------------------------------------------------------
// ! GENERAL SETTINGS
//--------------------------------------------------------------------

$lang['bf_site_name']			= 'نام سایت';
$lang['bf_site_email']			= 'سایت پست الکترونیک';
$lang['bf_site_email_help']		= 'ایمیل پیش فرض که ایمیل-سیستم تولید از فرستاده.';
$lang['bf_site_status']			= 'وضعیت سایت';
$lang['bf_online']				= 'آنلاین';
$lang['bf_offline']				= 'آفلاین';
$lang['bf_top_number']			= 'آیتم را <em> در </ EM> صفحه:';
$lang['bf_top_number_help']		= 'هنگام مشاهده گزارش ها، که چگونه بسیاری از موارد باید در یک زمان ذکر شده؟';
$lang['bf_home']				= 'خانه';
$lang['bf_site_information']	= 'اطلاعات سایت';
$lang['bf_timezone']			= 'منطقه زمانی';
$lang['bf_language']			= 'زبان';
$lang['bf_language_help']		= 'انتخاب زبان دسترس به کاربر.';

//--------------------------------------------------------------------
// ! AUTH SETTINGS
//--------------------------------------------------------------------

$lang['bf_security']			= 'امنیت';
$lang['bf_login_type']			= 'ورود به نوع';
$lang['bf_login_type_email']	= 'فقط ایمیل';
$lang['bf_login_type_username']	= 'نام کاربری فقط';
$lang['bf_allow_register']		= 'اجازه نام کاربری؟';
$lang['bf_login_type_both']		= 'ایمیل یا نام کاربری';
$lang['bf_use_usernames']		= 'صفحه نمایش کاربر در سراسر Avaitor:';
$lang['bf_use_own_name']		= 'استفاده از نام خود';
$lang['bf_allow_remember']		= 'اجازه \ 'ذخیره \'؟';
$lang['bf_remember_time']		= 'به یاد داشته باشید کاربران برای';
$lang['bf_week']				= 'هفته';
$lang['bf_weeks']				= 'هفته';
$lang['bf_days']				= 'روز';
$lang['bf_username']			= 'نام کاربری';
$lang['bf_password']			= 'رمز عبور';
$lang['bf_password_confirm']	= 'تکرار رمز عبور';
$lang['bf_display_name']		= 'نمایش نام';

//--------------------------------------------------------------------
// ! CRUD SETTINGS
//--------------------------------------------------------------------

$lang['bf_home_page']			= 'صفحه اصلی';
$lang['bf_pages']				= 'صفحات';
$lang['bf_enable_rte']			= 'فعال کردن RTE برای صفحات؟';
$lang['bf_rte_type']			= 'RTE نوع ';
$lang['bf_searchable_default']	= 'جستجو به صورت پیش فرض؟';
$lang['bf_cacheable_default']	= 'کش-قادر به طور پیش فرض؟';
$lang['bf_track_hits']			= 'آهنگ صفحه بازدیدها؟';

$lang['bf_action_save']			= 'ذخیره';
$lang['bf_action_delete']		= 'حذف کردن';
$lang['bf_action_cancel']		= 'لغو کردن';
$lang['bf_action_download']		= 'دانلود';
$lang['bf_action_preview']		= 'پیش دید';
$lang['bf_action_search']		= 'جستجو';
$lang['bf_action_purge']		= 'پالایش';
$lang['bf_action_restore']		= 'بازگرداندن';
$lang['bf_action_show']			= 'نمایش';
$lang['bf_action_login']		= 'ورود';
$lang['bf_action_logout']		= 'خروج';
$lang['bf_actions']				= 'Actions';
$lang['bf_clear']				= 'واضح';
$lang['bf_action_list']			= 'فهرست';
$lang['bf_action_create']		= 'ساختن';
$lang['bf_action_ban']			= 'منع';

//--------------------------------------------------------------------
// ! SETTINGS LIB
//--------------------------------------------------------------------

$lang['bf_do_check']			= 'برای به روز رسانی را بررسی کنید؟';
$lang['bf_do_check_edge']		= 'باید فعال باشد برای دیدن خونریزی به روز رسانی لبه نیز هست.';

$lang['bf_update_show_edge']	= 'مشخصات خونریزی به روز رسانی لبه؟';
$lang['bf_update_info_edge']	= 'را علامت نزنید به تنها برای به روز رسانی جدید برچسب تیک بزنید. بررسی کنید ببینید هر تصدیق جدید به مخزن رسمی.';

$lang['bf_ext_profile_show']	= 'آیا حساب کاربری مشخصات گسترش داده است؟';
$lang['bf_ext_profile_info']	= 'بررسی "پروفایل تمدید" به نمایش متا داده های اضافی گزینه در دسترس (پروژه ها)، omiting برخی از زمینه های Avaitor به طور پیش فرض (به عنوان مثال: آدرس).';

$lang['bf_yes']					= 'بله';
$lang['bf_no']					= 'بدون';
$lang['bf_none']				= 'هیچ کدام';
$lang['bf_id']					= 'شناسایی';

$lang['bf_or']					= 'یا';
$lang['bf_size']				= 'اندازه';
$lang['bf_files']				= 'فایل';
$lang['bf_file']				= 'پرونده';

$lang['bf_with_selected']		= 'با انتخاب';

$lang['bf_env_dev']				= 'توسعه';
$lang['bf_env_test']			= 'تست';
$lang['bf_env_prod']			= 'تولید';

$lang['bf_show_profiler']		= 'نمایش مدیریت پیشفیلتر؟';
$lang['bf_show_front_profiler']	= 'نمایش جبهه پایان پیشفیلتر؟';

$lang['bf_cache_not_writable']  = 'پوشه کش برنامه قابل نوشتن نمی';

$lang['bf_password_strength']			= 'تنظیمات امنیت رمز عبور';
$lang['bf_password_length_help']		= 'حداقل طول رمز عبور به عنوان مثال 8';
$lang['bf_password_force_numbers']		= 'باید تعداد نیروی رمز عبور؟';
$lang['bf_password_force_symbols']		= 'باید کاراکتر نیروی رمز عبور؟';
$lang['bf_password_force_mixed_case']	= 'باید نیروی رمز عبور مورد مخلوط؟';
$lang['bf_password_show_labels']	    = 'نمایش رمز عبور برچسب اعتبار';

//--------------------------------------------------------------------
// ! USER/PROFILE
//--------------------------------------------------------------------

$lang['bf_user']				= 'کاربر';
$lang['bf_users']				= 'کاربران';
$lang['bf_description']			= 'شرح';
$lang['bf_email']				= 'ایمیل';
$lang['bf_user_settings']		= 'پروفایل من';

//--------------------------------------------------------------------
// !
//--------------------------------------------------------------------

$lang['bf_both']				= 'هر دو';
$lang['bf_go_back']				= 'برگرد';
$lang['bf_new']					= 'جدید';
$lang['bf_required_note']		= 'زمینه های مورد نیاز در <b> جسورانه </ B> است.';
$lang['bf_form_label_required'] = 'توسط <span class = "نیاز"> * </ span> را';

//--------------------------------------------------------------------
// MY_Model
//--------------------------------------------------------------------
$lang['bf_model_db_error']		= 'خطا DB: ';
$lang['bf_model_no_data']		= 'داده ای در دسترس است.';
$lang['bf_model_invalid_id']	= 'ID نامعتبر به مدل منتقل می شود.';
$lang['bf_model_no_table']		= 'مدل جدول پایگاه داده نامشخص.';
$lang['bf_model_fetch_error']	= 'نه اطلاعات کافی برای واکشی درست.';
$lang['bf_model_count_error']	= 'نه اطلاعات کافی برای نتایج شمارش.';
$lang['bf_model_unique_error']	= 'نه اطلاعات کافی برای بررسی منحصر به فرد.';
$lang['bf_model_find_error']	= 'نه اطلاعات کافی برای پیدا کردن توسط.';
$lang['bf_model_bad_select']	= 'انتخاب نامعتبر است.';

//--------------------------------------------------------------------
// Contexts
//--------------------------------------------------------------------
$lang['bf_no_contexts']			= 'آرایه زمینه است به درستی راه اندازی نمی کند. نرم افزار فایل پیکربندی خود را بررسی کنید.';
$lang['bf_context_content']		= 'مقدار';
$lang['bf_context_reports']		= 'گزارش ها';
$lang['bf_context_settings']	= 'تنظیمات';
$lang['bf_context_developer']	= 'توسعه دهنده';
$lang['bf_context_acart']	    = 'سبد خرید';

//--------------------------------------------------------------------
// Activities
//--------------------------------------------------------------------
$lang['bf_act_settings_saved']	= 'تنظیمات برنامه از ذخیره';
$lang['bf_unauthorized_attempt']= 'ناموفق تلاش برای دسترسی به یک صفحه که از نیاز به اجازه زیر "٪ s"';

$lang['bf_keyboard_shortcuts']		= 'میانبرهای صفحه کلید های موجود:';
$lang['bf_keyboard_shortcuts_none']	= 'هیچ میانبرهای صفحه کلید اختصاص داده وجود دارد.';
$lang['bf_keyboard_shortcuts_edit']	= 'به روز رسانی میانبرهای صفحه کلید';

//--------------------------------------------------------------------
// Common
//--------------------------------------------------------------------
$lang['bf_question_mark']	    = '?';
$lang['bf_language_direction']	= 'لیتر';
$lang['log_intro']              = 'این پیام ها ورود شما هستند';

//--------------------------------------------------------------------
// Login
//--------------------------------------------------------------------
$lang['bf_action_register']		= 'ثبت نام';
$lang['bf_forgot_password']		= 'رمز عبور خود را فراموش کرده اید؟';
$lang['bf_remember_me']			= 'مرا به خاطر بسپار';

//--------------------------------------------------------------------
// Password Help Fields to be used as a warning on register
//--------------------------------------------------------------------
$lang['bf_password_number_required_help']  = 'رمز عبور باید حداقل 1 علامت نقطه گذاری باشند.';
$lang['bf_password_caps_required_help']    = 'رمز عبور باید حداقل 1 حرف بزرگ باشد.';
$lang['bf_password_symbols_required_help'] = 'رمز عبور باید حداقل 1 نماد باشد.';

$lang['bf_password_min_length_help']       = 'رمز عبور باید حداقل٪ حرف طولانی باشد.';
$lang['bf_password_length']                = 'طول رمز عبور';

//--------------------------------------------------------------------
// User Meta examples
//--------------------------------------------------------------------

$lang['user_meta_street_name']	= 'نام خیابان';
$lang['user_meta_type']			= 'نوع';
$lang['user_meta_country']		= 'کشور';
$lang['user_meta_state']		= 'دولت';

// Activation
//--------------------------------------------------------------------
$lang['bf_activate_method']			= 'روش فعال سازی';
$lang['bf_activate_none']			= 'هیچ کدام';
$lang['bf_activate_email']			= 'ایمیل';
$lang['bf_activate_admin']			= 'مدیریت';
$lang['bf_activate']				= 'فعال کردن';
$lang['bf_activate_resend']			= 'فعال سازی ارسال دوباره';
$lang['bf_username_exists']			= 'نام کاربری در حال حاضر در حال استفاده';
$lang['bf_email_registered']		= 'ایمیل در حال حاضر در حال استفاده ';

$lang['bf_reg_complete_error']		= 'تکمیل ثبت نام شما خطایی رخ داد. لطفا دوباره سعی کنید یا با مدیر سایت برای کمک.';
$lang['bf_reg_activate_email'] 		= 'ایمیل حاوی کد فعال سازی شما شده است به [EMAIL] ارسال می شود.';
$lang['bf_reg_activate_admin'] 		= 'شما اطلاع داده خواهد شد هنگامی که مدیر سایت تا به عضویت خود را تایید شده است.';
$lang['bf_reg_activate_none'] 		= 'لطفا برای شروع با استفاده از سایت وارد سایت شوید.';
$lang['bf_user_not_active'] 		= 'حساب کاربری غیر فعال است.';
$lang['bf_login_activate_title']	= 'نیاز به حساب خود را فعال کنید؟';
$lang['bf_login_activate_email'] 	= '<b>دارای کد فعال سازی را وارد کنید برای فعال سازی عضویت شما؟ </ B> آن را در [ACCOUNT_ACTIVATE_URL] صفحه وارد کنید.<br /><br />    <b>نیاز کد خود را دوباره؟</b>درخواست دوباره آن را در [ACTIVATE_RESEND_URL] صفحه.';
$lang['acart_menu_main_navigation'] = 'صفحه اصلی';
$lang['acart_menu_dashboard'] = 'داشبورد';
$lang['acart_menu_messages'] = 'پیام ارتباطات';
$lang['acart_menu_reports'] = 'گزارش ها';
$lang['acart_menu_activities'] = 'فعالیت';
$lang['acart_menu_cart_admin'] = 'سبد خرید مدیریت';
$lang['acart_menu_settings'] = 'تنظیمات';
$lang['acart_menu_keyboardshortcut'] = 'میانبر صفحه کلید';
$lang['acart_menu_permissions'] = 'ویرایش';
$lang['acart_menu_roles'] = 'نقش';
$lang['acart_menu_users'] = 'کاربران';
$lang['acart_menu_emailqueue'] = 'فرستادن به ایمیل صف';
$lang['acart_menu_templates'] = 'قالب';
$lang['acart_menu_viewqueue'] = 'نمایش صف';
$lang['acart_menu_developer'] = 'توسعه دهنده';
$lang['acart_menu_code_builder'] = 'کد کشاورزی';
$lang['acart_menu_database_tools'] = 'بانک اطلاعات ابزار';
$lang['acart_menu_maintenance'] = 'نگهداری';
$lang['acart_menu_backups'] = 'پشتیبان گیری';
$lang['acart_menu_migrations'] = 'مهاجرت';
$lang['acart_menu_logs'] = 'گزارش ها';
$lang['acart_menu_sysinfo'] = 'اطلاعات سیستم';
$lang['acart_menu_translate'] = 'ترجمه کردن';
$lang['acart_menu_acart'] = 'سبد خرید';
$lang['acart_menu_sales'] = 'فروش';
$lang['acart_menu_sales_report'] = 'گزارش فروش';
$lang['acart_menu_sales_details'] = 'جزییات فروش';
$lang['acart_menu_publish_reports'] = 'کتاب انتشار گزارش';
$lang['acart_details'] = 'دریافت اطلاعات';
$lang['acart_menu_customers'] = 'مشتریان';
$lang['acart_menu_reports'] = 'گزارش ها';
$lang['acart_menu_orders'] = 'سفارشات';
$lang['acart_menu_coupons'] = 'کوپن';
$lang['acat_menu_giftcards'] = 'کارت های هدیه';
$lang['acart_menu_book_cover'] = 'جلد کتاب';
$lang['acart_menu_catalog'] = 'کاتالوگ';
$lang['acart_menu_categories'] = 'دسته بندی ها';
$lang['acart_menu_products'] = 'محصولات';
$lang['acart_menu_content'] = 'مقدار';
$lang['acart_menu_pages'] = 'صفحات';
$lang['acart_menu_blog'] = 'وبلاگ';
$lang['acart_menu_slider'] = 'مدیریت لغزان';
$lang['acart_manage_slider'] = 'مدیریت لغزان';
$lang['acart_menu_frontend'] = 'مشاهده وب سایت';
$lang['acart_menu_bookpricing_calculator'] = 'ماشین حساب کتاب قیمت گذاری';
$lang['acart_book_calculator'] = 'کتاب ماشین حساب';
$lang['acart_menu_locations'] = 'مکان';
$lang['acart_menu_manage_adv'] = 'مدیریت آگهی ها';
$lang['acart_menu_emailsignup'] = 'فرستادن به ایمیل ثبت نام';
$lang['acart_menu_footer_links'] = 'لینک های پاورقی';
$lang['acart_menu_author_admin'] = 'نویسنده مدیریت';
$lang['acart_menu_author'] = 'نویسنده عضو';
$lang['acart_menu_packcat'] = 'بسته بندی رده';
$lang['acart_menu_packages'] = 'بسته';
$lang['acart_menu_packorder'] = 'سفارشات بسته بندی';
$lang['acart_menu_sendmsg'] = 'ارسال پیام';
$lang['acart_menu_recmsg'] = 'پیام های دریافت';
$lang['acart_menu_sentmsg'] = 'پیام های ارسال شده';
$lang['acart_book_cover'] = 'جلد کتاب'; 
$lang['acart_menu_publish_book'] = 'انتشار کتاب';
$lang['bf_action_logout_header'] = 'خروج از سیستم';
$lang['bf_user_settings_header'] = 'نمایه';
$lang['upload_settings'] = 'تنظیمات آپلود';

$lang['restore_filesystem'] = 'بازگرداندن فایل سیستم';
$lang['upload'] = 'بارگذاری';
$lang['s3_setting'] = 'تنظیمات آمازون S3';
$lang['access_key'] = 'کلید دسترسی';
$lang['secret_key'] = 'کلید مخفی';
$lang['amazon_host'] = 'آمازون میزبان';
$lang['bucketName'] = 'نام سطل';
$lang['upload_on_s3'] = 'آپلود S3';
$lang['profile'] = 'پروفایل من';
$lang['folder_doesnt_exists'] = 'پوشه می کند وجود دارد نیست!';
$lang['folder_empty'] = 'پوشه خالی است!';
$lang['session_expired'] = 'جلسه شما منقضی شده است، لطفا ورود دوباره.';
$lang['restore_users_file_system'] = 'بازیابی کاربران سیستم فایل';
$lang['search_user'] = 'جستجو کاربر';
$lang['no_record_found'] = 'هیچ سابقه ای پیدا نشد!';
$lang['language_create_lang'] = 'اضافه کردن زبان های جدید';
$lang['manage_language'] = 'مدیریت زبان';
