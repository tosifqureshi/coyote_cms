<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	Copyright (c) 2011 Lonnie Ezell

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

$lang['us_account_deleted']			= 'متاسفانه حساب شما حذف شده است. این هنوز پاکسازی نشده است و <strong> هنوز هم ممکن است </ قوی> ترمیم شود. تماس با مدیر سایت در٪ s.';

$lang['us_bad_email_pass']			= 'ایمیل نادرست و یا رمز عبور.';
$lang['us_must_login']				= 'شما باید وارد برای مشاهده آن صفحه.';
$lang['us_no_permission']			= 'شما اجازه دسترسی به که صفحه را ندارید.';
$lang['us_fields_required']         = '٪ s و زمینه رمز عبور باید پر خواهد شد.';

$lang['us_access_logs']				= 'گزارش ها دسترسی';
$lang['us_logged_in_on']			= 'حضور در انجمن بر';
$lang['us_no_access_message']		= '<P> تبریک می گویم! </ P> <P> همه از کاربران خود را داشته باشند و خاطرات خوب! </ p>';
$lang['us_log_create']				= 'ایجاد جدید';
$lang['us_log_edit']				= 'کاربر تغییر';
$lang['us_log_delete']				= 'کاربر حذف';
$lang['us_log_logged']				= 'از وارد سایت شوید';
$lang['us_log_logged_out']			= 'از وارد سایت شوید';
$lang['us_log_reset']				= 'رمز عبور خود را بازنشانی کنید.';
$lang['us_log_register']			= 'ثبت نام یک حساب جدید.';
$lang['us_log_edit_profile']		= 'پروفایل خود را بروز';


$lang['us_deleted_users']			= 'کاربران حذف شده';
$lang['us_purge_del_accounts']		= 'تصفیه حساب حذف';
$lang['us_purge_del_note']			= '<p> در حساب پاکسازی حذف یک عمل دائمی است. هیچ وجود دارد به رفتن است، پس لطفا مطمئن شوید. </ P>';
$lang['us_purge_del_confirm']		= 'آیا مطمئن هستید که می خواهید به طور کامل حساب کاربری (بازدید کنندگان) حذف - وجود ندارد و رفتن به عقب؟';
$lang['us_action_purged']			= 'کاربران پاکسازی.';
$lang['us_action_deleted']			= 'کاربر با موفقیت حذف شد.';
$lang['us_action_not_deleted']		= 'ما می تواند کاربر را حذف کنید:';
$lang['us_delete_account']			= 'حذف حساب';
$lang['us_delete_account_note']		= '<H3> حذف این حساب </ H3> <P> حذف این حساب تمام امتیازات خود را در سایت لغو. </ P>';
$lang['us_delete_account_confirm']	= 'آیا مطمئن هستید که می خواهید حساب کاربری (بازدید کنندگان) را حذف کنید؟';

$lang['us_restore_account']			= 'بازگرداندن حساب';
$lang['us_restore_account_note']	= '<H3> بازگرداندن این حساب </ H3> <p> در سازمان ملل متحد حذف این کاربر \ حساب. </ P>';
$lang['us_restore_account_confirm']	= 'بازگرداندن این حساب کاربران؟';

$lang['us_role']					= 'نقش';
$lang['us_role_lower']				= 'نقش';
$lang['us_no_users']				= 'بدون کاربران در بر داشت.';
$lang['us_create_user']				= 'ایجاد کاربر جدید';
$lang['us_create_user_note']		= '<H3> ایجاد یک کاربر جدید </ H3> <P> ایجاد حساب جدید برای کاربران دیگر در دایره خود را. </ P>';
$lang['us_edit_user']				= 'ویرایش کاربر';
$lang['us_restore_note']			= 'بازگرداندن کاربر و دسترسی به سایت اجازه می دهد آنها را دوباره.';
$lang['us_unban_note']				= 'سازمان ملل متحد بان کی مون کاربر و همه آنها را از دسترسی به سایت.';
$lang['us_account_status']			= 'حساب وضعیت';

$lang['us_failed_login_attempts']	= 'ورود ناموفق';
$lang['us_failed_logins_note']		= '<P> تبریک می گویم! </ P> <P> همه از کاربران خود را داشته باشند و خاطرات خوب! </ p>';

$lang['us_banned_admin_note']		= 'این کاربر شده است از سایت ممنوع است.';
$lang['us_banned_msg']				= 'این حساب اجازه برای ورود به سایت ندارد.';

$lang['us_first_name']				= 'نام';
$lang['us_last_name']				= 'نام خانوادگی';
$lang['us_address']					= 'نشانی';
$lang['us_street_1']				= 'خیابان 1';
$lang['us_street_2']				= 'خیابان 2';
$lang['us_city']					= 'شهرستان';
$lang['us_state']					= 'دولت';
$lang['us_no_states']				= ';هیچ ایالات / استان / شهرستان / منطقه برای این کشور وجود دارد. ایجاد آنها را در آدرس فایل پیکربندی';
$lang['us_country']					= 'کشور';
$lang['us_zipcode']					= 'کد پستی';

$lang['us_user_management']			= 'مدیریت کاربر';
$lang['us_email_in_use']			= 'آدرس٪ است در حال حاضر در حال استفاده است. لطفا دیگری را انتخاب کنید.';

$lang['us_edit_profile']			= 'ویرایش پروفایل';
$lang['us_edit_note']				= 'جزئیات خود را در زیر وارد کنید و ذخیره را کلیک کنید.';

$lang['us_reset_password']			= 'کلمه عبور';
$lang['us_reset_note']				= 'ایمیل خود را وارد کنید و ما یک گذرواژه موقت برای شما ارسال.';

$lang['us_login']					= 'ورود';
$lang['us_remember_note']			= 'مرا به خاطر بسپار به مدت دو هفته';
$lang['us_no_account']				= 'دان برون T دارای حساب هستید؟';
$lang['us_sign_up']					= 'ثبت نام امروز';
$lang['us_forgot_your_password']	= 'رمز عبور خود را فراموش کرده اید؟';

$lang['us_password_mins']			= 'حداقل 8 کاراکتر.';
$lang['us_register']				= 'ثبت نام';
$lang['us_already_registered']		= 'در حال حاضر ثبت نام؟';

$lang['us_action_save']				= 'ذخیره این کاربر';
$lang['us_unauthorized']			= 'غیر مجاز. با عرض پوزش شما اجازه مناسب را برای مدیریت نقش "٪ s" را ندارد.';
$lang['us_empty_id']				= 'بدون شناسه ارائه شده است. شما باید یک شناسه برای انجام این عمل فراهم می کند.';
$lang['us_self_delete']				= 'غیر مجاز. با عرض پوزش، شما می توانید خودتان را حذف کنید.';

$lang['us_filter_first_letter']		= 'نام کاربری شروع می شود با:';
$lang['us_account_details']			= 'جزئیات حساب';
$lang['us_last_login']				= 'تاریخ و زمان آخرین ورود';



$lang['us_no_password']             = 'بدون حال حاضر رمز عبور.';
$lang['us_no_email']                = 'بدون پست الکترونیک داده شده است.';
$lang['us_email_taken']             = 'ایمیل وجود دارد.';
$lang['us_invalid_user_id']         = 'ایمیل وجود دارد.';

$lang['us_no_password']             = 'بدون حال حاضر رمز عبور.';

$lang['us_no_email']                = 'بدون پست الکترونیک داده شده است.';

$lang['us_email_taken']             = 'ایمیل وجود دارد.';
$lang['us_invalid_user_id']         = 'شناسه کاربر نامعتبر است';



$lang['us_account_created_success'] = 'حساب شما ایجاد شده است. لطفا در وارد شوید.';

$lang['us_email_already_used']      = 'این آدرس ایمیل در حال حاضر در حال استفاده است.';
$lang['us_username_already_used']   = 'این نام کاربری در حال حاضر در حال استفاده است.';
$lang['us_invalid_user_id']         = 'شناسه کاربر نامعتبر است.';
$lang['us_invalid_email']           = 'آیا می توانم که ایمیل در سوابق ما را پیدا کند.';

$lang['us_reset_invalid_email']     = 'که به نظر نمی رسد به یک درخواست رمز عبور معتبر.';
$lang['us_reset_pass_subject']      = 'رمز عبور موقت شما';
$lang['us_reset_pass_message']      = 'لطفا ایمیل خود را برای دستورالعمل ها را به رمز عبور خود را تنظیم مجدد کنید.';
$lang['us_reset_pass_error']        = 'قادر ایمیل به ارسال: ';
$lang['us_reset_password_success']  = 'لطفا با استفاده از رمز عبور جدید خود وارد شوید.';
$lang['us_reset_password_error']    = 'یک خطای رمز عبور خود را بازنشانی وجود دارد: ';


$lang['us_profile_updated_success'] = 'مشخصات موفقیت به روز.';
$lang['us_profile_updated_error']   = 'یک مشکل به روز رسانی مشخصات شما وجود دارد';

$lang['us_register_disabled']       = 'ثبت نام حساب کاربری جدید امکان پذیر نیست.';


$lang['us_user_created_success']    = 'کاربر با موفقیت ایجاد شده است.';
$lang['us_user_update_success']     = 'کاربر با موفقیت به روز شد.';

$lang['us_user_restored_success']   = 'کاربر با موفقیت دوباره بازسازی شد.';
$lang['us_user_restored_error']     = 'قادر به بازگرداندن کاربر: ';


/* Activations */
$lang['us_status']					= 'وضعیت';
$lang['us_inactive_users']			= 'کاربران غیر فعال';
$lang['us_activate']				= 'فعال سازی';
$lang['us_user_activate_note']		= 'کد فعال سازی خود را وارد کنید برای تایید آدرس ایمیل خود و عضویت خود را فعال کنید.';
$lang['us_activate_note']			= 'فعال کردن کاربر و به آنها اجازه دسترسی به سایت';
$lang['us_deactivate_note']			= 'غیرفعال کردن کاربر برای جلوگیری از دسترسی به سایت';
$lang['us_activate_enter']			= 'لطفا کد فعال سازی خود را وارد کنید برای ادامه.';
$lang['us_activate_code']			= 'کد فعال سازی';
$lang['us_activate_request']		= 'درخواست یک گذرواژه جدید';
$lang['us_activate_resend']			= 'ارسال دوباره کد فعال سازی';
$lang['us_activate_resend_note']	= 'ایمیل خود را وارد کنید و ما کد فعال سازی حساب کاربری برای شما ارسال مجدد.';
$lang['us_confirm_activate_code']	= ' تایید کد فعال سازی';
$lang['us_activate_code_send']		= 'ارسال کد فعال سازی';
$lang['bf_action_activate']			= 'فعال کردن';
$lang['bf_action_deactivate']		= 'بی اثر کردن';
$lang['us_purge_del_accounts']		= 'تصفیه حساب حذف';
$lang['us_no_inactive']				= 'هر گونه کاربران نیاز به فعال سازی در پایگاه داده وجود ندارد.';
$lang['us_activate_accounts']		= 'فعال کردن همه حساب';
$lang['us_purge_act_note']			= '<H3> فعال کردن همه حساب </ H3> <P> دسته ای فعال تمام کاربران نیاز به فعال سازی. </ P>';
$lang['us_account_activated']		= 'کاربر فعال نشده باشد.';
$lang['us_account_deactivated']		= 'کاربر غیر فعال کردن حساب.';
$lang['us_account_activated_admin']	= 'فعال سازی حساب کاربری اداری.';
$lang['us_account_deactivated_admin']	= 'غیر فعال کردن حساب کاربری اداری.';
$lang['us_active']					= 'فعال.';
$lang['us_inactive']				= 'غیر فعال.';
//email subjects
$lang['us_email_subj_activate']		= 'فعال کردن عضویت شما';
$lang['us_email_subj_pending']		= 'ثبت نام کامل. فعال سازی در انتظار.';
$lang['us_email_thank_you']			= 'با تشکر از شما برای ثبت نام! ';
// Activation Statuses
$lang['us_registration_fail'] 		= 'ثبت نام کامل انجام نمی موفقیت. ';
$lang['us_check_activate_email'] 	= 'لطفا ایمیل خود را برای دستورالعمل ها را به حساب خود را فعال کنید.';
$lang['us_admin_approval_pending']  = 'حساب شما در انتظار مدیر تایید. شما ایمیل اگر حساب خود را فعال می شود دریافت می کنند.';
$lang['us_account_not_active'] 		= 'حساب شما هنوز فعال لطفا به حساب خود با وارد کردن کد را فعال کنید.';
$lang['us_account_active'] 			= 'تبریک می گویم. حساب کاربری شما در حال حاضر فعال است !.';
$lang['us_account_active_login'] 	= 'حساب شما فعال است و شما هم اکنون می توانید وارد سایت شوید.';
$lang['us_account_reg_complete'] 	= 'ثبت نام [SITE_TITLE] تکمیل!';
$lang['us_active_status_changed'] 	= 'وضعیت کاربر با موفقیت تغییر یافت.';
$lang['us_active_email_sent'] 		= 'ایمیل فعال سازی ارسال شد.';
// Activation Errors
$lang['us_err_no_id'] 				= 'بدون شناسه کاربری دریافت شد.';
$lang['us_err_status_error'] 		= 'وضعیت کاربران تغییر نمی شد: ';
$lang['us_err_no_email'] 			= 'قادر ایمیل به ارسال:';
$lang['us_err_activate_fail'] 		= 'عضویت خود را نمی تواند در این زمان با توجه به دلیل زیر فعال: ';
$lang['us_err_activate_code'] 		= 'لطفا کد خود را بررسی کنید و دوباره امتحان کنید یا با مدیر سایت برای کمک.';
$lang['us_err_no_activate_code'] 	= 'کد اعتبار سنجی فعال شدن لازم است از دست رفته بود.';
$lang['us_err_no_matching_code'] 	= 'بدون کد فعال سازی تطبیق در سیستم یافت شد.';
$lang['us_err_no_matching_id'] 		= 'کاربری با این مشخصات شناسه در سیستم یافت شد.';
$lang['us_err_user_is_active'] 		= 'کاربر در حال حاضر فعال است.';
$lang['us_err_user_is_inactive'] 	= 'کاربر در حال حاضر غیر فعال است.';

/* Password strength/match */
$lang['us_pass_strength']			= 'استحکام';
$lang['us_pass_match']				= 'مقایسه';
$lang['us_passwords_no_match']		= 'هیچ بازی!';
$lang['us_passwords_match']			= 'مسابقه!';
$lang['us_pass_weak']				= 'ضعیف';
$lang['us_pass_good']				= 'خوب';
$lang['us_pass_strong']				= 'قوی';

/*Custom */

$lang['us_create_account']		= 'ایجاد یک حساب کاربری';
$lang['us_full_name']			= 'نام و نام خانوادگی';
$lang['us_email']				= 'ایمیل';
$lang['us_password']			= 'رمز عبور';
$lang['us_cnf_password']		= 'تکرار رمز عبور';
$lang['us_captcha_code']		= 'لطفا کد تصویر امنیتی را وارد';
$lang['us_agree1']				= 'من به توافق برسند';
$lang['us_agree2']				= 'نظر Airdoc.';
$lang['us_sign_in']				= 'ورود';

$lang['us_footer_1']			= 'و کپی. 2014 AIRDOC. تمام حقوق محفوظ می باشد.';
$lang['us_footer_2']			= 'سیاست حفظ حریم خصوصی';

$lang['us_hi']					= 'سلام ';
$lang['us_logout']				= 'خروج از سیستم';

$lang['us_reset_message']		= 'رمز عبور جدید خود را در زیر رمز عبور خود را برای تنظیم مجدد را وارد کنید.';
$lang['us_reset_password']		= 'رمز عبور خود را تنظیم مجدد؟';
$lang['us_reset']				= 'کلمه عبور';

$lang['us_forgot_password']		= 'رمز عبور خود را فراموش کرده اید؟';
$lang['us_send_password']		= 'ارسال رمز عبور';
$lang['us_cancel']				= 'لغو کردن';

$lang['us_home_message']		= 'با عرض پوزش این است دعوت تنها سایت.';
$lang['us_search']				= 'جستجو';
$lang['us_files']				= 'فایل';
$lang['us_share']				= 'سهم';
$lang['us_create']				= 'ساختن';
$lang['us_collaborate']			= 'همکاری';


$lang['us_menu_home']			= 'خانه';
$lang['us_menu_about']			= 'در باره';
$lang['us_menu_contact']		= 'تماس';

$lang['us_login_error_msg']		= 'با عرض پوزش این است دعوت تنها سایت.';
$lang['us_login_user_name']		= 'نام کاربری';
$lang['us_login_remember_me']	= 'مرا به خاطر بسپار';
$lang['us_login_forgot']		= 'فراموشی رمز عبور؟';
$lang['us_login_go']			= 'رفتن';
$lang['login_attempt_failure_msg']	= 'با توجه به چند تلاش ورود به شکست، دسترسی شما به سایت محدود شده است. لطفا پس از 24 دقیقه سعی';
$lang['captcha_error'] 			= 'کد امنیتی نامعتبر است!';


$lang['user_statistics'] = 'ارقام';
$lang['logged_in_from'] = 'از خارج';
$lang['date_time'] = 'تاریخ / زمان';
$lang['user_created_folders'] = 'پوشه ایجاد شده';
$lang['user_uploaded_files'] = 'فایل های آپلود شده';
$lang['user_deleted_files'] = 'فایل های پاک شده';
$lang['using'] = 'با استفاده از';
$lang['already_taken'] = 'نام کاربری وارد قبلا گرفته شده است.';
$lang['airdoc_email_saved_successfully'] = 'ایمیل Airdoc شما با موفقیت ذخیره شد.';
$lang['airdoc_email_saved_tech_error'] = 'با توجه به برخی از خطای فنی قادر به ذخیره Airdoc خود را ایمیل.';
$lang['airdoc_email_empty'] = 'لطفا نام کاربری را وارد کنید.';
$lang['us_invite'] = 'دعوت کردن';
$lang['us_contact_page'] = 'تماس با مثال';

