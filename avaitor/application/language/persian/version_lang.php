<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['version'] = 'نسخه';
$lang['uploaded_on'] = 'آپلود شده در تاریخ';
$lang['download'] = 'دانلود';
$lang['modified'] = 'اصلاح';
$lang['no_version_mssg'] = 'بدون نسخه در دسترس برای این فایل';
