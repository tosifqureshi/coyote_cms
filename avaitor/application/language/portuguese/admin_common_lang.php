<?php
/******************************************
US English
Admin Common Language
******************************************/

//header
$lang['common_sales']				= 'Sales';
$lang['common_orders']				= 'Orders';
$lang['common_customers']			= 'Customers';
$lang['common_groups']				= 'Groups';
$lang['common_reports']				= 'Reports';
$lang['common_coupons']				= 'Coupons';
$lang['common_giftcards']			= 'Gift Cards';
$lang['common_catalog']				= 'Catalog';
$lang['common_categories']			= 'Categories';
$lang['common_products']			= 'Products';
$lang['common_digital_products']	= 'Digital Products';
$lang['common_content']				= 'Content';
$lang['common_author']				= 'Author';
$lang['common_banners']				= 'Banners';
$lang['common_boxes']				= 'Boxes';
$lang['common_pages']				= 'Pages';
$lang['common_administrative']		= 'Administrative';
$lang['common_settings']			= 'Settings';
$lang['common_locations']			= 'Locations';
$lang['common_administrators']		= 'Administrators';
$lang['common_note']				= 'Note';
$lang['common_alert']				= 'Alert';
$lang['common_log_out']				= 'Log Out';
$lang['common_front_end']			= 'Front End';
$lang['common_dashboard']			= 'Dashboard';
$lang['common_home']				= 'Home';
$lang['common_actions']				= 'Actions';

//buttons
$lang['save']						= 'Save';
$lang['edit']						= 'Edit';
$lang['delete']						= 'Delete';
$lang['search']						= 'Search';
$lang['toggle_wysiwyg']				= 'Toggle WYSIWYG';


/*For DashBoard*/
$lang['authors'] = 'Author';
$lang['package_category'] = 'Package Category';
$lang['packages'] = 'Packages';
$lang['packages_orders'] = 'Packages Orders';
$lang['send_message'] = 'Sent Message';
$lang['inbox'] = 'Inbox';
$lang['acart_blog'] = 'Blog';
$lang['manage_slider'] = 'Manage Slider';
$lang['front_end'] = 'Front End';
$lang['adverts'] = 'Manage Advertise';
$lang['cover_page'] = 'Manage Book Cover';
$lang['add_cover_page'] = 'Add Book Cover';
$lang['update_cover_page'] = 'Book Cover updated successfully';
$lang['add_adverts'] = 'Add Advertisement';
$lang['box_ads'] = 'Manage Advertisement on content';
$lang['advertise_name'] = 'Name';
$lang['advertise_image'] = 'Cover Image';
$lang['advertise_cover_upload'] = 'Please Upload Cover Image';
$lang['advertise_front_upload'] = 'Please Upload front Image';
$lang['advertise_back_upload'] = 'Please Upload back Image';
$lang['advertise_back'] = 'For back image';
$lang['advertise_name_error'] = 'Please enter name for advertisement';
$lang['advertise_success'] = 'Book Cover added successfully';
$lang['advertise_cover'] = 'For cover image';
$lang['advertise_cover'] = 'For front image';
$lang['advertise_front'] = 'Front Image';
$lang['advertise_back'] = 'Back Image';
$lang['bookcover_confirm_delete'] = 'Are you sure you want to delete book cover ';
$lang['add_book_cover'] = 'Add New Book Cover';
$lang['delete_book_cover'] = 'Book cover deleted successfully';
$lang['disable_book_cover'] = 'Book cover disabled successfully';
$lang['enable_book_cover'] = 'Book cover enabled successfully';
$lang['advertise_description'] = 'Description';
$lang['type'] = 'Advertisment Type';
$lang['box1'] = 'Box1';
$lang['box2'] = 'Box2';
$lang['box_content'] = 'Manage Advertisement on content';
$lang['email_signup'] = 'Email signup';
$lang['sidebar'] = 'Sidebar';
$lang['registration_block'] = 'registration_block';
$lang['advertise_links'] = 'Link to Advertisment';
$lang['publish'] = 'Publish book';
$lang['register'] = 'Registration Page';
$lang['footer_links'] = 'Follow Us Links';
$lang['acart_calculator']              = 'Book Pricing Calculator';  
$lang['acart_customers']              = 'Customers';  
$lang['acart_customer_groups']        = 'Customer Groups';  
$lang['acart_orders']                 = 'Orders';  
$lang['acart_coupons']                = 'Coupons';  
$lang['acart_giftcards']              = 'Gift Cards';  
$lang['acart_categories']             = 'Categories';  
$lang['acart_products']               = 'Products'; 
$lang['acart_digital_products']       = 'Digital Products';  
$lang['acart_banners']                = 'Banners';  
$lang['acart_reports']                = 'Reports';  
$lang['acart_boxes']                  = 'Boxes';  
$lang['acart_pages']                  = 'Pages';  
$lang['acart_settings']               = 'Settings';  
$lang['acart_locations']              = 'Locations';  
$lang['enable']              = 'Enabled';  
$lang['disable']              = 'Disabled';  
$lang['blog_added_msg']              = 'Blog has been added successfully.';  
$lang['acart_menu_main_navigation_copy']              = 'Main navigation';  
$lang['acart_menu_dashboard_copy']              = 'Dashboard';  
$lang['acart_menu_cart_admin_copy']              = 'Cart Admin';  
$lang['acart_menu_emailqueue_copy']              = 'Message Communication';  
$lang['acart_menu_settings_copy']              = 'Setting';  

/*For DashBoard*/
$lang['authors'] = 'Author';
$lang['package_category'] = 'Package Category';
$lang['packages'] = 'Packages';
$lang['packages_orders'] = 'Packages Orders';
$lang['send_message'] = 'Sent Message';
$lang['inbox'] = 'Inbox';
$lang['acart_blog'] = 'Blog';
$lang['manage_slider'] = 'Manage Slider';
$lang['front_end'] = 'Front End';
$lang['adverts'] = 'Manage Advertise';
$lang['cover_page'] = 'Manage Book Cover';
$lang['add_cover_page'] = 'Add Book Cover';
$lang['update_cover_page'] = 'Book Cover updated successfully';
$lang['add_adverts'] = 'Add Advertisement';
$lang['box_ads'] = 'Manage Advertisement on content';
$lang['advertise_name'] = 'Name';
$lang['advertise_image'] = 'Cover Image';
$lang['advertise_cover_upload'] = 'Please Upload Cover Image';
$lang['advertise_front_upload'] = 'Please Upload front Image';
$lang['advertise_back_upload'] = 'Please Upload back Image';
$lang['advertise_back'] = 'For back image';
$lang['advertise_name'] = 'name';
$lang['advertise_success'] = 'Book Cover added successfully';
$lang['advertise_cover'] = 'For cover image';
$lang['advertise_cover'] = 'For front image';
$lang['advertise_front'] = 'Front Image';
$lang['advertise_back'] = 'Back Image';
$lang['bookcover_confirm_delete'] = 'Are you sure you want to delete book cover ';
$lang['add_book_cover'] = 'Add New Book Cover';
$lang['delete_book_cover'] = 'Book cover deleted successfully';
$lang['advertise_description'] = 'Description';
$lang['manage_advertise_description'] = 'Description';
$lang['type'] = 'Type';
$lang['manage_cart_type'] = 'Type';
$lang['box1'] = 'Box1';
$lang['box2'] = 'Box2';
$lang['box_content'] = 'Manage Advertisement on content';
$lang['email_signup'] = 'Email signup';
$lang['sidebar'] = 'Sidebar';
$lang['footer_links'] = 'Follow Us Links';
$lang['acart_calculator']              = 'Book Pricing Calculator';  
$lang['acart_customers']              = 'Customers';  
$lang['acart_customer_groups']        = 'Customer Groups';  
$lang['acart_orders']                 = 'Orders';  
$lang['acart_coupons']                = 'Coupons';  
$lang['acart_giftcards']              = 'Gift Cards';  
$lang['acart_categories']             = 'Categories';  
$lang['acart_products']               = 'Products'; 
$lang['acart_digital_products']       = 'Digital Products';  
$lang['acart_banners']                = 'Banners';  
$lang['acart_reports']                = 'Reports';  
$lang['acart_boxes']                  = 'Boxes';  
$lang['acart_pages']                  = 'Pages';  
$lang['acart_settings']               = 'Settings';  
$lang['acart_locations']              = 'Locations';  
$lang['enable']              = 'Enabled';  
$lang['disable']              = 'Disabled';  
$lang['language_create_lang'] = 'Add New Language';
/*For DashBoard*/


