<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['version'] = 'Version';
$lang['uploaded_on'] = 'Uploaded On';
$lang['download'] = 'Download';
$lang['modified'] = 'Modified';
$lang['no_version_mssg'] = 'No version available for this file';
