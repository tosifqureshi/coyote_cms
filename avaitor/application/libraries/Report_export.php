<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This class is used to manage for report controller logic and data
 *
 * @package   report manage
 * @author    Lokendra Meena
 * @email     lokendrameena@cdnsol.com
 * @since     Version 1.0
 * @filesource
 */
 
 
class Report_export{
    
    
    public $reportTitle          = NULL;
    public $reportName           = NULL;
    public $reportHeading        = NULL;
    public $reportGenerateDate   = NULL;
    public $filename             = NULL;
    public $headingTitle         = NULL;
    public $exportData           = NULL;
    public $columRange           = NULL;
    
    
    function __construct(){
        //create instance object
        $this->ci =& get_instance();
    }
    
    
    /*
    *  @Description: This method is use to export xls file
    *  @auther: lokendra meena
    *  @return: void
    */ 
    
    public function xlsexport(){
       
        //-----load libarray----------
        $this->ci->load->library(array('excel'));
        
        //activate worksheet number 1
        $this->ci->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->ci->excel->getActiveSheet()->setTitle($this->reportTitle);
        
        //----------set related information row start-------------//
                
        //set common width of colomn
        $this->ci->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
        $this->ci->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->ci->excel->getActiveSheet()->mergeCells('A1:E1');
        $this->ci->excel->getActiveSheet()->mergeCells('A2:E2');
        $this->ci->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);

        //set cell A1 content with some text
        $this->ci->excel->getActiveSheet()->setCellValue('A1', $this->reportName);
        $this->ci->excel->getActiveSheet()->setCellValue('A2', $this->reportHeading);
        $this->ci->excel->getActiveSheet()->setCellValue('A3', 'Report Date');
        $this->ci->excel->getActiveSheet()->setCellValue('B3', $this->reportGenerateDate);

        //change the font size
        $this->ci->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18)->getColor()->setRGB('425968');
        $this->ci->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(16)->getColor()->setRGB('cb2028');

        //set background color in cell of header
        $this->ci->excel->getActiveSheet()->getStyle('A1')->applyFromArray( array(
            'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'BFBFBF')	)	) );
        $this->ci->excel->getActiveSheet()->getStyle('A2')->applyFromArray( array(
            'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'BFBFBF')	)	) );

        $this->ci->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
        $this->ci->excel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);

        $this->ci->excel->getActiveSheet()->mergeCells('B3:E3');
        
        //make the font become bold
        $this->ci->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->ci->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);


        //----------Order data related information row start-------------//

        $geColNames = $this->columRange;

        $iCount = 0;
        foreach($geColNames as $colPalaceNames){
            // get cell name
            $getCellName = $colPalaceNames.'6';
            //set registrant type listing
            $this->ci->excel->getActiveSheet()->setCellValue($getCellName, $this->headingTitle[$iCount]);
            //set text bold
            $this->ci->excel->getActiveSheet()->getStyle($getCellName)->getFont()->setBold(true);
            $this->ci->excel->getActiveSheet()->getStyle($getCellName)->getFont()->setSize(10);
            
            //set background color in cell of header
            $this->ci->excel->getActiveSheet()->getStyle($getCellName)->applyFromArray( array(
                    'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'BFBFBF')	)	) );
                    
            $iCount++;
        }

        
        if(!empty($this->exportData)){
            $rowStart = '7';
            $seriCount = '1';
            foreach($this->exportData as $rowData){
                $iCount = 0;
                $getRowData = (array) $rowData;
                
                //---------set serial number------------
                $getCellName = $geColNames[$iCount].$rowStart;
                //set serial number value of type listing
                $this->ci->excel->getActiveSheet()->setCellValue($getCellName, $seriCount);
                $this->ci->excel->getActiveSheet()->getStyle($getCellName)->getFont()->setSize(10);
                
                $iCount = 0; // set start one se
                foreach($getRowData as $colData){
                    // get cell name
                    $getCellName = $geColNames[$iCount].$rowStart;
            
                    //set value of type listing
                    $this->ci->excel->getActiveSheet()->setCellValue($getCellName, $colData);
                    $this->ci->excel->getActiveSheet()->getStyle($getCellName)->getFont()->setSize(10);
                    $iCount++;
                }
                $rowStart++;
                $seriCount++;
            }
        }


        header('Content-type: text/csv'); //mime type
        header('Content-Disposition: attachment;filename="'.$this->filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->ci->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        //$filefullPath =  '../uploads/reports/'.$filename;
        //$objWriter->save($filefullPath);
    }
    
   /*
    *  @Description: This method is use to export pdf file
    *  @auther: lokendra meena
    *  @return: void
    */ 
    
    /*public function pdfexportmpdf(){
        
        //-----------load libraray------------
        $this->ci->load->library(array('MPDF/mpdf'));
        
        $mpdf = new mPDF('utf-8', 'A2', 0, '', 12, 12, 10, 10, 5, 5);
        
        //----------load pdf view-----------
        $data['reportName']         =   $this->reportName;
        $data['reportTitle']        =   $this->reportHeading;
        $data['reportGenerateDate'] =   $this->reportGenerateDate;
        $data['headingTitle']       =   $this->headingTitle;
        $data['exportData']         =   $this->exportData;
        $data['columRange']         =   $this->columRange;
        $htmView = $this->ci->load->view('pdf_report_list_export_view',$data,true);    
        
        $mpdf->SetAuthor('grabbr'); 
        //$mpdf->PDFX = true;
        $mpdf->PDFAauto = true;
        $mpdf->PDFXauto = true;
        $mpdf->WriteHTML($htmView);
        $mpdf->Output($this->filename, 'I');
    }*/
    
    public function pdfexport(){  
        // get max pdf records count
		$records_per_pdf = $this->ci->config->item('records_per_pdf');
		if(count($this->exportData) > $records_per_pdf) {
			// generate and export multiple pdf and wrap in zip
			$this->exportmultipdf();
		} else {
			// single pdf report export
			$this->handlepdfexport();
		}
    }
    
    public function planpdfexport(){  
        // get max pdf records count
		$records_per_pdf = $this->ci->config->item('records_per_pdf');
		// single pdf report export
		
		return $this->handleplanpdfexport();
    }
    
    //-------------------------------------------------------------------------
    
   /**
	* @Description: Generate pdf 
	* @input  : array recordListing
	* @access : private
	*/
	private function handlepdfexport($is_multi_pdf=0,$file_path='') {
		ob_start();
        //-----------load libraray------------
        require_once('tcpdf/examples/tcpdf_include.php');
        
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
        $pdf->SetAuthor('Grabbr');
        // set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        
         //----------load pdf view-----------
        $data['reportName']         =   $this->reportName;
        $data['reportTitle']        =   $this->reportHeading;
        $data['reportGenerateDate'] =   $this->reportGenerateDate;
        $data['headingTitle']       =   $this->headingTitle;
        $data['exportData']         =   $this->exportData;
        $data['columRange']         =   $this->columRange;
        $htmView = $this->ci->load->view('pdf_report_list_export_view',$data,true);       
        //echo $htmView;die();
        
        $pdf->SetFont('dejavusans', '', 8, '', true);
        
        // ---------------------------------------------------------

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage( 'L', 'LETTER' );
        
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        // set auto page breaks
        $pdf->writeHTMLCell(0, 0, '', '', $htmView, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        if(!empty($is_multi_pdf) && !empty($file_path)) {
			$pdf->Output($file_path.'/'.$this->filename, 'F');
		} else {
			$pdf->Output($this->filename, 'I');
		}
	}
    //-------------------------------------------------------------------------
    
   /**
	* @Description: Generate plan pdf 
	* @input  : array recordListing
	* @access : private
	*/
	private function handleplanpdfexport($is_multi_pdf=0,$file_path='') {
		ob_start();
        //-----------load libraray------------
        require_once('tcpdf/examples/tcpdf_include.php');
        
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
        $pdf->SetAuthor('Grabbr');
        // set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        
         //----------load pdf view-----------
        $data['reportName']         =   $this->reportName;
        $data['reportTitle']        =   $this->reportTitle;
        $data['reportRestName']        =   $this->reportRestName;
        $data['reportDescription']  =   $this->reportDescription;
        $data['reportInvestAmount']  =   $this->reportInvestAmount;
        $data['reportInvestDuration']  =   $this->reportInvestDuration;
        $data['reportInvestRoi']  =   $this->reportInvestRoi;
        $data['reportInvestFoi']  =   $this->reportInvestFoi;
        $data['reportGenerateDate'] =   $this->reportGenerateDate;
        $data['headingTitle']       =   $this->headingTitle;
        $data['exportData']         =   $this->exportData;
        $data['columRange']         =   $this->columRange;
        
        $htmView = $this->ci->load->view('pdf_plan_report',$data,true);       
        
        
        $pdf->SetFont('dejavusans', '', 8, '', true);
        
        // ---------------------------------------------------------

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage( 'L', 'LETTER' );
        
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        // set auto page breaks
        $pdf->writeHTMLCell(0, 0, '', '', $htmView, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        //~ if(!empty($is_multi_pdf) && !empty($file_path)) {
        $file_path = FCPATH.'uploads/investmentplan/';
        //echo $file_path;die;
			$pdf->Output($file_path.'/'.$this->filename, 'F');
		//~ } else {
			//$pdf->Output($this->filename, 'I');
		//}
		return $this->filename;
	}
    
    //-------------------------------------------------------------------------
    
   /**
	* @Function : Generate multiple pdf and wrap in zip folder 
	* @input  : null
	* @output : void
	* @access : private
	*/
	private function exportmultipdf() {
		// set requested record params
		$export_records = $this->exportData;
		$zip_folder_name = $this->reportName;
		// replace space from report name
		$zip_folder_name = str_replace(' ', '-', $zip_folder_name);  
		// get logged user id
		$user_id = $this->ci->session->userData('userId');
		// create pdf folder
		$user_pdf_path = FCPATH.'uploads/'.encode($user_id);
		$zip_path = $user_pdf_path.'/'.$zip_folder_name;
		$zip_file_path = $zip_path.'.zip';
		if(file_exists($zip_file_path)){
			unlink($zip_file_path);
		}
		if(file_exists($zip_path)){
			$this->rrmdir($zip_path);
		}
		if(!is_dir($user_pdf_path)) {
			mkdir($user_pdf_path);
			chmod($user_pdf_path,0777);
		}
		if(!is_dir($zip_path)) {
			mkdir($zip_path);
			chmod($zip_path,0777);
		}
		// get max pdf records count
		$records_per_pdf = $this->ci->config->item('records_per_pdf');
		// seprate records into chunks
		$records_chunk = array_chunk($export_records,$records_per_pdf);
		// generate multiple pdf as per chunk key's
		$i=1;
		foreach($records_chunk  as $key=>$chunk) {
			// set pdf file params
			$filename  = $zip_folder_name.'-part'.$i.'-'.date('d-M-Y').'.pdf';
			$this->ci->report_export->filename   = $filename;
			$this->ci->report_export->exportData = $chunk;
			// pdf report export
			$this->handlepdfexport(1,$zip_path);
			$i++;
		}
		// produce file/ folder in zip folder
		$this->zipdata($zip_path,$zip_file_path);
		$path = base_url('uploads/'.encode($user_id).'/'.$zip_folder_name.'.zip');
		$name = end(explode('/',$path));
		set_time_limit(0);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $path);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$exe_ch = curl_exec($ch);
		curl_close($ch);
		$mime = get_mime_by_extension($path);
		
		header('Expires: 0'); // no cache
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
		header('Cache-Control: private', false);
		header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
		header('Content-Disposition: attachment; filename="' . $name . '"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . strlen($exe_ch)); // provide file size
		header('Connection: close');
		echo $exe_ch;die;
	}
	
	//-------------------------------------------------------------------------
	
	/**
	* @Function : To make zip file for give directory path 
	* @input  : folder , zipTo
	* @output : Object/Bool
	*/
	private function zipdata($folder, $zipTo) {
		if (extension_loaded('zip')) {
			if (file_exists($folder)) {
				$zip = new ZipArchive();
				if ($zip->open($zipTo, ZIPARCHIVE::CREATE)) {
					$folder = realpath($folder);
					if (is_dir($folder)) {
						$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder), RecursiveIteratorIterator::SELF_FIRST);
						foreach ($files as $file) {
							//$file = realpath($file);
							if(realpath($file)) {
								$file = str_replace('\\', '/', $file);;
							
								if (is_dir($file)) {
									$zip->addEmptyDir(str_replace($folder . '/', '', $file . '/'));
								} 
								else if (is_file($file)) {
									$zip->addFromString(str_replace($folder . '/', '', $file), file_get_contents($file));
								}
							}
						}
					} else if (is_file($folder)) {
					$zip->addFromString(basename($folder), file_get_contents($folder));
					}
				}
				@chmod($zipTo,0777);
				return $zip->close();
			}
		}
		return false;
	}
	
	//-------------------------------------------------------------------------
	
   /**
	* @Function : Delete Directory from loca filesystem recursively 
	* @input	: string(directory path) 
	* @Output	: bool	
	*/
	function rrmdir($dir) {
		$structure = glob(rtrim($dir, "/").'/*');
		if (is_array($structure)) {
			foreach($structure as $file) {
				if (is_dir($file)) $this->rrmdir($file);
				elseif (is_file($file)) unlink($file);
			}
		}
		rmdir($dir);
	   return true;
	} 
    
}
