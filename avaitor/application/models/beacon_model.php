<?php 

Class Beacon_model extends CI_Model{
    
     /**
	 * Name of the table
	 * @access protected 
	 * @var string
	 */	 
	 
	protected $tblbeacons = 'beacons';    
	protected $tbloffers = 'beacon_offers';    
  
    function __construct(){	
        parent::__construct();    
        $this->read_db = $this->load->database('read', TRUE); //Loading read database.
        //$this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // define tables
		$this->beacons_table = 'beacons';
		$this->stores_log_table = 'stores_log';
		$this->settings_table = 'settings';
		$this->app_settings_table = 'app_settings';
		$this->kiosk_settings_table = 'kiosk_settings';
    }    
	/*Offer : Start*/
	function getOfferList($id = '') {    // get the list of offer to show in offer view list
		$this -> read_db -> select('beacon_offers.*');
		if ($id) {
			$this -> read_db -> where("beacon_offers.beacon_offer_id", $id);
			$result = $this -> read_db -> get('beacon_offers');
			return $result -> row();
		}
		$this -> read_db -> select('beacons.beacon_code');
		$this -> read_db -> where(array('beacon_offers.is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		$this -> read_db -> join('beacons','beacons.beacon_id = beacon_offers.beacon_id');
		$this -> read_db -> order_by('beacon_offers.beacon_offer_id', 'desc');
		$result = $this -> read_db -> get('beacon_offers');
		return $result -> result_array();
	}
	
	
	
	function getBeaconList() {		// beacon list to show is drop down 			
		$select = array(
			'beacons.beacon_id',
			'beacon_code',
		);
		$this -> db -> select($select);
		$this -> db -> where(array('status'=>'1','is_delete'=>'0'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> db -> get('beacons');
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select beacon';
			foreach($result -> result_array() as $row) {
				$return[$row['beacon_id']] = $row['beacon_code'];
			}
		}
		return $return;
	}
	
	function getStoreList() {		// store list to show is drop down
		$select = array(
			'STORTBL.STOR_STORE',
			'STOR_DESC',
		);
		$this -> mssql_db -> select($select);
		$this -> mssql_db -> where(array('STOR_STATUS'=>'Active'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> mssql_db -> get('STORTBL');
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['STOR_STORE']] = $row['STOR_DESC'];			
			}
		}
		return $return;
	}
	
	function check_store($data) {        // checking that beacon is available or not 
		$beacon_id	= $data['beacon_id'] ;
		$store_id	= $data['store_id'] ;
		$sql 		= "select * from ava_beacons where is_delete = '0' AND status = '1' AND beacon_id != '".$beacon_id."' AND store_id = '".$store_id."'";
		$query 	= $this->db->query($sql);
		return $query->num_rows();
	}
	
	function get_active_beacon_offer($data) {        // checking that beacon is available or not 
		$beacon_id	= $data['beacon_id'] ;
		$store_id	= ",".$data['store_id']."," ;
		$this -> read_db -> select('beacon_offers.*');
		$this -> read_db -> where(array('beacon_offers.status'=>'1','beacon_offers.is_delete' => '0')); 
		$this -> read_db -> like('store_id', $store_id); 
		$result = $this -> read_db -> get('beacon_offers');
		return $result->num_rows();
	}
	
	/**
	 * Get listing of beacons
	 * @input : beacon_id
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_beacons_listing($beacon_id = 0) {
		
		$this -> read_db -> select('beacons.*');
		if ($beacon_id) {
			$this -> read_db -> where("beacon_id", $beacon_id);
			$result = $this -> read_db -> get($this->beacons_table);
			return $result -> row();
		}
		$this -> read_db -> select('stores_log.store_name');
		$this -> read_db -> where(array('beacons.is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		$this -> read_db -> join('stores_log','stores_log.store_id = beacons.store_id','left');
		$this -> read_db -> order_by('beacons.beacon_id', 'desc');
		$result = $this -> read_db -> get('beacons');
		return $result -> result_array();
		
	}
	
	/**
	 * Function to manage insertion and updation of beacon data
	 * @input : data
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function manage_store_beacon($data) {   
		if($data['beacon_id']) { // update data
			$this->db->where('beacon_id', $data['beacon_id']);
			$this->db->update($this->beacons_table, $data);
			return $data['beacon_id'];
		} else { // add data
			$this->db->insert($this->beacons_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Get all store log listing
	 * @access: public
	 * 
	 */
	public function get_store_list() {
		
		/*$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> order_by('store_name', 'ASC');
		$result = $this -> read_db -> get($this->stores_log_table);*/
		
		$sql = "SELECT * FROM (`ava_stores_log`) WHERE `status` = 1 and `store_name` <> '' and SubString(store_name, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY `store_name` ASC";
		$result 	= $this->db->query($sql);
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['store_id']] = $row['store_name'];			
			}
		}
		return $return;
	}
	
	/**
	 * Function to manage insertion and updation of beacon setting
	 * @input : beacon_notification_time , form_action
	 * @output: int
	 * @auther: Tosif Qureshi
	 */
	public function save_beacon_setting($beacon_notification_time,$form_action) { 
		if($form_action == 'insert') { // insert data
			$this->db->insert($this->settings_table, array('name'=>'beacon_notification_time','module'=>'beacon','value'=>$beacon_notification_time));
		} else { // update data
			$this->db->where('name', 'beacon_notification_time');
			$this->db->update($this->settings_table, array('value'=>$beacon_notification_time));
		}
		return true;
	}
	
	/**
	 * Get beacon setting data
	 * @input : setting_name
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_beacons_setting($setting_name = '') {
		
		$this -> read_db -> select('*');
		$this -> read_db -> where("name", $setting_name);
		$result = $this -> read_db -> get($this->settings_table);
		return $result -> row();
	}
	
    /**
	 * Function to manage insertion and updation of cart setting
	 * @input : cart_total_product , form_action
	 * @output: int
	 * 
	 */
	public function save_cart_setting($cart_product_limit,$form_action) { 
		if($form_action == 'insert') { // insert data
			$this->db->insert($this->settings_table, array('name'=>'cart_product_limit','module'=>'beacon','value'=>$cart_total_product));
		} else { // update data
			$this->db->where('name', 'cart_product_limit');
			$this->db->update($this->settings_table, array('value'=>$cart_product_limit));
		}
		return true;
	}
    
    /**
	 * Function to manage insertion and updation of tablet setting
	 * @input : uplock_password , form_action
	 * @output: string
	 * 
	 */
	public function save_tablet_setting($unlock_password,$form_action) { 
		
		if($form_action == 'insert') { // insert data
			$this->db->insert($this->settings_table, array('name'=>'unlock_password','module'=>'tablet','value'=>$unlock_password));
		} else { // update data
			$this->db->where('name', 'unlock_password');
			$this->db->where('module', 'tablet');
			$this->db->update($this->settings_table, array('value'=>$unlock_password));
		}
		return true;
	}
	
	/**
	 * Function to manage insertion and updation of tablet setting
	 * @input : array data , string form_action
	 * @output: string
	 * 
	 */
	public function save_tablet_version_setting($data,$form_action) {
		
		if($form_action == 'insert') { // insert data
			$this->db->insert($this->app_settings_table,$data);
		} else { // update data
			$this->db->where('app_type', 2);
			$this->db->where('device_type', 'android');
			$this->db->update($this->app_settings_table, $data);
		}
		return true;
	}
	
	/**
	 * Get app setting data
	 * @input : setting_name
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_app_setting($device_type='',$app_type=0) {
		
		$this -> read_db -> select('*');
		$this -> read_db -> where("device_type", $device_type);
		$this -> read_db -> where("app_type", $app_type);
		$result = $this -> read_db -> get($this->app_settings_table);
		return $result -> row();
	}		
	
	/**
	 * Get app setting data
	 * @input : setting_name
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_kiosk_setting() {
		
		$this -> read_db -> select('*');
		$result = $this -> read_db -> get($this->kiosk_settings_table);
		return $result -> row();
	}	
					
	/**
	 * Function to manage insertion and updation of kiosk setting
	 * @input : array data , string form_action
	 * @output: string
	 * 
	 */
	public function save_kiosk_setting($data,$form_action) {
		
		if($form_action == 'insert') { // insert data
			$this->db->insert($this->kiosk_settings_table,$data);
		} else { // update data
			$this->db->update($this->kiosk_settings_table, $data);
		}
		return true;
	}				
 
}
