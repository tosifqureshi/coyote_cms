<?php 

Class Beacon_offers_model extends CI_Model{
    
     /**
	 * Name of the table
	 * @access protected 
	 * @var string
	 */	 
	 
	protected $tblbeacons = 'beacons';    
	protected $tbloffers = 'beacon_offers';    
  
	
    
    function __construct(){	
        parent::__construct();    
        $this->read_db = $this->load->database('read', TRUE); //Loading read database.
        //$this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // set tables
		$this->stores_log_table = 'stores_log';
		$this->categories_table = 'categories';
		$this->beacons_table = 'beacons';
		$this->beacon_offer_table = 'beacon_offers';
		$this->beacon_customer_offer_table = 'beacon_customer_offer';
    }    
	/*Offer : Start*/
	function getOfferList($id = '') {    // get the list of offer to show in offer view list
		$this -> read_db -> select('beacon_offers.*');
		if ($id) {
			$this -> read_db -> select('beacons.beacon_code');
			$this -> read_db -> where("beacon_offers.beacon_offer_id", $id);
			$this -> read_db -> join('beacons','beacons.beacon_id = beacon_offers.beacon_id','left');
			$result = $this -> read_db -> get('beacon_offers');
			return $result -> row();
		}
		$this -> read_db -> select('beacons.beacon_code');
		$this -> read_db -> where(array('beacon_offers.is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		$this -> read_db -> join('beacons','beacons.beacon_id = beacon_offers.beacon_id');
		$this -> read_db -> order_by('beacon_offers.beacon_offer_id', 'desc');
		$result = $this -> read_db -> get('beacon_offers');
		return $result -> result_array();
	}
	
	function add_offer($data) {    //function is used in both case edit and update
		if($data['beacon_offer_id']) {											// edit data w.r.t. to beacon_offer_id		
			$this->db->where('beacon_offer_id', $data['beacon_offer_id']);
			$this->db->update('beacon_offers', $data);
			return $data['beacon_offer_id'];
		} else {														// add data
			$this->db->insert('beacon_offers', $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Update customer's offer seen count
	 * @input: offer_data, offer_id
	 * @output: void
	 */
	function update_offer_seen_count($data,$offer_id=0) {    
		if(!empty($offer_id)) {								
			$this->db->where('beacon_offer_id', $offer_id);
			$this->db->update($this->beacon_customer_offer_table, $data);	
		}
		return true;
	}
	
	
	function getBeaconList($store_id=0) {		// beacon list to show is drop down 			
		$select = array(
			'beacons.beacon_id',
			'beacon_code',
		);
		$this -> db -> select($select);
		$this -> db -> where('is_delete','0');	// status=>1 : active and is_delete=>0 : data is not delete
		$this -> db -> where('status',1);
		if(!empty($store_id)) {
			$this -> db -> where('store_id',$store_id);
		}
		$result = $this -> db -> get('beacons');
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select beacon';
			foreach($result -> result_array() as $row) {
				$return[$row['beacon_id']] = $row['beacon_code'];
			}
		}
		return $return;
	}
	
	function getStoreList() {		// store list to show is drop down
		$select = array(
			'STORTBL.STOR_STORE',
			'STOR_DESC',
		);
		$this -> mssql_db -> select($select);
		$this -> mssql_db -> where(array('STOR_STATUS'=>'Active'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> mssql_db -> get('STORTBL');
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['STOR_STORE']] = $row['STOR_DESC'];			
			}
		}
		return $return;
	}
	
	function check_beacon($data) {        // checking that store is available or not 		
		$start_time	= $data['start_time'];
		$end_time 	= $data['end_time'];
		$start_date = date("Y-m-d",strtotime($data['start_date']));
		$end_date 	= date("Y-m-d",strtotime($data['end_date']));
		$store_id	= $data['store_id'] ;
		$beacon_offer_id	= (!empty($data['beacon_offer_id'])) ? $data['beacon_offer_id'] : 0;
		//$sql 	= "select * from ava_beacon_offers where is_simple_offer = '0' AND is_delete = '0' AND status = '1' AND store_id LIKE '%,".$store_id.",%' AND beacon_offer_id != '".$beacon_offer_id."' AND (((start_date BETWEEN '".$start_date."' AND '".$end_date."') OR (end_date BETWEEN '".$start_date."' AND '".$end_date."')) AND ((start_time BETWEEN '".$start_time."' AND '".$end_time."') OR (end_time BETWEEN '".$start_time."' AND '".$end_time."')))";
		$sql 	= "select * from ava_beacon_offers where is_simple_offer = '0' AND is_delete = '0' AND status = '1' AND store_id LIKE '%,".$store_id.",%' AND beacon_offer_id != '".$beacon_offer_id."' AND (((start_date >= '".$start_date."' AND start_date <= '".$end_date."') OR (end_date >= '".$start_date."' AND end_date <=  '".$end_date."') OR (start_date <= '".$start_date."' AND end_date >= '".$end_date."')) AND ((start_time <= '".$start_time."' AND end_time >= '".$end_time."' ) OR (end_time >= '".$start_time."' AND start_time <= '".$start_time."') OR (start_time >= '".$start_time."' AND end_time <= '".$end_time."')))";
		$query 	= $this->db->query($sql);
		return $query->num_rows();
	}
	
	/**
	 * Get listing of default beacon offers
	 * @input : store_id , beacon_offer_id
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function check_default_beacon_offer($data) {      // checking that store is available or not 		
		$store_id	= $data['store_id'];
		$beacon_offer_id	= $data['beacon_offer_id'];
		$sql 	= "select * from ava_beacon_offers where is_default = '1' AND is_delete = '0' AND status = '1' AND store_id LIKE '%,".$store_id.",%' AND beacon_offer_id != '".$beacon_offer_id."'";
		$query 	= $this->db->query($sql);
		return $query->num_rows();
	}
	
	/**
	 * Get listing of offers basis of type
	 * @input : offer_id , offer_type
	 * @output: array
	 * @auther: Tosif Qureshi
	 */
	function get_offer_listing($offer_id = 0,$offer_type=0,$is_default=0) { 
		$this -> read_db -> select('beacon_offers.*');
		if (!empty($offer_id)) {
			$this -> read_db -> where("beacon_offers.beacon_offer_id", $offer_id);
			$result = $this -> read_db -> get('beacon_offers');
			return $result -> row();
		}
		$this -> read_db -> select('stores_log.store_name');
		$this -> read_db -> select('categories.category_name');
		if($is_default == 1) {
			$this -> read_db -> where(array('beacon_offers.is_delete'=>'0','beacon_offers.is_default'=>$is_default));   // status=>1 : active and is_delete=>0 : data is not delete
		} else {
			$this -> read_db -> where(array('beacon_offers.is_delete'=>'0','beacon_offers.is_default'=>'0','beacon_offers.is_simple_offer'=>$offer_type));   // status=>1 : active and is_delete=>0 : data is not delete
		}
		$this -> read_db -> join('stores_log','stores_log.store_id = beacon_offers.store_id', 'left');
		$this -> read_db -> join('categories','categories.category_id = beacon_offers.category_id', 'left');
		$this -> read_db -> order_by('beacon_offers.beacon_offer_id', 'desc');
		$result = $this -> read_db -> get('beacon_offers');
		return $result -> result_array();
	}
	
	/**
	 * Get all store log listing
	 * @access: public
	 * 
	 */
	public function get_store_list($check= '') {
		$beaconStores = $this->get_active_beacon_store();// get active store associated with beacon which is to show in dropdown
		$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> where('store_name !=','');
		//$beacon_stores = Array([0] => 9,[1] => 7,[2] => 1);
		if($check == 'beacon') {
			$this -> read_db -> where_in('store_id', $beaconStores);
		}
		$this -> read_db -> order_by('store_name', 'ASC');
		$result = $this -> read_db -> get($this->stores_log_table);
		$return = array();
		if($result->num_rows() > 0){
				//$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['store_id']] = $row['store_name'];			
			}
		}
		return $return;
	}
	
	/*
	 * */
	 
	public function get_store_names($store_id=array()) {
		$this -> read_db -> select('store_name');
		$this -> read_db -> where_in('store_id', $store_id);
		$result = $this -> read_db -> get($this->stores_log_table);
		//echo $this->read_db->last_query();die;
		return $result -> result_array();
	}
	
	/**
	 * Get all active store associated with beacon
	 * @access: public
	 * 
	 */
	public function get_active_beacon_store() {
		$this -> read_db -> select('store_id');
		$this -> read_db -> where(array('status'=>1,'is_delete'=>0));
		$result = $this -> read_db -> get($this->beacons_table);
		$return = array();
		if($result->num_rows() > 0){
			foreach($result -> result_array() as $row){
				$return[] = $row['store_id'];			
			}
		}
		return $return;
	}
	
	
	
	/**
	 * Get all category listing
	 * @access: public
	 * 
	 */
	public function get_category_list() {
		
		$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> where('is_deleted',0);
		$result = $this -> read_db -> get($this->categories_table);
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Category';
			foreach($result -> result_array() as $row){
				$return[$row['category_id']] = $row['category_name'];			
			}
		}
		return $return;
	}
	
	/**
	 * Get all store beacons
	 * @access: public
	 * 
	 */
	public function get_store_beacons($store_id=0) {
		
		$this -> read_db -> select('*');
		$this -> read_db -> where('store_id',$store_id); 
		$result = $this -> read_db -> get($this->beacons_table);
		return $result -> result_array();
	}
	
	/**
	 * Get beacon data of store
	 * @access: public
	 * 
	 */
	public function get_store_beacon($store_id=0,$beacon_offer_id=0) {
		
		$this -> read_db -> select('beacons.*');
		$this -> read_db -> select('beacon_offers.beacon_offer_id,beacon_offers.status as offer_status,beacon_offers.is_delete  as offer_deleted,beacon_offers.is_simple_offer');
		$this -> read_db -> where('beacons.store_id',$store_id); 
		$this -> read_db -> where('beacons.status',1); 
		$this -> read_db -> where('beacons.is_delete',0);
		if(!empty($beacon_offer_id)) {
			$this -> read_db -> join('beacon_offers','beacon_offers.store_id = beacons.store_id AND ava_beacon_offers.is_simple_offer = 0 AND ava_beacon_offers.beacon_offer_id != '.$beacon_offer_id,'left');
		} else {
			$this -> read_db -> join('beacon_offers','beacon_offers.store_id = beacons.store_id AND ava_beacon_offers.is_simple_offer = 0','left');
		}
		$result = $this -> read_db -> get('beacons');
		return $result -> row();
	}
	
	
	/**
	 * Get barcode data
	 * @access: public
	 * 
	 */
	public function get_barcode_data($barcode=0,$is_simple_offer=0,$id) {
		
		$this -> read_db -> select('beacon_offer_id');
		$this -> read_db -> where('barcode',$barcode); 
		$this -> read_db -> where('is_simple_offer',$is_simple_offer); 
		$this -> read_db -> where_not_in('beacon_offer_id',$id);
		$result = $this -> read_db -> get($this->beacon_offer_table);
		//echo $this->read_db->last_query(); die;
		return $result->num_rows();
	}
	
	/*Offer : End*/
	
	
				
				
 
}
