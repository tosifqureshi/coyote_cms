<?php 

Class Common_model extends CI_Model{
    
    function __construct(){
        parent::__construct();    
        $this->read_db = $this->load->database('read', TRUE); //Loading read database.
        $this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // set tables
        $this->snw_product_tbl = 'scratch_n_win_products';
		$this->snw_product_sent_tbl = 'scratch_n_win_product_sent_log';
		$this->users_tbl = 'users';
		$this->beacon_tbl = 'ava_beacons';
		$this->beacon_offer_tbl = 'ava_beacon_offers';
		$this->beacon_customer_tbl = 'ava_beacon_customer_offer';
		$this->store_tbl = 'ava_stores_log';

    }  
    
    /**
	 * Get total claimed product count
	 * @input: snw_id , product_id
	 * @access: public
	 * 
	 */
	public function get_prize_claimed_count($snw_id=0,$product_id=0) {
		
		$this -> read_db -> select('sum(product_sent_count) as product_sent_count');
		$this -> read_db -> where("snw_id", $snw_id);
		$this -> read_db -> where("product_id", $product_id);
		$result = $this -> read_db -> get($this->snw_product_sent_tbl);
		$result_row = $result -> row();
		$return = '';
		if(!empty($result_row)) {
			$return = $result_row->product_sent_count;
		}
		return $return;
	}	
	
	/**
	 * Function to get product details from product id
	 * @input : product_id
	 * @output: obj array
	 * @author: Tosif Qureshi
	 */
	public function get_product_data($product_id) {
		// get product's result
		$product_result = $this->mssql_db->query("select Prod_Desc, Prod_Number,APN_NUMBER from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT join APNTBL on Prod_Number = APN_PRODUCT where OUTP_STATUS = 'active' and Prod_Number = $product_id and APN_PRODUCT = $product_id");
		return $product_result -> row();
	}
	
	/**
	 * Function to get data from table
	 * @input : table,field,whereField,whereValue,orderBy,order
	 * @output: obj array
	 * @author: Tosif Qureshi
	 */
	function getDataFromTabel($table='', $field='*',  $whereField='', $whereValue='', $orderBy='', $order='ASC', $limit=0, $offset=0, $resultInArray=false,$isAssoMember=0,$isrow=false){
		
		 $this->read_db->select($field);
		 $this->read_db->from($table);
		 
		if(is_array($whereField)){
			$this->read_db->where($whereField);
		}elseif(!empty($whereField) && $whereValue != ''){
			$this->read_db->where($whereField, $whereValue);
		} elseif($isAssoMember==1) {
            $this->read_db->where($whereField,NULL,FALSE);
        }
		if(!empty($orderBy)){  
			$this->read_db->order_by($orderBy, $order);
		}
		if($limit > 0){
			$this->read_db->limit($limit,$offset);
		}
		$query = $this->read_db->get();
		if($resultInArray){
			$result = $query->result_array();
		}else{
			$result = $query->result();
		}
		if($isrow){
		   $result = $query->row_array();
		}
		
		if(!empty($result)){
			return 	$result;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	 * @description: This function is used addDataIntoTabel
	 * $.$
	 */
	function addDataIntoTable($table='', $data=array()){
		$table=$table;
		if($table=='' || !count($data)){
			return false;
		}
		$inserted = $this->db->insert($table , $data);
		$this->db->last_query();
		$ID = $this->db->insert_id();
		return $ID;
	}
	
	/*
	 * @description: This function is used updateDataFromTabel
	 * $.$
	 */
	function updateDataFromTable($table='', $data=array(), $field='', $ID=0){
		$table=$table;
		if(empty($table) || !count($data)){
			return false;
		}
		else{
			if(is_array($field)){
				
				$this->db->where($field);
			}else{
				$this->db->where($field , $ID);
			}
			return $this->db->update($table , $data);
		}
	}
	
	/**
	 * Function to get users count from user tbl
	 * @output: coulumn count
	 * @author: $$
	 */
	public function get_users_count() {
		// get product's result
		$result = $this->read_db->query(
			   "SELECT SUM(if(a.role_id = 1, 1, 0)) AS count_admin, 
				SUM(if(a.role_id = 4, 1, 0)) AS count_user, 
				SUM(if(a.role_id = 7, 1, 0)) AS count_marketing, 
				SUM(if(a.device_type = 'android', 1, 0)) AS count_android,
				SUM(if(a.device_type= 'ios', 1, 0)) AS count_ios,
				SUM(if(a.device_type = '', 1, 0)) AS count_other,
				SUM(if(a.active = 1, 1, 0)) AS count_active,
				SUM(if(a.deleted = 1, 1, 0)) AS count_deleted
				FROM (ava_users a)
				LEFT JOIN ava_roles b ON b.role_id = a.role_id
				LEFT JOIN ava_stores_log c ON c.store_id = a.store_id"
			);
		return $result->row();
	}
	
	/**
	 * Function to get In stores offers count from becon_offer tbl
	 * @output: coulumn count
	 * @author: $$
	 */
	public function stores_offers_count() {
		// get product's result
		   $result = $this->read_db->query(
		   "SELECT 
		    SUM(if(is_simple_offer = 0 AND status = 1 AND is_default = 0, 1, 0)) AS count_beacon_offers, 
		    SUM(if(is_simple_offer = 0 AND status = 1 AND is_default = 1, 1, 0)) AS count_fallback_beacon_offers, 
			SUM(if(is_simple_offer = 1 AND status = 1 AND is_delete = 0, 1, 0)) AS count_vip_offers, 
			SUM(if(is_simple_offer = 2 AND status = 1 AND is_delete = 0, 1, 0)) AS count_target_push_offers,				
			SUM(if(is_simple_offer = 0 AND status = 0 , 1, 0)) AS count_inactive_beacon_offers,				
			SUM(if(is_simple_offer = 1 AND status = 0 , 1, 0)) AS count_inactive_vip_offers,				
			SUM(if(is_simple_offer = 2 AND status = 0 , 1, 0)) AS count_inactive_target_push_offers				
			FROM ava_beacon_offers" 
			);
		return $result->row();
	}

	/**
	 * Function to get simnple offers count from offer tbl
	 * @output: coulumn count
	 * @author: $$
	 */
	public function simple_stores_offer_count() {
		// get product's result
		$result = $this->read_db->query("SELECT SUM(if(status = 1 , 1, 0)) AS count_simple_offers,SUM(if(status = 0, 1, 0)) AS count_inactive_simple_offers FROM ava_offers");
		return $result->row();
	}
	/**
	 * Function to get device count from device_address_log tbl
	 * @output: device count
	 * @author: $$
	 */
	public function device_count() {
		// ava_device_address_log
		$result = $this->read_db->query("SELECT SUM(if(status = 1, 1, 0)) AS count_simple_offers  FROM ava_device_address_log");
		return $result->row();
	}
	
	/**
	 * Function to get becones count from ava_beacons tbl
	 * @output: device count
	 * @author: $$
	 */
	public function beacons_count() {
		// ava_device_address_log
		$result = $this->read_db->query("SELECT SUM(if(status = 1 AND is_delete = 0, 1, 0)) AS count_active_beacons,SUM(if(status = 0 AND is_delete = 1, 1, 0)) AS count_inactive_beacons  FROM ava_beacons");
		return $result->row();
	}
	/**
	 * Function to get reward campaign count from ava_reward_campaign
	 * @output: reward campaign count
	 * @author: $$
	 */
	public function reward_campaign_count() {
		// ava_device_address_log
		$result = $this->read_db->query("SELECT SUM(if(status = 1 AND is_deleted = 0, 1, 0)) AS count_active_reward_campaign,SUM(if(status = 0 AND is_deleted = 1, 1, 0)) AS count_inactive_reward_campaign  FROM ava_reward_campaign");
		return $result->row();
	}
	/**
	 * Function to get events count from events
	 * @output: eventscount
	 * @author: $$
	 */
	public function events_counts() {
		// ava_device_address_log
		$result = $this->read_db->query("SELECT SUM(if(status = 1 AND is_delete = 0, 1, 0)) AS count_active_event,SUM(if(is_delete = 1, 1, 0)) AS count_inactive_event  FROM ava_events");
		return $result->row();
	}
	/**
	 * Function to get product loyalty count from loyalty
	 * @output: loyalty count
	 * @author: $$
	 */
	public function product_loyalty_count() {
		// ava_device_address_log
		$result = $this->read_db->query("SELECT SUM(if(status = 1 AND is_delete = 0, 1, 0)) AS count_active_loyalty,SUM(if(is_delete = 1, 1, 0)) AS count_inactive_loyalty  FROM ava_loyalty");
		return $result->row();
	}
	/**
	 * Function to get competitions count from competitions
	 * @output: competitions count
	 * @author: $$
	 */
	public function competitions_count() {
		// ava_device_address_log
		$result = $this->read_db->query("SELECT SUM(if(status = 1 AND is_deleted = 0 AND type = 1, 1, 0)) AS count_active_lucky,SUM(if(status = 1 AND is_deleted = 0 AND type = 2, 1, 0)) AS count_active_leader FROM ava_competitions");
		return $result->row();
	}
	
	/**
	 * Function to get reward campaign count from ava_reward_campaign
	 * @output: reward campaign count
	 * @author: $$
	 */
	public function scratch_n_win_winners() {
		// ava_device_address_log
		$result = $this->read_db->query(" 
										SELECT
										YEAR(created_at) AS YEAR,
										SUM(MONTH(created_at) = 1 AND is_second_chance = 0) AS win_January,
										SUM(MONTH(created_at) = 1 AND is_second_chance = 1) AS sc_January,
										SUM(MONTH(created_at) = 2) AS February,
										SUM(MONTH(created_at) = 3) AS March,
										SUM(MONTH(created_at) = 4) AS April,
										SUM(MONTH(created_at) = 5) AS May,
										SUM(MONTH(created_at) = 6) AS June,
										SUM(MONTH(created_at) = 7) AS July,
										SUM(MONTH(created_at) = 8) AS August,
										SUM(MONTH(created_at) = 9) AS September,
										SUM(MONTH(created_at) = 10) AS October,
										SUM(MONTH(created_at) = 11) AS November,
										SUM(MONTH(created_at) = 12) AS December
										FROM ava_scratch_n_win_prize_winners
										WHERE  created_at >= NOW() - INTERVAL 12 MONTH AND is_second_chance = 0 AND snw_product_id >0 
										GROUP BY 1"
										);
		return $result->row_array();
	}
    
	/**
	 * @campaign_compitition_log Function to get reward campaign count from ava_reward_campaign
	 * @input: campain_year(string)
	 * @output: reward campaign count
	 * @author: $$
	 */
	public function campaign_compitition_log($campain_year='') {
		// ava_device_address_log
		$result = $this->read_db->query(" 
			SELECT
			YEAR(created_at) AS YEAR,
			SUM(MONTH(created_at) = 1 AND is_second_chance = 0 AND status = 1) AS win_January,
			SUM(MONTH(created_at) = 1 AND is_second_chance = 1) AS sec_January,
			SUM(MONTH(created_at) = 1 AND status = 0) AS nowin_January,
			SUM(MONTH(created_at) = 2 AND is_second_chance = 0 AND status = 1) AS win_February,
			SUM(MONTH(created_at) = 2 AND is_second_chance = 1) AS sec_February,
			SUM(MONTH(created_at) = 2 AND status = 0) AS nowin_February,
			SUM(MONTH(created_at) = 3 AND is_second_chance = 0 AND status = 1) AS win_March,
			SUM(MONTH(created_at) = 3 AND is_second_chance = 1) AS sec_March,
			SUM(MONTH(created_at) = 3 AND status = 0) AS nowin_March,
			SUM(MONTH(created_at) = 4 AND is_second_chance = 0 AND status = 1) AS win_April,
			SUM(MONTH(created_at) = 4 AND is_second_chance = 1) AS sec_April,
			SUM(MONTH(created_at) = 4 AND status = 0) AS nowin_April,
			SUM(MONTH(created_at) = 5 AND is_second_chance = 0 AND status = 1) AS win_May,
			SUM(MONTH(created_at) = 5 AND is_second_chance = 1) AS sec_May,
			SUM(MONTH(created_at) = 5 AND status = 0) AS nowin_May,
			SUM(MONTH(created_at) = 6 AND is_second_chance = 0 AND status = 1) AS win_June,
			SUM(MONTH(created_at) = 6 AND is_second_chance = 1) AS sec_June,
			SUM(MONTH(created_at) = 6 AND status = 0) AS nowin_June,
			SUM(MONTH(created_at) = 7 AND is_second_chance = 0 AND status = 1) AS win_July,
			SUM(MONTH(created_at) = 7 AND is_second_chance = 1) AS sec_July,
			SUM(MONTH(created_at) = 7 AND status = 0) AS nowin_July,
			SUM(MONTH(created_at) = 8 AND is_second_chance = 0 AND status = 1) AS win_August,
			SUM(MONTH(created_at) = 8 AND is_second_chance = 1) AS sec_August,
			SUM(MONTH(created_at) = 8 AND status = 0) AS nowin_August,
			SUM(MONTH(created_at) = 9 AND is_second_chance = 0 AND status = 1) AS win_September,
			SUM(MONTH(created_at) = 9 AND is_second_chance = 1) AS sec_September,
			SUM(MONTH(created_at) = 9 AND status = 0) AS nowin_September,
			SUM(MONTH(created_at) = 10 AND is_second_chance = 0 AND status = 1) AS win_October,
			SUM(MONTH(created_at) = 10 AND is_second_chance = 1) AS sec_October,
			SUM(MONTH(created_at) = 10 AND status = 0) AS nowin_October,
			SUM(MONTH(created_at) = 11 AND is_second_chance = 0 AND status = 1) AS win_November,
			SUM(MONTH(created_at) = 11 AND is_second_chance = 1) AS sec_November,
			SUM(MONTH(created_at) = 11 AND status = 0) AS nowin_November,
			SUM(MONTH(created_at) = 12 AND is_second_chance = 0 AND status = 1) AS win_December,
			SUM(MONTH(created_at) = 12 AND is_second_chance = 1) AS sec_December,
			SUM(MONTH(created_at) = 12 AND status = 0) AS nowin_December
			FROM ava_scratch_n_win_prize_winners
			WHERE YEAR(created_at) = $campain_year"
		);
		return $result->row_array();
	}
	
	/**
	 * Function to get users count from ava_users
	 * @output: ios and android count
	 * @author: $$
	 **/
	public function joined_user_device_log($month_limit=0){
		$output = FALSE;
		if(!empty($month_limit)){
			$SQL  = "SELECT YEAR(created_on) AS YEAR";
			$count = 0;
			foreach($month_limit as $months){
				$mNumber = $months['mNumber'];
				$mName   = $months['mName'];
				$SQL  	 .= ",SUM(MONTH(created_on) = $mNumber AND device_type ='ios') AS IOS_$mName";
				$SQL    .= ",SUM(MONTH(created_on) = $mNumber AND device_type ='android') AS AND_$mName";
				$count++;
			}
			$SQL   .=" FROM ava_users WHERE  created_on >= NOW()-INTERVAL 6 MONTH AND role_id = 4 GROUP BY 1";
			$result = $this->read_db->query($SQL);
			$output = $result->row_array();
		}
		return $output;
	}
	
	public function joined_user_device_log_($month_limit=0,$joined_year=''){
		$output = FALSE;
		if(!empty($month_limit)){
			$SQL  = "SELECT YEAR(created_on) AS YEAR";
			$count = 0;
			foreach($month_limit as $months){
				$mNumber = $months['mNumber'];
				$mName   = $months['mName'];
				$SQL  	 .= ",SUM(MONTH(created_on) = $mNumber AND device_type ='ios') AS IOS_$mName";
				$SQL    .= ",SUM(MONTH(created_on) = $mNumber AND device_type ='android') AS AND_$mName";
				$count++;
			}
			$SQL   .=" FROM ava_users WHERE  YEAR(created_on) = $joined_year AND role_id = 4 GROUP BY 1";
			$result = $this->read_db->query($SQL);
			$output = $result->row_array();
		}
		return $output;
	}
	
	/**
	 * @description : Function used to get active beacon offers
	 * @input : null
	 * @output : array
	 * @access : public
	 **/
	 public function get_active_beacon_offers() {
		$sql = "SELECT beacon_offer_id,offer_name FROM ".$this->beacon_offer_tbl." WHERE status = '1' AND is_simple_offer = '0' AND is_delete = '0' AND is_default = '0' AND start_date <= '".date('Y-m-d')."' AND end_date >= '".date('Y-m-d')."'";
		$result = $this->read_db->query($sql);
		$output = $result->result_array();
		return $output;
	 }
	
	/**
	 * @description : Function used to get customers beacon view log
	 * @input : beacon_offer_id(int)
	 * @output : array
	 * @access : public
	 **/
	 public function get_customer_beacon_log($beacon_offer_id=0) {
		$sql = "SELECT bc.beacon_id,SUM(bc.offer_seen_count) as beacon_offer_seen_count, b.beacon_code, s.store_name FROM ".$this->beacon_customer_tbl." bc JOIN ".$this->beacon_tbl." b ON b.beacon_id = bc.beacon_id JOIN ".$this->store_tbl." s ON s.store_id = b.store_id WHERE bc.beacon_offer_id = $beacon_offer_id GROUP BY bc.beacon_id HAVING SUM(bc.offer_seen_count) > 0 ORDER BY beacon_offer_seen_count DESC LIMIT 5";
		$result = $this->read_db->query($sql);
		$output = $result->result_array();
		return $output;
	 }
 
}
