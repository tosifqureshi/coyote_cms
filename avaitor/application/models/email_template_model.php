<?php 
Class Email_template_model extends CI_Model{
     /**
	 * Name of the table
	 * @access protected 
	 * @var string
	 */	 
    function __construct(){
        parent::__construct();    
        $this->email_template_table = 'email_template';
    }    
	/*email template : Start*/
	function getEmailTemplateList($id = '',$purpose = '',$NotIn=0) {    // get the list of email template to show in email template view list
		$this->db->select('*');
		if(!empty($purpose)){
			$this->db->where('purpose',$purpose);
			$result = $this->db->get($this->email_template_table);
			return $result->row();
		}
		if(!empty($NotIn) && is_array($NotIn)){
			$this->db->where_not_in('template_type',$NotIn);
		}
		if ($id) {
			$this->db->where("id", $id);
			$result = $this->db->get($this->email_template_table);
			return $result->row();
		}
		$this->db->order_by('id', 'desc');
		$result = $this->db->get($this->email_template_table);
		//echo $this->db->last_query();die;
		return $result->result_array();
	}
	
	function addEmailTemplate($data) {    //function is used in both case edit and update
		if(isset($data['id'])) {											// edit data w.r.t. to email_template_id		
			$this->db->where('id', $data['id']);
			$this->db->update($this->email_template_table, $data);
			return $data['id'];
		} else {														// add data
			$this->db->insert($this->email_template_table, $data);
			return $this->db->insert_id();
		}
	} //email template 
}
