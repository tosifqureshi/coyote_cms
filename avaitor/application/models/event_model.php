<?php 
Class Event_model extends CI_Model{
     /**
	 * Name of the table
	 * @access protected 
	 * @var string
	 */	 
    function __construct(){
        parent::__construct();    
        $this->event_table = 'events';
        $this->event_gallery_table = 'event_gallery';
    }    
	/*Event : Start*/
	function getEventList($id = '') {    // get the list of Event to show in Event view list
		$this->db->select('*');
		if ($id) {
			$this->db->where("event_id", $id);
			$result = $this->db->get($this->event_table);
			return $result->row();
		}
		$this->db->where(array('is_delete'=>'0'));   // is_delete=>0 : data is not delete
		$this->db->order_by('event_id', 'desc');
		$result = $this->db->get($this->event_table);
		//echo $this->db->last_query();die;
		return $result->result_array();
	}
	
	function add_event($data) {    //function is used in both case edit and update
		if($data['event_id']) {											// edit data w.r.t. to Event_id		
			$this->db->where('event_id', $data['event_id']);
			$this->db->update($this->event_table, $data);
			return $data['event_id'];
		} else {														// add data
			$this->db->insert($this->event_table, $data);
			return $this->db->insert_id();
		}
	}
	/*
	 * function delete the galary images w.r.t event id and add/update new images
	 * */
	function add_gallary_image($data,$event_id) { 
		//print_r($data);die;
		if($event_id && !empty($data)) {
			for($i=2;$i<=7;$i++) {
				$newdata['image'] = '';
				$newdata['event_id'] = $event_id;
				if(isset($data[$i])) {
					$newdata['image'] = $data[$i];
					$newdata['event_id'] = $event_id;
				}
				$this->db->insert($this->event_gallery_table, $newdata);
			}
		}
			
	}	 
	
	/**
	 * @function get event gallary
	 * */
	function get_gallary_image($event_id) { 
	//	print_r($data);die;
		$this->db->select('*');
		$this->db->where("event_id", $event_id);
		$result = $this->db->get($this->event_gallery_table);
		return $result->result_array();
	} 
	
	/*
	 * @function to update image w.r.t event gallary id
	 * */
	function update_gallary_image($data,$gallaryId) { 
	//	print_r($data);die;
		
		$this->db->where("event_gallery_id", $gallaryId);
		$this->db->update($this->event_gallery_table, $data);
		return $gallaryId;
		
	}
	 
}
