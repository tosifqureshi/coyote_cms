<?php 
/**
 * Loyalty Model
 *
 * @package    Coyote
 * @category   Module
 * @author     Tosif Qureshi 
 *
 */
Class Loyalty_model extends CI_Model{
   
    /**
     * Initiaze construct
     * 
     */
    function __construct(){
        parent::__construct();    
        // load read database.
        $this->read_db = $this->load->database('read', TRUE); 
        // set tables
		$this->loyalty_table = 'loyalty';
		$this->loyalty_qty_log_table = 'loyalty_qty_log';
		$this->stores_log_table = 'stores_log';
    }   
     
	/**
	 * Function to fetch loyalty list
	 * @input : loyalty_id(optional)
	 * @output: array
	 * @access: public
	 */
	public function get_loyalty_list($loyalty_id = 0) {
		$this -> read_db -> select('*');
		if ($loyalty_id) {
			$this -> read_db -> where("loyalty_id", $loyalty_id);
			$result = $this -> read_db -> get($this->loyalty_table);
			return $result -> row();
		}
		$this -> read_db -> where(array('is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		//$this -> read_db -> order_by('loyalty_id', 'desc');
		$this -> read_db -> order_by('position', 'asc');
		$result = $this -> read_db -> get($this->loyalty_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to fetch loyalty data from order
	 * @input : order(int)
	 * @output: array
	 * @access: public
	 */
	public function get_loyalty_from_order($order = 0) {
		$this -> read_db -> select('loyalty_id');
		if ($order) {
			$this -> read_db -> where("position", $order);
			$result = $this -> read_db -> get($this->loyalty_table);
			return $result -> row();
		} else {
			return false;
		}
	}
	
	/**
	 * Function to manage insert and update data
	 * @input : data(loyalty data)
	 * @output: int(loyalty_id)
	 * @access: public
	 */
	public function add_loyalty($data) {
		
		if(isset($data['loyalty_id'])) {								
			$this->db->where('loyalty_id', $data['loyalty_id']);
			$this->db->update($this->loyalty_table, $data);
			return $data['loyalty_id'];
		} else {
			// get last order data
			$last_order = $this->get_last_order();
			$order = 1;
			if(!empty($last_order)) {
				$order = $last_order->position+1;
			}
			$data['position'] = $order;
			// insert loyalty data
			$this->db->insert($this->loyalty_table, $data);
			return $this->db->insert_id();
		}
	}
	
	 /**
	 * Function to get all store log listing
	 * @input : 
	 * @output: array
	 * @access: public
	 */
	public function get_store_list() {
		
		/*$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> order_by('store_name', 'ASC');
		$result = $this -> read_db -> get($this->stores_log_table);*/
		$sql = "SELECT * FROM (`ava_stores_log`) WHERE `status` = 1 and SubString(store_name, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY `store_name` ASC";
		$result 	= $this->db->query($sql);
		//echo $this->read_db->last_query(); die;
		$return = array();
		if($result->num_rows() > 0){
				//$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['store_id']] = $row['store_name'];			
			}
		}
		return $return;
	}
	
	/**
	 * Function to get loyalty quantity list
	 * @input : loyalty_id
	 * @output: array
	 * @access: public
	 */
	public function get_loyalty_qty($loyalty_id=0) {
		if(!empty($loyalty_id)) {
			$this -> read_db -> select('*');
			$this -> read_db -> where(array('loyalty_id'=>$loyalty_id)); 
			$this -> read_db -> order_by('id', 'asc');
			$result = $this -> read_db -> get($this->loyalty_qty_log_table);
			return $result -> result_array();
		}	else {
			return false;
		}
	}
	
	/**
	 * Function to manage loyalty quantity insertion
	 * @input : data(loyalty qty data)
	 * @output: int(loyalty_id)
	 * @access: public
	 */
	public function add_loyalty_qty($data) {
		$this->db->insert($this->loyalty_qty_log_table, $data);
		return $this->db->insert_id();
		
	}
	
	/**
	 * Function to remove loyalty quantity
	 * @input : loyalty_id
	 * @output: void
	 * @access: public
	 */
	public function remove_loyalty_qty($loyalty_id) {
		// remove previouse loyalty quantities
		$this->db->where('loyalty_id',$loyalty_id);
		$this->db->delete($this->loyalty_qty_log_table);
		
	}
	
	/**
	 * Get barcode data
	 * @access: public
	 * 
	 */
	public function get_barcode_data($barcode=0,$is_simple_offer=0,$id) {
		
		$this -> read_db -> select('loyalty_id');
		$this -> read_db -> where('barcode',$barcode); 
		$this -> read_db -> where_not_in('loyalty_id',$id);
		$result = $this -> read_db -> get($this->loyalty_table);
		//echo $this->read_db->last_query(); die;
		return $result->num_rows();
	}
	
	/**
	 * Get last loyalty position
	 * @access: public
	 * 
	 */
	public function get_last_order() {
		
		$this -> read_db -> select('position');
		$this -> read_db -> where("is_delete", 0);
		$this -> read_db -> order_by('position', 'desc');
		$result = $this -> read_db -> get($this->loyalty_table);
		return $result -> row();
	}
	
	/*Offer : End*/
	
	
 
}
