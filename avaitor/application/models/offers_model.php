<?php 

Class Offers_model extends CI_Model{
    
     /**
	 * Name of the table
	 * @access protected 
	 * @var string
	 */	 
	 
	protected $tblbeacons = 'beacons';    
	protected $tbloffers = 'offers';    
  
	
    
    function __construct(){
        parent::__construct();    
        $this->read_db = $this->load->database('read', TRUE); //Loading read database.
        $this->mssql_db = $this->load->database('mssql', TRUE); //Loading read database.
        // set tables
        $this->store_table = 'stores';
		$this->stores_log_table = 'stores_log';
		$this->categories_table = 'categories';
		$this->account_ms_table = 'ACCOUNT';
    }    
	/*Offer : Start*/
	function getOfferList($id = '') {    // get the list of offer to show in offer view list
		$this -> read_db -> select('offers.*');
		if ($id) {
			$this -> read_db -> where("offers.offer_id", $id);
			$result = $this -> read_db -> get('offers');
			return $result -> row();
		}
		//$this -> read_db -> select('stores_log.store_id');
		$this -> read_db -> select('categories.category_name');
		$this -> read_db -> where(array('offers.is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		//$this -> read_db -> join('stores_log','stores_log.store_id = offers.store_id','left');
		$this -> read_db -> join('categories','categories.category_id = offers.category_id', 'left');
		$this -> read_db -> order_by('offers.offer_id', 'desc');
		$result = $this -> read_db -> get('offers');
		return $result -> result_array();
	}
	
	function add_offer($data) {    //function is used in both case edit and update
		if($data['offer_id']) {											// edit data w.r.t. to offer_id		
			$this->db->where('offer_id', $data['offer_id']);
			$this->db->update('offers', $data);
			return $data['offer_id'];
		} else {														// add data
			$this->db->insert('offers', $data);
			return $this->db->insert_id();
		}
	}
	
	function getBeaconList() {		// beacon list to show is drop down 			
		$select = array(
			'beacons.beacon_id',
			'beacon_code',
		);
		$this -> db -> select($select);
		$this -> db -> where(array('status'=>'1','is_delete'=>'0'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> db -> get('beacons');
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select beacon';
			foreach($result -> result_array() as $row) {
				$return[$row['beacon_id']] = $row['beacon_code'];
			}
		}
		return $return;
	}
	
	function getStoreList() {		// store list to show is drop down
		$select = array(
			'STORTBL.STOR_STORE',
			'STOR_DESC',
		);
		$this -> mssql_db -> select($select);
		$this -> mssql_db -> where(array('STOR_STATUS'=>'Active'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> mssql_db -> get('STORTBL');
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['STOR_STORE']] = $row['STOR_DESC'];			
			}
		}
		return $return;
	}
	
	function check_beacon($data) {        // checking that beacon is available or not 
		$start_date = $data['start_date'];
		$end_date 	= $data['end_date'] ;
		$start_time = $data['start_time'] ;
		$end_time	= $data['end_time'] ;
		$beacon_id	= $data['beacon_id'] ;
		$store_id	= $data['store_id'] ;
		$offer_id	= $data['offer_id'] ;
		$sql 		= "select * from ava_offers where is_delete = '0' AND status = '1' AND offer_id != '".$offer_id."' AND beacon_id = '".$beacon_id."' AND store_id = '".$store_id."' AND ((start_time <= '".$start_time."' AND end_time > '".$start_time."') OR (start_time < '".$end_time."' AND end_time > '".$end_time."')) AND ((start_date <= '".$start_date."' AND end_date >= '".$start_date."') OR (start_date < '".$end_date."' AND end_date > '".$end_date."') )";
		$query 		= $this->db->query($sql);
		return $query->num_rows();
	}
	
	/**
	 * Get all store log listing
	 * @access: public
	 * 
	 */
	public function get_store_list() {
		
		/*$this -> read_db -> select('*');
		$this -> read_db -> order_by('store_name', 'ASC');
		$result = $this -> read_db -> get($this->stores_log_table);*/
		
		$sql = "SELECT * FROM (`ava_stores_log`) WHERE `status` = 1 and `store_name` <> '' and SubString(store_name, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY `store_name` ASC";
		$result 	= $this->db->query($sql);
		$return = array();
		if($result->num_rows() > 0){
				//$return[''] = 'Select Store';
			foreach($result -> result_array() as $row){
				$return[$row['store_id']] = $row['store_name'];			
			}
		}
		return $return;
	}
	
	/**
	 * Get all store data
	 * 
	 */
	function get_store_data() {
		
		$this -> mssql_db -> select('*');
		//$this -> mssql_db -> where(array('STOR_STATUS'=>'Active'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> mssql_db -> get('STORTBL');
		return $result -> result_array();
	}
	
	/**
	 * Manage storing procedure for store data
	 * 
	 */
	function manage_store_data($data) {   
		$this->db->insert($this->store_table, $data);
		return $this->db->insert_id();
	}
	
	/**
	 * Get store name from id
	 * @input: store_id
	 * @access: public
	 * 
	 */
	public function get_store_name($store_id=0) {
		
		$this -> read_db -> select('store_name');
		$this -> read_db -> where("store_id", $store_id);
		$result = $this -> read_db -> get($this->stores_log_table);
		$result_row = $result -> row();
		$return = '';
		if(!empty($result_row)) {
			$return = $result_row->store_name;
		}
		return $return;
	}
	
	/**
	 * Get store name from id
	 * @input: store_id
	 * @access: public
	 * 
	 */
	public function get_user_name($user_id=0) {
		
		$this -> mssql_db -> select('ACC_FIRST_NAME as member_name');
		$this -> mssql_db -> where("ACC_NUMBER", $user_id);
		$result = $this -> mssql_db -> get($this->account_ms_table);
		$result_row = $result -> row();
		$return = '';
		if(!empty($result_row)) {
			$return = $result_row->member_name;
		}
		return $return;
	}
    
    
    /**
	 * Get store name and surname from id
	 * @input: store_id
	 * @access: public
	 * 
	 */
	public function get_user_fullname($user_id=0) {
		
		$this -> mssql_db -> select('ACC_FIRST_NAME as member_name,ACC_SURNAME as member_surname');
		$this -> mssql_db -> where("ACC_NUMBER", $user_id);
		$result = $this -> mssql_db -> get($this->account_ms_table);
		$result_row = $result -> row();
		$return = '';
		if(!empty($result_row)) {
			$return = $result_row->member_name.' '.$result_row->member_surname;
		}
		return $return;
	}
	
	
	
	/*Offer : End*/
	
	function get_category() {
		$this -> mssql_db -> select('*');
		$this -> mssql_db -> where(array('CODE_KEY_TYPE'=>'CATEGORY'));	// status=>1 : active and is_delete=>0 : data is not delete
		$result = $this -> mssql_db -> get('codetbl');
		return $result -> result_array();
	}
				
	/**
	 * Get all category listing
	 * @access: public
	 * 
	 */
	public function get_category_list() {
		
		$this -> read_db -> select('*');
		$this -> read_db -> where('status',1);
		$this -> read_db -> where('is_deleted',0);
		$result = $this -> read_db -> get($this->categories_table);
		$return = array();
		if($result->num_rows() > 0){
				$return[''] = 'Select Category';
			foreach($result -> result_array() as $row){
				$return[$row['category_id']] = $row['category_name'];			
			}
		}
		return $return;
	}	
	
	/**
	 * check offer name exits or not for copy offer
	 * @access: public
	 * 
	 */
	public function check_offer_name($offer_name=''){
		$this -> read_db -> select('*');
		$this -> read_db -> where(array('is_delete'=>'0'));   // is_delete=>0 : data is not delete
		$this -> read_db -> like('offer_name', $offer_name); 
		$result = $this -> read_db -> get('offers');
		return $result -> num_rows();
	}
    
    
    /**
	 * Get store name from id
	 * @input: store_id
	 * @access: public
	 * 
	 */
	public function get_ava_user_name($user_id=0) {
		
		$this -> read_db -> select('firstname as member_name');
		$this -> read_db -> where("acc_number", $user_id);
		$result = $this -> read_db -> get('ava_users');
		$result_row = $result -> row();
		$return = '';
		if(!empty($result_row)) {
			$return = $result_row->member_name;
		}
		return $return;
	}
    
 
}
