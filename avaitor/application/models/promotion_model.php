<?php 
/**
 * Promotion Model
 *
 * @package    Coyote
 * @category   Module
 * @author     Tosif Qureshi 
 *
 */
Class Promotion_model extends CI_Model{
   
    /**
     * Initiaze construct
     * 
     */
    function __construct(){
        parent::__construct();    
        // load read database.
        $this->read_db = $this->load->database('read', TRUE); 
        // set tables
        $this->web_promotions_table = 'web_promotions';
		$this->promotions_table = 'promotions';
		$this->kiosk_promotions_table = 'kiosk_promotions';
    }   
     
	/**
	 * Function to fetch promotions list
	 * @input : promotion_id(optional)
	 * @output: array
	 * @access: public
	 */
	public function get_promotion_list($promotion_id = 0) {
		$this -> read_db -> select('*');
		if ($promotion_id) {
			$this -> read_db -> where("promo_id", $promotion_id);
			$result = $this -> read_db -> get($this->promotions_table);
			return $result -> row();
		}
		$this -> read_db -> where(array('is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		$this -> read_db -> order_by('promo_id', 'desc');
		$result = $this -> read_db -> get($this->promotions_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to manage insert and update data
	 * @input : data(promotion data)
	 * @output: int(promo_id)
	 * @access: public
	 */
	public function add_promotion($data) {
		if(isset($data['promo_id'])) {								
			$this->db->where('promo_id', $data['promo_id']);
			$this->db->update($this->promotions_table, $data);
			return $data['promo_id'];
		} else {
			$this->db->insert($this->promotions_table, $data);
			return $this->db->insert_id();
		}
	}
	
	/**
	 * Function to fetch kiosk promotions list
	 * @input : promotion_id(optional)
	 * @output: array
	 * @access: public
	 */
	public function get_kiosk_promotion_list($promotion_id = 0) {
		$this -> read_db -> select('*');
		if ($promotion_id) {
			$this -> read_db -> where("promo_id", $promotion_id);
			$result = $this -> read_db -> get($this->kiosk_promotions_table);
			return $result -> row();
		}
		$this -> read_db -> where(array('is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		$this -> read_db -> order_by('promo_id', 'desc');
		$result = $this -> read_db -> get($this->kiosk_promotions_table);
		return $result -> result_array();
	}
	
	/**
	 * Function to manage insert and update data
	 * @input : data(promotion data)
	 * @output: int(promo_id)
	 * @access: public
	 */
	public function add_kiosk_promotion($data) {
		if(isset($data['promo_id'])) {								
			$this->db->where('promo_id', $data['promo_id']);
			$this->db->update($this->kiosk_promotions_table, $data);
			return $data['promo_id'];
		} else {
			$this->db->insert($this->kiosk_promotions_table, $data);
			return $this->db->insert_id();
		}
	}


	/**
	 * Function to fetch web promotions list
	 * @input : promotion_id(optional)
	 * @output: array
	 * @access: public
	 */
	public function get_web_promotion_list($promotion_id = 0) {
		$this -> read_db -> select('*');
		if ($promotion_id) {
			$this -> read_db -> where("promo_id", $promotion_id);
			$result = $this -> read_db -> get($this->web_promotions_table);
			return $result -> row();
		}
		$this -> read_db -> where(array('is_delete'=>'0'));   // status=>1 : active and is_delete=>0 : data is not delete
		$this -> read_db -> order_by('promo_id', 'desc');
		$result = $this -> read_db -> get($this->web_promotions_table);
		return $result -> result_array();
	}

		/**
	 * Function to manage insert and update data
	 * @input : data(web promotion data)
	 * @output: int(promo_id)
	 * @access: public
	 */
	public function add_web_promotion($data) {
		if(isset($data['promo_id'])) {								
			$this->db->where('promo_id', $data['promo_id']);
			$this->db->update($this->web_promotions_table, $data);
			return $data['promo_id'];
		} else {
			$this->db->insert($this->web_promotions_table, $data);
			return $this->db->insert_id();
		}
	}
	

 
}
