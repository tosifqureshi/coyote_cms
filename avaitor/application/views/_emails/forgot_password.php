
<div style="float:right;width:950px;margin-top:7px">
<img src ="<?php echo base_url();?>avaitor/themes/airdoc/images/logo_mini.png" >
<p></p>	<p></p>		
<p>So, you forgot your <?php e($this->settings_lib->item('site.title')) ?> password, eh? No worries!</p>
<p> Below you will find a link that you can use to reset your password for
<?php e($this->settings_lib->item('site.title')) ?>. Then, you can change your password to something
different (because we are sure your temp password isn't going to be easy to
remember...) </p>
<p>This link is only valid for 24 hours.</p>
<p></p>

<p><a href="<?php echo $link ?>"><?php echo $link ?></a></p>

<p>Regards,<br/>
Airdoc Team</p>

</div>
</div>

<br><br>



