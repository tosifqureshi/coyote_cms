<div class="row">              
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $beacon_title;?></h4>
				<div class="p-t-20">
					<?php echo form_open('backend/beacon/create/'.$beacon_id,'id="add_beacon_form" class="form-material form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Beacon code input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('beacon_code');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'beacon_code','id'=>'beacon1', 'value'=>set_value('beacon_code',$beacon_code), 'class'=>'form-control form-control-line required','placeholder'=>lang('tooltip_beacon_code'));
									echo form_input($data);?>
								</div>
								<!-- Beacon code input end here -->
								<!-- Store dropdown box start here -->
								<div class="form-group ">
									<label><?php echo lang('beacon_store');?><i style="color:red;">*</i></label>
									 <div class="">
										<?php
										// set store id value
										$store_id_val = (!empty($store_id)) ? $store_id : '';
										$data	= array('name'=>'store_id','id'=>'store_id','class'=>'form-control required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_beacon_store'));
										echo form_dropdown($data,$stores,set_value('store_id',$store_id_val));
										?>
									</div>
								</div>
								<!-- Store dropdown box end here -->
								<!-- Description input start here -->
								<div class="form-group">
									<label class=""><?php echo lang('description');?></label>
									<?php
									$data = array('name'=>'description', 'value'=>$description,'class'=>'form-control','placeholder'=>lang('tooltip_description'),'rows'=>'4');
									echo form_textarea($data);?>
									<input type="hidden" value="<?php echo set_value('beacon_id', ($beacon_id)); ?>" name='beacon_id' id='beacon_id'>
								</div>
								<!-- Description input end here -->
							</div>
						</div>		
						<div class="row pb-3 float-right">
							<div class="col-lg-6 col-md-4">
								<button type="submit" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="col-lg-6 col-md-4">
								<a href='<?php echo base_url().'backend/beacon' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					<?php echo form_close();?>
				</div>	
			</div>
		</div>
	</div>
</div>			

<script type="text/javascript">
	$(document).ready(function() {
		$("#add_beacon_form").validate({
			// post form to action url
		});
	});
	$("#beacon1").on("keyup", function(){
		var val = $("#beacon1").val();
		if(val.length > 8) {
			bootbox.alert('Beacons code can\'t have more then 8 characters.');
			$("#beacon1").val("");
		}
	});
	
	$('#store_id').on('change', function (e) {
		var store_id = this.value;
		var beacon_id =  $( "#beacon_id" ).val();
		var response 	=	0;
		if(store_id != "") {
			$.ajax ({     
				type: "POST",
				data : {store_id:store_id,beacon_id:beacon_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
				url: "<?php echo base_url() ?>backend/beacon/check_active_store",
				async: false,
				success: function(data) { 
					if(data > 0) {
						bootbox.alert('Store already assign to given beacons code.');
						 $('#store_id').prop('selectedIndex',0);
					}
				}, 
				error: function(error){
					bootbox.alert(error);
				}
			});
		}
	});
	
</script>
