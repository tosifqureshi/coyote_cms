<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo lang('beacon_setting_heading');?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					echo form_open('backend/beacon/store_settings/', 'class="form-material form-with-label" id="beacon_setting_form"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- input field start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('beacon_notification_time');?></label>
									<div class="col-md-6">
										<input type="text" name="beacon_notification_time" id="beacon_notification_time" class="form-control form-control-line required digits" value="<?php echo set_value('beacon_notification_time', (!empty($beacon_notification_time))  ? $beacon_notification_time : '') ?>" maxlength="2" min="1"/>
										<input type="hidden" name="form_action" value="<?php echo set_value('form_action', (!empty($beacon_notification_time)) ? 'update' : 'insert') ?>" />
									</div>
									<p class="help-inline"><?php echo lang('beacon_notification_help_note') ?></p>
								</div>
								<!-- input field end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('bf_action_save') .' '. lang('bf_context_settings') ?></button>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#beacon_setting_form").validate({
			// post form to action url
		});
	});
</script>
