<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo lang('cart_setting_heading');?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					echo form_open('backend/beacon/cart_store_settings/', 'class="form-material form-with-label" id="cart_setting_form"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- input field start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('cart_setting');?></label>
									<div class="col-md-6">
										<input type="text" name="cart_product_limit" id="cart_product_limit" class="form-control form-control-line required digits" value="<?php echo set_value('cart_product_limit', (!empty($cart_product_limit))  ? $cart_product_limit : '') ?>" maxlength="10" min="1"/>
										<input type="hidden" name="form_action" value="<?php echo set_value('form_action', (!empty($cart_product_limit)) ? 'update' : 'insert') ?>" />
									</div>
									<p class="help-inline"><?php echo lang('cart_notification_help_note') ?></p>
								</div>
								<!-- input field end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('bf_action_save') .' '. lang('bf_context_settings') ?></button>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#cart_setting_form").validate({
			// post form to action url
		});
	});
</script>
