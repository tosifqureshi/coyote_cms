<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('beacons') ?></h4>
				<a href="<?php echo site_url("backend/beacon/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_beacon'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<div class="table-responsive m-t-40">
					<table id="pages_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('sno'); ?></th>
								<th><?php echo lang('beacon_code'); ?></th>
								<th><?php echo lang('created_at'); ?></th>
								<th><?php echo lang('store'); ?></th>
								<th><?php echo lang('status'); ?></th>
								<th><?php echo lang('delete'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							if(isset($beacons) && is_array($beacons) && count($beacons)>0):
							foreach($beacons as $beacon) :
								// set encoded beacon id
								$beacon_id = encode($beacon['beacon_id']); ?>
								
								<tr>
								<td><?php echo $i;?></td>
								<td>
								<a href="<?php echo site_url("backend/beacon/create"); echo '/' . ($beacon_id);?>"><?php echo $beacon['beacon_code'];?></a>
								</td>
								<td><?php echo date('F j, Y',strtotime($beacon['created_at']));?></td>
								<td><?php echo (isset($beacon['store_name']) && !empty($beacon['store_name'])) ? ucfirst($beacon['store_name']) : '' ; ?></td>
								<td><?php 
									if($beacon['status'] == '0') { ?>
										<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo $beacon_id; ?>','<?php echo $beacon['status']; ?>','<?php echo encode($beacon['status']); ?>','<?php echo ($beacon['store_id']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
										<?php
									} else { ?>
										<a class="common-btn active-btn" onclick="changeStatus('<?php echo $beacon_id; ?>','<?php echo $beacon['status']; ?>','<?php echo encode($beacon['status']) ?>','<?php echo ($beacon['store_id']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
									}
								?></td>
								<td>
									<a class="delete-btn" href="javascript:;" onclick="myFunction('<?php echo $beacon_id; ?>','<?php echo $beacon['store_id']; ?>')"><i class="ti-trash"></i></a>
								</td>
							</tr>
							<?php 
							$i++;
							endforeach ; endif; ?>
						</tbody>	
					</table>	
				</div>
			</div>
		</div>
	</div>
</div>		

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});

	function myFunction(id,store_id) {
		var res = check_active_beacon_offer(id,store_id);
		if(res == 0) {
			bootbox.confirm("Are you sure want to delete this beacon?", function(result) {
				if(result==true) {
					$('#loader1').fadeIn();	
					location.href="<?php echo site_url("backend/beacon/delete_beacon/"); ?>/"+id;
				}
			});
		} else {
			bootbox.alert(res);
		}
	}

	function changeStatus(id,status,encodestatus,store_id) {
		var res = 0;
		if(status == 0) {
			$.ajax ({     
				type: "POST",
				data : {store_id:store_id,beacon_id:id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
				url: "<?php echo base_url() ?>backend/beacon/check_active_store",
				async: false,
				success: function(data) { 
					if(data > 0) {
						bootbox.alert('Store already have an active beacon.');
						res = 1;
					}
				}, 
				error: function(error){
					bootbox.alert(error);
				}
			});
		} else {
			var res = check_active_beacon_offer(id,store_id);
		}
		if(res == 0) {
			bootbox.confirm("Are you sure want to change the beacon status?", function(result) {
				if(result==true) {
					$('#loader1').fadeIn();
					location.href="<?php echo site_url("backend/beacon/beacon_status/"); ?>/"+id+"/"+encodestatus;
				}
			});
		} else {
			bootbox.alert(res);
		}
	}

	function check_active_beacon_offer(beacon_id,store_id) {
		var res = 0;
		if(beacon_id != 0) {
			$.ajax ({     
				type: "POST",
				data : {store_id:store_id,beacon_id:beacon_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
				url: "<?php echo base_url() ?>backend/beacon/check_active_beacon_offer",
				async: false,
				success: function(data) { 
					if(data > 0) {
						res = "Beacon associated with active beacon offer so can not delete/deactive beacon.";
					}
				}, 
				error: function(error){
					res = error;
				}
			});
		}
		return res;
	}
</script>

