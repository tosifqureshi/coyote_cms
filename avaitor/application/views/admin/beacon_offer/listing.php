<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('offers') ?></h4>
				<a href="<?php echo site_url("backend/vip_offer/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_beacon_offer'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="pages_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('offer_sno'); ?></th>
								<th><?php echo lang('offer_list_image'); ?></th>
								<th><?php echo lang('offer_name'); ?></th>
								<th><?php echo lang('offer_store'); ?></th>
								<th><?php echo lang('offer_category'); ?></th>
								<th><?php echo lang('offer_day_diffrence'); ?></th>
								<th><?php echo lang('offer_status'); ?></th>
								<th><?php echo lang('offer_delete'); ?></th>
							</tr>	
						</thead>
                    
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($offers) && is_array($offers) && count($offers)>0):
							foreach($offers as $offer) : 
								// set offer id
								$offer_id = encode($offer['beacon_offer_id']);?>
								
								<tr>
								<td><?php echo $i;?></td>
								<td>
									<?php if(isset($offer['is_banner_image']) && !empty($offer['is_banner_image'])) {
										$offer_image = $offer['offer_image_'.$offer['is_banner_image']] ;
										$offer_image = (!empty($offer_image)) ? $offer_image : '';
										$offer_image = base_url('uploads/offer_images/'.$offer_image);
										?>
										<img  id="imageresource" src="<?php echo $offer_image;?>" width="110" height="90" offer_name="<?php echo $offer['offer_name'];?>">
									<?php } ?>
								</td>
								<td>
								<a href="<?php echo site_url("backend/vip_offer/create"); echo '/' . ($offer_id);?>"><?php if(strlen($offer['offer_name']) > 15) { echo substr($offer['offer_name'],0,15).'...'; } else { echo $offer['offer_name']; }?></a>
								</td>
								<td class="mytooltip tooltip-effect-1 desc-tooltip">
									<span class="tooltip-content clearfix">
										<span class="tooltip-text">
											<?php 
											$start_date_time = date('Y-m-d H:i:s',strtotime($offer['start_date'].''.$offer['start_time']));
											$end_date_time = date('Y-m-d H:i:s',strtotime($offer['end_date'].''.$offer['end_time']));
											$diff =  get_date_diffrence($start_date_time, $end_date_time);
										
											echo '<b>'.lang('offer_name').' : </b>'.$offer['offer_name'] ;
											echo '<br>';
											echo '<b>'.lang('start_at').' : </b>'.date('F j, Y, g:i a',strtotime($offer['start_date'].''.$offer['start_time'])) ;
											echo '<br>';
											echo '<b>'.lang('end_at').'  : </b>'.date('F j, Y, g:i a',strtotime($offer['end_date'].''.$offer['end_time'])) ;
											echo '<br>';
											echo $diff;?>
										</span> 
									</span>
									<span class="text-ttip">	
										<?php 
										if(!empty($offer['store_id'])) {
											$store_ids = explode(',',$offer['store_id']);
											// get store name from store id
											if(is_array($store_ids) && count($store_ids) > 0) {
												foreach($store_ids as $store_id) {
													// get store name
													$store_name = get_store_name($store_id);
													if(!empty($store_name)) {
														echo ucfirst($store_name);
														echo '<br>';
													}
												}
											}
										} ?>
									</span>	
								</td>
								<td><?php echo (isset($offer['category_name'])) ? ucfirst($offer['category_name']) : ''; ?></td>
								<td>
									<?php 
									// get first segment of dates 
									$avail_diff =  get_date_diffrence($start_date_time, $end_date_time,6,1);
									if($offer['status'] == '0' || $avail_diff == 'Closed') { ?>
										<span class="label label-success"><?php echo lang('closed');?></span>
									<?php
									} else { ?>
										<span class="label label-info"><?php echo $avail_diff ;?></span>
										<?php
									}
									?>
								</td>
								
								<td><?php 
									if($offer['status'] == '0') {
										?>
										<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo encode($offer['beacon_offer_id']); ?>','<?php echo encode($offer['status']); ?>','<?php echo encode($offer['store_id']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
										<?php
									} else {
										?>
										<a class="common-btn active-btn" onclick="changeStatus('<?php echo encode($offer['beacon_offer_id']); ?>','<?php echo encode($offer['status']); ?>','<?php echo encode($offer['store_id']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
									}
								?></td>
								<td>
									<a class="common-btn delete-btn" href="javascript:;" onclick="myFunction('<?php echo encode($offer['beacon_offer_id']); ?>')"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							<?php 
							$i++;
							endforeach ; endif; ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</div>
</div>				

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});
	
	function myFunction(id) {
		bootbox.confirm("Are you sure want to delete the offer?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/vip_offer/offer_delete/"); ?>/"+id;
			}
		});
	}

	function changeStatus(id,status,store_id) {
		bootbox.confirm("Are you sure want to change the offer status?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();
				location.href="<?php echo site_url("backend/vip_offer/offer_status/"); ?>/"+id+"/"+status+"/"+store_id;
			}
		});
	}
	
	/**
	 *  display offer image in popup
	 */
	$(document).delegate( "#imageresource", "click", function(e) {
		$('#myModalLabel').html($(this).attr('offer_name'));
		$('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
		$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
	});
</script>

