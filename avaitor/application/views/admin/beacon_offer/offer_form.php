<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">          
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $offer_title;?></h4>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs customtab2" role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#content_tab" role="tab"> <span class=""><?php echo lang('content');?></span></a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attribute_tab" role="tab"> <span class="	"><?php echo lang('attributes');?></span></a> </li>
				</ul>
				<!-- Tab panes -->
				<?php echo form_open_multipart('backend/vip_offer/create/'.$beacon_offer_id,'id="add_offer_form", class="  form-with-label"'); ?>
					<div class="tab-content">
						<div class="tab-pane active" id="content_tab" role="tabpanel">
							<div class="p-t-20">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group form-material">
											<label class=""><?php echo lang('offer_name');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'offer_name', 'value'=>set_value('offer_name',$offer_name), 'class'=>'form-control form-control-line required', 'title'=>lang('tooltip_offer_name'));
											echo form_input($data);
											?>
										</div>
										<div class="form-group form-material">
											<label class=""><?php echo lang('offer_price');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'offer_price' ,'id'=>'offer_price', 'value'=>set_value('offer_price',$offer_price), 'class'=>'form-control form-control-line numeric required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_offer_price'));
											echo form_input($data);?>
										</div>
										
										<div class="row form-group form-material">
											<div class="col-md-6">
												<label class=""><?php echo lang('offer_barcode');?><i style="color:red;">*</i></label>
												<?php
												$data = array('name'=>'barcode' ,'id'=>'barcode', 'value'=>set_value('barcode',$barcode), 'class'=>'form-control form-control-line numeric required','maxlength'=>12, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_barcode'));
												echo form_input($data);?>
											</div>
											<div class="col-md-6">
												<!--- QR code upload field starts here --> 
												<?php if(isset($barcode_image) && !empty($barcode_image)) { ?>
													<div class="img-box">
														<label><?php echo lang('qrcode_image');?></label> 
														<div class="bar-img" >
															<img id="qr-image-picker" style="width: 100%;" src="<?php echo base_url('uploads/barcodes/'.$barcode_image) ?>" data-toggle='tooltip', data-placement='right', title='<?php echo lang('tooltip_barcode_image'); ?>' />
														</div>
													</div>	
												<?php } ?>
												<!--- QR code upload field end here --> 
											</div>
										</div>
										
										<div class="form-group form-material">
											<label class=""><?php echo lang('offer_short_description');?></label>
											<?php
											$data	= array('name'=>'offer_short_description', 'value'=>$offer_short_description,'class'=> 'form-control form-control-line','rows'=>3);
											echo form_textarea($data);
											?>
										</div>
											
										<div class="form-group form-material">
											<label><?php echo lang('offer_long_description');?></label>
											<?php
											$data = array('name'=>'offer_long_description', 'class'=>'BeaconmceEditor redactor span8', 'value'=>$offer_long_description);
											echo form_textarea($data);?>
										</div>
									</div>	
									<div class="col-md-6">
										<div class="form-group ">
											<label><?php echo lang('offer_image');?></label>
											<div class="row">
												<div class="col-md-4">
													<div class="img-box">
														<?php
														$offer_image_val_1 = (isset($offer_image_1) && !empty($offer_image_1)) ? $offer_image_1 :'';
														$img_1_display = (isset($offer_image_1) && !empty($offer_image_1)) ? '' :'dn';
														if(isset($offer_image_1) && $offer_image_1 != '') {
														$offer_image_1 = base_url('uploads/offer_images/'.$offer_image_val_1);
														$title = lang('tooltip_offer_image');
															$offer_image_1_doc = $this->config->item('document_path').'uploads/offer_images/'.$offer_image_val_1; 
															if(!file_exists($offer_image_1_doc)){
																$offer_image_1 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															}
														} else {
															$title = lang('tooltip_offer_image');
															$offer_image_1 = base_url('uploads/upload.png');
														} ?>
														<a id="remove_btn_1" class="<?php echo $img_1_display;?>" href="javascript:void(0);" onclick="remove_image(1)">×</a>
														<div class="cell-img" >
															<img id="image-picker_1" src="<?php echo $offer_image_1; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
														<?php
														$data	= array('name'=>'offer_image_1', 'id'=>'offer_image_1', 'value'=>set_value('offer_image_1',$offer_image_val_1),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
														echo form_upload($data);
														$data	= array('name'=>'is_banner_image', 'value'=>'1','id'=>'is_banner_image_1','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right'); // fist image is alway the default image
														echo form_radio($data);
														?>
														<label for="is_banner_image_1"><?php echo lang('offer_default_image');?></label>
													</div>
												</div>		
												<div class="col-md-4">
													<div class="img-box">
														<?php
														$offer_image_val_2 = (isset($offer_image_2) && !empty($offer_image_2)) ? $offer_image_2 :'';
														$img_2_display = (isset($offer_image_2) && !empty($offer_image_2)) ? '' :'dn';
														if(isset($offer_image_2) && $offer_image_2 != '') {
														$offer_image_2 = base_url('uploads/offer_images/'.$offer_image_val_2);
														$title = lang('tooltip_offer_image');
															$offer_image_2_doc = $this->config->item('document_path').'uploads/offer_images/'.$offer_image_val_2; 
															if(!file_exists($offer_image_2_doc)){
																$offer_image_2 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															}
														} else {
															$title = lang('tooltip_offer_image');
															$offer_image_2 = base_url('uploads/upload.png');
														} ?>
														<a id="remove_btn_2" class="<?php echo $img_2_display;?>" href="javascript:void(0);" onclick="remove_image(2)">×</a>
														<div class="cell-img" ><img id="image-picker_2" src="<?php echo $offer_image_2; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>					
														
														<?php
														$data	= array('name'=>'offer_image_2', 'id'=>'offer_image_2', 'value'=>set_value('offer_image_2',$offer_image_val_2),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
														echo form_upload($data);
												
														if(isset($is_banner_image) && $is_banner_image == 2) { 
															$data	= array('name'=>'is_banner_image', 'value'=>'2','id'=>'is_banner_image_2','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right');
														} else {
															$data	= array('name'=>'is_banner_image', 'value'=>'2','id'=>'is_banner_image_2','data-toggle'=>'tooltip', 'data-placement'=>'right');
														}
														echo form_radio($data);?>
														<label for="is_banner_image_2"><?php echo lang('offer_default_image');?></label>
													</div>	
												</div>	
												<div class="col-md-4">
													<div class="img-box">
														<?php
														$offer_image_val_3 = (isset($offer_image_3) && !empty($offer_image_3)) ? $offer_image_3 :'';
														$img_3_display = (isset($offer_image_3) && !empty($offer_image_3)) ? '' :'dn';
														if(isset($offer_image_3) && $offer_image_3 != '') {
														$offer_image_3 = base_url('uploads/offer_images/'.$offer_image_val_3);
														$title = lang('tooltip_offer_image');
															$offer_image_3_doc = $this->config->item('document_path').'uploads/offer_images/'.$offer_image_val_3; 
															if(!file_exists($offer_image_3_doc)){
																$offer_image_3 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															}
														} else {
															$title = lang('tooltip_offer_image');
															$offer_image_3 = base_url('uploads/upload.png');
														}?>
														<a id="remove_btn_3" class="<?php echo $img_3_display;?>" href="javascript:void(0);" onclick="remove_image(3)">×</a>
														<div class="cell-img" ><img id="image-picker_3" src="<?php echo $offer_image_3; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
														<?php
														$data	= array('name'=>'offer_image_3', 'id'=>'offer_image_3', 'value'=>set_value('offer_image_3',$offer_image_val_3),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
														echo form_upload($data);
													
														if(isset($is_banner_image) && $is_banner_image == 3) {
															$data	= array('name'=>'is_banner_image', 'value'=>'3','id'=>'is_banner_image_3','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right');
														} else {
															$data	= array('name'=>'is_banner_image', 'value'=>'3','id'=>'is_banner_image_3','data-toggle'=>'tooltip', 'data-placement'=>'right');
														}
														echo form_radio($data);?>
														<label for="is_banner_image_3"><?php echo lang('offer_default_image');?></label>
													</div>	
												</div>
											</div>


											<div class="row">
											<div class="col-md-12">
												<div class="form-group form-material">
												<!-- Is slider banner image checkbox options start here -->
												<input type="checkbox" name="is_image_show_in_slider" value="1" id="is_image_show_in_slider" <?php echo (isset($is_image_show_in_slider) && !empty($is_image_show_in_slider)) ? 'checked="checked"' : '';?> >
												<label for="is_image_show_in_slider"><?php echo lang('is_slider_image');?></label>

												<div class="alert alert-success alert-dismissible licence_help_txt">
												<i class="icon fa fa-info"></i> 
												Select this to show image in dashboard slider.
												</div>
												</div>
												<!-- Is slider banner image checkbox options end here -->
											
												<!-- Images right side block start here -->
												<div class="img-wrapper">
												<label for="title" class="img-title"><?php echo lang('offer_detail_page_image');?></label> 	
												<div class="img-box">
												<?php
												// set banner image action
												$offer_detail_page_image_val = (isset($offer_detail_page_image) && !empty($offer_detail_page_image)) ? $offer_detail_page_image :'';
												$img_1_display = (isset($offer_detail_page_image_val) && !empty($offer_detail_page_image_val)) ? '' :'dn';
												if(!empty($offer_detail_page_image_val)) {
												$banner_image_src = base_url('uploads/offer_images/'.$offer_detail_page_image);
												$title = lang('tooltip_offer_image');

												$banner_image_val_doc = $this->config->item('document_path').'uploads/offer_images/'.$offer_detail_page_image; 
												if(!file_exists($banner_image_val_doc)){
												// $promo_image_1 = base_url('uploads/No_Image_Available.png');
												$banner_image_src = base_url('uploads/No_Image_Available.png');
												$title = "No Image Available";
												}
												} else {
												$title = lang('tooltip_image');
												$banner_image_src = base_url('uploads/upload.png');
												} ?>
												<div class="cell-img" id="banner_image_cell">
												<img id="offer_detail_page_image_picker" src="<?php echo $banner_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
												</div>
												<?php
												$data	= array('name'=>'offer_detail_page_image', 'id'=>'offer_detail_page_image', 'value'=>$offer_detail_page_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_offer_detail_page_image(this)');
												echo form_upload($data);
												// set banner image value in hidden type
												$data = array('name'=>'offer_detail_page_image_val', 'value'=>$offer_detail_page_image_val,'id'=>'offer_detail_page_image_val','type'=>'hidden');
												echo form_input($data);
												?>
												</div>
												<div style="clear:both;"></div>
												<div class="alert red-alert alert-danger alert-dismissible ">
												<i class="icon fa fa-warning"></i>
												Upload up to 3MB images. 
												</div>
												</div>
												<!-- Images right side block end here -->
											</div>	

										</div>	



										</div>	
									</div>	
								</div>	
							</div>	
						</div>	
						<div class="tab-pane p-t-20" id="attribute_tab" role="tabpanel">
						
									<div class="form-group row">
										<label class="control-label col-md-12">
											<?php echo lang('offer_store');?> <span class="red">*</span>
										</label>
										<div class="demo-checkbox col-md-12">
											<input type="checkbox" id="select_all" class="filled-in chk-col-light-blue" />
											<label for="select_all"><?php echo lang('select_all');?></label>
										</div>
										<div class="col-md-6">
											<?php
											$store_id_array = (!empty($store_id)) ? explode(',',$store_id) : '';
											$data	= array('name'=>'store_id[]','multiple'=>'multiple','id'=>'beacon_store_id', 'class'=>'required form-control', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_store'));
											echo form_dropdown($data,$stores,set_value('store_id',$store_id_array));?>
										</div>	 
									</div>        
									<div class="form-group row form-material">
										<label class="control-label col-md-12">
											<h4> <?php echo lang('category');?><span class="red">*</span></h4>
										</label>
										<div class="col-md-6">
											<?php
											$data	= array('name'=>'category_id','id'=>'category_id','data-toggle'=>'tooltip', 'class'=>'required form-control', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_category'));
											echo form_dropdown($data,$categories,set_value('category_id',$category_id));?>
										</div>
									</div>
									<!-- Offer start / end date fields start here -->
									<div class="row form-group form-material">
										 <div class="col-md-3">
											<label class="m-t-20"><?php echo lang('start_date');?><i style="color:red;">*</i></label>
											<input type="text" name="start_date" class="form-control required" value="<?php echo !empty($start_date) ? $start_date : '';?>" id="start_date">
										</div>	
									
										<div class="col-md-3">
											<label class="m-t-20"><?php echo lang('end_date');?><i style="color:red;">*</i></label>
											<input type="text" name="end_date" class="form-control required"  value="<?php echo !empty($end_date) ? $end_date : '';?>" id="end_date">
										</div>
									</div>
									<!-- Offer start / end date fields end here -->	
									
									<!-- Offer start / end time fields start here -->
									<div class="row form-group form-material">
										 <div class="col-md-3">
											<label class="m-t-20"><?php echo lang('start_time');?><i style="color:red;">*</i></label>
											<input type="text" name="start_time" class="form-control required" value="<?php echo !empty($start_time) ? $start_time : '';?>" id="start_time">
										</div>	
									
										<div class="col-md-3">
											<label class="m-t-20"><?php echo lang('end_time');?><i style="color:red;">*</i></label>
											<input type="text" name="end_time" class="form-control required"  value="<?php echo !empty($end_time) ? $end_time : '';?>" id="end_time">
										</div>
									</div>
									<!-- Offer start / end time fields end here -->	
						
					
						</div>	
						<div class="row pb-3 float-right">
							<div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'backend/vip_offer/beacon' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					</div>
					<?php
					$tempbeconid = ($beacon_offer_id == 0) ? 0: decode($beacon_offer_id); ?>
					<input type="hidden" value="<?php echo set_value('beacon_offer_id', $tempbeconid); ?>" name='beacon_offer_id' id='beacon_offer_id'>
					<input type="hidden" value="<?php echo set_value('beacon_id', $beacon_id); ?>" name='beacon_id' id='beacon_id'>
				<?php echo form_close();?>		
			</div>	
		</div>	
	</div>	
</div>	

<script type="text/javascript">
	// Initialize material Date picker    
	$('#start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});

	$('#end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});
	
	$('#start_time').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		date: false,
		format : 'H:m:s'
	});

	$('#end_time').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		date: false,
		format : 'H:m:s'
	});
	
	$(document).ready(function() {
		$("#add_offer_form").validate({
			// post form to action url
		});
	});
	
	$("#page-changer select").change(function() {
		$("#page-changer select option:selected").val();
	});

	/**
	* Manage select all stores
	*/
	$('#select_all').click(function() {
		if($("#select_all").is(':checked')) {
			$('#beacon_store_id option').prop('selected', true);
		} else {
			$('#beacon_store_id option').prop('selected', false);
		}
		
	});
	   /**
	* unselect check all button status 
	*/
	$('#beacon_store_id').change(function() {
		$("#select_all").attr('checked', false);
	})
	
function checkCheckBox(checkbox){
	//alert(checkbox);
	for(var i=1;i<=3;i++){
		if(checkbox != 'check'+i) {
			$('#check'+i).attr('checked', false);
		}
	}
}
	$("#image-picker_1").click(function() {
		$("input[name='offer_image_1']").click();
	});
	$("#image-picker_2").click(function() {
		$("input[name='offer_image_2']").click();
	});
	$("#image-picker_3").click(function() {
		$("input[name='offer_image_3']").click();
	});
	$("#qr-image-picker").click(function() {
		$("input[name='barcode_image']").click();
		
	});
	function read_url_image(input) {
		//alert(input.id);
		if(input.id == 'offer_image_1') {
			var pickerId	= '#image-picker_1';
			var fieldId	= '#offer_image_1';
			$("#remove_btn_1").show();
		} else if(input.id == 'offer_image_2') {
			var pickerId	= '#image-picker_2';
			var fieldId	= '#offer_image_2';
			$("#remove_btn_2").show();
		} else if(input.id == 'offer_image_3') {
			var pickerId	= '#image-picker_3';
			var fieldId	= '#offer_image_3';
			$("#remove_btn_3").show();
		}
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					bootbox.alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	function read_qr_image(input) {
		if(input.id == 'barcode_image') {
			var pickerId	= '#qr-image-picker';
			var fieldId	= '#barcode_images';
		} 
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var imageType = /image.*/;
			if (file.type.match(imageType)) {
				var reader = new FileReader();
				reader.onload = function (e) {
					//alert(pickerId);
					$(pickerId).attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				$(fieldId).val("");
				bootbox.alert("File not supported!");
			}
		}
	}
	
	$("#offer_price").on("keyup", function(){
			var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
			val = this.value;
		
		if(!valid){
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});
	
	
	$( "#add_offer_form" ).submit(function( event ) {
		var res = checkBeacon();
		if(res != '' ) {
			var msg = res+"<?php echo lang('cannot_place_offer');?>";
			bootbox.alert(msg);	
			
		} else {
			return;
		}
	  event.preventDefault();
	});
	
	function checkBeacon() {
		var response = '';
		var store_ids		=  $( "#beacon_store_id" ).val();
		var beacon_offer_id =  $( "#beacon_offer_id" ).val();
		/*Re-Formatting date*/
		var start_date	=  $( "#start_date" ).val();
		var split_start_date	= 	start_date.split("-");
		var new_start_date	= split_start_date[0]+'-'+split_start_date[1]+'-'+split_start_date[2];
		
		var end_date	=  $( "#end_date" ).val();
		var split_end_date	= 	end_date.split("-");
		var new_end_date	= split_end_date[0]+'-'+split_end_date[1]+'-'+split_end_date[2];
		
		var sDate 		=  new Date(new_start_date);
		var eDate 		=  new Date(new_end_date);
		
		var nowTemp = new Date('<?php echo date('Y-m-d'); ?>');
		var today = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
		
		var start_time 	=  $( "#start_time" ).val();
		var end_time 	=  $( "#end_time" ).val();
		
		if(start_date != '' && today > eDate) { 
			bootbox.alert("<?php echo lang('end_date_is_greater');?>");
			$( "#end_date" ).val('');
		} else if(start_date != '' && end_date != '' && sDate > eDate) {
			bootbox.alert("<?php echo lang('start_date_is_greater');?>");
			$( "#start_date" ).val('');
		} else if(start_time >= end_time) {
			bootbox.alert("<?php echo lang('start_time_is_greater');?>");
			$( "#start_time" ).val('');
		} else if(store_ids != '' && store_ids != null && start_time != '' && end_time != '' && start_date != '' && end_date != '' ) {
			$('#loader1').fadeIn();
			$.ajax ({     
				type: "POST",
				data : {store_ids:store_ids,start_date:start_date,end_date:end_date,start_time:start_time,end_time:end_time,beacon_offer_id:beacon_offer_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
				url: "<?php echo base_url() ?>backend/vip_offer/checkFreeBeacon",
				async: false,
				success: function(data){ 
					$('#loader1').fadeOut();
					response = data;
				}, 
				error: function(error){
					$('#loader1').fadeOut();
					bootbox.alert(error);
				}
			});
		}
		return response;
	}
	
	/**
	* Function used to remove image
	*/
	function remove_image(image_type) {
		bootbox.confirm("Are you sure want to delete this image?", function(result) {
			if(result==true) {
				// unset image data
				$("#image-picker_"+image_type).attr('src', '<?php echo base_url('uploads/upload.png');?>');
				var offer_id = $('#beacon_offer_id').val();
				$.ajax ({
					type: "POST",
					data : {offer_id:offer_id,image_type:image_type},
					url: "<?php echo base_url() ?>backend/vip_offer/remove_offer_image",
					async: false,
					success: function(data){ 
						if(data) {
							var file = $('#image-picker_'+image_type).attr('src','<?php echo base_url('uploads/upload.png');?>');
							$("#remove_btn_"+image_type).hide();
						}
					}, 
					error: function(error){
						bootbox.alert(error);
					}
				});
			}
		});
	}

			 $("#offer_detail_page_image_picker").click(function() {
        $("input[name='offer_detail_page_image']").click();
    });

	function read_offer_detail_page_image(input) {
    
        var pickerId= '#'+input.id+'_picker';
        var fieldId	= '#'+input.id;
        $(fieldId+'_val').val(input.value);
        
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var size = file.size/1000000; // get image size in mb;
            if(size <= 3.0955135) {
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //alert(pickerId);
                        $(pickerId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $(fieldId).val("");
                    bootbox.alert("File not supported!");
                }
            } else {
                $(fieldId).val("");
                bootbox.alert("File size should be less then 3MB!");
            }
        }
    }



     	// Initialize description mce editor
	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "BeaconmceEditor",
		height: '230px',
		width: '585px',
		theme_advanced_resizing: true,
		plugins: ['link image'],
		/*image_advtab: true,
		file_picker_callback: function(callback, value, meta) {
		if (meta.filetype == 'image') {
		$('#upload').trigger('click');
		$('#upload').on('change', function() {
		var file = this.files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
		callback(e.target.result, {
		alt: ''
		});
		};
		reader.readAsDataURL(file);
		});
		}
		},*/
	});


	
</script>
