
<style>
   #canvas-holder {
   width: 100%;
   margin-top: 50px;
   text-align: center;
   }
   #chartjs-tooltip {
   opacity: 1;
   position: absolute;
   background: rgba(0, 0, 0, .7);
   color: white;
   border-radius: 3px;
   -webkit-transition: all .1s ease;
   transition: all .1s ease;
   pointer-events: none;
   -webkit-transform: translate(-50%, 0);
   transform: translate(-50%, 0);
   }
   .chartjs-tooltip-key {
   display: inline-block;
   width: 10px;
   height: 10px;
   margin-right: 10px;
   }
   .full-widthbar{margin: 15px 0;}
   .beacon-chart{margin-top:10px;}
   .clr-block-fallbackbeacon i {color:#FC8213!important;}
   .clr-block-normalbeacon i {color:#37BC98!important;}
   .year-filter-label{float:left;width:100%;}
   .prod-filter-label{float:left;width:100%;}
</style>

<!-- Row -->
<div class="row">
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url(SITE_AREA .'/settings/users/userlisting'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning"><i class="mdi mdi-account-multiple"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($user_count->count_user)?$user_count->count_user:0;  ?></h3>
							<h6 class="text-muted m-b-0">User</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url(SITE_AREA .'/settings/deviceaddress'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-info"><i class="mdi mdi-tablet"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($device_count->count_simple_offers)?$device_count->count_simple_offers:0;  ?></h3>
							<h6 class="text-muted m-b-0">Manager Devices</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url('/backend/beacon/'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary"><i class="mdi mdi-wifi"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($beacons_count->count_active_beacons)?$beacons_count->count_active_beacons:0;  ?></h3>
							<h6 class="text-muted m-b-0">Beacons</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url(SITE_AREA .'/settings/reward/rewardCampaign'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger"><i class="mdi mdi-bullhorn"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($reward_campaign_count->count_active_reward_campaign)?$reward_campaign_count->count_active_reward_campaign:0;?></h3>
							<h6 class="text-muted m-b-0">Rewards Campaign</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url('/backend/event/'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning"><i class="mdi mdi-calendar"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($events->count_active_event)?$events->count_active_event:0;  ?></h3>
							<h6 class="text-muted m-b-0">Events</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url('/backend/loyalty/'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-info"><i class="mdi mdi-cash"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($product_loyalty->count_active_loyalty)?$product_loyalty->count_active_loyalty:0;?></h3>
							<h6 class="text-muted m-b-0">Product Loyalty Program</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url(SITE_AREA .'/settings/competition'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary"><i class="mdi mdi-trophy"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($competitions->count_active_lucky)?$competitions->count_active_lucky:0;  ?></h3>
							<h6 class="text-muted m-b-0">Lucky Draw Competition</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
	
	<!-- Column -->
	<div class="col-lg-3 col-md-6">
		<div class="card">
			<div class="card-body">
				<a href='<?php echo site_url(SITE_AREA .'/settings/competition'); ?>'>
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger"><i class="mdi mdi-trophy"></i></div>
						<div class="m-l-10 align-self-center">
							<h3 class="m-b-0 font-lgiht"><?php echo !empty($competitions->count_active_leader)?$competitions->count_active_leader:0;  ?></h3>
							<h6 class="text-muted m-b-0">Leader Board Competition</h6>
						</div>
					</div>
				</a>	
			</div>
		</div>
	</div>
</div>
<!-- Row -->
	
	<!-- Row -->
	<div class="row">
		<!-- Users chart box start here -->
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">USER(S) JOINED IN YEAR</h4>		  
					<div class="chart-sect">
						<div class="camp-selct">
							<div class="filte" >
							<label for="sell" class="prod-filter-label"><?php echo lang('filter_by_year')?></label>
							<div class="col-md-4">
								<select class="form-control" id="joinedYear" onchange="yearlyUsersLineChart()">
									<?php
									// set past 5 years range
									$year1 = date('Y', strtotime(date('Y-m-d').'-4 year'));
									$year2 = date('Y');
									$years = array_reverse(range($year1, $year2),true);
									if(!empty($years)){
										foreach ($years as $year){ ?> 
											<option value = <?php echo $year; ?> ><?php echo $year; ?></option>
											<?php 
										}
									}?>
								</select>
							</div>
						</div>
							<div class="clearfix"></div>
						</div>
						<div class="">
							<div class="chart-sect" id="lineChartContent">
								<canvas id="line_chart" height="120"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Users chart box end here -->
		
		<!-- Users pie chart box start here-->
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">USERS</h4>
					<div class="chart-sect">
						<canvas id="chart-area" />
					</div>
				</div>
			</div>
		</div>	
		<!-- Users pie chart box end here-->
	</div>
	
	<!-- Row -->
	<div class="row">
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<a href='<?php echo site_url('/backend/vip_offer/beacon');?>'>
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info"><i class="ti-wallet"></i></div>
							<div class="m-l-10 align-self-center">
								<h3 class="m-b-0 font-light"><?php echo !empty($stores_offers_count->count_beacon_offers)?$stores_offers_count->count_beacon_offers:0;  ?></h3>
								<h6 class="text-muted m-b-0">Normal Beacon Offers</h6>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<!-- Column -->
		
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<a href='<?php echo site_url('/backend/vip_offer/fallback'); ?>'>
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-warning"><i class="mdi mdi-cellphone-link"></i></div>
							<div class="m-l-10 align-self-center">
								<h3 class="m-b-0 font-lgiht"><?php echo !empty($stores_offers_count->count_fallback_beacon_offers)?$stores_offers_count->count_fallback_beacon_offers:0;  ?></h3>
								<h6 class="text-muted m-b-0">Fallback Beacon Offers</h6>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<a href='<?php echo site_url('/backend/vip_offer/simple'); ?>'>
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-primary"><i class="mdi mdi-cart-outline"></i></div>
							<div class="m-l-10 align-self-center">
								<h3 class="m-b-0 font-lgiht"><?php echo !empty($stores_offers_count->count_vip_offers)?$stores_offers_count->count_vip_offers:0;  ?></h3>
								<h6 class="text-muted m-b-0">Vip Offers</h6>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<!-- Column -->
		
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<a href='<?php echo site_url(SITE_AREA .'/settings/targetpush/targetnotification'); ?>'>
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-danger"><i class="mdi mdi-bullseye"></i></div>
							<div class="m-l-10 align-self-center">
								<h3 class="m-b-0 font-lgiht"><?php echo !empty($stores_offers_count->count_target_push_offers)?$stores_offers_count->count_target_push_offers:0;  ?></h3>
								<h6 class="text-muted m-b-0">Target Push Offers</h6>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<!-- Column -->
	</div>
	
	<!-- Row -->
	<div class="row">
		<!-- Scratch & Win box start here -->
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title"><?php echo lang('scratch_n_win_chart');?></h4>
					<div class="chart-sect">
					<div class="camp-selct">
						<div class="filte">
						<label for="sell" class="year-filter-label"><?php echo lang('filter_by_year')?></label>
						<div class="col-md-3">
							<select class="form-control" id="campainYear" onchange="ajaxScrathNWinLog()">
								<?php
								// set past 5 years range
								$year1 = date('Y', strtotime(date('Y-m-d').'-4 year'));
								$year2 = date('Y');
								$years = array_reverse(range($year1, $year2),true);
								if(!empty($years)){
									foreach ($years as $year){ ?> 
										<option value = <?php echo $year; ?> ><?php echo $year; ?></option>
										<?php 
									}
								}?>
							</select>
						</div>
					</div>
						<div class="clearfix"></div>
					</div>
					<div class="">
						<div class="chart-sect" id="barChartContent">
							 <canvas id="bar-canvas"></canvas>
						</div>
					</div>					
				</div>
				</div>
			</div>
		</div>
		<!-- Scratch & Win box end here -->
		<?php 
		if(!empty($customer_beacon_offer_log)) { ?>
			<!-- Customer beacon offer view chart box start here--> 
			<div class="col-lg-6 col-md-12">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Beacon Offer Customer Views(Top 5 Store(s))</h4>
						   <div class="row">
						<?php 
						foreach($customer_beacon_offer_log as $key=>$val) { ?>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 beacon-chart">
								<div class="ad-pdbtm">
									<div class="chart-sect">
										<canvas id="beacon-chart-<?php echo $key;?>" />
									</div>
								</div>	
							</div>
						<?php 
						}?>
						</div>
					</div>
				</div>
			</div>	
			<!-- Customer beacon offer view chart box end here--> 
		<?php } ?>	
	</div>
	

<script src="<?php echo Template::theme_url('edm-dashboard/js/jquery-2.1.1.min.js'); ?>"></script> 
<script src="<?php echo Template::theme_url('js/chartjs.js'); ?>"></script>
<script src="<?php echo Template::theme_url('js/Chart.bundle.min.js'); ?>"></script>
<script src="<?php echo Template::theme_url('edm-dashboard/js/utils.js'); ?>"></script> 
<script type="text/javascript">
	function show_loader(action,className){
		var div = "<div class='loader loaderdiv'><i class='fa fa-refresh fa-spin' style='font-size: 28px;color: #111;position: absolute;left: 50%;bottom: 50%;transform: translate(-50%,-50%);'></i></div>";
		if(action){
			$('.'+className).css({"position": "relative","padding": "0px"});
			$('.'+className).prepend(div);
		}else{
			$('.'+className).css({"position":"","padding": ""});
			$('.loaderdiv').remove();
		}
	}
	
	// Manage users joined yearly chart
	function drawJoinedUsersYearLineChart(is_ajax=0,users_datasets=[]) {
		if(is_ajax != 1) {
			var users_datasets = [];
			<?php
			// prepare yearly android users join data
			if(!empty($joined_device_log_['android_users'])) {?>
				var android_data = {};
				// prepare android users array
				var android_users = JSON.parse('<?php echo json_encode($joined_device_log_['android_users']); ?>');
				var android_users_array = new Array();
				for (var i = 0; i < android_users.length; i++) {
					android_users_array.push(android_users[i]);
				}
				android_data['label'] = 'Android';
				android_data['borderColor'] = 'rgba(0, 188, 212, 0.75)';
				android_data['backgroundColor'] = 'rgba(0, 188, 212, 0.3)';
				android_data['pointBorderColor'] = 'rgba(0, 188, 212, 0)';
				android_data['pointBackgroundColor'] = 'rgba(0, 188, 212, 0.9)';
				android_data['pointBorderWidth'] = 1;
				android_data['data'] = android_users_array;
				console.log(android_data);
				users_datasets.push(android_data);
			<?php 
			}
			// prepare yearly ios users join data
			if(!empty($joined_device_log_['ios_users'])) {?>
				var ios_data = {};
				// prepare ios users array
				var ios_users = JSON.parse('<?php echo json_encode($joined_device_log_['ios_users']); ?>');
				var ios_users_array = new Array();
				for (var i = 0; i < ios_users.length; i++) {
					ios_users_array.push(ios_users[i]);
				}
				ios_data['label'] = 'iOS';
				ios_data['borderColor'] = 'rgba(233, 30, 99, 0.75)';
				ios_data['backgroundColor'] = 'rgba(233, 30, 99, 0.3)';
				ios_data['pointBorderColor'] = 'rgba(233, 30, 99, 0)';
				ios_data['pointBackgroundColor'] = 'rgba(233, 30, 99, 0.9)';
				ios_data['pointBorderWidth'] = 1;
				ios_data['data'] = ios_users_array;
				users_datasets.push(ios_data);
				console.log(ios_data);
			<?php 
			} ?>
		}
		console.log(users_datasets);
		var lineChartData = {
			type: 'line',
			data: {
				labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
				datasets: users_datasets
			},
			options: {
				responsive: true,    
				legend: false             
			}
		};
		var lctx   = document.getElementById("line_chart").getContext("2d");
		window.myline = new Chart(lctx,lineChartData);
	}
	
	lconfig = {
		type: 'line',
		data: {
			labels: ["<?php echo $joined_device_log['month1']; ?>","<?php echo $joined_device_log['month2']; ?>","<?php echo $joined_device_log['month3']; ?>","<?php echo $joined_device_log['month4']; ?>","<?php echo $joined_device_log['month5']; ?>","<?php echo $joined_device_log['month6']; ?>"],
			datasets: [{
				label: "Android",
				data: ["<?php echo $joined_device_log['month1_AND']; ?>","<?php echo $joined_device_log['month2_AND']; ?>","<?php echo $joined_device_log['month3_AND']; ?>","<?php echo $joined_device_log['month4_AND']; ?>","<?php echo $joined_device_log['month5_AND']; ?>","<?php echo $joined_device_log['month6_AND']; ?>"],
				borderColor: 'rgba(0, 188, 212, 0.75)',
				backgroundColor: 'rgba(0, 188, 212, 0.3)',
				pointBorderColor: 'rgba(0, 188, 212, 0)',
				pointBackgroundColor: 'rgba(0, 188, 212, 0.9)',
				pointBorderWidth: 1
			}, {
				label: "iOS",
				data: ["<?php echo $joined_device_log['month1_IOS']; ?>","<?php echo $joined_device_log['month2_IOS']; ?>","<?php echo $joined_device_log['month3_IOS']; ?>","<?php echo $joined_device_log['month4_IOS']; ?>","<?php echo $joined_device_log['month5_IOS']; ?>","<?php echo $joined_device_log['month6_IOS']; ?>"],
				borderColor: 'rgba(233, 30, 99, 0.75)',
				backgroundColor: 'rgba(233, 30, 99, 0.3)',
				pointBorderColor: 'rgba(233, 30, 99, 0)',
				pointBackgroundColor: 'rgba(233, 30, 99, 0.9)',
				pointBorderWidth: 1
			}]
		},
		options: {
			responsive: true,
			legend: false
		}
	}
	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};
	var cconfig = {
		type: 'pie',
		data: {
		datasets: [{
			data:['<?php echo !empty($user_count->count_android)?$user_count->count_android:0;  ?>','<?php echo !empty($user_count->count_ios)?$user_count->count_ios:0;  ?>','<?php echo !empty($user_count->count_other)?$user_count->count_other:0;  ?>'],              
			backgroundColor: [
				window.chartColors.red,
				window.chartColors.orange,
				window.chartColors.yellow
			],
			label: 'Dataset 1'
			}],
			labels: [
				"Android",
				"iOS",
				"Other"
			]
		},
		options: {
			responsive: true
		}
	};
	
	// Draw email year bar chart
	function drawScratchYearBarchart(winner_data,sec_chance_data,no_win_data) {
		var barChartData = {
			labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			datasets: [{
				label: 'Winner',
				backgroundColor: window.chartColors.red,
				data: winner_data
			}, {
				label: 'Second Chance',
				backgroundColor: window.chartColors.blue,
				data: sec_chance_data
			}
			, {
				label: 'No Win',
				backgroundColor: window.chartColors.green,
				data: no_win_data
			}]
		};
		
		bcconfig ={
			type: 'bar',
			data: barChartData,
			options: {
				title:{
					display:false,
					text:""
				},
				tooltips: {					
					intersect: false
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true
					}]
				}
			}
		};
		// remove existing chart canvas
		$('#bar-canvas').remove();
		$('#barChartContent').append('<canvas id="bar-canvas"></canvas>');
		var bcctx   = document.getElementById("bar-canvas").getContext("2d");
		window.mybc = new Chart(bcctx,bcconfig);
	}
	
	// Manage yearly scratch&win yearly log
	function ajaxScrathNWinLog() {
		var campainYear = $('#campainYear').val();
		if(campainYear !==0 || campainYear !== ''){
			$.ajax({
				type: "GET",
				dataType: "json",
				url: '<?php echo site_url("backend/dashboard/yearly_scratch_win_bar_chart"); ?>',
				data: {campainYear:campainYear},
				beforeSend: function() {
					show_loader(1,'campaign-bar-chart-loader');
				},
				success: function(data){
					if(data.result){
						drawScratchYearBarchart(data.winner_array,data.sec_chance_array,data.no_win_array)
						return false;					
					}
				},
				complete:function(){
					show_loader(0,'campaign-bar-chart-loader');
				}
			});
		}
	} 
	
    var beacon_chart_config = [];
	<?php
	// prepare beacon offer customers view charts
	if(!empty($customer_beacon_offer_log)) {
		foreach($customer_beacon_offer_log as $key=>$val) { ?>
			var beacon_view_count = JSON.parse('<?php echo json_encode($val['beacon_view_count']); ?>');
			var beacon_count_array = new Array();
			for (var i = 0; i < beacon_view_count.length; i++) {
				beacon_count_array.push(beacon_view_count[i]);
			}
			var stores = JSON.parse('<?php echo json_encode($val['stores']); ?>');
			var store_array = new Array();
			for (var i = 0; i < stores.length; i++) {
				store_array.push(stores[i]);
			}
			
			var beaconconfig = {
				type: 'doughnut',
				data: {
				datasets: [{
					data: beacon_count_array,              
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.orange,
						window.chartColors.yellow,
						window.chartColors.green,
						window.chartColors.blue,
						
					],
					label: 'Dataset 1'
					}],
					labels: store_array
			  },
			options: {
				title:{
					display:true,
					text:"<?php echo $val['beacon_offer_name']; ?>"
				},
				responsive: true,
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};
		beacon_chart_config.push(beaconconfig);
		<?php
		}
	} ?>
   
	// Manage yearly product stastistics on line chart
	function yearlyUsersLineChart() {
		var joinedYear =  $('#joinedYear').val();
		if(joinedYear !==0 || joinedYear !== ''){
			$.ajax({
				type: "GET",
				dataType: "json",
				url: '<?php echo site_url("backend/dashboard/yearly_joined_users_linechart"); ?>',
				data: {joinedYear:joinedYear},
				beforeSend: function() {
					show_loader(1,'prod-line-chart-loader');
				},
				success: function(data){
					if(data.result){
						var android_users = (data.android_users) ? data.android_users : 0;
						var ios_users   = (data.ios_users) ? data.ios_users : 0;
						// render chart
						var datasets = [{
							label: "Android",
							data: android_users,
							borderColor: 'rgba(0, 188, 212, 0.75)',
							backgroundColor: 'rgba(0, 188, 212, 0.3)',
							pointBorderColor: 'rgba(0, 188, 212, 0)',
							pointBackgroundColor: 'rgba(0, 188, 212, 0.9)',
							pointBorderWidth: 1
						}, {
							label: "iOS",
							data: ios_users,
							borderColor: 'rgba(233, 30, 99, 0.75)',
							backgroundColor: 'rgba(233, 30, 99, 0.3)',
							pointBorderColor: 'rgba(233, 30, 99, 0)',
							pointBackgroundColor: 'rgba(233, 30, 99, 0.9)',
							pointBorderWidth: 1
						}];
						console.log(datasets);
						drawJoinedUsersYearLineChart(1,datasets);
						return false;
						
					}
				},
				complete:function(){
					show_loader(0,'prod-line-chart-loader');
				}
			});
		}
	} 
    		  			  
	window.onload = function() {
		drawJoinedUsersYearLineChart(0);
		var cctx   = document.getElementById("chart-area").getContext("2d");
		//var lctx   = document.getElementById("line_chart").getContext("2d");
		window.myPie  = new Chart(cctx,cconfig);
		//window.myline = new Chart(lctx,lconfig);
		// load beacon customer view chartes
		if(beacon_chart_config.length > 0) {
			for (var i = 0; i < beacon_chart_config.length; i++) {
				var bctx   = document.getElementById("beacon-chart-"+i).getContext("2d");
				window.myDoughnut  = new Chart(bctx,beacon_chart_config[i]);    
			}
		}
		// load scratch&win bar chart initially
		drawScratchYearBarchart(<?php echo $campaign_compitition_log['winner_data'];?>,<?php echo $campaign_compitition_log['sec_chance_data'];?>,<?php echo $campaign_compitition_log['no_win_data'];?>);
	  };
   
</script>
