<div class="col-md-12 page-header headingnew">
<h4 class="card-title">Admin Dashboard</h4>
</div>
<?php $this->load->view('admin/dashboard/dashboard');?>

<?php $image_path =Template::theme_url('images').'/'; ?>
<!--
<div class="row-fluid">
<div class="span12">
	<ul class="widgeticons row-fluid">
		<li class="one_fifth link_menu" id="coupons">
			<a class="equal_height" title="<?php echo lang('acart_menu_dashboard'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-dashboard"></i></span>
				 <span><?php echo lang('acart_menu_dashboard'); ?></span>
			</a>
		</li>
		<?php if($this->session->userdata('role_id') == 1) { ?>
			<li class="one_fifth link_menu" id="orders">
				<a href="<?php echo site_url(SITE_AREA . '/settings/users/') ?>" class="equal_height" title="<?php echo lang('acart_menu_users'); ?>" >
					 <span class="admin_dashboar_main"><i class="fa fa-users"></i></span>
					 <span><?php echo lang('acart_menu_users'); ?></span>
				</a>
			</li>
			<li class="one_fifth link_menu" id="customers">
				<a href="<?php echo site_url(SITE_AREA . '/settings/permissions/') ?>" class="equal_height" title="<?php echo lang('acart_menu_permissions'); ?>">
					 <span class="admin_dashboar_main"><i class="fa fa-eye"></i></span>
					 <span><?php echo lang('acart_menu_permissions'); ?></span>
				</a>
			</li>		
			<li class="one_fifth link_menu" id="reg_authors">
				<a href="<?php echo site_url(SITE_AREA . '/settings/roles/') ?>" class="equal_height" title="<?php echo lang('acart_menu_roles'); ?>" >
					 <span class="admin_dashboar_main"><i class="fa fa-user"></i></span>
					 <span><?php echo lang('acart_menu_roles'); ?></span>
				</a>
			</li>	
		<?php } 
		/*?>
		<li class="one_fifth link_menu" id="reports">
			<a class="equal_height" title="<?php echo lang('acart_menu_settings'); ?>">
				 <span class="admin_dashboar_main"><i class="fa fa-cogs"></i></span>
				 <span><?php echo lang('acart_menu_settings'); ?></span>
			</a>
		</li>
		
		<li class="one_fifth link_menu" id="pagination">
			<a class="equal_height" title="<?php echo lang('acart_menu_maintenance'); ?>" >
				<span class="admin_dashboar_main"><i class="fa fa-wrench"></i></span>
				 <span><?php echo lang('acart_menu_maintenance'); ?></span>
			</a>
		</li>
		
		<li class="one_fifth link_menu">
			<a class="equal_height" title="<?php echo lang('acart_menu_backups'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-hdd-o"></i></span>
				 <span><?php echo lang('acart_menu_backups'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="coupons">
			<a class="equal_height" title="<?php echo lang('acart_menu_campaign'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-cubes"></i></span>
				 <span><?php echo lang('acart_menu_campaign'); ?></span>
			</a>
		</li>
		<?php */ ?>
		<li class="one_fifth link_menu" id="beacons">
			<a href="<?php echo base_url().'backend/beacon';?>" class="equal_height" title="<?php echo lang('acart_menu_beacons'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-wifi"></i></span>
				 <span><?php echo lang('acart_menu_beacons'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="offer">
			<a href="<?php echo base_url().'backend/offer';?>" class="equal_height" title="<?php echo lang('acart_menu_in_store_offers'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-gift"></i></span>
				 <span><?php echo lang('acart_menu_in_store_offers'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="beacon_offer">
			<a href="<?php echo base_url().'backend/vip_offer/beacon';?>" class="equal_height" title="<?php echo lang('acart_menu_beacon_offers'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-gift"></i></span>
				 <span><?php echo lang('acart_menu_beacon_offers'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="simple_offer">
			<a href="<?php echo base_url().'backend/vip_offer/simple';?>" class="equal_height" title="<?php echo lang('acart_menu_simple_offers'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-gift"></i></span>
				 <span><?php echo lang('acart_menu_simple_offers'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="promotion">
			<a href="<?php echo base_url().'backend/promotion';?>" class="equal_height" title="<?php echo lang('acart_menu_promotion'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-bullhorn"></i></span>
				 <span><?php echo lang('acart_menu_promotion'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="event">
			<a href="<?php echo base_url().'backend/event';?>" class="equal_height" title="<?php echo lang('acart_menu_event'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-calendar"></i></span>
				 <span><?php echo lang('acart_menu_event'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="loyalty">
			<a href="<?php echo base_url().'backend/loyalty' ?>" class="equal_height" title="<?php echo lang('acart_menu_loyalty'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-calendar"></i></span>
				 <span><?php echo lang('acart_menu_loyalty'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="competition">
			<a href="<?php echo base_url().'admin/settings/competition' ?>" class="equal_height" title="<?php echo lang('acart_menu_competition'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-trophy"></i></span>
				 <span><?php echo lang('acart_menu_competition'); ?></span>
			</a>
		</li>
		<li class="one_fifth link_menu" id="edm">
			<a href="<?php echo base_url().'admin/settings/edm' ?>" class="equal_height" title="<?php echo lang('acart_menu_edm'); ?>" >
				 <span class="admin_dashboar_main"><i class="fa fa-envelope"></i></span>
				 <span><?php echo lang('acart_menu_edm'); ?></span>
			</a>
		</li>
	</ul>
</div><!--span4-->
<!--
</div>
--->
<!--row-fluid-->

