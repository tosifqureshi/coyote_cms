<div class="page-header">
    <h1>
		<?php 
		 if(!empty($id)) { ?>
			 <i class="fa fa-edit"></i>
		<?php } else { ?>
			 <i class="fa fa-plus-square-o"></i>
		<?php } ?>
		<?php echo $email_template_title;?>
	</h1>
</div>

<?php echo form_open_multipart('backend/email_template/create/'.(isset($email_id)?encode($email_id):''),'id="add_email_template_form"'); ?>

<div class="tab-content">
	<div class="tab-pane active" id="content_tab">
		<div class="form-wrapper clearboth">
			<div class="form-box">
				<label for="subject">
					<?php echo lang('email_template_subject');?>
					<i style="color:red;">*</i>
				</label>
				<div class="control-group">	
					<?php
					$data	= array('name'=>'subject', 'value'=>set_value('subject', $subject), 'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_email_template_subject'));
					echo form_input($data);
					?>
				</div>
			</div>
			<div class="form-box">
				<label for="purpose">
					<?php echo 'purpose';?>
					<i style="color:red;">*</i>
				</label>
				<div class="control-group">	
					<?php 
						  $data	= array('name'=>'purpose','value'=>set_value('purpose',$purpose), 'class'=>'span8 required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Enter the email purpose');
						  echo form_input($data);
					 ?>
				</div>
			</div>
			<div class="form-box">
				<label for="required_tag">
					<?php echo 'Required Tags';?>
					<i style="color:red;">*</i>
				</label>
				<div class="control-group">	
				 <?php echo $required_tag; ?>	
				 </div>
			</div>
			<div class="form-box">
				<label for="body">
					<?php echo lang('email_template_template');?>
					<i style="color:red;">
						*
					</i>
				</label>
				<div class="">	
					<?php
					$data	= array('name'=>'body','id'=>'body', 'value'=>set_value('body',$body), 'cols'=>'80', 'rows'=>'10');
					echo form_textarea($data);
					?>
				</div><a href="javascript:popWin()">Template Preview</a>				
			</div>					
		</div>
	</div>
</div>
	
<div class="text-right btn_wrapZ mt10">
	<button type="submit" name="save" class="btn btn-primary">
		<?php echo lang('form_save');?>
	</button>
</div>	

<script src="<?php echo  Template::theme_url('js/ckeditor/ckeditor.js'); ?>"></script>
<script src="<?php echo  Template::theme_url('js/ckfinder/ckfinder.js'); ?>"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$("#add_email_template_form").validate({
				// post form to action url
			});
		});
var editor = CKEDITOR.replace( 'body', {
    filebrowserBrowseUrl : '<?php echo base_url('templates/system/js/ckfinder/ckfinder.html'); ?>',
    filebrowserImageBrowseUrl : '<?php echo base_url('templates/system/js/ckfinder/ckfinder.html?type=Images'); ?>',
    filebrowserFlashBrowseUrl : '<?php echo base_url('templates/system/js/ckfinder/ckfinder.html?type=Flash'); ?>',
    filebrowserUploadUrl : '<?php echo base_url('templates/system/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'); ?>',
    filebrowserImageUploadUrl : '<?php echo base_url('templates/system/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'); ?>',
    filebrowserFlashUploadUrl : '<?php echo base_url('templates/system/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'); ?>'
});

CKFinder.setupCKEditor(editor, '../');
</script>
