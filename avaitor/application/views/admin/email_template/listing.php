<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('email_template') ?></h4>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<div class="table-responsive m-t-40">
					<table id="template_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('email_template_sno'); ?></th>
								<th><?php echo lang('email_template_purpose'); ?></th>
								<th><?php echo lang('email_template_subject'); ?></th>
								<th>Action<?php //echo lang('email_template_subject'); ?></th>
							</tr>
							
						</thead>
						
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($emailTemplates) && is_array($emailTemplates) && count($emailTemplates)>0):
								foreach($emailTemplates as $emailTemplate) : 
									// set emailTemplates id
									$id = encode($emailTemplate['id']);?>
									<tr>
										<td>
											<?php echo $i;?>
										</td>
										
										<td>
											<a href="<?php echo site_url("backend/email_template/create"); echo '/' . ($id);?>">
												<?php if(strlen($emailTemplate['purpose']) > 30) { echo substr($emailTemplate['purpose'],0,30).'...'; } else { echo $emailTemplate['purpose']; }?>
											</a>
										</td>
								
										<td>
											<?php if(strlen($emailTemplate['subject']) > 30) { echo substr($emailTemplate['subject'],0,30).'...'; } else { echo $emailTemplate['subject']; }?>
										</td>
										<td>
											<a href="<?php echo base_url('backend/email_template/template_view/'.$id) ?>" >Template Preview</a>
										</td>
									</tr>
									<?php 
									$i++;
								endforeach ; 
							endif; ?>
						</tbody>
					</table>
				</div>		
			</div>	
		</div>	
	</div>	
</div>	

<script>
	$(document).ready(function() {
		$('#template_table').DataTable();
	});
</script>	
