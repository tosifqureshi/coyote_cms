<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Email Preview</title>
</head>
<body>
<?php 
echo isset($records->body) ? htmlspecialchars_decode($records->body) : '';
?>
</body>
</html>
