<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $event_title;?></h4>
				<div class="p-t-20">
					<?php echo form_open_multipart('backend/event/create/'.(isset($records->event_id) ? encode($records->event_id) : ''),'id="add_event_form" class="  form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Event name start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('event_name');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'event_name', 'value'=>set_value('event_name', isset($records->event_name) ? $records->event_name : ''), 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_event_name'));
									echo form_input($data);?>
								</div>
								<!-- Event name end here -->
								
								<!-- Event address start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('event_address');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'event_address','id'=>'event_address', 'value'=>set_value('event_address', isset($records->event_address) ? $records->event_address : '') , 'class'=>'required form-control form-control-line');
									echo form_input($data);
									?>
								</div>
								<div class="form-group form-material" style="display:none">
									<div id="map_canvas" style="height: 250px;width: 500px;"></div>
									<div style="clear:both;">&nbsp;</div>
								</div>
								<!-- Event address end here -->
								
								<!-- Event latitude start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('event_latitude');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'event_latitude','id'=>'event_latitude', 'value'=>set_value('event_latitude', isset($records->event_latitude) ? $records->event_latitude : '') , 'class'=>'MapLat form-control form-control-line', 'readonly'=>'readonly');
									echo form_input($data);?>
								</div>
								<!-- Event latitude end here -->
								
								<!-- Event longitude start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('event_longitude');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'event_longitude','id'=>'event_longitude', 'value'=>set_value('event_longitude', isset($records->event_longitude) ? $records->event_longitude : '') , 'class'=>'MapLon form-control form-control-line', 'readonly'=>'readonly');
									echo form_input($data);?>
								</div>
								<!-- Event longitude end here -->
								
								<!--  Event date fields start here -->
								<div class="row form-group form-material">
									 <div class="col-md-6">
										<label class="m-t-20"><?php echo lang('event_date');?><i style="color:red;">*</i></label>
										<input type="text" name="event_date" class="form-control required" value="<?php echo !empty($records->event_date) ? date("d-m-Y",strtotime($records->event_date)) : '';?>" id="event_date">
									</div>	
								</div>
								<!-- Event date fields end here -->
		
								<!-- Event short description start here -->
								<div class="form-group form-material">
									<label><?php echo lang('event_short_description');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'event_short_description', 'value'=>set_value('event_short_description', isset($records->event_short_description) ? $records->event_short_description : '') , 'class'=>'required form-control form-control-line','rows'=>3);
									echo form_textarea($data);?>
								</div>
								<!-- Event short description end here -->
								
								<!-- Event long description start here -->
								<div class="form-group ">
									<label for="promo_long_desc"><?php echo lang('event_long_description');?></label>
									<?php
									$data	= array('name'=>'event_long_description', 'class'=>'mceEditor redactor form-control', 'value'=>set_value('event_long_description', isset($records->event_long_description) ? $records->event_long_description : ''), 'cols'=>'40', 'rows'=>'10');
									echo form_textarea($data);
									?>
								</div>	
								<!-- Event long description end here -->
							</div>	
							
							<!-- Event banner image box start here-->	
							<div class="col-md-6">
								<div class="form-group ">
									<label>
										<?php echo lang('event_banner_image');?><i style="color:red;">*</i>
									</label>
									<div class="row">
										<div class="col-md-4">
											<div class="img-box">
												<?php
												$event_image_val_1 = (isset($records->event_image_1) && !empty($records->event_image_1)) ? $records->event_image_1 :'';
												if(isset($records->event_image_1) && $records->event_image_1 != '') {
												$event_image_1 = base_url('uploads/event_images/'.$records->event_image_1);
												$title = lang('tooltip_event_image');;
													 $event_image_1_doc = $this->config->item('document_path').'uploads/event_images/'.$records->event_image_1; 
													 if(!file_exists($event_image_1_doc)){
														$event_image_1 = base_url('uploads/No_Image_Available.png');
														$title = "No Image Available";
													 }
												} else {
													$title = lang('tooltip_event_image');
													$event_image_1 = base_url('uploads/upload.png');
												}
												?>
												<div class="cell-img" id="banner_image" >
													<img id="image-picker_1" class="image-group" src="<?php echo $event_image_1; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
												</div>
												<?php
												$data	= array('name'=>'event_image_1', 'id'=>'event_image_1', 'value'=>set_value('event_image_1',isset($records->event_image_1) ? $records->event_image_1 : ''),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_event_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
												echo form_upload($data);
												?>
											</div>
										</div>	
									</div>	
									
									<label>
										<?php echo lang('event_image');?><i style="color:red;">*</i>
									</label>
									<div class="row">
										<?php 
										$temp = 2;
										if(isset($gallary_records)) {
											foreach($gallary_records as $gallary_record) {
												$img_display = (isset($gallary_record['image']) && !empty($gallary_record['image'])) ? '' :'dn';
												$event_image_val = (isset($gallary_record['image']) && !empty($gallary_record['image'])) ? $gallary_record['image'] :'';
												if(isset($gallary_record['image']) && $gallary_record['image'] != '') {
													$event_image = base_url('uploads/event_images/'.$gallary_record['image']);
													$title = lang('tooltip_event_image');;
														
													$event_image_doc = $this->config->item('document_path').'uploads/event_images/'.$gallary_record['image']; 
													if(!file_exists($event_image_doc)){
														$event_image = base_url('uploads/No_Image_Available.png');
														$title = "No Image Available";
													}
												} else {
													$title = lang('tooltip_event_image');
													$event_image = base_url('uploads/upload.png');
												} ?>
												<div class="col-md-4">
													<div class="img-box">
														<a id="remove_btn_<?php echo $temp?>" class="<?php echo $img_display;?>"  href="javascript:void(0);" onclick="remove_image(<?php echo $gallary_record['event_gallery_id'];?>,<?php echo $temp?>)">×</a>
														<div class="cell-img" id="banner_image_<?php echo $temp?>" >
															<img id="image-picker_<?php echo $temp?>" class="image-group" src="<?php echo $event_image; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
															<input type="hidden" value="<?php echo $gallary_record['event_gallery_id'];?>" name='event_gallery_id_<?php echo $temp?>' id='event_gallery_id_<?php echo $temp?>'>
														</div>
														<?php
															$data	= array('name'=>'event_image_'.$temp, 'id'=>'event_image_'.$temp, 'value'=>set_value('event_image_'.$temp,isset($gallary_record['image']) ? $gallary_record['image'] : ''),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_event_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
															echo form_upload($data);
														?>
														<input type="hidden" value="<?php echo $event_image_val; ?>" name='event_image_val_<?php echo $temp?>' id='event_image_val_<?php echo $temp?>'>
													</div>
												</div>
												
												<?php 
												$temp++;
											}	
										} 
										for($i=$temp;$i<=7;$i++) { ?>	
											<div class="col-md-4">
												<div class="img-box">
													<?php
													$title = lang('tooltip_event_image');
													$event_image_2 = base_url('uploads/upload.png');?>
													<div class="cell-img" id="banner_image_<?php echo $i?>" >
														<img id="image-picker_<?php echo $i?>" class="image-group" src="<?php echo $event_image_2; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
														<input type="hidden" value="" name='event_gallery_id_<?php echo $i?>' id='event_gallery_id_<?php echo $i?>'>
													</div>
													<?php
													$data	= array('name'=>'event_image_'.$i, 'id'=>'event_image_'.$i, 'value'=>'',  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_event_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
													echo form_upload($data);?>
													<input type="hidden" value="" name='event_image_val_<?php echo $i?>' id='event_image_val_<?php echo $i?>'>
												</div>
											</div>
										<?php } ?>
										
										<div class="alert red-alert alert-danger alert-dismissible ">
											<i class="icon fa fa-warning"></i>
											Upload up to 3MB images. 
										</div>  
									</div>
								</div>
								<!-- Prmotion banner image box end here-->		 
							</div>	 
						</div>	
						<div class="row pb-3 float-right">
							<input type="hidden" value="<?php echo $event_image_val_1;?>" name='event_image_val_1' id='event_image_val_1'>
							<div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'backend/event' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div> 
	   </div> 
   </div> 
</div>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&region=AU"></script>
<script>
     $(function () {
         var lat = -27.46841,
             lng = 153.03573900000004,
             latlng = new google.maps.LatLng(lat, lng),
             image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png';

         //zoomControl: true,
         //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,

         var mapOptions = {
             center: new google.maps.LatLng(lat, lng),
             zoom: 13,
             mapTypeId: google.maps.MapTypeId.ROADMAP,
             panControl: true,
             panControlOptions: {
                 position: google.maps.ControlPosition.TOP_RIGHT
             },
             zoomControl: true,
             zoomControlOptions: {
                 style: google.maps.ZoomControlStyle.LARGE,
                 position: google.maps.ControlPosition.TOP_left
             }
         },
         map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
             marker = new google.maps.Marker({
                 position: latlng,
                 map: map,
                 icon: image
             });

         var input = document.getElementById('event_address');
         var autocomplete = new google.maps.places.Autocomplete(input, {
             types: ["geocode"]
         });

         autocomplete.bindTo('bounds', map);
         var infowindow = new google.maps.InfoWindow();

         google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
             infowindow.close();
             var place = autocomplete.getPlace();
             if (place.geometry.viewport) {
                 map.fitBounds(place.geometry.viewport);
             } else {
                 map.setCenter(place.geometry.location);
                 map.setZoom(17);
             }

             moveMarker(place.name, place.geometry.location);
             $('.MapLat').val(place.geometry.location.lat());
             $('.MapLon').val(place.geometry.location.lng());
         });
         google.maps.event.addListener(map, 'click', function (event) {
             $('.MapLat').val(event.latLng.lat());
             $('.MapLon').val(event.latLng.lng());
             infowindow.close();
                     var geocoder = new google.maps.Geocoder();
                     geocoder.geocode({
                         "latLng":event.latLng
                     }, function (results, status) {
                         console.log(results, status);
                         if (status == google.maps.GeocoderStatus.OK) {
                             console.log(results);
                             var lat = results[0].geometry.location.lat(),
                                 lng = results[0].geometry.location.lng(),
                                 placeName = results[0].address_components[0].long_name,
                                 latlng = new google.maps.LatLng(lat, lng);

                             moveMarker(placeName, latlng);
                             $("#event_address").val(results[0].formatted_address);
                         }
                     });
         });
        
         function moveMarker(placeName, latlng) {
             marker.setIcon(image);
             marker.setPosition(latlng);
             infowindow.setContent(placeName);
             //infowindow.open(map, marker);
         }
     });
</script>

<script type="text/javascript">
	// Initialize material Date picker    
	$('#event_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: false,
		format : 'YYYY-MM-DD'
	});
	
	$(document).ready(function() {
		$("#add_event_form").validate({
			// post form to action url
		});
	});
	
	$('#add_more').click(function() {
		$(this).before($("<div/>", {
			class: 'form-box'
		}).fadeIn('slow').append($("<dev/>", {
			class: 'cell-img'
		}).append($("<img/>", {
			src: '<?php echo base_url('uploads/upload.png') ?>',
			class: 'image-group'
		}))));
	});
	
	$(".image-group").click(function() {
		var imageId = this.id;
		var imageIdSplit = imageId.split("_");
		var lastIndex = imageIdSplit.length-1;
		if(imageIdSplit[lastIndex]) {
			$("input[name='event_image_"+imageIdSplit[lastIndex]+"']").click();
		}
	});
	
	function read_url_image(input) {
		var imageId = input.id;
		if(imageId == 'event_image_1') {
			$('#banner_image').removeClass('item_image_cell_error');
			$('#event_image_val_1').val(input.value);
		}
		var temp;
		for (temp = 2;temp <= 7; temp++) {
			if(imageId == 'event_image_'+temp) {
				$('#banner_image_2').removeClass('item_image_cell_error');
				$('#event_image_val_'+temp).val(input.value);
			}
		}
		var imageIdSplit = imageId.split("_");
		var lastIndex = imageIdSplit.length-1;
		if(imageIdSplit[lastIndex]) {
			var pickerId	= '#image-picker_'+imageIdSplit[lastIndex];
			var fieldId		= '#event_image_'+imageIdSplit[lastIndex];
			var gallaryId	= '#event_gallery_id_'+imageIdSplit[lastIndex];
			var gallaryValue	= $('#event_gallery_id_'+imageIdSplit[lastIndex]).val();
			
		}
	
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader 		= new FileReader();
					reader.onload 	= function (e) {
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
					if(gallaryValue != '' && imageIdSplit[lastIndex] > 1) {
						var file_data = $(fieldId).prop("files")[0];
						var form_data = new FormData();
						form_data.append("file", file_data)
						form_data.append("gallaryId", gallaryValue) 
						$.ajax({
							type: 'POST',               
							processData: false, // important
							contentType: false, // important
							data: form_data,
							url: "<?php echo base_url() ?>backend/event/add_image",
							dataType : 'json',  
							success: function(jsonData){
								//alert(jsonData);
							}
						}); 
						$("#remove_btn_"+imageIdSplit[lastIndex]).show();
					}
				} else {
					$(fieldId).val("");
					alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}

	$( "#add_event_form" ).submit(function( event ) {
		var event_image_val_1 = $('#event_image_val_1').val();
		if( event_image_val_1 == '') {
			bootbox.alert("Please select banner image.!!");
			$('#banner_image').addClass('item_image_cell_error');
			return false;
		}
		var req = 0;
		for (temp = 2;temp <= 7; temp++) {
			var event_image_val = $('#event_image_val_'+temp).val();
			if(event_image_val == '') {
				req++;
			}			
		}
		if (req == 6) {
			bootbox.alert("Please select atleast one image.!!");
			$('#banner_image_2').addClass('item_image_cell_error');
			return false;
		}
		//checkBeacon()
		return;
		event.preventDefault();
	});
	
	/* 
	| -----------------------------------------------------
	| Function used to remove image
	| -----------------------------------------------------
	*/
	function remove_image(gallaryId,image_type) {
		bootbox.confirm("Are you sure want to delete this image?", function(result) {
			if(result==true) {
				// unset image data
				$.ajax ({
					type: "POST",
					data : {gallaryId:gallaryId},
					url: "<?php echo base_url() ?>backend/event/remove_image",
					async: false,
					success: function(data){ 
						if(data) {
							$('#event_image_val_'+image_type).val("");
							var file = $('#image-picker_'+image_type).attr('src','<?php echo base_url('uploads/upload.png');?>');
							$("#remove_btn_"+image_type).hide();
						}
					}, 
					error: function(error){
						bootbox.alert(error);
					}
				});
			}
		});
	}

	function checkBeacon() {
		var start_date 	=  $( "#event_date" ).val();
		var split_date 	=	start_date.split("-");
		var new_date_time 	= split_date[2]+'/'+split_date[1]+'/'+split_date[0];

		/*var end_date 	=  $( "#end_date" ).val();
		var split_end_date_time = 	end_date.split(" ");
		var split_end_date 		=	split_end_date_time[0].split("-");
		var new_end_date_time 	= split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0]+" "+split_end_date_time[1];*/

		var sDate 		=  new Date(new_date_time);
		//var eDate 		=  new Date(new_end_date_time);

		var nowTemp 	= new Date('<?php echo date('Y/m/d'); ?>');
		var today 		= new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
		if(start_date != '' && today > sDate) { 
			bootbox.alert("Event date should be greater than current date");
			$( "#event_date" ).val('');
		} /*else if(start_date != '' && end_date != '' && sDate > eDate) {
			bootbox.alert("<?php echo lang('start_date_is_greater');?>");
			$( "#end_date" ).val('');
		} */
	}
</script>
