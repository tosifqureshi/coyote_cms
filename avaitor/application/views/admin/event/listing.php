<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('events') ?></h4>
				<a href="<?php echo site_url("backend/event/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_event'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="pages_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('event_sno'); ?></th>
								<th><?php echo lang('event_image'); ?></th>
								<th><?php echo lang('event_name'); ?></th>
								<th><?php echo lang('event_date'); ?></th>
								<th><?php echo lang('event_status'); ?></th>
								<th><?php echo lang('action'); ?></th>
							</tr>	
						</thead>
                    
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($events) && is_array($events) && count($events)>0):
							foreach($events as $event) : 
								// set event id
								$event_id = encode($event['event_id']);?>
								
								<tr>
								<td><?php echo $i;?></td>
								<td>
									<?php if(isset($event['is_default_image']) && !empty($event['is_default_image'])) {
										$event_image = $event['event_image_'.$event['is_default_image']] ;
										$event_image = (!empty($event_image)) ? $event_image : '';
										$event_image = base_url('uploads/event_images/'.$event_image); 							
										?>
										<img  id="imageresource" src="<?php echo $event_image;?>" width="110" height="90" event_name="<?php echo $event['event_name'];?>">
									<?php } ?>
								</td>
								<td class="mytooltip tooltip-effect-1 desc-tooltip">
									<span class="tooltip-content clearfix">
										<span class="tooltip-text">
											<?php 
											echo '<b>'.lang('event_name').' : </b>'.$event['event_name'] ;
											echo '<br>';
											echo '<b>'.lang('event_at').' : </b>'.date('F j, Y, g:i a',strtotime($event['event_date'])) ;
											echo '<br>';
											?>
										</span> 
									</span>
									<span class="text-ttip">
										<a href="<?php echo site_url("backend/event/create"); echo '/' . ($event_id);?>"><?php if(strlen($event['event_name']) > 20) { echo substr($event['event_name'],0,20).'...'; } else { echo $event['event_name']; }?></a>
									</span>
								</td>
						
								<td>
									<?php 
									/* for complete date */
									$event_date = date('Y-m-d',strtotime($event['event_date']))." 23:59:59";
									// get first segment of dates 
									if(strtotime($event_date) < strtotime(date('Y-m-d H:i:s'))) {
										?>
										<span class="label label-success"><?php echo lang('closed');?></span>
										<?php
									} else {
										?>
										<span class="label label-info"><?php echo date('F j, Y, g:i a',strtotime($event['event_date']));?></span>
										<?php
										
									}
									?>
								</td>
								<td><?php 
									if($event['status'] == '0') {
										?>
										<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo encode($event['event_id']); ?>','<?php echo encode($event['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
										<?php
									} else {
										?>
										<a class="common-btn active-btn" onclick="changeStatus('<?php echo encode($event['event_id']); ?>','<?php echo encode($event['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
									}
								?></td>
								<td>
									<a class="common-btn delete-btn" href="javascript:;" onclick="myFunction('<?php echo encode($event['event_id']); ?>')"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							<?php 
							$i++;
							endforeach ; endif; ?>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});
	
	function myFunction(id) {
		bootbox.confirm("Are you sure want to delete the event?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/event/event_delete/"); ?>/"+id;
			}
		});
	}

	function changeStatus(id,status) {
		bootbox.confirm("Are you sure want to change the event status?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/event/event_status/"); ?>/"+id+"/"+status;
			}
		});
	}
	
	function copyevent(id){
		bootbox.confirm("Are you sure want to copy the event?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/event/copy_event/"); ?>/"+id;
			}
		});
	}
	
	/**
	 *  display event image in popup
	 */
	$(document).delegate( "#imageresource", "click", function(e) {
		$('#myModalLabel').html($(this).attr('event_name'));
		$('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
		$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
	});
</script>

