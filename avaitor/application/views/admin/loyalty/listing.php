<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('loyalty') ?></h4>
				<a href="<?php echo site_url("backend/loyalty/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_loyalty'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="pages_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('loyalty_sno'); ?></th>
								<th><?php echo lang('loyalty_list_image'); ?></th>
								<th><?php echo lang('loyalty_name'); ?></th>
								<th><?php echo lang('loyalty_day_diffrence'); ?></th>
								<th><?php echo lang('loyalty_status'); ?></th>
								<th><?php echo lang('action'); ?></th>
							</tr>
							
						</thead>
                    
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php
							$i=1;
							if(isset($loyalty_data) && is_array($loyalty_data) && count($loyalty_data)>0):
							foreach($loyalty_data as $loyalty) : 
								// set loyalty id
								$loyalty_id = encode($loyalty['loyalty_id']);?>
								<tr  data-position="<?php echo $i; ?>" class="even_gradeC" id="<?php echo $loyalty_id;?>">
								<td><?php echo $i;?></td>
								<td>
									<?php if(isset($loyalty['default_image_type']) && !empty($loyalty['default_image_type'])) {
										$loyalty_image = $loyalty['loyalty_image_'.$loyalty['default_image_type']] ;
										$loyalty_image = (!empty($loyalty_image)) ? $loyalty_image : '';
										$loyalty_image = base_url('uploads/loyalty_images/'.$loyalty_image); 							
										?>
										<img  id="imageresource" src="<?php echo $loyalty_image;?>" width="110" height="90" loyalty_name="<?php echo $loyalty['loyalty_name'];?>">
									<?php } ?>
								</td>
								<td class="mytooltip tooltip-effect-1 desc-tooltip">
									<span class="tooltip-content clearfix">
										<span class="tooltip-text">
											<?php 
											$start_date_time = date('Y-m-d H:i:s',strtotime($loyalty['loyalty_start_date']));
											$end_date_time = date('Y-m-d H:i:s',strtotime($loyalty['loyalty_end_date']));
											$diff =  get_date_diffrence($start_date_time, $end_date_time);
									
											echo '<b>'.lang('loyalty_name').' : </b>'.$loyalty['loyalty_name'] ;
											echo '<br>';
											echo '<b>'.lang('start_at').' : </b>'.date('F j, Y, g:i a',strtotime($loyalty['loyalty_start_date'])) ;
											echo '<br>';
											echo '<b>'.lang('end_at').'  : </b>'.date('F j, Y, g:i a',strtotime($loyalty['loyalty_end_date'])) ;
											echo '<br>';
											echo $diff;
											?>
										</span> 
									</span>
									<span class="text-ttip">
										<a href="<?php echo site_url("backend/loyalty/create"); echo '/' . ($loyalty_id);?>"><?php if(strlen($loyalty['loyalty_name']) > 20) { echo substr($loyalty['loyalty_name'],0,20).'...'; } else { echo $loyalty['loyalty_name']; }?></a>
									</span>
								</td>
						
								<td>
									<?php 
									// get first segment of dates 
									$avail_diff =  get_date_diffrence($start_date_time, $end_date_time,6,1);
									if($loyalty['status'] == '0' || $avail_diff == 'Closed') { ?>
										<span class="label label-error label-success"><?php echo lang('closed');?></span>
									<?php
									} else { ?>
										<span class="label label-success label-info"><?php echo $avail_diff ;?></span>
										<?php
									}
									?>
								</td>
								<td>
									<?php 
									if($loyalty['status'] == '0') { ?>
										<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo encode($loyalty['loyalty_id']); ?>','<?php echo encode($loyalty['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
										<?php
									} else { ?>
										<a class="common-btn active-btn" onclick="changeStatus('<?php echo encode($loyalty['loyalty_id']); ?>','<?php echo encode($loyalty['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
										<?php
									}
									?>
								</td>
									<td>
										<a class="common-btn delete-btn" href="javascript:;" onclick="myFunction('<?php echo encode($loyalty['loyalty_id']); ?>')"><i class="fa fa-trash"></i></a>
										<!--<a class="common-btn yellow-btn" href="javascript:;" onclick="copyOffer('<?php echo encode($loyalty['loyalty_id']); ?>')"><i class="fa fa-files-o"></i></a>-->
									</td>
								</tr>
							<?php 
							$i++;
							endforeach ; endif; ?>
						</tbody>
						
					</table>
				</div>		
				
			</div>
		</div>
	</div>
</div>				

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});
	
	function myFunction(id) {
		bootbox.confirm("Are you sure want to delete the Loyalty?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/loyalty/loyalty_delete/"); ?>/"+id;
			}
		});
	}

	function changeStatus(id,status) {
		bootbox.confirm("Are you sure want to change the Loyalty status?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/loyalty/loyalty_status/"); ?>/"+id+"/"+status;
			}
		});
	}
	
	function copyOffer(id){
		bootbox.confirm("Are you sure want to copy the Loyalty?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/loyalty/copy_loyalty/"); ?>/"+id;
			}
		});
	}
	
	/**
	 *  display offer image in popup
	 */
	$(document).delegate( "#imageresource", "click", function(e) {
		$('#myModalLabel').html($(this).attr('loyalty_name'));
		$('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
		$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
	});
	
	
	
	/**
	 *  Manage reordering of loyalty programs
	 */
	var postUpdateOrderUrl =  BASEURL+"backend/loyalty/update_order";
	$(document).ready(function () {
		$('#pages_table').dataTable().rowReordering({ 
									sURL:postUpdateOrderUrl, 
									sRequestType: "POST", 
									fnAlert: function(text){
													window.location.href = window.location.href;
									 }});
		$('.no-footer').children('.dataTables_length').hide();
		$('.no-footer').children('.dataTables_filter').hide();
		$('.no-footer').children('.dataTables_paginate').hide();
		$('.no-footer').children('.dataTables_info').hide();
	});
		    
</script>

