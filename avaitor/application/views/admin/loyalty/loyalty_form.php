<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">          
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $loyalty_title;?></h4>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs customtab2" role="tablist">
					<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#content_tab" role="tab"><span class=""><?php echo lang('content');?></span></a> </li>
					<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#attribute_tab" role="tab"> <span class=""><?php echo lang('attributes');?></span></a> </li>
				</ul>
				<!-- Tab panes -->
				<?php echo form_open_multipart('backend/loyalty/create/'.$loyalty_id,'id="add_loyalty_form", class="  form-with-label"'); ?>
					<div class="tab-content">
						<div class="tab-pane active" id="content_tab" role="tabpanel">
							<div class="p-t-20">
								<div class="row">
									<div class="col-md-6">
										<!-- Loyalty name start here -->
										<div class="form-group form-material">
											<label class=""><?php echo lang('loyalty_name');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'loyalty_name', 'value'=>set_value('loyalty_name',$loyalty_name), 'class'=>'form-control form-control-line  required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_promo_name'));
											echo form_input($data);?>
										</div>
										<!-- Loyalty name end here -->
										
										<!-- Loyalty price start here -->
										<div class="form-group form-material">
											<label class=""><?php echo lang('loyalty_price');?><i style="color:red;">*</i></label>
											<?php
											$data = array('name'=>'loyalty_price' ,'id'=>'loyalty_price', 'value'=>set_value('loyalty_price',$loyalty_price), 'class'=>'form-control form-control-line numeric required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_promo_price'));
											echo form_input($data);?>
										</div>
										<!-- Loyalty price end here -->
										
										<!-- Loyalty barcode start here -->
										<div class="row form-group form-material">
											<div class="col-md-6">
												<label class=""><?php echo lang('loyalty_barcode');?><i style="color:red;">*</i></label>
												<?php
												$data = array('name'=>'barcode' ,'id'=>'barcode', 'value'=>set_value('barcode',$barcode), 'class'=>'form-control form-control-line numeric required','maxlength'=>12, 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_barcode'));
												echo form_input($data);?>
											</div>
											<div class="col-md-6 form-material">
												<!--- QR code upload field starts here --> 
												<?php if(isset($barcode_image) && !empty($barcode_image)) { ?>
													<div class="img-box">
														<label><?php echo lang('qrcode_image');?></label> 
														<div class="bar-img" >
															<img id="qr-image-picker" style="width: 100%;" src="<?php echo base_url('uploads/barcodes/'.$barcode_image) ?>" data-toggle='tooltip', data-placement='right', title='<?php echo lang('tooltip_barcode_image'); ?>' />
														</div>
													</div>	
												<?php } ?>
												<!--- QR code upload field end here --> 
											</div>
										</div>
										<!-- Loyalty barcode end here -->
										
										<!-- Loyalty start / end date fields start here -->
										<div class="row form-group form-material">
											 <div class="col-md-6">
												<label class="m-t-20"><?php echo lang('start_time');?><i style="color:red;">*</i></label>
												<input type="text" name="loyalty_start_date" class="form-control required" value="<?php echo !empty($loyalty_start_date) ? $loyalty_start_date : '';?>" id="loyalty_start_date">
											</div>	
										
											<div class="col-md-6 form-material">
												<label class="m-t-20"><?php echo lang('end_time');?><i style="color:red;">*</i></label>
												<input type="text" name="loyalty_end_date" class="form-control required"  value="<?php echo !empty($loyalty_end_date) ? $loyalty_end_date : '';?>" id="loyalty_end_date">
											</div>
										</div>
										<!-- Loyalty start / end date fields end here -->
										
										<!-- Loyalty short description start here -->
										<div class="form-group form-material">
											<label class=""><?php echo lang('loyalty_short_description');?></label>
											<?php
											$data	= array('name'=>'loyalty_short_desc', 'value'=>$loyalty_short_desc,'class'=> 'form-control form-control-line','rows'=>3,'maxlength'=>80);
											echo form_textarea($data);
											?>
										</div>
										<!-- Loyalty short description end here -->	
										
										<!-- Loyalty long description start here -->
										<div class="form-group html-editor">
											<label><?php echo lang('loyalty_long_description');?></label>
											<?php
											$data = array('name'=>'loyalty_long_desc', 'class'=>'long_desmceEditor redactor span8', 'value'=>$loyalty_long_desc);
											echo form_textarea($data);?>
										</div>
										<!-- Loyalty long description end here -->
									</div>	
									<div class="col-md-6">
										<div class="form-group ">
											<div class="row">
												<div class="col-md12">
													<label><?php echo lang('loyalty_banner_image');?></label>
													<div class="img-box">
														<?php
														$loyalty_image_val_1 = (isset($loyalty_image_1) && !empty($loyalty_image_1)) ? $loyalty_image_1 :'';
														$img_1_display = (isset($loyalty_image_1) && !empty($loyalty_image_1)) ? '' :'dn';
														if(isset($loyalty_image_1) && $loyalty_image_1 != '') {
														$loyalty_image_1 = base_url('uploads/loyalty_images/'.$loyalty_image_1);
														$title = lang('tooltip_offer_image');
															$loyalty_image_1_doc = $this->config->item('document_path').'uploads/loyalty_images/'.$loyalty_image_val_1; 
															if(!file_exists($loyalty_image_1_doc)){
																$loyalty_image_1 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															}
														} else {
															$title = lang('tooltip_promo_image');
															$loyalty_image_1 = base_url('uploads/upload.png');
														} ?>
														<a id="remove_btn_1" class="<?php echo $img_1_display;?>"  href="javascript:void(0);" onclick="remove_image(1)">×</a>
														<div class="cell-img" id="banner_image" >
															<img id="image-picker_1" src="<?php echo $loyalty_image_1; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
														</div>
														<?php
														$data = array('name'=>'loyalty_image_1', 'id'=>'loyalty_image_1', 'value'=>set_value('loyalty_image_1',$loyalty_image_val_1),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_web_banner_url_image(this)');
														echo form_upload($data);?>
													</div>
												</div>

												<div class="col-md-12">
													<label for="title" class="img-title"><?php echo lang('app_loyalty_banner_image');?></label> 	
													<div class="img-box">
													<?php
													$loyalty_image_val_3 = (isset($loyalty_image_3) && !empty($loyalty_image_3)) ? $loyalty_image_3 :'';
													$img_3_display = (isset($loyalty_image_3) && !empty($loyalty_image_3)) ? '' :'dn';
													if(isset($loyalty_image_3) && $loyalty_image_3 != '') {
													$loyalty_image_3 = base_url('uploads/loyalty_images/'.$loyalty_image_3);
													$title = lang('tooltip_offer_image');
													$loyalty_image_3_doc = $this->config->item('document_path').'uploads/loyalty_images/'.$loyalty_image_val_3; 
													if(!file_exists($loyalty_image_3_doc)){
													$loyalty_image_3 = base_url('uploads/No_Image_Available.png');
													$title = "No Image Available";
													}
													} else {
													$title = lang('tooltip_promo_image');
													$loyalty_image_3 = base_url('uploads/upload.png');
													} ?>
													<a id="remove_btn_3" class="<?php echo $img_3_display;?>"  href="javascript:void(0);" onclick="remove_image(3)">×</a>
													<div class="cell-img" id="app_banner_image" >
													<img id="image-picker_3" src="<?php echo $loyalty_image_3; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
													</div>
													<?php
													$data	= array('name'=>'loyalty_image_3', 'id'=>'loyalty_image_3', 'value'=>set_value('loyalty_image_3',$loyalty_image_val_3),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_app_banner_url_image(this)');
													echo form_upload($data);?>
													<label for="offer_default_image"><?php echo lang('offer_default_image');?></label>
													</div>
												</div>

												<div class="col-md-12">
													<label><?php echo lang('loyalty_bkgd_image');?></label> 
													<div class="img-box">
														<?php
														$loyalty_image_val_2 = (isset($loyalty_image_2) && !empty($loyalty_image_2)) ? $loyalty_image_2 :'';
														$img_2_display = (isset($loyalty_image_2) && !empty($loyalty_image_2)) ? '' :'dn';
														if(isset($loyalty_image_2) && $loyalty_image_2 != '') {
														$loyalty_image_2 = base_url('uploads/loyalty_images/'.$loyalty_image_val_2);
														$title = lang('tooltip_offer_image');
															$loyalty_image_2_doc = $this->config->item('document_path').'uploads/loyalty_images/'.$loyalty_image_val_2; 
															if(!file_exists($loyalty_image_2_doc)){
																$loyalty_image_2 = base_url('uploads/No_Image_Available.png');
																$title = "No Image Available";
															}
														} else {
															$title = lang('tooltip_promo_image');
															$loyalty_image_2 = base_url('uploads/upload.png');
														} ?>
														<a id="remove_btn_2" class="<?php echo $img_2_display;?>"  href="javascript:void(0);" onclick="remove_image(2)">×</a>
														<div class="cell-img" id="bkgd_image" >
															<img id="image-picker_2" src="<?php echo $loyalty_image_2; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
														</div>
														<?php
														$data	= array('name'=>'loyalty_image_2', 'id'=>'loyalty_image_2', 'value'=>set_value('loyalty_image_2',$loyalty_image_val_2),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
														echo form_upload($data);?>
													</div>
												</div>		
												<div class="alert red-alert alert-danger alert-dismissible ">
													<i class="icon fa fa-warning"></i>
													Upload up to 3MB images. 
												</div>
											</div>	
										</div>	
									</div>	
								</div>	
							</div>	
						</div>
						
						<div class="tab-pane p-20" id="attribute_tab" role="tabpanel">
							<!-- Loyalty multi store field start here-->
							<div class="form-group row">
								<label class="control-label col-md-12">
									<?php echo lang('loyalty_store');?> <span class="red">*</span>
								</label>
								<div class="demo-checkbox col-md-12">
									<input type="checkbox" id="select_all" class="filled-in chk-col-light-blue" />
									<label for="select_all"><?php echo lang('select_all');?></label>
								</div>
								<div class="col-md-4">
									<?php
									$store_id_array = (!empty($store_id)) ? explode(',',$store_id) : '';
									$data	= array('name'=>'store_id[]','multiple'=>'multiple','id'=>'loyalty_store_id', 'class'=>'required form-control', 'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_store'));
									echo form_dropdown($data,$stores,set_value('store_id',$store_id_array));?>
								</div>	 
							</div> 
							<!-- Loyalty multi store field end here-->
							<div class="row form-material" >
								<!-- Loyalty trigger product ids field start here-->
								<div class="form-group col-md-3  ">
									<label class=""><?php echo lang('loyalty_trigger_products');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'trigger_product_ids', 'value'=>set_value('trigger_product_ids', $trigger_product_ids),'id'=>'trigger_product_ids','class'=>'required form-control form-control-line', 'title'=>lang('tooltip_trigger_products'));
									echo form_input($data);?>
								</div>
								<!-- Loyalty trigger product ids field end here-->
								<!-- Loyalty reward product ids field start here-->
								<div class="form-group col-md-3  ">
									<label class=""><?php echo lang('loyalty_reward_products');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'reward_product_ids', 'value'=>set_value('reward_product_ids', $reward_product_ids),'id'=>'reward_product_ids','class'=>'required form-control form-control-line', 'title'=>lang('tooltip_reward_products'));
									echo form_input($data);?>
								</div>
								<!-- Loyalty reward product ids field end here-->
							</div>
							<!-- Loyalty quantity fields start here -->
							<div class="row form-material">
								<div class="form-group col-md-3">
									<label class=""><?php echo lang('paid_qty');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'paid_qty', 'value'=>set_value('paid_qty', $paid_qty),'id'=>'paid_qty','class'=>'paid_qty required numeric form-control form-control-line','maxlength'=>3, 'title'=>lang('tooltip_loyalty_paid_qty'));
									echo form_input($data);?>
									<div id="paid_qty_err" class="error qty_error"></div>
								</div>
								<div class="form-group col-md-3">
									 <label class=""><?php echo lang('free_qty');?><i style="color:red;">*</i></label>
									 <?php
									$data = array('name'=>'free_qty', 'value'=>set_value('free_qty', $free_qty),'id'=>'free_qty','class'=>'free_qty required numeric form-control form-control-line', 'maxlength'=>3, 'title'=>lang('tooltip_loyalty_free_qty'));
									echo form_input($data);?>
									<div id="free_qty_err" class="error qty_error"></div>
								</div>
								<div class="form-group col-md-3">
									<label class=""><?php echo lang('discount');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'discount', 'value'=>set_value('discount', $discount),'id'=>'discount','class'=>'discount required numeric form-control form-control-line', 'maxlength'=>5, 'title'=>lang('tooltip_loyalty_discount'));
									echo form_input($data);?>
									<div id="discount_err" class="error qty_error"></div>
								</div>
							</div>	
							<!-- Loyalty quantity fields end here -->
							<!-- Item's image upload area start here -->
							<div class="row form-material">
								<!-- Used image container start here -->
								<div class="col-md-3">
									<label><?php echo lang('used_item_image');?><i style="color:red;">*</i></label>
									<div class="img-box">
										<?php
										$used_image_val = (isset($used_image) && !empty($used_image)) ? $used_image :'';
										if(!empty($used_image_val)) {
											$used_image = base_url('uploads/loyalty_images/'.$used_image);
											$title = lang('tooltip_used_image');
											$used_image_doc = $this->config->item('document_path').'uploads/loyalty_images/'.$used_image_val; 
											if(!file_exists($used_image_doc)){
												$used_image = base_url('uploads/No_Image_Available.png');
												$title = "No Image Available";
											}
										} else {
											$title = lang('tooltip_used_image');
											$used_image = base_url('uploads/upload.png');
										} ?>
										<div class="cell-img" id="used_image_cell">
											<img id="used-image-picker" src="<?php echo $used_image; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
										</div>
										<?php
										$data	= array('name'=>'used_image', 'id'=>'used_image', 'value'=>set_value('used_image',$used_image_val),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_used_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
										echo form_upload($data);?>
									</div>
								</div>
								<!-- Used image container end here -->
								<!-- Not Used image container start here -->
								<div class="col-md-3">
									<label><?php echo lang('not_used_item_image');?><i style="color:red;">*</i></label>
									<div class="img-box">
										<?php
										$not_used_image_val = (isset($not_used_image) && !empty($not_used_image)) ? $not_used_image :'';
										if(!empty($not_used_image_val)) {
											$not_used_image = base_url('uploads/loyalty_images/'.$not_used_image);
											$title = lang('tooltip_not_used_image');
											$not_used_image_doc = $this->config->item('document_path').'uploads/loyalty_images/'.$not_used_image_val; 
											if(!file_exists($not_used_image_doc)){
												$not_used_image = base_url('uploads/No_Image_Available.png');
												$title = "No Image Available";
											}
										} else {
											$title = lang('tooltip_not_used_image');
											$not_used_image = base_url('uploads/upload.png');
										} ?>
										<div class="cell-img" id="not_used_image_cell">
											<img id="not-used-image-picker" src="<?php echo $not_used_image; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
										</div>
										<?php
										$data	= array('name'=>'not_used_image', 'id'=>'not_used_image', 'value'=>set_value('not_used_image',$not_used_image_val),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_not_used_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
										echo form_upload($data);?>
									</div>
								</div>	
								<!-- Not Used image container end here -->
								<!-- Free image container start here -->
								<div class="col-md-3 ">
									<label><?php echo lang('free_item_image');?><i style="color:red;">*</i></label>
									<div class="img-box">
										<?php
										$free_image_val = (isset($free_image) && !empty($free_image)) ? $free_image :'';
										if(!empty($free_image_val)) {
											$free_image = base_url('uploads/loyalty_images/'.$free_image);
											$title = lang('tooltip_free_image');
											$free_image_doc = $this->config->item('document_path').'uploads/loyalty_images/'.$free_image_val; 
											if(!file_exists($free_image_doc)){
												$free_image = base_url('uploads/No_Image_Available.png');
												$title = "No Image Available";
											}
										} else {
											$title = lang('tooltip_not_used_image');
											$free_image = base_url('uploads/upload.png');
										} ?>
										<div class="cell-img" id="free_image_cell">
											<img id="free-image-picker" src="<?php echo $free_image; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
										</div>
										<?php
										$data	= array('name'=>'free_image', 'id'=>'free_image', 'value'=>set_value('free_image',$free_image_val),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_free_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
										echo form_upload($data);?>
									</div>
								</div>	 
								<!-- Free image container end here -->
							</div>	
							<!-- Item's image upload area end here -->		
						</div>	
												
						<div class="row pb-3 float-right">
							<input type="hidden" value="<?php echo set_value('loyalty_id', $loyalty_id); ?>" name='loyalty_id' id='loyalty_id'>
							<input type="hidden" value="<?php echo $used_image_val;?>" name='used_image_val' id='used_image_val'>
							<input type="hidden" value="<?php echo $not_used_image_val;?>" name='not_used_image_val' id='not_used_image_val'>
							<input type="hidden" value="<?php echo $free_image_val;?>" name='free_image_val' id='free_image_val'>
							<input type="hidden" value="<?php echo $loyalty_image_val_1;?>" name='loyalty_image_val_1' id='loyalty_image_val_1'>
							<input type="hidden" value="<?php echo $loyalty_image_val_2;?>" name='loyalty_image_val_2' id='loyalty_image_val_2'>
							<?php
							// set current date in hidden field 
							$data = array('name'=>'current_date','type'=>'hidden','value'=>date('d-m-Y H:i:s'),'id'=>'current_date');
							echo form_input($data);?>
							<div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'backend/loyalty' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					</div>
				<?php echo form_close();?>		
			</div>	
		</div>	
	</div>
</div>	


<script type="text/javascript">
	// Initialize material Date picker    
	$('#loyalty_start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});

	$('#loyalty_end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
	$(document).ready(function() {
		$("#add_loyalty_form").validate({
			// post form to action url
		});
	});
	
	/**
	* Manage select all stores
	*/
	$('#select_all').click(function() {
		if($("#select_all").is(':checked')) {
			$('#loyalty_store_id option').prop('selected', true);
		} else {
			$('#loyalty_store_id option').prop('selected', false);
		}
		
	});
	   /**
	* unselect check all button status 
	*/
	$('#loyalty_store_id').change(function() {
		$("#select_all").attr('checked', false);
	})
	
	/**
	* Manage select all stores
	*/
	$('#select_all_lps').click(function() {
		if($("#select_all_lps").is(':checked')) {
			$('#loyalty_product_size option').prop('selected', true);
		} else {
			$('#loyalty_product_size option').prop('selected', false);
		}
		
	});
	   /**
	* unselect check all button status 
	*/
	$('#loyalty_product_size').change(function() {
		$("#select_all_lps").attr('checked', false);
	})

	$( "#add_loyalty_form" ).submit(function( event ) {
		var used_image = $('#used_image_val').val(); 
		var not_used_image = $('#not_used_image_val').val(); 
		var free_image = $('#free_image_val').val();
		var loyalty_image_val_1 = $('#loyalty_image_val_1').val();
		var loyalty_image_val_2 = $('#loyalty_image_val_2').val();
		$('#paid_qty_err').text('');
		$('#free_qty_err').text('');
		if( used_image == '' ) {
			bootbox.alert("Please select used image.!!");
			$('#used_image_cell').addClass('item_image_cell_error');
			return false;
		}
		if( not_used_image == '' ) {
			bootbox.alert("Please select not used image.!!");
			$('#not_used_image_cell').addClass('item_image_cell_error');
			return false;
		}
		if( free_image == '') {
			bootbox.alert("Please select free image.!!");
			$('#free_image_cell').addClass('item_image_cell_error');
			return false;
		}
		if( loyalty_image_val_1 == '') {
			bootbox.alert("Please select banner image.!!");
			$('#banner_image').addClass('item_image_cell_error');
			return false;
		}if( loyalty_image_val_2 == '') {
			bootbox.alert("Please select background image.!!");
			$('#bkgd_image').addClass('item_image_cell_error');
			return false;
		}
	  
	});
	
	

	function checkCheckBox(checkbox){
		//alert(checkbox);
		for(var i=1;i<=3;i++){
			if(checkbox != 'check'+i) {
				$('#check'+i).attr('checked', false);
			}
		}
	}
	
	$("#image-picker_1").click(function() {
		$('#banner_image').removeClass('item_image_cell_error');
		$("input[name='loyalty_image_1']").click();
	});
	$("#image-picker_2").click(function() {
		$('#bkgd_image').removeClass('item_image_cell_error');
		$("input[name='loyalty_image_2']").click();
	});
	$("#image-picker_3").click(function() {
		$('#app_banner_image').removeClass('item_image_cell_error');
		$("input[name='loyalty_image_3']").click();
	});
	$("#used-image-picker").click(function() {
		$('#used_image_cell').removeClass('item_image_cell_error');
		$("input[name='used_image']").click();
	});
	$("#not-used-image-picker").click(function() {
		$('#not_used_image_cell').removeClass('item_image_cell_error');
		$("input[name='not_used_image']").click();
	});
	$("#free-image-picker").click(function() {
		$('#free_image_cell').removeClass('item_image_cell_error');
		$("input[name='free_image']").click();
	});
	
	
	
	function read_url_image(input) {

		if(input.id == 'loyalty_image_2') {
			var pickerId	= '#image-picker_2';
			var fieldId	    = '#loyalty_image_2';
			$('#loyalty_image_val_2').val(input.value);
			$("#remove_btn_2").show();
		}else if(input.id == 'used_image') {
			var pickerId	= '#used-image-picker';
			var fieldId	    = '#used_image';
			$('#used_image_val').val(input.value);
		} else if(input.id == 'not_used_image') {
			var pickerId	= '#not-used-image-picker';
			var fieldId 	= '#not_used_image';
			$('#not_used_image_val').val(input.value);
		} else if(input.id == 'free_image') {
			var pickerId    = '#free-image-picker';
			var fieldId	    = '#free_image';
			$('#free_image_val').val(input.value);
		}
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	


	function read_web_banner_url_image(input) {
		if(input.id == 'loyalty_image_1') {
			var pickerId	= '#image-picker_1';
			var fieldId	= '#loyalty_image_1';
			$('#loyalty_image_val_1').val(input.value);
			$("#remove_btn_1").show();
		}
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//Initiate the JavaScript Image object.
                        var image = new Image();
         
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
						image.onload = function () {
                            var height = parseInt(this.height);
                            var width = parseInt(this.width);

                            if (height != 90 || width != 600) {
                                //$(fieldId).val("");
                                bootbox.alert("Image should be 600 X 90");
                              // $(pickerId).attr('src','<?php echo base_url('uploads/upload.png');?>');
                              // $("#remove_btn_1").hide();
                                return false;
                            }
                        };
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	function read_app_banner_url_image(input) {
		if(input.id == 'loyalty_image_3') {
			var pickerId	= '#image-picker_3';
			var fieldId	= '#loyalty_image_3';
			$('#loyalty_image_val_3').val(input.value);
			$("#remove_btn_3").show();
		}
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//Initiate the JavaScript Image object.
                        var image = new Image();
         
                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;
						image.onload = function () {
                            var height = parseInt(this.height);
                            var width = parseInt(this.width);
                            var r = gcd (width, height);
                            var w = width/r;
                            var h = height/r;
                            //alert(w+':'+h);
                            if (h != 1 || w != 6) {
                               // $(fieldId).val("");
                                bootbox.alert("Image aspect be 1:6");
                              //  $(pickerId).attr('src','<?php echo base_url('uploads/upload.png');?>');
                              //  $("#remove_btn_3").hide();
                                return false;
                            }
                        };
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}

	 function gcd (a, b) {
        return (b == 0) ? a : gcd (b, a%b);
    }

	
	$("#loyalty_price").on("keyup", function(){
			var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
			val = this.value;
		
		if(!valid){
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Function used to remove image
	| -----------------------------------------------------
	*/
	function remove_image(image_type) {
		bootbox.confirm("Are you sure want to delete this image?", function(result) {
			if(result==true) {
				// unset image data
				$("#image-picker_"+image_type).attr('src', '<?php echo base_url('uploads/upload.png');?>');
				var loyalty_id = $('#loyalty_id').val();
				$.ajax ({
					type: "POST",
					data : {loyalty_id:loyalty_id,image_type:image_type},
					url: "<?php echo base_url() ?>backend/loyalty/remove_image",
					async: false,
					success: function(data){ 
						if(data) {
							var file = $('#image-picker_'+image_type).attr('src','<?php echo base_url('uploads/upload.png');?>');
							$("#remove_btn_"+image_type).hide();
						}
					}, 
					error: function(error){
						bootbox.alert(error);
					}
				});
			}
		});
	}
	
	/* 
	| -----------------------------------------------------
	| Manage the free quantity validations 
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".free_qty","change",function(e){
		var paid_qty = $('#paid_qty').val();
		var free_qty = $('#free_qty').val();
		if (paid_qty <= free_qty && free_qty!='') {
			$('#free_qty').val('');
			$('#free_qty').addClass('error');
			$('#free_qty_err').text('Must be less than paid quantity');
		} else {
			$('#free_qty').removeClass('error');
			$('#free_qty_err').text('');
			$('#paid_qty').removeClass('error');
			$('#paid_qty_err').text('');
		}
	});
	
	/* 
	| -----------------------------------------------------
	| Manage the paid quantity validations 
	| -----------------------------------------------------
	*/ 
	$(document).delegate(".paid_qty","change",function(e){
		var paid_qty = $('#paid_qty').val();
		var free_qty = $('#free_qty').val();
		if(free_qty != '' && paid_qty <= free_qty && paid_qty!='') {
			$('#paid_qty').val('');
			$('#paid_qty').addClass('error');
			$('#paid_qty_err').text('Must be higher than free quantity');
		} else {
			$('#paid_qty').removeClass('error');
			$('#paid_qty_err').text('');
		}
	});
	
	// validation on entered discount
	$(document).delegate("#discount","change",function(e){
		var str = $('#discount').val();
		var x = parseFloat(str);
		if(x != ''){
		
			if (isNaN(x) || x < 0 || x > 100) {
				$('#discount').val('');
				$('#discount').addClass('error');
				$('#discount_err').text('Discount value is out of range');
			} else {
				$('#discount').val(x);
				$('#discount').removeClass('error');
				$('#discount_err').text('');
			}
		}
	});


	tinymce.init({
		mode: "specific_textareas",
		editor_selector: "long_desmceEditor",
		height: '230px',
		width: '585px',
		theme_advanced_resizing: true,
		plugins: ['link'],
		setup: function(editor) {
			editor.on('change', function(e) {
				var edm_description = tinyMCE.activeEditor.getContent( { format : 'html' } );
				if(edm_description != '' && edm_description != undefined) {
					$('.email_footer_text_tr').show();
					$('.email_footer_text').html(edm_description);
				}
				
			});
		}
	});


    </script>
