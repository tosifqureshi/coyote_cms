<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title float-left"><?php echo lang('offers') ?></h4>
				<a href="<?php echo site_url("backend/offer/create"); ?>">
					<button type="button" class="btn waves-effect waves-light btn-info float-right"><?php echo lang('add_new_offer'); ?></button>
				</a>
				<?php			
				if (isset($message) && !empty($message)){
					echo '<div class="alert alert-success">' . $message	 . '</div>';
				} ?>
				<!-- start the bootstrap modal where the image will appear -->
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<img src="" id="imagepreview" style="width: 400px; height: 264px;" >
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- end the bootstrap modal where the image will appear -->
				<div class="table-responsive m-t-40">
					<table id="pages_table" class="table table-bordered table-striped">
						<thead>
							<tr role="row">
								<th><?php echo lang('offer_sno'); ?></th>
								<th><?php echo lang('offer_list_image'); ?></th>
								<th><?php echo lang('offer_name'); ?></th>
								<th><?php echo lang('offer_day_diffrence'); ?></th>
								<th><?php echo lang('offer_status'); ?></th>
								<th><?php echo lang('action'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							if(isset($offers) && is_array($offers) && count($offers)>0):
							foreach($offers as $offer) : 
								// set offer id
								$offer_id = encode($offer['offer_id']);?>
								
								<tr>
									<td><?php echo $i;?></td>
									<td>
										<?php if(isset($offer['is_default_image']) && !empty($offer['is_default_image'])) {
											$offer_image = $offer['offer_image_'.$offer['is_default_image']] ;
											$offer_image = (!empty($offer_image)) ? $offer_image : '';
											$offer_image = base_url('uploads/offer_images/'.$offer_image); 							
											?>
											<img  id="imageresource" src="<?php echo $offer_image;?>" width="110" height="90" offer_name="<?php echo $offer['offer_name'];?>">
										<?php } ?>
									</td>
									<td class="mytooltip tooltip-effect-1 desc-tooltip">
										<span class="tooltip-content clearfix">
											<span class="tooltip-text">
												<?php 
												$start_date_time = date('Y-m-d H:i:s',strtotime($offer['start_time']));
												$end_date_time = date('Y-m-d H:i:s',strtotime($offer['end_time']));
												$diff =  get_date_diffrence($start_date_time, $end_date_time);
										
												echo '<b>'.lang('offer_name').' : </b>'.$offer['offer_name'] ;
												echo '<br>';
												echo '<b>'.lang('start_at').' : </b>'.date('F j, Y, g:i a',strtotime($offer['start_time'])) ;
												echo '<br>';
												echo '<b>'.lang('end_at').'  : </b>'.date('F j, Y, g:i a',strtotime($offer['end_time'])) ;
												echo '<br>';
												echo $diff;
												?>
											</span> 
										</span>
										<span class="text-ttip">
											<a href="<?php echo site_url("backend/offer/create"); echo '/' . ($offer_id);?>"><?php if(strlen($offer['offer_name']) > 20) { echo substr($offer['offer_name'],0,20).'...'; } else { echo $offer['offer_name']; }?></a>
										</span> 
									</td>
							
									<td>
										<?php 
										// get first segment of dates 
										$avail_diff =  get_date_diffrence($start_date_time, $end_date_time,6,1);
										if($offer['status'] == '0' || $avail_diff == 'Closed') { ?>
											<span class="label label-success"><?php echo lang('closed');?></span>
										<?php
										} else { ?>
											<span class="label label-info"><?php echo $avail_diff ;?></span>
											<?php
										}
										?>
										
									</td>
									<td><?php 
										if($offer['status'] == '0') {
											?>
											<a class="common-btn deactive-btn" onclick="changeStatus('<?php echo encode($offer['offer_id']); ?>','<?php echo encode($offer['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-off "></i></a>
											<?php
										} else {
											?>
											<a class="common-btn active-btn" onclick="changeStatus('<?php echo encode($offer['offer_id']); ?>','<?php echo encode($offer['status']); ?>')" href="javascript:;"><i class="fa fa-toggle-on "></i></a>
											<?php
										}
									?></td>
									<td>
										<a class="common-btn delete-btn" href="javascript:;" onclick="myFunction('<?php echo encode($offer['offer_id']); ?>')"><i class="fa fa-trash"></i></a>
										<a class="common-btn yellow-btn" href="javascript:;" onclick="copyOffer('<?php echo encode($offer['offer_id']); ?>')"><i class="fa fa-files-o"></i></a>
									</td>
								</tr>
								<?php 
								$i++;
							endforeach ; endif; ?>
						</tbody>	
					</table>	
				</div>
			</div>
		</div>
	</div>
</div>				

<script>
	$(document).ready(function() {
		$('#pages_table').DataTable();
	});
	
	function myFunction(id) {
		bootbox.confirm("Are you sure want to delete the offer?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/offer/offer_delete/"); ?>/"+id;
			}
		});
	}

	function changeStatus(id,status) {
		bootbox.confirm("Are you sure want to change the offer status?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/offer/offer_status/"); ?>/"+id+"/"+status;
			}
		});
	}
	
	function copyOffer(id){
		bootbox.confirm("Are you sure want to copy the offer?", function(result) {
			if(result==true) {
				$('#loader1').fadeIn();	
				location.href="<?php echo site_url("backend/offer/copy_offer/"); ?>/"+id;
			}
		});
	}
	
	/**
	 *  display offer image in popup
	 */
	$(document).delegate( "#imageresource", "click", function(e) {
		$('#myModalLabel').html($(this).attr('offer_name'));
		$('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
		$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
	});
</script>

