<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $promo_title;?></h4>
				<div class="p-t-20">
					<?php echo form_open_multipart('backend/promotion/create/'.$promo_id,'id="add_promotion_form" class="  form-with-label"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- Promotion name start here -->
								<div class="form-group form-material">
									<label class=""><?php echo lang('promo_name');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'promo_name', 'value'=>set_value('promo_name',$promo_name), 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_promo_name'));
									echo form_input($data);?>
								</div>
								<!-- Promotion name end here -->
								
								<!-- Promotion start / end date fields start here -->
								<div class="row form-group form-material">
									 <div class="col-md-6">
										<label class="m-t-20"><?php echo lang('start_time');?><i style="color:red;">*</i></label>
										<input type="text" name="promo_start_date" class="form-control required" value="<?php echo !empty($promo_start_date) ? $promo_start_date : '';?>" id="promo_start_date">
									</div>	
									
									 <div class="col-md-6">
										<label class="m-t-20"><?php echo lang('end_time');?><i style="color:red;">*</i></label>
										<input type="text" name="promo_end_date" class="form-control required"  value="<?php echo !empty($promo_end_date) ? $promo_end_date : '';?>" id="promo_end_date">
									</div>
								</div>
								<!-- Promotion start / end date fields end here -->
								
								<!-- Promotion short description start here -->
								<div class="form-group form-material">
									<label><?php echo lang('promo_short_description');?><i style="color:red;">*</i></label>
									<?php $data	= array('name'=>'promo_short_desc', 'value'=>$promo_short_desc, 'class'=>'required form-control form-control-line','rows'=>3);
									echo form_textarea($data);?>
								</div>
								<!-- Promotion short description end here -->
								
								<!-- Promotion long description start here -->
								<div class="form-group form-material">
									<label for="promo_long_desc"><?php echo lang('promo_long_description');?></label>
									<?php $data	= array('name'=>'promo_long_desc', 'class'=>'mceEditor redactor form-control', 'value'=>$promo_long_desc);
									echo form_textarea($data);?>	
								</div>	
								<!-- Promotion short description end here -->
							</div>	
							
							<!-- Prmotion banner image box start here-->	
							<div class="col-md-6">
								<div class="form-group ">
									<label><?php echo lang('offer_image');?></label>
									<div class="row">
										<div class="col-md-4">
											<div class="img-box">
												<?php
												$promo_image_val_1 = (isset($promo_image_1) && !empty($promo_image_1)) ? $promo_image_1 :'';
												$img_1_display = (isset($promo_image_1) && !empty($promo_image_1)) ? '' :'dn';
												if(isset($promo_image_1) && $promo_image_1 != '') {
													$promo_image_1 = base_url('uploads/promotion_images/'.$promo_image_val_1);
													$title = lang('tooltip_offer_image');
													
													$promo_image_1_doc = $this->config->item('document_path').'uploads/promotion_images/'.$promo_image_val_1; 
													if(!file_exists($promo_image_1_doc)){
														$promo_image_1 = base_url('uploads/No_Image_Available.png');
														$title = "No Image Available";
													}
												} else {
													$title = lang('tooltip_promo_image');
													$promo_image_1 = base_url('uploads/upload.png');
												} ?>
												<a id="remove_btn_1" class="<?php echo $img_1_display;?>"  href="javascript:void(0);" onclick="remove_image(1)">×</a>
												<div class="cell-img" >
													<img id="image-picker_1" src="<?php echo $promo_image_1; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
												</div>
												<?php
												$data	= array('name'=>'promo_image_1', 'id'=>'promo_image_1', 'value'=>set_value('promo_image_1',$promo_image_val_1),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
												echo form_upload($data);	
												$data	= array('name'=>'default_image_type', 'value'=>'1','id'=>'is_default_image_1','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image')); // fist image is alway the default image
												echo form_radio($data);?>
												<label for="is_default_image_1"><?php echo lang('offer_default_image');?></label>
											</div>	
										 </div>	
										 
										<div class="col-md-4 " >
											<div class="img-box">
												<?php
												$promo_image_val_2 = (isset($promo_image_2) && !empty($promo_image_2)) ? $promo_image_2 :'';
												$img_2_display = (isset($promo_image_2) && !empty($promo_image_2)) ? '' :'dn';
												if(isset($promo_image_2) && $promo_image_2 != '') {
													$promo_image_2 = base_url('uploads/promotion_images/'.$promo_image_val_2);
													$title = lang('tooltip_offer_image');
													$promo_image_2_doc = $this->config->item('document_path').'uploads/promotion_images/'.$promo_image_val_2; 
													if(!file_exists($promo_image_2_doc)){
														$promo_image_2 = base_url('uploads/No_Image_Available.png');
														$title = "No Image Available";
													}
												} else {
													$title = lang('tooltip_offer_image');
													$promo_image_2 = base_url('uploads/upload.png');
												} ?>
												<a id="remove_btn_2" class="<?php echo $img_2_display;?>"  href="javascript:void(0);" onclick="remove_image(2)">×</a>
												<div class="cell-img" ><img id="image-picker_2" src="<?php echo $promo_image_2; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
												<?php
												$data	= array('name'=>'promo_image_2', 'id'=>'promo_image_2', 'value'=>set_value('promo_image_2',$promo_image_val_2),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
												echo form_upload($data);
												if(isset($default_image_type) && $default_image_type == 2) { 
													$data	= array('name'=>'default_image_type', 'value'=>'2','id'=>'is_default_image_2','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
												} else {
													$data	= array('name'=>'default_image_type', 'value'=>'2','id'=>'is_default_image_2','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
												}
												echo form_radio($data);?>
												<label for="is_default_image_2"><?php echo lang('offer_default_image');?></label>
											</div>
										</div>
										
										<div class="col-md-4 " >
											<div class="img-box">
												<?php
												$promo_image_val_3 = (isset($promo_image_3) && !empty($promo_image_3)) ? $promo_image_3 :'';
												$img_3_display = (isset($promo_image_3) && !empty($promo_image_3)) ? '' :'dn';
												if(isset($promo_image_3) && $promo_image_3 != '') {
													$promo_image_3 = base_url('uploads/promotion_images/'.$promo_image_val_3);
													$title = lang('tooltip_offer_image');											
													$promo_image_3_doc = $this->config->item('document_path').'uploads/promotion_images/'.$promo_image_val_3; 
													if(!file_exists($promo_image_3_doc)){
														$promo_image_3 = base_url('uploads/No_Image_Available.png');
														$title = "No Image Available";
													}
												   
												} else {
													$title = lang('tooltip_offer_image');
													$promo_image_3 = base_url('uploads/upload.png');
												}
												?>
												<a id="remove_btn_3" class="<?php echo $img_3_display;?>"  href="javascript:void(0);" onclick="remove_image(3)">×</a>
												<div class="cell-img" ><img id="image-picker_3" src="<?php echo $promo_image_3; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' /></div>
												<?php
												$data	= array('name'=>'promo_image_3', 'id'=>'promo_image_3', 'value'=>set_value('promo_image_3',$promo_image_val_3),  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_image'), 'style'=>'display:none', 'onchange'=>'read_url_image(this)');
												echo form_upload($data);
												
												if(isset($default_image_type) && $default_image_type == 3) { 
													$data	= array('name'=>'default_image_type', 'value'=>'3','id'=>'is_default_image_3','checked'=>'checked','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
												} else {
													$data	= array('name'=>'default_image_type', 'value'=>'3','id'=>'is_default_image_3','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_default_image'));
												}
												echo form_radio($data);	?>
												<label for="is_default_image_3"><?php echo lang('offer_default_image');?></label>
											</div>	  
										</div>	
										<div class="alert red-alert alert-danger alert-dismissible ">
											<i class="icon fa fa-warning"></i>
											Upload up to 3MB images. 
										</div>  
									</div>
								</div>
								<!-- Prmotion banner image box end here-->		 
							</div>	 
						</div>	
						<div class="row pb-3 float-right">
							<input type="hidden" value="<?php echo set_value('promo_id', $promo_id); ?>" name='promo_id' id='promo_id'>
							<div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'backend/promotion' ?>'>
									<button type="button" class="btn waves-effect waves-light btn-block btn-secondary"><?php echo lang('form_cancel');?></button>
								</a>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div> 
	   </div> 
   </div> 
</div>

<script type="text/javascript">
	// Initialize material Date picker    
	$('#promo_start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
	$('#promo_end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
	$(document).ready(function() {
		$("#add_promotion_form").validate({
			// post form to action url
		});
	});
	
	function checkCheckBox(checkbox){
		//alert(checkbox);
		for(var i=1;i<=3;i++){
			if(checkbox != 'check'+i) {
				$('#check'+i).attr('checked', false);
			}
		}
	}
	
	$("#image-picker_1").click(function() {
		$("input[name='promo_image_1']").click();
	});
	$("#image-picker_2").click(function() {
		$("input[name='promo_image_2']").click();
	});
	$("#image-picker_3").click(function() {
		$("input[name='promo_image_3']").click();
	});
	$("#image-picker_kiosk").click(function() {
		$("input[name='kiosk_small_promo_image']").click();
	});
	$("#image-picker_kiosk_large").click(function() {
		$("input[name='kiosk_large_promo_image']").click();
	});
	
	function read_url_image(input) {
		//alert(input.id);
		if(input.id == 'promo_image_1') {
			var pickerId	= '#image-picker_1';
			var fieldId	= '#promo_image_1';
			$("#remove_btn_1").show();
		} else if(input.id == 'promo_image_2') {
			var pickerId	= '#image-picker_2';
			var fieldId	= '#promo_image_2';
			$("#remove_btn_2").show();
		} else if(input.id == 'promo_image_3') {
			var pickerId	= '#image-picker_3';
			var fieldId	= '#promo_image_3';
			$("#remove_btn_3").show();
		} else if(input.id == 'kiosk_small_promo_image') {
			var pickerId	= '#image-picker_kiosk';
			var fieldId	= '#kiosk_small_promo_image';
		} else if(input.id == 'kiosk_large_promo_image') {
			var pickerId	= '#image-picker_kiosk_large';
			var fieldId	= '#kiosk_large_promo_image';
		}
		if (input.files && input.files[0]) {
			var file = input.files[0];
			var size = file.size/1000000; // get image size in mb;
			if(size <= 3) {
				var imageType = /image.*/;
				if (file.type.match(imageType)) {
					var reader = new FileReader();
					reader.onload = function (e) {
						//alert(pickerId);
						$(pickerId).attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					$(fieldId).val("");
					alert("File not supported!");
				}
			} else {
				$(fieldId).val("");
				bootbox.alert("File size should be less then 3MB!");
			}
		}
	}
	
	
	$("#promo_price").on("keyup", function(){
			var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
			val = this.value;
		
		if(!valid){
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});
	
	/**
	* Function used to remove image
	*/
	function remove_image(image_type) {
		bootbox.confirm("Are you sure want to delete this image?", function(result) {
			if(result==true) {
				// unset image data
				$("#image-picker_"+image_type).attr('src', '<?php echo base_url('uploads/upload.png');?>');
				var promo_id = $('#promo_id').val();
				$.ajax ({
					type: "POST",
					data : {promo_id:promo_id,image_type:image_type},
					url: "<?php echo base_url() ?>backend/promotion/remove_image",
					async: false,
					success: function(data){ 
						if(data) {
							var file = $('#image-picker_'+image_type).attr('src','<?php echo base_url('uploads/upload.png');?>');
							$("#remove_btn_"+image_type).hide();
						}
					}, 
					error: function(error){
						bootbox.alert(error);
					}
				});
			}
		});
	}
	
	/*
	form submition 
	*/
	$( "#add_promotion_form" ).submit(function( event ) {
		checkBeacon()
		return;
		event.preventDefault();
	});
	
	/*
	start date and end date validation
	*/
	function checkBeacon() {
		var start_date 	=  $( "#promo_start_date" ).val();
		var split_date_time = 	start_date.split(" ");
		var split_date 	=	split_date_time[0].split("-");
		var new_date_time 	= split_date[2]+'/'+split_date[1]+'/'+split_date[0]+" "+split_date_time[1];

		var end_date 	=  $( "#promo_end_date" ).val();
		var split_end_date_time = 	end_date.split(" ");
		var split_end_date 		=	split_end_date_time[0].split("-");
		var new_end_date_time 	= split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0]+" "+split_end_date_time[1];

		var sDate 		=  new Date(new_date_time);
		var eDate 		=  new Date(new_end_date_time);

		var nowTemp 	= new Date('<?php echo date('Y/m/d H:i:s'); ?>');
		var today 		= new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), nowTemp.getHours(), nowTemp.getMinutes(), nowTemp.getSeconds());
		if(start_date != '' && today > eDate) { 
			bootbox.alert("End date should be greater than Today's date");
			$( "#promo_end_date" ).val('');
		} else if(start_date != '' && end_date != '' && sDate > eDate) {
			bootbox.alert("<?php echo lang('start_date_is_greater');?>");
			$( "#promo_end_date" ).val('');
		}
	}

    </script>
