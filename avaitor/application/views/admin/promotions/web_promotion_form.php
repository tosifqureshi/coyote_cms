<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo $promo_title;?></h4>
				<div class="p-t-20">

<?php echo form_open_multipart('backend/web_promotion/create/'.$promo_id,'id="add_promotion_form"'); ?>
						<div class="row">
							<div class="col-md-6">
							<!-- Promotion name start here -->
							<div class="form-group form-material">
							<label for="title"><?php echo lang('promo_name');?><i style="color:red;">*</i></label>
							<?php
							$data = array('name'=>'promo_name', 'value'=>set_value('promo_name',$promo_name), 'class'=>'form-control form-control-line required', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>lang('tooltip_promo_name'));
							echo form_input($data);
							?>
							</div>
							<!-- Promotion name end here -->

							<!-- Promotion start / end date fields start here -->
							<div class="row form-group form-material">
								<div class="col-md-6">
									<label class="m-t-20"><?php echo lang('start_time');?><i style="color:red;">*</i></label>
									<?php
									$data	= array('name'=>'promo_start_date', 'data-format'=>'dd-MM-yyyy hh:mm:ss', 'value'=>set_value('promo_start_date', $promo_start_date),'id'=>'promo_start_date','class'=>'form-control required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_start_time'), 'readonly'=>'readonly');
									echo form_input($data);
									?>
								</div>
								<div class="col-md-6">
									<label class="m-t-20"><?php echo lang('end_time');?><i style="color:red;">*</i></label>
									<?php
									$data = array('name'=>'promo_end_date', 'data-format'=>'dd-MM-yyyy hh:mm:ss', 'value'=>set_value('promo_end_date', $promo_end_date),'id'=>'promo_end_date','class'=>'form-control required','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_offer_end_time'), 'readonly'=>'readonly');
									echo form_input($data);
									?>
								</div>
							</div>
							<!-- Promotion start / end date fields end here -->

								<!-- Promotion short description start here -->
								<div class="form-group form-material">
									<label for="promo_short_description"><?php echo lang('promo_short_description');?><i style="color:red;">*</i></label>		
									<?php $data	= array('name'=>'promo_short_desc', 'value'=>$promo_short_desc, 'class'=>'required form-control form-control-line' , 'rows'=>3);
									echo form_textarea($data);?>
								</div>
								<!-- Promotion short description end here -->

								<!-- Promotion long description start here -->
								<div class="form-group form-material">
									<label for="promo_long_desc"><?php echo lang('promo_long_description');?></label>	
									<?php $data	= array('name'=>'promo_long_desc', 'class'=>'mceEditor redactor form-control', 'value'=>$promo_long_desc);
									echo form_textarea($data);?>	
								</div>	
								<!-- Promotion short description end here -->				
							</div>


							<!-- Images right side block start here -->
							<div class="col-md-6">
								<div class="form-group">
								<label><?php echo lang('offer_image');?></label> 
								<div class="row">	
									<div class="img-box">
										<?php
										// set banner image action
										$promo_image_val = (isset($promo_image) && !empty($promo_image)) ? $promo_image :'';
										$img_1_display = (isset($promo_image_val) && !empty($promo_image_val)) ? '' :'dn';
										if(!empty($promo_image_val)) {
										$promo_image_src = base_url('uploads/promotion_images/'.$promo_image);
										$title = lang('tooltip_offer_image');

										$promo_image_val_doc = $this->config->item('document_path').'uploads/promotion_images/'.$promo_image; 
										if(!file_exists($promo_image_val_doc)){
										// $promo_image_1 = base_url('uploads/No_Image_Available.png');
										$promo_image_src = base_url('uploads/No_Image_Available.png');
										$title = "No Image Available";
										}
										} else {
										$title = lang('tooltip_image');
										$promo_image_src = base_url('uploads/upload.png');
										} ?>
										<div class="cell-img" id="promo_image_cell">
										<img id="snw_promo_image_picker" src="<?php echo $promo_image_src; ?>" data-toggle='tooltip', data-placement='right', title='<?php echo $title; ?>' />
										</div>
										<?php
										$data	= array('name'=>'promo_image', 'id'=>'promo_image', 'value'=>$promo_image_val,  'data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>lang('tooltip_promo_image'), 'style'=>'display:none', 'onchange'=>'read_reward_url_promo_image(this)');
										echo form_upload($data);
										// set banner image value in hidden type
										$data = array('name'=>'promo_image_val', 'value'=>$promo_image_val,'id'=>'promo_image_val','type'=>'hidden');
										echo form_input($data);
										?>
										<div class="alert red-alert alert-danger alert-dismissible ">
									<i class="icon fa fa-warning"></i>
									Upload up to 3MB images. 
									</div> 
									</div>
									
								</div>
								<!-- Images right side block end here -->	
								</div>
							</div>

						</div>

						<div class="row pb-3 float-right">
							<input type="hidden" value="<?php echo set_value('promo_id', $promo_id); ?>" name='promo_id' id='promo_id'>
							<div class="mx-1">
								<button type="submit" name="save" class="btn btn-primary"><?php echo lang('form_save');?></button>
							</div>
							<div class="mx-1">
								<a href='<?php echo base_url().'backend/promotion' ?>'><button type="button" name="cancel" class="btn default"><?php echo lang('form_cancel');?></button></a>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div> 
	   </div> 
   </div> 
</div>
<script type="text/javascript">

	$('#promo_start_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
	$('#promo_end_date').bootstrapMaterialDatePicker({
		weekStart: 0,
		time: true,
		format : 'YYYY-MM-DD H:m:s'
	});
	
	$(document).ready(function() {
		$("#add_promotion_form").validate({
			// post form to action url
		});
	});


	$(document).ready(function() {
		$("#add_promotion_form").validate({
			// post form to action url
		});
	});
	
	
	 $("#snw_promo_image_picker").click(function() {
        $("input[name='promo_image']").click();
    });

	function read_reward_url_promo_image(input) {
    
        var pickerId= '#snw_'+input.id+'_picker';
        var fieldId	= '#'+input.id;
        $(fieldId+'_val').val(input.value);
        
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var size = file.size/1000000; // get image size in mb;
            if(size <= 3.0955135) {
                var imageType = /image.*/;
                if (file.type.match(imageType)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //alert(pickerId);
                        $(pickerId).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $(fieldId).val("");
                    bootbox.alert("File not supported!");
                }
            } else {
                $(fieldId).val("");
                bootbox.alert("File size should be less then 3MB!");
            }
        }
    }
	
	
	$("#promo_price").on("keyup", function(){
			var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
			val = this.value;
		
		if(!valid){
			console.log("Invalid input!");
			this.value = val.substring(0, val.length - 1);
		}
	});
	
	/*
	form submition 
	*/
	$( "#add_promotion_form" ).submit(function( event ) {
		var promo_image_val = $('#promo_image_val').val();
		if(promo_image_val == '') { 
			bootbox.alert("Please upload promotion image.");
			return false;
		}
		
		checkBeacon()
		return;
		event.preventDefault();
	});
	/*
	start date and end date validation
	*/
	function checkBeacon() {
		var start_date 	=  $( "#promo_start_date" ).val();
		var split_date_time = 	start_date.split(" ");
		var split_date 	=	split_date_time[0].split("-");
		var new_date_time 	= split_date[2]+'/'+split_date[1]+'/'+split_date[0]+" "+split_date_time[1];

		var end_date 	=  $( "#promo_end_date" ).val();
		var split_end_date_time = 	end_date.split(" ");
		var split_end_date 		=	split_end_date_time[0].split("-");
		var new_end_date_time 	= split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0]+" "+split_end_date_time[1];

		var sDate 		=  new Date(new_date_time);
		var eDate 		=  new Date(new_end_date_time);

		var nowTemp 	= new Date('<?php echo date('Y/m/d H:i:s'); ?>');
		var today 		= new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), nowTemp.getHours(), nowTemp.getMinutes(), nowTemp.getSeconds());
		if(start_date != '' && today > eDate) { 
			bootbox.alert("End date should be greater than Today's date");
			$( "#promo_end_date" ).val('');
		} else if(start_date != '' && end_date != '' && sDate > eDate) {
			bootbox.alert("<?php echo lang('start_date_is_greater');?>");
			$( "#promo_end_date" ).val('');
		}
	}

    </script>
