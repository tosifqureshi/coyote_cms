<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo lang('kiosk_setting_heading');?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					echo form_open('backend/settings/kiosk_store_settings/', 'class="form-material form-with-label" id="kiosk_setting_form"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- employee discount input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('employee_discount');?></label>
									<input type="text" name="employee_discount" id="employee_discount" class="form-control form-control-line number" value="<?php echo set_value('employee_discount', (!empty($employee_discount))  ? $employee_discount : '') ?>" maxlength=5 />
									<input type="hidden" name="form_action" value="<?php echo set_value('form_action', (!empty($employee_discount)) ? 'update' : 'insert') ?>" />
								</div>
								<!-- employee discount input end here -->
								
								<!-- Normal discount input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('normal_discount');?></label>
									<input type="text" name="normal_discount" id="normal_discount" class="form-control form-control-line number" value="<?php echo set_value('normal_discount', (!empty($normal_discount))  ? $normal_discount : '') ?>" maxlength=5 />
								</div>
								<!-- Normal discount input end here -->
								
								<!-- Exclude category input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('exclude_category');?></label>
									<input type="text" name="exclude_category" id="exclude_category" class="form-control form-control-line" value="<?php echo set_value('exclude_category', (!empty($exclude_category))  ? $exclude_category : '') ?>"/>
									<input type="hidden" name="form_version_action" value="<?php echo set_value('form_version_action', (!empty($exclude_category)) ? 'update' : 'insert') ?>" />
								</div>
								<!-- Exclude category input end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('bf_action_save') .' '. lang('bf_context_settings') ?></button>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#kiosk_setting_form").validate({
			// post form to action url
		});
	});
</script>
