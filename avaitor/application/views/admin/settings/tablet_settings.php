<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body p-b-0">
				<h4 class="card-title"><?php echo lang('tablet_setting_heading');?></h4>
				<!-- Tab panes -->
				<div class="p-t-20">
					<?php
					echo form_open('backend/settings/tablet_store_settings/', 'class="form-material form-with-label" id="tablet_setting_form"'); ?>
						<div class="row">
							<div class="col-md-6">
								<!-- unlock password input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('unlock_password');?></label>
									<input type="text" name="unlock_password" id="unlock_password" class="form-control form-control-line required" value="<?php echo set_value('unlock_password', (!empty($unlock_password))  ? $unlock_password : '') ?>" />
									<input type="hidden" name="form_action" value="<?php echo set_value('form_action', (!empty($unlock_password)) ? 'update' : 'insert') ?>" />
								</div>
								<!-- unlock password input end here -->
								
								<!-- app version input start here -->
								<div class="form-group ">
									<label class=""><?php echo lang('app_version');?></label>
									<input type="text" name="app_version" id="app_version" class="form-control form-control-line required number" value="<?php echo set_value('app_version', (!empty($app_version))  ? $app_version : '') ?>"/>
									<input type="hidden" name="form_version_action" value="<?php echo set_value('form_version_action', (!empty($app_version)) ? 'update' : 'insert') ?>" />
								</div>
								<!-- app version input end here -->
							</div>
						</div>
						<div class="row pb-3 float-right">
							 <div class="mx-1">
								<button type="submit" name="save" class="btn waves-effect waves-light btn-block btn-info"><?php echo lang('bf_action_save') .' '. lang('bf_context_settings') ?></button>
							</div>
						</div>	
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#tablet_setting_form").validate({
			// post form to action url
		});
	});
</script>
