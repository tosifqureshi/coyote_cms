  <div class="row">
	<div class="col-md-12">
		<div class="row margin_10_top">
			<div class="col-md-2"></div>
			<div class="col-md-12">
				<!--<div class="row pos_relative">
					<div class="col-md-6 margin_10_bot margin_10_top"><h4 class="margin_zero"><?php //echo theme_view('parts/_breadcrumb');?></h4></div>
				</div> -->
			</div>
		</div>
	</div>  
	<div class="col-md-12 page_content">
		<div class="row">
			<div class="col-md-12 background_white">
				<?php
				echo Template::message();
				echo isset($content) ? $content : Template::generate();
				?>
			</div>        
		</div>
	</div>
 </div>
