<div class="row">
	<div class="col-md-12 home_content">
		<div class="row">
			<div class="col-md-6">
				<div class="box-404 bg">
					<h1>404</h1>
					<h2>The page cannot be found.</h2>
				</div>
			</div>
			<div class="col-md-6">
				<h3>Whoops, page not found...</h3>
				<p>The page you requested was not found, and we have a fine guess why.</p>
				<ul>
					<li>If you typed the URL directly, please make sure the spelling is correct.</li>
					<li>If you clicked on a link to get here, the link is outdated.</li>
				</ul>
					<b>What can you do?</b>
					<p>Have no fear, help is near! There are many ways you can get back on track with Coyote.</p>
				<ul>
					<li><a href="<?php echo base_url()?>">Go back</a> to the previous page.</li>                      
				</ul>
			</div>
		</div>
	</div>
</div>
    
