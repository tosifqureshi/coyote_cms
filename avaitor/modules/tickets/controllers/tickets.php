<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users Controller
 *
 * Provides front-end functions for Ticket listing,openticket,view,search,reply 
 *
 * @package    Airdoc
 * @subpackage Modules_Tickets
 * @category   Controllers
 * @author     Amit,Neeraj
 * @link       http://www.cdnsolutionsgroup.com
 *
 */
class Tickets extends Front_Controller
{
    var $data = array();
	//--------------------------------------------------------------------

	/**
	 * Setup the required libraries etc
	 *
	 * @retun void
	 */
	public function __construct()
	{
		 
		parent::__construct();       
        
		$this->load->helper('form');
		$this->load->helper('tickets');
		$this->load->library('form_validation');
		//$this->form_validation->CI =& $this;

		if (!class_exists('Tickets_model')){
			$this->load->model('tickets/Tickets_model', 'tickets_model');
		}
		$this->load->database();
		$this->lang->load('tickets');
				
	}//end __construct()

	/**
	 * Generate ticket
	 * Select department to generate ticket
	 * @retun void
	**/
    function index($msg=''){
		if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		}		
		$departments = $this->tickets_model->ListDepartments();
		if ($this -> input -> post('department')){
			$departmentid = $this -> input -> post('department');
		}else{
			$departmentid= $this -> uri -> segment(3);
		}
		
		Template::set('departments', $departments);
		Template::set('success', $msg);		
		Template::set('departmentid', $departmentid);
		Template::set_view('generate_ticket');
		Template::render();
	}  
     
    /**
	 * Generate ticket
	 * params int department id
	 * @retun void
	**/ 
    function openticket($deptid=0)
	{
		if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		} 
		if ($deptid >= 0){
			 //$departmentid = $this -> uri -> segment(3);
			$priorities = $this -> tickets_model -> ListPriorities();
		
			if ($this -> input -> post('department')){
			$departmentid = $this -> input -> post('department');
			$departmentid = $departmentid;
			}else{
			$departmentid= $this -> uri -> segment(3);
			$departmentid = $departmentid;
			}
						
			Template::set('priorities', $priorities);
			Template::set('departmentid', $departmentid);
			Template::set_view('openticket');
			Template::render();				
		}else{
			$message = 'Please select a department to proceed';
			$this -> index($message);			
		}
	}
	
	/**
	 * Create ticket
	 * @params post values
	 * @retun void
	**/
	function create(){
		if ($this->auth->is_logged_in() === FALSE){
			$this->auth->logout();
			redirect('login');
		}
		
		if(!$this->input->post()){
			redirect('tickets');		
			}
				
		$body = $this -> input -> post('body');
		$department = $this -> input -> post('department');
		$email =  $this->session->userdata('identity');
		$priority = $this -> input -> post('priority');
		$subject = $this -> input -> post('subject');
		
		
		$this -> form_validation -> set_rules('subject', 'Subject', 'trim|required|max_length[30]');
		$this -> form_validation -> set_rules('body', 'Query', 'trim|required');
		$this -> form_validation -> set_rules('department', 'Department', 'required');
 
		if ($this -> form_validation -> run() == FALSE)	{
			$priorities = $this -> tickets_model -> ListPriorities();
			Template::set('priorities', $priorities);
			Template::set('departmentid', $department);
			Template::set_view('openticket');
			Template::render();	
		}else{
			
			// Validation is success. So create ticket

			// Upload the file.
			if (isset($_FILES['userfile']))
				if (($_FILES['userfile']['name']) != ''){

					$config['upload_path'] = './attachments/';
					$config['allowed_types'] = 'gif|jpg|jpeg|pdf|png';
					$config['max_size'] = '5120';
					$config['max_width'] = '0';
					$config['max_height'] = '0';
					$config['encrypt_name'] = TRUE;
					$config['remove_spaces'] = TRUE;
					$this -> load -> library('upload', $config);

					if (!$this -> upload -> do_upload()){						
						$priorities = $this -> tickets_model -> ListPriorities();
						$error = $this -> upload -> display_errors();
						Template::set('priorities', $priorities);
						Template::set('departmentid', $department);
						Template::set('error', $error);
						Template::set_view('openticket');
						Template::render();							
					}

					$data = $this -> upload -> data();
					if (is_array($data))
						$attachment = $data['file_name'];
				}

			$clientid = $this->current_user->id;

			$insert = array(
				'subject' => $subject,
				'department' => $department,
				'clientid' => $clientid,
				'priority' => $priority,
				'body' => $body,
				'status' => 'open'
			);

			if (isset($attachment))
				$insert['attachment'] = $attachment;			

			$ticketid = $this ->tickets_model->Addticket($insert);

			if ($ticketid > 0)
			{
				// 1. Send notification to admin
				// 2. Redirect the user to the ticket list page				
				// Send mail to the ticket submitter

				$ticket_link = site_url() . 'tickets/viewticket/' . $ticketid;
				$data['email'] =$email; 
				$data['ticket_link'] =$ticket_link; 
				$body = $this->load->view('_emails/create_ticket',$data,TRUE);

				//$this ->SendMail($email, "Ticket #$ticketid Created", $body);

				// Now we send emails to staff

				$staffs = $this -> tickets_model -> ListStaffs();
				$link = site_url() . 'admin/tickets/view/' . $ticketid;
				$subject = "Ticket #$ticketid Opened";
				
				$data['email'] =$email; 
				$data['link'] =$link; 
				$body = $this->load->view('_emails/send_staff',$data,TRUE);
				
				
				foreach ($staffs as $staff)
				{
					//$this ->SendMail($staff['email'], $subject, $body);
				}

				$msg = 'Ticket Created Successfully. Please click here to view your ticket'; 
				$this -> mytickets();
			}

		}

	}

    /**
	 * Ticket Listing
	 * Show logged in users tickets
	 * @retun void
	**/
    function mytickets(){
		if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		}
		$clientid = $this->current_user->id;
		
		if ($clientid == FALSE)
			redirect(base_url().'tickets');

		$tickets = $this -> tickets_model -> GetTicketsOfClient($clientid);
		
		Template::set('tickets', $tickets);	
		Template::set('clientid', $clientid );
		Template::set('title', 'My Tickets');
		Template::set_view('mytickets');
		Template::render();	
	}
   
	/**
	* Show Ticket
	* @params int ticket id 
	* @retun void
	**/
   function viewticket($ticketid = ''){
	    if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		}
		
		if($ticketid ==''){
			redirect(base_url().'tickets/mytickets');
			}
		$ticketid = decode($ticketid);
		// Make sure the client has access to this ticket.		 	
		 $ticket = $this -> tickets_model -> GetTicketDetails($ticketid);
		 
		 if(empty($ticket)){
			   redirect(base_url().'tickets/mytickets');
			 }
		
	   	
        $clientid = $this->current_user->id;
		$ticketid = $ticketid;
		$replies = $this -> tickets_model -> GetTicketReplies($ticketid);
		
		foreach($replies as $reply) { 
		if(isset($reply['status']) && ($reply['status']==0) && ($reply['replier']!= 'client')){			
		  change_status($reply['id']);	 
		} }
		
		Template::set('ticket', $ticket );
		Template::set('ticketid', $ticketid );
		Template::set('clientid', $clientid );
		Template::set('replies', $replies );		
		Template::set('title', 'View Tickets');
		Template::set_view('viewticket');
		Template::render();	
	}
	
	/**
	* Reply Ticket
	*  
	* @retun void
	**/
	function reply(){
		if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		}
		$ticketid = $this -> input -> post('ticketid');
		$reply = $this -> input -> post('reply');		
		$ticket_id = decode($ticketid);
		
		$this -> form_validation -> set_rules('reply', 'Reply', 'trim|required');
		
		if ($this -> form_validation -> run() == FALSE)
		{   
			$this -> viewticket($ticketid);
		}
		else
		{
			$clientid = $this->current_user->id;
			$data = array(
				'replier' => 'client',
				'ticketid' => $ticket_id,
				'body' => $reply,
				'replierid' => $clientid,
			);

			$status = 'customer reply';
			$this -> tickets_model -> AddReply($data, $status);
			$success = "Ticket reply has been added";

			// Send mail to staffs

			$staffs = $this -> tickets_model -> ListStaffs();
			$link = site_url() . '/tickets/viewTicket/' . $ticketid;
			$subject = "Ticket #$ticketid Replied By Client";
			
			$data['ticketid']=$ticketid;
			$data['link']=$link;			
		    $body  = $this->load->view('_emails/ticket',$data,TRUE);		

			foreach ($staffs as $staff)
			{
				//$this -> SendMail($staff['email'], $subject, $body);
			}

			$this -> viewticket($ticketid);
		}
	}
	
	/**
	* Send Mail
	* get smtp details for database
	* @paramas to,subject and body of mail
	*  
	* @retun void
	**/
	function SendMail($to, $subject, $body)	{
		$settings = getSmtpDetails(); //Get details for Email settings
		$mailprotocol = (isset($settings['protocol']) && $settings['protocol']!='' ) ? $settings['protocol'] : '';
		$sender_email = (isset($settings['sender_email']) && $settings['sender_email']!='' ) ? $settings['sender_email'] : '';
		$smtp_host = (isset($settings['smtp_host']) && $settings['smtp_host']!='' ) ? $settings['smtp_host'] : '';
		$smtp_pass = (isset($settings['smtp_pass']) && $settings['smtp_pass']!='' ) ? $settings['smtp_pass'] : '';
		$smtp_user = (isset($settings['smtp_user']) && $settings['smtp_user']!='' ) ? $settings['smtp_user'] : '';
		$smtp_port = (isset($settings['smtp_port']) && $settings['smtp_port']!='' ) ? $settings['smtp_port'] : '';	
		
		$this -> load -> library('email');
        $mailprotocol = $mailprotocol;
		$smtp_host = $smtp_host;
		$smtp_port = $smtp_port;
		$smtp_user = $smtp_user;
		$smtp_pass = $smtp_pass;

		$config = array(
			'useragent' => "XTicket",
			'protocol' => $mailprotocol,
			'smtp_host' => $smtp_host,
			'smtp_user' => $smtp_user,
			'smtp_pass' => $smtp_pass,
			'smtp_port' => $smtp_port,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n",
			'wordwrap' => TRUE,
		);
		$this -> email -> initialize($config);
		$email = $sender_email;
		$companyname = 'Airdoc';		
		$this -> email -> from($email, $companyname);
		$this -> email -> to($to);
		$this -> email -> subject($subject);
		$this -> email -> message($body);
		if ($this -> email -> send()){
			return TRUE;			
		}else{
			echo 'Unable to send mail';
			$error = $this -> email -> print_debugger();			
			print_r($error);die;			
		}     
	}
	
	/*
	 * Search tickets in list view
	 * params get post values from ajax request
	 * @return array of searched data 
	 */
	function searchTickets(){
		if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		}
		$tid = $this->input->post('tid'); 
		$subject = $this->input->post('subject'); 
		$status = $this->input->post('status'); 
		$sort_by = $this->input->post('sort_by'); 		
		$sort_type = $this->input->post('sort_type');
		$clientid = $this->current_user->id;
		$data['tickets'] = $this->tickets_model->searchTickets($tid,$subject,$status,$sort_by,$sort_type,$clientid);
		echo $this->load->view('search',$data,TRUE);		
		die;		
		}
	
	/*
	 * Sort tickets in list view
	 * params get post values from ajax request
	 * @return array of searched data 
	 */	
	function sortTickets(){
		if ($this->auth->is_logged_in() === FALSE)
		{
			$this->auth->logout();
			redirect('login');
		}
		
		$tid = $this->input->post('tid'); 
		$subject = $this->input->post('subject'); 
		$status = $this->input->post('status'); 
		
		$field = $this->input->post('sort_by'); 		
		$sort_type = $this->input->post('sort_type'); 		
		$clientid = $this->current_user->id;
		$data['tickets'] = $this->tickets_model->sortTickets($tid,$subject,$status,$field,$sort_type,$clientid);
		echo $this->load->view('search',$data,TRUE);		
		die;		
		}
		

	
}//end Users

/* Front-end Users Controller */
/* End of file users.php */
/* Location: ./application/core_modules/users/controllers/users.php */
