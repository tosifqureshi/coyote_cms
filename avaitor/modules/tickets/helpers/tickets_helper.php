<?php 
function createRandomNo($length=10) {
    $len = $length;
    $base = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
    $max = strlen($base) - 1;
    $activatecode = '';
    mt_srand((double) microtime() * 1000000);

    while (strlen($activatecode) < $len + 1) {
        $activatecode.=$base{mt_rand(0, $max)};
        
    }
      return $activatecode;
    
}


function ListDepartments() {
   $CI = & get_instance();    
   $CI->load->model('tickets_model');
   $result = $CI->tickets_model->ListDepartments();
   return $result;    
}


function message() {
   $CI = & get_instance();    
   $CI->load->model('tickets_model');
   $result = $CI->tickets_model->GetNotificationMessage();
   return $result;    
}


function ListPriorities() {
   $CI = & get_instance();    
   $CI->load->model('tickets_model');
   $result = $CI->tickets_model->ListPriorities();
   return $result;    
}

function GetAdditionalFields() {
   $CI = & get_instance();    
   $CI->load->model('tickets_model');
   $result = $CI->tickets_model->GetAdditionalFields();
   return $result;    
}

function ListStaffs() {
   $CI = & get_instance();    
   $CI->load->model('tickets_model');
   $result = $CI->tickets_model->ListStaffs();
   return $result;    
}

/*
function getSmtpDetails(){
	$CI = & get_instance();    
	$CI->load->model('tickets_model');
	$result = $CI->tickets_model->getMailSettings();
	
	if(isset($result) && is_array($result) && count($result)>0){		
		$emailinfo = array();
		foreach ($result as $email){			  
		$emailinfo[$email->name] = $email->value;
		}
		return $emailinfo;		
		}
	return false; 
	
	}
*/	
	
function get_status($ticketid=0){	
	$CI = & get_instance();    
	$CI->load->model('tickets_model');
	$result = $CI->tickets_model->get_status($ticketid);
	  if(!empty($result)){
		 return $result = count($result); 	 
    }else {
		    return false;
		 }
	}	
	
	
	
	
