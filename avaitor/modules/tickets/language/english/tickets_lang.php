<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['files']				= 'Files';
$lang['no_record']			= 'No Record Found';
$lang['ticket']				= 'Ticket#';
$lang['subject']			= 'Subject';
$lang['status']				= 'Status';
$lang['close']				= 'Close';
$lang['edit_department']	= 'Edit Department';
$lang['staff_name']			= 'Staff Name';
$lang['name']				= 'Name';
$lang['action']				= 'Action';
$lang['edit']				= 'Edit';
$lang['delete']				= 'Delete';
$lang['view_ticket']		= 'Viewing Ticket #';
$lang['ticket_info']		= 'Ticket Info';
$lang['ticket_id']			= 'Ticket ID';
$lang['department']			= 'Department';
$lang['you']				= 'You -';
$lang['post_reply']			= 'Post a reply';
$lang['you_have']			= 'You have';
$lang['messages']			= 'messages';
$lang['no_messages']		= 'You have no new message';
$lang['see_all']			= 'See All Messages';
$lang['welcome_heading']	= 'Welcome to Support Desk';
$lang['my_tickets']			= 'My Tickets';
$lang['welcome_text']		= 'Welcome to our Support Desk. Our Support Desk will help you to contact us for all your questions and get instant answers to it.';
$lang['welcome_text_part']	= 'In order to open a support ticket, please select a department below.';
$lang['select_dept']		= 'Select Department';
$lang['gen_ticket']			= 'Generate Ticket';
$lang['new_ticket']			= 'New Ticket';
$lang['email_address']		= 'Email Address';
$lang['your_query']			= 'Your Query';
$lang['attachment']			= 'Attachment';
$lang['priority']			= 'Priority';


 

