<?php 

Class Tickets_model extends CI_Model{
    
     /**
	 * Name of the table
	 * @access protected 
	 * @var string
	 */	 
	 
	protected $tbldepartments = 'tbldepartments';    
	protected $tabelesetting = 'tblsettings';    
	protected $tblpriorities = 'tblpriorities';    
	protected $tblfields = 'tblfields';    
	protected $tblstaffs = 'tblstaffs';    
	protected $tbltickets = 'tbltickets';    
	protected $tblticketreplies = 'tblticketreplies';    
	protected $users = 'users';    
	protected $tblstatus = 'tblstatus';    
	protected $settings = 'settings` ';    
	
    
    function __construct(){	
        parent::__construct();    
        $this->read_db = $this->load->database('read', TRUE); //Loading read database.
    }    
	
	
	public function ListDepartments(){			
		$result = $this->read_db->get($this->tbldepartments)->result_array();
		if (!empty($result)){
			return $result;
		}
		return false;
	}
	
	function GetNotificationMessage(){		
		$result = $this->read_db->get_where($this->tabelesetting,array('setting' => 'message'));
		$result = $result->row_array();
		if (!empty($result)){
			return $result;
		}
		return false;
	}
	
	public function ListPriorities(){			
		$result = $this->read_db->get($this->tblpriorities)->result_array();
		if (!empty($result)){
			return $result;
		}
		return false;
	}
	
	public function GetAdditionalFields($department){			
		$result = $this->read_db->get_where($this->tblfields,array('deptid' => $department));
		if (!empty($result)){
			return $result;
		}
		return false;
	}
	
	
	function ListStaffs(){
		$result = $this->read_db->get_where($this->users,array('role_id' => '1'));
		if (!empty($result)){
			return $result;
		}
		return false;		
	}
	
	function GetTicketsOfClient($clientid=0){
		$this -> read_db -> order_by('id', 'asc');
		$result = $this->read_db->get_where($this->tbltickets,array('clientid' => $clientid))->result_array();
		if (!empty($result)){
			return $result;
		}
		return false;
	}
	
	
	function GetTicketDetails($ticketid){
		$this -> read_db -> join('tbldepartments', 'tbldepartments.deptid = tbltickets.department','left');
		$this -> read_db -> join('users', 'tbltickets.clientid = users.id');
		$result = $this -> read_db -> where('tbltickets.id', $ticketid) -> get("tbltickets");
		$array = $result -> row_array();
		$array['body'] = $this -> MakeClickableURL($array['body']);		
		if (!empty($array['body'])){
			return $array;
		}
		return false;
		
	}
	
	function MakeClickableURL($text){
		$text = preg_replace('/(((f|ht){1}tp(s?):\/\/)[-a-zA-Z0-9@:%_\+.~#?&;\/\/=]+)/', '<a href="\\1" target="_blank">\\1</a>', $text);
		$text = preg_replace("/(^|[ \\n\\r\\t])(www\.([a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+)(\/[^\/ \\n\\r]*)*)/", '\\1<a href="http://\\2" target="_blank">\\2</a>', $text);
		$text = preg_replace("/(^|[ \\n\\r\\t])([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,4})/", '\\1<a href="mailto:\\2" target="_blank">\\2</a>', $text);
		return $text;
	}
	
	
	function GetAllAdditionalFieldsofTicket($ticketid)	{
		$result = $this->read_db->get_where($this->tbltickets,array('id' => $ticketid))->result_array();
		if (!empty($result)){
			  
			 if ($result[0]['department']!=''){
				 $fields = $this -> ListAllFields($result[0]['department']);
		         return $fields;
				 }
		}
		return false;
	}
	
	function ListAllFields($deptid)	{
		$result = $this->read_db->get_where($this->tblfields,array('deptid' => $deptid))->result_array();
		if (!empty($result)){
			 return $result;
		}
		return false;
	}
	
	function GetTicketReplies($ticketid)
	{
		$result = $this -> read_db -> order_by('id', 'asc') -> where('ticketid', $ticketid) -> get('tblticketreplies');
		$rows = $result -> result_array();

		for ($i = 0; $i < sizeof($rows); $i++)
		{
			$body = $rows[$i]['body'] ;
			
			
			$body = $this->MakeClickableURL($body) ;
			$rows[$i]['body'] = $body ;
				
			$replierid = $rows[$i]['replierid'];

			if ($rows[$i]['replier'] == 'client')
			{
				$row = $this -> GetClientData($replierid);
				$rows[$i]['email'] = $row['email'];
			}
			else
			{
				$row = $this -> GetStaffDetails($replierid);
				$rows[$i]['firstname'] = $row['firstname'];
				$rows[$i]['lastname'] = $row['lastname'];
			}
		}

		return $rows;
	}
	
	function AddReply($data, $status)
	{
		//$this -> db -> where('ticketid', $data['ticketid']);
		$this -> db -> insert($this->tblticketreplies, $data);
		$this -> db -> where('id', $data['ticketid']) -> update($this->tbltickets, array('status' => $status));
	}
	
	
	function GetClientData($clientid=0){
		$result = $this->read_db->get_where($this->users,array('id' => $clientid))->row_array();
		if (!empty($result)){
			 return $result;
		}
		return false;		
		
	}
	
	
	function GetStaffDetails($id){
		$result = $this->read_db->get_where($this->users ,array('id' => $id))->row_array();
		if (!empty($result)){
			 return $result;
		}
		return false;		
	}
	
	
	/* Admin Area */
	
	function GetTicketsCount(){
	  $statuses = $this -> ListStatuses();
	  if (!empty($statuses) && is_array($statuses) && count($statuses) > 0){	
		foreach ($statuses as $status){
			$this -> read_db -> where('status', $status) -> get('tbltickets');
			$data[$status] = $this -> read_db -> affected_rows();
		  }
		 return $data;
	  }
	  return false;	
	}
	
	function ListStatuses()	{
		$results = $this->read_db->get($this->tblstatus)->result_array();
		if (!empty($results) && is_array($results) && count($results) > 0){
			 foreach ($results as $result){
			 $data[] = $result['status'];
		    }
		  return $data;
		}
		return false;	
	}
	
	
	function ListTickets($status = '')
	{
		if ($status)
		{
			$this -> read_db -> where("status", $status);
		}

		$select = array(
			'tbltickets.id',
			'LEFT(subject,30) AS subject',
			'status',
			'department',
			'clientid',
			'priority',
			'created',
			'deptname',
			'email'
		);
		$this -> read_db -> select($select);

		$this -> read_db -> order_by('tbltickets.id', 'desc');
		$this -> read_db -> join('tbldepartments', 'tbldepartments.deptid = tbltickets.department','left');
		$this -> read_db -> join('users', 'tbltickets.clientid = users.id','left');
		$result = $this -> read_db -> get('tbltickets');
		return $result -> result_array();
	}
	
	
	function GetStatuses(){
		$result = $this->read_db->get($this->tblstatus)->result_array();
		if (!empty($result)){
			return $result;
		}
		return false;
	}
	
	
	function getMailSettings(){
		$result = $this->db->get_where($this->settings,array('module' => 'email'))->result();		
		if (!empty($result)){
			return $result;
		}
		  return false;		
		}
	
	function Addticket($data){
		$this -> db -> insert($this->tbltickets, $data);
		return $this -> db -> insert_id();		
	}
	
	function change_status($id){
		$this -> db -> where('id', $id) -> update($this->tblticketreplies, array('status' => '1'));	
	}
		
		
	function is_read($id){
		$this -> db -> where('id', $id) -> update($this->tbltickets, array('is_read' => '1'));	
	}
	
	function get_unreadmails($clientId ='0'){
		$this->read_db->select('tbltickets.*,tblticketreplies.*,UNIX_TIMESTAMP(time) as time ,tblticketreplies.body as replybody');	
		$this -> read_db -> join('tblticketreplies', 'tbltickets.id = tblticketreplies.ticketid');
		$this -> read_db -> where('tbltickets.clientid', $clientId);
		$this -> read_db -> where('tblticketreplies.replierid != ',$clientId);
		$this -> read_db -> where('tblticketreplies.status','0');		
		$result = $this -> read_db ->get('tbltickets');
		$result = $result ->result();
		//echo $this->db->last_query();die;
		return $result;
	}
	
	
	function CloseTicket($ticketid){
		$data = array(
			'status' => 'closed',
			'additional' => ''
		);
		$this -> db -> where('id', $ticketid) -> update('tbltickets', $data);
	}

	function DeleteTicket($ticketid){
		$this -> db -> where('id', $ticketid) -> delete($this->tbltickets);
		$this -> db -> where('ticketid', $ticketid) -> delete($this->tblticketreplies);
	}	
	
	function AddDepartment($data)
	{
		$this -> db -> insert($this->tbldepartments, $data);
		return $this -> db -> insert_id();

	}
	
	function EditDepartment($data)
	{
		$this -> db -> where("deptid", $data['deptid']);
		$this -> db -> update($this->tbldepartments, $data);
	}

	function DeleteDepartment($deptid){
		$this -> db -> where('deptid', $deptid) -> delete($this->tbldepartments);
	}
	
	function searchTickets($id='',$subject='',$status='',$sort_by='',$sort_type='',$clientid){
		if($id!='')
		$this -> db -> like('id', $id);
		
		if($subject!='')
		$this -> db -> like('subject', $subject);
		
		if($status!='')
		$this -> db -> like('status', $status);
		
		if(($sort_by!='') && ($sort_type!=''))
		$this -> db -> order_by($sort_by, $sort_type);
		
		
		$this->db->where('clientid',$clientid);
		$result = $this -> db ->get($this->tbltickets);
		$result = $result ->result_array();
		//echo $this->db->last_query();
		return $result;
	}
	
	
	function sortTickets($id='',$subject='',$status='',$field='',$sort_type='',$clientid){
		
		if($id!='')
		$this -> db -> like('id', $id);
		
		if($subject!='')
		$this -> db -> like('subject', $subject);
		
		if($status!='')
		$this -> db -> like('status', $status);
		
		
		if($field !='')
		$this -> db -> order_by($field, $sort_type);		
				
		$this->db->where('clientid',$clientid);
		$result = $this -> db ->get($this->tbltickets);
		$result = $result ->result_array();
		//echo $this->db->last_query();
		return $result;
	}
	
	function check_deptname($deptname=''){		
		$result = $this->db->get_where($this->tbldepartments,array('deptname' => $deptname))->row_array();
		if (!empty($result)){
			return true;
		}
		  return false;		
		}
	
	function get_admin_unreadmails($clientId ='0'){
		$this->db->select('tblticketreplies.ticketid,UNIX_TIMESTAMP(time) as time ,tblticketreplies.body as replybody,users.display_name');	
		$this -> db -> join('tblticketreplies', 'tbltickets.id = tblticketreplies.ticketid');		
		$this -> db -> join('users', 'tblticketreplies.replierid = users.id');
		$this -> db -> order_by('tblticketreplies.id', 'asc');		
		$this -> db -> where('tblticketreplies.replierid != ',$clientId);
		$this -> db -> where('tblticketreplies.status','0');		
		$result = $this -> db ->get('tbltickets');
		$result = $result ->result();
		//echo $this->db->last_query();die;
		return $result;
	}
	
	function admin_mail($clientId ='1'){
		$this->db->select($this->tbltickets.'.id as ticketid,'.$this->tbltickets.'.subject,'.$this->tbltickets.'.body as replybody,'.$this->users.'.display_name,UNIX_TIMESTAMP(created) as time');	
		$result = $this -> db -> join('users', 'tbltickets.clientid = users.id');
		$this -> db -> where($this->tbltickets.'.is_read','0');
		$this -> db -> where($this->tbltickets.'.clientid != ',$clientId);
		$result = $this -> db ->get($this->tbltickets);
		$result = $result ->result();
		
		if (!empty($result)){
			 return $result;
		}
		return false;
	}	
				
				
 
}
