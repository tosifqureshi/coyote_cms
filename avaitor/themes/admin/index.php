<?php ob_start();
	Assets::add_js( array( 'bootstrap.min.js', 'jwerty.js'), 'external', true);
	echo theme_view('partials/_header'); ?>
	<div class="container-fluid">
		
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center">
				 <h3 class="text-themecolor"><?php echo (!empty($page_title) ? $page_title : '');?></h3>     
			</div>
		</div>
		
		<?php if (!empty($error)): ?>
			<div class="alert alert-error">
				<a class="close" data-dismiss="alert">×</a>
				<?php echo $error; ?>
			</div>
		<?php endif; ?>
			<?php echo Template::message(); ?>
			<?php echo Template::generate(); ?>
	</div>

<?php echo theme_view('partials/_footer'); ?>
