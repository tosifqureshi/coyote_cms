//~ jQuery.noConflict();


jQuery(document).ready(function(){
	
	
	//Check All Feature
	$(".check-all").click(function(){
		$("table input[type=checkbox]").attr('checked', $(this).is(':checked'));
	});
	
	//~ $('.link_menu').click(function(){		
		//~ var target = $(this).attr('id');
			//~ //alert("test"+target);		
			//~ jQuery.cookie("new_target", target, { path: '/ops/' });
	//~ });
	
	
	 //~ setTimeout(function(){		 	
		//~ if(jQuery.cookie('new_target')) {
			//~ var current_url = window.location.href;
			//~ //alert("current_url: "+current_url+"New Target: "+jQuery.cookie('new_target'));		
			//~ if(jQuery.cookie('new_target'))
			//~ {	
				//~ var id = jQuery.cookie('new_target');
				//~ jQuery('#admin_dashboard').removeClass('active'); 
				//~ //$('#'+id).parent().addClass('active');
				//~ $('#'+id).parent().parent().css('display','block');
				//~ jQuery('#'+id).closest('ul').css('display','block');
				//~ jQuery('#'+id+' a').css('background','none repeat scroll 0 0 #D7D7D7');
				//~ jQuery('#'+id).parent().parent().addClass('active');
			//~ }
		//~ }
	//~ },1000);
	
	
	
	//prettyPrint();			//syntax highlighter
	//~ mainwrapperHeight();
	//~ responsive();
	
	
	// animation
	/*if(jQuery('.contentinner').hasClass('content-dashboard')) {
		var anicount = 4;	
		jQuery('.leftmenu .nav-tabs > li').each(function(){										   
			jQuery(this).addClass('animate'+anicount+' fadeInUp');
			anicount++;
		});
		
		jQuery('.leftmenu .nav-tabs > li a').hover(function(){
			jQuery(this).find('span').addClass('animate0 swing');
		},function(){
			jQuery(this).find('span').removeClass('animate0 swing');
		});
		
		jQuery('.logopanel').addClass('animate0 fadeInUp');
		jQuery('.datewidget, .headerpanel').addClass('animate1 fadeInUp');
		jQuery('.searchwidget, .breadcrumbwidget').addClass('animate2 fadeInUp'); 
		jQuery('.plainwidget, .pagetitle').addClass('animate3 fadeInUp');
		jQuery('.maincontent').addClass('animate4 fadeInUp');
	}*/
	
	// widget icons dashboard
	/*if(jQuery('.widgeticons').length > 0) {
		jQuery('.widgeticons a').hover(function(){
			jQuery(this).find('img').addClass('animate0 bounceIn');
		},function(){
			jQuery(this).find('img').removeClass('animate0 bounceIn');
		});	
	}*/


	// adjust height of mainwrapper when 
	// it's below the document height
	function mainwrapperHeight() {
		var windowHeight = jQuery(window).height();
		var mainWrapperHeight = jQuery('.mainwrapper').height();
		var leftPanelHeight = jQuery('.leftpanel').height();
		if(leftPanelHeight > mainWrapperHeight)
			jQuery('.mainwrapper').css({minHeight: leftPanelHeight});	
		if(jQuery('.mainwrapper').height() < windowHeight)
			jQuery('.mainwrapper').css({minHeight: windowHeight});
	}
	
	function responsive() {
		
		var windowWidth = jQuery(window).width();
		
		// hiding and showing left menu
		if(!jQuery('.showmenu').hasClass('clicked')) {
			
			if(windowWidth < 960)
				hideLeftPanel();
			else
				showLeftPanel();
		}
		
		// rearranging widget icons in dashboard
		if(windowWidth < 768) {
			if(jQuery('.widgeticons .one_third').length == 0) {
				var count = 0;
				jQuery('.widgeticons li').each(function(){
					jQuery(this).removeClass('one_fifth last').addClass('one_third');
					if(count == 2) {
						jQuery(this).addClass('last');
						count = 0;
					} else { count++; }
				});	
			}
		} else {
			if(jQuery('.widgeticons .one_firth').length == 0) {
				var count = 0;
				jQuery('.widgeticons li').each(function(){
					jQuery(this).removeClass('one_third last').addClass('one_fifth');
					if(count == 4) {
						jQuery(this).addClass('last');
						count = 0;
					} else { count++; }
				});	
			}
		}
	}
	
	// when resize window event fired
	//~ jQuery(window).resize(function(){
		//~ mainwrapperHeight();
		//~ responsive();
	//~ });
	
	// dropdown in leftmenu
	jQuery('.leftmenu .dropdown > a').click(function(){
		if(!jQuery(this).next().is(':visible'))
			jQuery(this).next().slideDown('fast');
		else
			jQuery(this).next().slideUp('fast');	
		return false;
	});
	
	// hide left panel
	function hideLeftPanel() {
		jQuery('.leftpanel').css({marginLeft: '-260px'}).addClass('hide');
		jQuery('.rightpanel').css({marginLeft: 0});
		jQuery('.mainwrapper').css({backgroundPosition: '-260px 0'});
		jQuery('.footerleft').hide();
		jQuery('.footerright').css({marginLeft: 0});
	}
	
	// show left panel
	function showLeftPanel() {
		jQuery('.leftpanel').css({marginLeft: '0px'}).removeClass('hide');
		jQuery('.rightpanel').css({marginLeft: '260px'});
		jQuery('.mainwrapper').css({backgroundPosition: '0 0'});
		jQuery('.footerleft').show();
		jQuery('.footerright').css({marginLeft: '260px'});
	}
	
	// show and hide left panel
	jQuery('.showmenu').click(function() {
		jQuery(this).addClass('clicked');
		if(jQuery('.leftpanel').hasClass('hide'))
			showLeftPanel();
		else
			hideLeftPanel();
		return false;
	});
	
	// transform checkbox and radio box using uniform plugin
	//~ if(jQuery().uniform)
		//~ jQuery('input:checkbox, input:radio, select.uniformselect').uniform();
	
	
	// show/hide widget content or widget content's child	
	if(jQuery('.showhide').length > 0 ) {
		jQuery('.showhide').click(function(){
			var t = jQuery(this);
			var p = t.parent();
			var target = t.attr('href');
			target = (!target)? p.next() :	p.next().find('.'+target);
			t.text((target.is(':visible'))? 'View Source' : 'Hide Source');
			(target.is(':visible'))? target.hide() : target.show(100);
			return false;
		});
	}
	
	
	// check all checkboxes in table
	/*if(jQuery('.checkall').length > 0) {
		jQuery('.checkall').click(function(){
			var parentTable = jQuery(this).parents('table');										   
			var ch = parentTable.find('tbody input[type=checkbox]');										 
			if(jQuery(this).is(':checked')) {
			
				//check all rows in table
				ch.each(function(){ 
					jQuery(this).attr('checked',true);
					jQuery(this).parent().addClass('checked');	//used for the custom checkbox style
					jQuery(this).parents('tr').addClass('selected'); // to highlight row as selected
				});
							
			
			} else {
				
				//uncheck all rows in table
				ch.each(function(){ 
					jQuery(this).attr('checked',false); 
					jQuery(this).parent().removeClass('checked');	//used for the custom checkbox style
					jQuery(this).parents('tr').removeClass('selected');
				});	
				
			}
		});
	}*/
	
	
	// delete row in a table
	/*if(jQuery('.deleterow').length > 0) {
		jQuery('.deleterow').click(function(){
			var conf = confirm('Continue delete?');
			if(conf)
				jQuery(this).parents('tr').fadeOut(function(){
					jQuery(this).remove();
					// do some other stuff here
				});
			return false;
		});	
	}*/
	
	
	// dynamic table
	/*if(jQuery('#dyntable').length > 0) {
		jQuery('#dyntable').dataTable({
			"sPaginationType": "full_numbers",
			"aaSortingFixed": [[0,'asc']],
			"fnDrawCallback": function(oSettings) {
				jQuery.uniform.update();
			}
		});
	}*/
	
	
	/////////////////////////////// ELEMENTS.HTML //////////////////////////////
	
	
	// tabbed widget
	//jQuery('#tabs, #tabs2').tabs();
	
	// accordion widget
	//jQuery('#accordion, #accordion2').accordion({heightStyle: "content"});
	
	
	// color picker
	/*if(jQuery('#colorpicker').length > 0) {
		jQuery('#colorSelector').ColorPicker({
			onShow: function (colpkr) {
				jQuery(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				jQuery(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				jQuery('#colorSelector span').css('backgroundColor', '#' + hex);
				jQuery('#colorpicker').val('#'+hex);
			}
		});
	}*/

	
	// date picker
	if(jQuery('#datepicker').length > 0)
		jQuery( "#datepicker" ).datepicker();
		
		
	/*date picker for offer Start : added by P */
	
	$(".fade.in").fadeOut( 10000 );
	
	 //$('[data-toggle="tooltip"]').tooltip();   
	/*
	if(jQuery('#start_date').length > 0)  {
		jQuery( "#start_date" ).datepicker({
			dateFormat: 'yy-mm-dd'
		}).on("change", function() {
			var start_date 	=  $("#start_date").val();
			var sDate = new Date(start_date);
			var nowTemp = new Date();
			var today = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
			if(start_date != '' && today > sDate) {
				
				alert("Start date should be greater than Today's date");
				$( "#start_date" ).val('');
				$( "#end_date" ).val('');
			} else {
				checkBeacon();
			}	
		  });
	}
	
	if(jQuery('#end_date').length > 0)  {
		jQuery( "#end_date" ).datepicker({
			dateFormat: 'yy-mm-dd'
		}).on("change", function() {
			var start_date 	=  $("#start_date").val();
			var end_date 	=  $("#end_date").val();
			var sDate = new Date(start_date);
			var eDate = new Date(end_date);
			if(start_date != '' && end_date != '' && sDate > eDate) {
				alert("Please ensure that the End Date is greater than or equal to the Start Date.");
				$( "#end_date" ).val('');
			} else {
				checkBeacon();
			}
		});
	}
	*/
	//~ if(jQuery('#timepicker1').length > 0)  { 
		//~ jQuery('#timepicker1').datetimepicker({
			//~ //pick12HourFormat: true
		//~ });
	//~ }
	//~ if(jQuery('#timepicker2').length > 0)  { 
		//~ jQuery('#timepicker2').datetimepicker({
			//~ //pick12HourFormat: true
		//~ });
	//~ }

	
/* 
| ------------------------------------------------------------
| Beacon offer date time picker
| ------------------------------------------------------------
*/ 
	
	/*date picker for offer End : added by P */
	
	// growl notification
	/*if(jQuery('#growl').length > 0) {
		jQuery('#growl').click(function(){
			jQuery.jGrowl("Hello world!");
		});
	}*/
	
	// another growl notification
	/*if(jQuery('#growl2').length > 0) {
		jQuery('#growl2').click(function(){
			var msg = "This notification will live a little longer.";
			jQuery.jGrowl(msg, { life: 5000});
		});
	}*/

	// basic alert box
	/*if(jQuery('.alertboxbutton').length > 0) {
		jQuery('.alertboxbutton').click(function(){
			jAlert('This is a custom alert box', 'Alert Dialog');
		});
	}*/
	
	// confirm box
	/*if(jQuery('.confirmbutton').length > 0) {
		jQuery('.confirmbutton').click(function(){
			jConfirm('Can you confirm this?', 'Confirmation Dialog', function(r) {
				jAlert('Confirmed: ' + r, 'Confirmation Results');
			});
		});
	}*/
	
	// promptbox
	/*if(jQuery('.promptbutton').length > 0) {
		jQuery('.promptbutton').click
		(function(){
			jPrompt('Type something:', 'Prefilled value', 'Prompt Dialog', function(r) {
				if( r ) alert('You entered ' + r);
			});
		});
	}*/
	
	// alert with html
	/*if(jQuery('.alerthtmlbutton').length > 0) {
		jQuery('.alerthtmlbutton').click(function(){
			jAlert('You can use HTML, such as <strong>bold</strong>, <em>italics</em>, and <u>underline</u>!');
		});
	}*/
	
	// sortable list
	/*if(jQuery('#sortable').length > 0)
		jQuery("#sortable").sortable();*/
	
	// sortable list with content-->
	/*if(jQuery('#sortable2').length > 0) {
		jQuery("#sortable2").sortable();
		jQuery('.showcnt').click(function(){
			var t = jQuery(this);
			var det = t.parents('li').find('.details');
			if(!det.is(':visible')) {
				det.slideDown();
				t.removeClass('icon-arrow-down').addClass('icon-arrow-up');
			} else {
				det.slideUp();
				t.removeClass('icon-arrow-up').addClass('icon-arrow-down');
			}
		});
	}*/
	
	// tooltip sample
	/*if(jQuery('.tooltipsample').length > 0)
		jQuery('.tooltipsample').tooltip({selector: "a[rel=tooltip]"});
		
	jQuery('.popoversample').popover({selector: 'a[rel=popover]', trigger: 'hover'});*/
	
	
	
	///// MESSAGES /////	
	
	/*if(jQuery('.mailinbox').length > 0) {
		
		// star
		jQuery('.msgstar').click(function(){
			if(jQuery(this).hasClass('starred'))
				jQuery(this).removeClass('starred');
			else
				jQuery(this).addClass('starred');
		});*/
		
		//add class selected to table row when checked
		/*jQuery('.mailinbox tbody input:checkbox').click(function(){
			if(jQuery(this).is(':checked'))
				jQuery(this).parents('tr').addClass('selected');
			else
				jQuery(this).parents('tr').removeClass('selected');
		});*/
		
		// trash
		/*if(jQuery('.msgtrash').length > 0) {
			jQuery('.msgtrash').click(function(){
				var c = false;
				var cn = 0;
				var o = new Array();
				jQuery('.mailinbox input:checkbox').each(function(){
					if(jQuery(this).is(':checked')) {
						c = true;
						o[cn] = jQuery(this);
						cn++;
					}
				});
				if(!c) {
					alert('No selected message');	
				} else {
					var msg = (o.length > 1)? 'messages' : 'message';
					if(confirm('Delete '+o.length+' '+msg+'?')) {
						for(var a=0;a<cn;a++) {
							jQuery(o[a]).parents('tr').remove();	
						}
					}
				}
			});
		}
	}*/

	
	// change layout
	/*jQuery('.skin-layout').click(function(){
		jQuery('.skin-layout').each(function(){ jQuery(this).parent().removeClass('selected'); });
		if(jQuery(this).hasClass('fixed')) {
			jQuery('.mainwrapper').removeClass('fullwrapper');
			if(jQuery('.stickyheaderinner').length > 0) jQuery('.stickyheaderinner').removeClass('wideheader');
			jQuery.cookie("skin-layout", 'fixed', { path: '/' });
		} else {
			jQuery('.mainwrapper').addClass('fullwrapper');
			if(jQuery('.stickyheaderinner').length > 0) jQuery('.stickyheaderinner').addClass('wideheader');
			jQuery.cookie("skin-layout", 'wide', { path: '/' });
		}
		return false;
	});*/
	
	// load selected layout from cookie
	/*if(jQuery.cookie('skin-layout')) {
		var layout = jQuery.cookie('skin-layout');
		if(layout == 'fixed') {
			jQuery('.mainwrapper').removeClass('fullwrapper');
			if(jQuery('.stickyheaderinner').length > 0) jQuery('.stickyheaderinner').removeClass('wideheader');
		} else {
			jQuery('.mainwrapper').addClass('fullwrapper');
			if(jQuery('.stickyheaderinner').length > 0) jQuery('.stickyheaderinner').addClass('wideheader');
		}	
	}*/
	
	
	// change skin color
	/*jQuery('.skin-color').click(function(){
		var s = jQuery(this).attr('href');
		if(jQuery('#skinstyle').length > 0) {
			if(s!='default') {
				jQuery('#skinstyle').attr('href','css/style.'+s+'.css');	
				jQuery.cookie('skin-color', s, { path: '/' });
			} else {
				jQuery('#skinstyle').remove();
				jQuery.cookie("skin-color", '', { path: '/' });
			}
		} else {
			if(s!='default') {
				jQuery('head').append('<link id="skinstyle" rel="stylesheet" href="css/style.'+s+'.css" type="text/css" />');
				jQuery.cookie("skin-color", s, { path: '/' });
			}
		}
		return false;
	});*/
	
	// load selected skin color from cookie
	/*if(jQuery.cookie('skin-color')) {
		var c = jQuery.cookie('skin-color');
		if(c) {
			jQuery('head').append('<link id="skinstyle" rel="stylesheet" href="css/style.'+c+'.css" type="text/css" />');
			jQuery.cookie("skin-color", c, { path: '/' });
		}
	}*/
	
	
//$('#user_search_bar').tooltip();
	
	
});


/* 
| ------------------------------------------------------------
| Initialize mce editor in textarea
| ------------------------------------------------------------
*/ 
tinymce.init({
	mode: "specific_textareas",
	editor_selector: "mceEditor",
	height: '300px',
	width: '770px',
	theme_advanced_resizing: true,
	plugins: ['link'],
});


/* 
| ------------------------------------------------------------
| Manage store's beacon availability
| ------------------------------------------------------------
*/ 
$(document).delegate("#beacon_store_id1","change",function(e){
	var store_id = $(this).val();
	var beacon_offer_id = $('#beacon_offer_id').val();
	$.ajax ({     
			type: "POST",
			data : {store_id:store_id,beacon_offer_id:beacon_offer_id},
			url: BASEURL+'backend/vip_offer/manage_beacon_availability',
			async: false,
			success: function(data){ 
				if(data != 0 && data != 1) {
					var arr = data.split('_');
					$('#beacon_id').val(arr[0]);
					$('#beacon_code').val(arr[1]);
					$('#beacon_error').html('');
				} else if(data == 0) {
					$('#beacon_error').html('Already associated with other beacon offer.');
					$('#beacon_id').val('');
					$('#beacon_code').val('');
				} else {
					$('#beacon_id').val('');
					$('#beacon_code').val('');
					$('#beacon_error').html('No Beacon associate with this store.');
				}
				
			}, 
			error: function(error){
				alert(error);
			}
		});
});

/* 
| ------------------------------------------------------------
| Show bootstrap image popup on listing view 
| ------------------------------------------------------------
*/ 
$(document).delegate( "#imageresource", "click", function(e) {
	$('#myModalLabel').html($(this).attr('title'));
	$('#imagepreview').attr('src', $(this).attr('src')); // here asign the image to the modal when the user click the enlarge link
	$('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});

/* 
| ------------------------------------------------------------
| Manage updation status for selected entity
| ------------------------------------------------------------
*/ 
function updateStatus(url) {
	bootbox.confirm("Are you sure want to change the status?", function(result) {
		if(result==true) {
			$('#loader1').fadeIn();	
			location.href = url;
		}
	});
}

/* 
| ------------------------------------------------------------
| Manage delete status for selected entity
| ------------------------------------------------------------
*/ 
function removeData(url) {
	
	bootbox.confirm("Are you sure want to delete this?", function(result) {
		if(result==true) {
			$('#loader1').fadeIn();	
			location.href = url;
		}
	});
}

/* 
| ------------------------------------------------------------
| Initialize image pickers
| ------------------------------------------------------------
*/ 

$("#co_image-picker_1").click(function() {
	$("input[name='image_1']").click();
});
$("#co_image-picker_2").click(function() {
	$("input[name='image_2']").click();
});
$("#co_image-picker_3").click(function() {
	$("input[name='image_3']").click();
});

	
/* 
| ------------------------------------------------------------
| Manage images uploading from picker
| ------------------------------------------------------------
*/ 	
function read_url_image_co(input) {
	//alert(input.id);
	if(input.id == 'image_1') {
		var pickerId= '#co_image-picker_1';
		var fieldId	= '#image_1';
		$("#remove_btn_1").show();
	} else if(input.id == 'image_2') {
		var pickerId	= '#co_image-picker_2';
		var fieldId	= '#image_2';
		$("#remove_btn_2").show();
	} else if(input.id == 'image_3') {
		var pickerId	= '#co_image-picker_3';
		var fieldId	= '#image_3';
		$("#remove_btn_3").show();
	}
	if (input.files && input.files[0]) {
		var file = input.files[0];
		var size = file.size/1000000; // get image size in mb;
		if(size <= 3) {
			var imageType = /image.*/;
			if (file.type.match(imageType)) {
				var reader = new FileReader();
				reader.onload = function (e) {
					//alert(pickerId);
					$(pickerId).attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				$(fieldId).val("");
				bootbox.alert("File not supported!");
			}
		} else {
			$(fieldId).val("");
			bootbox.alert("File size should be less then 3MB!");
		}
	}
}

/* 
| ------------------------------------------------------------
| Function used to remove uploaded image
| ------------------------------------------------------------
*/ 	
function removeImage(image_type,id,url,file_name) {
	bootbox.confirm("Are you sure want to delete this image?", function(result) {
		if(result==true) {
			// unset image data
			$("#co_image-picker_"+image_type).attr('src', BASEURL+'uploads/upload.png');
			$.ajax ({
				type: "POST",
				data : {id:id,image_type:image_type,file_name:file_name},
				url: BASEURL+url,
				async: false,
				success: function(data){
					if(data) {
						var file = $('#co_image-picker_'+image_type).attr('src',BASEURL+'uploads/upload.png');
						$("#remove_btn_"+image_type).hide();
					}
				}, 
				error: function(error){
					bootbox.alert(error);
				}
			});
		}
	});
}

/* 
| ------------------------------------------------------------
| Function used to check date validation on form submit
| ------------------------------------------------------------
*/ 	
$( "#add_competition_form,#add_loyalty_form" ).submit(function( event ) {
		// get form input date values
		var start_date = new Date($('#start_date').val());
		var end_date = new Date($('#end_date').val());
		var today = new Date($('#current_date').val());
		// check validations
		if(start_date != '' && today > end_date) {
			bootbox.alert("End date should be greater than today\'s date.");
			$( "#end_date" ).val('');
		} else if(start_date != '' && end_date != '' && start_date > end_date) {
			bootbox.alert("Please ensure that the end date is greater than or equal to the start date.");
			$( "#start_date" ).val('');
		} else {
			return true;
		}
		event.preventDefault();
	});

/* 
| ------------------------------------------------------------
| Function used to redirect for user email filteration
| ------------------------------------------------------------
*/
$('#search_email_btn').click(function() {
	var email_search = $('#email_search').val();
	window.location.href = BASEURL+'admin/settings/users?email='+email_search;
});

