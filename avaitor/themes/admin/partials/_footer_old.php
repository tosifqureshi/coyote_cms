</div><!--contentinner-->
</div><!--maincontent-->

</div><!--mainright-->
<!-- END OF RIGHT PANEL -->

<div class="clearfix"></div>




</div><!--mainwrapper-->



<!--footer class="container-fluid ">
  <p class="pull-right">
    Executed in {elapsed_time} seconds, using {memory_usage}.
    <br/>
  </p>
</footer-->


<div id="debug"><!-- Stores the Profiler Results --></div>

<?php echo Assets::js(); ?>
<script type="text/javascript">var BASEURL = "<?php echo base_url();?>";</script>

<script type="text/javascript" src="<?php echo Template::theme_url('js/prettify/prettify.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/jquery-1.9.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/jquery-migrate-1.1.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/jquery-ui-1.9.2.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/jquery.flot.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/jquery.flot.resize.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/bootstrap-datetimepicker.pt-BR.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/jquery.cookie.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/bootbox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/js/custom.js'); ?>"></script>


<script src="<?php echo site_url('avaitor/themes/admin/js/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" charset="utf-8">
jQuery('#package_category_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 3 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#users_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#book_category_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 2 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#pages_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 1 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#organize_category_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#book_package_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 5 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#product_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 8, "desc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 5,6,7,9 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#sales_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#books_sales_detail_tbl').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 2, "desc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#location_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 0,6 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#location_zones_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#advertise_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 1,6 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#slider_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 1 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#coupon_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 2 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#giftcards_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 0, "asc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 6 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#best_seller_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 1, "desc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#signup_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 1, "desc" ]],
	"sPaginationType": "full_numbers"
});
jQuery('#user_feedback_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 1, "desc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#blog_list_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 2, "desc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 3 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#author_list_table').dataTable({
	"bJQueryUI": true,
	//"aaSorting": [[ 2, "desc" ]],
	//"aaSorting": [[ 0, "desc" ]],
	//"aaSorting": [[ 6, "desc" ]],
	"aaSorting": [],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 7 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#author_packagelist_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 3, "desc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#message_inbox_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 3, "desc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 4 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#message_outbox_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 3, "desc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#calculation_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 3, "desc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 7 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#customers_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 4, "desc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 5 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#orders_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 6, "desc" ]],
	"aoColumnDefs": [ { 'bSortable': false, 'aTargets': [ 0,9 ] } ],
	"sPaginationType": "full_numbers"
});

jQuery('#package_order_table').dataTable({
	"bJQueryUI": true,
	"aaSorting": [[ 6, "desc" ]],
	"sPaginationType": "full_numbers"
});

jQuery('#manual_approval').dataTable({
	//"aaSorting": [[ 6, "desc" ]],
	 "aaSorting": [],
	"sPaginationType": "full_numbers"
});
/*
$(function() {
	$(document).tooltip({
		position: {
			my: "center bottom-20",
			at: "center top",
			using: function(position, feedback) {
				$(this).css(position);
				$("<div>")
				.addClass("arrow")
				.addClass(feedback.vertical)
				.addClass(feedback.horizontal)
				.appendTo(this);
			}
		}
	});
});

commented by P
*/ 

$(document).ready(function(){  
	//set the starting bigestHeight variable  
	var biggestHeight = 0;  
	//check each of them  
	$('.equal_height').each(function(){  
	//alert('test123');
			//if the height of the current element is  
			//bigger then the current biggestHeight value  
			if($(this).height() > biggestHeight){  
					//update the biggestHeight with the  
					//height of the current elements  
					biggestHeight = $(this).height();  
			}  
	});  
	//when checking for biggestHeight is done set that  
	//height to all the elements  
	$('.equal_height').height(biggestHeight);  
		
		
	$("#user_edit_form").validate({
		rules: {
			email: {required: true,email: true},
			username: {required: true},
			display_name: {required: true},
			
			
		},
		messages: {
			email: {required:"<span style='color:red;'><?php echo lang('email_error_res'); ?></span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>"},
			username: {required:"<span style='color:red;'>Please enter the enable date</span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>"},
			display_name: {required:"<span style='color:red;'>Please enter the disable date</span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			
		}
	});
	
	$("#author_details_edit").validate({
		rules: {
			firstname: {required: true,rangelength: [2, 30]},
			lastname: {required: true,rangelength: [2, 30]},
			email: {required: true, email: true},
			confirm_email: { required: true, email: true, equalTo: '#email1'},
			password: {required: true, minlength: 8},
			pass_confirm: {required: true, equalTo: '#password3'},
			address:{required: true},
			country_id:{required:true},
			zone:{required:true,number: true},
			phone:{required:true,alphaNumeric: true},
			city:{required:true},
			postcode:{required:true,number: true,rangelength: [2, 15]},
			challenge_question:{required:true},
			challenge_answer:{required:true},
			firstname_arabic:{required:true},
			lastname_arabic:{required:true},
			accomodation:{required:true},
			nationality:{required:true},
			identity:{alphaNumeric: true},
			dob:{required:true, date: true},
			publish_status:{required:true},
			qualification:{required:true},
			distribution:{required:true},
			attachments:{accept: true},
			upload_image:{image: true}
			
		},
		messages: {
			firstname: {required:"<span style='color:red;'><?php echo lang('first'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			lastname: {required:"<span style='color:red;'><?php echo lang('last'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			email: {required:"<span style='color:red;'><?php echo lang('email_error_res'); ?></span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>"},
			confirm_email: {required:"<span style='color:red;'><?php echo lang('confirm_email_regis'); ?></span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>",equalTo:"<span style='color:red;'><?php echo lang('not_confirm_email'); ?></span>"},
			password: {required:"<span style='color:red;'><?php echo lang('login_error_pass'); ?></span>",minlength:"<span style='color:red;'><?php echo lang('pass3'); ?></span>"},
			pass_confirm: {required:"<span style='color:red;'><?php echo lang('con_pass'); ?></span>", equalTo:"<span style='color:red;'><?php echo lang('not_confirm_password'); ?></span>"},
			address:{required:"<span style='color:red;'><?php echo lang('address_required'); ?></span>"},
			country_id:{required:"<span style='color:red;'><?php echo lang('country_required'); ?></span>"},
			zone:{required:"<span style='color:red;'><?php echo lang('state_required'); ?></span>",number:"<span style='color:red;'><?php echo lang('number_numeric'); ?></span>"},
			phone:{required:"<span style='color:red;'><?php echo lang('phone_required'); ?></span>",alphaNumeric:"<span style='color:red;'><?php echo lang('phone_alphanumeric'); ?></span>"},
			city:{required:"<span style='color:red;'><?php echo lang('city_required'); ?></span>"},
			postcode:{required:"<span style='color:red;'><?php echo lang('zip_required'); ?></span>",number:"<span style='color:red;'><?php echo lang('number_numeric'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			challenge_question:{required:"<span style='color:red;'><?php echo lang('challange_que_required'); ?></span>"},
			challenge_answer:{required:"<span style='color:red;'><?php echo lang('challange_ans_required'); ?></span>"},
			firstname_arabic:{required:"<span style='color:red;'><?php echo lang('bf_first_name_arerror'); ?></span>"},
			lastname_arabic:{required:"<span style='color:red;'><?php echo lang('bf_last_name_arerror'); ?></span>"},
			accomodation:{required:"<span style='color:red;'><?php echo lang('bf_accomodation_error'); ?></span>"},
			nationality:{required:"<span style='color:red;'><?php echo lang('bf_nationality_error'); ?></span>"},
			identity:{alphaNumeric:"<span style='color:red;'><?php echo lang('passport_alphanumeric'); ?></span>"},
			dob:{required:"<span style='color:red;'><?php echo lang('us_dob_error1'); ?></span>",date:"<span style='color:red;'><?php echo lang('date_of_birth_valid'); ?></span>"},
			publish_status:{required:"<span style='color:red;'><?php echo lang('Publish_book_status_required'); ?></span>"},
			qualification:{required:"<span style='color:red;'><?php echo lang('qualification_required'); ?></span>"},
			distribution:{required:"<span style='color:red;'><?php echo lang('distribution_required'); ?></span>"},
			attachments: {accept: "<span style='color:red;'><?php echo lang('us_attachments_error1'); ?></span>"},
			upload_image: {image: "<span style='color:red;'><?php echo lang('us_photo_error1'); ?></span>"}
		}
	}); 
		
	$("#banner_slider").validate({
		rules: {
			title: {required: true,rangelength: [2, 30]},
			enable_on: {required: true,date: true},
			disable_on: {required: true,date: true},
			image: {required: true,image: true},
			
		},
		messages: {
			title: {required:"<span style='color:red;'>Please enter the title</span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			enable_on: {required:"<span style='color:red;'>Please enter the enable date</span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>"},
			disable_on: {required:"<span style='color:red;'>Please enter the disable date</span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			image: {required:"<span style='color:red;'>Please select the image</span>",image:"<span style='color:red;'>Please Select jpg,png,jpeg format for image</span>"}
		}
	});
	
	$("#banner_slider1").validate({
		rules: {
			title: {required: true,rangelength: [2, 30]},
			enable_on: {required: true,date: true},
			disable_on: {required: true,date: true},
			
			
		},
		messages: {
			title: {required:"<span style='color:red;'>Please enter the title</span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			enable_on: {required:"<span style='color:red;'>Please enter the enable date</span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>"},
			disable_on: {required:"<span style='color:red;'>Please enter the disable date</span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			
		}
	});
	
	
	$("#box_advertise").validate({
         ignore: [],
		rules: {
			title: {required: true,rangelength: [2, 30]},
			enable_on: {required: true},
			disable_on: {required: true,notequaldate: '#enable_on'},
			box_image: {required: true}
		},
		messages: {
			title: {required:"<span style='color:red;'>Please Enter The Title</span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			enable_on: {required:"<span style='color:red;'>Please Enter The Enable Date</span>"},
			disable_on: {required:"<span style='color:red;'>Please Enter The Disable Date</span>",notequaldate:"<span style='color:red;'><?php echo 'disable date should be greater than enable date'; ?></span>"},
            box_image: {required:"<span style='color:red;'>Please select image for advertisment</span>"}
		}
	});
	
	
	$("#add_gift_card_form").validate({
		rules: {
			to_name: {required: true,rangelength: [2, 30]},
			to_email: {required: true,email: true},
			from: {required: true,rangelength: [2, 30]},
			beginning_amount: {required: true,number: true},
			expiry_date: {required: true}
		},
		messages: {
			to_name: {required:"<span style='color:red;'><?php echo lang('add_giftcard_name_error'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			to_email: {required:"<span style='color:red;'><?php echo lang('add_recipent_email_error'); ?></span>",email:"<span style='color:red;'><?php echo lang('add_giftcard_email_error'); ?></span>"},
			from: {required:"<span style='color:red;'><?php echo lang('add_giftcard_sendername_error'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>"},
			beginning_amount: {required:"<span style='color:red;'><?php echo lang('add_giftcard_amount_error'); ?></span>",number:"<span style='color:red;'><?php echo lang('add_giftcard_number_error'); ?></span>"},
			expiry_date: {required:"<span style='color:red;'><?php echo 'Please enter expirt date for gift card'; ?></span>"}
		}
	});
	
	
	//phone: {required:"<span style='color:red;'><?php echo lang('phone_required'); ?></span>",alphanumericnew:"<span style='color:red;'><?php echo lang('passport_alphanumeric'); ?></span>"},
	$("#customer_add_address_form").validate({
		rules: {
			firstname: {required: true,rangelength: [2, 30],notnumeric:true},
			lastname: {required: true,rangelength: [2, 30],notnumeric:true},
			email: {required: true,email:true},
			phone: {required: true},
			address1: {required: true},
			city: {required: true},
			zone_id: {required: true},
			zip: {required: true,number:true}
		},
		messages: {
			firstname: {required:"<span style='color:red;'><?php echo lang('first'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>",notnumeric:"<span style='color:red;'><?php echo lang('firstname_charcter'); ?></span>"},
			lastname: {required:"<span style='color:red;'><?php echo lang('last'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>",notnumeric:"<span style='color:red;'><?php echo lang('firstname_charcter'); ?></span>"},
			email: {required:"<span style='color:red;'><?php echo lang('email_error_res'); ?></span>",email:"<span style='color:red;'><?php echo lang('valid'); ?></span>"},
			phone: {required:"<span style='color:red;'><?php echo lang('phone_required'); ?></span>"},
			address1: {required:"<span style='color:red;'><?php echo lang('address_required'); ?></span>"},
			city: {required:"<span style='color:red;'><?php echo lang('city_required'); ?></span>"},
			zone_id: {required:"<span style='color:red;'><?php echo lang('state_required'); ?></span>"},
			zip: {required:"<span style='color:red;'><?php echo lang('zip_required'); ?></span>",number:"<span style='color:red;'><?php echo lang('number_numeric'); ?></span>"}
		}
	});
	
	$("#admin_country_form").validate({
		rules: {
			name: {required: true,rangelength: [2, 30],notnumeric:true},
			name_ar: {required: true,rangelength: [2, 30],notnumeric:true},
			iso_code_2: {required: true},
			iso_code_3: {required: true},
			postcode_required: {required: true,number:true}	
			
		},
		messages: {
			name: {required:"<span style='color:red;'><?php echo lang('country_name_error'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>",notnumeric:"<span style='color:red;'><?php echo lang('firstname_charcter'); ?></span>"},
			name_ar: {required:"<span style='color:red;'><?php echo lang('country_name_arabic_error'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>",notnumeric:"<span style='color:red;'><?php echo lang('firstname_charcter'); ?></span>"},
			iso_code_2: {required:"<span style='color:red;'><?php echo lang('country_iso_code2_error'); ?></span>"},
			iso_code_3: {required:"<span style='color:red;'><?php echo lang('country_iso_code3_error'); ?></span>"},
			postcode_required: {required:"<span style='color:red;'><?php echo lang('country_code_error'); ?></span>",number:"<span style='color:red;'><?php echo lang('number_numeric'); ?></span>"}
			
		}
	});
	
	$("#admin_countryzone_form").validate({
		rules: {
			name: {required: true,rangelength: [2, 30],notnumeric:true},
			name_ar: {required: true,rangelength: [2, 30],notnumeric:true},
			code: {required: true,number:true}	
			
		},
		messages: {
			name: {required:"<span style='color:red;'><?php echo lang('countryzone_name_error'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>",notnumeric:"<span style='color:red;'><?php echo lang('firstname_charcter'); ?></span>"},
			name_ar: {required:"<span style='color:red;'><?php echo lang('countryzone_name_arabic_error'); ?></span>",rangelength:"<span style='color:red;'><?php echo lang('range_length_validation'); ?></span>",notnumeric:"<span style='color:red;'><?php echo lang('firstname_charcter'); ?></span>"},
			code: {required:"<span style='color:red;'><?php echo lang('countryzone_code_error'); ?></span>",number:"<span style='color:red;'><?php echo lang('number_numeric'); ?></span>"}
			
		}
	});
	
	$("#add_pages_form").validate({
		rules: {
			title: {required: true},
			title_ar: {required: true}
		},
		messages: {
			title: {required:"<span style='color:red;'><?php echo lang('add_page_title_error'); ?></span>"},
			title_ar: {required:"<span style='color:red;'><?php echo lang('add_page_title_ar_error'); ?></span>"},
		}
	});
	
	$("#add_link_form").validate({
		rules: {
			title: {required: true},
			title_ar: {required: true},
			url: {required: true}
		},
		messages: {
			title: {required:"<span style='color:red;'><?php echo lang('add_page_title_error'); ?></span>"},
			title_ar: {required:"<span style='color:red;'><?php echo lang('add_page_title_ar_error'); ?></span>"},
			url: {required:"<span style='color:red;'><?php echo lang('add_link_url_error'); ?></span>"}
		}
	});
	
	$("#send_notification_form").validate({
		rules: {
			subject: {required: true},
			content: {required: true}
		},
		messages: {
			subject: {required:"<span style='color:red;'><?php echo lang('send_notification_subject_error'); ?></span>"},
			content: {required:"<span style='color:red;'><?php echo lang('send_notification_content_error'); ?></span>"}
		}
	});
	
	$("#admin_notes_form").validate({
		rules: {
			notes: {required: true}
		},
		messages: {
			notes: {required:"<span style='color:red;'><?php echo lang('view_order_adminnotes_error'); ?></span>"}
		}
	});
	
	$("#add_zone_area_form").validate({
		rules: {
			code: {required: true}
		},
		messages: {
			code: {required:"<span style='color:red;'><?php echo lang('no_zone_code'); ?></span>"}
		}
	});
	
	$("#add_package_form").validate({
		rules: {
			package_title: {required: true},
			package_title_ar: {required: true},
			amount: {required: true},
			package_category: {required: true,number:true}
		},
		messages: {
			package_title: {required:"<span style='color:red;'><?php echo lang('package_packcage_error'); ?></span>"},
			package_title_ar: {required:"<span style='color:red;'><?php echo lang('package_packcage_ar_error'); ?></span>"},
			amount: {required:"<span style='color:red;'><?php echo lang('package_packcage_amount_error'); ?></span>"},
			package_category: {required:"<span style='color:red;'><?php echo lang('package_packcage_amount_error'); ?></span>",number:"<span style='color:red;'><?php echo lang('package_packcage_validamount_error'); ?></span>"}
		}
	});
	
	$("#add_package_category_form").validate({
		rules: {
			package_category_title: {required: true},
			package_category_title_ar: {required: true},
			package_category_price: {required: true,number:true}
		},
		messages: {
			package_category_title: {required:"<span style='color:red;'><?php echo lang('package_title_error'); ?></span>"},
			package_category_title_ar: {required:"<span style='color:red;'><?php echo lang('package_title_ar_error'); ?></span>"},
			package_category_price: {required:"<span style='color:red;'><?php echo lang('package_price_error'); ?></span>",number:"<span style='color:red;'><?php echo lang('package_price_numeric_error'); ?></span>"}
		}
	});
	
	$("#book_calculator_form").validate({
		rules: {
			book_type: {required: true},
			book_cover_type: {required: true},
			book_pricing_system: {required: true},
			trim_size: {required: true},
			page_range: {required: true},
			book_price: {required: true,number:true},
			royalty: {required: true}
		},
		messages: {
			book_type: {required:"<span style='color:red;'><?php echo lang('book_type_required'); ?></span>"},
			book_cover_type: {required:"<span style='color:red;'><?php echo lang('book_cover_type_required'); ?></span>"},
			book_pricing_system: {required:"<span style='color:red;'><?php echo lang('book_pricing_system_required'); ?></span>"},
			trim_size: {required:"<span style='color:red;'><?php echo lang('trim_size_required'); ?></span>"},
			page_range: {required:"<span style='color:red;'><?php echo lang('page_range_required'); ?></span>"},
			book_price: {required:"<span style='color:red;'><?php echo lang('page_range_required'); ?></span>",number:"<span style='color:red;'><?php echo lang('book_price_error'); ?></span>"},
			royalty: {required:"<span style='color:red;'><?php echo lang('book_royalty_error'); ?></span>"}
		}
	});
	
	$("#add_blog_form").submit(function(){
		
		var title =$.trim($(".blog_title").val());
		var title_ar =$.trim($(".blog_title_ar").val());
		var messag = tinyMCE.get('blog_content_ar').getContent();
        var my_editor = tinymce.get('blog_content_ar');
		var message = $.trim($(my_editor.getBody()).text());
		var sub =0;
		var message_error=0;
		var firstChar = message.substr(0, 5);
		var search =firstChar.search("&n");
		if(search>=1)
		{
			var message_error=1;
		}
	
		if(message=="")
		{
			$('#blog_description_error').addClass('show').removeClass('hide');
			var sub=1;
		}
		else
		{
			$('#blog_description_error').addClass('hide').removeClass('show');
		}

		if(title=="")
		{
			//$('#blog_title_error').addClass('show').removeClass('hide');
			//var sub=1;
		}
		else
		{
			//$('#blog_title_error').addClass('hide').removeClass('show');
		}
		
		if(title_ar=="")
		{
			$('#blog_title_ar_error').addClass('show').removeClass('hide');
			var sub=1;
		}
		else
		{
			$('#blog_title_ar_error').addClass('hide').removeClass('show');
		}
		
		if(sub==1)
		{
			return false;
		}
		else
		{
			return true;
		}
	});
	
		$( ".blog_title" ).keyup(function( event ) {		
			var title =$.trim($(".blog_title").val());
			if(title!="")
			{
				$('#blog_title_error').addClass('hide').removeClass('show');
			}
			else
			{
				$('#blog_title_error').addClass('show').removeClass('hide');
			}
		});
		
		$( ".blog_title_ar" ).keyup(function( event ) {
			var title =$.trim($(".blog_title_ar").val());
			if(title!="")
			{
				$('#blog_title_ar_error').addClass('hide').removeClass('show');
			}
			else
			{
				$('#blog_title_ar_error').addClass('show').removeClass('hide');
			}
		});
	
});  

</script>
<style>
  .ui-tooltip, .arrow:after {
    background: black;
    border: 2px solid white;
  }
  .ui-tooltip {
    padding: 10px 20px;
    color: white;
    border-radius: 20px;
    font: bold 14px "Helvetica Neue", Sans-Serif;
    text-transform: uppercase;
    box-shadow: 0 0 7px black;
  }
  .arrow {
    width: 70px;
    height: 16px;
    overflow: hidden;
    position: absolute;
    left: 50%;
    margin-left: -35px;
    bottom: -16px;
  }
  .arrow.top {
    top: -16px;
    bottom: auto;
  }
  .arrow.left {
    left: 20%;
  }
  .arrow:after {
    content: "";
    position: absolute;
    left: 20px;
    top: -20px;
    width: 25px;
    height: 25px;
    box-shadow: 6px 5px 9px -9px black;
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    tranform: rotate(45deg);
  }
  .arrow.top:after {
    bottom: -20px;
    top: auto;
  }
</style>



</body>
</html>
