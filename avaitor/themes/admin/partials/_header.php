<?php
$RTR = & load_class('Router', 'core');
$module_name = $RTR->fetch_module(); //get current module
$this->config->set_item('language', 'english');
$this->session->unset_userdata('site_lang');
$this->session->set_userdata('site_lang', 'english');

//Assets::add_js(array('fancybox/jquery.fancybox.pack.js','jquery.confirm.js','modernizr.js','jquery.validate.js'));?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo Assets::css(null, true); ?>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo Template::theme_url('assets/images/favicon.png'); ?>">
	<title>
		<?php echo $this->settings_lib->item('site.title') ?> 
	</title>
   
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Template::theme_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo Template::theme_url('vendor/css/style.css'); ?>" rel="stylesheet">
	<link href="<?php echo Template::theme_url('assets/css/custom.css'); ?>" rel="stylesheet">
	<link href="<?php echo Template::theme_url('vendor/css/fontawesome-all.css'); ?>" rel="stylesheet">
	<link href="<?php echo Template::theme_url('vendor/css/colors/blue.css'); ?>" rel="stylesheet">
	
	<link href="<?php echo Template::theme_url('assets/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet">
	<!-- Date picker plugins css -->
    <link href="<?php echo Template::theme_url('assets/css/bootstrap-datepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo Template::theme_url('assets/css/bootstrap-timepicker.min.css'); ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo Template::theme_url('assets/css/daterangepicker.css'); ?>" type="text/css" /> 
	<!-- Add JS -->
	<script>var BASEURL = '<?php echo base_url();?>';</script>
	<script src="<?php echo Template::theme_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo Template::theme_url('js/tinymce/tinymce.min.js'); ?>"></script>
	<!-- Date Picker Plugin JavaScript -->
	<script type="text/javascript" src="<?php echo Template::theme_url('assets/js/moment.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo Template::theme_url('assets/js/bootstrap-material-datetimepicker.js'); ?>"></script>
	
</head>	
	
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- <div class="loader1" id="loader1"><div class="hexdots-loader"> Loading… </div></div> -->

    <div class="loader1" id="loader1">
    <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="circle" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg>
    </div>


	<!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
		  <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo site_url('backend/dashboard') ?>">
                      
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo Template::theme_url('assets/images/logo.png'); ?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo Template::theme_url('assets/images/logo.png'); ?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>         
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">                               
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo Template::theme_url('assets/images/users/1.jpg'); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
											<div class="u-img"><img src="<?php echo Template::theme_url('assets/images/users/1.jpg'); ?>" alt="user"></div>
                                            <div class="u-text">
                                                <h4><?php echo (isset($current_user->display_name) && !empty($current_user->display_name)) ? $current_user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $current_user->username : $current_user->email); ?> </h4>
                                                <p class="text-muted"><?php echo $current_user->email;?></p><a href="<?php echo site_url(SITE_AREA . '/settings/users/edit') ?>" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>                            
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-power-off"></i> <?php echo lang('bf_action_logout_header') ?></a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header> 
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
		<!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
		<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
						<li>
                            <a class="has-arrow" href="<?php echo site_url('backend/dashboard') ?>" aria-expanded="false"><span class="hide-menu"><?php echo lang('acart_menu_dashboard'); ?> </span></a>
						</li>
						<li>
                            <a class="has-arrow" href="<?php echo base_url().'admin/settings/campaign';?>" aria-expanded="false"><span class="hide-menu"><?php echo lang('acart_menu_campaigns'); ?></span></a>
                        </li>
                        <li>
                            <a class="has-arrow" href="" aria-expanded="false"><span class="hide-menu"><?php echo lang('acart_menu_offers'); ?><i class="fa fa-caret-down"></i></span></a>
                            <ul aria-expanded="false" class="collapse">
                                
                                <li><a class="" href="<?php echo base_url().'backend/offer' ?>" aria-expanded="false"><?php echo lang('acart_menu_in_store_offers'); ?></a></li>
                                <li><a href="<?php echo base_url().'backend/vip_offer/simple' ?>"><?php echo lang('acart_menu_simple_offers'); ?></a></li> 
                                 <li><a href="<?php echo base_url().'backend/vip_offer/beacon';?>"><?php echo lang('acart_menu_beacon_normal_offers'); ?></a></li>
                                <li><a href="<?php echo base_url().'backend/vip_offer/fallback';?>"><?php echo lang('acart_menu_beacon_fallback_offers'); ?></a></li>                                    
                            </ul>
                        </li>
						<!--
                        <li >
                          <a class="has-arrow" href="" aria-expanded="false"><span class="hide-menu"><?php //echo lang('acart_menu_beacon_management');?><i class="fa fa-caret-down"></i></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php //echo site_url('backend/beacon') ?>"><?php //echo lang('acart_menu_beacons'); ?></a></li>
                                <li><a href="<?php //echo base_url().'backend/vip_offer/beacon';?>"><?php //echo lang('acart_menu_beacon_normal_offers'); ?></a></li>
                                <li><a href="<?php //echo base_url().'backend/vip_offer/fallback';?>"><?php //echo lang('acart_menu_beacon_fallback_offers'); ?></a></li>                          
                            </ul>
                        </li>                       
						-->
                       
                        <li>
                            <a class="has-arrow " href="" aria-expanded="false"><span class="hide-menu"><?php echo lang('acart_menu_promotion'); ?><i class="fa fa-caret-down"></i></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo base_url().'backend/promotion' ?>"><?php echo lang('acart_menu_app_promotion'); ?></a></li>
                                <li><a href="<?php echo base_url().'backend/kiosk_promotion' ?>"><?php echo lang('acart_menu_kiosk_promotion'); ?></a></li>
                                <li><a href="<?php echo base_url().'backend/web_promotion' ?>"><?php echo lang('acart_menu_web_promotion'); ?></a></li> 
								<li><a href="<?php echo base_url().'admin/settings/edm';?>">EDM</a></li>
								<li><a href="<?php echo base_url().'admin/settings/targetpush/targetnotification' ?>"><?php echo lang('target_notification'); ?></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow " href="<?php echo base_url().'backend/event' ?>" aria-expanded="false"><span class="hide-menu"><?php echo lang('acart_menu_event'); ?></span></a>                          
                        </li>
						<li>
							<a class="has-arrow " href="<?php echo base_url().'backend/loyalty' ?>" aria-expanded="false"><span class="hide-menu"> <?php echo lang('acart_menu_loyalty'); ?> </span></a> 
                        </li>
						
                        <li>
                            <a class="has-arrow " href="" aria-expanded="false"><span class="hide-menu"><?php echo lang('acart_menu_competition_management'); ?><i class="fa fa-caret-down"></i></span></a>
                            <ul aria-expanded="false" class="collapse">
								<li><a href="<?php echo base_url().'admin/settings/competition';?>"><?php echo lang('acart_menu_competition'); ?></a></li>
								<li><a href="<?php echo base_url().'admin/settings/competition/luckydraw';?>"><?php echo lang('acart_menu_lucky_draw'); ?></a></li>
								<li><a href="<?php echo base_url().'admin/settings/competition/leaderboard';?>"><?php echo lang('acart_menu_leader_board_winners'); ?></a></li>
							 </ul>
						 </li> 
						 
						<!-- points and rewards menu start here -->
						<?php if($this->session->userdata('role_id') == 1) { ?>
							<li>
								<a class="has-arrow " href="" aria-expanded="false"> <?php echo lang('menu_points_n_reward'); ?><i class="fa fa-caret-down"></i></a>
								<ul aria-expanded="false" class="collapse">
									<li><a href="<?php echo base_url().'admin/settings/reward/loyaltycards';?>"><?php echo lang('menu_reward_cards'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/reward/create_catalogue';?>"><?php echo lang('menu_reward_catalogue'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/reward/rewardCampaign';?>"><?php echo lang('menu_reward_campaign'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/reward/config';?>"><?php echo lang('menu_reward_config'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/reward';?>"><?php echo lang('menu_reward_rule'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/reward/reward_pool';?>"><?php echo lang('menu_reward_transaction'); ?></a></li>
								</ul>
							</li>
						<?php } ?>
						
						<!-- points and rewards menu end here -->      
						<?php if($this->session->userdata('role_id') == 1) { ?>
							<li>
								<a class="has-arrow " href="" aria-expanded="false"><?php echo lang('menu_kiosk_management'); ?><i class="fa fa-caret-down"></i></a>
								<ul aria-expanded="false" class="collapse">
									<li><a href="<?php echo base_url().'admin/settings/kioskcompany';?>"><?php echo lang('menu_kiosk_companies'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/kioskoutlet';?>"><?php echo lang('menu_kiosk_outlets'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/kiosktill';?>"><?php echo lang('menu_kiosk_tills'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/kiosklicence';?>"><?php echo lang('menu_kiosk_licences'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/kioskversion';?>"><?php echo lang('menu_kiosk_exe_versions'); ?></a></li>
									<li><a href="<?php echo base_url().'backend/settings/kiosk_settings';?>"><?php echo lang('menu_kiosk_settings'); ?></a></li>
									
								</ul>
							</li>
						<?php }?>
						
						<?php if($this->session->userdata('role_id') == 1) { ?>
							<li>
								<a class="has-arrow " href="" aria-expanded="false"><?php echo lang('acart_menu_settings'); ?><i class="fa fa-caret-down"></i></a>
								<ul aria-expanded="false" class="collapse">
									<li><a href="<?php echo site_url(SITE_AREA . '/settings/users/userlisting') ?>"><?php echo lang('acart_menu_users'); ?></a></li>
									<li><a href="<?php echo site_url(SITE_AREA . '/settings/permissions/') ?>"><?php echo lang('acart_menu_permissions'); ?></a></li>
									<li><a href="<?php echo site_url(SITE_AREA . '/settings/roles/') ?>"><?php echo lang('acart_menu_roles'); ?></a></li>
									<li><a href="<?php echo site_url('backend/beacon') ?>"><?php echo lang('acart_menu_beacons'); ?></a></li>
									<li><a href="<?php echo base_url().'backend/beacon/settings';?>"><?php echo lang('acart_menu_beacon_settings'); ?></a></li>
									<li><a href="<?php echo base_url().'backend/settings/tablet_settings';?>"><?php echo lang('acart_menu_tablet_settings'); ?></a></li>
									<li><a href="<?php echo base_url().'admin/settings/deviceaddress' ?>"><?php echo lang('acart_menu_deviceaddress'); ?></a></li>
									<li><a href="<?php echo base_url().'backend/beacon/cart_settings';?>"><?php echo lang('acart_menu_cart_settings'); ?></a></li>
									<li><a href="<?php echo base_url().'backend/email_template/' ?>">Email Template</a></li>
                                    <li><a href="<?php echo base_url().'admin/settings/imagelibrary' ?>">Image Library</a></li>
                                    <li><a href="<?php echo base_url().'admin/settings/pages' ?>">Pages</a></li>
								</ul>
							</li>
						<?php } ?>
					</ul>
                       
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
          <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
       <div class="page-wrapper">
            
