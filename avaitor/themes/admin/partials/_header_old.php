<?php
    $RTR = & load_class('Router', 'core');
    $module_name = $RTR->fetch_module(); //get current module
    
    Assets::add_css( array(
		'bootstrap.min.css',
		'bootstrap-responsive.css',
		'fancybox/jquery.fancybox.css',
		'font-awesome.css','dev_custom.css','normalize.css','styleLanguage.css'
	));
    
   Assets::add_js(array('fancybox/jquery.fancybox.pack.js','custome.js','jquery.confirm.js','bootstrap.min.js','modernizr.js','jquery.validate.js','jquery.dataTables.rowReordering.js'));
   
    if($module_name=='acart'){  // check if current module in a cart
        $cart_js = array('jquery-ui.js',
                'file-browser.js',
                'redactor.min.js'
        );
        Assets::add_js($cart_js);
        Assets::add_css('cart_dashboard.css');
    }
     $cart_css = array(
            'file-browser.css',
            'redactor.css',
            'jquery-ui.css'
    );
    Assets::add_css($cart_css);
    
    
	if (isset($shortcut_data) && is_array($shortcut_data['shortcut_keys'])) {
		Assets::add_js($this->load->view('ui/shortcut_keys', $shortcut_data, true), 'inline');
	}
    $this->config->set_item('language', 'english');
	$this->session->unset_userdata('site_lang');
	$this->session->set_userdata('site_lang', 'english');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php //echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> 
	<?php echo $this->settings_lib->item('site.title') ?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex" />
	<?php echo Assets::css(null, true); ?>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
	<!-- Add CSS -->
	<link rel="stylesheet" href="<?php echo Template::theme_url('css/bootstrap-combined.min.css'); ?>" type="text/css" /> 
	<link rel="stylesheet" href="<?php echo Template::theme_url('css/bootstrap-datetimepicker.min.css'); ?>" type="text/css" /> 
	<link rel="stylesheet" href="<?php echo Template::theme_url('css/css/style.default.css'); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo Template::theme_url('js/prettify/prettify.css'); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo Template::theme_url('css/css/pulse.css'); ?>" type="text/css" />
    
	<!-- Add JS -->
	<script src="<?php echo Template::theme_url('js/modernizr-2.5.3.js'); ?>"></script>
	<script src="<?php echo Template::theme_url('js/tinymce/tinymce.min.js'); ?>"></script>
	<script>
		var BASEURL = '<?php echo base_url();?>';
	</script>
    <script src="https://use.fontawesome.com/8618742fb1.js"></script>

	<script src="<?php echo Template::theme_url('js/datatable/jquery.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo Template::theme_url('js/datatable/jquery.dataTables.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo Template::theme_url('js/datatable/jquery-ui.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo Template::theme_url('js/datatable/jquery.dataTables.rowReordering.js');?>" type="text/javascript"></script>
</head>
<body class="desktop">
	<div class="loader1" id="loader1"><div class="hexdots-loader"> Loading… </div></div>


<div class="mainwrapper">

      <!-- START OF LEFT PANEL -->
      <div class="leftpanel">
		
          <h1 title="<?php //echo $this->settings_lib->item('site.title');?>"><?php //echo anchor('/',"<div class='logopanel'></div>" , 'target="_blank" title=""', 'class="brand"'); ?>
         		<div class='logopanel'>	<a href="<?php echo site_url('backend/dashboard') ?>"><img src="<?php echo site_url('uploads/logo.jpg'); ?>" alt="" /></a></div>
          </h1>
        <!--logopanel-->

        <div class="datewidget"><?php echo date('l, M d, Y H:i') ?></div>

		<div class="leftmenu">
			<ul class="nav nav-tabs nav-stacked">
				<!-- <li class="nav-header"><?php echo lang('acart_menu_main_navigation'); ?></li> -->

				<li class="link_menu"  id="dashboard"><a href="<?php echo site_url('backend/dashboard') ?>"><i class="fa fa-dashboard"></i><?php echo lang('acart_menu_dashboard'); ?></a></li>
				<li class="link_menu"  id="campaign"><a href="<?php echo base_url().'admin/settings/campaign';?>"><i class="fa fa-bullhorn"></i><?php echo lang('acart_menu_campaigns'); ?></a></li>				
				<li class="dropdown">
                    <a href=""><i class="fa fa-tags"></i><?php echo lang('acart_menu_offers'); ?></a>
                    <ul class="childmenu">
                        <li class="link_menu"  id="offer"><a href="<?php echo base_url().'backend/offer' ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_in_store_offers'); ?></a></li>
                        <li class="link_menu"  id="simple"><a href="<?php echo base_url().'backend/vip_offer/simple' ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_simple_offers'); ?></a></li>
                    </ul>
                </li>
                <li class="dropdown">
					<a href=""><i class="fa fa-wifi"></i><?php echo lang('acart_menu_beacon_management'); ?></a>
					<ul class="childmenu">
                        <li class="link_menu"  id="beacons"><a href="<?php echo site_url('backend/beacon') ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_beacons'); ?></a></li>
						<li class="link_menu" id="beacon"><a href="<?php echo base_url().'backend/vip_offer/beacon';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_beacon_normal_offers'); ?></a></li>
						<li class="link_menu" id="fallback"><a href="<?php echo base_url().'backend/vip_offer/fallback';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_beacon_fallback_offers'); ?></a></li>
					</ul>
				</li>    
				<li class="link_menu"  id="targetnotification"><a href="<?php echo base_url().'admin/settings/targetpush/targetnotification' ?>"><i class="fa fa-bullseye"></i><?php echo lang('target_notification'); ?></a></li>  
				
				<li class="dropdown">
                    <a href=""><i class="fa fa-bullhorn"></i><?php echo lang('acart_menu_promotion'); ?></a>
                    <ul class="childmenu">
                        <li class="link_menu"  id="promotion"><a href="<?php echo base_url().'backend/promotion' ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_app_promotion'); ?></a></li>
                        <li class="link_menu"  id="kiosk_promotion"><a href="<?php echo base_url().'backend/kiosk_promotion' ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_kiosk_promotion'); ?></a></li>
                    </ul>
                </li>
				          	
				<li class="link_menu"  id="event"><a href="<?php echo base_url().'backend/event' ?>"><i class="fa fa-calendar"></i><?php echo lang('acart_menu_event'); ?></a></li>      	
				<li class="link_menu"  id="loyalty"><a href="<?php echo base_url().'backend/loyalty' ?>"><i class="fa fa-handshake-o"></i><?php echo lang('acart_menu_loyalty'); ?></a></li>
				<li class="link_menu"  id="edm"><a href="<?php echo base_url('admin/settings/edm'); ?>"><i class="fa fa-envelope"></i><?php echo 'Email Direct Marketing'; ?></a></li>
				<!--<li class="link_menu"  id="scratchnwin"><a href="<?php echo base_url().'admin/settings/scratchnwin' ?>"><i class="fa fa-trophy"></i><?php echo lang('acart_menu_scratchnwin'); ?></a></li>-->
			
				<!-- competition menu start here -->
				<li class="dropdown">
					<a href=""><i class="fa fa-trophy"></i><?php echo lang('acart_menu_competition_management'); ?></a>
					<ul class="childmenu">
						<li class="link_menu" id="competition"><a href="<?php echo base_url().'admin/settings/competition';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_competition'); ?></a></li>
						<li class="link_menu" id="luckydraw"><a href="<?php echo base_url().'admin/settings/competition/luckydraw';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_lucky_draw'); ?></a></li>
						<li class="link_menu" id="leaderboard"><a href="<?php echo base_url().'admin/settings/competition/leaderboard';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_leader_board_winners'); ?></a></li>
					
					</ul>
				</li> 
				<!-- competition menu end here -->
				
				<!-- points and rewards menu start here -->
				<?php if($this->session->userdata('role_id') == 1) { ?>
					<li class="dropdown">
						<a href=""><i class="fa fa-money"></i><?php echo lang('menu_points_n_reward'); ?></a>
						<ul class="childmenu nav-tabs-child">
                            <li></li>
                            <li class="link_menu" id="loyaltycards"><a href="<?php echo base_url().'admin/settings/reward/loyaltycards';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_reward_cards'); ?></a></li>
                            <li class="link_menu" id="catalogue"><a href="<?php echo base_url().'admin/settings/reward/create_catalogue';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_reward_catalogue'); ?></a></li>
                            <li class="link_menu" id="campaign"><a href="<?php echo base_url().'admin/settings/reward/rewardCampaign';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_reward_campaign'); ?></a></li>
							<li class="link_menu" id="config"><a href="<?php echo base_url().'admin/settings/reward/config';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_reward_config'); ?></a></li>
                            <li class="link_menu" id="rewardrules"><a href="<?php echo base_url().'admin/settings/reward';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_reward_rule'); ?></a></li>
							<!--<li class="dropdown">
								<a href="" class="child-a"><i class="fa fa-money"></i><?php echo lang('menu_reward_point_rates'); ?></a>
								<ul>
									<li id="earnrate"><a href="<?php echo base_url().'admin/settings/reward/earnrate';?>"><?php echo lang('menu_reward_earn_rate'); ?></a></li>
									<li id="spendrate"><a href="<?php echo base_url().'admin/settings/reward/spendrate';?>"><?php echo lang('menu_reward_spend_rate'); ?></a></li>
								</ul>
							</li>-->
							<li class="link_menu" id="reward_pool"><a href="<?php echo base_url().'admin/settings/reward/reward_pool';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_reward_transaction'); ?></a></li>
							<!--<li class="link_menu" id="reward_customers"><a href="<?php echo base_url().'admin/settings/reward/reward_customers';?>"><i class="fa fa-users "></i><?php echo lang('acart_menu_customers'); ?></a></li>-->
						</ul>
					</li> 
				<?php } ?>
				<!-- points and rewards menu start here -->
				
				<!-- points and rewards menu start here -->
				<?php if($this->session->userdata('role_id') == 1) { ?>
					<li class="dropdown">
						<a href=""><i class="fa fa-tablet"></i><?php echo lang('menu_kiosk_management'); ?></a>
						<ul class="childmenu nav-tabs-child">
                            <li></li>
                            <li class="link_menu" id="kioskcompany"><a href="<?php echo base_url().'admin/settings/kioskcompany';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_kiosk_companies'); ?></a></li>
                            <li class="link_menu" id="kioskoutlet"><a href="<?php echo base_url().'admin/settings/kioskoutlet';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_kiosk_outlets'); ?></a></li>
                            <li class="link_menu" id="kiosktill"><a href="<?php echo base_url().'admin/settings/kiosktill';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_kiosk_tills'); ?></a></li>
							<li class="link_menu" id="kiosklicence"><a href="<?php echo base_url().'admin/settings/kiosklicence';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_kiosk_licences'); ?></a></li>
							<li class="link_menu" id="kioskversion"><a href="<?php echo base_url().'admin/settings/kioskversion';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_kiosk_exe_versions'); ?></a></li>
							<li class="link_menu" id="kiosk_settings"><a href="<?php echo base_url().'backend/settings/kiosk_settings';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('menu_kiosk_settings'); ?></a></li>
						</ul>
					</li> 
				<?php } ?>
				<!-- points and rewards menu start here -->
				
				<?php if($this->session->userdata('role_id') == 1) { ?>
					<li class="dropdown"><a href=""><i class="fa fa-cogs"></i><?php echo lang('acart_menu_settings'); ?></a>
						<ul class="childmenu">
							<li class="link_menu" id="users"><a href="<?php echo site_url(SITE_AREA . '/settings/users/userlisting') ?>" ><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_users'); ?></a></li>
							<li class="link_menu" id="permissions"><a href="<?php echo site_url(SITE_AREA . '/settings/permissions/') ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_permissions'); ?></a></li>
							<li class="link_menu" id="roles" ><a href="<?php echo site_url(SITE_AREA . '/settings/roles/') ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_roles'); ?></a></li>
							<!--<li class="link_menu" id="settings"><a ><i class="fa fa-cogs"></i><?php //echo lang('acart_menu_settings'); ?></a></li>-->
							<li class="link_menu" id="tablet_settings"><a href="<?php echo base_url().'backend/settings/tablet_settings';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_tablet_settings'); ?></a></li>
							<li class="link_menu"  id="deviceaddress"><a href="<?php echo base_url().'admin/settings/deviceaddress' ?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_deviceaddress'); ?></a></li>
                            <li class="link_menu" id="beacon_settings"><a href="<?php echo base_url().'backend/beacon/settings';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_beacon_settings'); ?></a></li>
							<li class="link_menu" id="cart_settings"><a href="<?php echo base_url().'backend/beacon/cart_settings';?>"><i class="fa fa-angle-double-right"></i><?php echo lang('acart_menu_cart_settings'); ?></a></li>
							<li class="link_menu" id="email_template"><a href="<?php echo base_url().'backend/email_template/' ?>"><i class="fa fa-angle-double-right"></i><?php echo "Email Template"; ?></a></li>
							
						</ul>
					</li>             
					<?php /* ?>
					<li class="dropdown"><a href=""><i class="fa fa-th"></i><?php echo lang('acart_menu_content'); ?></a>
						<ul>
							<li class="link_menu" id="pages"><a ><span class="fa fa-book"></span><?php echo lang('acart_menu_pages'); ?></a></li>
							<li class="link_menu" id="language"><a ><span><i class="fa fa-language"></i></span><?php echo lang('manage_language'); ?></a></li>
						</ul>
					</li>
           
					<li class="dropdown"><a href=""><i class="fa fa-database"></i><?php echo lang('acart_menu_database_tools'); ?></a>
						<ul>
						  <li class="link_menu" id="database"><a ><i class="fa fa-wrench"></i><?php echo lang('acart_menu_maintenance'); ?></a></li>
						  <li class="link_menu" id="backups"><a ><i class="fa fa-hdd-o"></i><?php echo lang('acart_menu_backups'); ?></a></li>
						  <li class="link_menu" id="migrations"><a ><i class="fa fa-mail-forward"></i><?php echo lang('acart_menu_migrations'); ?></a></li>
						</ul>
					</li>
					<?php */?>
				<?php } ?>
                <!--<li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-power-off"></i> <?php echo lang('bf_action_logout_header') ?></a></li>-->
			</ul>
		</div><!--leftmenu-->    
	</div><!--mainleft-->
	<!-- END OF LEFT PANEL -->

	<!-- START OF RIGHT PANEL -->
	<div class="rightpanel">
        <div class="headerpanel">
			<a href="" class="showmenu"><i class="fa fa-bars" aria-hidden="true"></i></a>
			<div class="headerright">
				<div class="dropdown userinfo">
					<a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="<?php echo site_url(SITE_AREA . '/settings/users/edit') ?>">Hi, <?php echo (isset($current_user->display_name) && !empty($current_user->display_name)) ? $current_user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $current_user->username : $current_user->email); ?> ! <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#"><?php echo $current_user->username ?>:   <?php e($current_user->email) ?></a></li>
						<!--li><a href="<?php echo site_url(SITE_AREA . '/settings/users/edit') ?>"><span class="icon-edit"></span><?php echo lang('bf_user_settings_header') ?></a></li-->
						<!--<li class="divider"></li>-->
						<div class="btn-logout"><a href="<?php echo site_url('logout'); ?>" role="button" class="btn btn-primary pull-right"><i class="fa fa-power-off"></i> <?php echo lang('bf_action_logout_header') ?></a></div>
					</ul>  
				</div><!--dropdown-->
			</div><!--headerright-->
        </div><!--headerpanel-->

		<div class="maincontent">
			<div class="contentinner content-dashboard">
				<div class="pull-right">
					<?php Template::block('sub_nav', ''); ?>
				</div>
				<!-- <div class="divider10"></div> -->
	<script type="text/javascript">
		$(document).ready(function() {
			var module_id = "<?php echo $this->uri->segment(4);?>";
			var id = "<?php echo $this->uri->segment(3);?>";
			var section = "<?php echo $this->uri->segment(2);?>";
			var _class = "<?php echo $this->uri->segment(1);?>";
			
			// set class as menu id if action not exists 
			if(id=='' && section=='') {
				id = _class;
			}
			// set menu id if core module id exists 
			if(module_id !='') {
				id = module_id;
			}
			
			if(_class == 'backend' && section != 'vip_offer' && section != 'beacon' && section != 'settings') {
				id = section;
			} else if(section == 'beacon' || section == 'fallback') {
				if(id=='settings') {
					id = 'beacon_settings';
				} else if (id == 'cart_settings' ){
					id = 'cart_settings';
				} else {
					id = 'beacons';
				}
			}  else if ( section=='vip_offer' && id=='create' ){
				id = 'beacon';
			}  else if (section=='vip_offer' && id=='create_fallback_offer' ){
				id = 'fallback';
			}
			
			$('#'+id).parent().show();	
			$('#'+id).parent().parent().addClass('active');
			$('#'+id).addClass('active');
            
            $(".showmenu ").click(function(){
                $(".rightpanel").toggleClass("afterclick-rgt");
                $(".leftpanel").toggleClass("afterclick-left");
            });
		});
        
        
		
	</script>
