<?php echo theme_view('parts/_header'); ?>

<div class="container body login-wrapper"> <!-- Start of Main Container -->

<?php

	echo Template::message();
	echo isset($content) ? $content : Template::generate();
?>

<?php echo theme_view('parts/_footer'); ?>
