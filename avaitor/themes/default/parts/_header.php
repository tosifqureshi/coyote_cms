<?php
	// Setup our default assets to load.
	Assets::add_js( 'bootstrap.min.js' );
	Assets::add_css( array('bootstrap.min.css', 'bootstrap-responsive.min.css','noty/buttons.css'));

	$inline  = '$(".dropdown-toggle").dropdown();';
	$inline .= '$(".tooltips").tooltip();';
	$inline .= '$(".login-btn").click(function(e){ e.preventDefault(); $("#modal-login").modal(); });';

// Setup our default assets to load.
    $js_arr = array('noty/jquery.noty.js','noty/promise.js','noty/layouts/bottomRight.js','noty/layouts/bottomLeft.js','noty/layouts/center.js','noty/layouts/bottom.js',
		'noty/themes/default.js');
	Assets::add_js($js_arr);
	
	Assets::add_js( $inline, 'inline' );

	Template::block('header', 'parts/head');

	Template::block('topbar', 'parts/topbar');
	
	echo Template::block('amessage');
?>
