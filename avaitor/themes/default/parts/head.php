<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo ($this->settings_lib->item('site.title')); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!--
	<script src="<?php //echo Template::theme_url('js/modernizr-2.5.3.js') ?>"></script>
	-->
	<?php //echo Assets::css(); ?>
	<!-- Bootstrap Core CSS -->
    <link href="<?php echo Template::theme_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo Template::theme_url('vendor/css/style.css'); ?>" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo Template::theme_url('vendor/css/colors/blue.css'); ?>" id="theme" rel="stylesheet">
    <!-- iPhone and Mobile favicon's and touch icons -->
	<!--
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/apple-touch-icon-114x114.png">
	-->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
	<!--
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-1.7.2.min.js"><\/script>')</script>
	-->

  </head>
<body>
