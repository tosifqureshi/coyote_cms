<?php 

include ("BPOINT.php");

BPOINT\URLDirectory::setBaseURL("reserved","https://www.bpoint.com.au/webapi/v2");	
$credentials = new BPOINT\Credentials("nightowlapi", "Ny0R13s1jV", "5353109496121214",BPOINT\Mode::Live);

$txn = new BPOINT\Transaction();
$cardDetails = new BPOINT\CardDetails();
$order = new BPOINT\Order();
$shippingAddress = new BPOINT\OrderAddress();
$billingAddress = new BPOINT\OrderAddress();
$address = new BPOINT\Address();
$customer = new BPOINT\Customer(); 
$personalDetails = new BPOINT\PersonalDetails(); 
$contactDetails = new BPOINT\ContactDetails();
$order_item_1 = new BPOINT\OrderItem();
$order_recipient_1 = new BPOINT\OrderRecipient();
$fraudScreening = new BPOINT\FraudScreeningRequest();

$txn->setAction(BPOINT\Actions::Payment);
$txn->setCredentials($credentials);
$txn->setAmount(10000);
$txn->setCurrency("AUD");
$txn->setMerchantReference("Merchant Reference");
$txn->setCrn1("My Customer Reference");
$txn->setCrn2("Medium");
$txn->setCrn3("Large");
$txn->setStoreCard(FALSE);
$txn->setSubType("single");
$txn->setType(BPOINT\TransactionType::Internet);

$cardDetails->setCardHolderName("NightOwlUser");
$cardDetails->setCardNumber("5123456789012346");
$cardDetails->setCVN("444");
$cardDetails->setExpiryDate("1117");

$txn->setCardDetails($cardDetails);

$address->setAddressLine1("123 Street");
$address->setCity("Melbourne");
$address->setCountryCode("AUS");
$address->setPostCode("3000");
$address->setState("Vic");

$contactDetails->setEmailAddress("neemaamit@gmail.com");

$personalDetails->setDateOfBirth("1991-01-01");
$personalDetails->setFirstName("John");
$personalDetails->setLastName("Smith");
$personalDetails->setSalutation("Mr");

$billingAddress->setAddress($address);
$billingAddress->setContactDetails($contactDetails);
$billingAddress->setPersonalDetails($personalDetails);

$shippingAddress->setAddress($address);
$shippingAddress->setContactDetails($contactDetails);
$shippingAddress->setPersonalDetails($personalDetails);

$order_item_1->setDescription("an item");
$order_item_1->setQuantity(1);
$order_item_1->setUnitPrice(1000);

$orderItems = array($order_item_1);

$order_recipient_1->setAddress($address);
$order_recipient_1->setContactDetails($contactDetails);
$order_recipient_1->setPersonalDetails($personalDetails);

$orderRecipients = array($order_recipient_1);

$order->setBillingAddress($billingAddress);
$order->setOrderItems($orderItems);
$order->setOrderRecipients($orderRecipients);
$order->setShippingAddress($shippingAddress);
$order->setShippingMethod("boat");

$txn->setOrder($order);

$customer->setCustomerNumber("1234");
$customer->setAddress($address);
$customer->setExistingCustomer(false);
$customer->setContactDetails($contactDetails);
$customer->setPersonalDetails($personalDetails);
$customer->setCustomerNumber("1");
$customer->setDaysOnFile(1);

$txn->setCustomer($customer);

$txn->setTokenisationMode(3);
$txn->setTimeout(93121);

/* TestMode on for dummy transaction */
$txn->setTestMode(TRUE);
$txn->setStoreCard(TRUE);


$response = $txn->submit();


$response =  (array) $response;

$resultResponseKey = array();


foreach($response as $key => $val){
    //echo '<b>KEY -----------------</b>'.print_r($key);
    if(is_object($val)){
        $valNew = (array) $val;
        foreach($valNew as $k1 => $v1){
            $k1 = str_ireplace('BPOINT\\','',$k1);
            $resultResponseKey[$k1] = $v1;    
        }
        
    }else if(is_array($val)){
        foreach($val as $k1 => $v1){
            $k1 = str_ireplace('BPOINT\\','',$k1);
            $resultResponseKey[$k1] = $v1; 
        }
    }else{
        
        $key = str_ireplace('BPOINT\\','',$key);
        $resultResponseKey[$key] = $val;
    }
     
}
echo "<pre>";
echo "<br>";
//print_r($resultResponseKey);

foreach($resultResponseKey as $key => $val){
    if(is_object($val))
    {}else{
        if(!empty($val)){
            echo '<b>'.$key .'</b>--------------'.$val; 
        }
    }
    echo "<br>";
}
?>
