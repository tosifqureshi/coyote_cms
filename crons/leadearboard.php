<?php 
/**
 * Setup cron to manage members leader board points as per product purchase log 
 * 
 */

// Get url of web site with domain name and folder address
$host	 = gethostname();
$host_ip = gethostbyname($host); 

// Host Configurations based on Environment	
$production		= "110.232.114.48";	// PRODUCTION
$testing		= "10.10.10.2";		// TESTING

// Set action url
if($host_ip==$production) {
	$url = 'http://ws01.coyotepos.com.au:81/coyote/services/competition/leaderboardcron'; 
} else if($host_ip==$testing) {
	$url = 'http://cdnsolutionsgroup.com/coyote/services/competition/leaderboardcron'; 
} else {
	$url = 'http://ws01.coyotepos.com.au:81/coyote/services/competition/leaderboardcron'; 
}

// Execute action from curl
if(isset($url)) {
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	echo $output = curl_exec($ch);
	$err = curl_error($ch);
}
?>
