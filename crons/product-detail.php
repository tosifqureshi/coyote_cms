<?php
require_once('/config.php'); 
	$edmId = !empty($_REQUEST['eid'])?$_REQUEST['eid']:0;
	$uId   = !empty($_REQUEST['uid'])?$_REQUEST['uid']:0;
	$prId  = !empty($_REQUEST['pr'])?$_REQUEST['pr']:0;		
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Night-Owl</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body>
	<div class="loader dn">
         <i class="fa fa-refresh fa-spin" style=" font-size:75px;color:#111;position:absolute;top:50%;left:50%;"></i>
    </div>  
    <!-- Navigation -->
    <div class="top-header text-center">
      <img src="img/logo.png">
    </div>
	   <nav class="navbar navbar-expand-lg navbar-dark main-menu ">
		 <div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
			   <ul class="navbar-nav ml-auto">
				  <li class="nav-item active">
					 <a class="nav-link" href="http://nightowl.com.au/stores/">Store
					 <span class="sr-only">(current)</span>
					 </a>
				  </li>
				  <li class="nav-item">
					 <a class="nav-link" href="http://nightowl.com.au/offers/">Offer</a>
				  </li>
				  <li class="nav-item">
					 <a class="nav-link" href="http://nightowl.com.au/our-products/">Our Product</a>
				  </li>
				  <li class="nav-item">
					 <a class="nav-link" href="http://nightowl.com.au/contact/">Contact</a>
				  </li>
			   </ul>
			</div>
		 </div>
	  </nav>
   <!-- Page Content -->
   <div class="container bd-container">
    <div class="card-product">
      <div class="container-fliud">
        <div class="wrapper row">
          <div class="preview col-md-6">
            <div class="preview-pic tab-content">
              <div class="tab-pane active" id="pic-1"><img src="img/banner1.jpg"></div>
              <div class="tab-pane " id="pic-2"><img src="img/banner2.jpg" /></div>
              <div class="tab-pane" id="pic-3"><img src="img/banner3.jpg" /></div>
              <div class="tab-pane" id="pic-4"><img src="img/banner4.jpg" /></div>
            </div>
            <ul class="preview-thumbnail nav nav-tabs">
<!--
              <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="img/banner1.jpg" /></a></li>
              <li><a data-target="#pic-2" data-toggle="tab"><img src="img/banner2.jpg" /></a></li>
              <li><a data-target="#pic-3" data-toggle="tab"><img src="img/banner3.jpg" /></a></li>
              <li><a data-target="#pic-4" data-toggle="tab"><img src="img/banner4.jpg" /></a></li>
            </ul>
-->
          </div>
          <div class="details col-md-6">
            <h3 class="product-title"></h3>
            <div class="rating">
              <div class="stars">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
              </div>
              <span class="review-no"></span>
            </div>
            <p class="product-description"></p>
<!--
            <h5 class="price">current price: <span>$180</span></h5>
            <p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>
            <h6 class="sizes">sizes:
              <span class="size" data-toggle="tooltip" title="small">s</span>
              <span class="size" data-toggle="tooltip" title="medium">m</span>
              <span class="size" data-toggle="tooltip" title="large">l</span>
              <span class="size" data-toggle="tooltip" title="xtra large">xl</span>
            </h6>
            <h6 class="colors">colors:
              <span class="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
              <span class="color green"></span>
              <span class="color blue"></span>
            </h6>

            <div class="action">
              <button class="add-to-cart btn btn-default" type="button">add to cart</button>
              <button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
            </div>
-->
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="main-footer">
    <div class="container">
    <div class="row">
      <div class="col-md-2 menu-footer">
      <h6>Stote</h6>
      <ul class="list-unstyled">
        <li><a href="">Store Locator</a></li>
      </ul>
      </div>
          <div class="col-md-2 menu-footer" >
      <h6>Special Offers</h6>
      <ul class="list-unstyled">
        <li><a href=""> Special Offers </a></li>
        <li><a href="">  Monthly Specials </a></li>
        <li><a href=""> Everyday Low Prices </a></li>
        <li><a href=""> Food Combos </a></li>
      </ul>
      </div>
          <div class="col-md-2 menu-footer">
      <h6>Our Products</h6>
      <ul class="list-unstyled">
        <li><a href=""> Cheap Tuesday </a></li>
        <li><a href=""> FrostBite </a></li>
        <li><a href=""> OwlCafe </a></li>
        <li><a href=""> Food Service </a></li>
      </ul>
      </div>
          <div class="col-md-2 menu-footer">
      <h6>About Us</h6>
      <ul class="list-unstyled">
        <li><a href="">Our History </a></li>
      </ul>
      </div>
          <div class="col-md-2 menu-footer">
      <h6>Franchising</h6>
      <ul class="list-unstyled">
        <li><a href=""> Franchising </a></li>
      </ul>
      </div>
          <div class="col-md-2 menu-footer">
      <h6>Contact Us</h6>
      <ul class="list-unstyled">
         <li><a href="">Contact </a></li>
         <li><a href="">Terms & Conditions </a></li>
         <li><a href="">Facebook </a></li>
      </ul>
      </div>
      </div>
      </div>
      <!-- /.container -->
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<script>
	$.ajax({
		type: "GET",
		dataType: "json",
		url: '<?php echo SERVICE_URL_PATH.'reward/edm_product_detail'; ?>',
		data: {edmId:'<?php echo $edmId; ?>',uId:'<?php echo $uId; ?>',prId:'<?php echo $prId; ?>'},
		beforeSend: function() {
			$('.loader').removeClass('dn');
		},
		success: function(data){
			console.log(data);
		  if(data.result){
			  $('.product-title').text(data.Product.PROD_POS_DESC);
  			  $('.product-description').text(data.Product.Prod_Desc);
  			  //var number = randomBetween(1,4);
  			  var number = Math.floor(Math.random() * 4 + 1);
  			  $('.review-no').text(number+' reviews');
  			  $('.tab-pane').removeClass('active');
  			  $('#pic-'+number).addClass('active');
  			  //bd-container
		  }else{
			  $('.bd-container').html('');
			  $('.bd-container').html(data.html);
		  
		  }
		},
		complete: function() {
        $('.loader').addClass('dn');
		}
	});
	
</script>
