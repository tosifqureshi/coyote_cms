<?php 
/**
 * Setup cron to send notification to on assign reward points
 * 
 */

// Get url of web site with domain name and folder address

$host	 = gethostname();
$host_ip = gethostbyname($host); 

// Host Configurations based on Environment	
$production		= "110.232.114.48";	// PRODUCTION
$testing		= "10.10.10.2";		// TESTING

// Set action url
if($host_ip==$testing) {
	$url = 'http://cdnsolutionsgroup.com/coyotev3/services/reward/sendNotification'; 
} else {
	$url = 'http://ws01.coyotepos.com.au:81/coyote/services/reward/sendNotification'; 
}
$url = 'http://mst.cdnsolutionsgroup.com:84/coyotev3/services/reward/sendNotification';

// Execute action from curl
if(isset($url)) {
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	echo $output = curl_exec($ch);
	$err = curl_error($ch);
}
?>
