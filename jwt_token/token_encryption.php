<?php
require_once 'vendor/autoload.php';

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

if($action == 'encode') {
	$token = (new Builder())->setIssuer('JWT Example')
      ->setAudience('JWT Example')
      ->setId($token_json, true)
      ->setIssuedAt(time())
      ->setExpiration(time() + 3600)
      ->getToken();
     $encoded_licence =  $token; 
     
} else if($action == 'decode') {
	
	$parsedToken = (new Parser())->parse($encoded_token);
	$decoded_licence = $parsedToken->getHeader('jti');
}
 

 
