<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    /**
     * Timestamp : 01-Oct-2015 01:22 PM
     * Copyright : www.cdnsol.com
     */
    final class pushnotifications {

        public $_parents = array();
        public $_data = array();
        
        /// -- Hold Static Connection Object --
        private static $ref;
        /// -- Hold Connection Object --
        
        public function __construct() {
            /// -- Create config instance --
			 $this->_config  = config::getInst();
        }
        
        /**
         * final static env::getInst
         * @access Public
         * @return Object
         */
        final public static function getInst(){
            if(!is_object(pushnotifications::$ref))
            {
                pushnotifications::$ref=new pushnotifications(config::getInst());
            }
            return pushnotifications::$ref;
        }

}

