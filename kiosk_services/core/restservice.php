<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : CDNSOL
 * Email  : admin@cdnsol.com
 * Timestamp : Jul-05 11:54PM
 * Copyright : CDNSOL TEAM
 *
 */

final class restService{

	    /// -- Hold Service Action Request --
	    private $_action;
	    /// -- Hold Service Action Request --
	    private $_actionMethod;
	    /// -- Hold Service token --
	    private $_token;
	    /// -- Hold Service hash key --
	    private $_hash;
	    /// -- Hold Service Action Parameter Request --
	    private $_register;
	    /// -- Hold encoding  --
	    private $_encoding = "UTF-8";
	    /// -- Force UTF encoding  --
	    private $_force_utf_encoding = false;
	    /// -- Hold All error in Array --
	    private $_errorSet=array();
	    /// -- Hold Service Action Request --
	    private $_db;
	    /// -- Hold Static private class members variables --
	    static private $_config=array();

	    private $_leastSupportedAndroidVersion = 1;

	    private $_leastSupportediOSVersion = 1;

	    /**
	    * @method __construct
	    * @see private constructor to protect beign inherited
	    * @access private
	    * @return void
	    */
	    public function __construct()
	    {
			/// -- Log Debug Message --
			$this->_db=env::getInst();
			//log_message("debug","DB Object Initialized!!");
	    }

	    /**
	    * @method Init
	    * @see public Initialization
	    * @access public
	    * @return void
	    */
	    public function init()
	    {
	    	/** Create and write the parameter logs - Start **/
			$this->logMessages($_REQUEST);
			/** Create and write the parameter logs - End **/
			$actionMethodName 	= $_REQUEST["actionMethod"];
			// check authenticated request calling
			$licence_token = (!empty($_SERVER['HTTP_ACCESSCODE'])) ? $_SERVER['HTTP_ACCESSCODE'] : '1';
			$mac_id		   = (!empty($_SERVER['HTTP_MACID'])) ? $_SERVER['HTTP_MACID'] : '1';
			$kiosk_version = (!empty($_SERVER['HTTP_KIOSKVERSION'])) ? $_SERVER['HTTP_KIOSKVERSION'] : '';
			$kiosk_type    = (!empty($_SERVER['HTTP_KIOSKTYPE'])) ? $_SERVER['HTTP_KIOSKTYPE'] : '';
			if(!empty($licence_token) && !empty($mac_id)) {
				$is_request_authorized = true;
				// validate kiosk vesrion availability
				//~ $actionMethod = $this->check_kiosk_version($kiosk_version,$kiosk_type);
				//~ if ($actionMethod == 'check_app_version') {
					//~ $_REQUEST["action"] = 'kiosk';
					//~ $_REQUEST["actionMethod"] = $actionMethod;
				//~ } else {
					//~ // verify input access code
					//~ if($_REQUEST["actionMethod"] != 'validate_kiosk_licence') {
						//~ $is_request_authorized = $this->varify_access_code($licence_token,$mac_id);
					//~ }
				//~ }
				
				// verify input access code
				if($_REQUEST["actionMethod"] != 'my_reward_points') {
					$is_request_authorized = $this->varify_access_code($licence_token,$mac_id);
				}
				if($is_request_authorized) {
					if(isset($_REQUEST["action"]) && $_REQUEST["action"]!="")
					{
						$this->_action = $_REQUEST["action"];
						if(isset($_REQUEST["actionMethod"]) && $_REQUEST["actionMethod"]!="")
						{
							$this->_actionMethod = $_REQUEST["actionMethod"];
						}
						else{
							$this->setErrors("faultActionMethod","Action Method requested Not found");
							log_message("debug",json_encode($this->_errorSet));
						}///--END:FII--
					}
					else{
						$this->setErrors("faultAction","Action requested Not found");
						log_message("debug",json_encode($this->_errorSet));
					}///--END:FII--
				} else {
					$this->setErrors("faultAction","Authorization failed!");
					log_message("debug",json_encode($this->_errorSet));
				}
			} else {
				$this->setErrors("faultAction","Authentication params should not be blank!");
	    		log_message("debug",json_encode($this->_errorSet));
			}
			
	    	
	    }///--END:init --
		
		/**
		 * This function used to authenticate access code
		 * @input licence_token string , mac_id string
		 * @return void
		 * 
		 */
		function varify_access_code($licence_token='',$mac_id='') {
			$encoded_token = $licence_token;
			$action = 'decode';
			// include JWT library
			require_once($_SERVER['DOCUMENT_ROOT'].'/coyotev3/jwt_token/token_encryption.php');
			$decoded_licence = json_decode($decoded_licence);
			if(!empty($decoded_licence)) {
				$decoded_licence = (array)$decoded_licence;
				$decoded_mac_id = (string) $decoded_licence['mac_id'];
			
				if($mac_id == $decoded_licence['mac_id']) {
					// fetch kiosk licence requests
					$sql = "SELECT * FROM ava_kiosk_licence_keys WHERE mac_id = '".$mac_id."' AND till_id = '".$decoded_licence['till_id']."'";
					$result = $this->_db->my_query($sql);
					if ($this->_db->my_num_rows($result) > 0) {
						return true;
					}
				}
			}
			return false;
		}
		
		/**
		 * This function used to check kiosk exe version 
		 * @input kiosk_version string, kiosk_type int
		 * @return string
		 * 
		 */
		function check_kiosk_version($kiosk_version='',$kiosk_type='') {
			$actionMethodName 	= $_REQUEST["actionMethod"];
			if(!empty($kiosk_version) && !empty($kiosk_type)) {
				// fetch latest exe version
				$sql = "SELECT * FROM ava_kiosk_exe_versions WHERE exe_device_type = $kiosk_type ORDER BY exe_version DESC" ;
				$result = $this->_db->my_query($sql);
				if ($this->_db->my_num_rows($result) > 0) {
					$row = $this->_db->my_fetch_object($result);
					if($kiosk_version < $row->exe_version) {
						$actionMethodName = 'check_app_version';
					}
				}
			}
			return $actionMethodName;
		}
		
	    /** Function for log the message - Start **/
	    public function logMessages($PostParameters)
	    {
			$PostParameters = "";
			foreach ($_REQUEST as $key => $value) {
				if($key==='password'||$key==='oldPassword'||$key==='newPasword')
				{   $password='';
					for($i=1;$i<=strlen($value);++$i)
					{
						$password.='*';
					}
					$PostParameters = $PostParameters."$key = $password, ";
				}else{
				     $PostParameters = $PostParameters."$key = $value, ";
			     }
			}

			$Ip = "";
				if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != '')
					$Ip = $_SERVER['HTTP_CLIENT_IP'];
				elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '')
					$Ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '')
					$Ip = $_SERVER['REMOTE_ADDR'];

				log_message("info",$PostParameters);
				log_message("info","Request Type = ".$_SERVER['REQUEST_METHOD']);
				log_message("info","Client IP = ".$Ip);
				log_message("info","Request Agent = ".$_SERVER['HTTP_USER_AGENT']);
		}

	    /**
	    * @method setEncoding
	    * @access public
	    * @param string (UTF-8, ISO-8859-1)
	    * @return void
	    */
	    public function setEncoding($str)
	    {
			$this->encoding = strval($str);
			return true;
		}///--END:setEncoding --

	    /**
	    * @method execute
	    * @see private Initialization
	    * @access public
	    * @return response
	    */
	    public function execute($requestBody = null)
	    {
	    	if(count($this->_errorSet)>0){
	    		/// -- return Error Message --
    			return $this->getErrors($requestBody);
	    	}///--END:FII--

	    	/// -- check if class exists --
	    	if(class_exists($this->_action))
	    	{
	    		/// -- Load Class Object --
	    		$class=loadObject($this->_action);
	    		/// -- Log Debug Message --
				//log_message("debug","Object of Class ".$this->_action." created successfully");

	    	}else{

	    		/// -- Set Error Message --
	    		$this->setErrors("faultClass","Class ".$this->_action." not Found");
	    		/// -- Log Debug Message --
				log_message("debug","Object of Class ".$this->_action." not Found");
	    		/// -- return error message with requestbody type --
	    		return $this->getErrors($requestBody);

	    	}///--END:FII class_exists () --

	    	/// -- if $class is object of requested class --
    		if(is_a($class, $this->_action))
    		{
    			/// -- if  method_exists in class --
    			if(method_exists($class,$this->_actionMethod))
    			{
    				/// -- return response of method --
    				return $this->getResponse($class->{$this->_actionMethod}(),$requestBody);
    			} else{
    				/// -- set error message --
    				$this->setErrors("faultClassMethod","Class ".$this->_action." Method ".$this->_actionMethod." not Found");
    				/// -- return error message with requestbody type --
    				return $this->getErrors($requestBody);
    			}///--END:FII method_exists () --

    		}else{

    			/// -- Set Error Message --
	    		$this->setErrors("faultClass","Object Is not of type ".$this->_action."!!!");
	    		/// -- Log Debug Message --
					log_message("debug","Object Is not of type ".$this->_action."!!!");
	    		/// -- return error message with requestbody type --
	    		return $this->getErrors($requestBody);

    		}///--END:FII is_a () --
	    }///--END:execute --

	    /**
	    * @method setError
	    * @see private setError($actionTag,$actionValue)
	    * @access private
	    * @params $actionTag as STRING
	    * @params $actionValue as STRING
	    * @return void
	    */
	    private function setErrors($actionTag,$actionValue)
	    {
	    	$this->_errorSet[$actionTag]=$actionValue;
	    }

	    /**
	    * @method getErrors
	    * @see public getErrors($requestBody="")
	    * @access public
	    * @params $requestBody as OPTIONAL
	    * @return Error Message as _errorSet with requestBody
	    */
	    public function getErrors($requestBody="")
	    {
			if(strtolower($requestBody)=="json")
			{
				return json_encode($this->_errorSet);
			}
			else if(strtolower($requestBody)=="xml")
			{
				// Initiate the class
				$xml = loadObject("xml");
				// Set the array so the class knows what to create the XML from
				$xml->setArray((array)$object);
				// return the XML to screen
				return $xml->outputXML('return');
			}else{
				return $this->_errorSet;
			}
			///--END:FII--
	    }

	     /**
	    * @method getResponse
	    * @see public getResponse($object,$requestBody="")
	    * @access public
	    * @params $object as OBJECT
	    * @params $requestBody as response Type
	    * @return Response Message as requestBody
	    */
	    public function getResponse($object,$requestBody="")
	    {
			if(strtolower($requestBody)=="json")
			{
				return $this->getJsonResponse($object);
			}
			else if(strtolower($requestBody)=="xml")
			{
				return $this->getXMLResponse($object);
			}else{
				return $object;
			}///--END:FII--
	    }///--END:FII getResponse () --


	    /**
	    * @method setForceUTFEncoding
	    * @access public
	    * @params Boolean
	    * @return set force utf encoding
	    */
	    public function setForceUTFEncoding($setForce = false) {
			$this->_force_utf_encoding = $setForce;
		}///--END:getXMLResponse () --


	    /**
	    * @method getXMLResponse
	    * @access private
	    * @params object (Array)
	    * @return Returns xml output
	    */
	    private function getXMLResponse($object) {
			// Initiate the class
			$xml = loadObject("xml");
			// Set encoding for xml if it is other than utf8
			if($this->_encoding != "UTF-8")
				$xml->setXMLEncoding($this->_encoding);
			// Set the array so the class knows what to create the XML from
			$xml->setArray($object);
			// return the XML to screen
			return $xml->outputXML('return');
		}///--END:getXMLResponse () --


	    /**
	    * @method getJsonResponse
	    * @access private
	    * @params object (Array)
	    * @return json response of passed array
	    */
	    private function getJsonResponse($object) {
			if($this->_encoding == "UTF-8") {
				$this->definePhpVersionId();

				if(PHP_VERSION_ID >= 50400) {
					return json_encode($object, JSON_UNESCAPED_UNICODE);
				} else {
					if($this->_force_utf_encoding) {
						return $this->getUnicodeJson($object);
					} else {
						return json_encode($object);
					}
				}
			} else {
				return json_encode($object);
			}
		}///--END:getJsonResponse () --

	    /**
	    * @method definePhpVersionId
	    * @access private
	    * @params none
	    * @return Define PHP_VERSION_ID for php versions that are less than 5.2
	    */
	    private function definePhpVersionId() {
			if (!defined('PHP_VERSION_ID')) {
				$version = explode('.', phpversion());

				define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
			}///--END:FII--
		}///--END:definePhpVersionId () --


	    /**
	    * @method getUnicodeJson
	    * @access private
	    * @params object (Array)
	    * @return json string with unicode characters
	    */
	    private function getUnicodeJson($object) {
			array_walk_recursive($object, function(&$item, $key) {
				if(is_string($item)) {
					$item = htmlentities($item);
				}///--END:FII--
			});///--END:FUNCTION--

			$json = json_encode($object);
			return html_entity_decode($json);
			//return $json;
		}///--END:getUnicodeJson () --

		public function processRequest($requestBody){
			header("content-type: application/json;charset=utf-8");
			$appVerCode = (isset($_REQUEST['avc'])) ? $_REQUEST['avc'] : "";
			$os 		= (isset($_REQUEST['os'])) ? $_REQUEST['os'] : "";
            $is_zip     = (isset($_REQUEST['is_zip'])) ? $_REQUEST['is_zip'] : 0;
			if($is_zip == 1){
                return "";
            }
            if(isset($_REQUEST["is_zip"]) && $_REQUEST["is_zip"]=="1"){
					// if supported, gzips data
					//ob_start("ob_gzhandler");
					header("Content-Encoding: gzip");

					// set default value
					$setOsVersion = 9999;

					if($os=='a'){
						$setOsVersion  = $this->_leastSupportedAndroidVersion;
					}elseif($os=='i'){
						$setOsVersion  = $this->_leastSupportediOSVersion;
					}
                    
                    
					// if request variable don't support to least version code then send to upsupported error
					if($is_zip != "1"){
					   return "";
					}else{
							$this->setErrors("msg","UNSUPPORTED_VERSION_ERROR");
							$this->setErrors("status","0");
							return $this->stringToGZip($this->getErrors($requestBody));
					}
			}
			return "";

		}

		public function processResponse($string="",$requestBody){

		   // get request variable
		   $appVerCode = (isset($_REQUEST['avc'])) ? $_REQUEST['avc'] : "";
           $is_zip     = (isset($_REQUEST['is_zip'])) ? $_REQUEST['is_zip'] : 0;

		   	if($is_zip == "1"){
					return $this->stringToGZip($string);
				}

			// if gzip not supported, simple json data
			return $string;
		}

	   // Convert Json string to Gzip
		private function stringToGZip($string){
			$is_zip = (isset($_REQUEST['is_zip'])) ? $_REQUEST['is_zip'] : "";
			if($is_zip != 1){
                return $string;
			}else{
                $replyBody = gzencode($string,9); // 9 is for compression level
                return $replyBody;    
            }
		}
}
?>
