<?php
/**
 * @Author : Anoop singh
 * @Email  : anoop.immortal@gmail.com
 * @Timestamp : Aug-29 06:11PM
 * @Copyright : avaitor team
 * @Package : Avaitor\Core
 */
	error_reporting(0);
	
	ob_start();// -- Get Output in Buffer -
	
	$server_address = getHostByName(php_uname('n'));
	date_default_timezone_set('Australia/Sydney');
	/*if($server_address == '110.232.114.48') {
		date_default_timezone_set('Australia/Brisbane');
	} else {
		date_default_timezone_set('Asia/Kolkata');
	}*/
	define('CURRENT_TIME',date('Y-m-d H:i:s'));
	define('CURRENT_DATE',date('Y-m-d'));

	define('SERVICEPATH',dirname(__FILE__)); // -- Define servicee path to check it is a correct way to access --

	define('BASEPATH',dirname(__FILE__)); // -- Define base path to check it is a correct way to access --
	
	//define('ACCESSKEY','yzddccH3BRaf7Mz');
	
	define('LIBPATH',"core"); // -- Define Lib path --

	define('DS',"/"); // -- Define Directory seprator -- 

	define('EXT',".php");  // -- Define Base file extension type --

	define('ENCODING',"UTF-8");  // -- Define Encoding --
	
	
	$root_folder = basename(dirname(dirname(__FILE__))); // -- Get service folder's parent name
	$setup_folder = ($root_folder == 'coyote' ) ? '/coyote' : '/coyotev3'; // -- Set directory name
	define('ROOT', 'var/www/html'.$setup_folder); // --Path to the front controller (this file) --
	
    $base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http"); 	// -- Define URL path to pass in the push notification messages.
    $base_url .= "://". @$_SERVER['HTTP_HOST'];
    $base_url .= $setup_folder;
    $base_url .= "/";
    define('URL_PATH',$base_url);
    
    define('SERVICES_URL_PATH',$base_url."services/");


    $folder_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $folder_url .= "://". @$_SERVER['HTTP_HOST'];
    $folder_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
    define('FOLDER_URL_PATH',$folder_url);

    define('IMG_URL',URL_PATH."uploads/");
    define('IMG_FOLDER_PATH',$_SERVER["DOCUMENT_ROOT"].$setup_folder."/uploads/");

	
	try {  // -- All to catch exception handling --

		if(file_exists(SERVICEPATH.DS.LIBPATH.DS."common.php")) { // -- check FIle exists or now --

			include(SERVICEPATH.DS.LIBPATH.DS."common.php"); // -- Include File exists or now --
			
			log_message("debug","common file included"); // -- Log Debug Message --
			
			$resService=loadObject("restService"); // -- Create Service Object --
			
			$resService->init(); // -- Init Service Object --
			
			if(ENCODING != 'UTF-8') // -- See if need to set Encoding
				$resService->setEncoding(ENCODING);
			
			$format="JSON"; // -- Set output format --
			
			/* SET Header Information so that all other host access service */
			header("Access-Control-Allow-Orgin: *");
			header("Access-Control-Allow-Methods: *");
			
			/*  XML Or JSON Response Start */
			if(strtolower($format)=="xml")
			{
				header("Pragma: public"); // required
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
				header("Cache-Control: private",false); // required for certain browsers 
				header("content-type: text/xml; charset=utf-8");
				header("Content-Transfer-Encoding: binary");
			} else if(strtolower($format)=="json") {
				header("content-type: application/json; charset=utf-8");
			}
			/*  XML Or JSON Response End */
			if((isset($_REQUEST["is_zip"]) && $_REQUEST["is_zip"]=="1")){
                ini_set("zlib.output_compression", "On");
                $result = $resService->processRequest($format);
                if($result==""){
                    $result=$resService->execute($format);
                    $result=$resService->processResponse($result,$format);
                }
                ob_clean();
                echo $result;	
            }else{
                $result=$resService->execute($format);
            }
            
            ob_clean();
			echo $result;	
            
            
            
            
		}
	} catch(Exception $e) {
		
		die("OOPS something goes wrong");
	
	}
?>
