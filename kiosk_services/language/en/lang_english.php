<?php
/***-COMMON LANGUAGE MASSAGE FOR ALL CLASS :Start-***/

/**-LANGUAGE MESSAGE FOR "Kiosk" :Start-**/
$_lang['txt.kiosk.upgrade_txt'] = "Your app version is too old. Need to upgrade it!";
$_lang['txt.kiosk.valid_version_txt'] = "You have a upgraded version!";
$_lang['txt.kiosk.invalid_verion'] = "Please check your given version code!";
$_lang['txt.kiosk.invalid_macid'] = "Please provide a valid Mac Id!";
$_lang['txt.kiosk.request_inqueue'] = "Your request is in queue! We'll update you shortly.";
$_lang['txt.kiosk.already_have_licence'] = "You already have an licence key.";
$_lang['txt.kiosk.add_request'] = "Your request sent successfully. We'll update you shortly!";
$_lang['txt.kiosk.invalid_macid_licence'] = "Please provide a valid MacId/LicenceKey!";
$_lang['txt.kiosk.valid_licence'] = "You have an valid Licence token.";
$_lang['txt.kiosk.invalid_licence'] = "Invalid Licence Token.";
$_lang['txt.kiosk.mismatch_mac_id'] = "Mismatch Mac Id.";
$_lang['txt.kiosk.licence_expired'] = "Licence expired.Please contact to service provider!";
$_lang['txt.kiosk.error_journal_sync'] = "Error during sync journal data!";
$_lang['txt.kiosk.error_journal_product'] = "Journal products should not be blank!";
$_lang['txt.kiosk.error_journal_header'] = "Journal header details should not be empty!";
$_lang['txt.kiosk.success_journal_sync'] = "Successfully synced";
$_lang['txt.kiosk.trx_data_exists'] = "Transaction record already exists!";
$_lang['txt.get_user_profile.error.notfound'] = "User not found.";
$_lang['txt.reward.no_card_found'] = "No card associated with this account!";
$_lang['txt.reward.trx_entry_exists'] = "This transaction already have a locking entry!";
$_lang['txt.reward.non_enough_amt_to_redeem'] = "You don't have enough amount for redemption!";
$_lang['txt.reward.locked_successfully'] = "Transaction redeem points locked successfully.";
$_lang['txt.reward.error.input_params'] = "Please provide all required inputs!";
$_lang['txt.reward.error.no_locking'] = "Invalid redeem request!";
$_lang['txt.reward.unlock_redeem_lock'] = "Transaction failed.Redeem point unlocked!";
$_lang['txt.reward.success.redeem_trx_points'] = "Successfully redeemed points.";
$_lang['txt.get_promotion_list.error.no_promo'] = "No active promotion found.";
$_lang['txt.kiosk.upgrade_exe'] = "Your exe version is too old. Need to upgrade it!.";
/**-LANGUAGE MESSAGE FOR "Kiosk" :End-**/

?>
