<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class commonclass {
	private $user_account = 'ava_users';
    private $user_account_data = 'ava_account_data';
    private $beacons_table = 'ava_beacons';
    private $stores_table = 'ava_stores_log';
    private $categories_table = 'ava_categories';
    private $setting_table = 'ava_app_settings';
    private $notification_tbl = 'ava_reward_notification';
    private $reward_campaign_table = 'ava_reward_campaign';
    private $reward_campaign_log = 'ava_reward_campaign_log';
	private $reward_transaction_table = 'ava_reward_transaction';
    private $reward_redeem_log_tbl = 'ava_reward_redeem_log';
    private $cte_winners_table = 'ava_cte_winners_log';
    private $cte_member_entries_table = 'ava_cte_member_entries_log';
    private $kiosk_redeem_log_tbl = 'ava_kiosk_redeem_points';

    public function __construct() {
        $this->_db = env::getInst();
    }
    public function langText($locale,$string){
        header('Content-Type: text/html; charset=utf-8');
        if($locale=='de') {
			$lang=load_lang($locale,"german");
        } else {
			$lang=load_lang($locale,"english");
        }
        return $lang["$string"];
    }

    public function locale() {
        if(!empty($_REQUEST['locale']) && $_REQUEST['locale']=='de') {
            $locale = $_REQUEST['locale'];
        } else {
            $locale = 'en';
        }
        return $locale;
    }

	public function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	public function image_upload($imgurl) {
        $encoded_image      = $imgurl;
        $upload_path        = IMG_FOLDER_PATH;
        $decoded_image = base64_decode($encoded_image);
        $imgname       = md5(uniqid()) . ".png";
        file_put_contents($upload_path . $imgname, $decoded_image);
        return $imgname;
    }
    
    public function file_image_upload() {
        if(!empty($_FILES["profile_image"])) {
            $target_dir = IMG_FOLDER_PATH.'user_images/';
            $target_file_type = $target_dir . basename($_FILES["profile_image"]["name"]);
            $imageFileType = pathinfo($target_file_type,PATHINFO_EXTENSION);
            $imgname       = md5(uniqid()).".".$imageFileType;
            $target_file = $target_dir . $imgname;
            $check = getimagesize($_FILES["profile_image"]["tmp_name"]);
            if($check !== false) {
                if (move_uploaded_file($_FILES["profile_image"]["tmp_name"], $target_file)) {
                    return $imgname;
                }
            }
        }
        return '';
    }

    public function getFullImgByteCode($imgName){
        $byteCode = "";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, IMG_URL.$imgName);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       
        $byteCode = curl_exec($ch);
        curl_close($ch);
		//$byteCode = file_get_contents(IMG_URL.$imgName);
        $imageEncoded = base64_encode($byteCode);
        return $imageEncoded;
    }
	/*COYOTE*/
	
	/*
    *@mssql_db_connection() this function, connect with live mssql server db
    * */
	public function mssql_db_connection() {
		$con=mssql_connect('10.10.10.4\MSSQL2012','coyote','cdn123') or die("can't connect to DB");
		if($con) {	
			$link=mssql_select_db('coyote') or die("cant use db coyote");
			return $con;
		} else {
			return false;
		}
	}
	
	/*
    *@clean_device_token() this function is use to  clear privous same device id found any
    * */
	public function clean_device_token($device_token) {
		$query = "SELECT id FROM " . $this->user_account . " WHERE device_token = '$device_token'";
		$getUserId = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserId) > 0) {
			while($row = $this->_db->my_fetch_object($getUserId)) {
				$customerId = $row->id;
				$updateArray = array("device_token" => "");
				$whereClause = array("id" => $customerId);
				$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");	
			}
			return TRUE;
		}
	}
	
	 /*
    *@check_session() this function is use to check user session or returns the user id if session is found
    * */
    public function check_session($session_token) {
        $query = "SELECT id FROM " . $this->user_account . " WHERE session_token = '$session_token' AND deleted = '0'";
        $getUserId = $this->_db->my_query($query);
        if ($this->_db->my_num_rows($getUserId) > 0) {
            $row = $this->_db->my_fetch_object($getUserId);
            $customerId = $row->id;
            return $customerId;
        } else {
            return 0;
        }
    } //check_session()
    
     /*
    *@get_account_id_by_verification() this function is use to check user session or returns the user id if session is found
    * */
    public function get_account_id_by_verification($verification_code) {
        $query = "SELECT ACC_NUMBER FROM " . $this->user_account_data . " WHERE verification_code = '$verification_code' AND is_delete = '0'";
        $getUserId = $this->_db->my_query($query);
        if ($this->_db->my_num_rows($getUserId) > 0) {
            $row = $this->_db->my_fetch_object($getUserId);
            $customerId = $row->ACC_NUMBER;
            return $customerId;
        } else {
            return 0;
        }
    } //get_account_id_by_verification()
    /*
    
    /*
    *@check_user_email() Function to check is requested email already exists
    * */
    function check_user_email($email = '', $cutomer_id = '0') { // At insert time : $cutomer_id will be 0 or any numeric value.
	$sql = "SELECT email FROM " . $this->user_account . " WHERE email = '" . $email . "' AND id != '" . $cutomer_id . "'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    } //end check_user_email()
    /*
    *@get_user_data_by_email() Function to get user data correspounding to his email address
    * */
    function get_user_data_by_email($USER_EMAIL = '') {
        $sql = "SELECT * FROM " . $this->user_account . " WHERE email = '" . $USER_EMAIL . "' order by id desc";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row;
        } else {
            return FALSE;
        }
    } //end get_user_data_by_email()
    /*
    *@get_user_data_by_user_id() Function to get user data correspounding to his verification code set at time of forgot_password
    * */
    function get_user_data_by_user_id($ACC_NUMBER = '') {
        $sql = "SELECT * FROM " . $this->user_account . " WHERE id = '" . $ACC_NUMBER . "'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row;
        } else {
            return FALSE;
        }
    } //end get_user_data_by_user_id()
    
	/*
    *@get_account_data_by_user_id() Function to get user data correspounding to his verification code set at time of forgot_password
    * */
    function get_account_data_by_user_id($ACC_NUMBER = '') {
        $sql = "SELECT * FROM " . $this->user_account_data . " WHERE ACC_NUMBER = '" . $ACC_NUMBER . "'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row;
        } else {
            return FALSE;
        }
    } //end get_account_data_by_user_id()

	/*
    *@get_account_data_by_user_id() Function to get user data correspounding to his verification code set at time of forgot_password
    * */
    function get_beacon_id($beacon_code = '') {
        $sql = "SELECT beacon_id,store_id FROM " . $this->beacons_table . " WHERE status = '1' AND beacon_code = '" . $beacon_code . "'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row;
        } else {
            return FALSE;
        }
    } //end get_account_data_by_user_id()
    
    function generate_barcode($code='') { 
		include('libraries/php-barcode.php');
		$upload_path  = IMG_FOLDER_PATH.'user_barcodes/';
		$fontSize = 10; // GD1 in px ; GD2 in point
		$marge = 10; // between barcode and hri in pixel
		$x = 80;  // barcode center
		$y = 50;  // barcode center
		$height = 50;  // barcode height in 1D ; module size in 2D
		$width = 2;  // barcode height in 1D ; not use in 2D
		$angle =0; // rotation in degrees 
		$type = 'code128';

		$im = imagecreatetruecolor(200, 100);
		$black = ImageColorAllocate($im,0x00,0x00,0x00);
		$white = ImageColorAllocate($im,0xff,0xff,0xff);
		imagefilledrectangle($im, 0, 0, 300, 300, $white);

		$src = imagecreatefromgif($code);
		imagecopyresampled($tmp, $src, 0, 0, 0, 0, 300, 300, 300, 300);
		$dst = $upload_path.$im;

		$data = Barcode::gd($im, $black, $x, $y, $angle, $type,   array('code'=>$code), $width, $height);
		//header('Content-type: image/gif');
		
		imagejpeg($im, $upload_path.$code.".jpg");
		imagegif($im);
		imagedestroy($im);
		return $code.".jpg";
	}

	function get_category_data() {
		$sql = "SELECT category_id,category_name FROM " . $this->categories_table . " WHERE status = '1' AND is_deleted = '0'";
        $result = $this->_db->my_query($sql);
        $array = array();
        if ($this->_db->my_num_rows($result) > 0) {
            while($row = $this->_db->my_fetch_object($result)) {
				$array['category_id']	 = $row->category_id;
				$array['category_name'] = $row->category_name;
				$return[] = $array;
			}
			return $return;
        } else {
            return FALSE;
        }
	}
	
	function get_category_data_by_id($category_id = '') {
        $sql = "SELECT category_name FROM " . $this->categories_table . " WHERE category_id = '" . $category_id . "'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row->category_name;
        } else {
            return "No category";
        }
    }
	
	function get_store_data() {
		$sql = "SELECT store_id,store_name FROM " . $this->stores_table . " WHERE status = '1'";
        $result = $this->_db->my_query($sql);
        $array = array();
        if ($this->_db->my_num_rows($result) > 0) {
            while($row = $this->_db->my_fetch_object($result)) {
				$array['store_id']	 = $row->store_id;
				$array['store_name'] = ucwords(strtolower($row->store_name));
				$return[] = $array;
			}
			return $return;
        } else {
            return FALSE;
        }
	}
	
	function get_store_data_by_id($store_id = '') {
        $sql = "SELECT store_name FROM " . $this->stores_table . " WHERE store_id = '" . $store_id . "'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return ucwords(strtolower($row->store_name));
        } else {
            return "No store";;
        }
    }
    
    function get_last_id($tabel,$column) {
		$sql = "select ".$column." from ".$tabel." where ".$column."=(select max(".$column.") from ".$tabel.")";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row->$column;
        } else {
            return 0;
        }
	}
	
	function get_beacon_store($customerId=0,$beaconOfferId=0){
		$sql = "SELECT store_name FROM ava_beacon_customer_offer,ava_beacons,ava_stores_log WHERE ava_beacon_customer_offer.beacon_offer_id = '".$beaconOfferId."' AND ava_beacon_customer_offer.ACC_NUMBER = '".$customerId."' AND ava_beacon_customer_offer.beacon_id = ava_beacons.beacon_id AND ava_beacons.store_id = ava_stores_log.store_id";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row->store_name;
        } else {
            return 0;
        }
	}
    
    /*
     * get specific data from user table
     * @input : table name, column name, where condition
     * @output : data object
     * */
    
    public function get_table_record($tabel='',$columns=array(),$where=array(),$orderby=''){
        $respone = false;
        if(!empty($tabel)) {
            $columnName = '';
            //get columns that are requested in response
            if(!empty($columns)) {
                foreach($columns as $column) {
                    if(!empty($column)) {
                        $columnName .= $column.',';
                    }
                }
            }
            //remove last , from string
            $columnName = rtrim($columnName,',');
            if(empty($columnName)) {
                $columnName = '*';
            } 
            
            $whereCondition = '';
            // prepare the where condition
            if(!empty($where)) {
                foreach($where as $key=>$value) {
                    if(!empty($key)) {
                        $whereCondition .= $key." = '".$value."' AND ";
                    }
                }
            }
            //remove last ' AND' from string
            $whereCondition = rtrim($whereCondition,' AND');
            if(!empty($whereCondition)) {
                $whereCondition = ' WHERE '.$whereCondition;
            }
            $orderbystring = '';
            if(!empty($orderby)) {
                $orderbystring = " order by ".$tabel.'.'.$orderby.' desc';
            }
            $sql = "SELECT ".$columnName." FROM ".$tabel.$whereCondition.$orderbystring;
            $result = $this->_db->my_query($sql);
            if ($this->_db->my_num_rows($result) > 0) {
                while($row = $this->_db->my_fetch_object($result)) {
                    $respone[] = $row;
                }
            }
        } 
        return $respone;
    }
    
    /*
     * @get_reward_config :  get reward points by type
     * @input string
     * @output string
     * */
    public function get_reward_config($point_type='',$is_points_history=0) {
        $reward_config = [
            'registration_points' => ($is_points_history == 1) ? 'Registration Points' : 'Welcome to NightOwl',
            'birthday' => ($is_points_history == 1) ? 'Birthday Points' : 'Happy Birthday',
            'application_social_sharing_points' => ($is_points_history == 1) ? 'Share Application Points' : 'Application Share Successfully',
            'social_sharing_points' => ($is_points_history == 1) ? 'Share Application Points' : 'Application Share Successfully',
            'rate_application_points' => ($is_points_history == 1) ? 'Application Rate Points' : 'Rate Application',
            'make_purchase_points' => ($is_points_history == 1) ? 'Make Purchase Points' : 'Make Purchase',
            'view_vip_deals_points' => ($is_points_history == 1) ? 'View Vip Deal Points' : 'View Vip Deals',
            'view_last_min_deal_points' => ($is_points_history == 1) ? 'View Last Min Deal Points' : 'View Last Min Deal',
            'referral_invitation_points' => ($is_points_history == 1) ? 'Referral Join Points' : 'Referral Join',
            'referral_first_purchase' => ($is_points_history == 1) ? "Referral's First Order Points" : 'Referral First Order',
            'contact_synchronisation_points' => ($is_points_history == 1) ? 'Contact Synchronization Points' : 'Contact Synchronize Successfully',
            'assign_order_purchase_points' => ($is_points_history == 1) ? 'Assign Order Purchase Points' : 'Assign Order Purchase',
            'reward_points' => 'Reward Points',
        ];
        return (!empty($reward_config[$point_type])) ? $reward_config[$point_type] : 'Reward Points';
    } 
    
    public function get_reward_message($reward_type='') {
        switch($reward_type) {
            case 'social_sharing_points' :
                $message = $this->langText($this->locale(),'txt.reward.social_sharing_points_msg');
            break;
            case 'rate_application_points' :
                $message = $this->langText($this->locale(),'txt.reward.rate_application_points_msg');
            break;
            case 'make_purchase_points' :
                $message = $this->langText($this->locale(),'txt.reward.order_purchase_points_push_msg');
            break;
            case 'view_vip_deals_points' :
                $message = 'view_vip_deals_points';
            break;
            case 'view_last_min_deal_points' :
                $message = 'view_last_min_deal_points';
            break;
            case 'referral_first_purchase' :
                $message = $this->langText($this->locale(),'txt.reward.order_purchase_points_push_msg');
            break;
            default:
                $message = '';
            break;
        }
        return $message;
    }
    
    /*
     * @save_notification : save notification data
     * @input array
     * @output boolean/int
     * */
    public function save_notification($data=array()) {
        // get data
        $user_id 		   = (!empty($data['user_id'])) ? $data['user_id'] : 0;
        $notification 	   = (!empty($data['notification'])) ? $data['notification'] : '';
        $notification_type = (!empty($data['notification_type'])) ? $data['notification_type'] : '';
        $is_send		   = (!empty($data['is_send'])) ? $data['is_send'] : '';
        $created_date 	   = date('Y-m-d H:i:s');
        $reward_point 	   = (!empty($data['reward_point']))?$data['reward_point']:'';
        // insert the data into table

        $query = "INSERT ".$this->notification_tbl." (notification, user_id, notification_type, is_send, created_date,reward_point) VALUES ('".$notification."','".$user_id."','".$notification_type."','".$is_send."','".$created_date."','".$reward_point."')";
        if($this->_db->my_query($query)){
            return true; // return inserted id
        } else {
            return false; // return false
        }   
    }
    
    /*
     * @update_referral_code : use the insert the referral w.r.t to user_id if empty
     * @input : user_id(int),firstname(string),my_reference_code(string)
     * @output: string
     * */
    public function update_referral_code($user_id=0,$firstname='',$my_reference_code='') {
        // check reference code empty aur not
        if(empty($my_reference_code)){
            
            // set reference_code to null
            $reference_code = $this->random_string(4);
            
            //set total length of string
            $totallength = 4;
            $idlength = strlen($user_id);
            
            // find the no of zero would be added
            $finallength = $totallength - $idlength;
            if($finallength > 0) {
                $newid = '';
                // add zeros
                for($i=0;$i < $finallength;$i++){
                    $newid .= 0;
                }
                $reference_code .= $newid;
                $reference_code .= $user_id;
            } else {
                $reference_code .= $user_id;
            }
            $my_reference_code = $reference_code;      
            $updateArray = array("my_reference_code" => $reference_code);
            $whereClause = ['id'=>$user_id];
            $update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
        }
        return $my_reference_code;
    }
    
    /*
     * @send_email is use to send email
     * @input array
     * @input void
     **/
    public function send_email($emailData=array()) {
        //include the file
        require_once ("libraries/sendMailCron.php");
        
        //get parameter
        $emailBody = (!empty($emailData['emailBody'])) ? $emailData['emailBody'] : ''; //email body to be send
        $receiverId = (!empty($emailData['receiverId'])) ? $emailData['receiverId'] : ''; //email id on which email to be send
        $purpose = (!empty($emailData['purpose'])) ? $emailData['purpose'] : ''; // the purpose or subject
        // send email
        sendActivationMailAction($receiverId, $purpose, 'en', $emailBody);
    }
    
    /**
     * @send_edm_mail is use to send edm email
     * @input emailId,subject,body
     * @input void
     **/
    public function send_edm_mail($emailId,$subject,$body) {
        //include the file
        require_once ("libraries/sendMailCron.php");
        // send email
        send_edm_mails($emailId,$subject,$body);
    }
    
    /**
     *@random_string is use to generate alphanumeric string
     *@input length(int)
     *@output string
     */
    
    public function random_string($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
	
	/**
	 * Create barcode final digit string
	 * @input : barcode(string)
	 * @output: string
	 * @access: public
	 * @author: Tosif Qureshi
	 */
	public function get_barcode_digits($barcode='') {
		
		//first change digits to a string so that we can access individual numbers
		$digits =(string)$barcode;
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum/10))*10;
		$check_digit = $next_ten - $total_sum;
		$barcode_digits = $digits.$check_digit;
		// Count of barcode text digits
		$barcode_digit_count = strlen($barcode_digits);
		if($barcode_digit_count < 13) {
			$remain_text = 13 - $barcode_digit_count;
			for($i=0;$i<$remain_text;$i++) {
				$barcode_digits = '0'.$barcode_digits;
			}
		}
		return $barcode_digits;
	}
    
    
    /* function use to read image src code */
    
    public function file_get_contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
	
     /**
      * @manageCompaignLog use the manage log for user 
      * @input: user_id,catalogue_id
      * @output:json array
      **/
	public function manageCompaignLog($user_id,$campaign_id){
        $return = false;
        if(!empty($user_id) && !empty($campaign_id)){
            $today_date   = CURRENT_DATE;
            // get campaign detail for campaign table
            $campaign_query = "SELECT action_recurrence,reward_points FROM ".$this->reward_campaign_table." where status = '1' AND is_deleted = '0' AND start_date <= '".$today_date."' AND end_date >= '".$today_date ."' AND id = ".$campaign_id ;
            $get_record = $this->_db->my_query($campaign_query);
            if($this->_db->my_num_rows($get_record) > 0){
                $campaignData = $this->_db->my_fetch_object($get_record);
                // collect the data
                $action_recurrence = (!empty($campaignData->action_recurrence)) ? $campaignData->action_recurrence : 0;
                $reward_points = (!empty($campaignData->reward_points)) ? $campaignData->reward_points : 0;

                //get data from campaign log
                $campaign_log_query = "SELECT COUNT(id) as log_sum FROM " .$this->reward_campaign_log. " WHERE campaign_id = '".$campaign_id."' AND user_id = '".$user_id ."' ";
                $campaign_log_result = $this->_db->my_query($campaign_log_query);
                if($this->_db->my_num_rows($campaign_log_result) > 0){
                    $campaign_log_Data = $this->_db->my_fetch_object($campaign_log_result);
                    //collect the data
                    $log_sum = (!empty($campaign_log_Data->log_sum)) ? $campaign_log_Data->log_sum : 0;
                    if($log_sum < $action_recurrence){
                        //insert data inyo  campaign log table
                        $insert_share_log = "INSERT INTO ".$this->reward_campaign_log." (campaign_id,user_id) VALUES('".mysql_real_escape_string($campaign_id)."','".mysql_real_escape_string($user_id)."')";
                        $this->_db->my_query($insert_share_log);
                        return $reward_points;
                    }
                }
            }
        }
        return $return;
	}//manageCompaignLog
    
   /**
    * @balancePoint for calcualting the 
    * @input: $userId
    * @output:blance point OR array
    **/
	public function getBalancePoint($user_id=0){
        $currentdatetime = CURRENT_TIME;
		$reward_trans_table     = $this->reward_transaction_table;
		$reward_redeem_log_tbl  = $this->reward_redeem_log_tbl;
		$kiosk_redeem_log_tbl  = $this->kiosk_redeem_log_tbl;
		$return['redeem_points']  = 0;
		$return['is_expired']  = 1;
		if(!empty($user_id)){
            $balance_query ="SELECT (SELECT COALESCE(SUM(balance_point),0) FROM ".$reward_trans_table." WHERE status = '2' AND user_id = '".$user_id."') as Spoint,(SELECT COALESCE(SUM(redeem_point),0)  FROM ".$reward_redeem_log_tbl."  WHERE user_id = '".$user_id."' AND is_redeem = 1) as Dpoint,(SELECT COALESCE(SUM(redeem_points),0)  FROM ".$kiosk_redeem_log_tbl."  WHERE user_id = '".$user_id."') as Kpoint,(SELECT create_date FROM ".$reward_redeem_log_tbl."  WHERE user_id = '".$user_id."' AND is_redeem = 0 ORDER BY create_date DESC LIMIT 1) as create_date";
            $balance_result = $this->_db->my_query($balance_query);	
            $balance_data   = $this->_db->my_fetch_object($balance_result);
            $return['redeem_points']  = ($isArray)?$balance_data:(int)((!empty($balance_data->Spoint)?$balance_data->Spoint:0)-(!empty($balance_data->Dpoint)?$balance_data->Dpoint:0)-(!empty($balance_data->Kpoint)?$balance_data->Kpoint:0));
            $create_date  = (!empty($balance_data->create_date)) ? $balance_data->create_date : 0;
            $is_expired  = 1;
            $time_difference = (strtotime($currentdatetime) - strtotime($create_date));
            if($time_difference <= 60) {
                $return['is_expired']  = 0;
            }
		}
		return $return;
	}
	
	/**
    * @description: Function used to get cracktheegg entry points
    * @input: $acc_number
    * @output: int
    **/
	public function get_cte_entry_points($acc_number=0){
		$total_entry_points = 0;
		if(!empty($acc_number)){
			$point_query ="SELECT COALESCE(SUM(entry_points),0) as total_entry_points FROM ".$this->cte_member_entries_table." WHERE member_id = $acc_number";
			$point_result = $this->_db->my_query($point_query);	
            $points_data   = $this->_db->my_fetch_object($point_result);
            $total_entry_points  = !empty($points_data->total_entry_points) ? $points_data->total_entry_points : 0;
		}
		return $total_entry_points;
	}
}
?>
