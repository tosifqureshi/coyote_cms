<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Pushpraj
 * Timestamp : March-28
 * Copyright : @coyote nightowl team
 *
 */
class kiosk {
    public $_response = array();
    public $result = array();
    private $JNLHTBL = 'JNLHTBL';
    private $TILLTBL = 'TILLTBL';
    private $kiosk_licence_table = 'ava_kiosk_licence_keys';
    private $kiosk_exe_version_table = 'ava_kiosk_exe_versions';
	private $user_table = 'ava_users';
	private $reward_cards = 'ava_reward_cards';
	private $kiosk_promotions_table = 'ava_kiosk_promotions';
	private $kiosk_reward_point_lock_table = 'ava_kiosk_redeem_point_lock';
	private $kiosk_redeem_points_tbl = 'ava_kiosk_redeem_points';
	private $kiosk_settings_tbl = 'ava_kiosk_settings';

    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
        $this->_config  = config::getInst();
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_mssql	= new mssql(); // Create instance of mssql class
        $this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
    }
    
    public function sync_journal_log() {
		$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.error_journal_sync');;
		$_response['result_code'] = 0;
		$filename =  BASEPATH.'/log.txt'; 
		file_put_contents($filename, $_REQUEST['product_list'].'=='.date('H:i:s'));
        // get request data
        $get_JNLH_YYYYMMDD = (!empty($_REQUEST['JNLH_YYYYMMDD'])) ? $_REQUEST['JNLH_YYYYMMDD'] : '';
        $get_JNLH_YYYYMMDD = explode('/',$get_JNLH_YYYYMMDD);
        $JNLH_YYYYMMDD = $get_JNLH_YYYYMMDD[2].$get_JNLH_YYYYMMDD[1].$get_JNLH_YYYYMMDD[0];
        
        $get_JNLH_HHMMSS = (!empty($_REQUEST['JNLH_HHMMSS'])) ? $_REQUEST['JNLH_HHMMSS'] : '';
        $JNLH_HHMMSS = date('His',strtotime($get_JNLH_HHMMSS));
        $JNLH_HOUR = date('H',strtotime($get_JNLH_HHMMSS));
        
        $JNLH_TILL = (!empty($_REQUEST['JNLH_TILL'])) ? $_REQUEST['JNLH_TILL'] : '';
        $JNLH_TRX_NO = (!empty($_REQUEST['JNLH_TRX_NO'])) ? $_REQUEST['JNLH_TRX_NO'] : '';
        $JNLH_TYPE = (!empty($_REQUEST['JNLH_TYPE'])) ? $_REQUEST['JNLH_TYPE'] : '';
        $JNLH_STATUS = (!empty($_REQUEST['JNLH_STATUS'])) ? $_REQUEST['JNLH_STATUS'] : '';
        $JNLH_TRX_AMT = (!empty($_REQUEST['JNLH_TRX_AMT'])) ? $_REQUEST['JNLH_TRX_AMT'] : '';
        $JNLH_TRX_GST = (!empty($_REQUEST['JNLH_TRX_GST'])) ? $_REQUEST['JNLH_TRX_GST'] : '';
        $JNLH_date_formate = $get_JNLH_YYYYMMDD[2].'-'.$get_JNLH_YYYYMMDD[1].'-'.$get_JNLH_YYYYMMDD[0];
        $JNLH_time_formate =  date('H:i:s',strtotime($get_JNLH_HHMMSS));
        $JNLH_DATE = $JNLH_date_formate.' '.$JNLH_time_formate;
        $JNLH_TIMESTAMP = $JNLH_date_formate.' '.$JNLH_time_formate;
        $JNLH_TRADING_DATE = (!empty($_REQUEST['JNLH_TRADING_DATE'])) ? $_REQUEST['JNLH_TRADING_DATE'] : '';
        $JNLH_HOUR = (!empty($JNLH_HOUR)) ? $JNLH_HOUR : '';
        $JNLH_CASHIER = (!empty($_REQUEST['JNLH_CASHIER'])) ? $_REQUEST['JNLH_CASHIER'] : '';
        if(!empty($get_JNLH_YYYYMMDD) && !empty($JNLH_TILL) && !empty($JNLH_TRX_NO) && !empty($JNLH_TRX_AMT)) {
			//get JNLH_OUTLET id from TILLTBL
			$JNLH_OUTLET = 0;
			$sql = "SELECT TILL_OUTLET FROM TILLTBL WHERE TILL_NUMBER = '".$JNLH_TILL."'";
			$getData = $this->_mssql->my_mssql_query($sql,$this->con);
			if($this->_mssql->mssql_num_rows_query($getData) > 0 ) { 
				$TILL_OUTLET = $this->_mssql->mssql_fetch_object_query($getData);
				$JNLH_OUTLET = (!empty($TILL_OUTLET->TILL_OUTLET)) ? $TILL_OUTLET->TILL_OUTLET : 0;
			}
			// fetch journal header data if exists
			$sql = "SELECT * FROM JNLHTBL WHERE JNLH_TRX_NO = '".$JNLH_TRX_NO."'";
			$getData = $this->_mssql->my_mssql_query($sql,$this->con);
			if($this->_mssql->mssql_num_rows_query($getData) > 0 ) { 
				$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.trx_data_exists');
			} else {
				//insert JNLHTBL data 
				$insertSQL = "INSERT JNLHTBL (JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, JNLH_TRX_AMT, JNLH_TRX_GST, JNLH_DATE, JNLH_TIMESTAMP, JNLH_TRADING_DATE, JNLH_HOUR, JNLH_CASHIER, JNLH_POST_STATUS) VALUES ('$JNLH_YYYYMMDD', '$JNLH_HHMMSS', '$JNLH_OUTLET', '$JNLH_TILL', '$JNLH_TRX_NO', '$JNLH_TYPE', '$JNLH_STATUS', '$JNLH_TRX_AMT', '$JNLH_TRX_GST', '$JNLH_DATE', '$JNLH_TIMESTAMP', '$JNLH_TRADING_DATE', '$JNLH_HOUR', '$JNLH_CASHIER', '0')";
				$this->_mssql->my_mssql_query($insertSQL,$this->con);
				// check the product lust
				if(!empty($_REQUEST['product_list'])) {
					$product_list_array = json_decode($_REQUEST['product_list']);
					$JNLD_SEQ = 1;
					if(!empty($product_list_array)) {
						$product_list_array = $product_list_array;
						foreach($product_list_array as $product){
							// collect data from product data
							$JNLD_TRX_NO = (!empty($product->JNLD_TRX_NO)) ? $product->JNLD_TRX_NO : '0';
							$JNLD_TYPE = (!empty($product->JNLD_TYPE)) ? $product->JNLD_TYPE : '0';
							$JNLD_STATUS = (!empty($product->JNLD_STATUS)) ? $product->JNLD_STATUS : '0';
							$JNLD_PRODUCT = (!empty($product->JNLD_PRODUCT)) ? $product->JNLD_PRODUCT : '0';
							$JNLD_DESC = (!empty($product->JNLD_DESC)) ? $product->JNLD_DESC : '0';
							$JNLD_QTY = (!empty($product->JNLD_QTY)) ? $product->JNLD_QTY : '0';
							$JNLD_AMT = (!empty($product->JNLD_AMT)) ? $product->JNLD_AMT : '0';
							$JNLD_DISC_AMT = (!empty($product->JNLD_DISC_AMT)) ? $product->JNLD_DISC_AMT : '0';
							$JNLD_GST_AMT = (!empty($product->JNLD_GST_AMT)) ? $product->JNLD_GST_AMT : '0';
							$JNLD_COST = '0';
							$JNLD_GST_COST = '0';
							$JNLD_CASHIER = (!empty($product->JNLD_CASHIER)) ? $product->JNLD_CASHIER : '0';
							$JNLD_PRICE_LEVEL = (!empty($product->JNLD_PRICE_LEVEL)) ? $product->JNLD_PRICE_LEVEL : '0';
							if($JNLD_PRICE_LEVEL >= 10) {
								$JNLD_PRICE_LEVEL = 0;
							}
							$JNLD_PROM_SELL = '0';
							$JNLD_MIXMATCH = (!empty($product->JNLD_MIXMATCH)) ? $product->JNLD_MIXMATCH : '0';
							$JNLD_OFFER = (!empty($product->JNLD_OFFER)) ? $product->JNLD_OFFER : '0';
							$JNLD_POST_STATUS = '0';
							$JNLD_APN_SOLD = (!empty($product->JNLD_APN_SOLD)) ? $product->JNLD_APN_SOLD : '0';
							$JNLD_COMP = (!empty($product->JNLD_COMP)) ? $product->JNLD_COMP : '0';
							$JNLD_LOYALTY_INFO = (!empty($product->JNLD_LOYALTY_INFO)) ? $product->JNLD_LOYALTY_INFO : '0';
							$JNLD_REFERENCE_TYPE = (!empty($product->JNLD_REFERENCE_TYPE)) ? $product->JNLD_REFERENCE_TYPE : '0';
							$JNLD_REFERENCE = (!empty($product->JNLD_REFERENCE)) ? $product->JNLD_REFERENCE : '0';
							//insert JNLHTBL data 
							$insertJNLDTBLSQL = "INSERT JNLDTBL (JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_OUTLET, JNLH_TILL, JNLD_TRX_NO, JNLD_TYPE, JNLD_STATUS, JNLD_PRODUCT, JNLD_DESC, JNLD_QTY, JNLD_AMT, JNLD_DISC_AMT, JNLD_COST, JNLD_GST_COST, JNLD_CASHIER, JNLD_PRICE_LEVEL, JNLD_PROM_SELL, JNLD_MIXMATCH, JNLD_OFFER, JNLD_POST_STATUS, JNLD_APN_SOLD, JNLD_COMP, JNLD_LOYALTY_INFO, JNLD_REFERENCE_TYPE, JNLD_REFERENCE, JNLD_SEQ) VALUES ('$JNLH_YYYYMMDD', '$JNLH_HHMMSS', '$JNLH_OUTLET', '$JNLH_TILL', '$JNLD_TRX_NO', '$JNLD_TYPE', '$JNLD_STATUS', '$JNLD_PRODUCT', '$JNLD_DESC', '$JNLD_QTY', '$JNLD_AMT', '$JNLD_DISC_AMT', '$JNLD_COST', '$JNLD_GST_COST', '$JNLD_CASHIER', '$JNLD_PRICE_LEVEL', '$JNLD_PROM_SELL', '$JNLD_MIXMATCH', '$JNLD_OFFER', '$JNLD_POST_STATUS', '$JNLD_APN_SOLD', '$JNLD_COMP', '$JNLD_LOYALTY_INFO', '$JNLD_REFERENCE_TYPE', '$JNLD_REFERENCE'', '$JNLD_SEQ')";                   
							$this->_mssql->my_mssql_query($insertJNLDTBLSQL,$this->con);             
							//increment the JNLDTBL
							$JNLD_SEQ++;
						}
						$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.success_journal_sync');
						$_response['result_code'] = 1;
					}
				}	else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.error_journal_product');
				}
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.error_journal_header');;
		}
        return  $_response;
    }
    
     /**
	 * Function used to validate kiosk licence
	 * @input null
	 * @return array
	 * @access public
	 *
	 */
	public function validate_kiosk_licence() {
		
		// set input params
		$mac_id = (isset($_REQUEST['mac_id'])) ? trim($_REQUEST['mac_id']) : "";
		$kiosk_type = (isset($_REQUEST['kiosk_type'])) ? trim($_REQUEST['kiosk_type']) : 1;
		$access_code = (isset($_REQUEST['access_code'])) ? trim($_REQUEST['access_code']) : "";
		$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.invalid_macid_licence');;
		$_response['result_code'] = 0;
		$action  = 'decode';
		$current_date = date('Y-m-d');
		//echo $mac_id.'=='.$access_code;die;
		$filename =  BASEPATH.'/log.txt'; 
		file_put_contents($filename, $mac_id.'=='.$access_code.'=='.date('H:i:s'));
		if(!empty($mac_id) && !empty($access_code) && !empty($kiosk_type)){
			$encoded_token = $access_code;
			// include JWT library
			require_once($_SERVER['DOCUMENT_ROOT'].'/coyotev3/jwt_token/token_encryption.php');
			$decoded_licence = json_decode($decoded_licence);
			if(!empty($decoded_licence)) {
				$decoded_licence = (array)$decoded_licence;
				$decoded_mac_id = (string) $decoded_licence['mac_id'];
				if($mac_id == $decoded_mac_id) {
					// fetch kiosk licence requests
					$sql = "SELECT * FROM " . $this->kiosk_licence_table." WHERE mac_id = '".$mac_id."' AND kiosk_type = '".$kiosk_type."' AND till_id = '".$decoded_licence['till_id']."'";
					$result = $this->_db->my_query($sql);
					if ($this->_db->my_num_rows($result) > 0) {
						$row = $this->_db->my_fetch_object($result);
						// set licence params
						$id = (!empty($row->id)) ? $row->id : 0;
						$status = (!empty($row->status)) ? $row->status : 0;
						$expire_at = (!empty($row->expire_at)) ? $row->expire_at : '';
						// check licence expiry date
						if($expire_at < $current_date) {
							// set error response
							$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.licence_expired');
							return $_response;
						}
						
						if($status != 2) {
							// associated mac id with licence key
							$updateArray = array("mac_id" => $mac_id,"status" => 2);
							$whereClause = array("id" => $id);
							$update = $this->_db->UpdateAll($this->kiosk_licence_table, $updateArray, $whereClause, "");
						}
						// set result data
						$licence_result['expire_at'] = $expire_at;
						$licence_result['mac_id'] = $mac_id;
						// set final success response
						$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.valid_licence');
						$_response['result_code'] = 1;
						$_response['result'] = $licence_result;
					} else {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.invalid_licence');
					}
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.mismatch_mac_id');
				}
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to fetch kiosk version details
	 * @input kiosk_version float
	 * @return array
	 * @access public
	 *
	 */
	 public function check_kiosk_version() {
		// set input params
		$kiosk_version = (isset($_REQUEST['kiosk_version'])) ? $_REQUEST['kiosk_version'] : "";
		$kiosk_type = (isset($_REQUEST['kiosk_type'])) ? $_REQUEST['kiosk_type'] : 1;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.invalid_verion');;
		$_response['result_code'] = 0;
		if($kiosk_version != '') {
			// fetch kiosk app version details 
			$sql = "SELECT exe_version,exe_download_url,exe_device_type FROM " . $this->kiosk_exe_version_table." WHERE exe_device_type = $kiosk_type ORDER BY exe_version DESC" ;
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				if($kiosk_version < $row->exe_version) {
					// set result 
					$version_result['current_exe_version'] = $row->exe_version;
					$version_result['exe_download_url'] = $row->exe_download_url;
					// set return force update response
					$_response['result_code'] = 111;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.upgrade_txt');
					$_response['result'] = $version_result;
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.valid_version_txt');
					$_response['result_code'] = 1;
				}
			}
		}
		return $_response;
	 }
	 
	 /**
     * Function used to get my reward point details
     * @input acc_number int
     * @access public
     * @return array
     */
     public function my_reward_points() {
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.get_user_profile.error.notfound');
		
		if(!empty($member_id)) {
			
			// fetch user's reward card details
			$reward_query  = "SELECT u.id,u.acc_employee_ind,u.reward_card_id,rc.card_name,rc.description,rc.card_discount,rc.reward_point_rate,rc.reward_spend_rate FROM " . $this->user_table . " u JOIN " .$this->reward_cards. " rc ON rc.id = u.reward_card_id WHERE u.user_barcode = $member_id AND rc.is_deleted = '0' AND rc.status = '1' ";
			echo $reward_query;die;
			$reward_result = $this->_db->my_query($reward_query);
			if ($this->_db->my_num_rows($reward_result) > 0) {
				// fetch result object data
				$row = $this->_db->my_fetch_object($reward_result);
				$user_id = (isset($row->id)) ? $row->id: 0;
				$acc_employee_ind = (isset($row->acc_employee_ind)) ? $row->acc_employee_ind: 0;
				// get reward total points
				$point_balance = $this->_common->getBalancePoint($user_id);
				$reward_points = (!empty($point_balance['redeem_points'])) ? $point_balance['redeem_points'] : 0;
				$reward_spend_rate = (!empty($row->reward_spend_rate)) ? $row->reward_spend_rate : '';
				$card_discount = (!empty($row->card_discount)) ? $row->card_discount : 0;
			}
		}
		// fetch kiosk setting details
		$kiosk_query  = "SELECT * FROM " . $this->kiosk_settings_tbl . " ";
		$kiosk_result = $this->_db->my_query($kiosk_query);
		if ($this->_db->my_num_rows($kiosk_result) > 0) {
			// fetch result object data
			$settings = $this->_db->my_fetch_object($kiosk_result);
			$employee_discount = (!empty($settings->employee_discount)) ? $settings->employee_discount: 0;
			$normal_discount   = (!empty($settings->normal_discount)) ? $settings->normal_discount: 0;
			$exclude_category  = (!empty($settings->exclude_category)) ? $settings->exclude_category: '';
		}
		
		
		// set card name and discount if user is staff
		$card_name = (!empty($row->card_name)) ? $row->card_name : '';
		$card_discount = (isset($card_discount)) ? $card_discount : 0;
		if(isset($acc_employee_ind) && $acc_employee_ind == 1) {
			$card_name = 'STAFF';
			$card_discount = (isset($employee_discount)) ? $employee_discount : 0;
		}
		
		// set reward params
		$_response['result_code'] 	= 1;
		$_response['message'] 		= '';
		$_response['card_name'] 	= $card_name;
		$_response['reward_spend_rate'] = (!empty($row->reward_spend_rate)) ? $row->reward_spend_rate : '';
		$_response['reward_points'] = (isset($reward_points)) ? $reward_points : 0;
		$_response['reward_amount'] = (isset($reward_spend_rate) && isset($reward_points)) ? $reward_points*$reward_spend_rate : 0;
		$_response['employee_discount'] = (isset($employee_discount)) ? $employee_discount : 0;
		$_response['normal_discount']   = (isset($normal_discount)) ? $normal_discount : 0;
		$_response['card_discount']   = $card_discount;
		$_response['exclude_category']  = (isset($exclude_category)) ? $exclude_category : 0;
		return $_response;
	 }
	 
	 /**
	 * Function used to fetch kiosk promotion list
	 * @input null
	 * @return array
	 * @access public 
	 * 
	 */
	function get_kiosk_promotion_list() {
		$datetime = date('Y-m-d H:i:s');
		$array = array();
		
		$selectPromotion = "SELECT * FROM " . $this->kiosk_promotions_table . " WHERE status = '1' AND is_delete = '0' AND promo_end_date >= '".$datetime."' ORDER BY created_at DESC" ;
		$resultPromotion = $this->_db->my_query($selectPromotion);
		if ($this->_db->my_num_rows($resultPromotion) > 0) {
			while($row = $this->_db->my_fetch_object($resultPromotion)) {
				$array['promo_id']	= (isset($row->promo_id)) ? trim($row->promo_id) : "";
				$array['promo_name']	= (isset($row->promo_name)) ? trim($row->promo_name) : "";
				$array['promo_short_description']	= (isset($row->promo_short_desc)) ? trim($row->promo_short_desc) : "";
				$array['promo_long_description']	= (isset($row->promo_long_desc)) ? trim($row->promo_long_desc) : "";
				$kiosk_small_promo_image = (isset($row->kiosk_small_promo_image)) ? IMG_URL.'promotion_images'.DS.trim($row->kiosk_small_promo_image) : "";
				$kiosk_large_promo_image = (isset($row->kiosk_large_promo_image)) ? IMG_URL.'promotion_images'.DS.trim($row->kiosk_large_promo_image) : "";
				$array['kiosk_small_promo_image']	= $kiosk_small_promo_image;
				$array['kiosk_large_promo_image']	= $kiosk_large_promo_image;
				$array['promo_start_date']	= (isset($row->promo_start_date)) ? trim($row->promo_start_date) : "";
				$array['promo_end_date']	= (isset($row->promo_end_date)) ? trim($row->promo_end_date) : "";
				$result[] = $array;
			}
			$_response['result_code'] = 1;
			$_response['result'] 	= $result;
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_promotion_list.error.no_promo');
		}
		return $_response;
	}
	
	 /**
	 * Function used to lock member's redeem points
	 * @input null
	 * @return array
	 * @access public 
	 * 
	 */
	function lock_trx_redeem_points() {
		// set post input params
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		$redeem_amount = ceil($this->_common->test_input($_REQUEST['redeem_amount']));
		$till_number = $this->_common->test_input($_REQUEST['till_number']);
		$mac_id = $this->_common->test_input($_REQUEST['mac_id']);
		
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.error.input_params');
		if(!empty($member_id) && !empty($redeem_amount) && !empty($till_number)) {
			// check trx lock records
			$sql = "SELECT id FROM " . $this->kiosk_reward_point_lock_table." WHERE member_id = '".$member_id."' AND till_number = '".$till_number."' AND is_locked = 1";
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.trx_entry_exists');
			} else {
				// fetch user's reward balance details
				$reward_query  = "SELECT u.id,u.reward_card_id,rc.card_name,rc.description,rc.reward_point_rate,rc.reward_spend_rate FROM " . $this->user_table . " u JOIN " .$this->reward_cards. " rc ON rc.id = u.reward_card_id WHERE u.user_barcode = $member_id AND rc.is_deleted = '0' AND rc.status = '1' ";
				$reward_result = $this->_db->my_query($reward_query);
				if ($this->_db->my_num_rows($reward_result) > 0) {
					// fetch result object data
					$row = $this->_db->my_fetch_object($reward_result);
					$user_id = (isset($row->id)) ? $row->id: 0;
					$reward_spend_rate = (!empty($row->reward_spend_rate)) ? $row->reward_spend_rate : '';
					// get reward total points
					$point_balance = $this->_common->getBalancePoint($user_id);
					$reward_points = (!empty($point_balance['redeem_points'])) ? $point_balance['redeem_points'] : 0;
					$reward_amount = $reward_points*$reward_spend_rate;
					
					if($reward_amount >= $redeem_amount) {
						// lock transaction entry
						$lock_reward_sql = "INSERT INTO ".$this->kiosk_reward_point_lock_table."(member_id,redeem_amount,reward_spend_rate,till_number,mac_id,created_at) VALUES ('$member_id','$redeem_amount','$reward_spend_rate','$till_number','$mac_id','".date('Y-m-d H:i:s')."')";
						$this->_db->my_query($lock_reward_sql);
						$lock_id = mysql_insert_id();
						// set success response
						$_response['result_code'] = 1;
						$_response['lock_id'] = $lock_id;
						$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.locked_successfully');
					} else {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.non_enough_amt_to_redeem');
					}
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.no_card_found');
				}
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to redeem points / unlock transaction lock from backend
	 * @input null
	 * @return array
	 * @access public 
	 * 
	 */
	function redeem_trx_points() {
		// set post input params
		$member_id 	= $this->_common->test_input($_REQUEST['acc_number']);
		$trx_id 	= $this->_common->test_input($_REQUEST['trx_id']);
		$lock_id 	= $this->_common->test_input($_REQUEST['lock_id']);
		$redeem_amount = ceil($this->_common->test_input($_REQUEST['redeem_amount']));
		$till_number= $this->_common->test_input($_REQUEST['till_number']);
		$action	 	= $this->_common->test_input($_REQUEST['trx_action']);
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.error.input_params');
		if(!empty($member_id) && !empty($lock_id) && !empty($redeem_amount) && !empty($till_number) && !empty($action)) {
			// check trx lock record
			$sql = "SELECT l.*,u.id as user_id,u.acc_number FROM " . $this->kiosk_reward_point_lock_table." l JOIN " . $this->user_table . " u ON u.user_barcode = l.member_id  WHERE l.id = '".$lock_id."' AND l.member_id = '".$member_id."' AND l.till_number = '".$till_number."' AND l.is_locked = 1";
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				// fetch result object data
				$row = $this->_db->my_fetch_object($result);
				$id				= (isset($row->id)) ? $row->id: 0;
				$member_id 		= (isset($row->member_id)) ? $row->member_id: 0;
				$redeem_amount 	= (isset($row->redeem_amount)) ? $row->redeem_amount: 0;
				$trx_id 		= (!empty($trx_id)) ? $trx_id: 0;
				$till_number 	= (isset($row->till_number)) ? $row->till_number: 0;
				$mac_id 		= (isset($row->mac_id)) ? $row->mac_id: 0;
				$user_id 		= (isset($row->user_id)) ? $row->user_id: 0;
				$acc_number 	= (isset($row->acc_number)) ? $row->acc_number: 0;
				$spend_rate 	= (isset($row->reward_spend_rate)) ? $row->reward_spend_rate: 0;
				// calculate redeem points
				$redeem_points = $redeem_amount/$spend_rate;
				// set success code
				$_response['result_code'] = 1;
				if($action == 'success') {
					// insert redeem transaction log
					$redeem_sql = "INSERT INTO ".$this->kiosk_redeem_points_tbl."(member_id,user_id,redeem_points,redeem_amount,trx_id,till_number,mac_id) VALUES ('$acc_number','$user_id','$redeem_points','$redeem_amount','$trx_id','$till_number','$mac_id')";
					$this->_db->my_query($redeem_sql);
					// set success msg
					$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.success.redeem_trx_points');
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.unlock_redeem_lock');
				}
				//update transaction lock status as unlock
				$updateArray = array("is_locked" => 0);
				$whereClause = ['id'=>$id];
				$update = $this->_db->UpdateAll($this->kiosk_reward_point_lock_table, $updateArray, $whereClause, "");
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.reward.error.no_locking');
			}
		}
		return $_response;
	}
	
	/**
     * Function used to authenticate kiosk version
     * @input null
	 * @return array
	 * @access public 
     */
    function check_app_version() {
		$kiosk_version = (!empty($_SERVER['HTTP_KIOSKVERSION'])) ? $_SERVER['HTTP_KIOSKVERSION'] : '';
		$kiosk_type    = (!empty($_SERVER['HTTP_KIOSKTYPE'])) ? $_SERVER['HTTP_KIOSKTYPE'] : 1;
		$_response['result_code'] = 1;
		if(!empty($kiosk_version) &&!empty($kiosk_type)){
			$sql = "SELECT exe_version,exe_download_url,exe_device_type FROM " . $this->kiosk_exe_version_table." WHERE exe_device_type = $kiosk_type ORDER BY exe_version DESC" ;
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				if($kiosk_version < $row->exe_version) {
					// set result 
					$version_result['current_exe_version'] = $row->exe_version;
					$version_result['exe_download_url'] = $row->exe_download_url;
					// set return response
					$_response['result_code'] = 111;
					$_response['result'] = $version_result;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.kiosk.upgrade_exe');
				} 
			}
		}
		return $_response;
	}
    
} // End users class
?> 
