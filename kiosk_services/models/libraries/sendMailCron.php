<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * Author : CDNSOL
	 * Email  : admin@cdnsol.com
	 * Timestamp : Aug-8 04:29 PM
	 * Copyright : www.cdnsol.com
	 */
	
	/** Fetch the email template as per the post purpose and language
	 * Return the email template data from zon_email_template table.
	 * **/
	function get_email_template($purpose,$language)
	{
		mysql_query("SET NAMES 'UTF8'");
		$sql = "SELECT * FROM `ava_email_template` WHERE `purpose` = '$purpose' AND `language` = '$language'";
		$result = mysql_query($sql); 
		mysql_set_charset("UTF8", $result);
		$row = mysql_fetch_array($result);
		$newData = $row[2]."||".$row[3];	
		return $newData;
	}
	
	
	function send_edm_mails($emailId,$subject,$body){
		require_once("class.phpmailer.php");
		$mail = new PHPMailer(); // create a new object
		$mail->IsSMTP(); // enable SMTP
		//$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
		$mail->SMTPAuth = true; // authentication enabled
		
		$mail->CharSet="windows-1251";
		$mail->CharSet="utf-8";
		$mail->WordWrap = 50;      			// set word wrap to 50 characters
		
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->AddAddress($emailId);
		if(!$mail->send()){
			echo "Mailer Error: " . $mail->ErrorInfo;
		}else{
			echo "<br>"."Message has been sent to >>>".$emailId;
		}
	}
	
	function sendActivationMailAction($receiverId,$purpose,$language,$otherData)
	{
		require_once("class.phpmailer.php");
		$mail = new PHPMailer();
		$mail->IsSMTP();                 	// set mailer to use SMTP
		$mail->SMTPAuth = true;     		// turn on SMTP authentication
		$mail->CharSet="windows-1251";
		$mail->CharSet="utf-8";
		$mail->WordWrap = 50;      			// set word wrap to 50 characters
		$mail->IsHTML(true);  
		
		switch ($purpose) {
			case "forgotPassword":
                $emailid = $receiverId;
                $mail->AddAddress($emailid);	//sender user email id
                // get email template and subject form table
                $refundTemplate = get_email_template('reset_password','en');
                $refundTemplateNew = explode("||",$refundTemplate);
                $refundTemplateSubject = $refundTemplateNew[0];
                $refundTemplateBody = $refundTemplateNew[1];
                // replace data in email template
                
                $message = explode('{#STRINGBREAKER#}',$otherData);
                $searchArray = array("{#USERNAME#}","{#PASSWORD#}");
                $replaceArray = array($message[0], $message[1]);
                $refurndTmailBody=str_replace($searchArray, $replaceArray, $refundTemplateBody);	
                $mail->Subject = $refundTemplateSubject;
                $mail->Body    = $refurndTmailBody;
                $mail->send();
			break;

			case "birthday":
                $emailid = $receiverId;
                $mail->AddAddress($emailid);	//sender user email id
                // get email template and subject form table
                $refundTemplate = get_email_template('birthday','en');
                $refundTemplateNew = explode("||",$refundTemplate);
                $refundTemplateSubject = $refundTemplateNew[0];
                $refundTemplateBody = $refundTemplateNew[1];
                // replace data in email templat
                $message = explode('{#STRINGBREAKER#}',$otherData);
                $searchArray  = array("{#USERNAME#}","{#ERNEDPOINT#}");
                $replaceArray = array($message[0], $message[2]);
                $refurndTmailBody=str_replace($searchArray, $replaceArray, $refundTemplateBody);			
                $mail->Subject = $refundTemplateSubject;
                $mail->Body    = $refurndTmailBody;
                $mail->send();
			break;
            
			case "contact_synchronisation_points":
                $emailid = $receiverId;
                $mail->AddAddress($emailid);	//sender user email id
                // get email template and subject form table
                $refundTemplate 	   = get_email_template('sync_contacts','en');
                $refundTemplateNew 	   = explode("||",$refundTemplate);
                $refundTemplateSubject = $refundTemplateNew[0];
                $refundTemplateBody    = $refundTemplateNew[1];
                // replace data in email templat
                $message 	= explode('{#STRINGBREAKER#}',$otherData);
                $searchArray  = array("{#USERNAME#}","{#EARNPOINT#}");
                $replaceArray = array($message[0], $message[2]);
                $refurndTmailBody=str_replace($searchArray, $replaceArray, $refundTemplateBody);			
                $mail->Subject = $refundTemplateSubject;
                $mail->Body    = $refurndTmailBody;
                $mail->send();
			break;
			
			case "registration_points":
                $emailid = $receiverId;
                $mail->AddAddress($emailid);	//sender user email id
                // get email template and subject form table
                $refundTemplate 	   = get_email_template('new_registration','en');
                $refundTemplateNew 	   = explode("||",$refundTemplate);
                $refundTemplateSubject = $refundTemplateNew[0];
                $refundTemplateBody    = $refundTemplateNew[1];
                // replace data in email templat
                $message 	= explode('{#STRINGBREAKER#}',$otherData);
                $searchArray  = array("{#USERNAME#}","{#EARNPOINT#}");
                $replaceArray = array($message[0], $message[2]);
                $refurndTmailBody=str_replace($searchArray, $replaceArray, $refundTemplateBody);			
                $mail->Subject = $refundTemplateSubject;
                $mail->Body    = $refurndTmailBody;
                $mail->send();
			break;
			
			case "application_social_sharing_points":
                $emailid = $receiverId;
                $mail->AddAddress($emailid);	//sender user email id
                // get email template and subject form table
                $refundTemplate 	   = get_email_template('sharing_application','en');
                $refundTemplateNew 	   = explode("||",$refundTemplate);
                $refundTemplateSubject = $refundTemplateNew[0];
                $refundTemplateBody    = $refundTemplateNew[1];
                // replace data in email templat
                $message 	= explode('{#STRINGBREAKER#}',$otherData);
                $searchArray  = array("{#USERNAME#}","{#EARNPOINT#}");
                $replaceArray = array($message[0], $message[2]);
                $refurndTmailBody=str_replace($searchArray, $replaceArray, $refundTemplateBody);			
                $mail->Subject = $refundTemplateSubject;
                $mail->Body    = $refurndTmailBody;
                $mail->send();
			break;
            
            
            default:
                $emailid = $receiverId;
                $mail->AddAddress($emailid);	//sender user email id
                // get email template and subject form table
                $refundTemplate = get_email_template('reward_points','en');
                $refundTemplateNew = explode("||",$refundTemplate);
                $refundTemplateSubject = $refundTemplateNew[0];
                $refundTemplateBody = $refundTemplateNew[1];
            
                // replace data in email templat
                $message = explode('{#STRINGBREAKER#}',$otherData);
                $searchArray = array("{#USERNAME#}","{#EMAILBODY#}");
                $replaceArray = array($message[0], $message[1]);
                $refurndTmailBody=str_replace($searchArray, $replaceArray, $refundTemplateBody);			
                $mail->Subject = $refundTemplateSubject;
                $mail->Body    = $refurndTmailBody;
                $mail->send();
			break;
		}
	}
?>
