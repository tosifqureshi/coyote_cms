<?php

	$root_folder = basename(dirname(dirname(__FILE__))); // -- Get service folder's parent name
	$setup_folder = ($root_folder == 'coyote' ) ? '/coyote' : '/coyotev3'; // -- Set directory name
	define('ROOT', 'var/www/html'.$setup_folder); // --Path to the front controller (this file) --
	$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http"); 	// -- Define URL path to pass in the push notification messages.
    $base_url .= "://". @$_SERVER['HTTP_HOST'];
    $base_url .= $setup_folder;
    $base_url .= "/";
    define('URL_PATH',$base_url);
    define('SERVICE_URL_PATH',$base_url."services/");
    define('FOLDER_URL_PATH',$base_url."promotion/");
?>
