<?php
require_once('config.php'); 
$edmId = !empty($_REQUEST['eid'])?$_REQUEST['eid']:0;
$uId   = !empty($_REQUEST['uid'])?$_REQUEST['uid']:0;
$prId  = !empty($_REQUEST['pr'])?$_REQUEST['pr']:0;
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Night-Owl</title>
      <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   </head>
   <body>
      <div class="loader dn">
         <i class="fa fa-refresh fa-spin" style=" font-size:75px;color:#111;position:absolute;top:50%;left:50%;"></i>
      </div>
      <!-- Navigation -->
      <div class="top-header text-center">
         <img src="img/logo.png">
      </div>
      <nav class="navbar navbar-expand-lg navbar-dark main-menu ">
         <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item active">
                     <a class="nav-link" href="http://nightowl.com.au/stores/">Store
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="http://nightowl.com.au/offers/">Offer</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="http://nightowl.com.au/our-products/">Our Product</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="http://nightowl.com.au/contact/">Contact</a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Page Content -->
      <div class="container bd-container">
         <!-- Jumbotron Header -->
         <header class="jumbotron my-4"></header>
         <!-- Page Features -->
         <div class="row text-center ajdata" style="min-height:250px;"></div>
         <!-- /.row -->
      </div>
      <!-- /.container -->
      <!-- Footer -->
      <footer class="main-footer">
         <div class="container">
            <div class="row">
               <div class="col-md-2 menu-footer">
                  <h6>Stote</h6>
                  <ul class="list-unstyled">
                     <li><a href="http://nightowl.com.au/stores/">Store Locator</a></li>
                  </ul>
               </div>
               <div class="col-md-2 menu-footer">
                  <h6>Our Products</h6>
                  <ul class="list-unstyled">
                     <li><a href="http://nightowl.com.au/cheap-tuesday/"> Cheap Tuesday </a></li>
                     <li><a href="http://nightowl.com.au/frostbite/"> FrostBite </a></li>
                     <li><a href="http://nightowl.com.au/owlcafe/"> OwlCafe </a></li>
                     <li><a href="http://nightowl.com.au/famous-6-food-combo/"> Food Service </a></li>
                  </ul>
               </div>
               <div class="col-md-2 menu-footer">
                  <h6>About Us</h6>
                  <ul class="list-unstyled">
                     <li><a href="http://nightowl.com.au/our-history/">Our History </a></li>
                  </ul>
               </div>
               <div class="col-md-2 menu-footer">
                  <h6>Franchising</h6>
                  <ul class="list-unstyled">
                     <li><a href="http://franchising.nightowl.com.au/"> Franchising </a></li>
                  </ul>
               </div>
               <div class="col-md-2 menu-footer">
                  <h6>Contact Us</h6>
                  <ul class="list-unstyled">
                     <li><a href="http://nightowl.com.au/contact/">Contact </a></li>
                     <li><a href="http://nightowl.com.au/terms/">Terms & Conditions </a></li>
                     <li><a href="https://www.facebook.com/NightOwlConvenience">Facebook </a></li>
                  </ul>
               </div>
            </div>
         </div>
         <!-- /.container -->
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
   </body>
</html>
<script>
	$.ajax({
		type: "GET",
		dataType: "json",
		url: '<?php echo SERVICE_URL_PATH.'reward/edm_product' ?>',
		data: {edmId:'<?php echo $edmId; ?>',uId:'<?php echo $uId; ?>'},
		beforeSend: function() {
			$('.loader').removeClass('dn');
		},
		success: function(data){
		  if(data.product_count){
			  $('.ajdata').html(data.html);
		  }else{
			  $('.bd-container').html("");
			  $('.bd-container').html(data.html);	
		  }
		},
		complete: function() {
        $('.loader').addClass('dn');
		}
	});
</script>
