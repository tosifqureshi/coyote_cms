<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    final class config
    {
        /// -- Hold Static class object --
        private static $ref;
        /// -- Hold Static private class members variables --
        private $_config=array();
        /**
        * @method __construct
        * @see private constructor to protect beign inherited
        * @access private
        * @return void
        */
        private function __construct()
        {

            /*
            |--------------------------------------------------------------------------
            | Web Site URL
            |--------------------------------------------------------------------------
            | Get url of web site with domain name and folder address
            */
            $this->_config['siteUrl'] = $_SERVER["REQUEST_URI"];
            /*
            |--------------------------------------------------------------------------
            | Web Site Language
            |--------------------------------------------------------------------------
            | Get Default language of web sservice with domain name and folder address
            */
            $this->_config['lang'] = "en";
            /*
            |--------------------------------------------------------------------------
            | Web Service Name
            |--------------------------------------------------------------------------
            | Display Web Service Name on Service Interface
            */
            $this->_config['webServiceTitle'] = "Coyote Supermarket App";
            /*
            |--------------------------------------------------------------------------
            | Web soap server Namespace
            |--------------------------------------------------------------------------
            | Display Web Service - create a soap server Namespace
            */
            $this->_config['namespace'] = "Coyote";
            /*
            |--------------------------------------------------------------------------
            | Sql Host Server Name
            |--------------------------------------------------------------------------
            |
            | Name of server which you are trying to connect Ex. (www.chatsupport.co.uk)
            | Please do not allow this use access to "%" to mantain security
            |
            */
			// defining DB_DRIVER_TYPE for mssql driver : for liver server sqlsrv and cdn/local mssql 
			$server_address = getHostByName(php_uname('n'));
            if($server_address == '10.10.10.2' || $server_address == '127.0.1.1') {
				$this->_config["DB_DRIVER_TYPE"] 	=	"sqlsrv";
				$this->_config["SQL_HOST_NAME"] 	=	"10.10.10.2";
				$this->_config['SQL_USER_NAME']	 	=	"coyotev3";
				$this->_config['SQL_PASSWORD']		=	"hzKKPYnCYt9NSMjH";
				$this->_config['SQL_DB']			=	"coyotev3";	
				
				/*mssql database details*/
                /*
				$this->_config["MSSQL_HOST_NAME"] 	=	"db03.coyotepos.com.au";
				$this->_config['MSSQL_USER_NAME'] 	=	"sa";
				$this->_config['MSSQL_PASSWORD']	=	'F1$hrFr13nd$';
				$this->_config['MSSQL_DB']			=	"AA01Sandbox";	
                */
                $this->_config["MSSQL_HOST_NAME"] 	=	"dbs99.coyotepos.com.au";
				$this->_config['MSSQL_USER_NAME'] 	=	"CSAdmin";
				$this->_config['MSSQL_PASSWORD']	=	'$qu1dlyD1ddly';
				$this->_config['MSSQL_DB']			=	"AASandbox";	
				
				//~ $this->_config["LIVE_MSSQL_HOST_NAME"] =	"db01.coyotepos.com.au";
				//~ $this->_config['LIVE_MSSQL_USER_NAME'] 	=	"ws01admin";
				//~ $this->_config['LIVE_MSSQL_PASSWORD']	=	'ACMEwebapp';
				//~ $this->_config['LIVE_MSSQL_DB']			=	"AASystem";
				
				$this->_config["LIVE_MSSQL_HOST_NAME"] 	=	"dbs99.coyotepos.com.au";
				$this->_config['LIVE_MSSQL_USER_NAME'] 	=	"CSAdmin";
				$this->_config['LIVE_MSSQL_PASSWORD']	=	'$qu1dlyD1ddly';
				$this->_config['LIVE_MSSQL_DB']			=	"AASandbox";	
					

			} else if($server_address == '110.232.114.48') {
				$this->_config["DB_DRIVER_TYPE"] 	=	"sqlsrv";
				$this->_config["SQL_HOST_NAME"] 	=	"localhost";
				$this->_config['SQL_USER_NAME'] 	=	"root";
				$this->_config['SQL_PASSWORD']  	=	"";
				$this->_config['SQL_DB']			=	"coyote";	

				/*mssql database details*/
				$this->_config["MSSQL_HOST_NAME"] 	=	"db01.coyotepos.com.au";
				$this->_config['MSSQL_USER_NAME'] 	=	"ws01admin";
				$this->_config['MSSQL_PASSWORD']	=	"ACMEwebapp";
				$this->_config['MSSQL_DB']			=	"AASystem";	

			} else {
				
				$this->_config["DB_DRIVER_TYPE"] 	=	"sqlsrv";
				$this->_config["SQL_HOST_NAME"] 	=	"10.10.10.2";
				$this->_config['SQL_USER_NAME'] 	=	"coyotev3";
				$this->_config['SQL_PASSWORD']		=	"hzKKPYnCYt9NSMjH";
				$this->_config['SQL_DB']			=	"coyotev3";	
				/*mssql database details*/
                /*
				$this->_config["MSSQL_HOST_NAME"] 	=	"db03.coyotepos.com.au";
				$this->_config['MSSQL_USER_NAME'] 	=	"sa";
				$this->_config['MSSQL_PASSWORD']	=	'F1$hrFr13nd$';
				$this->_config['MSSQL_DB']			=	"AA01Sandbox";	
                */
                $this->_config["MSSQL_HOST_NAME"] 	=	"dbs99.coyotepos.com.au";
				$this->_config['MSSQL_USER_NAME'] 	=	"CDNAdmin";
				$this->_config['MSSQL_PASSWORD']	=	'R3@dyPl@y3r1';
				$this->_config['MSSQL_DB']			=	"CoyoteDB";	
				
				 $this->_config["LIVE_MSSQL_HOST_NAME"] =	"db01.coyotepos.com.au";
				$this->_config['LIVE_MSSQL_USER_NAME'] 	=	"ws01admin";
				$this->_config['LIVE_MSSQL_PASSWORD']	=	'ACMEwebapp';
				$this->_config['LIVE_MSSQL_DB']			=	"AASystem";	
			}

            /*
            |--------------------------------------------------------------------------
            | Error Logging Directory Path
            |--------------------------------------------------------------------------
            |
            | Leave this BLANK unless you would like to set something other than the default
            | system/logs/ folder.  Use a full server path with trailing slash.
            |
            */
            $this->_config['log_path'] = '';
            /*
            |--------------------------------------------------------------------------
            | Error Logging Threshold
            |--------------------------------------------------------------------------
            | If you have enabled error logging, you can set an error threshold to
            | determine what gets logged. Threshold options are:
            | You can enable error logging by setting a threshold over zero. The
            | threshold determines what gets logged. Threshold options are:
            |
            |   0 = Disables logging, Error logging TURNED OFF
            |   1 = Error Messages (including PHP errors)
            |   2 = Debug Messages
            |   3 = Informational Messages
            |   4 = All Messages
            | For a live site you'll usually only enable Errors (1) to be logged otherwise
            | your log files will fill up very fast.
            */
            $this->_config['log_threshold'] = 4;
            /*
            |--------------------------------------------------------------------------
            | Date Format for Logs
            |--------------------------------------------------------------------------
            | Each item that is logged has an associated date. You can use PHP date
            | codes to set your own date formatting
            */
            $this->_config['log_date_format']= 'D M j G:i:s T Y';
			 /*
             * Define encryption_key
             * 
             * */
            $this->_config['access_key'] = "367534gfyh78f265262a4221338e5dda35";
            /*
            |--------------------------------------------------------------------------
            | Minimum app versions
            |--------------------------------------------------------------------------
            */
            $this->_config['mini_ios_version'] = '3.1';
            $this->_config['mini_android_version'] = '30';
            /*
            |--------------------------------------------------------------------------
            | Minimum app versions for access CracktheEgg
            |--------------------------------------------------------------------------
            */
            $this->_config['cte_mini_ios_version'] = '3.1';
            $this->_config['cte_mini_android_version'] = '30';
			/*
            |--------------------------------------------------------------------------
            | Push types
            |--------------------------------------------------------------------------
            */
            $this->_config['snw_push_type'] = '5';
            $this->_config['cte_push_type'] = '6';
            /*
            |--------------------------------------------------------------------------
            | Define push api keys
            |--------------------------------------------------------------------------
            */
            $this->_config['gcm_nightowl_key'] = 'AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc';
            $this->_config['fcm_nightowl_key'] = 'AIzaSyAtKf_7TYNrgTtG93k4n2gwaouDmAYgVMI';
            //$this->_config['fcm_tablet_key'] = 'AIzaSyAhmngWQ9LooPBIU4Ys2R_f23FJC1J2Wb8';
            $this->_config['fcm_tablet_key'] = 'AAAAfT1Uhd4:APA91bHW3nRLRQHLR3e5rNirwb-SC5h8n9nmV9n4_hF_JEj8a090YvMhMvvjxMo6eg4XyENzOjzVECzaATx6FY46z8Vz5PNF4HE7jMEWUGu-_fUJFx65M8VmAVT2m6B27gKG8otFtMCb';
            /*
            |--------------------------------------------------------------------------
            |                       Define Twilio auth keys
            |--------------------------------------------------------------------------
            */
            if($server_address == '10.10.10.2' || $server_address == '127.0.1.1') { // Mangesh Sir Account Detail
                $this->_config['twilio_account_sid']	= 'AC3b236463ec9a9fb9670cccedb126f714';
                $this->_config['twilio_auth_token']	 	= 'a83abc5578d9ba5e829fccfd4d07f677';
                $this->_config['twilio_from_no'] 		= '+14803866270';
            }else{ // Live Account Detail
                $this->_config['twilio_account_sid']	= 'ACb5e11f816a1cbffa807441fe3ee7bae1';
                $this->_config['twilio_auth_token']	 	= '93ef6bdeea59d01f23216d802ebc0563';
                $this->_config['twilio_from_no'] 		= 'nightowl';
            }
        }

        /**
        *Config::getInst()
        *@Param Void
        *@RETURN Static Object of Class
        */
        final public static function getInst()
        {
          if(!is_object(config::$ref)){
              config::$ref=new config();
          }
          return config::$ref;
        }

        /**
        *Config::getConfig Array()
        *@Param Void
        *@RETURN Array of Settings
        */
        final public function getSettings()
        {
            return $this->_config;
        }

        /**
        *config::getKeyValue Array()
        *@Param key STRING
        *@RETURN Array of Settings
        */
        final public function getKeyValue($key)
        {
            if(array_key_exists($key,$this->_config))
            {
              return $this->_config[$key];
            }
        }

        /**
        *config::getKeyValue Array()
        *@Param key STRING
        *@RETURN Array of Settings
        */
        public function setLang($idiom)
        {
            if(array_key_exists("lang",$this->_config))
            {
              $this->_config["lang"]=$idiom;
            }
        }
    }///  -- Class:config ENDS --
?>
