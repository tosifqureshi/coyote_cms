<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    /**
     * Timestamp : 01-Oct-2015 01:22 PM
     * Copyright : www.cdnsol.com
     */
    final class pushnotifications {

        public $_parents = array();
        public $_data = array();
        
        /// -- Hold Static Connection Object --
        private static $ref;
        /// -- Hold Connection Object --
        
        public function __construct() {
            /// -- Create config instance --
			 $this->_config  = config::getInst();
        }
        
        /**
         * final static env::getInst
         * @access Public
         * @return Object
         */
        final public static function getInst(){
            if(!is_object(pushnotifications::$ref))
            {
                pushnotifications::$ref=new pushnotifications(config::getInst());
            }
            return pushnotifications::$ref;
        }
        
        /**
         *  This function is for send push notifications on user iPhone device - Start. 
        **/
        public function sendIphonePushNotification($deviceId) 
        {
            // Adjust to your timezone
            //date_default_timezone_set('Europe/Rome');

            // Report all PHP errors
            error_reporting(1);
            
            require_once 'pushNotifications/ApnsPHP/Autoload.php';
            
            // Instanciate a new ApnsPHP_Push object
            $push = new ApnsPHP_Push( ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' );
            
            // Set the Root Certificate Autority to verify the Apple remote peer
            //$push->setRootCertificationAuthority('entrust_root_certification_authority.pem');

            // Connect to the Apple Push Notification Service
            $push->connect();
			
            foreach ($deviceId as $key => $value)
            {
                $device_Id = $value['deviceId'];                
                $offer_id = $value['offer_id'];                
                $offer_name = $value['offer_name'];                
                $offer_short_description = $value['offer_short_description'];                
                $offer_short_description1 = '';
                $checkId = 1;
                
                $message = new ApnsPHP_Message($device_Id);
                // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
                // over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("Message-Badge-3");
                $badgeCount = intval($checkId);
                $message->setBadge($badgeCount);
                // Set a simple welcome text
                $message->setText($offer_name);
                // Play the default sound
                $message->setSound();
                // Set a custom property
                $message->setCustomProperty('post_data', array('offer_id'=>$offer_id,'offer_name'=>$offer_name,'offer_short_description'=>$offer_short_description1,'push_type'=>'1'));
                // Set another custom property
                //$message->setCustomProperty('acme3', array('bing', 'bong'));
                // Set the expiry value to 30 seconds
                //$message->setExpiry(30);
                // Add the message to the message queue
                $push->add($message);
                
            }

            // Send all messages in the message queue
            $push->send();

            // Disconnect from the Apple Push Notification Service
            $push->disconnect();

            // Examine the error message container
            $aErrorQueue = $push->getErrors();
            if (!empty($aErrorQueue)) {
                //var_dump($aErrorQueue);
            }
        }
        /** This function is for send push notifications on user iPhone device - End **/
        
        /**
         *  This function is for send push notifications on user android device - Start. 
        **/
        public function sendAndroidPushNotification($details){
			
            /* Send Push on Android  Device*/
            foreach ($details as $key => $value)
            {
				$device_Id 		= $value['deviceId'];                
                $offer_id 		= $value['offer_id'];                
                $offer_name 	= $value['offer_name'];                
                $pushMessage 	= $value['offer_short_description'];     
                
                $apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
				//$apiKey = "AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc";
				// Replace with real client registration IDs ('Device ID') 
				$registatoin_ids = array($device_Id);
                $message = array('offer_id'=>$offer_id,'offer_name'=>$offer_name,'offer_short_description'=>$pushMessage,'push_type'=>'1');
				$url = 'https://android.googleapis.com/gcm/send';
				$fields = array('registration_ids' => $registatoin_ids,'data' => $message);
				$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
				 
				// Open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				// Execute post
				$result = curl_exec($ch);
				if ($result === FALSE) {
					die('Curl failed: ' . curl_error($ch));
				}

				// Close connection
				curl_close($ch);
				//return $result;
			}
			return true;
            
        }
        
		/**
         *  This function is for send push notifications on user android device - Start.  /* In case of supplier
        **/
        public function sendAndroidPushNotification_order_tablet($device_ids,$details){
			
			$result = false;
			if(!empty($details)) {
				try {
					// set order push input params    
					$order_id 		    = $details['order_id'];                
					$order_description 	= $details['order_description'];     
					$push_type      	= $details['push_type'];       
					// set FCM api key
					$apiKey = $this->_config->getKeyValue("fcm_tablet_key"); 
					// cast device token in array
					$registatoin_id = $device_ids;
					// set push message params
					$message = array('offer_id'=>$order_id,'offer_name'=>$order_description,'offer_short_description'=>$order_description,'push_type'=>$push_type);
					$fields  = array('registration_ids' => $registatoin_id,'notification' => $message,"data" => $message);
					$header  = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
					// open connection
					$ch = curl_init("https://fcm.googleapis.com/fcm/send");
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
					// execute post
					$result = curl_exec($ch);
					curl_close($ch);
				} catch(Exception $e) {
					$result = false;
				}
			}
			return $result;
        }
        
        
         /**
         *  This function is for send push notifications on user android device - Start. 
        **/
        public function sendAndroidPushNotification_order($details){
			
       /* You are free to modify at your own risk */
            foreach ($details as $key => $value)
            {
				$device_id 		= $value['device_id'];                
                $order_id 		= $value['order_id'];                
                $order_description 	= $value['order_description'];     
                $push_type 	= $value['push_type'];     
                
                $apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
				//$apiKey = "AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc";
				// Replace with real client registration IDs ('Device ID') 
				$registatoin_ids = array($device_id);
                
                $message = array('offer_id'=>$order_id,'offer_name'=>$order_description,'offer_short_description'=>$order_description,'push_type'=>$push_type);
				$url = 'https://android.googleapis.com/gcm/send';
				$fields = array('registration_ids' => $registatoin_ids,'data' => $message);
				$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
				 
				// Open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				// Execute post
				$result = curl_exec($ch);
				if ($result === FALSE) {
					die('Curl failed: ' . curl_error($ch));
				}

				// Close connection
				curl_close($ch);
                return $result;
				
			}
            
        }
        /** This function is for send push notifications on user Android device - End **/
        
        
        
        /**
         *  This function is for send push notifications on user iPhone device - Start. 
        **/
        public function sendIphonePushNotification_order($deviceId) 
        {
            
            // Report all PHP errors
            error_reporting(1);
            
            require_once 'pushNotifications/ApnsPHP/Autoload.php';
            
            // Instanciate a new ApnsPHP_Push object
            $push = new ApnsPHP_Push( ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' );
            
            // Connect to the Apple Push Notification Service
            $push->connect();
			
            foreach ($deviceId as $key => $value)
            {
                $device_id = $value['device_id'];                
                $order_id = $value['order_id'];                
                $order_description = $value['order_description'];                
                $push_type = $value['push_type'];                
                $checkId = 1;
                $message = new ApnsPHP_Message($device_id);
                // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
                // over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("Message-Badge-3");
                $badgeCount = intval($checkId);
                $message->setBadge($badgeCount);
                // Set a simple welcome text
                $message->setText($order_description);
                // Play the default sound
                $message->setSound();
                // Set a custom property
                
                $message->setCustomProperty('post_data', array('offer_id'=>$order_description,'offer_name'=>$order_description,'offer_short_description'=>$order_description,'push_type'=>$push_type));
                // Set the expiry value to 30 seconds
                //$message->setExpiry(30);
                // Add the message to the message queue
                $push->add($message);
                
            }

            // Send all messages in the message queue
            $push->send();

            // Disconnect from the Apple Push Notification Service
            $push->disconnect();

            // Examine the error message container
            $aErrorQueue = $push->getErrors();
            if (!empty($aErrorQueue)) {
                //var_dump($aErrorQueue);
            }
        }
        
        /**
         *  This function is for send push notifications on user iPhone device - Start. 
        **/
        public function sendIphonePushNotification_target($deviceId) 
        {
            // Adjust to your timezone
            //date_default_timezone_set('Europe/Rome');
		
            // Report all PHP errors
            error_reporting(1);
            
            require_once 'pushNotifications/ApnsPHP/Autoload.php';
            
            // Instanciate a new ApnsPHP_Push object
            $push = new ApnsPHP_Push( ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' );
            
            // Set the Root Certificate Autority to verify the Apple remote peer
            //$push->setRootCertificationAuthority('entrust_root_certification_authority.pem');

            // Connect to the Apple Push Notification Service
            $push->connect();
			
            foreach ($deviceId as $key => $value)
            {
                $device_Id = $value['deviceId'];                
                $offer_id = $value['offer_id'];                
                $offer_name = $value['offer_name'];                
                $offer_short_description = $value['offer_short_description'];                
                $offer_short_description1 = '';
                $checkId = 1;
                
                $message = new ApnsPHP_Message($device_Id);
                // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
                // over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("Message-Badge-3");
                $badgeCount = intval($checkId);
                $message->setBadge($badgeCount);
                // Set a simple welcome text
                $message->setText($offer_name);
                // Play the default sound
                $message->setSound();
                // Set a custom property
                $message->setCustomProperty('post_data', array('offer_id'=>$offer_id,'offer_name'=>$offer_name,'offer_short_description'=>$offer_short_description1,'push_type'=>'2'));
                // Set another custom property
                //$message->setCustomProperty('acme3', array('bing', 'bong'));
                // Set the expiry value to 30 seconds
                //$message->setExpiry(30);
                // Add the message to the message queue
                $push->add($message);
                
            }

            // Send all messages in the message queue
            $push->send();

            // Disconnect from the Apple Push Notification Service
            $push->disconnect();

            // Examine the error message container
            $aErrorQueue = $push->getErrors();
            if (!empty($aErrorQueue)) {
                var_dump($aErrorQueue);
            }
        }
        /** This function is for send push notifications on user iPhone device - End **/
        
        /**
         *  This function is for send push notifications on user android device - Start. 
        **/
        public function sendAndroidPushNotification_target($details){
            /* Send Push on Android  Device*/
			
            foreach ($details as $key => $value)
            {
				$device_Id 		= $value['deviceId'];                
                $offer_id 		= $value['offer_id'];                
                $offer_name 	= $value['offer_name'];                				
                $pushMessage 	= $value['offer_short_description'];				  
                
				//$apiKey = "AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc";
				$apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
				// Replace with real client registration IDs ('Device ID') 
				$registatoin_ids = array($device_Id);
                           
                $message = array('offer_id'=>$offer_id,'offer_name'=>$offer_name,'offer_short_description'=>$pushMessage,'push_type'=>'2');
				$url = 'https://android.googleapis.com/gcm/send';
				$fields = array('registration_ids' => $registatoin_ids,'data' => $message);
				$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
				 
				// Open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				// Execute post
				$result = curl_exec($ch);
				
                if ($result === FALSE) {
					die('Curl failed: ' . curl_error($ch));
				}

				// Close connection
				curl_close($ch);
				//print_r($result);
				return $result;
				
			}
        }
        
        /**
         * Function used to send prize push to andriod devices
         * @param array data 
		 * @return void
		 * 
         */
         public function sendAndroidPushNotification_prize($device_ids,$snw_id,$title,$push_sent_at) {
			$return_response = false;
			if(!empty($device_ids) && count($device_ids) > 0) {
				//$apiKey = "AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc";
				$apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
				// Replace with real client registration IDs ('Device ID') 
				$registatoin_ids = $device_ids;
				$message = array('title'=>'New Giveaways','offer_id'=>$snw_id,'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'5','push_sent_at'=>$push_sent_at,'push_count'=>1);
				$url = 'https://android.googleapis.com/gcm/send';
				$fields = array(
				'delay_while_idle' => true,
				'priority' => 'high',
				'data' => $message,
				'registration_ids' => $registatoin_ids
				);
				
				$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
				 
				// Open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				// Execute post
				$result = curl_exec($ch);
				
				if ($result === FALSE) {
					//die('Curl failed: ' . curl_error($ch));
				}

				// Close connection
				curl_close($ch);
				$return_response = $result;
			}
			//print_r($return_response);die;
			return $return_response;
        }
        
        /**
         * Function used to send prize push to ios devices
         * @param array data 
		 * @return void
		 * 
         */
		public function sendIphonePushNotification_prize($device_ids,$snw_id,$title,$push_sent_at,$snw_total_count) {
			if(!empty($device_ids) && count($device_ids) > 0) {
				// Using Autoload all classes are loaded on-demand
				require_once 'pushNotifications/ApnsPHP/Autoload.php';	
				$push = new ApnsPHP_Push(
						ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, 
						dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' 
						);
				
				// Connect push server
				$push->connect();
				// Add multiple devices in single send
				for($i=0;$i<count($device_ids);$i++) {
					// set giveaways count
					$giveaway_count = $device_ids[$i]['giveaway_count'];
					$prize_count 	= $device_ids[$i]['prize_count'];
					$viewed_count 	= $device_ids[$i]['viewed_count'];
					$total_badge_count = $snw_total_count;
					if($giveaway_count != '') {
						$total_badge_count = $giveaway_count+$prize_count;
						if($viewed_count > 0 ) {
							$total_badge_count = ($snw_total_count-$viewed_count)+$prize_count;
						}
					}
					$message = new ApnsPHP_Message($device_ids[$i]['device_id']);
					// Set a custom identifier
					$message->setCustomIdentifier("Message-Badge-3");
					$badgeCount = intval($total_badge_count);
					$message->setBadge($badgeCount);
					// Set a simple welcome text			
					$message->setText($title);
					// Set a custom property
					$message->setCustomProperty('post_data', array('title'=>'New Giveaways','offer_id'=>$snw_id,'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'5','push_sent_at'=>$push_sent_at));		
					// Play the default sound
					$message->setSound();							
					// Add the message to the message queue
					$push->add($message);
				}
				// Send iOS push
				$data =$push->send();
				// Disconnect push server	
				$push->disconnect();
				$aErrorQueue = $push->getErrors();
				if (!empty($aErrorQueue)) {
					//var_dump($aErrorQueue);
				}
			}
        }
        
       /**
        * Function used for test scratch&win notification 
        */
       function test_tab($device_ids,$snw_id,$title,$push_sent_at) {
		   
			$apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
			// Replace with real client registration IDs ('Device ID')
			$registatoin_ids = $device_ids;
			$message = array('title'=>'New Giveaways','offer_id'=>$snw_id,'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'5','push_sent_at'=>$push_sent_at,'push_count'=>1);
			
			$url = 'https://android.googleapis.com/gcm/send';
			//$url = "https://fcm.googleapis.com/fcm/send";
			
			$fields = array(
				'delay_while_idle' => true,
				'priority' => 'high',
				'data' => $message,
				'registration_ids' => $registatoin_ids
			);
			
			$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
			 
			// Open connection
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			// Execute post
			$result = curl_exec($ch);
			
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($ch));
			}

			// Close connection
			curl_close($ch);
			print_r($result);die;
			
			return $result;
	   }
		
	/*------------------------------------------------------------
	 | Function for send curl multipal request simultanousaly
	 ------------------------------------------------------------*/
	function MultiRequests($urls,$data,$apiKey) {
		$curlMultiHandle = curl_multi_init();

		$curlHandles = array();
		$responses = array();

		foreach($urls as $id => $url) {
			$curlHandles[$id] = $this->CreateHandle($url , $data[$id],$apiKey);
			curl_multi_add_handle($curlMultiHandle, $curlHandles[$id]);
		}
		
		$running = null;
		do {
			curl_multi_exec($curlMultiHandle, $running);
		} while($running > 0);
		
		foreach($curlHandles as $id => $handle) {
			
			$responses[$id] = curl_multi_getcontent($handle);
			
			curl_multi_remove_handle($curlMultiHandle, $handle);
			curl_close($handle);
		}
		curl_multi_close($curlMultiHandle);

		return $responses;
	}
	
	
	/*--------------------------------------------------------------
	 | Initiate Function for send curl multipal request simultanousaly
	 ----------------------------------------------------------------
	*/
	function CreateHandle($url,$data,$apiKey) {
		$curlHandle = curl_init($url);
		$headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);
		$defaultOptions = array (
			CURLOPT_HTTPHEADER =>$headers,		
			CURLOPT_ENCODING => "gzip" ,
			CURLOPT_FOLLOWLOCATION => true ,
			CURLOPT_RETURNTRANSFER => true ,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => json_encode($data)
		);

		$test = curl_setopt_array($curlHandle , $defaultOptions);

		return $curlHandle;
	}
    
    
       
	/**
	 * Function used to send crack the egg push to iphone device
	 * @param array data 
	 * @return void
	 * 
	 */
	public function sendIphonePushNotification_cte($device_ids,$title,$push_sent_at) {
		if(!empty($device_ids) && count($device_ids) > 0) {
			// Using Autoload all classes are loaded on-demand
			require_once 'pushNotifications/ApnsPHP/Autoload.php';	
			$push = new ApnsPHP_Push(
					ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, 
					dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' 
					);
			
			// Connect push server
			$push->connect();
			// Add multiple devices in single send
			for($i=0;$i<count($device_ids);$i++) {
				// set giveaways count
				$giveaway_count = $device_ids[$i]['giveaway_count'];
				$prize_count 	= $device_ids[$i]['prize_count'];
				$viewed_count 	= $device_ids[$i]['viewed_count'];
				
				$message = new ApnsPHP_Message($device_ids[$i]['device_id']);
				// Set a custom identifier
				$message->setCustomIdentifier("Message-Badge-3");
				$badgeCount = intval($total_badge_count);
				$message->setBadge($badgeCount);
				// Set a simple welcome text			
				$message->setText($title);
				// Set a custom property
				$message->setCustomProperty('post_data', array('title'=>'New Giveaways','offer_id'=>$device_ids[$i]['pushlog_id'],'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'6','push_sent_at'=>$push_sent_at));		
				// Play the default sound
				$message->setSound();							
				// Add the message to the message queue
				$push->add($message);
			}
			// Send iOS push
			$data =$push->send();
			// Disconnect push server	
			$push->disconnect();
			$aErrorQueue = $push->getErrors();
			if (!empty($aErrorQueue)) {
				//var_dump($aErrorQueue);
			}
		}
	}
	
	/**
	 * Function used to send push for crack the egg to andriod devices
	 * @param array data 
	 * @return void
	 * 
	 */
	 public function sendAndroidPushNotification_cte($device_ids,$title,$push_sent_at) {
		$return_response = false;
		if(!empty($device_ids) && count($device_ids) > 0) {
			for($i=0;$i<count($device_ids);$i++) {
				//$apiKey = "AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc";
				$apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
				// Replace with real client registration IDs ('Device ID') 
				$registatoin_ids = array($device_ids[$i]['device_id']);
				$message = array('title'=>'New Giveaways','offer_id'=>$device_ids[$i]['pushlog_id'],'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'6','push_sent_at'=>$push_sent_at,'push_count'=>1);
				$url = 'https://android.googleapis.com/gcm/send';
				$fields = array(
				'delay_while_idle' => true,
				'priority' => 'high',
				'data' => $message,
				'registration_ids' => $registatoin_ids
				);
				
				$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
				 
				// Open connection
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Disabling SSL Certificate support temporarly
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				// Execute post
				$result = curl_exec($ch);
				if ($result === FALSE) {
					//die('Curl failed: ' . curl_error($ch));
				}

				// Close connection
				curl_close($ch);
				$return_response = $result;
			}   
		}
		//print_r($return_response);die;
		return $return_response;
	}
	
	/**
	 * Function used to send reward push to andriod devices
	 * @param device_ids(array), push_msg(string) 
	 * @return void
	 * 
	 */
	 public function send_android_reward_push($device_ids,$push_msg) {
		$return_response = false;
		if(!empty($device_ids) && count($device_ids) > 0) {
			$apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
			// Replace with real client registration IDs ('Device ID') 
			$registatoin_ids = $device_ids;
			$message = array('title'=>'Reward Points','offer_name'=>$push_msg,'offer_short_description'=>'','push_type'=>'7');
			$url = 'https://android.googleapis.com/gcm/send';
			$fields = array(
			'delay_while_idle' => true,
			'priority' => 'high',
			'data' => $message,
			'registration_ids' => $registatoin_ids
			);
			
			$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
			// Open connection
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			// Execute post
			$result = curl_exec($ch);
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($ch));
			}
			// Close connection
			curl_close($ch);
			$return_response = $result;
		}
		return $return_response;
	}
	
	/**
	 * Function used to send reward push to ios devices
	  * @param device_ids(array), push_msg(string) 
	 * @return void
	 * 
	 */
	public function send_ios_reward_push($device_ids,$push_msg) {
		if(!empty($device_ids) && count($device_ids) > 0) {
			try{
				// Using Autoload all classes are loaded on-demand
				require_once 'pushNotifications/ApnsPHP/Autoload.php';	
				$push = new ApnsPHP_Push(ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, dirname(__DIR__).'/core/pushNotifications/apns-dev.pem');
				// Connect push server
				$push->connect();
				// Add multiple devices in single send
				for($i=0;$i<count($device_ids);$i++) {
					$message = new ApnsPHP_Message($device_ids[$i]);
					// Set a custom identifier
					$message->setCustomIdentifier("Message-Badge-3");
					$badgeCount = 1;
					$message->setBadge($badgeCount);
					// Set a simple welcome text			
					$message->setText($push_msg);
					// Set a custom property
					$message->setCustomProperty('post_data', array('title'=>'Reward Points','offer_name'=>$push_msg,'offer_short_description'=>'','push_type'=>'7'));		
					// Play the default sound
					$message->setSound();							
					// Add the message to the message queue
					$push->add($message);
				}
				// Send iOS push
				$data =$push->send();
				// Disconnect push server	
				$push->disconnect();
				$aErrorQueue = $push->getErrors();
				if (!empty($aErrorQueue)) {
					//var_dump($aErrorQueue);
				}
			}catch(Exception $e) {
				return true;
			}
		}
	}
	
	/**
	 * Function used to send prize push to ios devices
	 * @param array data 
	 * @return void
	 * 
	 */
	public function sendIphoneCtePushNotification($device_ids,$cte_id,$title,$push_sent_at,$snw_total_count) {
		if(!empty($device_ids) && count($device_ids) > 0) {
			// Using Autoload all classes are loaded on-demand
			require_once 'pushNotifications/ApnsPHP/Autoload.php';	
			$push = new ApnsPHP_Push(
					ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, 
					dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' 
					);
					
			// Connect push server
			$push->connect();
			
			// Add multiple devices in single send
			for($i=0;$i<count($device_ids);$i++) {
				// set giveaways count
				$giveaway_count = $device_ids[$i]['giveaway_count'];
				$prize_count 	= $device_ids[$i]['prize_count'];
				$viewed_count 	= $device_ids[$i]['viewed_count'];
				$total_badge_count = $snw_total_count;
				if($giveaway_count != '') {
					$total_badge_count = $giveaway_count+$prize_count;
					if($viewed_count > 0 ) {
						$total_badge_count = ($snw_total_count-$viewed_count)+$prize_count;
					}
				}
				
				$message = new ApnsPHP_Message($device_ids[$i]['device_id']);
				// Set a custom identifier
				$message->setCustomIdentifier("Message-Badge-3");
				$badgeCount = intval($total_badge_count);
				$message->setBadge($badgeCount);
				// Set a simple welcome text			
				$message->setText($title);
				// Set a custom property
				$message->setCustomProperty('post_data', array('title'=>'New Giveaways','offer_id'=>$cte_id,'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'6','push_sent_at'=>$push_sent_at));		
				// Play the default sound
				$message->setSound();							
				// Add the message to the message queue
				$push->add($message);
			}
			// Send iOS push
			$data =$push->send();
			// Disconnect push server	
			$push->disconnect();
			$aErrorQueue = $push->getErrors();
			if (!empty($aErrorQueue)) {
				var_dump($aErrorQueue);
			}
		}
	}
        
	/**
	 * Function used to send prize push to andriod devices
	 * @param array data 
	 * @return void
	 * 
	 */
	 public function sendAndroidCtePushNotification($device_ids,$cte_id,$title,$push_sent_at) {
		$return_response = false;
		if(!empty($device_ids) && count($device_ids) > 0) {
			//$apiKey = "AIzaSyBQYa7k7EyhTObvlpi37X-WVp26hGLt6jc";
			$apiKey = $this->_config->getKeyValue("gcm_nightowl_key");
			// Replace with real client registration IDs ('Device ID') 
			$registatoin_ids = $device_ids;
			$message = array('title'=>'New Giveaways','offer_id'=>$cte_id,'offer_name'=>$title,'offer_short_description'=>'','push_type'=>'6','push_sent_at'=>$push_sent_at,'push_count'=>1);
			$url = 'https://android.googleapis.com/gcm/send';
			$fields = array(
			'delay_while_idle' => true,
			'priority' => 'high',
			'data' => $message,
			'registration_ids' => $registatoin_ids
			);
			
			$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
			 
			// Open connection
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

			// Execute post
			$result = curl_exec($ch);
			
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($ch));
			}

			// Close connection
			curl_close($ch);
			$return_response = $result;
		}
		//print_r($return_response);die;
		return $return_response;
	}
}

