<?php
	/**
	 * -----------------------------------------
	 * Generate Ean 13 barcodes
	 * -----------------------------------------
	 */
	 
	 // include barcode class
	include_once('Zend/Barcode.php');
	// generate barcode of ean13 formate
	$_SESSION['is_mandatory_checksum'] = true;
	if(isset($acc_card_number) && !empty($acc_card_number)) {
		$_SESSION['is_mandatory_checksum'] = false;
	}
	$file = Zend_Barcode::draw('ean13', 'image', array('text' => $barcode), array());
	// set user barcode upload image path
	$dir_path = IMG_FOLDER_PATH.'user_barcodes/';
	// set barcode image name
	$image_name = $barcode.'.png';
	if($account_number) {
		$image_name = $account_number.'.png';
	}
	// create image from barcode
	ImagePNG($file, $dir_path.$image_name);
	ImageDestroy($file);
	
?>
