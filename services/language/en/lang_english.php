<?php
/***-COMMON LANGUAGE MASSAGE FOR ALL CLASS :Start-***/
//$_lang['txt.error.session'] = "Your session has expired. Please login again.";
$_lang['txt.error.session'] = "Customer Id can't be empty.";
$_lang['txt.error.insert'] = "Error in inserting record";

/***-COMMON LANGUAGE MASSAGE FOR ALL CLASS :End-***/


/***-LANGUAGE MASSAGE FOR "users/register" :Start-***/
$_lang['txt.register.error.required'] = "Please complete all required fields.";
$_lang['txt.register.error.unique'] = "Email is already registered.";
$_lang['txt.register.error.invalidemail'] = "Please enter valid email format.";
$_lang['txt.register.success'] = "You are registered successfully!";
$_lang['txt.register.error.device_details'] = "Error due to update device details.";
$_lang['txt.register.success.device_details'] = "Successfully updated.";
$_lang['txt.register.invalid_request'] = "Invalid request.Please check your input details!";
$_lang['txt.register.otp_success'] = "Verification Code sent to your mobile number! Please verify your account.";
$_lang['txt.register.otp_error'] = "Error during Verification Code sending.";
$_lang['txt.register.invalid_otp'] = "Verification Code is invalid or expired!";
$_lang['txt.register.already_exist'] = "Account already exists!";
$_lang['txt.register.number_already_exist'] = "Number already associated with other user!";
$_lang['txt.register.mobile_updated'] = "Mobile number updated.";
$_lang['txt.register.mobile_otp_updated'] = "Verification Code sent to your mobile number! Please verify your account.";
$_lang['txt.register.already_sent_otp'] = "Verification Code already sent to your number! Please verify your account.";
$_lang['txt.register.account_verified'] = "Account verified.";
$_lang['txt.register.no.facebook_feed'] = "No facebook feed available.";
$_lang['txt.register.invalid_refernce_code'] = "Invalid refernce code.";
/***-LANGUAGE MASSAGE FOR "users/register" :End-***/


/***-LANGUAGE MESSAGE FOR "users/login" :Start-***/
$_lang['txt.login.error.required'] 	= "Username and password should not be blank.";
$_lang['txt.login.error.active_0'] 	= "Your account is not active yet.";
$_lang['txt.login.error.active_1'] 	= "Your account is not varified yet.";
$_lang['txt.login.error.active_2'] 	= "You have an inactive account.";
$_lang['txt.login.error.active_3'] 	= "Your account is suspended.";
$_lang['txt.login.error.delete'] 	= "Your account has been deleted.";
$_lang['txt.login.error.usrnotexist'] 	= "Username does not exist.";
$_lang['txt.login.error.pswdnotexist'] 	= "Incorrect password entered.";
$_lang['txt.login.success'] 		= "Login successful.";
$_lang['txt.login.error.not_verified'] = "Please verify your account!";
/***-LANGUAGE MESSAGE FOR users/login :End-***/

/***-LANGUAGE MESSAGE FOR "users/social_login" :Start-***/
$_lang['txt.user.social_login.error.required'] 		= "Social Id is required.";
$_lang['txt.user.social_login.error.notexist'] 		= "Given Social id is not found.";
$_lang['txt.user.social_login.error.exist_login'] 	= "Facebook user exits and login.";
$_lang['txt.user.custom.success.msg'] 		= "Order dispatch successfully.";
$_lang['txt.login.error.active_0'] 	= "Your account is not active yet.";
$_lang['txt.login.error.active_1'] 	= "Your account is not varified yet.";
$_lang['txt.login.error.active_2'] 	= "You have an inactive account.";
$_lang['txt.login.error.active_3'] 	= "Your account is suspended.";
$_lang['txt.user.social_login.success'] 			= "Login Successful.";


/***-LANGUAGE MESSAGE FOR users/social_login :End-***/


/***-LANGUAGE MESSAGE FOR "users/change_user" :Start-***/
$_lang['txt.change_user.success'] = "Profile updated successfully.";
$_lang['txt.success.report_created'] = "report created successfully.";
$_lang['txt.change_user.error.unable'] = "Unable to update profile.";
$_lang['txt.change_user.error.mobile_exist'] = "Can't update.Mobile number associated with another account";
/***-LANGUAGE MESSAGE FOR "users/change_user" :End-***/

/***-LANGUAGE MESSAGE FOR "users/get_user_profile" :Start-***/
$_lang['txt.get_user_profile.success'] = "Profile listed successfully.";
$_lang['txt.get_user_profile.error.notfound'] = "User not found.";
/***-LANGUAGE MESSAGE FOR "users/get_user_profile" :End-***/

/***-LANGUAGE MESSAGE FOR "users/change_user_password" :Start-***/
$_lang['txt.change_user_password.error.required'] = "Password fields can not be blank.";
$_lang['txt.change_user_password.error.current_password'] = "Current password does not match.";
$_lang['txt.change_user_password.success'] = "Password updated successfully.";
/***-LANGUAGE MESSAGE FOR "users/change_user_password" :End-***/


/**-LANGUAGE MESSAGE FOR "users/forgot_password" :Start-**/
$_lang['txt.forgot_password.error.required'] = "Email address is required.";
$_lang['txt.forgot_password.error.email_not_exist'] = "Email address not found.";
$_lang['txt.forgot_password.success'] = "A reset password email has been sent.";
/**-LANGUAGE MESSAGE FOR "users/forgot_password" :End-**/


/**-LANGUAGE MESSAGE FOR "users/get_data" :Start-**/
$_lang['txt.get_data.success'] = "Record successfully listed.";

/**-LANGUAGE MESSAGE FOR "users/get_data" :End-**/


/**-LANGUAGE MESSAGE FOR "offers/get_offer" :Start-**/
$_lang['txt.get_offer.error.required'] = "Beacon and store is required.";
$_lang['txt.get_offer.error.no_offer'] = "No offer available.";

/**-LANGUAGE MESSAGE FOR "offers/get_offer" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_offer_details" :Start-**/
$_lang['txt.get_offer_details.error.required'] = "Offer Id not is required.";
$_lang['txt.get_offer_details.error.no_offer'] = "No offer available.";

/**-LANGUAGE MESSAGE FOR "offers/get_offer_details" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_beacon_offer" :Start-**/
$_lang['txt.get_beacon_offer.error.required'] = "Beacon Id and Customer Id can't be empty.";
$_lang['txt.get_beacon_offer.error.no_offer'] = "No offer available for given beacon.";
$_lang['txt.get_snw_beacon_offer.error.no_offer'] = "No campaign offer available for given beacon.";

/**-LANGUAGE MESSAGE FOR "offers/get_beacon_offer" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_all_vip_offers" :Start-**/
$_lang['txt.get_all_vip_offers.error.required'] = "Customer Id can't be empty.";
$_lang['txt.get_all_vip_offers.error.no_offer'] = "No offer available.";

/**-LANGUAGE MESSAGE FOR "offers/get_all_vip_offers" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_vip_offer" :Start-**/
$_lang['txt.get_vip_offer.success'] = "offer added into push queue";
$_lang['txt.get_vip_offer.error.already'] = "offer already in push queue";
/**-LANGUAGE MESSAGE FOR "offers/get_vip_offer" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_beacon" :Start-**/
$_lang['txt.get_beacon.error.no_beacon'] = "No beacon found.";
/**-LANGUAGE MESSAGE FOR "offers/get_beacon" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_store" :Start-**/
$_lang['txt.get_store.error.no_store'] = "No store available!";
/**-LANGUAGE MESSAGE FOR "offers/get_store" :End-**/


/**-LANGUAGE MESSAGE FOR "sendPush/send_beacon_offer_push" :Start-**/
$_lang['txt.sendPush.send_beacon_offer_push.success.added'] = "Offer added into push queue.";
$_lang['txt.sendPush.send_beacon_offer_push.error.no_offer'] = "No offer available to send push .";
$_lang['txt.sendPush.send_beacon_offer_push.success.send'] = "Push send successful.";
/**-LANGUAGE MESSAGE FOR "sendPush/send_beacon_offer_push" :End-**/

/**-LANGUAGE MESSAGE FOR "users/logout" :Start-**/
$_lang['txt.logout.success'] = "Logout successful.";
/**-LANGUAGE MESSAGE FOR "users/logout" :End-**/

/**-LANGUAGE MESSAGE FOR "users/push_setting" :Start-**/
$_lang['txt.push_setting.error.required'] = "Parameter is missing.";
$_lang['txt.push_setting.success'] = "Push settings updated successfully.";
/**-LANGUAGE MESSAGE FOR "users/push_setting" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_event_list" :Start-**/
$_lang['txt.get_event_list.error.no_event'] = "Currently no event is running.";
/**-LANGUAGE MESSAGE FOR "offers/get_event_list" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_promotion_list" :Start-**/
$_lang['txt.get_promotion_list.error.no_promo'] = "No active promotion found.";
/**-LANGUAGE MESSAGE FOR "offers/get_promotion_list" :End-**/

/**-LANGUAGE MESSAGE FOR "offers/get_loyalty_list" :Start-**/
$_lang['txt.get_loyalty_list.error.no_loyality'] = "Currently no loyalty program is running.";
$_lang['txt.get_loyalty_list.error.required'] = "Please provide latitude and logitute.";
/**-LANGUAGE MESSAGE FOR "offers/get_loyalty_list" :End-**/

/**-LANGUAGE MESSAGE FOR "competition/get_competitions" :Start-**/
$_lang['txt.get_competitions.error.no_competition'] = "Currently no competition is running.";
$_lang['txt.get_competitions.error.required'] = "Please provide member id.";
$_lang['txt.get_competitions.error.not_valid'] = "Please use valid competition entry.";
/**-LANGUAGE MESSAGE FOR "competition/get_competitions" :End-**/

/**-LANGUAGE MESSAGE FOR "ordering/outlets" :Start-**/
$_lang['txt.outlet.error.no_outlets'] = "No Outlets available.";
$_lang['txt.outlet.error.no_products'] = "No Products available.";
$_lang['txt.outlet.error.outlet_not_valid'] = "Outlet not valid.";
$_lang['txt.ordering.invalid_cart'] = "Please provide cart item details.";
$_lang['txt.ordering.cart_limit']   = "Please add product quantity upto ";
$_lang['txt.ordering.fav_product_details'] = "Please provide valid product details.";
$_lang['txt.ordering.success.cart_item_remove'] = "Successfully removed";
$_lang['txt.ordering.success.status_changed'] = "Status change successfully";
$_lang['txt.ordering.error.status_not_changed'] = "Status not changed";
$_lang['txt.success.empty_cart'] = "Successfully empty cart.";
$_lang['txt.ordering.already_fav_product'] = "Already selected as favorite.";
$_lang['txt.ordering.select_fav_product_first'] = "Please select product as favorite first.";
$_lang['txt.ordering.cart_empty'] = "You have an empty cart.";
$_lang['txt.ordering.success.added_cart'] = "Successfully added into cart.";
$_lang['txt.ordering.success.updated_cart'] = "Successfully updated into cart.";
$_lang['txt.ordering.error.cart_item_remove'] = "Error while remove cart item.";
$_lang['txt.ordering.error.cart_outlets'] = "Already associated with other outlet.";
$_lang['txt.ordering.error.invalid_order_id'] = "Please provide valid order inputs";
$_lang['txt.ordering.error.invalid_customer_id'] = "Please Provide valid customer number";
$_lang['txt.ordering.error.invalid_cart'] = "Invalid cart values";
$_lang['txt.ordering.error.change_cart_data'] = "Cart Updated";
$_lang['txt.ordering.error.cant_clone_cart'] = "Error during authenticating cart";
$_lang['txt.ordering.success.valid_cart'] = "Valid cart entities";
$_lang['txt.ordering.error.mismatch_cart'] = "Invalid cart entity.";
$_lang['txt.ordering.error.no_cart_found'] = "Invalid cart id";
$_lang['txt.ordering.error.no_order_placed'] = "No order place yet";
$_lang['txt.ordering.error.valid_cart_item'] = "Invalid cart item.";
$_lang['txt.outlet.error.no_fav_products'] = "No Favourites available.";
$_lang['txt.ordering.error.invalid_device_request'] = "Not authorize for access this cart.";
$_lang['txt.ordering.success.order_created'] = "Successfully Ordered";
$_lang['txt.ordering.error.pickupdate'] = "Pickup date must be future date.";
/**-LANGUAGE MESSAGE FOR "ordering/outlets" :End-**/

/**-LANGUAGE MESSAGE FOR "supplier" :Start-**/
$_lang['txt.error.valid_user'] = "User is not exist";
$_lang['txt.error.date_range'] = "Please select date range.";
$_lang['txt.error.sales_type_blank'] = "Please provide item sales type.";
$_lang['txt.error.outlet_type_blank'] = "Please provide outlet.";
$_lang['txt.error.fill_all_filed'] = "Please fill all field.";
$_lang['txt.error.no_sales_data_found'] = "No record found.";
$_lang['txt.outlet.error.no_department'] = "No Departments available.";
$_lang['txt.error.supplier_execution_exceeded'] = "Maximum execution time of 30 seconds exceeded";
/**-LANGUAGE MESSAGE FOR "supplier" :End-**/

/**-LANGUAGE MESSAGE FOR "supplier" :Request -**/
$_lang['txt.error.request_not_saved'] = "Request for download not send";
$_lang['txt.error.request_saved'] = "Report has been emailed";
$_lang['txt.error.User_invalid'] = "User not validate";
$_lang['txt.error.supplier_required_outlets'] = "Please select outlets";
$_lang['txt.error.supplier_required_dates'] = "Please select date range";
$_lang['txt.error.seven_day_validation'] = "Date range should not be more than one week";
$_lang['txt.no_record_found'] = "No record found.";
$_lang['txt.sucess.savedSuccessfully'] = "Successfully Uploaded";
$_lang['txt.error.stock.required_order_input'] = "Please select required order values";
$_lang['txt.error.stock.nothing_to_update'] = "Nothing to update";
$_lang['txt.success.stock.placed_order'] = "Successfully placed an order";
$_lang['txt.success.stock.update_order'] = "Successfully updated order";
$_lang['txt.success.stock_product.update_order'] = "Successfully updated";
$_lang['txt.error.stock.product_param_not_valid'] = "Please use valid outlet and product.";
$_lang['txt.success.order_place_success'] = "Order place successfully";
$_lang['txt.error.invalid_request'] = "Invalid request";
$_lang['txt.error.deviceidempty'] = "Please Provide device id";
$_lang['txt.error.suppliernot_associate'] = "Supplier not associate with any outlets";
$_lang['txt.stock.error.stock_outlet_valid'] = "Outlet not exist in Stocktake";
$_lang['txt.error.invalid_unlock_password'] = "Oops something goes wrong in unlock password";
$_lang['txt.login.unlock_password_matched'] = "Successfully matched password";
$_lang['txt.error.supplier.invalid_device_address'] = "Device address is not valid";

$_lang['txt.stock.error.invalid_outlet'] = "Invalid Outlet";
$_lang['txt.stock.error.already_in_stocktake'] = "This Outlet Stocktake batch has already been created";
$_lang['txt.stock.error.not_in_stocktake'] = "This Outlet is not exist Stocktake batch";
$_lang['txt.stock.error.add_products'] = "Please Add Products";
$_lang['txt.stock.success.add_products'] = "Successfully added products";
$_lang['txt.stock.success.add_product']  = "Successfully added";
$_lang['txt.stock.success.update_product'] = "Successfully updated";
$_lang['txt.stock.success.remove_stocktake'] = "Successfully removed";
$_lang['txt.stock.success.update_stocktake_products'] = "Successfully updated stocktake";
$_lang['txt.stock.error.no_purchase_history'] = "No record found";
$_lang['txt.stock.error.invalid_order_items'] = "Invalid order item";
$_lang['txt.stock.error.no_order_found'] = "No order found";
$_lang['txt.stock.error.order_not_allowed_remove'] = "Deletion of Posted documents not allowed";
$_lang['txt.stock.success.deleted'] = "Successfull Deleted";
$_lang['txt.error.required_weekly_report_param'] = "Please select all required values";
$_lang['txt.error.need_to_upgrade_app_version'] = "Your app version is too old. Need to upgrade it!.";
$_lang['txt.error.required_app_versions'] = "Please select all required values";
$_lang['txt.stock.error.no_till_sync_records'] = "No record found for sync!";
$_lang['txt.stock.success.records_synched'] = "Successfully synced!";
$_lang['txt.stock.success.inserted_stock_adjustment'] = "Successfully adjusted stock records!";
$_lang['txt.stock.error.product_adjustment'] = "Invalid product entity!";
$_lang['txt.stock.error.stock_adjustment'] = "Error during stock adjustment!";

/**-LANGUAGE MESSAGE FOR "sendPush/send_beacon_offer_push" :Start-**/
$_lang['txt.sendPush.targetpush_offer_push.success.added'] = "Target Push added .";
$_lang['txt.sendPush.send_targetpush_push.error.no_offer'] = "No targetpush available to send push .";
$_lang['txt.sendPush.send_targetpush_push.success.send'] = "Push send successful.";
$_lang['txt.sendPush.send_targetpush_push.not.send'] = "Push not send.";
$_lang['txt.sendPush.send_targetpush_push.already.send'] = "Push already send.";

/**-LANGUAGE MESSAGE FOR "offers/get_offer_details" :Start-**/
$_lang['txt.get_targetpush_details.error.no_offer'] = "Detail Not available.";
$_lang['txt.get_targetpush_details.error.required'] = "No target push available.";

/**-LANGUAGE MESSAGE FOR "scratchnwin" :Start-**/
$_lang['txt.scratchnwin.error.snw_expired'] = "Prize has been expired.";
$_lang['txt.scratchnwin.error.snw_invalid'] = "Invalid scratch&Win Id.";
$_lang['txt.scratchnwin.error.invalid_member'] = "Invalid customer.";
$_lang['txt.scratchnwin.error.no_prize_available_member'] = "No prize available.";
$_lang['txt.scratchnwin.error.invalid_member_prize'] = "Invalid customer's prize request.";
$_lang['txt.scratchnwin.error.invalid_winner'] = "Invalid Winner.";
$_lang['txt.scratchnwin.success.save_for_later'] = "Prize successfully saved for later.";
$_lang['txt.scratchnwin.success.prize_msg_1'] = "Congrats! <br> You've Won!";
$_lang['txt.scratchnwin.success.prize_msg_2'] = "No Win";
$_lang['txt.scratchnwin.success.prize_msg_3'] = "2ND CHANCE DRAW";
$_lang['txt.scratchnwin.success.congrates_you_won_note1'] = "Congrats!";
$_lang['txt.scratchnwin.success.congrates_you_won_note2'] = "You've Won!";
$_lang['txt.scratchnwin.success.second_chance_draw_note1'] = "2ND CHANCE DRAW";
$_lang['txt.scratchnwin.success.second_chance_draw_note2'] = "Congrats {x}, you’re in the draw to Win 1 of these major prize!";
$_lang['txt.scratchnwin.success.no_win_note1'] = "No Win";
$_lang['txt.scratchnwin.success.no_win_note2'] = "This time {x}.";
$_lang['txt.scratchnwin.success.no_win_note3'] = "Why not give it another go tomorrow!";
$_lang['txt.scratchnwin.error.snw_prizes_no_found'] = "No Offer(s) Found!";
$_lang['txt.scratchnwin.error.snw_prizes'] = "Prizes";
$_lang['txt.scratchnwin.success.scan_to_claim_prize'] = "Scan to claim your prize";
$_lang['txt.scratchnwin.success.push_count'] = "Push Count";
$_lang['txt.scratchnwin.error.no_push_count'] = "You don't have push count for games";
$_lang['txt.scratchnwin.success.snw_active_prizes'] = "Games";
$_lang['txt.scratchnwin.error.no_active_prizes'] = "No game(s) available for play.";
$_lang['txt.scratchnwin.error.already_win_today'] = "You have already won today's prize";
$_lang['txt.scratchnwin.error.already_played_today'] = "You have already played for today";
$_lang['txt.cte.error.already_played_today'] = "You have already played ";
$_lang['txt.scratchnwin.offer_expire_note'] = "*Offer expires at midnight. Single use offer.";
$_lang['txt.scratchnwin.error.snw_prize_no_found'] = "Prize has been expired.";
$_lang['txt.stock.error.temp_stocktake_error'] = "Network Error.Please try after some time!";
$_lang['txt.cte.error.price_level_invalid'] = "Invalid Price range";
$_lang['txt.cte.success.win_prize_note'] = "When you purchase any {x} Product today";
$_lang['txt.cte.entries'] = "Entries";
$_lang['txt.cte.error.no_prize_available'] = "No prize available for today";
$_lang['txt.cte.offer_expire_note'] = "*Offer expires at midnight. Offer is unlimited for 24hrs.\nNot to be used in conjunction with any other offer.";
$_lang['txt.cte.total_entries'] = "\nTOTAL\nENTRIES";
$_lang['txt.cte.additional_prize_note_1'] = "Plus";
$_lang['txt.cte.additional_prize_note_2'] = "for all additional Eligible Products";
$_lang['txt.cte.redeem_bottom_note'] = "Your entries will take 15 minutes to update. \nView your Profile for a total entry count.";

$_lang['txt.outlet.error.no_change'] = "Not Change";
$_lang['txt.outlet.error.need_to_update'] = "Need to update";
$_lang['txt.outlet.error.clear_old_data'] = "Clear old data";

/**-LANGUAGE MESSAGE FOR "reward & refferal" :Start-**/
$_lang['txt.refferal.error.blank_code'] = "Refferal code should not be blank.";
$_lang['txt.reward.birthday_note'] = "Heyy! It's your <b>Birthday</b>!! On your special day you earned %s Reward points!";
$_lang['txt.reward.referral_invitation_points'] = "Heyy! You have earned <b> %u reward points by referring</b> %s.";
$_lang['txt.reward.registration_points'] = "Heyy! You have earned <b>%s</b> reward points on <b>first time registration</b>.";
$_lang['txt.reward.notification_success'] = "Notifications listed successfully.";
$_lang['txt.reward.notification_deleted'] = "Notifications deleted successfully.";
$_lang['txt.reward.referral_purchase_points'] = "On the first purchase of your referrel's you have earned <b>%s</b> reward points!";
$_lang['txt.reward.referral_purchase_points_push_msg'] = "On the first purchase of your referrel's you have earned reward points!";
$_lang['txt.reward.order_purchase_points_push_msg'] = "Congrates! You earned reward points on purchasing from NightOwl.";
$_lang['txt.reward.order_purchase_points_msg'] = "Congrates! On purchase from Nightowl you have earned <b>%s</b> reward points!.";
$_lang['txt.reward.social_sharing_points_msg'] = "Congrates! On sharing the Nightowl app you have earned the reward points!. ";
$_lang['txt.reward.application_social_sharing_points'] = "Congrates! On sharing the Nightowl app you have earned the <b>%s</b> reward points!. ";
$_lang['txt.reward.contact_synchronisation_points'] = "Congrates! You have <b>%s</b> reward points on <b>contact synchronization</b>!.";
$_lang['txt.reward.rate_application_points_msg'] = "Congrates! On rating the Nightowl app you have earned the Reward Points!. ";
$_lang['txt.reward.error.add_reward_points'] = "Reward does not exist.";
$_lang['txt.reward.success.add_reward_points'] = "Reward point successfully inserted/updated.";
$_lang['txt.reward.success.contact_synchronisation'] = "Contact synchronization successfully.";
$_lang['txt.reward.campaign_success'] = "Reward campaign listed successfully.";
$_lang['txt.reward.redeem_point_notnull'] = "Member redeem Point should not be zero.";
$_lang['txt.reward.redeem_previous_point'] = "You can not redeem two products simultaneously. Please redeem first product completely or try after some time.";
$_lang['txt.reward.redeem_invalid'] = "Invalid reward redemption.";
$_lang['txt.reward.redeem_point_not_enough'] = "You do not have earned enough points to redeem.";
$_lang['txt.reward.redeem_sucessfully'] = "Congratulations! You have successfully Redeemed {x}.";
$_lang['txt.reward.user_redeem_history'] = "User redeem history listed successfully.";
$_lang['txt.reward.share_pplication.notvalid'] = "Not eligible for reward points";
$_lang['txt.reward.share_pplication.success'] = "Shared successfully";
$_lang['txt.reward.no_card_found'] = "No card associated with this account!";
/**-LANGUAGE MESSAGE FOR "reward & refferal" :End-**/

/**-LANGUAGE MESSAGE FOR "Kiosk" :Start-**/
$_lang['txt.kiosk.upgrade_txt'] = "Your app version is too old. Need to upgrade it!";
$_lang['txt.kiosk.invalid_verion'] = "Please check your given version code!";
$_lang['txt.kiosk.invalid_macid'] = "Please provide a valid Mac Id!";
$_lang['txt.kiosk.request_inqueue'] = "Your request is in queue! We'll update you shortly.";
$_lang['txt.kiosk.already_have_licence'] = "You already have an licence key.";
$_lang['txt.kiosk.add_request'] = "Your request sent successfully. We'll update you shortly!";
$_lang['txt.kiosk.invalid_macid_licence'] = "Please provide a valid MacId/LicenceKey!";
$_lang['txt.kiosk.valid_licence'] = "You have an valid Licence key.";
$_lang['txt.kiosk.invalid_licence'] = "Invalid Licence key.";
/**-LANGUAGE MESSAGE FOR "Kiosk" :End-**/

?>
