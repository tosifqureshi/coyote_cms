<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : Apl-15 12:20PM
 * Copyright : Coyote team
 *
 */
class competition {
	
    public $_response = array();
    public $result = array();
    public $iosPush = array();
    public $androidPush = array();
    private $competitions_table = 'ava_competitions';
    private $competition_products_table = 'ava_competition_products';
    private $competition_participants_table = 'ava_competition_participants';
    private $leaderboards_table = 'ava_leaderboards';
    private $competition_winners_table = 'ava_competition_winners';
    private $competition_luckydraw_table = 'ava_competition_luckydraw';
    private $account_table = 'ACCOUNT';
   
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
		$this->_mssql = new mssql(); // Create instance of mssql class
		$this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
        $this->competition_img_path = IMG_URL.'competition_images/'; // -- Set competition image path
        $this->product_img_path = IMG_URL.'product_images/'; // -- Set product image path
    }
	
	/**
	 * Function used to get lucky draw competition listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function get_luckydraws() {
		// get lucky draw competitions
		$_response = $this->get_competitions(1);
		return $_response; 
	}
	
	/**
	 * Function used to get leaderboard competition listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function get_leaderboards() {
		// get leaderboard competitions
		$_response = $this->get_competitions(2);
		return $_response; 
	}
	
	/**
	 * Function used to get competition listing
	 * @param acc_number
	 * @return array
	 * @access public
	 * 
	 */
	public function get_competitions($type=0) {
		//$datetime = date('Y-m-d H:i:s');
		$datetime = date('Y-m-d');
		$array = array();
		// get competition details whos not expired
		$competition_query = "SELECT * FROM " . $this->competitions_table . " WHERE status = '1' AND is_deleted = '0' AND (start_date	 <= '".$datetime."' AND end_date >= '".$datetime."') " ;
		if(!empty($type)) {
			$competition_query .= " AND type = $type ORDER BY created_at DESC";
		} else {
			$competition_query .= "ORDER BY created_at DESC";
		}
		$competition_result = $this->_db->my_query($competition_query);
		if ($this->_db->my_num_rows($competition_result) > 0) {
			while($row = $this->_db->my_fetch_object($competition_result)) {
				// set competition values
				$array['co_id']			= (isset($row->competition_id)) ? trim($row->competition_id) : "";
				$array['co_type']		= (isset($row->type)) ? trim($row->type) : "";
				$array['co_name']		= (isset($row->title)) ? trim($row->title) : "";
				$array['co_short_desc']	= (isset($row->short_description)) ? trim($row->short_description) : "";
				$array['co_long_desc']	= (isset($row->description)) ? trim($row->description) : "";
				$array['co_image_1']	= (isset($row->image_1) && !empty($row->image_1)) ? trim($this->competition_img_path.$row->image_1) : "";
				$array['co_image_2']	= (isset($row->image_2) && !empty($row->image_2)) ? trim($this->competition_img_path.$row->image_2) : "";
				$array['co_image_3']	= (isset($row->image_3) && !empty($row->image_3)) ? trim($this->competition_img_path.$row->image_3) : "";
				$array['co_start_date']	= (isset($row->start_date)) ? trim($row->start_date) : "";
				$array['co_end_date']	= (isset($row->end_date)) ? trim($row->end_date) : "";
				$array['is_default_image']= (isset($row->default_image_type)) ? trim($array['co_image_'.$row->default_image_type]) : "";
				// append competition array values
				$result[] = $array;
			}
			
			$_response['result_code'] = 1;
			$_response['result'] 	= $result;
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_competitions.error.no_competition');
		}
		return $_response;
	} // get_competitions
	
	/**
	 * Function used to get competition details
	 * @param acc_number
	 * @return array
	 * @access public
	 * 
	 */
	public function competitiondetails() {
		$datetime = date('Y-m-d H:i:s');
		$competition_id = $this->_common->test_input($_REQUEST['competition_id']);
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		// check competition id exists or not
		if(empty($competition_id)) {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_competitions.error.not_valid');
			return $_response;
		}
		
		if(!empty($member_id)) {
			// get competition details whos not expired
			$competition_query  = "SELECT * FROM " . $this->competitions_table . " WHERE status = '1' AND is_deleted = '0' AND competition_id = '".$competition_id."'";
			$competition_result = $this->_db->my_query($competition_query);
			if ($this->_db->my_num_rows($competition_result) > 0) {
				// fetch result object data
				$row = $this->_db->my_fetch_object($competition_result);
				// set competition values
				$co_array['co_id']			= (isset($row->competition_id)) ? trim($row->competition_id) : "";
				$co_array['co_name']		= (isset($row->title)) ? trim($row->title) : "";
				$co_array['co_type']		= (isset($row->type)) ? trim($row->type) : "";
				$co_array['co_repeat_days']	= (isset($row->reset_days)) ? trim($row->reset_days) : "";
				$co_array['co_short_desc']	= (isset($row->short_description)) ? trim($row->short_description) : "";
				$co_array['co_long_desc']	= (isset($row->description)) ? trim($row->description) : "";
				$co_array['co_image_1']		= (isset($row->image_1) && !empty($row->image_1)) ? trim($this->competition_img_path.$row->image_1) : "";
				$co_array['co_image_2']		= (isset($row->image_2) && !empty($row->image_2)) ? trim($this->competition_img_path.$row->image_2) : "";
				$co_array['co_image_3']		= (isset($row->image_3) && !empty($row->image_3)) ? trim($this->competition_img_path.$row->image_3) : "";
				$co_array['co_start_date']	= (isset($row->start_date)) ? trim($row->start_date) : "";
				$co_array['co_end_date']	= (isset($row->end_date)) ? trim($row->end_date) : "";
				$co_array['is_default_image']= (isset($row->default_image_type)) ? trim($co_array['co_image_'.$row->default_image_type]) : "";
				
				// get competitions last leader board entry point
				//$leaderboard = $this->get_product_leaderboard($co_array['co_id'],$member_id,1);
				// set last entry point
				$last_entry_point  = (isset($leaderboard->entry_point) && !empty($leaderboard->entry_point)) ? $leaderboard->entry_point : 0;
				// set last entry point date
				$last_entry_date  = (isset($leaderboard->created_at) && !empty($leaderboard->created_at)) ? $leaderboard->created_at : $co_array['co_start_date'];		
				$co_array['last_entry_point'] = $last_entry_point;
				// get competition's products log
				$co_array['competition_products'] = $this->get_product_images($co_array['co_id'],$member_id,$last_entry_date ,$co_array['co_end_date'],$last_entry_point);
				// append competition basic information in result array
				$_response['result_code'] = 1;
				$_response['competition_result'] = $co_array; 
				
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_competitions.error.no_competition');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_competitions.error.required');
		}
		return $_response;
	} // competitiondetails
	
	/**
	 * Function to get purchase details during competition
	 * @param int $member_id , string $product_ids , string $start_date , string $end_date
	 * @return array
	 * 
	 */
	 private function get_members_purchase_data($member_id='',$product_ids='',$start_date='',$end_date='') {
		// set loyalty start date
		$start_date = date('Ymd',strtotime($start_date));
		// set loyalty end date
		$end_date = date('Ymd',strtotime($end_date));
		
		// get member's purchase product log
		$member_query = "SELECT A.ACC_FIRST_NAME as member_name , A.ACC_EMAIL as member_email , ATX.ATRX_ACCOUNT as member_id, SUM(ATX.ATRX_QTY) as purchased_qty  FROM ACCOUNT_TRXTBL ATX LEFT JOIN ACCOUNT A on ATX.ATRX_ACCOUNT = A.ACC_NUMBER WHERE  ATX.ATRX_PRODUCT in (".$product_ids.") and ATX.ATRX_DATE between '".$start_date."' and '".$end_date."' and ATX.ATRX_TYPE = 'ITEMSALE' GROUP BY ATX.ATRX_ACCOUNT,A.ACC_FIRST_NAME , A.ACC_EMAIL order by purchased_qty desc";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$this->con);
		$co_member_txns = array();
		if($this->_mssql->pdo_num_rows_query($member_query,$this->con) > 0 ) {
			while($result = $this->_mssql->mssql_fetch_object_query($get_member_data)) {
				// set member transaction values in array
				$member_txt['member_name'] 	= $result->member_name;
				$member_txt['member_email'] = $result->member_email;
				$member_txt['member_id'] 	= $result->member_id;
				$member_txt['purchased_qty']= $result->purchased_qty;
				// append transaction 
				$co_member_txns[] = $member_txt;
			}
		}
		return $co_member_txns;
	}
	
	/**
	 * Function used to get competition's product log
	 * @param int $compitition_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function get_product_images($competition_id=0,$member_id=0,$start_date='',$end_date='',$last_entry_point) {
		 if(!empty($competition_id) && !empty($member_id)) {
			// get product images data for competition id
			$competition_product_query  = "SELECT * FROM " . $this->competition_products_table . " WHERE competition_id = '".$competition_id."' and is_deleted = 0 ORDER BY product_img_id ASC" ;
			$competition_product_result = $this->_db->my_query($competition_product_query);
			$product_result = array();	
			if ($this->_db->my_num_rows($competition_product_result) > 0) {
				$is_all_product_purchased = true;
				$product_ids = array();
				while($row = $this->_db->my_fetch_object($competition_product_result)) {
					
					// prepare product purchase values
					$array['product_img_id']= (isset($row->product_img_id)) ? trim($row->product_img_id) : "";
					$array['product_id']	= (isset($row->product_id)) ? $row->product_id : "";
					$array['product_name']	= (isset($row->product_name)) ? trim($row->product_name) : "";
					$array['product_group']	= (isset($row->product_group)) ? trim($row->product_group) : "";
					$array['product_qty']	= (isset($row->product_qty)) ? trim($row->product_qty) : "";
					$array['product_point']	= (isset($row->product_point)) ? trim($row->product_point) : "";
					$array['product_image']	= (isset($row->product_image) && !empty($row->product_image)) ? trim($this->product_img_path.$row->product_image) : "";
					// get member's product purchase count
					$purchase_count = $this->member_product_purchase($member_id,$array['product_id'],$start_date,$end_date);
					$is_all_product_purchased = ( $purchase_count == 0 ) ? false : true;
					$array['purchase_count']= $purchase_count;
					// append product array values
					$product_result[] = $array;
					// set product ids
					$product_ids[] = $array['product_id'];
				}
				
				// manage competition's leaderboard details
				if($is_all_product_purchased) {
					$entry_point = $last_entry_point+1; //  set entry point with increment value 1
					$product_ids = implode(',',$product_ids); // set product ids array as json 
				}
			}
		 }
		 return $product_result;
	 }
	 
	/**
	 * Function to get member's product purchase details
	 * @param int $member_id , int $product_id , string $start_date , string $end_date
	 * @return array
	 * 
	 */
	 private function member_product_purchase($member_id='',$product_id='',$start_date='',$end_date='') {
		// set loyalty start date
		$start_date = date('Ymd',strtotime($start_date));
		// set loyalty end date
		$end_date = date('Ymd',strtotime($end_date));
		
		// get member's purchase product log
		$member_query = "SELECT SUM(ATRX_QTY) as purchase_count  FROM ACCOUNT_TRXTBL WHERE ATRX_ACCOUNT = '".$member_id."' and  ATRX_PRODUCT = '".$product_id."' and ATRX_DATE between '".$start_date."' and '".$end_date."' and ATRX_TYPE = 'ITEMSALE'";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$this->con);
		$co_member_txns = array();
		if($this->_mssql->pdo_num_rows_query($member_query,$this->con) > 0 ) {
			$row = $this->_mssql->mssql_fetch_object_query($get_member_data);
		}
		// set purchased count 
		$purchase_count = (isset($row->purchase_count) && !empty($row->purchase_count)) ? $row->purchase_count : 0;
		return $purchase_count;
	}
	
	
	/**
	 * Function used to get competition's leader board
	 * @param compitition_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function get_product_leaderboard($competition_id=0,$member_id=0,$is_fetch_row='') {
		 if(!empty($competition_id) && !empty($member_id)) {
			// get competition's product leader boards 
			$leaderboard_query  = "SELECT * FROM " . $this->leaderboards_table . " WHERE competition_id = '".$competition_id."' and member_id = '".$member_id."' ORDER BY entry_point DESC" ;
			$leaderboard_product_result = $this->_db->my_query($leaderboard_query);
			$leaderboard_result = array();
			if ($this->_db->my_num_rows($leaderboard_product_result) > 0) {
				if(!empty($is_fetch_row)) {
					$leaderboard_result = $this->_db->my_fetch_object($leaderboard_product_result);
				} else {
					while($row = $this->_db->my_fetch_object($leaderboard_product_result)) {
						// prepare leaderboard values
						$array['leaderboard_id']= (isset($row->leaderboard_id)) ? trim($row->leaderboard_id) : "";
						$array['entry_point']	= (isset($row->entry_point)) ? trim($row->entry_point) : "";
						$array['created_at']	= (isset($row->created_at)) ? trim($row->created_at) : "";
						// append product array values
						$leaderboard_result[] = $array;
					}
				}
			}
		 }
		 return $leaderboard_result;
	 }
	 
	/**
	 * Function used to insert leader board entry corresponds to competition
	 * @param null
	 * @return int
	 * @access private
	 * 
	 */
	 private function add_leaberboard($member_id,$competition_id,$product_ids,$entry_point) {
		// insert leaberboard entry
		$insert_query = "INSERT INTO ".$this->leaderboards_table."(member_id,competition_id,product_ids,entry_point) VALUES ('$member_id','$competition_id','$product_ids','$entry_point')";
		$this->_db->my_query($insert_query);
		$leaderboard_id = mysql_insert_id();
		return $leaderboard_id;
	 }
	 
	/**
	 * Function used to manage competition's participant log by cron job
	 * @param null
	 * @return void
	 *
	 */
	 public function participantscron() {
		$datetime = date('Y-m-d H:i:s');
		$current_date = date('Y-m-d');
		// get competition details whos not expired
		$competition_query  = "SELECT competition_id,competition_result_date FROM " . $this->competitions_table . " WHERE status = '1' AND is_deleted = '0' AND (competition_result_date <= '".$current_date."' AND end_date >= '".$current_date."') ORDER BY created_at ASC" ;
		$competition_result = $this->_db->my_query($competition_query);
		if ($this->_db->my_num_rows($competition_result) > 0) {
			while($co_row = $this->_db->my_fetch_object($competition_result)) {
				// set competition id
				$competition_id  = (isset($co_row->competition_id)) ? $co_row->competition_id : 0;
				
				if(!empty($competition_id)) {
					// get products whos associated in running competition
					$co_products_query = "SELECT product_id FROM ".$this->competition_products_table." WHERE competition_id = '$competition_id' AND is_deleted = '0'" ;
					$co_product_result = $this->_db->my_query($co_products_query);	
					if ($this->_db->my_num_rows($co_product_result) > 0) {
						while($row = $this->_db->my_fetch_object($co_product_result)) {
							// set product id
							$product_id = (isset($row->product_id)) ? $row->product_id : 0;
							if(!empty($product_id)) {
								// get last entry date of member purchase
								$purchase_date_query  = "SELECT purchase_date,created_at FROM " . $this->competition_participants_table . " WHERE product_id = '".$row->product_id."' and competition_id = '$competition_id' order by purchase_date desc limit 1";
								$purchase_date_result = $this->_db->my_query($purchase_date_query);
								$purchased_date = $this->_db->my_fetch_object($purchase_date_result);
								$purchase_date = (isset($purchased_date->purchase_date) && !empty($purchased_date->purchase_date)) ? $purchased_date->purchase_date : $co_row->competition_result_date;
								
								// set loyalty start date
								$start_date = date('Y-m-d H:i:s',strtotime($purchase_date)); // like 20150120
								// set loyalty end date
								//$end_date = date('Ymd',strtotime($datetime)); // like 20160428
								$end_date = $datetime; // like 20160428
								
								// get member's purchase product log
								$member_query = "SELECT SUM(ATRX_QTY) as purchase_count, ATRX_ACCOUNT as member_id  FROM ACCOUNT_TRXTBL JOIN MEMBER on ATRX_ACCOUNT = MEMB_NUMBER AND  MEMB_Exclude_From_Competitions = 0 WHERE ATRX_PRODUCT = '".$row->product_id."' and ATRX_TIMESTAMP>'".$start_date."' AND ATRX_TIMESTAMP<='".$end_date."' and ATRX_TYPE = 'ITEMSALE' group by ATRX_ACCOUNT ";
								//$member_query = "SELECT SUM(ATRX_QTY) as purchase_count, ATRX_ACCOUNT as member_id  FROM ACCOUNT_TRXTBL JOIN MEMBER on ATRX_ACCOUNT = MEMB_NUMBER WHERE ATRX_PRODUCT = '".$row->product_id."' and ATRX_TIMESTAMP>'".$start_date."' AND ATRX_TIMESTAMP<='".$end_date."' and ATRX_TYPE = 'ITEMSALE' group by ATRX_ACCOUNT ";
								
								$get_member_data = $this->_mssql->my_mssql_query($member_query,$this->con);
								if($this->_mssql->pdo_num_rows_query($member_query,$this->con) > 0 ) {
									while($result = $this->_mssql->mssql_fetch_object_query($get_member_data)) {
										// set purchased product values
										$member_id = (isset($result->member_id)) ? $result->member_id : 0;
										$purchased_qty = (isset($result->purchase_count)) ? $result->purchase_count : 0;
										// add participant entry
										$participant_id = $this->add_participant($member_id,$product_id,$purchased_qty,$datetime,$competition_id);	
									}
								}
							}
						}
					}
				}
			}
		}
		
		return true;
	 }
	 
	 function test_purchase_data() {
		 // set loyalty start date
		$start_date = date('Ymd',strtotime('2015-01-20 18:30:56')); // like 20150120
		// set loyalty end date
		$end_date = date('Ymd',strtotime('2016-05-18 18:30:56')); // like 20160428
		// connecting with mssql live server db
		$ms_con = $this->_mssql->mssql_db_connection();
		// get member's purchase product log
		$member_query = "SELECT SUM(ATRX_QTY) as purchase_count, ATRX_ACCOUNT as member_id  FROM ACCOUNT_TRXTBL WHERE ATRX_PRODUCT = 79119 and ATRX_DATE <= '".$start_date."' AND ATRX_DATE >= '".$end_date."' and ATRX_TYPE = 'ITEMSALE' group by ATRX_ACCOUNT ";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$ms_con);
		
		if($this->_mssql->pdo_num_rows_query($member_query,$ms_con) > 0 ) {
			while($result = $this->_mssql->mssql_fetch_object_query($get_member_data)) {
				echo '<pre>';
				print_r($result);
			}
		}
		die;
	 }
	 
	 /**
	 * Function used to insert participant's entry corresponds to competition
	 * @param int $member_id, int $product_id,int $purchased_qty, string $purchase_date
	 * @return int
	 * @access private
	 * 
	 */
	 private function add_participant($member_id,$product_id,$purchased_qty,$purchase_date,$competition_id=0) {
		$datetime = date('Y-m-d H:i:s');
		// get last purchase datetime of member
		$last_purchase_date_query = "select top 1 ATRX_DATE,ATRX_TIMESTAMP from ACCOUNT_TRXTBL where ATRX_ACCOUNT = '".$member_id."' and ATRX_PRODUCT = '".$product_id."' order by ATRX_TIMESTAMP desc";
        $last_purchase_date_result = $this->_mssql->my_mssql_query($last_purchase_date_query,$this->con);
		if($this->_mssql->pdo_num_rows_query($last_purchase_date_query,$this->con) > 0 ) {
			$last_purchase_date_row = $this->_mssql->mssql_fetch_object_query($last_purchase_date_result);
			$purchase_date = (isset($last_purchase_date_row->ATRX_TIMESTAMP)) ? date('Y-m-d H:i:s',strtotime($last_purchase_date_row->ATRX_TIMESTAMP)) : $purchase_date;
		}
       
		// get member's existing product purchase 
		//$sql = "SELECT id,purchased_qty FROM " . $this->competition_participants_table . " WHERE member_id = '".$member_id."' and product_id = '".$product_id."' and competition_id = '".$competition_id."' and DATE(purchase_date) = DATE('".$purchase_date."')";
		$sql = "SELECT id,purchased_qty FROM " . $this->competition_participants_table . " WHERE member_id = '".$member_id."' and product_id = '".$product_id."' and competition_id = '".$competition_id."' and purchase_date = '".$purchase_date."'";
		
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
			// set participant id
			$participant_id = $row->id;
			// get existing purchase count
			$existing_purchase_qty = (isset($row->purchased_qty) && !empty($row->purchased_qty)) ? $row->purchased_qty : 0;
			// set total purchase count
			$purchased_qty = $purchased_qty+$existing_purchase_qty;
			// update participant product log
			$update_array = array("purchased_qty" => $purchased_qty,"purchase_date" => $purchase_date,"created_at" =>$datetime);// Update Create_at //
			//$update_array = array("purchased_qty" => $purchased_qty,"purchase_date" => $purchase_date); // 2016-07-18 5:30 //
			$where_clause = array("id" => $participant_id);
			$this->_db->UpdateAll($this->competition_participants_table, $update_array, $where_clause, "");
			
		} else {
			// insert participant entry
			$insert_query = "INSERT INTO ".$this->competition_participants_table."(member_id,product_id,competition_id,purchased_qty,purchase_date,created_at) VALUES ('$member_id','$product_id','$competition_id','$purchased_qty','$purchase_date','$datetime')";
			$this->_db->my_query($insert_query);
			$participant_id = mysql_insert_id();
		}
		
		return $participant_id;
	 }
	 
	/**
	 * Function used to manage competition's participant spent points by cron job
	 * @param null
	 * @return void
	 *
	 */
	 public function leaderboardcron() {
		
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		
		// get competition details whos not expired
		$competition_query  = "SELECT * FROM " . $this->competitions_table . " WHERE status = '1' AND is_deleted = '0' AND (competition_result_date	 <= '".$datetime."' AND end_date >= '".$date."') ORDER BY created_at ASC" ;
		$competition_result = $this->_db->my_query($competition_query);
		if ($this->_db->my_num_rows($competition_result) > 0) {
			while($co_row = $this->_db->my_fetch_object($competition_result)) {
				// set competition id
				$competition_id  = (isset($co_row->competition_id)) ? $co_row->competition_id : 0;
				if(!empty($competition_id)) {
					// set competition's product values
					$last_participant_cron_date = (!empty($co_row->last_participant_cron_date)) ? $co_row->last_participant_cron_date : $co_row->start_date; 
					$start_date		= date('Y-m-d H:i:s',strtotime($last_participant_cron_date));
					// get competition's associated products 
					//$co_products_query = "SELECT pt.member_id,pt.product_id,sum(pt.purchased_qty) as total_purchased_qty ,cp.product_group,cp.product_qty,cp.product_point FROM ".$this->competition_participants_table." pt join ".$this->competition_products_table." cp on cp.product_id = pt.product_id WHERE  pt.created_at >= '".$start_date."' and pt.created_at <= '".$datetime."' and  cp.is_deleted = '0' and cp.competition_id = '".$co_row->competition_id."' group by pt.member_id, pt.product_id order by pt.member_id,cp.product_group asc";
					$co_products_query = "SELECT pt.member_id,pt.product_id,sum(pt.purchased_qty) as total_purchased_qty ,cp.product_group,cp.product_qty,cp.product_point FROM ".$this->competition_participants_table." pt join ".$this->competition_products_table." cp on cp.product_id = pt.product_id WHERE  pt.purchase_date >= '".$start_date."' and pt.purchase_date <= '".$datetime."' and  cp.is_deleted = '0' and cp.competition_id = '".$co_row->competition_id."' and pt.competition_id = '".$co_row->competition_id."' group by pt.member_id, pt.product_id order by pt.member_id,cp.product_group asc";
					$co_product_result = $this->_db->my_query($co_products_query);
				
					if ($this->_db->my_num_rows($co_product_result) > 0) {
						$co_product_array = array();
						$group_array = array();
						
						while($row = $this->_db->my_fetch_object($co_product_result)) {
							// set competition's product values
							$member_id  	= (isset($row->member_id)) ? $row->member_id : 0; 
							$product_group 	= (isset($row->product_group)) ? $row->product_group : 0;
							$product_qty	= (isset($row->product_qty)) ? $row->product_qty : 0;
							$product_point 	= (isset($row->product_point)) ? $row->product_point : 0;
							$product_id	 	= (isset($row->product_id)) ? $row->product_id : 0; 
							$total_qty 		= (isset($row->total_purchased_qty)) ? $row->total_purchased_qty : 0;
							
							if(!empty($product_group)) {
									
								// update points for group product
								$group_array['member_id'] = $member_id;
								$group_array['product_id'] = $product_id;
								$group_array['product_group'] = $product_group;
								$group_array['total_qty'] = $total_qty;
								$group_array['product_qty'] = $product_qty;
								$group_array['product_point'] = $product_point;
								$co_product_array[$product_group][$member_id][] = $group_array;
								
							} else {
								$this->manage_participant_points($total_qty,$product_qty,$product_point,$member_id,$competition_id,$product_id);
							}
						}
							
						// manage participant's group points
						if(is_array($co_product_array) && count($co_product_array) > 0) {
							
							foreach($co_product_array as $key=>$product_data) {
								// get competition's total group product count
								$co_group_sql  = "SELECT product_img_id FROM ".$this->competition_products_table." WHERE competition_id = '".$competition_id."' and product_group = '".$key."'" ;
								$co_group_result = $this->_db->my_query($co_group_sql);
								if(is_array($product_data) && count($product_data) > 0) {
									
									foreach( $product_data as $key=>$member_data) {
										if(count($member_data) == $this->_db->my_num_rows($co_group_result)) {
											// add participant's group points
											$this->add_participant_group_points($member_data,$competition_id);
										}
									}
								}
							}
						}
					
						// update last participant cron date in competition
						$co_update_values = array("last_participant_cron_date" => $datetime);
						$co_where_clause = array("competition_id" => $competition_id);
						$co_update = $this->_db->UpdateAll($this->competitions_table, $co_update_values, $co_where_clause, "");
					}
				}
			}	
		}
		return true;
	 }
	 
	function is_val_exists($needle, $haystack) {
		if(in_array($needle, $haystack)) {
			return true;
		}
		foreach($haystack as $element) {
			if(is_array($element) && $this->is_val_exists($needle, $element))
			return true;
		}
		return false;
	}
	 
	 
	private function manage_participant_points($total_qty,$product_qty,$product_point,$member_id,$competition_id,$product_id) {
		$leaderid = 0;
		// calculate spent points
		$spent_points = floor($total_qty/$product_qty)*$product_point;
		// insert participant leaderboard points entry
		if($spent_points > 0) {
			// get last leader board entry of competition's member
			$co_leader_query  = "SELECT sum(spent_points) as total_spent_points FROM ".$this->leaderboards_table." WHERE member_id = '".$member_id."' AND competition_id = '".$competition_id."' AND product_id = '".$product_id."' order by created_at desc" ;
			$co_leader_result = $this->_db->my_query($co_leader_query);
			$leader_row	  	  = $this->_db->my_fetch_object($co_leader_result);
			$leader_points    = (isset($leader_row->total_spent_points) && !empty($leader_row->total_spent_points)) ? $leader_row->total_spent_points : 0;
			
			if($spent_points > $leader_points) {
				// deduct spent points with existing member's points
				$spent_points = $spent_points-$leader_points;
				// insert participant leaderboard points entry
				$leaderid = $this->add_participant_points($member_id,$product_id,$competition_id,$spent_points);
			}
		}
		return $leaderid;
	 }
	 
	  private function add_participant_group_points($co_product_array,$competition_id) {
		$leaderid = 0;
		$participant_group_data = $this->manage_participant_group_points($co_product_array); 
		// insert participant leaderboard points entry
		if(!empty($participant_group_data) && is_array($participant_group_data)) {
			$member_id = $participant_group_data['member_id'];
			$product_id = $participant_group_data['product_ids'];
			$spent_points = $participant_group_data['spent_points'];
			
			// get last leader board entry of competition's member
			$co_leader_query  = "SELECT sum(spent_points) as total_spent_points FROM ".$this->leaderboards_table." WHERE member_id = '".$member_id."' AND competition_id = '".$competition_id."' AND product_id = '".$product_id."' order by created_at desc" ;
			$co_leader_result = $this->_db->my_query($co_leader_query);
			$leader_row	  	  = $this->_db->my_fetch_object($co_leader_result);
			$leader_points    = (isset($leader_row->total_spent_points) && !empty($leader_row->total_spent_points)) ? $leader_row->total_spent_points : 0;
		
			if($spent_points > $leader_points) {
				// deduct spent points with existing member's points
				$spent_points = $spent_points-$leader_points;
				// insert participant leaderboard points entry
				$leaderid = $this->add_participant_points($member_id,$product_id,$competition_id,$spent_points);
			}
		}
		return $leaderid;
	 }
	 
	private function manage_participant_group_points($co_product_array) {
		
		$return_points_data = array();
		if( is_array($co_product_array) && count($co_product_array) > 0 ) {
			$spent_points_array = array();
			$product_id_array = array();
			$member_id = 0;
			
			foreach($co_product_array as $data) {
				if($data['total_qty'] >= $data['product_qty']) {
					// calculate spent points
					$spent_points = floor($data['total_qty']/$data['product_qty'])*$data['product_point'];
					$spent_points_array[] = $spent_points;
					// set product ids
					$product_id_array[]   = $data['product_id'];
					// set member id
					$member_id = $data['member_id'];
				} else {
					return false;
				}
			}
			
			// check all values are same or not
			if (count(array_count_values($spent_points_array)) == 1) {
				$total_spent_points = $spent_points_array[0];
			} else {
				$total_spent_points = min($spent_points_array);
			}
			
			// set product id as asending order
			asort($product_id_array);
			// set product ids
			$product_ids = implode(',',$product_id_array);
			
			$return_points_data = array('spent_points'=>$total_spent_points,'product_ids'=>$product_ids,'member_id'=>$member_id);
		}
		return $return_points_data;
	 }
	 
	 
	/**
	 * Function used to manage participant's purchase points
	 * @param int $member_id, int $product_id,int $competition_id, int $spent_points
	 * @return int
	 * @access private
	 * 
	 */
	 private function add_participant_points($member_id,$product_id,$competition_id,$spent_points) {
		// conver product id string in array
		$product_id_array = explode(',',$product_id);
		// set product id as asending order
		asort($product_id_array);
		//set as string
		$product_id = implode(',',$product_id_array);
		$created_at = date('Y-m-d H:i:s');
		// check existance of participant point entry
		$co_leader_query  = "SELECT leaderboard_id FROM ".$this->leaderboards_table." WHERE member_id = $member_id AND competition_id = $competition_id AND product_id = '".$product_id."' AND created_at = '".$created_at."'" ;
		$co_leader_result = $this->_db->my_query($co_leader_query);
		$leader_row	  	  = $this->_db->my_fetch_object($co_leader_result);
		if(!empty($leader_row)) {
			$participant_id = 0;
		} else {
			// get last participant entry 
			$last_purchase_date_query  = "SELECT purchase_date FROM ".$this->competition_participants_table." WHERE member_id = $member_id AND product_id = '".$product_id."' AND competition_id = '".$competition_id."' ORDER BY purchase_date DESC LIMIT 1" ;
			$last_purchase_date_result = $this->_db->my_query($last_purchase_date_query);
			$last_purchase_date_row = $this->_db->my_fetch_object($last_purchase_date_result);
			// set created date 
			$created_at = (isset($last_purchase_date_row->purchase_date) && !empty($last_purchase_date_row->purchase_date)) ? date('Y-m-d H:i:s',strtotime($last_purchase_date_row->purchase_date)) : $created_at; 
			
			// insert participant leaderboard points entry
			$insert_query = "INSERT INTO ".$this->leaderboards_table."(member_id,competition_id,product_id,spent_points,created_at) VALUES ($member_id,$competition_id,'$product_id','$spent_points','$created_at')";
			$this->_db->my_query($insert_query);
			$participant_id = mysql_insert_id();
		}
		return $participant_id;
	 }
	
	/**
	 * Function used to manage competition leader board winners by cron job
	 * @param null
	 * @return void
	 * @access public
	 * 
	 */
	public function leaderboardwinnerscron() {
			
		// set current date time
		$datetime = date('Y-m-d H:i:s');
		$currentdate = date('Y-m-d');
		// get competition details whos not expired
		$competition_query  = "SELECT competition_id,reset_days,start_date,end_date,competition_result_date FROM " . $this->competitions_table . " WHERE status = '1' AND is_deleted = '0' AND is_expired = 0 AND type = 2 AND (date(competition_result_date) <= '".$currentdate."' AND date(end_date) >= '".$currentdate."') ORDER BY created_at ASC" ;
		$competition_result = $this->_db->my_query($competition_query);
		if ($this->_db->my_num_rows($competition_result) > 0) {
			while($co_row = $this->_db->my_fetch_object($competition_result)) {
				
				// set repeat date value
				$reset_days	 = (isset($co_row->reset_days)) ? $co_row->reset_days : 0;
				// set end and result date
				$end_date = date('Y-m-d',strtotime($co_row->end_date));
				$competition_result_date = date('Y-m-d',strtotime($co_row->competition_result_date));
				// set competition result date
				$co_result_date = (!empty($reset_days)) ? date('Y-m-d', strtotime($competition_result_date.' +'.$reset_days.' days')) : date('Y-m-d', strtotime($end_date.' +1 days'));
				// set one day next date
				//$final_result_date = date('Y-m-d', strtotime($co_result_date.' +1 days'));
				$final_result_date = $co_result_date;
				//echo $currentdate.'_'.$co_row->competition_id.'_'.$final_result_date.'|||';
				
				if( $currentdate ==  $final_result_date ) {
					
					// set competition's updated start and end date
					$co_start_date	= date('Ymd',strtotime($co_row->competition_result_date)); // like 20150120
					$co_end_date	= date('Ymd',strtotime($final_result_date)); // like 20160428
					// get competition's highest spent points member 
					$co_leader_query  = "SELECT member_id,sum(spent_points) as total_spent_points FROM ".$this->leaderboards_table." WHERE competition_id = '".$co_row->competition_id."' AND created_at >= '".$co_start_date."' AND created_at <= '".$co_end_date."' group by member_id order by total_spent_points desc" ;
					$co_leader_result = $this->_db->my_query($co_leader_query);
					$leader_row	  	  = $this->_db->my_fetch_object($co_leader_result);
					// set winning member id
					$member_id	= (isset($leader_row->member_id) && !empty($leader_row->member_id)) ? $leader_row->member_id : 0;
					$total_spent_points	= (isset($leader_row->total_spent_points) && !empty($leader_row->total_spent_points)) ? $leader_row->total_spent_points : 0;
					//if(!empty($member_id)) {
						// insert competition winner log
						$insert_win_query = "INSERT INTO ".$this->competition_winners_table."(member_id,competition_id,spent_points) VALUES ('".$member_id."','".$co_row->competition_id."','".$total_spent_points."')";
						$this->_db->my_query($insert_win_query);
						$winner_id = mysql_insert_id();
						
						// update result date of competition
						if(!empty($winner_id)) {
							// update competition's result date 
							$co_update_values = array("competition_result_date" => date('Y-m-d',strtotime($final_result_date)));
							$co_where_clause = array("competition_id" => $co_row->competition_id);
							$co_update = $this->_db->UpdateAll($this->competitions_table, $co_update_values, $co_where_clause, "");
						}
					//}
				}
			}
		}
		return true;
	}
	
	/**
	 * Function used to manage competition lucky draw winners by cron job
	 * @param null
	 * @return void
	 * @access public
	 * 
	 */
	public function luckydrawcron() {
		
		// set current date time
		$datetime = date('Y-m-d H:i:s');
		$currentdate = date('Y-m-d');
		// get competition details whos not expired
		$competition_query  = "SELECT competition_id,reset_days,start_date,end_date,competition_result_date FROM " . $this->competitions_table . " WHERE status = '1' AND is_deleted = '0' AND is_expired = 0 AND type = 1 AND (competition_result_date <= '".$datetime."' AND end_date >= '".$datetime."') ORDER BY created_at ASC" ;
		$competition_result = $this->_db->my_query($competition_query);
		if ($this->_db->my_num_rows($competition_result) > 0) {
			while($co_row = $this->_db->my_fetch_object($competition_result)) {
			
				// set repeat date value
				$reset_days	 = (isset($co_row->reset_days)) ? $co_row->reset_days : 0;
				// set end and result date
				$end_date = date('Y-m-d',strtotime($co_row->end_date));
				$competition_result_date = date('Y-m-d',strtotime($co_row->competition_result_date));
				// set competition result date
				$co_result_date = (!empty($reset_days)) ? date('Y-m-d', strtotime($competition_result_date.' +'.$reset_days.' days')) : date('Y-m-d', strtotime($end_date.' +1 days'));
				// set one day next date
				//$final_result_date = date('Y-m-d', strtotime($co_result_date.' +1 days'));
				$final_result_date = $co_result_date;
				//echo $currentdate.'='.$final_result_date.'='.$co_row->competition_id.'|||';
				
				if( $currentdate ==  $final_result_date ) {
					// set competition's updated start and end date
					$co_start_date	= date('Ymd',strtotime($co_row->competition_result_date)); // like 20150120
					$co_end_date	= date('Ymd',strtotime($final_result_date)); // like 20160428
					
					// get competition's participant data in random order
				    $co_leader_query  = "SELECT pt.member_id FROM ".$this->competition_participants_table." pt JOIN ".$this->competition_products_table." cop ON cop.product_id = pt.product_id  WHERE cop.competition_id = '".$co_row->competition_id."' AND pt.created_at >= '".$co_start_date."' AND pt.created_at <= '".$co_end_date."' GROUP BY pt.member_id ORDER BY RAND() LIMIT 1" ;
					$co_leader_result = $this->_db->my_query($co_leader_query);
					$draw_row	  	  = $this->_db->my_fetch_object($co_leader_result);
					// set winning member id
					$member_id	= (isset($draw_row->member_id) && !empty($draw_row->member_id)) ? $draw_row->member_id : 0;
					//if(!empty($member_id)) {

						// insert competition lucky draw result log
						$insert_draw_query = "INSERT INTO ".$this->competition_luckydraw_table."(competition_id,created_at) VALUES ('".$co_row->competition_id."','".$co_result_date."')";
						$this->_db->my_query($insert_draw_query);
						$draw_id = mysql_insert_id();
						
						// update result date of competition
						if(!empty($draw_id)) {
							$co_update_values = array("competition_result_date" => date('Y-m-d',strtotime($final_result_date)));
							$co_where_clause = array("competition_id" => $co_row->competition_id);
							$co_update = $this->_db->UpdateAll($this->competitions_table, $co_update_values, $co_where_clause, "");
						}
					//}
				}
			}
		}
		return true;
	}
	
	/**
	 * Function used to get competition's leader board information
	 * @param int competition_id , int acc_number
	 * @return void
	 *
	 */
	 public function myleaderboard() {
		 
		$datetime = date('Y-m-d H:i:s');
		// get post values
		$competition_id = $this->_common->test_input($_REQUEST['competition_id']);
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		// check competition id exists or not
		if(empty($competition_id)) {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_competitions.error.not_valid');
			return $_response;
		}
		
		// get competition details
		$competition_query  = "SELECT * FROM " . $this->competitions_table . " WHERE competition_id = '".$competition_id."'  ORDER BY created_at ASC" ;
		$competition_result = $this->_db->my_query($competition_query);
		if ($this->_db->my_num_rows($competition_result) > 0) {
			$co_row = $this->_db->my_fetch_object($competition_result);
			// set competition values
			$co_array['co_id']			= (isset($co_row->competition_id)) ? trim($co_row->competition_id) : "";
			$co_array['co_name']		= (isset($co_row->title)) ? trim($co_row->title) : "";
			$co_array['co_type']		= (isset($co_row->type)) ? trim($co_row->type) : "";
			$co_array['co_repeat_days']	= (isset($co_row->reset_days)) ? trim($co_row->reset_days) : "";
			$co_array['co_short_desc']	= (isset($co_row->short_description)) ? trim($co_row->short_description) : "";
			$co_array['co_long_desc']	= (isset($co_row->description)) ? trim($co_row->description) : "";
			$co_array['co_image_1']		= (isset($co_row->image_1) && !empty($co_row->image_1)) ? trim($this->competition_img_path.$co_row->image_1) : "";
			$co_array['co_image_2']		= (isset($co_row->image_2) && !empty($co_row->image_2)) ? trim($this->competition_img_path.$co_row->image_2) : "";
			$co_array['co_image_3']		= (isset($co_row->image_3) && !empty($co_row->image_3)) ? trim($this->competition_img_path.$co_row->image_3) : "";
			$co_array['co_start_date']	= (isset($co_row->start_date)) ? trim($co_row->start_date) : "";
			$co_array['co_end_date']	= (isset($co_row->end_date)) ? trim($co_row->end_date) : "";
			$co_array['is_default_image']=(isset($co_row->default_image_type)) ? trim($co_array['co_image_'.$co_row->default_image_type]) : "";
			
			// set competition's updated start and end date
			$co_start_date	= date('Ymd',strtotime($co_row->competition_result_date)); // like 20150120
			$co_end_date	= date('Ymd',strtotime($datetime)); // like 20160428
			// set 1 day increment if start and end date same
			$co_end_date = ($co_start_date == $co_end_date) ? date('Ymd', strtotime($co_end_date.' +1 days')) : $co_end_date;
			// get competition's highest spent points member 
			$leaderboard_result = $this->leaderboard_members($co_array['co_id'],$co_start_date,$co_end_date,$member_id);
			// append leader board members log
			$co_array['leaderboard'] = $leaderboard_result;
			// append competition basic information in result array
			$_response['result_code'] = 1;
			$_response['result'] = $co_array;
			
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_competitions.error.not_valid');
		}
		
		return $_response;
	 }
  
	/**
	 * Function used to get leader boards participants details
	 * @param int competition_id , int acc_number
	 * @return void
	 *
	 */
	 private function leaderboard_members($competition_id=0,$co_start_date='',$co_end_date='',$member_id=0) {
		
		$co_end_date = date('Ymd', strtotime($co_end_date.' +1 days')); // set end date as 1 day after
		// get competition's highest spent points member 
		$co_leader_query  = "SELECT member_id,sum(spent_points) as total_spent_points FROM ".$this->leaderboards_table." WHERE competition_id = '".$competition_id."' AND created_at >= '".$co_start_date."' AND created_at < '".$co_end_date."'  group by member_id order by total_spent_points desc limit 5" ;
		$co_leader_result = $this->_db->my_query($co_leader_query);
		$leaderboard_result = array();
		$is_my_points = false;
		if ($this->_db->my_num_rows($co_leader_result) > 0) {
			while($leader_row = $this->_db->my_fetch_object($co_leader_result)) {
				
				// get user's account details
				$member_query = "SELECT ACC_FIRST_NAME as member_name FROM ACCOUNT WHERE ACC_NUMBER = '".$leader_row->member_id."'";
				$get_member_data = $this->_mssql->my_mssql_query($member_query,$this->con);
				$member_row = $this->_mssql->mssql_fetch_object_query($get_member_data);
				// prepare leaderboard values
				$array['member_name']   = (isset($member_row->member_name)) ? trim($member_row->member_name) : "";
				$array['member_id']     = (isset($leader_row->member_id)) ? trim($leader_row->member_id) : "";
				$array['spent_points']	= (isset($leader_row->total_spent_points)) ? trim($leader_row->total_spent_points) : "";
				$array['is_my_points']	= 0;
				if( !empty($member_id) && $member_id == $array['member_id'] ) {
					$array['is_my_points']	= 1;
					$is_my_points = true;
				}
				// append product array values
				$leaderboard_result['top_participants'][] = $array;
			}
		}
		
		// get login user's leader board points details
		if(!empty($member_id)) {
			// get competition's leader board points
			$leader_member_query  = "SELECT sum(spent_points) as total_spent_points FROM ".$this->leaderboards_table." WHERE competition_id = '".$competition_id."' AND created_at between '".$co_start_date."' AND '".$co_end_date."' AND member_id = '".$member_id."'" ;
			$leader_member_result = $this->_db->my_query($leader_member_query);
			$member_point_row = $this->_db->my_fetch_object($leader_member_result);
			// set member's points
			$member_points = (isset($member_point_row->total_spent_points) && !empty($member_point_row->total_spent_points)) ? $member_point_row->total_spent_points : 0;
			$leaderboard_result['my_leader_points'] = $member_points;
		}
		return $leaderboard_result;
	 }
} // End users class

?> 
