<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Amit Neema
 * Timestamp : June-09 3:55PM
 * Copyright : Coyote team
 *
 */
class cracktheegg {
	
    public $_response = array();
    public $result = array();
    public $iosPush = array();
    public $androidPush = array();
    private $scratch_n_win_table = 'ava_scratch_n_win';
    private $scratch_n_win_products_table = 'ava_scratch_n_win_products';
    private $scratch_n_win_prize_winners_table = 'ava_scratch_n_win_prize_winners';
    private $scratch_n_win_push_log_table = 'ava_scratch_n_win_push_log';
    private $scratch_n_win_product_sent_log_table = 'ava_scratch_n_win_product_sent_log';
    private $scratch_n_win_notification_log_table = 'ava_scratch_n_win_notification_log';
    private $ava_cte_member_transaction_log = 'ava_cte_member_transaction_log';
    private $ava_cte_basket_size = 'ava_cte_basket_size';
    private $ava_cte_campaign_push = 'ava_cte_campaign_push';
    private $user_account_table = 'ava_users';
    private $ava_cte_last_push = 'ava_cte_last_push';
    private $apn_table = 'APNTBL';
    private $member_table = 'Member';
    private $prod_table = 'ProdTbl';
    private $outp_table = 'OutpTbl';
	private $cte_brands_table = 'ava_cte_brands';
	private $cte_winners_table = 'ava_cte_winners_log';
	private $cte_brands_entries_table = 'ava_cte_brands_entry_availability';
	private $cte_member_entries_table = 'ava_cte_member_entries_log';

   
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
		$this->_mssql = new mssql(); // Create instance of mssql class
		$this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
        $this->_config  = config::getInst();
        $this->scatchnwin_img_path = IMG_URL.'scratchnwin_images/'; // -- Set image path
		$this->product_img_path = IMG_URL.'scratchnwin_images/'; // -- Set image path
    }
	
    
     /**
	 * Function used to Fetch member transaction log 
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
     public function fetch_member_transaction_log() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		$current_day = strtolower(date('l'));
        $trx_query = "SELECT trx_timestamp FROM ".$this->ava_cte_member_transaction_log." ORDER BY id DESC LIMIT 1";
        $trx_result = $this->_db->my_query($trx_query);
        $queryString = '';
        if ($this->_db->my_num_rows($trx_result) > 0) {
            $trx_row = $this->_db->my_fetch_object($trx_result);
            if(!empty($trx_row)){
                $trx_timestamp = $trx_row->trx_timestamp;
                $queryString = "AND JNAH_TRX_TimeStamp > '$trx_timestamp'";
            }
        }
        /* 1) For check order limit */
        /* $member_trx_query = "SELECT * FROM ( SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id,ROW_NUMBER() OVER (ORDER BY JNAH_TRX_TimeStamp ASC) as row  FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' ".$queryString." ) a WHERE row > 0 and row <= 2";*/
        /* 2) Transaction log without exculded member list */
		/*
        $member_trx_query = "SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' ".$queryString." ORDER BY JNAH_TRX_TimeStamp ASC ";
        */ 
		/* 3) Transaction log with exculded member list*/
        $member_trx_query = "SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO JOIN Member on MEMB_NUMBER = JNAD_PRODUCT WHERE MEMB_Exclude_From_Competitions = '0' AND JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' ".$queryString." ORDER BY JNAH_TRX_TimeStamp ASC ";
        $member_trx_data = $this->_mssql->my_mssql_query($member_trx_query,$this->con);
        $flag = 0;
        while($member_trx_result = $this->_mssql->mssql_fetch_object_query($member_trx_data))
        {
            $trx_no = $member_trx_result->trx_no;
            $trx_timestamp = $member_trx_result->trx_timestamp;
            $member_id = $member_trx_result->member_id;
            if(!empty($trx_no) &&  !empty($trx_timestamp)  &&  !empty($member_id)){
                $member_trx_product_query = "SELECT JNAD_QTY as product_count,JNAD_PRODUCT as product_id,JNAD_AMT as product_amount,JNAD_OUTLET as store_id FROM JNLD_Account_TBL  WHERE JNAD_STATUS = 1 AND JNAD_TYPE = 'SALE' AND JNAD_TRX_NO = '$trx_no' ORDER BY JNAD_TRX_NO DESC";       
                $member_product_trx_data = $this->_mssql->my_mssql_query($member_trx_product_query,$this->con);
                $i = 1;
                while($member_product_trx_result = $this->_mssql->mssql_fetch_object_query($member_product_trx_data))
                {
                    
                    $i++;
                    $product_count = $member_product_trx_result->product_count;
                    $product_id = $member_product_trx_result->product_id;
                    $product_amount = $member_product_trx_result->product_amount;
                    $store_id = $member_product_trx_result->store_id;
                    $trx_datetime = date('Y-m-d H:i:s',strtotime($trx_timestamp));// convert timestamp string in sql datetime formate
                    if(!empty($product_id) && ($product_id > 0) && ($product_amount>0) ){
                        $insert_query = "INSERT INTO ".$this->ava_cte_member_transaction_log."(trx_no,member_id,product_id,product_qty,product_amount,trx_timestamp,created_date,transaction_datetime,store_id) VALUES ('$trx_no','$member_id','$product_id','$product_count','$product_amount','$trx_timestamp','$datetime','$trx_datetime','$store_id')";
                        $this->_db->my_query($insert_query);
                        $insert_id = mysql_insert_id();
                    }
                }    
            }
            $flag++;
        }   
        if($flag > 0){
             $_response['result_code'] = 1;
             $_response['error_msg'] = 'Fetch Data';  
        
        }else{
                $_response['result_code'] = 0;
                $_response['error_msg'] = 'Not Fetch Data';  
        }
        return $_response;
    }
    
    
     /**
	 * Function used to get member who is eligible for campaign at that time interval 
    */

    public function campaign_eligible_member() {
        
        $push_last_query = "SELECT * FROM ".$this->ava_cte_last_push." WHERE `id` = '1' ORDER BY id DESC LIMIT 1";
        $push_last_result = $this->_db->my_query($push_last_query);
        $push_last_row = $this->_db->my_fetch_object($push_last_result);
        // Last Push Id Last time push transaction id
        $last_push_id = (!empty($push_last_row->last_fetch_id) && isset($push_last_row->last_fetch_id))?$push_last_row->last_fetch_id:0;// last fetch id
        
        // Last fetch Id in current cron
        $push_fetch_query = "SELECT * FROM ".$this->ava_cte_member_transaction_log." ORDER BY id DESC LIMIT 1";
        $push_fetch_result = $this->_db->my_query($push_fetch_query);
        $push_fetchlast_row = $this->_db->my_fetch_object($push_fetch_result);
        $last_fetch_id = (!empty($push_fetchlast_row->id) && isset($push_fetchlast_row->id))?$push_fetchlast_row->id:0;// last fetch id
        
        if($last_fetch_id  > $last_push_id){                   
            
            /* update last Push fetch Id */
            $updateArray = array("last_fetch_id" => $last_fetch_id);
            $whereClause = array("id" =>1);
            $update = $this->_db->UpdateAll($this->ava_cte_last_push, $updateArray, $whereClause,"");
        
            $datetime = date('Y-m-d H:i:s');
            $datetimebefore = date('Y-m-d H:i:s',strtotime("-5 minutes", strtotime($datetime)));
            $html = '';
            $date = date('Y-m-d');
            $current_day = strtolower(date('l'));
            $scratch_n_win_query = "SELECT * FROM ".$this->scratch_n_win_table." WHERE campaign_type = '3' AND status = '1' AND is_deleted = '0' AND (start_date <= '".$date."' AND end_date >= '".$date."')  ORDER BY snw_id DESC";
            $html = $scratch_n_win_query;
            $scratch_n_win_result = $this->_db->my_query($scratch_n_win_query);
            while($scratch_n_win_row = $this->_db->my_fetch_object($scratch_n_win_result)){
                if(!empty($scratch_n_win_row)){
             
                    $campaign_id = $snw_id = (isset($scratch_n_win_row->snw_id) && !empty($scratch_n_win_row->snw_id)) ? $scratch_n_win_row->snw_id : '';
                    $title = (isset($scratch_n_win_row->title) && !empty($scratch_n_win_row->title)) ? $scratch_n_win_row->title : '';
                    $cte_expire_days = (isset($scratch_n_win_row->cte_expire_days) && !empty($scratch_n_win_row->cte_expire_days)) ? $scratch_n_win_row->cte_expire_days : 0;
                    $datetime7after = date('Y-m-d H:i:s',strtotime("+".$cte_expire_days." day", strtotime($datetime)));
                    $QueryString = '';
                    $QueryHave = '';
                    $flagQ = 0;        
                    /* Find Max and Minimum range of basket size level */
                    // Min 
                    $basketMinQ = "SELECT min_spend FROM ".$this->ava_cte_basket_size." WHERE campaign_id = '$campaign_id' ORDER BY id ASC LIMIT 1";
                    $html .= '-------'.$basketMinQ;
                    $basketMinResult = $this->_db->my_query($basketMinQ);
                    $basketMin_row = $this->_db->my_fetch_object($basketMinResult);
                    $min_spend = (!empty($basketMin_row->min_spend) && isset($basketMin_row->min_spend))?$basketMin_row->min_spend:0;// Min Price
                    $basketMaxQ = "SELECT max_spend FROM ".$this->ava_cte_basket_size." WHERE campaign_id = '$campaign_id' ORDER BY id DESC LIMIT 1";
                    $html .= '-------'.$basketMaxQ;
                    $basketMaxResult = $this->_db->my_query($basketMaxQ);
                    $basketMax_row = $this->_db->my_fetch_object($basketMaxResult);
                    $max_spend = (!empty($basketMax_row->max_spend) && isset($basketMax_row->max_spend))?$basketMax_row->max_spend:0;// Max Price
                    
                    // Query Filter for minimum spend
                    $QueryHave = " HAVING ( amount >= $min_spend ";
                    if(!empty($max_spend)){
                        if($max_spend != 'INFINITE'){
                            $QueryHave .= " AND amount <= $max_spend ) ";
                        }else{
                            $QueryHave .= ")";
                        }
                    }else{
                        $QueryHave .= ")";
                    }
                    
                    $excluded_product = $scratch_n_win_row->excluded_product; // Excluded Product
                    $excluded_product = trim($scratch_n_win_row->excluded_product,','); // Excluded Product
                    
                    /* Query For 5 Minute Back */
                    //$QueryString = " WHERE (created_date >= '".$datetimebefore."' AND created_date <= '".$datetime."')"; 
                    /* Query For last transaction Id */
                    $QueryString = " WHERE (id > '".$last_push_id."' AND id <= '".$last_fetch_id."')"; 
                    
                    if(!empty($excluded_product)){
                        // Condition to exclude product
                        $QueryString .= " AND product_id NOT IN ($excluded_product) ";
                    }
                    $trx_no_data = array();
                    $itemcount_data = array();
                    $amount_data = array();
                    $memberFilter = '';
                    if(!empty($campaign_id) && !empty($max_spend)){
                        $filterQurey = "SELECT trx_no,SUM(product_qty) AS itemcount,member_id,SUM(product_amount) AS amount FROM ".$this->ava_cte_member_transaction_log." ".$QueryString." GROUP BY trx_no,member_id ".$QueryHave;
                        $html .= '-------'.$filterQurey;
                        $filterResult = $this->_db->my_query($filterQurey);
                        while($filterRaw = $this->_db->my_fetch_object($filterResult)){
                            $ios_devices = array(); 
                            $android_devices = array(); 
                            $trx_no = $filterRaw->trx_no;
                            $itemcount = $filterRaw->itemcount;
                            $member_id = $filterRaw->member_id;
                            $amount = $filterRaw->amount;
                            if(!empty($trx_no) && ($itemcount > 0) && ($member_id>0) && ($amount>0) ){    
                                $trx_no_data[$member_id] = $filterRaw->trx_no;
                                $itemcount_data[$member_id] = $filterRaw->itemcount;
                                $amount_data[$member_id] = $filterRaw->amount;
                                //$memberFilter .= $member_id.',';    
                                $memberFilter = $member_id;    
                                     if(!empty($memberFilter)){
                                        $user_query = "SELECT u.device_token,u.device_type,u.acc_number,nt.giveaway_count,nt.prize_count,nt.viewed_count FROM ".$this->user_account_table." u LEFT JOIN ".$this->scratch_n_win_notification_log_table." nt ON (nt.member_id = u.acc_number) WHERE u.acc_number IN (".$memberFilter.") AND u.device_token <> '' AND u.device_type <> '' AND u.acc_number <> '' AND u.active = 1 AND u.is_push_send = 1 AND ((u.device_type = 'ios' AND u.mini_app_version >= '$mini_ios_version') OR (u.device_type = 'android' AND u.mini_app_version >= '$mini_android_version') )";
                                        $user_result = $this->_db->my_query($user_query);
                                        $push_members = array();
                        
                                        if ($this->_db->my_num_rows($user_result) > 0) {
                                            while($user_resultRaw = $this->_db->my_fetch_object($user_result)){  
                                                if(!empty($user_resultRaw)){
                                                    $member_id = $user_resultRaw->acc_number;
                                                    //if($member_id == '2100000094' || $member_id == '2100001136'){  // Push only For mangesh sir and BHushan 
                                                        $acc_number =  $user_resultRaw->acc_number;
                                                        $insert_query = "INSERT INTO ".$this->ava_cte_campaign_push."(cte_campaign_id,member_id,product_total_qty,product_total_amount,trx_no,push_sent_at,created_date,push_expire_at) VALUES ('$campaign_id','$acc_number','$itemcount_data[$acc_number]','$amount_data[$acc_number]','$trx_no_data[$acc_number]','$datetime','$datetime','$datetime7after')";
                                                        $this->_db->my_query($insert_query);
                                                        $push_cetid = mysql_insert_id();
                                                        if(!empty($push_cetid)){
                                                            $device_type = $user_resultRaw->device_type;
                                                            $device_id = trim($user_resultRaw->device_token);
                                                            $member_id = $user_resultRaw->acc_number;
                                                            $giveaway_count = (isset($user_resultRaw->giveaway_count)) ? $user_resultRaw->giveaway_count : '';
                                                            $prize_count = (isset($user_resultRaw->prize_count)) ? $user_resultRaw->prize_count : '';
                                                            $viewed_count = (isset($user_resultRaw->viewed_count)) ? $user_resultRaw->viewed_count : '';
                                                            // Push Notification Come here  
                                                            if(!empty($device_id)) {
																if($device_type == 'android'){
																	// set andriod device token in array
																	$android_member_array['device_id'] = $device_id;
																	$android_member_array['pushlog_id'] = $push_cetid;
																	$android_devices[] = $android_member_array;
																}else{
																	// set ios device token in array
																	$ios_member_array['device_id'] = $device_id;
																	$ios_member_array['giveaway_count'] = $giveaway_count;
																	$ios_member_array['prize_count'] = $prize_count;
																	$ios_member_array['viewed_count'] = $viewed_count;
																	$ios_member_array['pushlog_id'] = $push_cetid;
																	$ios_devices[] = $ios_member_array;
																}
																//set member id in push member array
																$push_members[] = $member_id;
                                                            }
                                                            // Push Notification End here  
                                                        }                                                 
                                                   //}
                                                }
                                            }
                                        }//End Member While Loop
                                        
                                        
                                        if(is_array($push_members) && count($push_members) > 0) {
                                            // send prizes IOS push
                                            $this->_push->sendIphonePushNotification_cte($ios_devices,$title,$date);
                                            // send prizes andriod push 
                                            $sent_result = $this->_push->sendAndroidPushNotification_cte($android_devices,$title,$date);
                                            echo 'Send';
                                            // store prize push log for today
                                            //$this->manage_push_log($push_members,$snw_id,$date,$sent_result,$android_devices,$ios_devices);
                                        }                        
                                    }
                                
                                
                                                      
                            }
                        }
                      //  $memberFilter = rtrim($memberFilter,",");
                        /* Send push to member List */
                   }
                    
                }
            } 
        }
    }
    
    
        
	/**
	 * Function used to get member who is eligible for campaign at that time interval 
     */
	public function check_push_amount() {
		$user_id    = $this->_common->test_input($_REQUEST['user_id']);
		$scratch_n_win_query = "SELECT * FROM ".$this->ava_cte_campaign_push." WHERE member_id = '$user_id' ORDER BY id DESC LIMIT 100";
		$scratch_n_win_result = $this->_db->my_query($scratch_n_win_query);
		while($scratch_n_win_row = $this->_db->my_fetch_object($scratch_n_win_result)){
			print_r($scratch_n_win_row);
		}
		die;
    }
    
     /**
	 * Function used to get member who is eligible for campaign at that time interval 
    */

    public function ava_cte_member_transaction_log() {
		$user_id    = $this->_common->test_input($_REQUEST['user_id']);
		$scratch_n_win_query = "SELECT * FROM ".$this->ava_cte_member_transaction_log." WHERE member_id = '$user_id' ORDER BY id DESC LIMIT 100";
		$scratch_n_win_result = $this->_db->my_query($scratch_n_win_query);
		while($scratch_n_win_row = $this->_db->my_fetch_object($scratch_n_win_result)){
			print_r($scratch_n_win_row);
		}
		die;
    }
    
     /**
	 * Function used to send crack the egg prize push notification
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
     public function cte_customer_push() {
		
        // get active crack the egg records 
		$datetime = date('Y-m-d H');
		$date = date('Y-m-d');
		$current_day = strtolower(date('l'));
		// get app versions
		$mini_ios_version = $this->_config->getKeyValue("cte_mini_ios_version");
		$mini_android_version = $this->_config->getKeyValue("cte_mini_android_version");
		$cracktheegg_query  = "SELECT snw.snw_id,snw.title,snw.notification_time,wp.push_sent_at FROM " . $this->cte_brands_table . " cteb JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id LEFT JOIN " .$this->scratch_n_win_push_log_table. " wp ON snw.snw_id = wp.snw_id JOIN " . $this->cte_brands_entries_table . " cbea ON cbea.brand_id = cteb.id WHERE cteb.brand_image <> '' AND cteb.status = 1 AND snw.banner_image <> '' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND  snw.campaign_type = '3' AND snw.end_date >= '$date' AND cbea.entry_point > 0 AND cbea.entry_point != '' AND cbea.day = '$current_day' AND cteb.days_availability like '%,$current_day,%' GROUP BY cteb.cte_id ORDER BY snw.snw_id DESC";
		$cracktheegg_result = $this->_db->my_query($cracktheegg_query);
		
		if ($this->_db->my_num_rows($cracktheegg_result) > 0) {
			$snw_total_count = $this->_db->my_num_rows($cracktheegg_result);
			// get app customers
			$user_query = "SELECT u.device_token,u.device_type,u.acc_number,nt.giveaway_count,nt.prize_count,nt.viewed_count FROM ".$this->user_account_table." u LEFT JOIN ".$this->scratch_n_win_notification_log_table." nt ON (nt.member_id = u.acc_number AND nt.modified_at = '$date') WHERE u.device_token <> '' AND u.device_type <> '' AND u.acc_number <> '' AND u.active = 1 AND u.is_push_send = 1 AND ((u.device_type = 'ios' AND u.mini_app_version >= '$mini_ios_version') OR (u.device_type = 'android' AND u.mini_app_version >= '$mini_android_version') )";
		
			$user_result = $this->_db->my_query($user_query);
			if ($this->_db->my_num_rows($user_result) > 0) {
				$push_members = array();
				$android_devices = array();
				$ios_devices = array(); 
				while($user_row = $this->_db->my_fetch_object($user_result)) {
					// set customer's device param
					$device_type = $user_row->device_type;
					$device_id 	= trim($user_row->device_token);
					$member_id 	= $user_row->acc_number;
					$giveaway_count = (isset($user_row->giveaway_count)) ? $user_row->giveaway_count : '';
					$prize_count = (isset($user_row->prize_count)) ? $user_row->prize_count : '';
					$viewed_count = (isset($user_row->viewed_count)) ? $user_row->viewed_count : '';
					
					if(!empty($device_id)) {
						if($device_type == 'android'){
							// set andriod device token in array
							$android_devices[] = $device_id;
						}else{
							// set ios device token in array
							$ios_member_array['device_id'] = $device_id;
							$ios_member_array['giveaway_count'] = $giveaway_count;
							$ios_member_array['prize_count'] = $prize_count;
							$ios_member_array['viewed_count'] = $viewed_count;
							$ios_devices[] = $ios_member_array;
						}
						//set member id in push member array
						$push_members[] = $member_id;	
					}
				}
			}
			
			if(is_array($push_members) && count($push_members) > 0) {
				// send push for individual scratch & win entry
				while($row = $this->_db->my_fetch_object($cracktheegg_result)) {
					// set scratch values for push
					$push_sent_at = (isset($row->push_sent_at) && !empty($row->push_sent_at)) ? $row->push_sent_at : '';
					if(!empty($push_sent_at) && $push_sent_at == $date) {
						// do nothing in this case
					} else {
						// set cte values for push
						$cte_id = (isset($row->snw_id) && !empty($row->snw_id)) ? $row->snw_id : 0;
						$title = (isset($row->title) && !empty($row->title)) ? $row->title : '';

						$notification_time = (isset($row->notification_time) && !empty($row->notification_time)) ? explode(':',$row->notification_time) : '';
						$notification_time = (isset($notification_time[0]) && !empty($notification_time[0])) ? $notification_time[0] : '00';
						$cron_execute_at = $date.' '.$notification_time;
						
						if($cron_execute_at <= $datetime) {
							// set unique device ids array
							$android_devices = array_unique($android_devices);
							// reset device ids array
							$android_devices = $this->device_array_keys($android_devices);
							// send prizes IOS push
							$this->_push->sendIphoneCtePushNotification($ios_devices,$cte_id,$title,$date,$snw_total_count);
							// split android device array in chunk
							$android_chunk_devices = array_chunk($android_devices, 950);
							$push_response_array = array();
							// send prizes andriod push
							for($i=0;$i<count($android_chunk_devices);$i++) {
								$sent_result = $this->_push->sendAndroidCtePushNotification($android_chunk_devices[$i],$cte_id,$title,$date);
								$push_response_array[] = $sent_result;
							}
							// store prize push log for today
							$this->manage_push_log($push_members,$cte_id,$date,$sent_result,$android_devices,$ios_devices);
						}
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * Function used to manage customer's push log
	 * @param int cte_id , int member_id
	 * @return void
	 * @access private
	 * 
	 */
	private function manage_push_log($member_ids='',$cte_id=0,$date=0,$push_response='',$android_devices='',$ios_devices='') {
		
		$datetime = date('Y-m-d H:i:s');
		if(is_array($member_ids) && count($member_ids) > 0 && !empty($cte_id)) {
			
			// split member ids array in string
			$member_ids = implode(',',$member_ids);
			// write log in test file
			$filename =  BASEPATH.'/log.txt'; 
			file_put_contents($filename, $push_response);
			// optimize andriod push response
			$decoded_push_res = json_decode($push_response);
			// set android device count
			$android_device_count = count($android_devices);
			// set ios device count
			$ios_device_count = count($ios_devices);
			$push_res = array();
			if(isset($decoded_push_res->success) && isset($decoded_push_res->failure)) {
				$push_res = array('android_device_count'=>$android_device_count,'success'=>$decoded_push_res->success,'failure'=>$decoded_push_res->failure,'ios_device_count'=>$ios_device_count);
			}
			$android_push_res = json_encode($push_res);
			//$android_push_res = $push_response;
			// get customer's info crack the egg entry
			$snw_push_query  = "SELECT snw_push_id FROM " . $this->scratch_n_win_push_log_table . " WHERE snw_id = '$cte_id'" ;
			$snw_push_result = $this->_db->my_query($snw_push_query);
			if ($this->_db->my_num_rows($snw_push_result) > 0) {
				$push_row = $this->_db->my_fetch_object($snw_push_result);
				if(!empty($push_row) && isset($push_row->snw_push_id)) {
					// set push id
					$snw_push_id = $push_row->snw_push_id;
					// get push sent entry
					$push_sent_query  = "SELECT member_ids FROM " . $this->scratch_n_win_push_log_table . " WHERE snw_push_id = '$snw_push_id'" ;
					$push_sent_result = $this->_db->my_query($push_sent_query);
					if ($this->_db->my_num_rows($push_sent_result) > 0) {
						// update cte customer's push record
						if(!empty($snw_push_id)) {
							$update_array = array("modified_at"=>$datetime,"member_ids"=>$member_ids,"push_sent_at"=>$date,"push_sent_response"=>$android_push_res);// Update values
							$where_clause = array("snw_push_id"=>$snw_push_id);
							$this->_db->UpdateAll($this->scratch_n_win_push_log_table, $update_array, $where_clause, "");
						}
					}
				}
			} else {
				
				// insert customer's push count log
				$push_log_query = "INSERT INTO ".$this->scratch_n_win_push_log_table."(member_ids,snw_id,push_sent_at,push_sent_response,modified_at) VALUES ('$member_ids','$cte_id','$date','$android_push_res','$datetime')";
				$this->_db->my_query($push_log_query);
				$snw_push_id = mysql_insert_id();
			}
		}
		return true;
	}
	
	/**
	 * Function used to manage device array keys
	 * @param array device_ids
	 * @return void
	 * @access private
	 * 
	 */
     private function device_array_keys($device_ids='') {
		$device_array = array();
		if(!empty($device_ids)) {
			foreach($device_ids as $key=>$device_id) {
				$device_array[] = $device_id;
			}
		}
		return $device_array;
	 }
	 
	 /**
	 * Function used to manage members cracktheegg entry points
	 * @param null
	 * @return void
	 * @access public
	 * 
	 */
	 public function manage_member_entry_points() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		// fetch today's winner records
        $winners_query = "SELECT mt.id as trx_id,mt.trx_no,mt.product_qty,mt.product_id,mt.transaction_datetime,
		mt.member_id,mt.store_id,ctew.cte_id,ctew.id as win_prize_id,ctew.entry_points,cteb.products,ctew.brand_id 
		FROM ".$this->ava_cte_member_transaction_log." mt
		LEFT JOIN ".$this->cte_winners_table." ctew ON (mt.member_id = ctew.member_id AND date(ctew.prize_declare_at)  = '$date' AND (SELECT products from ".$this->cte_brands_table." WHERE id = ctew.brand_id) LIKE CONCAT('%,', mt.product_id ,',%') )
		JOIN ".$this->cte_brands_table." cteb ON (cteb.products like CONCAT('%,', mt.product_id ,',%'))
		JOIN ".$this->scratch_n_win_table." camp ON (camp.snw_id = cteb.cte_id AND camp.status = 1 AND camp.is_deleted = 0)
		JOIN ".$this->scratch_n_win_push_log_table." ctepl ON (ctepl.snw_id = cteb.cte_id AND date(ctepl.push_sent_at)  = '$date')
		WHERE cteb.status = 1 AND mt.transaction_datetime != '' AND ( (mt.transaction_datetime > (select created_at from ".$this->cte_member_entries_table." where member_id = mt.member_id ORDER BY created_at DESC LIMIT 1)) OR ( mt.transaction_datetime >= (select start_date from ".$this->scratch_n_win_table." where snw_id = cteb.cte_id )  AND (select count(id) from ".$this->cte_member_entries_table." where member_id = mt.member_id) = 0 ) ) GROUP BY mt.id ORDER BY mt.id DESC";
		
        $winner_result = $this->_db->my_query($winners_query);
		if ($this->_db->my_num_rows($winner_result) > 0) {
			while($row = $this->_db->my_fetch_object($winner_result)) {
				// set paramas
				$winner_id 	 = (!empty($row->win_prize_id)) ? $row->win_prize_id : 0;
				$cte_id 	 = (!empty($row->cte_id)) ? $row->cte_id : 0;
				$member_id 	 = (!empty($row->member_id)) ? $row->member_id : '';
				$brand_id 	 = (!empty($row->brand_id)) ? $row->brand_id : 0;
				$trx_id 	 = (!empty($row->trx_id)) ? $row->trx_id : '';
				$product_id  = (!empty($row->product_id)) ? $row->product_id : '';
				$store_id    = (!empty($row->store_id)) ? $row->store_id : 0;
				$transaction_datetime  = (!empty($row->transaction_datetime)) ? date('Y-m-d H:i:s',strtotime($row->transaction_datetime)) : '';
				$purchased_qty = (!empty($row->product_qty)) ? $row->product_qty : '';
				$entry_points  = (!empty($row->entry_points)) ? $row->entry_points : 1;
				$entry_points  = $purchased_qty*$entry_points;
				if(!empty($member_id) && !empty($entry_points)) {
					// insert member's entry points
					$entrylog_query = "INSERT INTO ".$this->cte_member_entries_table." (prize_brand_id,cte_id,member_id,brand_id,trx_id,product_id,store_id,entry_points,created_at) VALUES ('$winner_id','$cte_id','$member_id','$brand_id','$trx_id','$product_id','$store_id','$entry_points','$transaction_datetime')";
					$this->_db->my_query($entrylog_query);
					$id = mysql_insert_id();			
				}
			}
		}
		return true;
	}
	
	function sync_purchase_store() {
		// fetch existing transaction data
        $trx_sql = "SELECT id,trx_no,product_id
		FROM ".$this->ava_cte_member_transaction_log." 
		WHERE store_id = 0 ORDER BY id DESC";
        $trx_result = $this->_db->my_query($trx_sql);
		if ($this->_db->my_num_rows($trx_result) > 0) {
			while($row = $this->_db->my_fetch_object($trx_result)) {
				$id = (!empty($row->id)) ? $row->id : 0;
				$trx_no = (!empty($row->trx_no)) ? $row->trx_no : 0;
				$product_id = (!empty($row->product_id)) ? $row->product_id : 0;
				if(!empty($trx_no) &&!empty($product_id) && !empty($id)) {
					// get store id from mssql table
					$member_query = "SELECT * FROM JNLD_Account_TBL WHERE JNAD_TYPE = 'SALE' AND JNAD_TRX_NO = '$trx_no' AND JNAD_PRODUCT = '$product_id' ORDER BY JNAD_TRX_NO DESC";
					 $member_trx_data = $this->_mssql->my_mssql_query($member_query,$this->con);
					if($this->_mssql->mssql_num_rows_query($member_trx_data) > 0 ) {
						$result = $this->_mssql->mssql_fetch_object_query($member_trx_data);
						$store_id = (!empty($result->JNAD_OUTLET)) ? $result->JNAD_OUTLET : 0;
						if(!empty($store_id)) {
							// update store id under transaction log
							$update_array = array("store_id"=>$store_id);
							$where_clause = array("id"=>$id);
							$this->_db->UpdateAll($this->ava_cte_member_transaction_log, $update_array, $where_clause, "");	
							// update store id under reward entry log
							$update_entry_array = array("store_id"=>$store_id);
							$where_entry_clause = array("trx_id"=>$id,"product_id"=>$product_id);
							$this->_db->UpdateAll($this->cte_member_entries_table, $update_entry_array, $where_entry_clause, "");	
						}
					}
				}
			}
		}
		return true;
	}
	
	function testpush() {
		$device = $this->_common->test_input($_REQUEST['deviceId']);
		$sent_result = $this->_push->sendAndroidCtePushNotification(array($device),97,'Test Crack the Egg',date('Y-m-d'));
		echo '<pre>';
		print_r($sent_result);die;
	}
	
	function testiospush() {
		$device = $this->_common->test_input($_REQUEST['deviceId']);
		$array = array('device_id'=>$device,'giveaway_count'=>1,'prize_count'=>1,'viewed_count'=>1);
		$sent_result = $this->_push->sendIphoneCtePushNotification(array($array),97,'Test Crack the Egg',date('Y-m-d'));
		echo '<pre>';
		print_r($sent_result);die;
	}
} // End users class

?> 
