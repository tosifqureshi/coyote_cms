<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : June-28 3:10PM
 * Copyright : Coyote team
 *
 */
class managerwalk {
    public $_response = array();
    public $result = array();
    private $user_table = 'USERTBL';
	private $OUTPTBL = 'OUTPTBL';
	private $OUTLTBL = 'OUTLTBL';
    private $PRODTBL = 'PRODTBL';
    private $CODETBL = 'CODETBL';
	private $TRXTBL  = 'TRXTBL';
    private $APNTBL  = 'APNTBL';
    private $OrdhTbl = 'OrdhTbl';
    private $OrdlTbl = 'OrdlTbl';
    private $TillTbl = 'TillTbl';
    private $FIFOTbl = 'FIFOTbl';
    private $sales_report_queue = 'ava_sales_report_queue';
    private $print_label_table = 'ava_print_label';
	private $app_setting_table = 'ava_app_settings';
    private $ava_manager_walk_log = 'ava_manager_walk_log';

    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_mssql = new mssql(); // Create instance of mssql class
        $this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
    }
    
      
    /**
	 * Function used to fetch products listing
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function managerwalk_fetch_log() {
		
		// get post data
		$customer_id = $this->_common->test_input($_REQUEST['user_id']);
		$outlet_id   = $this->_common->test_input($_REQUEST['outlet_id']);
		$product_id   = $this->_common->test_input($_REQUEST['product_id']);
        $device_id   = $this->_common->test_input($_REQUEST['device_id']);
        $imei_number = $this->_common->test_input($_REQUEST['imei_number']);        
        $mac_number  = $this->_common->test_input($_REQUEST['mac_number']); 
        $datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		/* First time Check */
        $first_log_query = "SELECT id,modified_date FROM ".$this->ava_manager_walk_log." WHERE (imei_number = '$imei_number' OR mac_number = '$mac_number')  ORDER BY id DESC LIMIT 1";
        $first_log_result = $this->_db->my_query($first_log_query);
        if ($this->_db->my_num_rows($first_log_result) == 0) {
            $_response['result_code'] 	= 1;
            $_response['status'] 	= 4; // User Hit First time
            $_response['message'] =  '';
            $insert_query = "INSERT INTO ".$this->ava_manager_walk_log."(user_id,outlet_id,device_id,modified_date,imei_number,mac_number) VALUES ('$customer_id','$outlet_id','$device_id','$datetime','$imei_number','$mac_number')";
            $this->_db->my_query($insert_query);
            $insert_id = mysql_insert_id();  
        }else{
            $manager_log_query = "SELECT id,modified_date FROM ".$this->ava_manager_walk_log." WHERE DATE(modified_date) = '$date' AND (imei_number = '$imei_number' OR mac_number = '$mac_number')  AND outlet_id = '$outlet_id' AND device_id = '$device_id' ORDER BY id DESC LIMIT 1";
            $manager_log_result = $this->_db->my_query($manager_log_query);
            if ($this->_db->my_num_rows($manager_log_result) > 0) {
                $manager_log_row = $this->_db->my_fetch_object($manager_log_result);
                if(!empty($manager_log_row)){
                    $modified_date = $manager_log_row->modified_date;
                    $id = $manager_log_row->id;
                    $modified_date_strtime = strtotime('+30 minutes',strtotime($modified_date));
                    $datetime_strtime = strtotime($datetime);
                    if($datetime_strtime >= $modified_date_strtime){ // Pass more than 30 minutes
                         $_response['result_code'] 	= 1;
                        $_response['status'] 	= 2;
                        $_response['message'] =  $this->_common->langText($this->_locale,'txt.outlet.error.need_to_update');
                        /* update last fetch Id */
                        $updateArray = array("modified_date" => $datetime);
                        $whereClause = array("id" =>$id);
                        $update = $this->_db->UpdateAll($this->ava_manager_walk_log, $updateArray, $whereClause,"");
                    }else{
                        $_response['result_code'] 	= 1;
                        $_response['status'] 	= 3;
                        $_response['message'] =  $this->_common->langText($this->_locale,'txt.outlet.error.no_change');
                    }
                }
            }else{
                 $_response['result_code'] 	= 1;
                  $_response['status'] 	= 1; // 
                  $_response['message'] =  $this->_common->langText($this->_locale,'txt.outlet.error.clear_old_data');
                  $insert_query = "INSERT INTO ".$this->ava_manager_walk_log."(user_id,outlet_id,device_id,modified_date,imei_number,mac_number) VALUES ('$customer_id','$outlet_id','$device_id','$datetime','$imei_number','$mac_number')";
                  $this->_db->my_query($insert_query);
                  $insert_id = mysql_insert_id();  
            }
            
        }	
		return $_response;
    }
    
   
    
    
    /**
	 * Function used to get products listing
	 * @param int outlet_id, int limit, int offset
	 * @return array
	 * @access public
	 * 
	 */
	public function product_data_list() {
		
		// get post data
		$customer_id = $this->_common->test_input($_REQUEST['user_id']);
		$outlet_id   = $this->_common->test_input($_REQUEST['outlet_id']);
		$product_id   = $this->_common->test_input($_REQUEST['product_id']);
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
        $imei_number = $this->_common->test_input($_REQUEST['imei_number']);        
        $mac_number  = $this->_common->test_input($_REQUEST['mac_number']); 
        $datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
        $is_store_change   = $this->_common->test_input($_REQUEST['is_store_change']);
        $device_id   = $this->_common->test_input($_REQUEST['device_id']);
        $is_store_change = !empty($is_store_change)?$is_store_change:0;
        if(!empty($is_store_change)){
            $manager_log_query = "SELECT id,modified_date FROM ".$this->ava_manager_walk_log." WHERE DATE(modified_date) = '$date' AND (imei_number = '$imei_number' OR mac_number = '$mac_number') AND outlet_id = '$outlet_id' AND device_id = '$device_id' ORDER BY id DESC LIMIT 1";
            $manager_log_result = $this->_db->my_query($manager_log_query);
            if ($this->_db->my_num_rows($manager_log_result) > 0) {
                $manager_log_row = $this->_db->my_fetch_object($manager_log_result);
                if(!empty($manager_log_row)){
                    $modified_date = $manager_log_row->modified_date;
                    $id = $manager_log_row->id;
                    $updateArray = array("modified_date" => $datetime);
                    $whereClause = array("id" =>$id);
                    $update = $this->_db->UpdateAll($this->ava_manager_walk_log, $updateArray, $whereClause,"");
                }
            }else{
                  $insert_query = "INSERT INTO ".$this->ava_manager_walk_log."(user_id,outlet_id,device_id,modified_date,imei_number,mac_number) VALUES ('$customer_id','$outlet_id','$device_id','$datetime','$imei_number','$mac_number')";
                  $this->_db->my_query($insert_query);
                  $insert_id = mysql_insert_id();  
            }
        }
        
		// set pagination limit and offset values
        $limitString = '';
        if(empty($product_id)){
            $limit = (!empty($limit)) ? $limit : 10;
            $offset = (!empty($offset)) ? $offset : 0;
           // $limitString = "WHERE row > $offset AND row <= $limit";
        }
		$querystring = '';
        if(!empty($product_id)){
            $querystring = " AND Outp_Product = '$product_id' ";
        }
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
		if(!empty($outlet_id)) {
            $product_array = array();
            /*
            $product_sql_query1 = "select * from (select Outp_Product as product_number,OUTP_QTY_ONHAND as soh, Sum(ORDL_TOTAL_UNITS ) as on_order,
             (select case when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY) when TRX_TYPE = 'CHILDSALE' then case when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT) else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty end end / 8 as AvgWeeklySales from ProdTbl 
             LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = $outlet_id AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate()) where Prod_Number = OutpTbl.Outp_Product AND  TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = $outlet_id where (Prod_Parent = 0 or Prod_Parent is null) ) AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE') group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty) as avg_weekly_sales,
             OUTP_SKIP_REORDER_YN as skip_order, OUTP_NORM_PRICE_1 as price, OUTP_MIN_ONHAND as min_stock, OUTP_MIN_REORDER_QTY as min_order_qty, PROD_PARENT as prod_parent, OUTP_STATUS as product_status,PROD_DESC as prod_desc,PROD_POS_DESC as prod_pos_desc,PROD_SIZE as prod_size,PROD_HOST_NUMBER as host_number,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN,ROW_NUMBER() OVER (order by Outp_Product ASC) as row  from OutpTbl join ProdTbl on Outp_Product = Prod_Number  left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet = $outlet_id and ORDL_DOCUMENT_TYPE = 'ORDER' where Outp_Outlet = $outlet_id  $querystring group by ordl_product,Outp_Product,OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, PROD_PARENT,PROD_DESC,PROD_POS_DESC,PROD_SIZE,OUTP_STATUS,PROD_HOST_NUMBER,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN ) product_data  ";
            $product_query1 = $this->_mssql->my_mssql_query($product_sql_query1,$this->con);
            $product_total_count = $this->_mssql->mssql_num_rows_query($product_query1);
            */
            
            /* To Fetch Average sales week with respect to product */
            $avg_prd = array();
            $avg_sql_query = "select case when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY) when TRX_TYPE = 'CHILDSALE' then case when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT) else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty end end / 8 as AvgWeeklySales, Prod_Number from ProdTbl LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = $outlet_id AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate()) 
            where TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = $outlet_id
            where (Prod_Parent = 0 or Prod_Parent is null) ) AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE') group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty order by Prod_Number ASC";
            $avg_query = $this->_mssql->my_mssql_query($avg_sql_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($avg_sql_query,$this->con) > 0 ) {    
				while($avg_data = $this->_mssql->mssql_fetch_object_query($avg_query)){
                    $avg_prd[$avg_data->Prod_Number] = $avg_data->AvgWeeklySales;
                }    
            }
            
            /* To Fetch ON ORDER with respect to product */
            $onoder_prd = array();
            $onoder_sql_query = " select Prod_number, case coalesce(PROD_PARENT, 0)
                        when 0 then SUM(ORDL_TOTAL_UNITS) else SUM(ORDL_TOTAL_UNITS) * PROD_UNIT_QTY end AS UNITS_ON_ORDER
                        from OrdlTbl join PRODTBL on Prod_number = ORDL_PRODUCT where ORDL_PRODUCT in (PROD_PARENT,Prod_number) AND ORDL_OUTLET = $outlet_id AND ORDL_DOCUMENT_TYPE = 'ORDER' and ORDL_DOCUMENT_STATUS = 'ORDER' group by Prod_number,PROD_PARENT, PROD_UNIT_QTY";
            $onoder_query = $this->_mssql->my_mssql_query($onoder_sql_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($onoder_sql_query,$this->con) > 0 ) {    
				while($onorder_data = $this->_mssql->mssql_fetch_object_query($onoder_query)){
                    $onoder_prd[$onorder_data->Prod_number] = $onorder_data->UNITS_ON_ORDER;
                }    
            }
            
            /* To fetch APN No. With respect to Product */
                $Apn_array = array();
                $Apn_product_array = array();
                $apn_in_array = array();        
                $apn_query = "SELECT Prod_number,APN_NUMBER FROM APNTBL JOIN PRODTBL on Prod_Number = APN_PRODUCT JOIN OutpTbl on Prod_Number = Outp_Product where Outp_Outlet = $outlet_id GROUP BY APN_NUMBER,Prod_Number";         
                if($this->_mssql->pdo_num_rows_query($apn_query,$this->con) > 0 ) {    
                    $apn_query = $this->_mssql->my_mssql_query($apn_query,$this->con);
                    $constr = '';
                    while($apn_data = $this->_mssql->mssql_fetch_object_query($apn_query)){
                        if(in_array($apn_data->Prod_number,$Apn_product_array)){
                            $constr .= '-'.$apn_data->APN_NUMBER.'-,';
                            $Apn_array[$apn_data->Prod_number] = $constr;
                        }else{
                            $Apn_product_array[] = $apn_data->Prod_number;
                            $constr = '-'.$apn_data->APN_NUMBER.'-,';
                            $Apn_array[$apn_data->Prod_number] = $constr;
                        }
                    }    
                }


            $product_total_count = 0;
            $product_sql_query = "select * from (select Outp_Product as product_number,OUTP_QTY_ONHAND as soh,
             OUTP_SKIP_REORDER_YN as skip_order, OUTP_NORM_PRICE_1 as price, OUTP_MIN_ONHAND as min_stock, OUTP_MIN_REORDER_QTY as min_order_qty, PROD_PARENT as prod_parent, OUTP_STATUS as product_status,PROD_DESC as prod_desc,PROD_POS_DESC as prod_pos_desc,PROD_SIZE as prod_size,PROD_TYPE as prod_type,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN,ROW_NUMBER() OVER (order by Outp_Product ASC) as row  from OutpTbl join ProdTbl on Outp_Product = Prod_Number  left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet = $outlet_id and ORDL_DOCUMENT_TYPE = 'ORDER' where Outp_Outlet = $outlet_id  $querystring group by ordl_product,Outp_Product,OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, PROD_PARENT,PROD_DESC,PROD_POS_DESC,PROD_SIZE,OUTP_STATUS,PROD_TYPE,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN ) product_data $limitString ";
            $product_query = $this->_mssql->my_mssql_query($product_sql_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($product_sql_query,$this->con) > 0 ) {
                
				while($product_data = $this->_mssql->mssql_fetch_object_query($product_query)){
                    $array = array();
                    $OUTP_PROM_SELL_YN = $product_data->OUTP_PROM_SELL_YN;
                    $OUTP_PROM_MIX_YN = $product_data->OUTP_PROM_MIX_YN;
                    $OUTP_PROM_OFFER_YN = $product_data->OUTP_PROM_OFFER_YN;
                    $OUTP_PROM_BUY_YN = $product_data->OUTP_PROM_BUY_YN;
                    $on_promo = 'NO';
                    if($OUTP_PROM_SELL_YN == 'Y' || $OUTP_PROM_MIX_YN == 'Y' || $OUTP_PROM_OFFER_YN == 'Y'){
                        $on_promo = 'YES - Sell';
                    }else if($OUTP_PROM_BUY_YN == 'Y'){
                        $on_promo = 'YES - COST';
                    }
                    
                    $product_number = $array['product_number'] = $product_data->product_number;
                    $array['soh'] = !empty($product_data->soh)?$product_data->soh:0;
                    //$array['on_order'] = !empty($product_data->on_order)?$product_data->on_order:'';
                    $array['on_order'] = (isset($onoder_prd[$product_number]) && !empty($onoder_prd[$product_number]))?$onoder_prd[$product_number]:0;
                    //$array['avg_weekly_sales'] = !empty($product_data->avg_weekly_sales)?$product_data->avg_weekly_sales:'';
                    $array['avg_weekly_sales'] = !empty($avg_prd[$product_number])?$avg_prd[$product_number]:'';
                    
                    $array['apn_number'] = !empty($Apn_array[$product_number])?rtrim($Apn_array[$product_number],','):'';
                    
                    $array['on_promo'] = !empty($on_promo)?$on_promo:'';
                    $array['skip_order'] = !empty($product_data->skip_order)?$product_data->skip_order:'';
                    $array['price'] = $product_data->price;
                    $array['min_stock'] = $product_data->min_stock;
                    $array['min_order_qty'] = $product_data->min_order_qty;
                    $array['prod_parent'] = !empty($product_data->prod_parent)?$product_data->prod_parent:'';
                    $array['product_status'] = $product_data->product_status;
                    $array['prod_desc'] = utf8_encode($product_data->prod_desc);
                    $array['prod_pos_desc'] = utf8_encode($product_data->prod_pos_desc);
                    $array['prod_size'] = !empty($product_data->prod_size)?$product_data->prod_size:'';
                    $array['prod_host'] = !empty($product_data->prod_type)?$product_data->prod_type:'';
                    //$array['prod_host'] = !empty($product_data->host_number)?$product_data->host_number:'';
                    $array['outlet_id'] = !empty($outlet_id)?$outlet_id:'';
                    $product_array[] = $array;
                }
                // set response values
					$_response['result_code'] = 1;
					$_response['total_products'] = $product_total_count;
					$_response['result'] 	= $product_array;
                    $_response['message'] = $this->_common->langText($this->_locale,'');
			} else {
            
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
            }
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.outlet_not_valid');
		}
		
		return $_response;
	}
	
    
    
    /**
	 * Function used to get suppliers listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function update_stock() {
		
		$outlet_id     = $this->_common->test_input($_REQUEST['outlet_id']);
		$user_id     = $this->_common->test_input($_REQUEST['user_id']);
		$products      = $_REQUEST['product_data'];
		$date_time = date('Y-m-d H:i:s');
        //get array value from json
		$productData = json_decode($products);
        
        $datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
        $device_id   = $this->_common->test_input($_REQUEST['device_id']);
        
		// get post data
        
		// set default response as false
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.nothing_to_update');
		if(!empty($productData) && !empty($outlet_id)) {
            foreach($productData as $product){
                // set input item values
                $line_no = 1;
                $prod_number  = $product->product_number;
                $prod_desc    = str_replace( "'", "", $product->prod_desc);
                $soh    = !empty($product->soh)?$product->soh:0;
                $price    = !empty($product->price)?$product->price:0;
                $min_stock    = !empty($product->min_stock)?$product->min_stock:0;
                $min_order_qty    = !empty($product->min_order_qty)?$product->min_order_qty:0;
                $skip_order    = !empty($product->skip_order)?$product->skip_order:'N';
                $is_print_label    = !empty($product->is_print_label)?$product->is_print_label:0;
                $OUTP_CHANGE_LABEL_IND = '';
                if(!empty($is_print_label) && !empty($prod_number)){
                    $OUTP_CHANGE_LABEL_IND = ", OUTP_CHANGE_LABEL_IND = 'Yes' ";
                }
                
                // update Stock on hand from OUTP Table
                $update_sql = "UPDATE ".$this->OUTPTBL." SET OUTP_SKIP_REORDER_YN = '".$skip_order."' $OUTP_CHANGE_LABEL_IND , OUTP_QTY_ONHAND = '".$soh."' , OUTP_NORM_PRICE_1 = '".$price."' , OUTP_MIN_ONHAND = '".$min_stock."' , OUTP_MIN_REORDER_QTY = '".$min_order_qty."'  WHERE Outp_Product = $prod_number AND OUTP_OUTLET = $outlet_id";
                $this->_mssql->my_mssql_query($update_sql,$this->con);
            }
            $_response['result_code'] = 1;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.success.stock_product.update_order');
            $resultData = $this->fetch_stock_data($outlet_id);
            if($resultData['result_code'] == 1){
                /* Update Fetch Log Query */ 
                    $manager_log_query = "SELECT id,modified_date FROM ".$this->ava_manager_walk_log." WHERE DATE(modified_date) = '$date' AND user_id = '$user_id' AND outlet_id = '$outlet_id' AND device_id = '$device_id' ORDER BY id DESC LIMIT 1";
                    $manager_log_result = $this->_db->my_query($manager_log_query);
                    if ($this->_db->my_num_rows($manager_log_result) > 0) {
                        $manager_log_row = $this->_db->my_fetch_object($manager_log_result);
                        if(!empty($manager_log_row)){
                            $modified_date = $manager_log_row->modified_date;
                            $id = $manager_log_row->id;
                            $updateArray = array("modified_date" => $datetime);
                            $whereClause = array("id" =>$id);
                            $update = $this->_db->UpdateAll($this->ava_manager_walk_log, $updateArray, $whereClause,"");
                        }
                    }
            
                $_response['total_products'] = $resultData['total_products'];
                $_response['result'] 	= $resultData['result'];
                $_response['message'] = $resultData['message'];
            }else{
                $_response['result_code'] = 0;
                $_response['message'] = $resultData['message'];
            }
            
        }
    	return $_response;
	 }
     
     /**
	 * Function used to get suppliers listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	protected function fetch_stock_data($outlet_id) {
        $date_time = date('Y-m-d H:i:s');
        if(!empty($outlet_id)) {
            $product_array = array();
            /*
            $product_sql_query1 = "select * from (select Outp_Product as product_number,OUTP_QTY_ONHAND as soh, Sum(ORDL_TOTAL_UNITS ) as on_order,
             (select case when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY) when TRX_TYPE = 'CHILDSALE' then case when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT) else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty end end / 8 as AvgWeeklySales from ProdTbl 
             LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = $outlet_id AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate()) where Prod_Number = OutpTbl.Outp_Product AND  TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = $outlet_id where (Prod_Parent = 0 or Prod_Parent is null) ) AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE') group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty) as avg_weekly_sales,
             OUTP_SKIP_REORDER_YN as skip_order, OUTP_NORM_PRICE_1 as price, OUTP_MIN_ONHAND as min_stock, OUTP_MIN_REORDER_QTY as min_order_qty, PROD_PARENT as prod_parent, OUTP_STATUS as product_status,PROD_DESC as prod_desc,PROD_POS_DESC as prod_pos_desc,PROD_SIZE as prod_size,PROD_HOST_NUMBER as host_number,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN,ROW_NUMBER() OVER (order by Outp_Product ASC) as row  from OutpTbl join ProdTbl on Outp_Product = Prod_Number  left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet = $outlet_id and ORDL_DOCUMENT_TYPE = 'ORDER' where Outp_Outlet = $outlet_id  $querystring group by ordl_product,Outp_Product,OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, PROD_PARENT,PROD_DESC,PROD_POS_DESC,PROD_SIZE,OUTP_STATUS,PROD_HOST_NUMBER,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN ) product_data  ";
            $product_query1 = $this->_mssql->my_mssql_query($product_sql_query1,$this->con);
            $product_total_count = $this->_mssql->mssql_num_rows_query($product_query1);
            */
            
            
            /* To Fetch Average sales week with respect to product */
            $avg_prd = array();
            $avg_sql_query = "select case when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY) when TRX_TYPE = 'CHILDSALE' then case when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT) else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty end end / 8 as AvgWeeklySales, Prod_Number from ProdTbl LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = $outlet_id AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate()) 
            where TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = $outlet_id
            where (Prod_Parent = 0 or Prod_Parent is null) ) AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE') group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty order by Prod_Number ASC";
            $avg_query = $this->_mssql->my_mssql_query($avg_sql_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($avg_sql_query,$this->con) > 0 ) {    
				while($avg_data = $this->_mssql->mssql_fetch_object_query($avg_query)){
                    $avg_prd[$avg_data->Prod_Number] = $avg_data->AvgWeeklySales;
                }    
            }
            
            
            /* To Fetch ON ORDER week with respect to product */
            $onoder_prd = array();
            $onoder_sql_query = " select Prod_number, case coalesce(PROD_PARENT, 0)
                        when 0 then SUM(ORDL_TOTAL_UNITS) else SUM(ORDL_TOTAL_UNITS) * PROD_UNIT_QTY end AS UNITS_ON_ORDER
                        from OrdlTbl join PRODTBL on Prod_number = ORDL_PRODUCT where ORDL_PRODUCT in (PROD_PARENT,Prod_number) AND ORDL_OUTLET = $outlet_id AND ORDL_DOCUMENT_TYPE = 'ORDER' and ORDL_DOCUMENT_STATUS = 'ORDER' group by Prod_number,PROD_PARENT, PROD_UNIT_QTY";
            $onoder_query = $this->_mssql->my_mssql_query($onoder_sql_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($onoder_sql_query,$this->con) > 0 ) {    
				while($onorder_data = $this->_mssql->mssql_fetch_object_query($onoder_query)){
                    $onoder_prd[$onorder_data->Prod_number] = $onorder_data->UNITS_ON_ORDER;
                }    
            }
            
            
            /* To fetch APN No. With respect to Product */
                $Apn_array = array();
                $Apn_product_array = array();
                $apn_in_array = array();        
                $apn_query = "SELECT Prod_number,APN_NUMBER FROM APNTBL JOIN PRODTBL on Prod_Number = APN_PRODUCT JOIN OutpTbl on Prod_Number = Outp_Product where Outp_Outlet = $outlet_id GROUP BY APN_NUMBER,Prod_Number";         
                $apn_query = $this->_mssql->my_mssql_query($apn_query,$this->con);
                if( $this->_mssql->pdo_num_rows_query($apn_query,$this->con) > 0 ) {    
                    $constr = '';
                    while($apn_data = $this->_mssql->mssql_fetch_object_query($apn_query)){
                        if(in_array($apn_data->Prod_number,$Apn_product_array)){
                            $constr .= '-'.$apn_data->APN_NUMBER.'-,';
                            $Apn_array[$apn_data->Prod_number] = $constr;
                        }else{
                            $Apn_product_array[] = $apn_data->Prod_number;
                            $constr = '-'.$apn_data->APN_NUMBER.'-,';
                            $Apn_array[$apn_data->Prod_number] = $constr;
                        }
                    }    
                }

            
            $product_total_count = 0;
            $product_sql_query = "select * from (select Outp_Product as product_number,OUTP_QTY_ONHAND as soh, 
             OUTP_SKIP_REORDER_YN as skip_order, OUTP_NORM_PRICE_1 as price, OUTP_MIN_ONHAND as min_stock, OUTP_MIN_REORDER_QTY as min_order_qty, PROD_PARENT as prod_parent, OUTP_STATUS as product_status,PROD_DESC as prod_desc,PROD_POS_DESC as prod_pos_desc,PROD_SIZE as prod_size,PROD_TYPE as prod_type,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN,ROW_NUMBER() OVER (order by Outp_Product ASC) as row  from OutpTbl join ProdTbl on Outp_Product = Prod_Number  left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet = $outlet_id and ORDL_DOCUMENT_TYPE = 'ORDER' where Outp_Outlet = $outlet_id  group by ordl_product,Outp_Product,OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, PROD_PARENT,PROD_DESC,PROD_POS_DESC,PROD_SIZE,OUTP_STATUS,PROD_TYPE,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN ) product_data ";
            $product_query = $this->_mssql->my_mssql_query($product_sql_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($product_sql_query,$this->con) > 0 ) {
				while($product_data = $this->_mssql->mssql_fetch_object_query($product_query)){
                    $array = array();
                    $OUTP_PROM_SELL_YN = $product_data->OUTP_PROM_SELL_YN;
                    $OUTP_PROM_MIX_YN = $product_data->OUTP_PROM_MIX_YN;
                    $OUTP_PROM_OFFER_YN = $product_data->OUTP_PROM_OFFER_YN;
                    $OUTP_PROM_BUY_YN = $product_data->OUTP_PROM_BUY_YN;
                    $on_promo = 'NO';
                    if($OUTP_PROM_SELL_YN == 'Y' || $OUTP_PROM_MIX_YN == 'Y' || $OUTP_PROM_OFFER_YN == 'Y'){
                        $on_promo = 'YES - Sell';
                    }else if($OUTP_PROM_BUY_YN == 'Y'){
                        $on_promo = 'YES - COST';
                    }
                    
                    $product_number = $array['product_number'] = $product_data->product_number;
                    $array['soh'] = $product_data->soh;
                  //$array['on_order'] = !empty($product_data->on_order)?$product_data->on_order:'';
                    $array['on_order'] = (isset($onoder_prd[$product_number]) && !empty($onoder_prd[$product_number]))?$onoder_prd[$product_number]:0;
                    //$array['avg_weekly_sales'] = !empty($product_data->avg_weekly_sales)?$product_data->avg_weekly_sales:'';
                    $array['avg_weekly_sales'] = !empty($avg_prd[$product_number])?$avg_prd[$product_number]:'';
                    $array['apn_number'] = !empty($Apn_array[$product_number])?rtrim($Apn_array[$product_number],','):'';
                    $array['on_promo'] = !empty($on_promo)?$on_promo:'';
                    $array['skip_order'] = !empty($product_data->skip_order)?$product_data->skip_order:'';
                    $array['price'] = $product_data->price;
                    $array['min_stock'] = $product_data->min_stock;
                    $array['min_order_qty'] = $product_data->min_order_qty;
                    $array['prod_parent'] = !empty($product_data->prod_parent)?$product_data->prod_parent:'';
                    $array['product_status'] = $product_data->product_status;
                    $array['prod_desc'] = utf8_encode($product_data->prod_desc);
                    $array['prod_pos_desc'] = utf8_encode($product_data->prod_pos_desc);
                    $array['prod_size'] = !empty($product_data->prod_size)?$product_data->prod_size:'';
                    //$array['prod_host'] = !empty($product_data->host_number)?$product_data->host_number:'';
                    $array['prod_host'] = !empty($product_data->prod_type)?$product_data->prod_type:'';
                    $array['outlet_id'] = !empty($outlet_id)?$outlet_id:'';
                    $product_array[] = $array;
                }
                // set response values
					$_response['result_code'] = 1;
					$_response['total_products'] = $product_total_count;
					$_response['result'] 	= $product_array;
                    $_response['message'] = $this->_common->langText($this->_locale,'');
			} else {
            
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
            }
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.outlet_not_valid');
		}
		return $_response;
    }
	 
	
}
