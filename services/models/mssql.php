<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class mssql {
    public function __construct() {
        $this->_db 		= env::getInst();
        $this->_config  = config::getInst();
        $this->db_driver_type = $this->_config->getKeyValue("DB_DRIVER_TYPE");
        $this->db_host	= $this->_config->getKeyValue("MSSQL_HOST_NAME");
        $this->db_user	= $this->_config->getKeyValue("MSSQL_USER_NAME");
        $this->db_password	= $this->_config->getKeyValue("MSSQL_PASSWORD");
        $this->db_name	= $this->_config->getKeyValue("MSSQL_DB");
		$this->live_db_host	= $this->_config->getKeyValue("LIVE_MSSQL_HOST_NAME");
        $this->live_db_user	= $this->_config->getKeyValue("LIVE_MSSQL_USER_NAME");
        $this->live_db_password	= $this->_config->getKeyValue("LIVE_MSSQL_PASSWORD");
        $this->live_db_name	= $this->_config->getKeyValue("LIVE_MSSQL_DB");
        $this->con = '';
        $this->livecon = '';
    } 
    
    /*
    *@mssql_db_connection() this function, connect with live mssql server db
    * */
	public function mssql_db_connection() {
		if($this->db_driver_type == 'sqlsrv') {
			$connectionInfo = array( "Database"=>$this->db_name, "UID"=>$this->db_user, "PWD"=>$this->db_password,"ReturnDatesAsStrings"=>true);
			$con = sqlsrv_connect( $this->db_host, $connectionInfo);
			if($con) {
				return $con;
			} else {
				return false;
			}
		} else if($this->db_driver_type == 'pdo') {
            $this->con = new PDO('sqlsrv:Server='.$this->db_host.';Database='.$this->db_name, $this->db_user, $this->db_password);
            if($this->con) {
				return $this->con;
			} else {
				return false;
			}
        } else {
			$con=mssql_connect($this->db_host,$this->db_user,$this->db_password) or die("can't connect to DB");
			if($con) {	
				$link=mssql_select_db($this->db_name) or die("cant use db coyote");
				return $con;
			} else {
				return false;
			}
		}
	} //end mssql_db_connection()
	
	public function mssql_live_db_connection() {
		if($this->db_driver_type == 'sqlsrv') {
			$connectionInfo = array( "Database"=>$this->live_db_name, "UID"=>$this->live_db_user, "PWD"=>$this->live_db_password,"ReturnDatesAsStrings"=>true);
			$con = sqlsrv_connect( $this->live_db_host, $connectionInfo);
			if($con) {
				return $con;
			} else {
				return false;
			}
		} else if($this->db_driver_type == 'pdo') {
            $this->livecon = new PDO('sqlsrv:Server='.$this->live_db_name.';Database='.$this->live_db_name, $this->live_db_user, $this->live_db_password);
            var_dump($this->livecon);die;
            if($this->livecon) {
				return $this->livecon;
			} else {
				return false;
			}
        } else {
			$con=mssql_connect($this->db_host,$this->db_user,$this->db_password) or die("can't connect to DB");
			if($con) {	
				$link=mssql_select_db($this->db_name) or die("cant use db coyote");
				return $con;
			} else {
				return false;
			}
		}
	}
	
	
	/*
	 * @my_mssql_query()
	 * @REQUEST sql select query sting and connection variable
	 * @RESPONSE select query resource id   
	 * */
	public function my_mssql_query($sql,$con) {
		if($this->db_driver_type == 'sqlsrv') {
			$params = array();
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			return sqlsrv_query($con,$sql,$params,$options);
		} else if($this->db_driver_type == 'pdo') {
			return $con->query($sql);
        } else {
			return mssql_query($sql,$con);
		}
	} // mssql_select_query()
	
	/*
	 * @mssql_num_rows_query()
	 * @REQUEST resource id of select query
	 * @RESPONSE return num of rows
	 * */
	public function mssql_num_rows_query($resource_id) {
		if($this->db_driver_type == 'sqlsrv') {
			return sqlsrv_num_rows($resource_id);
		} else if($this->db_driver_type == 'pdo') {
            return $resource_id->rowCount();        
        } else {
			return mssql_num_rows($resource_id);
		}
	}
	/*
	 * @mssql_fetch_object_query()
	 * @REQUEST resource id of select query
	 * @RESPONSE return result-sets
	 * */
	public function mssql_fetch_object_query($resource_id) {
		if($this->db_driver_type == 'sqlsrv') {
			return sqlsrv_fetch_object($resource_id);
		} else if($this->db_driver_type == 'pdo') {
            return $resource_id->fetch(PDO::FETCH_OBJ);
        } else {
			return mssql_fetch_object($resource_id);
		}
	}
    
    /*
     * @pdo_num_rows_query()
     * @REQUEST resource id of select query
     * @RESPONSE return num of rows
     * */
    public function pdo_num_rows_query($sql,$con) {        
        $result = $con->prepare($sql); 
        $result->execute(); 
        return $number_of_rows = $result->fetchColumn();
    }

}
?>
