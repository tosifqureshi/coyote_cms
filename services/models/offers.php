<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Amit
 * Timestamp : Jan-24
 * Copyright : Coyote team
 *
 */
class offers {
    public $_response = array();
    public $result = array();
    public $iosPush = array();
    public $androidPush = array();
    private $offers_table = 'ava_offers';
    private $beacon_offers_table = 'ava_beacon_offers';
    private $beacons_table = 'ava_beacons';
    private $stores_table = 'ava_stores_log';
    private $beacon_customer_table = 'ava_beacon_customer_offer';
    private $push_activities_table = 'ava_push_activities';
    private $events_table = 'ava_events';
    private $event_gallery_table = 'ava_event_gallery';
    private $promotions_table = 'ava_promotions';
    private $kiosk_promotions_table = 'ava_kiosk_promotions';
    private $loyalty_table = 'ava_loyalty';
    private $settings_table = 'ava_settings';
    private $users_table = 'ava_users';
    
    private $ava_targetpush_notificaiton = 'ava_targetpush_notificaiton';
    private $targetpush_activities_table = 'ava_targetpush_activities';
    private $ava_cte_campaign_push = 'ava_cte_campaign_push';
    
	private $scratch_n_win_table = 'ava_scratch_n_win';
    private $scratch_n_win_products_table = 'ava_scratch_n_win_products';
    private $scratch_n_win_prize_winners_table = 'ava_scratch_n_win_prize_winners';
    private $scratch_n_win_push_log_table = 'ava_scratch_n_win_push_log';
    private $scratch_n_win_product_sent_log_table = 'ava_scratch_n_win_product_sent_log';
    private $scratch_n_win_notification_log_table = 'ava_scratch_n_win_notification_log';
    private $snw_beacon_offers_table = 'ava_scratch_n_win_beacon_offers';
	private $snw_beacon_customer_table = 'ava_scratch_n_win_beacon_offer_views';
	private $cte_winners_table = 'ava_cte_winners_log';
	private $cte_brands_table = 'ava_cte_brands';
	
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
		$this->_mssql = new mssql(); // Create instance of mssql class
		$this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
		$this->_config  = config::getInst();
    }
    /**
     * @method get_dashboard_data() return the banner offers
     * @access public
     * @return array
     */
     
    public function get_dashboard_data() {
		$current_date = CURRENT_TIME;
		if(isset($_REQUEST['session_token'])) {
			$session_token = $_REQUEST['session_token'];   // will used in future
		} 
		if(isset($_REQUEST['customer_id'])) {
			$customer_id = $_REQUEST['customer_id'];		// will used in future
		}
		$app_version = (isset($_REQUEST['avc'])) ? trim($_REQUEST['avc']) : ""; // get app version
		// update customer's app version
		$this->update_user_app_version($customer_id,$app_version);
		$selectBanner = "SELECT * FROM " . $this->offers_table . " WHERE is_banner_image = '1' AND status = '1' AND is_delete = '0' AND (start_time <= '".$current_date."' AND end_time >= '".$current_date."') ORDER BY created_at DESC LIMIT 5";
        $resultBanner = $this->_db->my_query($selectBanner);
        if ($this->_db->my_num_rows($resultBanner) > 0) {
			while($row = $this->_db->my_fetch_object($resultBanner)) {
				$array = array();
				$start_time = explode(" ",$row->start_time);
				$end_time = explode(" ",$row->end_time);
				
				$array['id'] 						= $row->offer_id;
				$array['name']		 				= $row->offer_name;
				$array['offer_short_description'] 	= $row->offer_short_description; 
				$array['offer_long_description'] 	= $row->offer_long_description;
				$array['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_1;
				$array['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_2;
				$array['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_3;
				$array['start_date'] 				= $start_time[0];
				$array['end_date'] 					= $end_time[0];
				$array['start_time'] 				= $start_time[1];
				$array['end_time'] 					= $end_time[1];
				$array['store_name'] 				= '';
				$array['category_name'] 			= '';
				$array['offer_price'] 				= $row->offer_price;
				$array['is_banner_image'] 			= $row->is_banner_image;
				if($row->is_default_image == 1) {
					$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_1;
				} else if($row->is_default_image == 2) {
					$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_2;
				} else {
					$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_3;
				}
				$result[] = $array;
			}
			$_response['result_code'] = 1;
			$_response['result'] = $result;
			// set member notification count for Scratch&Win
			$member_id = $this->_common->test_input($_REQUEST['acc_number']);
			if(!empty($member_id)) {
				// manage notification count
				$notification_count = $this->update_notification_counts($member_id);
				$giveaways_count = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
				$saved_prize_count = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
			}
			$_response['push_count'] = (isset($giveaways_count)) ? $giveaways_count : 0 ;
			$_response['saved_prize_count'] = (isset($saved_prize_count)) ? $saved_prize_count : 0 ;
			// get user profile details
			if(!empty($customer_id)) {
				$user = $this->_common->get_user_data_by_user_id($customer_id);
			}
			$_response['device_token'] = (isset($user->device_token) && !empty($user->device_token)) ? $user->device_token : '';
			$_response['device_type'] = (isset($user->device_type) && !empty($user->device_type)) ? $user->device_type : '';
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_offer.error.no_offer');
		}
        return $_response;
	} //end get_dashboard_data()
	
	/**
     * @Function used to update user's current app version
     * @input int customer_id, string app_version
     * @access private
     * @return void
    */
	private function update_user_app_version($customer_id=0,$app_version='') {
		if(!empty($app_version) && !empty($customer_id)) {
			$datetime = date('Y-m-d H:i:s');
			// update member's app version
			$update_array = array("mini_app_version" => $app_version,"update_at" =>$datetime);// Update values
			$where_clause = array("id" => $customer_id);
			$this->_db->UpdateAll($this->users_table, $update_array, $where_clause, "");	
		}
		return true;
	}
	
	/**
     * @method get_offer() return the in_store_offers 
     * @access public
     * @return array
    */
    public function get_offer() {
        $current_date = CURRENT_TIME;	
        $result =array();
            $selectOffer = "SELECT * FROM " . $this->offers_table . " WHERE status = '1' AND is_delete = '0' AND (start_time <= '".$current_date."' AND end_time >= '".$current_date."') ORDER BY created_at DESC";
            $resultOffer = $this->_db->my_query($selectOffer);
            if ($this->_db->my_num_rows($resultOffer) > 0) { 
				while($row = $this->_db->my_fetch_object($resultOffer)) {
					$start_time = explode(" ",$row->start_time);
					$end_time = explode(" ",$row->end_time);
					
					$array = array();
					$array['id'] 						= $row->offer_id;
					$array['name']		 				= $row->offer_name;
					$array['offer_short_description'] 	= $row->offer_short_description; 
					$array['offer_long_description'] 	= $row->offer_long_description;
					$array['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_1;
					$array['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_2;
					$array['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_3;
					$array['start_date'] 				= $start_time[0];
					$array['end_date'] 					= $end_time[0];
					$array['start_time'] 				= $start_time[1];
					$array['end_time'] 					= $end_time[1];
					$array['store_name'] 				= rtrim($store_name, ",");
					$array['category_name'] 			= '';
					$array['offer_price'] 				= '';
					$array['is_banner_image'] 			= $row->is_banner_image;
					if($row->is_default_image == 1) {
						$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_1;
					} else if($row->is_default_image == 2) {
						$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_2;
					} else {
						$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_3;
					}
					$result[] = $array;
				}
				$_response['result_code'] = 1;
				$_response['result']   = $result;
				$_response['category'] = $this->_common->get_category_data();
				$_response['store']	   = $this->_common->get_store_data();
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_offer.error.no_offer');
			}
        return $_response;
    } //end get_offer()
    
	/**
     * @method get_offer_details() return the offer detail w.r.t. offer id 
     * @access public
     * @return array
    */
    public function get_offer_details() {
		$offer_id = $this->_common->test_input($_REQUEST['id']);
		if (!empty($offer_id)) { 
			$selectOffer = "SELECT * FROM " . $this->beacon_offers_table . " WHERE beacon_offer_id = '$offer_id' AND status = '1' AND is_delete = '0'";
			$resultOffer = $this->_db->my_query($selectOffer);
			if ($this->_db->my_num_rows($resultOffer) > 0) { 
				$row = $this->_db->my_fetch_object($resultOffer);
					$start_time = explode(" ",$row->start_time);
					$end_time = explode(" ",$row->end_time);

					$result['id'] 				= $row->beacon_offer_id;
					$result['name']		 		= $row->offer_name;
					$result['offer_short_description'] 		= $row->offer_short_description; 
					$result['offer_long_description'] 		= $row->offer_long_description;
					$result['offer_image_1'] 				= IMG_URL.'offer_images'.DS.$row->offer_image_1;
					$result['offer_image_2'] 				= IMG_URL.'offer_images'.DS.$row->offer_image_2;
					$result['offer_image_3'] 				= IMG_URL.'offer_images'.DS.$row->offer_image_3;
					$result['start_date'] 					= $row->start_date;
					$result['end_date'] 					= $row->end_date;
					$result['start_time'] 					= $row->start_time;
					$result['end_time'] 					= $row->end_time;
					$result['store_name'] 					= $this->_common->get_store_data_by_id($row->store_id);
					$result['category_name'] 				= $this->_common->get_category_data_by_id($row->category_id);
					$result['store_id'] 					= $row->store_id;
					$result['offer_price'] 					= $row->offer_price;
					$result['is_banner_image'] 				= $row->is_banner_image;
					$result['is_banner_image'] 				= $row->is_simple_offer;
					$result['barcode_image'] 				= IMG_URL.'barcodes'.DS.$row->barcode_image;
					if($row->is_banner_image == 1) {
						$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_1;
					} else if($row->is_banner_image == 2) {
						$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_2;
					} else {
						$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_3;
					}
					$_response['result_code'] = 1;
					$_response['result'] = $result;
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_offer_details.error.no_offer');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_offer_details.error.required');
		}
		return $_response;
	} // end get_offer_details()
	
	/**
     * @method get_beacon_offer() return the vip beacon offer w.r.t. beacon code and customer id
     * @access public
     * @return array
    */
	public function get_beacon_offer() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$beacon_code = $this->_common->test_input($_REQUEST['beacon_code']);
		if(isset($_REQUEST['session_token'])) {
			$session_token = $this->_common->test_input($_REQUEST['session_token']);   // will used in future
		} 
		if(isset($_REQUEST['customer_id'])) {
			$customer_id = $this->_common->test_input($_REQUEST['customer_id']);		// will used in future
		}
		$app_version = (isset($_REQUEST['avc'])) ? trim($_REQUEST['avc']) : ""; // get app version
		$device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
		if (!empty($beacon_code) && !empty($customer_id)) {
			$dataRow = $this->_common->get_beacon_id($beacon_code);
			$store_id = $dataRow->store_id;
			$beacon_id = $dataRow->beacon_id;
			// get available beacon offer in duration 
			$selectOffer = "SELECT * FROM " . $this->beacon_offers_table . "  WHERE store_id LIKE '%,".$store_id.",%' AND status = '1' AND is_simple_offer = '0' AND is_delete = '0' AND is_default = '0' AND (start_date <= '".$date."' AND end_date >= '".$date."') AND (start_time <= '".$time."' AND end_time >= '".$time."')";
			$result_offer = $this->_db->my_query($selectOffer);
			if ($this->_db->my_num_rows($result_offer) > 0) {
				$_response =  $this->manage_offer_result($result_offer,$store_id,$beacon_id,$customer_id);
			} else {
				// get default beacon offer for store
				$select_default_offer = "SELECT * FROM " . $this->beacon_offers_table . "  WHERE store_id LIKE '%,".$store_id.",%' AND status = '1' AND is_default = '1' AND is_delete = '0'";
				$result_offer = $this->_db->my_query($select_default_offer);
				if ($this->_db->my_num_rows($result_offer) > 0) {
					$_response = $this->manage_offer_result($result_offer,$store_id,$beacon_id,$customer_id);
				} else {
					$_response['result_code'] = 0;
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_beacon_offer.error.no_offer');
				}
			}
			// get app versions
			$mini_ios_version = $this->_config->getKeyValue("mini_ios_version");
			$mini_android_version = $this->_config->getKeyValue("mini_android_version");
			if(($device_type == 'ios' && $app_version >= $mini_ios_version) || ($device_type == 'android' && $app_version >= $mini_ios_version)) {
				// get scratch n win beacon offer records
				$snw_beacon_offers = $this->get_snw_beacon_offer($beacon_code,$customer_id,$store_id,$beacon_id);
				if(!empty($snw_beacon_offers) && is_array($snw_beacon_offers) && count($snw_beacon_offers) > 0) {
					// append scratch n win beacon offer in result index
					$_response['result_code'] = 1;
				}
				// set scratch n win beacon offers
				$_response['snw_beacon_offer'] = $snw_beacon_offers;
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_beacon_offer.error.required');
		}
		
		return $_response;
	} //end get_beacon_offer
	
	/**
	 * Function used to prepare beacon offer result data set 
	 * @param $offer_row
	 * @return array
	 * @access private
	 * 
	 */
	 private function manage_offer_result($result_offer,$store_id,$beacon_id,$customer_id) {
		$datetime = date('Y-m-d H:i:s');
		// fetch offer result object
		$row = $this->_db->my_fetch_object($result_offer);
		$start_time = explode(" ",$row->start_time);
		$end_time = explode(" ",$row->end_time);
		// prepare offer result array
		$offer_id 							= $row->beacon_offer_id;
		$result['id'] 						= $row->beacon_offer_id;
		$result['is_default'] 				= $row->is_default;
		$result['name']		 				= $row->offer_name;
		$result['offer_short_description'] 	= $row->offer_short_description; 
		$result['offer_long_description'] 	= $row->offer_long_description;
		$result['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_1;
		$result['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_2;
		$result['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_3;
		$result['start_date'] 				= (isset($row->is_default) && $row->is_default == 0) ? $row->start_date : '';
		$result['end_date'] 				= (isset($row->is_default) && $row->is_default == 0) ? $row->end_date : '';
		$result['start_time'] 				= (isset($row->is_default) && $row->is_default == 0) ? $row->start_time : '';
		$result['end_time'] 				= (isset($row->is_default) && $row->is_default == 0) ? $row->end_time : '';
		$result['store_name'] 				= $this->_common->get_store_data_by_id($store_id);
		$result['category_name'] 			= $this->_common->get_category_data_by_id($row->category_id);
		$result['offer_price'] 				= $row->offer_price;
		$result['is_banner_image'] 			= $row->is_banner_image;
		$result['barcode_image'] 			= IMG_URL.'barcodes'.DS.$row->barcode_image;
		if($row->is_banner_image == 1) {
			$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_1;
		} else if($row->is_banner_image == 2) {
			$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_2;
		} else {
			$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_3;
		}
		$_response['result_code'] 			= 1;
		$_response['result'] 				= $result;
		// manage customer's beacon offer view status
		$_response['show'] = $this->manage_customer_offer_view($beacon_id,$customer_id,$offer_id,$datetime);
		return $_response;
	}
	
	/**
	 * Function used to manage customer's beacon offer view record 
	 * @param $offer_row
	 * @return array
	 * @access private
	 * 
	 */
	private function manage_customer_offer_view($beacon_id,$customer_id,$offer_id,$datetime) {
		// get customer's offer view result
		$selectCount = "SELECT * FROM ".$this->beacon_customer_table." WHERE beacon_id = '$beacon_id' AND ACC_NUMBER = '$customer_id' AND beacon_offer_id = '$offer_id'";
		$resultCount = $this->_db->my_query($selectCount);
		if ($this->_db->my_num_rows($resultCount) <= 0) { 
			// if not entry found the send offer and insert record
			$insertCount = "INSERT INTO ".$this->beacon_customer_table."(beacon_id,ACC_NUMBER,offer_seen_count,first_seen_count,last_seen_count,beacon_offer_id) VALUES ('$beacon_id','$customer_id','1','$datetime','$datetime','$offer_id')";
			$this->_db->my_query($insertCount);
			$show = 0;
		} else {
			// if record will found the send offer and Update record
			$count = $this->_db->my_fetch_object($resultCount);
			$oldcount = $count->offer_seen_count;
			$offer_seen_count = ($oldcount)+1;
			$updateArray = array("offer_seen_count" => $offer_seen_count,"last_seen_count" => $datetime);
			$whereClause = array("beacon_customer_offer_id" => $count->beacon_customer_offer_id);
			$update = $this->_db->UpdateAll($this->beacon_customer_table, $updateArray, $whereClause, "");
			if($oldcount == 0) {
				$show = 0;
			} else {
				$show = 1;
			}
			
			// get beacon notification time
			$beacon_setting_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'beacon_notification_time'");
			$beacon_setting_result = $this->_db->my_fetch_object($beacon_setting_query);
			$beacon_notification_time = (!empty($beacon_setting_result)) ? $beacon_setting_result->value : '';
			// set last seen date
			$last_seen_count = date('Y-m-d H:i:s',strtotime($count->last_seen_count));
			if(!empty($beacon_notification_time)) {
				$last_seen_count = date('Y-m-d H:i:s',strtotime('+'.$beacon_notification_time.' hour',strtotime($last_seen_count)));
			}
			// show popup for every day
			if($datetime > $last_seen_count) {
				$show = 0;
			}
		}
		return $show;
	 }
	 
	 /**
     * @method get_beacon_offer() return the vip beacon offer w.r.t. beacon code and customer id
     * @access public
     * @return array
    */
	public function get_snw_beacon_offer($beacon_code,$customer_id,$store_id,$beacon_id ) {
		
		$date = date('Y-m-d');
		$beacon_offers = array();
		// get available beacon offer in duration 
		$selectOffer = "SELECT * FROM ".$this->snw_beacon_offers_table." bo JOIN  ".$this->scratch_n_win_table." snw ON snw.snw_id = bo.snw_id WHERE bo.store_id LIKE '%,".$store_id.",%' AND snw.beacon_promotion = 1 AND snw.status = 1 AND snw.is_deleted = 0 AND snw.start_date <= '$date' AND snw.end_date >= '$date'";
		$result_offer = $this->_db->my_query($selectOffer);
		if ($this->_db->my_num_rows($result_offer) > 0) {
			while($row = $this->_db->my_fetch_object($result_offer)) {
				$beacon_offer_id = $row->beacon_offer_id;
				// get customer's offer view result
				$selectCount = "SELECT * FROM ".$this->snw_beacon_customer_table." WHERE beacon_id = '$beacon_id' AND customer_id = '$customer_id' AND snw_beacon_offer_id = '$beacon_offer_id' AND modified_at = '$date' ";
				$resultCount = $this->_db->my_query($selectCount);
				if ($this->_db->my_num_rows($resultCount) == 0) {
					$beacon_offers[] =  $this->manage_snw_offer_result($row,$store_id,$beacon_id,$customer_id);
				}
			}
		} 
		$_response = $beacon_offers;
		return $_response;
	} //end get_beacon_offer
	
	/**
	 * Function used to prepare beacon offer result data set 
	 * @param $offer_row
	 * @return array
	 * @access private
	 * 
	 */
	 private function manage_snw_offer_result($result_offer,$store_id,$beacon_id,$customer_id) {
		$datetime = date('Y-m-d H:i:s');
		// fetch offer result object
		//$row = $this->_db->my_fetch_object($result_offer);
		$row = $result_offer;
		$start_time = explode(" ",$row->start_time);
		$end_time = explode(" ",$row->end_time);
		// prepare offer result array
		$offer_id 							= $row->beacon_offer_id;
		$result['id'] 						= $row->beacon_offer_id;
		$result['name']		 				= $row->offer_name;
		$result['offer_short_description'] 	= $row->offer_short_description; 
		$result['offer_long_description'] 	= $row->offer_long_description;
		$result['start_date'] 				= '';
		$result['end_date'] 				= '';
		$result['start_time'] 				= '';
		$result['end_time'] 				= '';
		$result['store_name'] 				= $this->_common->get_store_data_by_id($store_id);
		$result['category_name'] 			= '';
		$result['offer_price'] 				= '0';
		$result['is_banner_image'] 			= '1';
		$result['barcode_image'] 			= '';
		$result['is_default_image'] 		= IMG_URL.'scratchnwin_images'.DS.$row->offer_image;
		// manage customer's beacon offer view status
		$result['show'] = $this->manage_snw_customer_offer_view($beacon_id,$customer_id,$offer_id,$store_id);
		return $result;
	}
	
	/**
	 * Function used to manage customer's beacon offer view record 
	 * @param $offer_row
	 * @return array
	 * @access private
	 * 
	 */
	private function manage_snw_customer_offer_view($beacon_id,$customer_id,$offer_id,$store_id=0) {
		$date = date('Y-m-d');
		// get customer's offer view result
		$selectCount = "SELECT * FROM ".$this->snw_beacon_customer_table." WHERE beacon_id = '$beacon_id' AND customer_id = '$customer_id' AND snw_beacon_offer_id = '$offer_id'";
		$resultCount = $this->_db->my_query($selectCount);
		if ($this->_db->my_num_rows($resultCount) <= 0) {
			// if not entry found the send offer and insert record
			$insertCount = "INSERT INTO ".$this->snw_beacon_customer_table."(beacon_id,customer_id,store_id,offer_seen_count,modified_at,snw_beacon_offer_id) VALUES ('$beacon_id','$customer_id','$store_id','1','$date','$offer_id')";
			$this->_db->my_query($insertCount);
			$show = 0;
		} else {
			// if record will found the send offer and Update record
			$count = $this->_db->my_fetch_object($resultCount);
			$oldcount = $count->offer_seen_count;
			$offer_seen_count = ($oldcount)+1;
			$updateArray = array("offer_seen_count" => $offer_seen_count,"modified_at" => $date);
			$whereClause = array("id" => $count->id);
			$update = $this->_db->UpdateAll($this->snw_beacon_customer_table, $updateArray, $whereClause, "");
			if($oldcount == 0) {
				$show = 0;
			} else {
				$show = 1;
			}
		}
		return $show;
	 }
	
	public function get_vip_offer() {
		$datetime = date('Y-m-d H:i:s');
		$beacon_id = $this->_common->test_input($_REQUEST['beacon_id']);
		if (!empty($beacon_id)) {
			$selectOffer = "SELECT * FROM " . $this->beacon_offers_table . " WHERE beacon_id = '$beacon_id' AND status = '1' AND is_delete = '0'";
			$resultOffer = $this->_db->my_query($selectOffer);
			if ($this->_db->my_num_rows($resultOffer) > 0) { 
				$row = $this->_db->my_fetch_object($resultOffer);
					$beacon_offer_id	= $row->beacon_offer_id;
					$selectPush = "SELECT * FROM ".$this->push_activities_table." WHERE beacon_offer_id = '$beacon_offer_id'";
					$resultPush = $this->_db->my_query($selectPush);
					
					if ($this->_db->my_num_rows($resultPush) == 0) {  
						$insertPush = "INSERT INTO ".$this->push_activities_table."(beacon_offer_id,is_push_send,added_at) VALUES ('$beacon_offer_id','0','$datetime')";
						$this->_db->my_query($insertPush);
						
						
						$_response['result_code'] = 1;
						$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.get_vip_offer.success');
					} 
					else {
						$_response['result_code'] = 0;
						$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_vip_offer.error.already');
					}
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_beacon_offer.error.no_offer');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_beacon_offer.error.required');
		}
		return $_response;
	} //get_VIP_offer()
    
    public function get_all_vip_offers() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		if(isset($_REQUEST['session_token'])) {
			$session_token = $this->_common->test_input($_REQUEST['session_token']);   // will used in future
		} 

		$beacon =array();
		$simple_offer =array();
		$newArray = array();
		if ($this->_common->test_input($_REQUEST['customer_id']) != '') {
			$customer_id = $this->_common->test_input($_REQUEST['customer_id']);
            // get Acc_number
			$user_query  = $this->_db->my_query("SELECT acc_number FROM ava_users WHERE id = '$customer_id'");
			$user_result = $this->_db->my_fetch_object($user_query);
			$acc_number = (!empty($user_result)) ? $user_result->acc_number : '';
            /* Target Push notification Search Start */
            $customer_id_search = $acc_number;
            $targetpush = array();
            if(!empty($customer_id_search)){
                $selectOffer = "SELECT * FROM " . $this->ava_targetpush_notificaiton . "  WHERE status = '1'  AND is_simple_offer = '2' AND is_delete = '0' AND (notification_start_date <= '".$date."' AND notification_end_date >= '".$date."' AND member_ids LIKE '%-".$customer_id_search."-%') ORDER BY created_at DESC";
                $resultOffer = $this->_db->my_query($selectOffer);
                if ($this->_db->my_num_rows($resultOffer) > 0) { 
                while($row = $this->_db->my_fetch_object($resultOffer)) {
                /* Target Push notification Search END */ 
                        $array = array();
                        $array['id'] 						= $row->targetpush_id;
                        $array['push_type'] 				= '2';
                        $array['create_at'] 				= $row->created_at;
                        $array['name']		 				= $row->targetpush_name;
                        $array['offer_short_description'] 	= $row->targetpush_short_description; 
                        $array['offer_long_description'] 	= $row->targetpush_long_description;
                        $array['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->targetpush_image_1;
                        $array['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->targetpush_image_2;
                        $array['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->targetpush_image_3;
                        $array['start_date'] 				= $row->notification_start_date;
                        $array['end_date'] 					= $row->notification_end_date;
                        $array['start_time'] 				= '00:00:00';
                        $array['end_time'] 					= '23:59:59';
                        $array['store_name'] 				= '';
                        $array['category_name'] 			= '';
                        $array['offer_price'] 				= $row->targetpush_price;
                        $array['is_banner_image'] 			= $row->is_banner_image;
                        $array['is_simple_offer'] 			= $row->is_simple_offer;
                        $array['barcode_image'] 			= IMG_URL.'barcodes'.DS.$row->barcode_image;
                        if($row->is_banner_image == 1) {
                            $array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->targetpush_image_1;
                        } else if($row->is_banner_image == 2) {
                            $array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->targetpush_image_2;
                        } else {
                            $array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->targetpush_image_3;
                        }
                        $targetpush[$row->created_at] = $array;

                    }
                } 
            
            }
          
			//$selectOffer = "SELECT * FROM ava_beacon_offers WHERE is_default = '0' AND (is_simple_offer = '1' OR (beacon_offer_id IN (SELECT beacon_offer_id FROM ava_beacon_customer_offer WHERE ACC_NUMBER = '".$customer_id."'))) AND is_delete = '0' AND status = '1' AND ((start_date < '".$date."' AND end_date > '".$date."') OR (start_date = '".$date."' AND start_time < '".$time."') OR (end_date = '".$date."' AND end_time < '".$time."'))  ORDER BY created_at DESC";
			$selectOffer = "SELECT * FROM ava_beacon_offers WHERE is_default = '0' AND (is_simple_offer = '1' OR (beacon_offer_id IN (SELECT beacon_offer_id FROM ava_beacon_customer_offer WHERE ACC_NUMBER = '".$customer_id."'))) AND is_delete = '0' AND status = '1' AND (start_date <= '".$date."' AND end_date >= '".$date."') AND (start_time <= '".$time."' AND end_time >= '".$time."')  ORDER BY created_at DESC";
			$resultOffer = $this->_db->my_query($selectOffer);
			if ($this->_db->my_num_rows($resultOffer) > 0) { 
				while($row = $this->_db->my_fetch_object($resultOffer)) {
					$start_time = explode(" ",$row->start_time);
					$end_time = explode(" ",$row->end_time);
					$array = array();
					if($row->is_simple_offer == 0) {
						$store_name = $this->_common->get_beacon_store($customer_id,$row->beacon_offer_id);
						
						$array['id'] 						= $row->beacon_offer_id;
						$array['name']		 				= $row->offer_name;
						$array['offer_short_description'] 	= $row->offer_short_description; 
						$array['offer_long_description'] 	= $row->offer_long_description;
						$array['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_1;
						$array['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_2;
						$array['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_3;
						$array['start_date'] 				= $row->start_date;
						$array['end_date'] 					= $row->end_date;
						$array['start_time'] 				= $row->start_time;
						$array['end_time'] 					= $row->end_time;
						$array['store_name'] 				= $store_name;
						$array['category_name'] 			= $this->_common->get_category_data_by_id($row->category_id);
						$array['offer_price'] 				= $row->offer_price;
						$array['is_banner_image'] 			= $row->is_banner_image;
						$array['is_banner_image'] 			= $row->is_simple_offer;
						$array['barcode_image'] 			= IMG_URL.'barcodes'.DS.$row->barcode_image;
						if($row->is_banner_image == 1) {
							$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_1;
						} else if($row->is_banner_image == 2) {
							$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_2;
						} else {
							$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_3;
						}
						$beacon[] = $array;
					} else {
						$array['id'] 						= $row->beacon_offer_id;
						$array['name']		 				= $row->offer_name;
                        $array['push_type'] 				= '1';
                        $array['create_at'] 				= $row->created_at;
						$array['offer_short_description'] 	= $row->offer_short_description; 
						$array['offer_long_description'] 	= $row->offer_long_description;
						$array['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_1;
						$array['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_2;
						$array['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->offer_image_3;
						$array['start_date'] 				= $row->start_date;
						$array['end_date'] 					= $row->end_date;
						$array['start_time'] 				= $row->start_time;
						$array['end_time'] 					= $row->end_time;
						$array['store_name'] 				= $this->_common->get_store_data_by_id($row->store_id);
						$array['category_name'] 			= $this->_common->get_category_data_by_id($row->category_id);
						$array['offer_price'] 				= $row->offer_price;
						$array['is_banner_image'] 			= $row->is_banner_image;
						$array['is_banner_image'] 			= $row->is_simple_offer;
						$array['barcode_image'] 			= IMG_URL.'barcodes'.DS.$row->barcode_image;
						if($row->is_banner_image == 1) {
							$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_1;
						} else if($row->is_banner_image == 2) {
							$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_2;
						} else {
							$array['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->offer_image_3;
						}
						$targetpush[$row->created_at] = $array;
					}
				}
                
                rsort($targetpush);                
                foreach($targetpush as $valArray){
                    $newArray[] = $valArray;
                }
                     
				$_response['result_code'] 	= 1;
				$_response['beacon'] 		= (count($beacon > 0) && count($newArray) == 0) ? $newArray : $beacon;
				$_response['simple_offer']  = (count($beacon > 0) && count($newArray) == 0) ? $beacon : $newArray;
				//$_response['targetpush']	= $targetpush;
			} else {
              
                /* target push notificatio for customer */
                if(!empty($targetpush)){
                     rsort($targetpush);
                     foreach($targetpush as $valArray){
                        $newArray[] = $valArray;
                     }
                     
                    //$_response['targetpush']	= $newArray;
                    $_response['simple_offer']	= $newArray;
                    $_response['result_code'] 	= 1;
                }else{                
                    $_response['result_code'] = 0;
                    $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_all_vip_offers.error.no_offer');
                }
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_all_vip_offers.error.required');
		}
		return $_response;
	} // get_all_vip_offers()
	
    public function get_beacon() {
		$selectBeacon = "SELECT * FROM " . $this->beacons_table . " WHERE status = '1' AND is_delete = '0'";
		$resultBeacon = $this->_db->my_query($selectBeacon);
		if ($this->_db->my_num_rows($resultBeacon) > 0) { 
			while($row = $this->_db->my_fetch_object($resultBeacon)) {
				$array = array();
				$array['id'] = $row->beacon_id;
				$array['name'] = $row->beacon_code;
				$array['description'] = $row->description;
				$result[] = $array;
			}
			$_response['result_code'] = 1;
			$_response['result'] = $result;
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_beacon.error.no_beacon');
		}
		return $_response;
	} //end get_offer()
	
	public function get_event_list() {
		$date = date('Y-m-d');
		$array = array();
		$selectEvent = "SELECT * FROM " . $this->events_table . " WHERE status = '1' AND is_delete = '0' AND event_date >= '".$date."' ORDER BY event_date DESC" ;
		$resultEvent = $this->_db->my_query($selectEvent);
		if ($this->_db->my_num_rows($resultEvent) > 0) {
			while($row = $this->_db->my_fetch_object($resultEvent)) {
				$gallery = array();
				$event_id = (isset($row->event_id)) ? trim($row->event_id) : "";
				$selectEventGallery = "SELECT * FROM " . $this->event_gallery_table . " WHERE event_id = '$event_id' AND image != ''" ;
				$resultEventGallery = $this->_db->my_query($selectEventGallery);
				if ($this->_db->my_num_rows($resultEventGallery) > 0) {
					while($rowrGallery = $this->_db->my_fetch_object($resultEventGallery)) {
						$image = (isset($rowrGallery->image)) ? trim($rowrGallery->image) : "";
						$gallery[] = IMG_URL.'event_images'.DS.$image;
					}
				}
				$array['event_id']	= (isset($row->event_id)) ? trim($row->event_id) : "";
				$array['event_name']	= (isset($row->event_name)) ? trim($row->event_name) : "";
				$array['event_short_description']	= (isset($row->event_short_description)) ? trim($row->event_short_description) : "";
				$array['event_long_description']	= (isset($row->event_long_description)) ? trim($row->event_long_description) : "";
				$array['event_address']	= (isset($row->event_address)) ? trim($row->event_address) : "";
				$event_image_1 = (isset($row->event_image_1)) ? trim($row->event_image_1) : "";
				$array['is_default_image']	= IMG_URL.'event_images'.DS.$row->event_image_1;
				$array['event_date']	= (isset($row->event_date)) ? trim($row->event_date) : "";
				//$array['start_date']	= (isset($row->start_date)) ? trim($row->start_date) : "";
				//$array['end_date']	= (isset($row->end_date)) ? trim($row->end_date) : "";
				$array['event_latitude']	= (isset($row->event_latitude)) ? trim($row->event_latitude) : "";
				$array['event_longitude']	= (isset($row->event_longitude)) ? trim($row->event_longitude) : "";
				$array['gallery']	= $gallery;
				$result[] = $array;
				
			}
			$_response['result_code'] = 1;
			$_response['result'] 	= $result;
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_event_list.error.no_event');
		}
		return $_response;
	} // end get_event_list()
	
	function get_promotion_list() {
		$datetime = date('Y-m-d H:i:s');
		$array = array();
		
		$selectPromotion = "SELECT * FROM " . $this->promotions_table . " WHERE status = '1' AND is_delete = '0' AND promo_end_date >= '".$datetime."' ORDER BY created_at DESC" ;
		$resultPromotion = $this->_db->my_query($selectPromotion);
		if ($this->_db->my_num_rows($resultPromotion) > 0) {
			while($row = $this->_db->my_fetch_object($resultPromotion)) {
				$array['promo_id']	= (isset($row->promo_id)) ? trim($row->promo_id) : "";
				$array['promo_name']	= (isset($row->promo_name)) ? trim($row->promo_name) : "";
				$array['promo_short_description']	= (isset($row->promo_short_desc)) ? trim($row->promo_short_desc) : "";
				$array['promo_long_description']	= (isset($row->promo_long_desc)) ? trim($row->promo_long_desc) : "";
				$promo_image_1 = (isset($row->promo_image_1)) ? trim($row->promo_image_1) : "";
				$promo_image_2 = (isset($row->promo_image_2)) ? trim($row->promo_image_2) : "";
				$promo_image_3 = (isset($row->promo_image_3)) ? trim($row->promo_image_3) : "";
				$array['promo_image_1']	= IMG_URL.'promotion_images'.DS.$promo_image_1;
				$array['promo_image_2']	= IMG_URL.'promotion_images'.DS.$promo_image_2;
				$array['promo_image_3']	= IMG_URL.'promotion_images'.DS.$promo_image_3;
				$array['promo_start_date']	= (isset($row->promo_start_date)) ? trim($row->promo_start_date) : "";
				$array['promo_end_date']	= (isset($row->promo_end_date)) ? trim($row->promo_end_date) : "";
				if($row->default_image_type == 1) {
					$array['is_default_image']	= IMG_URL.'promotion_images'.DS.$promo_image_1;
				} else if($row->is_default_image == 2) {
					$array['is_default_image']	= IMG_URL.'promotion_images'.DS.$promo_image_2;
				} else {
					$array['is_default_image']	= IMG_URL.'promotion_images'.DS.$promo_image_3;
				}
				$result[] = $array;
			}
			$_response['result_code'] = 1;
			$_response['result'] 	= $result;
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_promotion_list.error.no_promo');
		}
		return $_response;
	}

	function get_loyalty_list(){
		$datetime = date('Y-m-d H:i:s');
		$array = array();
		$lat1 = $this->_common->test_input($_REQUEST['lat']);
        $lng1 = $this->_common->test_input($_REQUEST['lng']);
       
        $member_id = $this->_common->test_input($_REQUEST['acc_number']);
		// if(!empty($lat1) && !empty($lng1)) {
			$selectLoyalty = "SELECT * FROM " . $this->loyalty_table . " WHERE status = '1' AND is_delete = '0' AND (loyalty_start_date	 <= '".$datetime."' AND loyalty_end_date >= '".$datetime."') ORDER BY position ASC" ;
			$resultLoyalty = $this->_db->my_query($selectLoyalty);
			if ($this->_db->my_num_rows($resultLoyalty) > 0) {
				while($row = $this->_db->my_fetch_object($resultLoyalty)) {
					$stores = array();
					$array['loyalty_id']	= (isset($row->loyalty_id)) ? trim($row->loyalty_id) : "";
					$array['loyalty_name']	= (isset($row->loyalty_name)) ? trim($row->loyalty_name) : "";
					$array['loyalty_short_desc']	= (isset($row->loyalty_short_desc)) ? trim($row->loyalty_short_desc) : "";
					$array['loyalty_long_desc']	= (isset($row->loyalty_long_desc)) ? trim($row->loyalty_long_desc) : "";
					$loyalty_image_1 = (isset($row->loyalty_image_1)) ? trim($row->loyalty_image_1) : "";
					$loyalty_image_2 = (isset($row->loyalty_image_2)) ? trim($row->loyalty_image_2) : "";
					$loyalty_image_3 = (isset($row->loyalty_image_3)) ? trim($row->loyalty_image_3) : "";
					$array['loyalty_image_1']	= IMG_URL.'loyalty_images'.DS.$loyalty_image_1;
					$array['loyalty_image_2']	= IMG_URL.'loyalty_images'.DS.$loyalty_image_2;
					$array['loyalty_image_3']	= IMG_URL.'loyalty_images'.DS.$loyalty_image_3;
					$array['loyalty_start_date']	= (isset($row->loyalty_start_date)) ? trim($row->loyalty_start_date) : "";
					$array['loyalty_end_date']	= (isset($row->loyalty_end_date)) ? trim($row->loyalty_end_date) : "";
					$array['free_qty']	= (isset($row->free_qty)) ? trim($row->free_qty) : "";
					$array['discount']	= (isset($row->discount)) ? trim($row->discount) : "0";
					$used_image 	= (isset($row->used_image)) ? trim($row->used_image) : "";
					$not_used_image = (isset($row->not_used_image)) ? trim($row->not_used_image) : "";
					$free_image 	= (isset($row->free_image)) ? trim($row->free_image) : "";
					$array['used_image']	= IMG_URL.'loyalty_images'.DS.$used_image;
					$array['not_used_image']= IMG_URL.'loyalty_images'.DS.$not_used_image;
					$array['free_image']	= IMG_URL.'loyalty_images'.DS.$free_image;
					$paid_qty	= (isset($row->paid_qty)) ? trim($row->paid_qty) : "";
					// Set used product count
					$trigger_product_ids = (isset($row->trigger_product_ids)) ? trim($row->trigger_product_ids) : "";
					$used_product_counts = $this->get_loyalty_purchase_data($member_id,$trigger_product_ids,$array['loyalty_start_date'],$array['loyalty_end_date'],$paid_qty,$array['free_qty']);
					$array['used_qty']	 = $used_product_counts;
					
					// manage remaining quantity
					$remain_qty = $paid_qty-$used_product_counts;
					$array['paid_qty']   = $remain_qty; 
					$array['unused_qty'] = $remain_qty;
					
					if($row->default_image_type == 1) {
						$array['is_default_image']	= IMG_URL.'loyalty_images'.DS.$loyalty_image_1;
					} else if($row->is_default_image == 2) {
						$array['is_default_image']	= IMG_URL.'loyalty_images'.DS.$loyalty_image_2;
					} else {
						$array['is_default_image']	= IMG_URL.'loyalty_images'.DS.$loyalty_image_3;
					}
					$store_id 	= (isset($row->store_id)) ? trim($row->store_id) : "";
					$expStore = explode(',',$store_id);
					foreach($expStore as $key=>$value) {
						if($value != '') {
							$storeArray = array();
							$sqlStore = "SELECT * FROM " . $this->stores_table . " WHERE store_id = '" . $value . "'";
							$resultStore = $this->_db->my_query($sqlStore);
							if ($this->_db->my_num_rows($resultStore) > 0) {
								while($row1 = $this->_db->my_fetch_object($resultStore)) {
									$lat2 				= (isset($row1->store_latitude)) ? trim($row1->store_latitude) : "";
									$lng2 				= (isset($row1->store_longitude)) ? trim($row1->store_longitude) : "";
									$distance 			= $this->distance($lat1, $lng1, $lat2, $lng2, $unit);
									$storeArray['store_id']	 = (isset($row1->store_id)) ? trim($row1->store_id) : "";
									$storeArray['store_name'] = (isset($row1->store_name)) ? trim($row1->store_name) : "";
									$storeArray['lat'] 		= (isset($row1->store_latitude)) ? trim($row1->store_latitude) : "";
									$storeArray['lng'] 		= (isset($row1->store_longitude)) ? trim($row1->store_longitude) : "";
									$storeArray['distance'] 	= strval(round($distance,2));
								}
								$stores[] = $storeArray;
							}
						}
					}
					foreach ($stores as $key2 => $row2) {
						$mid[$key2]  = $row2['distance'];
					}
					array_multisort($mid, SORT_ASC, $stores);
					$array['stores'] = $stores;
					$result[] = $array;
				}
				$_response['result_code'] = 1;
				$_response['result'] 	= $result;
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_loyalty_list.error.no_loyality');
			}
		//} else {
			//$_response['result_code'] = 0;
			//$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_loyalty_list.error.required');
		//}
		return $_response;
	} // get_loyalty_list
	
	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return ($miles * 1.609344);
		}
	}
	
	/**
	 * Function to get loyalty purchase count in loyalty date range
	 * @input int $member_id , string $trigger_product_ids , string $start_date , string $end_date
	 * @output int
	 * 
	 */
	 public function get_loyalty_purchase_data($member_id='',$trigger_product_ids='',$start_date='',$end_date='',$buy_qty=0,$discount_qty=0) {
		// set loyalty start date
		$start_date = date('Ymd',strtotime($start_date));
		// set loyalty end date
		$end_date = date('Ymd',strtotime($end_date));
		// connecting with mssql live server db
		$ms_con = $this->_mssql->mssql_db_connection();
		// get member's purchase product count in loyalty date range 
		$member_query = "select Cast(Sum(ATRX_QTY) as Integer)  % (".$buy_qty." + ".$discount_qty.") as purchase_count from ACCOUNT_TRXTBL where ATRX_ACCOUNT = ".$member_id." and ATRX_PRODUCT in (".$trigger_product_ids.") and ATRX_DATE between '".$start_date."' and '".$end_date."' and ATRX_TYPE = 'ITEMSALE' having Sum(ATRX_QTY) > 0";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$ms_con);
		if($this->_mssql->pdo_num_rows_query($member_query,$ms_con) > 0 ) {
			// Fetch the result row
			//$row = mssql_fetch_row($get_member_data);
			$row = $this->_mssql->mssql_fetch_object_query($get_member_data);
		}
		// set purchased count 
		$purchase_count = (isset($row) && !empty($row)) ? $row->purchase_count : 0;
		return $purchase_count;
	}
	
	
	public function get_event_promo_list() {
		$date = date('Y-m-d');
		$datetime = date('Y-m-d H:i:s');
		$current_date = date('Ymd');
		$array = array();
		$promo_array = array();
		$event_list = array();
		$promo_list = array();
		$selectEvent = "SELECT * FROM " . $this->events_table . " WHERE status = '1' AND is_delete = '0' AND event_date >= '".$current_date."' ORDER BY event_date ASC" ;
		$resultEvent = $this->_db->my_query($selectEvent);
		if ($this->_db->my_num_rows($resultEvent) > 0) {
			while($row = $this->_db->my_fetch_object($resultEvent)) {
				$gallery = array();
				$event_id = (isset($row->event_id)) ? trim($row->event_id) : "";
				$selectEventGallery = "SELECT * FROM " . $this->event_gallery_table . " WHERE event_id = '$event_id' AND image != ''" ;
				$resultEventGallery = $this->_db->my_query($selectEventGallery);
				if ($this->_db->my_num_rows($resultEventGallery) > 0) {
					while($rowrGallery = $this->_db->my_fetch_object($resultEventGallery)) {
						$image = (isset($rowrGallery->image)) ? trim($rowrGallery->image) : "";
						$gallery[] = IMG_URL.'event_images'.DS.$image;
					}
				}
				$array['event_id']	= (isset($row->event_id)) ? trim($row->event_id) : "";
				$array['event_name']	= (isset($row->event_name)) ? trim($row->event_name) : "";
				$array['event_short_description']	= (isset($row->event_short_description)) ? trim($row->event_short_description) : "";
				$array['event_long_description']	= (isset($row->event_long_description)) ? trim($row->event_long_description) : "";
				$array['event_address']	= (isset($row->event_address)) ? trim($row->event_address) : "";
				$event_image_1 = (isset($row->event_image_1)) ? trim($row->event_image_1) : "";
				$array['is_default_image']	= IMG_URL.'event_images'.DS.$row->event_image_1;
				$array['event_date']	= (isset($row->event_date)) ? trim($row->event_date) : "";
				//$array['start_date']	= (isset($row->start_date)) ? trim($row->start_date) : "";
				//$array['end_date']	= (isset($row->end_date)) ? trim($row->end_date) : "";
				$array['event_latitude']	= (isset($row->event_latitude)) ? trim($row->event_latitude) : "";
				$array['event_longitude']	= (isset($row->event_longitude)) ? trim($row->event_longitude) : "";
				$array['gallery']	= $gallery;
				$event_list[] = $array;
				
			}
		}
		
		$selectPromotion = "SELECT * FROM " . $this->promotions_table . " WHERE status = '1' AND is_delete = '0' AND promo_end_date >= '".$datetime."' ORDER BY created_at DESC" ;
		$resultPromotion = $this->_db->my_query($selectPromotion);
		if ($this->_db->my_num_rows($resultPromotion) > 0) {
			while($row = $this->_db->my_fetch_object($resultPromotion)) {
				$promo_array['promo_id']	= (isset($row->promo_id)) ? trim($row->promo_id) : "";
				$promo_array['promo_name']	= (isset($row->promo_name)) ? trim($row->promo_name) : "";
				$promo_array['promo_short_description']	= (isset($row->promo_short_desc)) ? trim($row->promo_short_desc) : "";
				$promo_array['promo_long_description']	= (isset($row->promo_long_desc)) ? trim($row->promo_long_desc) : "";
				$promo_image_1 = (isset($row->promo_image_1)) ? trim($row->promo_image_1) : "";
				$promo_image_2 = (isset($row->promo_image_2)) ? trim($row->promo_image_2) : "";
				$promo_image_3 = (isset($row->promo_image_3)) ? trim($row->promo_image_3) : "";
				$promo_array['promo_image_1']	= IMG_URL.'promotion_images'.DS.$promo_image_1;
				$promo_array['promo_image_2']	= IMG_URL.'promotion_images'.DS.$promo_image_2;
				$promo_array['promo_image_3']	= IMG_URL.'promotion_images'.DS.$promo_image_3;
				$promo_array['promo_start_date']	= (isset($row->promo_start_date)) ? trim($row->promo_start_date) : "";
				$promo_array['promo_end_date']	= (isset($row->promo_end_date)) ? trim($row->promo_end_date) : "";
				if($row->default_image_type == 1) {
					$promo_array['is_default_image']	= IMG_URL.'promotion_images'.DS.$promo_image_1;
				} else if($row->is_default_image == 2) {
					$promo_array['is_default_image']	= IMG_URL.'promotion_images'.DS.$promo_image_2;
				} else {
					$promo_array['is_default_image']	= IMG_URL.'promotion_images'.DS.$promo_image_3;
				}
				$promo_list[] = $promo_array;
			}
		}
		$_response['result_code'] = 1;
		$_response['event_list'] 	= $event_list;
		$_response['promo_list'] 	= $promo_list;
		// set messages in case of blank entries
		$_response['event_error_msg'] = (count($event_list) == 0) ? $this->_common->langText($this->_locale,'txt.get_event_list.error.no_event') : '';
		$_response['promo_error_msg'] = (count($promo_list) == 0) ? $this->_common->langText($this->_locale,'txt.get_promotion_list.error.no_promo') : '';
		if(count($event_list) == 0 && count($promo_list) == 0) {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.no_record_found');
		}
		return $_response;
		
	}
	
	/**
	 * Function used to fetch customer's saved prize count
	 * @param int member_id
	 * @return void
	 * @access private
	 * 
	 */
	private function saved_prize_count($member_id) {
		$saved_prize_count = 0;
		if(!empty($member_id)) {
			$date= date('Y-m-d');
			// get scatchnwin details whos not expired
			$snw_winner_query  = "SELECT wp.snw_id FROM " . $this->scratch_n_win_prize_winners_table . " wp JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = wp.snw_id LEFT JOIN " . $this->scratch_n_win_products_table . " snwp ON wp.snw_product_id = snwp.id WHERE (wp.is_redeemed = 2 OR wp.is_second_chance = 1) AND wp.member_id = '$member_id' AND snw.status = '1' AND wp.status = '1' AND wp.is_second_chance <> 1 AND snw.status = 1 AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND wp.prize_declare_at = '".$date."'";
			$snw_prizes_results = $this->_db->my_query($snw_winner_query);
			$snw_saved_prize_count = $this->_db->my_num_rows($snw_prizes_results);
			// get cracktheegg details whos not expired
			$cte_winner_query  = "SELECT ctew.cte_id FROM " . $this->cte_winners_table . " ctew JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = ctew.cte_id WHERE ctew.member_id = '$member_id' AND snw.status = 1 AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND ctew.prize_declare_at = '$date'";
			$cte_prizes_results = $this->_db->my_query($cte_winner_query);
			$cte_saved_prize_count = $this->_db->my_num_rows($cte_prizes_results);
			// total saved prizes
			$saved_prize_count = $snw_saved_prize_count+$cte_saved_prize_count;
		}
		return $saved_prize_count;
	}
	
	/**
	 * Function used to fetch customer's push count for active prizes
	 * @param int member_id
	 * @return void
	 * @access private
	 * 
	 */
	private function customer_push_count($member_id=0) {
		$push_count = 0;
		if(!empty($member_id)) {
			$date= date('Y-m-d');
			$datetime= date('Y-m-d H:i:s');
            $current_day = strtolower(date('l'));
			// get customer's push count of perticuler scratch n win entry
			$snw_push_query  = "(SELECT snw.snw_id,snw.campaign_type,snw.title,snw.start_date,snw.end_date,snw.second_chance_draw,snw.second_chance_count,snw.banner_image,snwp.product_id,snwp.product_name,snwp.product_image,
				snwp.id as snw_product_id,pc.push_sent_at,pc.snw_push_id as ctelogid FROM " . $this->scratch_n_win_products_table . " snwp 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id
				JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
				WHERE snwp.product_image <> ''  AND snw.campaign_type = '1' AND snwp.status = 1 AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date' AND pc.push_sent_at  = '$date' AND NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = snwp.snw_id AND member_id = '$member_id' AND prize_declare_at = '$date') GROUP BY snwp.snw_id ORDER BY snw.snw_id DESC)
                ";  	
			$active_prizes = array();
            $snw_push_result = $this->_db->my_query($snw_push_query);
			if ($this->_db->my_num_rows($snw_push_result) > 0) {
				
				while($row = $this->_db->my_fetch_object($snw_push_result)) {
                    $campaign_type = (isset($row->campaign_type)) ? $row->campaign_type : "";
					$snw_member_id = (isset($row->member_id)) ? $row->member_id : "";
					$snw_member_ids = (isset($row->member_ids)) ? explode(',',$row->member_ids) : "";
					$push_sent_at	= (isset($row->push_sent_at)) ? date('Y-m-d',strtotime($row->push_sent_at)) : "";
					
					//if(in_array($member_id,$snw_member_ids) && $push_sent_at == $date) {
					if($push_sent_at == $date) {
						// prepare prize values
						$array['snw_id']	= (isset($row->snw_id)) ? trim($row->snw_id) : "";
						// append active prize array values
						$active_prizes[] = $array;
					} 
				}
			}
			/* Check For Crack the egg */
			$snw_push_query1 = "SELECT snw.snw_id,snw.campaign_type,snw.title,snw.start_date,snw.end_date,snw.banner_image,pc.push_sent_at FROM " . $this->cte_brands_table . " cteb 
			JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id
			JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = cteb.cte_id
			WHERE  cteb.brand_image <> ''  AND snw.campaign_type = '3' AND cteb.status = 1 AND pc.push_sent_at  = '$date' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date'
			AND NOT EXISTS (SELECT id FROM " . $this->cte_winners_table . " WHERE cte_id = cteb.cte_id AND member_id = '$member_id' AND prize_declare_at = '$date') GROUP BY cteb.cte_id ORDER BY snw.snw_id DESC";		
			$snw_push_result1 = $this->_db->my_query($snw_push_query1);
			if ($this->_db->my_num_rows($snw_push_result1) > 0) {
				while($row1 = $this->_db->my_fetch_object($snw_push_result1)) {
					$campaign_type = (isset($row1->campaign_type)) ? $row1->campaign_type : "";
					// prepare prize values
					$array['snw_id']	= (isset($row->snw_id)) ? trim($row->snw_id) : "";
					// append active prize array values
					$active_prizes[] = $array;
				}
			}
			if(count($active_prizes) > 0) {
				// append basic information in result array
				$push_count = count($active_prizes);
			}
		}
		return $push_count;
	}
	
	/**
	 * Function used to manage members notification counts
	 * @param int member_id
	 * @return void
	 * @access public
	 * 
	 */
	public function update_notification_counts($member_id) {
		//$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		$datetime = date('Y-m-d H:i:s');
		// set default response for invalid member
		$_response['push_count'] = 0;
		$_response['saved_prize_count'] = 0;
		if(!empty($member_id)) {
			// check member is exclude from competition or not
			$is_member_include_in_competition = $this->is_member_include_in_competition($member_id);
			if($is_member_include_in_competition == 1) {
				
				// get giveaways count
				$giveaways_count = $this->customer_push_count($member_id);
				// get saved prize count
				$saved_prize_count = $this->saved_prize_count($member_id);
				// get member's notification count
				$notification_query  = "SELECT id FROM " . $this->scratch_n_win_notification_log_table . " WHERE member_id = '$member_id'";
				$notification_results = $this->_db->my_query($notification_query);
				
				if ($this->_db->my_num_rows($notification_results) > 0) {
				
					// update member's notification count
					$update_array = array("giveaway_count" => $giveaways_count,"prize_count" => $saved_prize_count,"modified_at" =>$datetime);// Update values
					$where_clause = array("member_id" => $member_id);
					$this->_db->UpdateAll($this->scratch_n_win_notification_log_table, $update_array, $where_clause, "");	
				} else {
					
					// insert member's notification count
					$insert_query = "INSERT INTO ".$this->scratch_n_win_notification_log_table."(member_id,giveaway_count,prize_count,modified_at) VALUES ('$member_id','$giveaway_count','$prize_count','$datetime')";
					$this->_db->my_query($insert_query);
					$prize_winner_id = mysql_insert_id();
				}
				
				// set response data
				$_response['push_count'] = $giveaways_count;
				$_response['saved_prize_count'] = $saved_prize_count;
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to check user's competition existance 
	 * @param int acc_number
	 * @return void
	 * @access private
	 * 
	 */
	private function is_member_include_in_competition($acc_number=0) {
		$return_res = 0;
		// get member's log
		$member_query = "SELECT MEMB_NUMBER FROM MEMBER WHERE MEMB_Exclude_From_Competitions = 0 AND MEMB_NUMBER = $acc_number";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$this->con);
		if($this->_mssql->pdo_num_rows_query($member_query,$this->con) > 0 ) {
			$return_res = 1;
		}
		return $return_res;
	}
  
} // End users class

?> 
