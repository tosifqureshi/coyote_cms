<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : Apl-25 11:15AM
 * Copyright : Coyote team
 *
 */
class ordering {
	
    public $_response = array();
    public $result = array();
    private $OUTPTBL = 'OUTPTBL';
    private $PRODTBL = 'PRODTBL';
    private $CODETBL = 'CODETBL';
    private $user_table = 'USERTBL';
    private $cart_table = 'ava_cart';
    private $cart_items_table = 'ava_cart_items';
    private $fav_products_table = 'ava_fav_products';
	private $stores_table = 'ava_stores_log';
	private $app_order_table = 'APP_ORDERS';
	private $app_order_items_table = 'APP_ORDER_ITEMS';
	private $app_store_order_table = 'AppStoreOrderTbl';
    private $supplier_device = 'ava_supplier_device';
    private $settings_table = 'ava_settings';
	
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
		$this->_mssql = new mssql(); // Create instance of mssql class
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->ms_con = $this->_mssql->mssql_db_connection(); // connecting with mssql live server db
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
    }
    
	/**
	 * Function used to get outlet listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function outlets() {
		// get post data
		$user_id = $this->_common->test_input($_REQUEST['customer_id']);
		$lat1 	 = $this->_common->test_input($_REQUEST['lat']);
        $lng1 	 = $this->_common->test_input($_REQUEST['lng']);
        $_response['result_code'] = 0; // set default result code
        // check customer id exists or not
        if(empty($user_id)) {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.session');
			return $_response;
		}
		
        // get stores
		//$select_store = "SELECT * FROM " . $this->stores_table . " WHERE status = '1' ORDER BY store_id";
		$select_store = "SELECT * FROM " . $this->stores_table . " WHERE status = '1' and is_online = 1 and display_on_app = 1 ORDER BY store_id";
		$result_store = $this->_db->my_query($select_store);
		if ($this->_db->my_num_rows($result_store) > 0) {
			$stores = array();
			while($row = $this->_db->my_fetch_object($result_store)) {
				
				// get store distance
				$lat2 = (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
				$lng2 = (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
				$distance = $this->distance($lat1, $lon1, $lat2, $lon2);
				// prepare outlet values
				$array['outlet_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
				$array['outlet_name'] = (isset($row->store_name)) ? trim($row->store_name) : "";
				$array['lat'] 		= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
				$array['lng'] 		= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
				$array['distance'] 	= strval(round($distance,2));
				$stores[] = $array;
			}
			
			foreach ($stores as $key => $row) {
				$mid[$key]  = $row['distance'];
			}
			array_multisort($mid, SORT_ASC, $stores);
			$cart_data    = $this->user_cart_data($user_id);
			$_response['result_code']= 1;
			$_response['cart_id']    = (isset($cart_data['cart_id'])) ? $cart_data['cart_id'] : '';
			$_response['outlet_id'] = (isset($cart_data['outlet_id'])) ? $cart_data['outlet_id'] : '';
			$_response['outlet_name']= (isset($cart_data['outlet_name'])) ? $cart_data['outlet_name'] : '';
			$_response['cart_item_count']  = (isset($cart_data['cart_item_count'])) ? $cart_data['cart_item_count'] : '';
			$_response['fav_products'] = $this->my_fav_product_ids($user_id); // user's favorite products
			$_response['result'] = $stores;
			
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.get_store.error.no_store');
		}
		$_response['error_msg'] = ($_response['result_code'] == 0) ? $_response['message'] : '';
		return $_response;
	}
	
	/**
	 * Function used to get user's favorite products
	 * @param int user_id
	 * @return array
	 * @access private
	 * 
	 */
	private function my_fav_product_ids($user_id=0) {
		$products = array();
		if(!empty($user_id)) {
			// get favorite product data
			$fav_sql = "SELECT distinct(fav_product_id) FROM " . $this->fav_products_table . " WHERE user_id = ".$user_id;
			$fav_result = $this->_db->my_query($fav_sql);
			if ($this->_db->my_num_rows($fav_result) > 0) {
				// set total product count
				$total_products = $this->_db->my_num_rows($fav_result);
				while($row = $this->_db->my_fetch_object($fav_result)) {
					// set product values
					$products[] = (isset($row->fav_product_id)) ? trim($row->fav_product_id) : "";
				}
			}
		}
		return $products;
	}
	
	/**
	 * Function used to get user's cart details
	 * @param int user_id
	 * @return array
	 * @access private
	 * 
	 */
	 private function user_cart_data($user_id=0) {
		// get user's cart details
		$cart_sql = "SELECT c.cart_id,c.outlet_id,c.total_amount,st.store_name,count(cp.cart_item_id) as cart_item_count FROM ".$this->cart_table." c LEFT JOIN ".$this->cart_items_table." cp ON c.cart_id = cp.cart_id and cp.is_deleted = 0 LEFT JOIN ".$this->stores_table." st ON c.outlet_id = st.ms_store_id and st.status =1 WHERE c.status = 1 AND c.user_id = ".$user_id;
		
		$cart_result = $this->_db->my_query($cart_sql);
		$cart_details = array();
		if ($this->_db->my_num_rows($cart_result) > 0) {
			$cart_row = $this->_db->my_fetch_object($cart_result);
			// prepare cart values
			$cart_details['cart_id'] = (isset($cart_row->cart_id)) ? trim($cart_row->cart_id) : "";
			$cart_details['outlet_id'] = (isset($cart_row->outlet_id)) ? trim($cart_row->outlet_id) : "";
			$cart_details['outlet_name'] = (isset($cart_row->store_name)) ? trim($cart_row->store_name) : "";
			$cart_details['total_amount'] = (isset($cart_row->total_amount)) ? trim($cart_row->total_amount) : "";
			$cart_details['cart_item_count'] = (isset($cart_row->cart_item_count)) ? trim($cart_row->cart_item_count) : "";
		}
		return $cart_details;
	 }
	
	/**
	 * Function used to calculate distance from locations
	 * @param float lat1 , float lon1 , float lat2 , float lon2 
	 * @return string
	 * @access private
	 * 
	 */
	private function distance($lat1, $lon1, $lat2, $lon2, $unit='') {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return ($miles * 1.609344);
		}
	}
	
	/**
	 * Function used to get departments of outlet
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function departments() {
		//get post user params
		$outlet_id   = $this->_common->test_input($_REQUEST['outlet_id']);
		// get department data
		$department_query = "SELECT CODE_KEY_NUM, CODE_DESC FROM " . $this->OUTPTBL . " join " . $this->PRODTBL . "  on Outp_Product = Prod_Number join " . $this->CODETBL . "  on CODE_KEY_NUM = PROD_DEPARTMENT where  OUTP_OUTLET = $outlet_id and OUTP_STATUS = 'Active' and CODE_KEY_TYPE = 'DEPARTMENT' and PROD_NATIONAL = 0 GROUP BY CODE_KEY_NUM, CODE_DESC ORDER BY CODE_DESC";
		$get_department_data = $this->_mssql->my_mssql_query($department_query,$this->ms_con);
		$departments = array();
		if( $this->_mssql->pdo_num_rows_query($department_query,$this->ms_con) > 0 ) {
			while( $result = $this->_mssql->mssql_fetch_object_query($get_department_data) ) {
				// set department values
				$array['department_id'] 	= (isset($result->CODE_KEY_NUM)) ? trim($result->CODE_KEY_NUM) : "";
				$array['department_name']	= (isset($result->CODE_DESC)) ? trim($result->CODE_DESC) : "";
				$array['department_image']	= IMG_URL."No_Image_Available.png";
				$departments[] = $array;
			}
			
			$_response['result_code'] = 1;
			$_response['result'] 	= $departments;
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_department');
		}
		return $_response;
	}
	
	/**
	 * Function used to get products listing
	 * @param int outlet_id, int department_id, int limit, int offset
	 * @return array
	 * @access public
	 * 
	 */
	public function products() {
		// get post data
		$customer_id = $this->_common->test_input($_REQUEST['customer_id']);
		$outlet_id   = $this->_common->test_input($_REQUEST['outlet_id']);
		$department_id  = $this->_common->test_input($_REQUEST['department_id']);
		$search_keyword = $_REQUEST['search_keyword'];
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
		if(!empty($outlet_id)) {
			// connecting with mssql live server db
			$ms_con = $this->_mssql->mssql_db_connection();
			if(!empty($department_id)) {
				// get total count of department's products
				$product_count_query = "SELECT count(Prod_Number) as product_count FROM  " .$this->OUTPTBL. " JOIN " .$this->PRODTBL. " on Outp_Product = Prod_Number JOIN " .$this->CODETBL. " on CODE_KEY_NUM = PROD_DEPARTMENT WHERE  OUTP_OUTLET = $outlet_id AND CODE_KEY_NUM = $department_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND CODE_KEY_TYPE = 'DEPARTMENT' AND PROD_NATIONAL = 0";
			} else {
				// get total count of outlet's products
				$product_count_query = "SELECT count(Prod_Number) as product_count FROM  " .$this->OUTPTBL. " JOIN " .$this->PRODTBL. " on Outp_Product = Prod_Number WHERE OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND PROD_NATIONAL = 0";
			}
			
			// merge search entity in query
			if(!empty($search_keyword)) {
				// set search key value
				$search_keyword1 = str_replace("'","''",$search_keyword);
				$product_count_query .= " AND Prod_Desc like '%$search_keyword1%'";
			}
			
			$product_count_data = $this->_mssql->my_mssql_query($product_count_query,$this->ms_con);
			if( $this->_mssql->pdo_num_rows_query($product_count_query,$this->ms_con) > 0 ) {
				$product_count = $this->_mssql->mssql_fetch_object_query($product_count_data);
				if(!empty($department_id)) {
					// get department's products listing
					$product_query = "SELECT * FROM ( SELECT Prod_Number, Prod_Desc,PROD_POS_DESC, OUTP_NORM_PRICE_1, CODE_KEY_NUM, CODE_DESC, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number JOIN ".$this->CODETBL." on CODE_KEY_NUM = PROD_DEPARTMENT where  OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND CODE_KEY_TYPE = 'DEPARTMENT' AND CODE_KEY_NUM = $department_id AND PROD_NATIONAL = 0";
				} else {
					// get outlets's products listing
					$product_query = "SELECT * FROM ( SELECT Prod_Number, Prod_Desc,PROD_POS_DESC, OUTP_NORM_PRICE_1, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number where OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND PROD_NATIONAL = 0 ";
				}
				
				// merge search entity in query 
				if(!empty($search_keyword)) {
					// set search key value
					$search_keyword2 = str_replace("'","''",$search_keyword);
					$product_query .= " AND Prod_Desc like '%$search_keyword2%'";
				}
				// set limit string in query
				$product_query .= ") a WHERE row > ".$offset." AND row <= ".$limit;
				//$filename =  BASEPATH.'/log.txt'; 
				//file_put_contents($filename, $product_query);
				$get_product_data = $this->_mssql->my_mssql_query($product_query,$ms_con);
				$products = array();
				if( $this->_mssql->pdo_num_rows_query($product_query,$ms_con) > 0 ) {
					while( $result = $this->_mssql->mssql_fetch_object_query($get_product_data) ) {
						
						// set product values
						$array['prod_desc'] 	= (isset($result->Prod_Desc)) ? utf8_encode($result->Prod_Desc) : "";
						$array['prod_number']	= (isset($result->Prod_Number)) ? trim($result->Prod_Number) : "";
						$array['prod_pos_desc'] = (isset($result->PROD_POS_DESC)) ? utf8_encode($result->PROD_POS_DESC) : "";
						$array['prod_price']	= (isset($result->OUTP_NORM_PRICE_1)) ? trim($result->OUTP_NORM_PRICE_1) : "";
						$array['prod_cart_qty'] = 1;
						$array['prod_image']	= IMG_URL."No_Image_Available.png";
						//$array['is_fav']      = $this->is_product_fav($customer_id,$array['prod_number'],$outlet_id);
						$products[] = $array;
					}
                    
					// get cart limit
                    $cart_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'cart_product_limit'");
                    $cart_setting_result = $this->_db->my_fetch_object($cart_query);
                    $cart_product_limit = (!empty($cart_setting_result)) ? $cart_setting_result->value : 0;
        			// set response values
					$_response['result_code'] = 1;
                    $_response['message'] = '';
		            $_response['cart_product_limit'] = $cart_product_limit; // Product limit
        			$_response['total_products'] = (isset($product_count->product_count)) ? $product_count->product_count : 0;
					$_response['fav_products'] = $this->my_fav_product_ids($customer_id); // user's favorite products
					$_response['result'] 	= $products;
				} else {
					$_response['result_code'] = 0;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
				}
			} else {
				$_response['result_code'] = 0;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.outlet_not_valid');
		}
		return $_response;
	} // get_products
	
	 /**
	 * Function used to check product's status ; is active or not
	 * @param int prod_number , int outlet_id 
	 * @return bool
	 * @access private
	 * 
	 */
	private function check_product_status($prod_number,$outlet_id) {
		// set default response as active
		$response_result = false;
		if(!empty($prod_number) && !empty($outlet_id)) {
			// get outlets's product details
			$product_query = "SELECT OUTP_NORM_PRICE_1 FROM ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number where OUTP_OUTLET = '$outlet_id' AND Prod_Number = '$prod_number' AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active' AND PROD_NATIONAL = 0 ";
			$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->ms_con);
			$result = $this->_mssql->mssql_fetch_object_query($get_product_data);
			if(!empty($result)) {
				// set default response as inactive
				$response_result = (isset($result->OUTP_NORM_PRICE_1)) ? $result->OUTP_NORM_PRICE_1 : 0;
			}
		}
		return $response_result;
	 }
	
	/**
	 * Function used to get user's product favourite status
	 * @param int user_id,int product_id,int outlet_id
	 * @return bool
	 * @access private
	 * 
	 */
	 private function is_product_fav($user_id=0,$product_id=0,$outlet_id=0) {
		$return = 0;
		if(!empty($user_id) && !empty($product_id) && !empty($outlet_id)) {
			// get favorite product data
			$fav_sql = "SELECT fav_id FROM " . $this->fav_products_table . " WHERE fav_product_id = $product_id and user_id = ".$user_id;
			$fav_result = $this->_db->my_query($fav_sql);
			if ($this->_db->my_num_rows($fav_result) > 0) {
				$return = 1;
			}
		}
		return $return;
	 }
	
	/**
	 * Function used to create cart
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function create_cart() {
		// get post data
		$user_id               = $this->_common->test_input($_REQUEST['customer_id']);
		$outlet_id             = $this->_common->test_input($_REQUEST['outlet_id']);
		$product_id            = $this->_common->test_input($_REQUEST['product_id']);
		$product_name          = $this->_common->test_input($_REQUEST['product_name']);
		$product_description   = $this->_common->test_input($_REQUEST['product_description']);
		$product_price         = $this->_common->test_input($_REQUEST['product_price']);
		$product_qty           = $this->_common->test_input($_REQUEST['product_qty']);
		$device_token   	   = $this->_common->test_input($_REQUEST['device_token']);
		// set default failure respose values
		$_response['result_code'] = 0;
        // get cart limit
        $cart_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'cart_product_limit'");
        $cart_setting_result = $this->_db->my_fetch_object($cart_query);
        $cart_product_limit = (!empty($cart_setting_result)) ? $cart_setting_result->value : 0;
        
		if(!empty($user_id) && !empty($product_id) && !empty($outlet_id) && !empty($product_qty)) {
			// prepare cart values
			$cart_data = array();
			$cart_data['user_id']    = $user_id;
			$cart_data['outlet_id']  = $outlet_id;
			$cart_data['product_id'] = $product_id;
			$cart_data['product_name'] = mysql_real_escape_string($product_name);
			$cart_data['product_description'] = mysql_real_escape_string($product_description);
			$cart_data['product_qty'] = $product_qty;
			$cart_data['price'] = $product_price;
			// set total product price
			$total_price = $product_price*$product_qty;
			$cart_data['total_price'] = $total_price;
			
			// get user's active cart details
			$user_cart = $this->get_user_cart_data($user_id);

			if(!empty($user_cart) && isset($user_cart->cart_id)) {
				// check for mismatch outlets
				if($outlet_id != $user_cart->outlet_id) {
					// set response values
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cart_outlets');
					return $_response;
				}
				
				/*if(!empty($user_cart->device_token) && $device_token != $user_cart->device_token) {
					// return error msg if request coming from diffrent device
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_device_request');
					return $_response;
				}*/
				
				$cart_id = $user_cart->cart_id;
                
                $cart_sql = "SELECT cart_item_id,product_qty,total_price FROM " . $this->cart_items_table . " WHERE is_deleted = 0 and cart_id = '".$cart_id."' and product_id = '".$product_id."'";
                $cart_result = $this->_db->my_query($cart_sql);
                if ($this->_db->my_num_rows($cart_result) > 0) {
                    $row = $this->_db->my_fetch_object($cart_result);
                    if(!empty($row) && isset($row->cart_item_id)) {
                    // set product cart values
                        $product_qty = $row->product_qty+$product_qty;                
                        if($cart_product_limit < $product_qty){
                            $_response['result_code'] = 0;
                            $_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.cart_limit').''.$cart_product_limit;
                            return $_response;
                        }
                    }
                }
            
				// update cart item data
				$this->update_cart_data($cart_data,$user_cart);
				// manage cart item store procedure
				$this->manage_cart_items($cart_id,$cart_data);
				
			} else {
				// insert cart details
				$cart_id = $this->insert_cart_data($cart_data);
				// manage cart item store procedure
				$this->manage_cart_items($cart_id,$cart_data);
			}
			
			// set response values
			$_response['result_code'] = 1;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.success.added_cart');
			// get cart details 
			$cart_data = $this->user_cart_data($user_id);
			$_response['cart_id']    = (isset($cart_data['cart_id'])) ? $cart_data['cart_id'] : '';
			$_response['outlet_id']  = (isset($cart_data['outlet_id'])) ? $cart_data['outlet_id'] : '';
			$_response['outlet_name']= (isset($cart_data['outlet_name'])) ? $cart_data['outlet_name'] : '';
			$_response['cart_item_count'] = (isset($cart_data['cart_item_count'])) ? $cart_data['cart_item_count'] : '';
			//$_response['cart_data'] = $this->get_cart_details($cart_id);
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.invalid_cart');
		}
		
		return $_response;
	}
	
	/**
	 * Function used to create cart
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function update_cart() {
		// get post data
		$user_id       = $this->_common->test_input($_REQUEST['customer_id']);
		$outlet_id     = $this->_common->test_input($_REQUEST['outlet_id']);
		$cart_id       = $this->_common->test_input($_REQUEST['cart_id']);
		$product_id    = $this->_common->test_input($_REQUEST['product_id']);
		$product_price = $this->_common->test_input($_REQUEST['product_price']);
		$product_qty   = $this->_common->test_input($_REQUEST['product_qty']);
		$device_token  = $this->_common->test_input($_REQUEST['device_token']);
		// set default failure respose values
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.invalid_cart');
		// get cart limit
        $cart_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'cart_product_limit'");
        $cart_setting_result = $this->_db->my_fetch_object($cart_query);
        $cart_product_limit = (!empty($cart_setting_result)) ? $cart_setting_result->value : 0;
        
        $_response['cart_product_limit'] = $cart_product_limit;
        if($cart_product_limit < $product_qty){
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.cart_limit').''.$cart_product_limit;
            return $_response;
        }
		
		// set product values in array
		$product_data[] = array('product_id'=>$product_id,'qty'=>$product_qty,'price'=>$product_price,'special_price'=>0);
		// get promo price for product if exist
		$product_price_data = $this->manage_product_price($product_data,$outlet_id);
		$promo_price  = !empty($product_price_data[$product_id]) ? $product_price_data[$product_id] : $product_qty*$product_price;
				
		if(!empty($user_id) && !empty($product_id) && !empty($outlet_id) && !empty($product_qty)) {
			// prepare cart values
			$cart_data = array();
			$cart_data['user_id']    = $user_id;
			$cart_data['outlet_id']  = $outlet_id;
			$cart_data['product_id'] = $product_id;
			$cart_data['product_qty'] = $product_qty;
			$cart_data['price'] = $product_price;
			$cart_data['total_price'] = $promo_price;
			
			// get user's active cart details
			$user_cart = $this->get_user_cart_data($user_id);
			
			if(!empty($user_cart) && isset($user_cart->cart_id)) {
				
				if(isset($user_cart->cart_id) && $cart_id != $user_cart->cart_id) {
					// return error msg if request coming from diffrent cart id
					$_response['result_code'] = 2;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cant_clone_cart');
					return $_response;
				}
				
				// check for mismatch outlets
				if($outlet_id != $user_cart->outlet_id) {
					// set response values
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cart_outlets');
					return $_response;
				}
				
				$cart_id = $user_cart->cart_id;
				// get cart item existing price
				$cart_item_data = $this->get_cart_item_data($product_id,$cart_id);
				if(isset($cart_item_data->cart_item_id) && !empty($cart_item_data->cart_item_id)) {
					
					/*if(!empty($user_cart->device_token) && $device_token != $user_cart->device_token) {
						// return error msg if request coming from diffrent device
						$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_device_request');
						return $_response;
					}*/
					
					// manage product total price
					$cart_total_amount = $user_cart->total_amount-$cart_item_data->total_price;
					$cart_total_amount = $cart_total_amount+$promo_price;
					// set modified datetime
					$modified_date = date('Y-m-d h:i:s');
					// update cart total amount data
					$update_array = array("total_amount" => $cart_total_amount,"modified_date" => $modified_date);
					$where_clause = array("cart_id" => $cart_id);
					$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");	
					
					// update cart item data
					$update_array = array("product_qty" => $product_qty,"price" => $product_price,"total_price" => $promo_price,"modified_date" => $modified_date);
					$where_clause = array("cart_item_id" => $cart_item_data->cart_item_id);
					$this->_db->UpdateAll($this->cart_items_table, $update_array, $where_clause, "");
					
					// set response values
					$_response['result_code'] = 1;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.success.updated_cart');
					// get cart details 
					$cart_data = $this->user_cart_data($user_id);
					$_response['cart_id']    = (isset($cart_data['cart_id'])) ? $cart_data['cart_id'] : '';
					$_response['outlet_id']  = (isset($cart_data['outlet_id'])) ? $cart_data['outlet_id'] : '';
					$_response['outlet_name']= (isset($cart_data['outlet_name'])) ? $cart_data['outlet_name'] : '';
					$_response['modified_date'] = $modified_date;
					$_response['cart_item_count'] = (isset($cart_data['cart_item_count'])) ? $cart_data['cart_item_count'] : '';
					$_response['cart_data'] = $this->get_cart_details($user_id);
				}
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.mismatch_cart');
			}
		}
		return $_response;
	}
	
	function manage_product_price($productArray = array(),$outlet_id = '') {
		
		$product_price_data = $productArray;
		
        
        /* Get All Product Id with Price */
        $productMinPrice = array();
        $productMinPromo = array();
        foreach($product_price_data as $productKey => $productVal){
              $productPriceArray[$productVal['product_id']] = $productVal['qty']*$productVal['price'];
              $productMinPrice[$productVal['product_id']] = 0;
              $productMinPromo[$productVal['product_id']] = '';
        }
        
        
		// prepare promo products filter array
		$product_promo_data = $this->promo_products($product_price_data,$outlet_id);
        
		// prepare promo pricing and grouping values
		$promo_product_array = $this->promo_pricing($product_promo_data);
		
        
        if(!empty($promo_product_array)){
        /* Sorting Start Array --- Amit Neema ------ */
		$productArray = array();
		$promoProductId = array();
		$promoProductPrice = array();
        foreach($promo_product_array as $keyParent => $valParent) {
        	foreach($valParent['products'] as $keyChild => $valChild) {
				$promoProductId[$keyParent][] = $valChild['product_id'];
				$promoProductPrice[$keyParent][$valChild['product_id']] = $valChild['special_price'];
			}
		}
        
        
        //print_r($promoProductPrice);
		//die;
        $promoTotalPrice = array();
        $finalPricing = array();
        $finalPromo = array();
        
        foreach($promo_product_array as $keyParent => $valParent) {
            $promoTotalPrice[$keyParent] = 0;
        	foreach($productPriceArray as $productId => $productPrice) {
				if(in_array($productId,$promoProductId[$keyParent])){
                   if($productPrice < $promoProductPrice[$keyParent][$productId]){
                        $promoTotalPrice[$keyParent] += $productPrice;
                        $finalPricing[$keyParent][$productId] = $productPrice;
                        $finalPromo[$keyParent][$productId] = '';
                    }else{
                            $promoTotalPrice[$keyParent] += $promoProductPrice[$keyParent][$productId];    
                            $finalPricing[$keyParent][$productId] = $promoProductPrice[$keyParent][$productId];
                            $finalPromo[$keyParent][$productId] = $keyParent;
                    }
                }else{
                    $promoTotalPrice[$keyParent] += $productPrice;
                    $finalPricing[$keyParent][$productId] = $productPrice;
                    $finalPromo[$keyParent][$productId] = '';
                }
			}
		}
        
        /* Take Short value from item list */
        
        
            foreach($finalPricing as $promokey => $productPric){
                foreach($productPric as $productId =>$productValP){
                        if($productMinPrice[$productId] < $productValP && !empty($productMinPrice[$productId])){
                             $productMinPrice[$productId] = $productMinPrice[$productId];
                        }else{
                            $productMinPrice[$productId] = $productValP;
                            $productMinPromo[$productId] = $promokey;
                        }
                        
                }
            }
            //print_r($productMinPromo);
        }else{
                        $productMinPrice = $productPriceArray;
                        $productMinPromo = $promokey;
        }
        
        
        return $productMinPrice;
		//die;
		
		
	}
	
	/**
	 * Function used to manage promo and products of promo
	 * @param null
	 * @return array
	 * @access private
	 * 
	 */
	 private function promo_products($product_price_data=array(),$outlet_id) {
		
		$product_promo_data = array(); // initialize promo products array
		if(!empty($product_price_data)) {
			foreach($product_price_data as $price_data) {
				// get promo results associated with product
				$product_promo_query = "select PRMP_PRODUCT,PRMP_PRICE_1,PRMP_PROM_CODE,PRMP_OFFER_GROUP,PRMH_PROMOTION_TYPE from PRMPTBL join PRMHTBL on PRMP_PROM_CODE = PRMH_PROM_CODE where GetDate() between PRMH_START and PRMH_END and PRMH_STATUS = 'Active' and PRMH_PROMOTION_TYPE in ('SELLING', 'MIXMATCH', 'OFFER') and (PRMH_OUTLET_ZONE in (select distinct CODE_KEY_ALP from CodeTbl where CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_NUM = $outlet_id ) and PRMP_PRODUCT in ( '".$price_data['product_id']."' ))";
				//$product_promo_query = "select PRMP_PRODUCT, PRMP_PROM_CODE,PRMP_OFFER_GROUP,PRMH_PROMOTION_TYPE from PRMPTBL join PRMHTBL on PRMP_PROM_CODE = PRMH_PROM_CODE where PRMH_PROMOTION_TYPE in ('SELLING', 'MIXMATCH', 'OFFER') and (PRMH_OUTLET_ZONE in (select distinct CODE_KEY_ALP from CodeTbl where CODE_KEY_TYPE = 'ZONEOUTLET' ) and PRMP_PRODUCT in ( '".$price_data['product_id']."' ))"; // dummy query
				$product_promo_results = $this->_mssql->my_mssql_query($product_promo_query,$this->ms_con);
				if($this->_mssql->pdo_num_rows_query($product_promo_query,$this->ms_con) > 0 ) {
					
					while($product_promo_result = $this->_mssql->mssql_fetch_object_query($product_promo_results)) {
                        if($product_promo_result->PRMH_PROMOTION_TYPE == 'SELLING'){
                              
                                $promo_array['CODE_ALP_1'] = 0; // total quantity / break quantity 1
                                $promo_array['CODE_ALP_2'] = 0; // total price / break price 1
                                $promo_array['CODE_ALP_3'] = ''; // min required in group 1 
                                $promo_array['CODE_ALP_4'] = ''; // min required in group 2 
                                $promo_array['CODE_ALP_5'] = ''; // min required in group 3 / break quantity 2
                                $promo_array['CODE_ALP_6'] = ''; // item price at group 1 / break price 2
                                $promo_array['CODE_ALP_7'] = ''; // item price at group 2
                                $promo_array['CODE_ALP_8'] = ''; // item price at group 3
                                $promo_array['CODE_ALP_9'] = 'N'; // bundle product check value
                                $promo_array['promo_type'] = $product_promo_result->PRMH_PROMOTION_TYPE; // promo type
                                $promo_array['promo_offer_group'] =$product_promo_result->PRMP_OFFER_GROUP;
                                // prepare product promo values
                                $product_promo_array['product_id']    = $product_promo_result->PRMP_PRODUCT;
                                $product_promo_array['product_qty']   = $price_data['qty'];
                                $product_promo_array['product_base_price'] = $product_promo_result->PRMP_PRICE_1;
                                $product_promo_array['product_offer_group'] = $product_promo_result->PRMP_OFFER_GROUP;
                                
                                // append promo data
                                $product_promo_data[$product_promo_result->PRMP_PROM_CODE]['promo_data']  = $promo_array;
                                // append promo product data
                                $product_promo_data[$product_promo_result->PRMP_PROM_CODE]['products'][]  = $product_promo_array;
                        
                        }else{
                            // get promo's group count
                            $promo_group_query = "select distinct(PRMP_OFFER_GROUP) from PrmpTbl where PRMP_PROM_CODE = '".$product_promo_result->PRMP_PROM_CODE."' and PRMP_OFFER_GROUP != ''";
                            $promo_group_results = $this->_mssql->my_mssql_query($promo_group_query,$this->ms_con);
                            $promo_offer_group = array();
                            if( $this->_mssql->pdo_num_rows_query($promo_group_query,$this->ms_con) > 0 ) {
                                while($group_results = $this->_mssql->mssql_fetch_object_query($promo_group_results)) {
                                    $promo_offer_group[] = $group_results->PRMP_OFFER_GROUP;
                                }
                            }
                            
                            // get promo price and quantity results
                            $promo_query = "select top 1 CODE_ALP_1, CODE_ALP_2, CODE_ALP_3, CODE_ALP_4, CODE_ALP_5, CODE_ALP_6, CODE_ALP_7, CODE_ALP_8, CODE_ALP_9 from CodeTbl join PrmpTbl on CODE_KEY_ALP = PRMP_PROM_CODE where PRMP_PROM_CODE = '".$product_promo_result->PRMP_PROM_CODE."'";
                            $promo_results = $this->_mssql->my_mssql_query($promo_query,$this->ms_con);
                            if( $this->_mssql->pdo_num_rows_query($promo_query,$this->ms_con) > 0 ) {
                                // set promo result
                                $promo_result = $this->_mssql->mssql_fetch_object_query($promo_results);
                                // prepare promo values
                                $promo_array['CODE_ALP_1'] = $promo_result->CODE_ALP_1; // total quantity / break quantity 1
                                $promo_array['CODE_ALP_2'] = $promo_result->CODE_ALP_2; // total price / break price 1
                                $promo_array['CODE_ALP_3'] = $promo_result->CODE_ALP_3; // min required in group 1 
                                $promo_array['CODE_ALP_4'] = $promo_result->CODE_ALP_4; // min required in group 2 
                                $promo_array['CODE_ALP_5'] = $promo_result->CODE_ALP_5; // min required in group 3 / break quantity 2
                                $promo_array['CODE_ALP_6'] = $promo_result->CODE_ALP_6; // item price at group 1 / break price 2
                                $promo_array['CODE_ALP_7'] = $promo_result->CODE_ALP_7; // item price at group 2
                                $promo_array['CODE_ALP_8'] = $promo_result->CODE_ALP_8; // item price at group 3
                                $promo_array['CODE_ALP_9'] = $promo_result->CODE_ALP_9; // bundle product check value
                                $promo_array['promo_type'] = $product_promo_result->PRMH_PROMOTION_TYPE; // promo type
                                $promo_array['promo_offer_group'] = $promo_offer_group;
                                
                                // prepare product promo values
                                $product_promo_array['product_id']    = $price_data['product_id'];
                                $product_promo_array['product_qty']   = $price_data['qty'];
                                $product_promo_array['product_base_price'] = $price_data['price'];
                                $product_promo_array['product_offer_group'] = $product_promo_result->PRMP_OFFER_GROUP;
                                
                                // append promo data
                                $product_promo_data[$product_promo_result->PRMP_PROM_CODE]['promo_data']  = $promo_array;
                                // append promo product data
                                $product_promo_data[$product_promo_result->PRMP_PROM_CODE]['products'][]  = $product_promo_array;
                            }
                        }
					}
				}
			}
		}
		
        return $product_promo_data;
	 }
	 
	/**
	 * Function used to manage promo offer's special price with all condition
	 * @param null
	 * @return array
	 * @access private
	 * 
	 */
	 private function promo_pricing($product_promo_data=array()) {
       
		$promo_product_array = array(); // initialize promo products array
		if(!empty($product_promo_data)) {
			// manage pricing for promos
			foreach($product_promo_data as $key=>$promo_data) {
				$total_product_count = 0;
				$promo_product_data = $promo_data['products'];
				// set products total quantities
                $productQA = array();
                $productPricing = array();
                $product_single_array = array();
                foreach($promo_product_data as $promo_product) {
					$total_product_count = $total_product_count+$promo_product['product_qty'];
                    for($i = 0;$i < $promo_product['product_qty'];$i++){
                        $product_single_array[] = $promo_product['product_id'];
                    }
                    $productQA[$promo_product['product_id']] = $promo_product['product_qty'];
                    $productPricing[$promo_product['product_id']] = 0;
                    $productBasePricing[$promo_product['product_id']] = $promo_product['product_base_price'];                    
                }
						
				// prepare promo values
				$promo_array['CODE_ALP_1']   = trim($promo_data['promo_data']['CODE_ALP_1']); // total quantity / break quantity 1
				$promo_array['CODE_ALP_2']   = trim($promo_data['promo_data']['CODE_ALP_2']); // total price / break price 1
				$promo_array['CODE_ALP_3']   = trim($promo_data['promo_data']['CODE_ALP_3']); // min required in group 1
				$promo_array['CODE_ALP_4']   = trim($promo_data['promo_data']['CODE_ALP_4']); // min required in group 2 
				$promo_array['CODE_ALP_5']   = trim($promo_data['promo_data']['CODE_ALP_5']); // min required in group 3 / break quantity 2
				$CODE_ALP_6 = $promo_array['CODE_ALP_6']   = trim($promo_data['promo_data']['CODE_ALP_6']); // item price at group 1 / break price 2
				$CODE_ALP_7 = $promo_array['CODE_ALP_7']   = trim($promo_data['promo_data']['CODE_ALP_7']); // item price at group 2
				$CODE_ALP_8 = $promo_array['CODE_ALP_8']   = trim($promo_data['promo_data']['CODE_ALP_8']); // item price at group 3
				$promo_array['CODE_ALP_9']   = trim($promo_data['promo_data']['CODE_ALP_9']); // bundle product check value
				$promo_array['promo_type']   = trim($promo_data['promo_data']['promo_type']); // promo type
				$promo_offer_group = $promo_array['promo_offer_group'] = $promo_data['promo_data']['promo_offer_group'];
				$promo_array['total_product_count'] = $total_product_count;
				
				// set promo values
				$break_qty1		 = $promo_array['CODE_ALP_1'];
				$break_discount1 = substr($promo_array['CODE_ALP_2'], 1);
				// set promo price
				$promo_price = $break_discount1/$break_qty1; // Use as break point one and bundle case */
				// initializing required arrays
				$product_array = array();
				$valid_promo_products = array();
				$product_group_id = array();
                
                /* Changes in function with respect to current flow */
              
                
                if($promo_array['promo_type'] == 'MIXMATCH'){
                 
                   if($promo_array['CODE_ALP_9'] == 'Y') {
                    foreach($promo_product_data as $promo_product) {
                        $special_price = 0;
                        $product_qty = $promo_product['product_qty']; // set product quantity
                        $product_price = $promo_product['product_base_price']*$product_qty; // set product's total price
                        $product_offer_group = $promo_product['product_offer_group']; // set product offer group
                        $special_price = $promo_price*$product_qty; // set product's promo/special price
                        // prepare product promo values
                        $product_promo_array['product_id']    = $promo_product['product_id'];
                        $product_promo_array['product_qty']   = $product_qty;
                        $product_promo_array['product_base_price'] = $promo_product['product_base_price'];
                        $product_promo_array['product_total_price'] = $product_price;
                        $product_promo_array['special_price'] = $special_price;
                        $product_promo_array['product_disc_amount'] = (!empty($special_price)) ? $product_price-$special_price : 0;
                        $product_promo_array['product_offer_group'] = $promo_product['product_offer_group'];
                        // append promo product data
                        $product_array[]  = $product_promo_array;
                        $product_group_id[] = $product_promo_array['product_offer_group'];  
                    }
                }else{
                    // set promo values
                    $break_qty1		 = $promo_array['CODE_ALP_1'];
                    $break_discount1 = substr($promo_array['CODE_ALP_2'], 1);
                    $promo_price1 = $break_discount1/$break_qty1; // Use as break point one and bundle case */
                    $break_qty2		 = $promo_array['CODE_ALP_5'];
                    $break_discount2 = substr($promo_array['CODE_ALP_6'], 1);
                    $promo_price2 = $break_discount2/$break_qty2; // Use as break point two and bundle case */
                    $special_price2 = 0;
                       
                       
                       if(($total_product_count > $break_qty2) && !empty($break_qty2)){
                            /* Calculate pricing with respect to product count */
                            $productRemain2 = $total_product_count%$break_qty2;
                            $productDeduct2 = $total_product_count/$break_qty2;
                            $productDeduct2 = intval($productDeduct2);         
                            if($productDeduct2 > 0){
                                $special_price2 = $promo_price2*$productDeduct2; // set product's promo/special price
                            }
                            
                            $removeProductQ = $productDeduct2*$break_qty2;
                            /* To check existing product count deduction */
                          
                            foreach($product_single_array as $productQK) {
                                if($removeProductQ >= 1 && $productQA[$productQK] > 0){
                                    $productQA[$productQK] =$productQA[$productQK] - 1;
                                    $removeProductQ = $removeProductQ - 1;
                                    $productPricing[$productQK] = $productPricing[$productQK] + 1*$promo_price2;
                                    
                                }
                            }
                        }
                        else{ // When Break point is bigger
                            $productRemain2 = $total_product_count;
                        }
                        
                      $special_price1 = 0;
                      if(($productRemain2 > $break_qty1) && !empty($break_qty1)){
                            /* Remaining product should check it break 1 */
                            $productRemain1 = $productRemain2%$break_qty1;
                            $productDeduct1 = $productRemain2/$break_qty1;
                            $productDeduct1 = intval($productDeduct1);
                            if($productDeduct1 > 0){
                                $special_price1 = $promo_price1*$productDeduct1; // set product's promo/special price
                            }
                            $removeProductQ = $productDeduct1*$break_qty1;
							
                            /* To check existing product count deduction */
                            foreach($product_single_array as $productQK){
                                if($removeProductQ >= 1 && $productQA[$productQK] > 0 && $productPricing[$productQK] > 0){
                                   $productQA[$productQK] = $productQA[$productQK] - 1;
                                   $removeProductQ = $removeProductQ - 1;
                                   $productPricing[$productQK] = $productPricing[$productQK] + 1*$promo_price1;
                                }
                            }
                        }else{
                            $productRemain1 = $productRemain2;
                        }
                           
                        $special_price_remain1 = 0;
                        if($productRemain1 > 0){
                        /* Product Pricing with respect to item */
                            foreach($productQA as $productQK => $productQV){
                                
                                if($productRemain1 >= 1 && $productQA[$productQK] > 0){
                                   $productQA[$productQK] = 0;
                                   $productRemain1 = $productRemain1 - $productQA[$productQK];
                                   $productPricing[$productQK] = $productPricing[$productQK] + $productQV*$productBasePricing[$productQK];
                                   $special_price_remain1 += $productQA[$productQK]*$productQV*$productBasePricing[$productQK];  
                                }
                            }                  
                        }
						
						$special_price = 0;
						foreach($promo_product_data as $promo_product) {
                            $special_price = 0;
                            $product_qty = $promo_product['product_qty']; // set product quantity
                            $product_price = $promo_product['product_base_price']*$product_qty; // set product's total price
                            $product_offer_group = $promo_product['product_offer_group']; // set product offer group
							// $special_price = $promo_price*$product_qty; // set product's promo/special price
                            // prepare product promo values
                            $product_promo_array['product_id']    = $promo_product['product_id'];
                            $product_promo_array['product_qty']   = $product_qty;
                            $product_promo_array['product_base_price'] = $promo_product['product_base_price'];
                            $product_promo_array['product_total_price'] = $product_price;
                            $product_promo_array['special_price'] = $productPricing[$promo_product['product_id']];
                            $product_promo_array['product_disc_amount'] = (!empty($special_price)) ? $product_price-$special_price : 0;
                            $product_promo_array['product_offer_group'] = $promo_product['product_offer_group'];
                            // append promo product data
                            $product_array[]  = $product_promo_array;
                            $product_group_id[] = $product_promo_array['product_offer_group'];  
                        }
					}
					
					// check products in promo group or not
                    if(!empty(count($promo_array['promo_offer_group'])) && count($promo_array['promo_offer_group']) > 0) {
                        if(count($promo_array['promo_offer_group']) > count($promo_product_data)) {
                            $valid_promo_products[] = '0';
                        }
                        
                        foreach($promo_array['promo_offer_group'] as $groupkey=>$offer_group) {
                            if(!empty($offer_group[$groupkey]) && !in_array($offer_group[$groupkey],$product_group_id)) {
                                $valid_promo_products[] = '0';
                            } 
                        }
                    }
                        
                }
                else if($promo_array['promo_type'] == 'OFFER'){                    
                        $totalQuantity   = $promo_data['promo_data']['CODE_ALP_1']; // total quantity
                        $totalPrice   = substr($promo_data['promo_data']['CODE_ALP_2'],1); // total price 
                        $singleOfferPrice = $totalPrice/$totalQuantity; // Offer pricing for single unit
                        
                        
                        if(!empty($promo_offer_group)){
                            $groupmaxQ = array();
                            $groupmaxPrice = array();
                            foreach($promo_offer_group as $valG){
                                $groupmaxPrice[$valG] = 0;
                                if($valG == 1){ 
                                    $groupmaxQ[$valG] = $promo_array['CODE_ALP_3']; 
                                    $groupmaxPrice[$valG] = $promo_array['CODE_ALP_6']; 
                                }  
                                if($valG == 2){ 
                                    $groupmaxQ[$valG] = $promo_array['CODE_ALP_4']; 
                                    $groupmaxPrice[$valG] = $promo_array['CODE_ALP_7']; 
                                }  
                                if($valG == 3){ 
                                    $groupmaxQ[$valG] = $promo_array['CODE_ALP_5']; 
                                    $groupmaxPrice[$valG] = $promo_array['CODE_ALP_8']; 
                                }  
                            }
                   
                           $productGQ = array();
                            /* Use product count with respect to group */
                            $groupproductPrice = array();
                            foreach($promo_product_data as $promo_product){
                                $productGQ[$promo_product['product_offer_group']] = intval("'".$productGQ[$promo_product['product_offer_group']]."'") + intval($promo_product['product_qty']);
                                for($j=0;$j < $promo_product['product_qty'];$j++){
                                    $groupproductPrice[$promo_product['product_offer_group']][$promo_product['product_id']][] = 0;
                                }
                            }
                            
                            
                            $deductGroupCA = array();
                            $groupMaxFlag = 0;
                            /* Grou max quantity check */ 
                            foreach($groupmaxQ as $groupKey => $grouppVal){
                                if($grouppVal > $productGQ[$groupKey]){ // Maximum quantity condition check */
                                    $groupMaxFlag = 1;
                                }else{
                                    $deductGroupC = $productGQ[$groupKey]/$grouppVal;
                                    $deductGroupCA[$groupKey] = intval($deductGroupC);
                                }
                            } 
                            
                            
                            
                            if($groupMaxFlag > 0){ // Quantity not exist with respect to maximum count 
                                    $valid_promo_products[] = '0';
                            }else{   // Maximum quanity valid;
                                
                                /* Minimum product deduct from group */
                                $minGroupD = min($deductGroupCA);
                                    /* integrate pricing for products */ 
                                    foreach($groupmaxQ as $groupKey => $grouppVal){
                                        $deductionCount = $minGroupD*$grouppVal;
                                        $k = 0;
                                        foreach($groupproductPrice[$groupKey] as $grpKey => $grpVal){
                                            foreach($grpVal as $grpValK => $grpValV){
                                                if($deductionCount > $k){
                                                     if(!empty($groupmaxPrice[$groupKey])){
                                                        if($groupmaxPrice[$groupKey] == 'FREE'){
                                                            $groupproductPrice[$groupKey][$grpKey][$k] = 0;
                                                        }else if($groupmaxPrice[$groupKey] == 'NORMAL'){
                                                            $groupproductPrice[$groupKey][$grpKey][$k] = $productBasePricing[$grpKey];
                                                        }else{
                                                            $groupproductPrice[$groupKey][$grpKey][$k] = $groupmaxPrice[$groupKey];
                                                        }  
                                                     }else{
                                                        $groupproductPrice[$groupKey][$grpKey][$k] = $singleOfferPrice;    
                                                     }
                                                }else{
                                                    $groupproductPrice[$groupKey][$grpKey][$k] = $productBasePricing[$grpKey];
                                                }
                                                $k++;
                                            }
                                        }
                                    }
                                    
                                  foreach($promo_product_data as $promo_product) {
                                        $special_price = 0;
                                        $product_qty = $promo_product['product_qty']; // set product quantity
                                        $product_id    = $promo_product['product_id'];
                                        $product_price = $promo_product['product_base_price']*$product_qty; // set product's total price
                                        $product_offer_group = $promo_product['product_offer_group']; // set product offer group
                                        //$special_price = $promo_price*$product_qty; // set product's promo/special price
                                        $special_price = array_sum($groupproductPrice[$product_offer_group][$product_id]);
                                        // prepare product promo values
                                        $product_promo_array['product_id']    = $promo_product['product_id'];
                                        $product_promo_array['product_qty']   = $product_qty;
                                        $product_promo_array['product_base_price'] = $promo_product['product_base_price'];
                                        $product_promo_array['product_total_price'] = $product_price;
                                        $product_promo_array['special_price'] = $special_price;
                                        $product_promo_array['product_disc_amount'] = (!empty($special_price)) ? $product_price-$special_price : 0;
                                        $product_promo_array['product_offer_group'] = $promo_product['product_offer_group'];
                                        // append promo product data
                                        $product_array[]  = $product_promo_array;
                                        $product_group_id[] = $product_promo_array['product_offer_group'];  
                                    }
                                    if(empty($product_array)){
                                        $valid_promo_products[] = '0';
                                    }
                            }  
                        }
                        
                    }
                    else if($promo_array['promo_type'] == 'SELLING'){
                        
                        foreach($promo_product_data as $promo_product) {
                            $special_price = 0;
                            $product_qty = $promo_product['product_qty']; // set product quantity
                            $product_price = $promo_product['product_base_price']*$product_qty; // set product's total price
                            $product_offer_group = $promo_product['product_offer_group']; // set product offer group
                            $special_price = $promo_product['product_base_price']*$product_qty; // set product's promo/special price
                            // prepare product promo values
                            $product_promo_array['product_id']    = $promo_product['product_id'];
                            $product_promo_array['product_qty']   = $product_qty;
                            $product_promo_array['product_base_price'] = $promo_product['product_base_price'];
                            $product_promo_array['product_total_price'] = $product_price;
                            $product_promo_array['special_price'] = $special_price;
                            $product_promo_array['product_disc_amount'] = (!empty($special_price)) ? $product_price-$special_price : 0;
                            $product_promo_array['product_offer_group'] = $promo_product['product_offer_group'];
                            // append promo product data
                            $product_array[]  = $product_promo_array;
                            $product_group_id[] = $product_promo_array['product_offer_group'];  
                        }
                    }
                   
                // manage promo products 
                if(!in_array('0',$valid_promo_products)) {
                    // append promo data
                    $promo_product_array[$key]['promo_data']  = $promo_array;
                    // append promo data
                    $promo_product_array[$key]['products']  = $product_array;
                }
            }
        }
        return $promo_product_array;
     }
     
     /**
	 * Function used to check user's cart is block or not
	 * @param int cart_id
	 * @return bool
	 * @access private
	 * 
	 */
	 private function is_cart_blocked($cart_id=0) {
		// current date
		$current_date = date('Y-m-d H:i:s');
		// set default response value
		$return_response = 1;
		// get user's cart details
		$sql = "SELECT is_blocked,blocked_at FROM " . $this->cart_table . " WHERE cart_id = '".$cart_id."'";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            if(!empty($row) && isset($row->blocked_at) && ($row->blocked_at >=$current_date) ) {
				$return_response = 1;
			}
        } 
		return $return_response; 
	 }
	
    /**
	 * Function used to get user's cart data
	 * @param null
	 * @return array
	 * @access private
	 * 
	 */
	 private function get_user_cart_data($user_id=0) {
		// get user's cart details
		$sql = "SELECT ct.cart_id,ct.outlet_id,ct.total_amount,ct.device_token,st.store_name as outlet_name FROM " . $this->cart_table . " ct  LEFT JOIN " . $this->stores_table . " st ON st.ms_store_id = ct.outlet_id WHERE ct.user_id = '".$user_id."' and ct.status=1";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row;
        } else {
            return FALSE;
        }
	 }
	 
	 /**
	 * Function used to get item's data in cart
	 * @param int product_id , int cart_id
	 * @return array
	 * @access private
	 * 
	 */
	 private function get_cart_item_data($product_id=0,$cart_id=0) {
		// get cart item details
		$sql = "SELECT cart_id,cart_item_id,product_qty,price,total_price FROM " . $this->cart_items_table . " WHERE cart_id = '".$cart_id."' and product_id = '".$product_id."' and is_deleted=0";
        $result = $this->_db->my_query($sql);
        if ($this->_db->my_num_rows($result) > 0) {
            $row = $this->_db->my_fetch_object($result);
            return $row;
        } else {
            return FALSE;
        }
	 }
	 
    /**
	 * Function used to insert cart data
	 * @param array cart_data
	 * @return int
	 * @access private
	 * 
	 */
	 private function insert_cart_data($cart_data='') {
		 $cart_id = 0;
		 $modified_date = date('Y-m-d H:i:s');
		 if(is_array($cart_data) && !empty($cart_data)) {
			$insert_sql = "INSERT INTO ".$this->cart_table."(user_id,outlet_id,total_amount,status,modified_date) VALUES ('".$cart_data['user_id']."','".$cart_data['outlet_id']."','".$cart_data['total_price']."',1,'$modified_date')";
			$this->_db->my_query($insert_sql);
			$cart_id = mysql_insert_id();
		 }
		 return $cart_id;
	 }
	 
	/**
	 * Function used to insert cart data
	 * @param array cart_data
	 * @return int
	 * @access private
	 * 
	 */
	 private function update_cart_data($cart_data='',$user_cart='') {
		
		 $return = false;
		 if(is_array($cart_data) && !empty($cart_data) && !empty($user_cart)) {
			// set cart total amount
			$total_amount = $cart_data['total_price']+$user_cart->total_amount;
			// update cart item data
			$update_array = array("total_amount" => $total_amount,"modified_date" => date('Y-m-d h:i:s'));
			$where_clause = array("cart_id" => $user_cart->cart_id);
			$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");	
			$return = true;
		 }
		 return $return;
	 }
	 
    /**
	 * Function used to manage cart item store procedure
	 * @param array cart_data
	 * @return int
	 * @access private
	 * 
	 */
	 private function manage_cart_items($cart_id=0,$cart_data='') {
		 
		 if(!empty($cart_id) && is_array($cart_data) && !empty($cart_data)) {
			// get cart item data if exists
			$sql = "SELECT cart_item_id,product_qty,total_price FROM " . $this->cart_items_table . " WHERE is_deleted = 0 and cart_id = '".$cart_id."' and product_id = '".$cart_data['product_id']."'";
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				if(!empty($row) && isset($row->cart_item_id)) {
					
					// set product cart values
					$product_qty = $row->product_qty+$cart_data['product_qty'];
					$total_price = $row->total_price+$cart_data['total_price'];
					
					// update cart item data
					$update_array = array("product_qty" => $product_qty,"price" => $cart_data['price'],"total_price" => $total_price,"modified_date" => date('Y-m-d h:i:s'));
					$where_clause = array("cart_item_id" => $row->cart_item_id);
					$this->_db->UpdateAll($this->cart_items_table, $update_array, $where_clause, "");
				}
			} else {
				// insert cart item data
				$insert_sql = "INSERT INTO ".$this->cart_items_table."(cart_id,product_id,product_name,product_description,product_qty,price,total_price) VALUES ('".$cart_id."','".$cart_data['product_id']."','".$cart_data['product_name']."','".$cart_data['product_description']."','".$cart_data['product_qty']."','".$cart_data['price']."','".$cart_data['total_price']."')";
				$this->_db->my_query($insert_sql);
				$cart_item_id = mysql_insert_id();
			}
		 }
		 return $cart_id;
	 }
	 
	/**
	 * Function used to get cart details
	 * @param array user_id
	 * @return array
	 * @access private
	 * 
	 */
	 private function get_cart_details($user_id=0) {
		 $cart_data = array();
		 if(!empty($user_id)) {
			//initialize cart total amount
			$total_amount = 0;
			// get cart data
			$cart_sql = "SELECT * FROM " . $this->cart_table . " WHERE status = 1 and user_id = ".$user_id;
			$cart_result = $this->_db->my_query($cart_sql);
			if ($this->_db->my_num_rows($cart_result) > 0) {
				$cart_row = $this->_db->my_fetch_object($cart_result);
				if(!empty($cart_row)) {
					$cart_data['cart_id']      = $cart_row->cart_id;
					$cart_data['outlet_id']    = $cart_row->outlet_id;
					$cart_data['user_id']      = $cart_row->user_id;
					$cart_data['notes']        = $cart_row->notes;
					$cart_data['pickup_date']  = $cart_row->pickup_date;
					$cart_data['device_token'] = $cart_row->device_token;
					$cart_data['modified_date']= $cart_row->modified_date;
					//$cart_data['total_amount'] = $cart_row->total_amount;
					$outlet_id = $cart_row->outlet_id;
					$cart_items = array();
					// get cart item data if exists
					$sql = "SELECT * FROM " . $this->cart_items_table . " WHERE is_deleted = 0 AND cart_id = ".$cart_row->cart_id;
					$result = $this->_db->my_query($sql);
					// total cart items
					$cart_item_count = $this->_db->my_num_rows($result);
					$cart_data['cart_item_count'] = $cart_item_count;
					$productA = array();
                    if ($cart_item_count > 0) {
						$productA = array();
                        while($row = $this->_db->my_fetch_object($result)) {
							// check existing values of product
							$prod_res = $this->check_product_status($row->product_id,$outlet_id); // check product's status
							$prod_price_a = ($prod_res != false) ? $prod_res : $row->price;
							
							$productA[] = array(
                            'product_id'=>$row->product_id,
                            'qty'=>$row->product_qty,
                            'price'=>$prod_price_a,
                            'special_price'=>0,
                            );
						}
                        
                        $productPriceA = $this->manage_product_price($productA,$outlet_id);
                        
                        $sql = "SELECT * FROM " . $this->cart_items_table . " WHERE is_deleted = 0 AND cart_id = ".$cart_row->cart_id;
                        $result = $this->_db->my_query($sql); 
                        while($row = $this->_db->my_fetch_object($result)) {
							
							$cart_item['cart_item_id'] = $row->cart_item_id;
							$cart_item['prod_desc']    = $row->product_name;
							$cart_item['prod_pos_desc']= $row->product_description;
							$cart_item['prod_number']  = $row->product_id;
							$cart_item['prod_cart_qty']= $row->product_qty;
							$cart_item['prod_image']   = IMG_URL."No_Image_Available.png";
							// check existing values of product
							$prod_resp = $this->check_product_status($row->product_id,$outlet_id); // check product's status
							$cart_item['prod_price'] = $row->price;
							if(!empty($productPriceA[$row->product_id])) {
								$prod_price = $productPriceA[$row->product_id]/$row->product_qty;
								$cart_item['prod_price'] = $prod_price;
							}
							$cart_item['is_deleted']   = ($prod_resp != false) ? 0 : 1;
							if($cart_item['is_deleted'] == 0) {
								$total_price = (!empty($productPriceA[$row->product_id])) ? $productPriceA[$row->product_id] : $row->product_qty*$row->price;
							}
							$cart_item['total_price']   = (isset($total_price) && !empty($total_price)) ? $total_price : 0;
							$cart_item['special_price'] = (isset($total_price) && !empty($total_price)) ? $total_price : 0;
							$cart_items[] = $cart_item;
							// add product total amount in cart total
							$total_amount = $total_amount+$cart_item['total_price'];
						}
					}
					$cart_data['total_amount'] = $total_amount;
					$cart_data['cart_items'] = $cart_items;
				}
			}
		 }
		 return $cart_data;
	 }
	 
	/**
	 * Function used to manage favourite product
	 * @param int customer_id , 
	 * @return array
	 * @access public
	 * 
	 */
	 public function fav_product() {
		// get post data
		$user_id     = $this->_common->test_input($_REQUEST['customer_id']);
		$product_id  = $this->_common->test_input($_REQUEST['product_id']);
		$is_fav 	 = $this->_common->test_input($_REQUEST['is_fav']);
		// set product favourite status
		$is_fav = (!empty($is_fav)) ? $is_fav : 0;
		$_response['result_code'] = 0;
		
		if(!empty($user_id) && !empty($product_id)) {
			// get favorite product data
			$fav_sql = "SELECT fav_id FROM " . $this->fav_products_table . " WHERE fav_product_id = '".$product_id."' and user_id = ".$user_id;
			$fav_result = $this->_db->my_query($fav_sql);
			$fav_product_row = $this->_db->my_fetch_object($fav_result);
			
			if(!empty($fav_product_row) && isset($fav_product_row->fav_id)) {
				if($is_fav == 1) {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.already_fav_product');
				} else {
					// remove favourite cart item data
					$remove_sql = "DELETE FROM ".$this->fav_products_table." WHERE fav_product_id = '".$product_id."' and user_id =".$user_id;
					$this->_db->my_query($remove_sql);
					$_response['result_code'] = 1;
				}
			} else {
				if($is_fav == 0) {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.select_fav_product_first');
				} else {
					// insert favourite product log
					$insert_sql = "INSERT INTO ".$this->fav_products_table."(fav_product_id,user_id) VALUES ('".$product_id."','".$user_id."')";
					$this->_db->my_query($insert_sql);
					$_response['result_code'] = 1;
				}
			}
			$_response['fav_products'] = $this->my_fav_product_ids($user_id); // user's favorite products
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.fav_product_details');
		}
		return $_response;
	 }
	 
	/**
	 * Function used to get user's favorite product list
	 * @param int customer_id , int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function fav_user_products() {
		// get post data
		$user_id   = $this->_common->test_input($_REQUEST['customer_id']);
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$department_id = $this->_common->test_input($_REQUEST['department_id']);
		// set default response as false
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_fav_products');
		if(!empty($user_id) && !empty($outlet_id)) {
			
			// get favorite product data
			$fav_sql = "SELECT * FROM " . $this->fav_products_table . " WHERE user_id = ".$user_id;
			$fav_result = $this->_db->my_query($fav_sql);
			if ($this->_db->my_num_rows($fav_result) > 0) {
				// set total product count
				$total_product_count = $this->_db->my_num_rows($fav_result);
				$products = array();
				while($row = $this->_db->my_fetch_object($fav_result)) {
					// get product data
					$product_query = "SELECT Prod_Number, Prod_Desc,PROD_POS_DESC, OUTP_NORM_PRICE_1 from ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number  where  OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Number = '".$row->fav_product_id."' ";
					//$product_query = "SELECT Prod_Number, Prod_Desc,PROD_POS_DESC, OUTP_NORM_PRICE_1 from ".$this->PRODTBL." JOIN ".$this->OUTPTBL." on Outp_Product = Prod_Number where OUTP_OUTLET = $outlet_id and Prod_Number = '".$row->fav_product_id."'";
					$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->ms_con);
					$total_products = $this->_mssql->pdo_num_rows_query($product_query,$this->ms_con);
					if( $total_products > 0 ) {
						
						while( $result = $this->_mssql->mssql_fetch_object_query($get_product_data) ) {
							// set product values
							$array['prod_desc'] 	= (isset($result->Prod_Desc)) ? utf8_encode($result->Prod_Desc) : "";
							$array['prod_pos_desc'] = (isset($result->PROD_POS_DESC)) ? trim($result->PROD_POS_DESC) : "";
							$array['prod_number']	= (isset($result->Prod_Number)) ? utf8_encode($result->Prod_Number) : "";
							$array['prod_price']	= (isset($result->OUTP_NORM_PRICE_1)) ? trim($result->OUTP_NORM_PRICE_1) : "";
							$array['prod_cart_qty'] = 1;
							$array['prod_image']	= IMG_URL."No_Image_Available.png";
							$products[] = $array;
						}
					}
				}
            
              // get cart limit
                $cart_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'cart_product_limit'");
                $cart_setting_result = $this->_db->my_fetch_object($cart_query);
                $cart_product_limit = (!empty($cart_setting_result)) ? $cart_setting_result->value : 0;
                // set response values
                $_response['result_code'] = 1;
                $_response['cart_product_limit'] = $cart_product_limit; // Product limit
                $_response['total_products'] = count($products);
				$_response['fav_products'] = $this->my_fav_product_ids($user_id); // user's favorite products
				$_response['result'] 	= $products;
				$_response['message'] = ( count($products) > 0 ) ? '' : $this->_common->langText($this->_locale,'txt.outlet.error.no_fav_products');	
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.fav_product_details');
		}
		
		return $_response;
	 }
	 
	/**
	 * Function used to remove cart item
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function remove_cart_item() {
		// get post data
		$user_id     = $this->_common->test_input($_REQUEST['customer_id']);
		$cart_id     = $this->_common->test_input($_REQUEST['cart_id']);
		$cart_item_id = $this->_common->test_input($_REQUEST['cart_item_id']);
		$device_token = $this->_common->test_input($_REQUEST['device_token']);
		
		$_response['result_code'] = 0;
		if(!empty($user_id) && !empty($cart_item_id) &&!empty($cart_id)) {
			
			// get user's cart details
			$cart_row = $this->user_cart_data($user_id);
			
			if(isset($cart_row['cart_id']) && $cart_id != $cart_row['cart_id']) {
				// return error msg if request coming from diffrent cart id
				$_response['result_code'] = 2;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cant_clone_cart');
				return $_response;
			}
			
			// set cart items count
			$cart_item_count = (isset($cart_row['cart_item_count']) && !empty($cart_row['cart_item_count'])) ? $cart_row['cart_item_count'] : "";
				
			if(!empty($cart_row) && isset($cart_row['cart_id']) && !empty($cart_item_count) && $cart_row['cart_id'] == $cart_id) {
				// get cart item data if exists
				$item_sql = "SELECT total_price FROM " . $this->cart_items_table . " WHERE is_deleted = 0 AND cart_item_id = ".$cart_item_id;
				$item_result = $this->_db->my_query($item_sql);
				$cart_item_row = $this->_db->my_fetch_object($item_result);
				if(!empty($cart_item_row) && isset($cart_item_row->total_price)) {
					// manage product total price
					$cart_total_amount = $cart_row['total_amount']-$cart_item_row->total_price;
					// set modified datetime
					$modified_date = date('Y-m-d h:i:s');
					// update cart item data
					$update_array = array("is_deleted" => 1,"modified_date" => $modified_date);
					$where_clause = array("cart_item_id" => $cart_item_id);
					$this->_db->UpdateAll($this->cart_items_table, $update_array, $where_clause, "");
					// update cart data
					$update_array = array("total_amount" => $cart_total_amount,"modified_date" => $modified_date);
                    if($cart_item_count == 1) {
                        $update_array['status'] = 0;					
                    }
					// update cart total amount data
					$where_clause = array("cart_id" => $cart_id);
					$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");	
					
					$_response['result_code'] = 1;
					$_response['cart_item_count'] = ($cart_item_count > 0) ? $cart_item_count-1 : 0; // set cart total items
					$_response['modified_date'] = $modified_date;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.success.cart_item_remove');
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.valid_cart_item');
				}
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_cart');
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cart_item_remove');
		}
		return $_response;
	 }
	
	/**
	 * Function used to get user's cart information
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function cart_info() {
		// get post data
		$user_id = $this->_common->test_input($_REQUEST['customer_id']);
        $_response['result_code'] = 0; // set default result code
        // check customer id exists or not
        if(empty($user_id)) {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.session');
			return $_response;
		}

        // get cart limit
        $cart_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'cart_product_limit'");
        $cart_setting_result = $this->_db->my_fetch_object($cart_query);
        $cart_product_limit = (!empty($cart_setting_result)) ? $cart_setting_result->value : 0;
        
		// user's cart information
		$cart_data  = $this->get_cart_details($user_id);
		if(is_array($cart_data) && count($cart_data) > 0) {
			$_response['result_code'] = 1;
            $_response['cart_product_limit'] = $cart_product_limit;
            
            //$_response['cart_count'] = count($cart_data);
			$_response['result'] = $cart_data;
		} else {
            $_response['cart_count'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.cart_empty');
		}
	
		return $_response;
	}
	
	/**
	 * Function used to destroy cart entries
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function destroy_cart() {
		
		// get post data
		$user_id  = $this->_common->test_input($_REQUEST['customer_id']);
		// check customer id exists or not
        if(empty($user_id)) {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.session');
			return $_response;
		}
		// update cart status as inactive
		$update_array = array("status" => 0,"modified_date" => date('Y-m-d h:i:s'));
		$where_clause = array("user_id" => $user_id);
		$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");	
		$_response['result_code'] = 1;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.success.empty_cart');
		return $_response;
	}
	
    /**
	 * Function used for favourite listing
	 * @param $userId - customer_id
	 * @return array
	 * @access public
	 * 
	 */
	public function get_favourites() {
		// get post data
		$user_id = $this->_common->test_input($_REQUEST['customer_id']);
		$_response['result_code'] = 0; // set default result code
        // check customer id exists or not
        if(empty($user_id)) {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.session');
			return $_response;
		}else{
        	$cart_data  = $this->user_cart_data($user_id);
        	
			$_response['result_code']= 1;
			$_response['outlet_id'] = (isset($cart_data['outlet_id'])) ? $cart_data['outlet_id'] : ''; // selected store
			$_response['outlet_name']= (isset($cart_data['outlet_name'])) ? $cart_data['outlet_name'] : '';
			$_response['cart_item_count']  = (isset($cart_data['cart_item_count'])) ? $cart_data['cart_item_count'] : '';
			$_response['fav_products'] = $this->my_fav_product_ids($user_id); // user's favorite products
		}
		return $_response;
	}
	
	/**
	 * Function used to manage ordering
	 * @param int cart_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function create_order() {
        /* To Read Payment response Form Bpoint */
        $response = $_REQUEST;
        $responseCode = $response['ResponseCode'];	
        $resultKey = $response['ResultKey'];	
       
        $responseText = $response['ResponseText'];	
        $trx_no = '';
        $cart_id = '';
        $_response = array();
        
        
		if($responseCode == 0){
            include_once('../bpoint/BPOINT.php');              
            BPOINT\URLDirectory::setBaseURL("reserved","https://www.bpoint.com.au/webapi/v2");	
            $credentials = new BPOINT\Credentials("nightowlapi", "Ny0R13s1jV", "5353109496121214",BPOINT\Mode::Live);
            $txn = new BPOINT\ResultKeyRetrieval($resultKey);				
            $txn->setCredentials($credentials);
            $resultResponseKey = array();
            $response = $txn->submit();
            $response = (array) $response;
            
                foreach($response as $key => $val){
                    //echo '<b>KEY -----------------</b>'.print_r($key);
                    if(is_object($val)){
                        $valNew = (array) $val;
                        foreach($valNew as $k1 => $v1){
                            $k1 = trim($k1);
                            $k1 = strip_tags($k1);
                            $k1 = stripcslashes($k1);
                            $resultResponseKey[$k1] = $v1;    
                        }
                        
                    }else if(is_array($val)){
                        foreach($val as $k1 => $v1){
                            $k1 = trim($k1);
                            $k1 = strip_tags($k1);
                            $k1 = stripcslashes($k1);
                            $resultResponseKey[$k1] = $v1; 
                        }
                    }else{
                        $key = trim($key);
                        $key = strip_tags($key);
                        $key = stripcslashes($key); 
                        $resultResponseKey[$key] = $val;
                    }
                }
               
    
               
               $trxresponseCode  = $resultResponseKey['BPOINTTransactionResponseresponseCode'];
               $trxresponsetext  = $resultResponseKey['BPOINTTransactionResponseresponseText'];
				
				if($trxresponseCode == 0){
					$cart_id = $resultResponseKey['BPOINTTransactionResponsecrn1'];
					$trx_no  = $resultResponseKey['BPOINTTransactionResponsetxnNumber'];
                    $cart_id = intval($cart_id);
                    $trx_no = intval($trx_no); 
               }else{
                    $cart_id = 0;
                    $trx_no = 0; 
                   // return $_response;
               }             
        }else{
			$cart_id = 0;
			$trx_no = 0; 
			//return $_response;
        }
        
		//$filename =  BASEPATH.'/log.txt'; 
		//$text = $trxresponseCode.'=='.$cart_id.'=='.$trx_no;
		//file_put_contents($filename, $text);
        
		
        // set default response as false
		$_response['result_code'] = 0;
		
		if(!empty($cart_id) && !empty($trx_no)) {
			// get response code
			$response_code = $trxresponseCode;
			$response_msg = $responseText;
			if($response_code == 0 && $trxresponseCode == '0') {
				// get cart data
				$cart_sql = "SELECT * FROM " . $this->cart_table . " WHERE cart_id = ".$cart_id;
				$cart_result = $this->_db->my_query($cart_sql);
				if ($this->_db->my_num_rows($cart_result) > 0) {
					$cart_row = $this->_db->my_fetch_object($cart_result);
					if(!empty($cart_row)) {
						
						// set order cart param
						$outlet_id = $cart_row->outlet_id;
						$user_id   = $cart_row->user_id;
						$total_amount = $cart_row->total_amount;
						$ord_notes = (isset($cart_row->notes)) ? $cart_row->notes : '';
						$trx_no    = (isset($trx_no)) ? $trx_no : '123';
						$ord_type  = 'SALE';
						$ord_trx_gst = 0;
						$ord_status = 1; // 1: Sent , 2:In-Progress, 3: Pickup
						$ord_trx_info = '';
                        $ord_post_date = date('M d Y');
						$ord_post_date_time = date('M d Y h:i:s A');
						$ord_pickup_date = (isset($cart_row->pickup_date)) ? date('M d Y h:i:s A',strtotime($cart_row->pickup_date)) : '';
                        
						$cart_data['cart_id']      = $cart_row->cart_id;
						$cart_data['user_id']      = $cart_row->user_id;
						$cart_data['total_amount'] = $cart_row->total_amount;
						$cart_data['notes']        = $ord_notes;
						
						$cart_items = array();
						// insert order information
						$insert_ord_sql = "INSERT ".$this->app_order_table." (APPORD_OUTLET,APPORD_USER_ID,APPORD_TRX_NO,APPORD_TYPE,APPORD_TRX_AMT,APPORD_TRX_GST,APPORD_TRX_INFO,APPORD_STATUS,APPORD_DATE,APPORD_MODIFIED_DATE,APPORD_NOTES,APPORD_PICKUP_DATE ) VALUES ('$outlet_id','$user_id','$trx_no','$ord_type','$total_amount','$ord_trx_gst','$ord_trx_info','$ord_status','$ord_post_date_time','$ord_post_date_time','$ord_notes','$ord_pickup_date')";
						$this->_mssql->my_mssql_query($insert_ord_sql,$this->ms_con);
						
                        $order_id = 0;
                        $sql = "select APPORD_ORD_ID from ".$this->app_order_table." where APPORD_ORD_ID =(select max( APPORD_ORD_ID ) from APP_ORDERS )";
                        $query = $this->_mssql->my_mssql_query($sql,$this->ms_con);
                        if($this->_mssql->pdo_num_rows_query($sql,$this->ms_con) > 0 ){
                            $row = $this->_mssql->mssql_fetch_object_query($query);
                            $order_id = (isset($row->APPORD_ORD_ID) && !empty($row->APPORD_ORD_ID)) ? $row->APPORD_ORD_ID : 0;
                        }
                       
                        if($order_id > 0){
							// get cart item data if exists
                            $sql = "SELECT * FROM " . $this->cart_items_table . " WHERE is_deleted = 0 AND cart_id = ".$cart_row->cart_id;
                            $result = $this->_db->my_query($sql);
                            // total cart items
                            $cart_item_count = $this->_db->my_num_rows($result);
                            $cart_data['cart_item_count'] = $cart_item_count;
                            if ($cart_item_count > 0) {
								while($row = $this->_db->my_fetch_object($result)) {
									
                                    $order_id = $order_id;
                                    $product_id = $row->product_id;
                                    $product_name = $row->product_name;
                                    $product_description = $row->product_description;
                                    $product_qty = $row->product_qty;
                                    $price = $row->price;
                                    $total_price = $row->total_price;
                                    $item_gst_amount = 0;
                                    $item_disc_amount = 0;
                                    /* insert data in item detail tabel */
                                    $insert_ord_item_sql = "INSERT ".$this->app_order_items_table." (APPORDITM_ORD_ID,APPORDITM_PRODUCT,APPORDITM_PRODUCT_DESC,APPORDITM_QTY,APPORDITM_BASE_AMT,APPORDITM_AMT,APPORDITM_DISC_AMT,APPORDITM_GST_AMT,APPORDITM_DATE,APPORDITM_PRODUCT_POS_DESC ) VALUES ('$order_id','$product_id','$product_name','$product_qty','$price','$total_price','$item_disc_amount','$item_gst_amount','$ord_post_date','$product_description')";
                                    $this->_mssql->my_mssql_query($insert_ord_item_sql,$this->ms_con);
                                   // $order_item_detail = $this->_mssql->mysql_insert_id();
                                }
								$_response['order_id'] = $order_id;
								$_response['result_code'] = 1;
								$_response['message'] = $this->_common->langText($this->_locale,'txt.success.order_place_success');
								
								// insert app store summary
								$insert_ord_summary_sql = "INSERT ".$this->app_store_order_table." (ASO_Date,ASO_Outlet,ASO_Account_Number,ASO_Amount) VALUES ('$ord_post_date_time','$outlet_id','$user_id','$total_amount')";
								$this->_mssql->my_mssql_query($insert_ord_summary_sql,$this->ms_con);
								
								// change cart status as inactive
								$update_array = array("status" => 0,"modified_date" => date('Y-m-d h:i:s'),"order_id"=>$order_id);
								$where_clause = array("user_id"=> $user_id);
								$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");	
                                
                                /* Push notification to supplier */
                                $orderDetail = array('order_name'=>'$order_id','order_description'=>'New Order Created');
                                $this->push_notification_supplier($outlet_id,$order_id,$orderDetail);
                               
                            }
                        }else{
                            $_response['order_id'] = 0;
                            $_response['message'] = $this->_common->langText($this->_locale,'txt.error.invalid_request');
                            $_response['result_code'] = 0;
                        }    
					}
				}
			} else {
				$_response['message'] = $response_msg;
                $_response['result_code'] = 0;
            }
		}else{
           	$_response['message'] = $this->_common->langText($this->_locale,'txt.error.invalid_request');
            $_response['result_code'] = 0;
            
        }
        die;
		//return $_response;
	}
    
    /**
	 * Function used to get user's existing orders
	 * @param int customer_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function existing_orders() {
		// get post data
		$user_id  = $this->_common->test_input($_REQUEST['customer_id']);
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
		// check customer id exists or not
        if(empty($user_id)) {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.session');
			return $_response;
		}
		// query for fetching total count
		$order_count_query = "SELECT count(APPORD_ORD_ID) as total_count FROM ".$this->app_order_table." where APPORD_USER_ID = $user_id";
		$order_count_data = $this->_mssql->my_mssql_query($order_count_query,$this->ms_con);	
		$order_count = $this->_mssql->mssql_fetch_object_query($order_count_data);
		$orders = array();
		// get order records data
		$order_query = "SELECT * FROM ( SELECT APPORD_ORD_ID,APPORD_TRX_NO,APPORD_DATE,APPORD_STATUS,APPORD_OUTLET,APPORD_TRX_AMT,OUTL_DESC, ROW_NUMBER() OVER (ORDER BY APPORD_MODIFIED_DATE DESC) as row FROM ".$this->app_order_table." LEFT JOIN OutlTbl on APPORD_OUTLET = OUTL_OUTLET where APPORD_USER_ID = $user_id) a WHERE row > ".$offset." and row <= ".$limit;
				
		$get_order_data = $this->_mssql->my_mssql_query($order_query,$this->ms_con);
		if( $this->_mssql->pdo_num_rows_query($order_query,$this->ms_con) > 0 ) {
			while( $result = $this->_mssql->mssql_fetch_object_query($get_order_data) ) {
				// set order values
				$array['order_id']  	= (isset($result->APPORD_ORD_ID)) ? trim($result->APPORD_ORD_ID) : "";
				$array['outlet_id']  	= (isset($result->APPORD_OUTLET)) ? trim($result->APPORD_OUTLET) : "";
				$array['outlet_name'] 	= (isset($result->OUTL_DESC)) ? utf8_encode($result->OUTL_DESC) : '';
				$array['order_date'] 	= (isset($result->APPORD_DATE)) ? date('d-m-Y',strtotime($result->APPORD_DATE)) : "";
				$array['order_amount']	= (isset($result->APPORD_TRX_AMT)) ? $result->APPORD_TRX_AMT : "";
				$array['order_trx_no']	= (isset($result->APPORD_TRX_NO)) ? $result->APPORD_TRX_NO : "";
				// set order status
				$order_status  = (isset($result->APPORD_STATUS)) ? trim($result->APPORD_STATUS) : "";
				if($order_status == 2) {
					$status = 'In Progress';
				} else if($order_status == 3) {
					$status = 'Pick Up';
				} else {
					$status = 'Sent';
				}
				$array['order_status_type']  = $order_status;
				$array['order_status']  = $status;
				// get order's item result
				$ord_item_query = "SELECT count(APPORDITM_ITEM_ID) as order_items FROM ".$this->app_order_items_table." WHERE APPORDITM_ORD_ID = '".$result->APPORD_ORD_ID."'";
				$ord_item_result = $this->_mssql->my_mssql_query($ord_item_query,$this->ms_con);
				
				if( $this->_mssql->pdo_num_rows_query($ord_item_query,$this->ms_con) > 0 ) {
					// set order item result
					$item_result = $this->_mssql->mssql_fetch_object_query($ord_item_result);
					$order_items_count 	  = (isset($item_result->order_items)) ? $item_result->order_items : "";
				}
				$array['order_items'] = (isset($order_items_count)) ? $order_items_count : 0;
				// append order data array if order items exists
				//if($array['order_items'] > 0) {
					$orders[] = $array;
				//}
			}
			$_response['result_code'] = 1;
			$_response['total_count'] = (isset($order_count->total_count)) ? $order_count->total_count : 0;
			$_response['results']     = $orders;
		} else {
           	$_response['message']	 = $this->_common->langText($this->_locale,'txt.no_record_found');
            $_response['result_code']= 0;
            
        }
		return $_response;
	}
	
	/**
	 * Function used to get order's details
	 * @param int cart_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function view_order() {
		// get post data
		$order_id  = $this->_common->test_input($_REQUEST['order_id']);
		if(!empty($order_id)) {
			// get order's item result
			$ord_item_query  = "SELECT * FROM ".$this->app_order_items_table." WHERE APPORDITM_ORD_ID = '".$order_id."'";
			$ord_item_result = $this->_mssql->my_mssql_query($ord_item_query,$this->ms_con);
			if( $this->_mssql->pdo_num_rows_query($ord_item_query,$this->ms_con) > 0 ) {
				$order_items = array();
				while( $result = $this->_mssql->mssql_fetch_object_query($ord_item_result) ) {
					// set order product values
					$item['prod_number']	 = (isset($result->APPORDITM_PRODUCT)) ? trim($result->APPORDITM_PRODUCT) : "";
					$item['prod_desc'] 		 = (isset($result->APPORDITM_PRODUCT_DESC)) ? trim($result->APPORDITM_PRODUCT_DESC) : "";
					$item['prod_pos_desc'] 	 = (isset($result->APPORDITM_PRODUCT_POS_DESC)) ? trim($result->APPORDITM_PRODUCT_POS_DESC) : "";
					$item['prod_qty'] 		 = (isset($result->APPORDITM_QTY)) ? trim($result->APPORDITM_QTY) : "";
					$item['prod_price']		 = (isset($result->APPORDITM_BASE_AMT)) ? number_format($result->APPORDITM_BASE_AMT, 2, '.', '') : "";
					$item['prod_total_price']= (isset($result->APPORDITM_AMT)) ? number_format($result->APPORDITM_AMT, 2, '.', '') : "";
					$item['prod_image']      = IMG_URL."No_Image_Available.png";
					$order_items[] = $item;
				}
				$_response['result_code'] = 1;
				$_response['result']     = $order_items;
			} else {
				$_response['result_code'] = 0;
				$_response['message']	  = $this->_common->langText($this->_locale,'txt.no_record_found');         
			}
		} else {
			$_response['result_code']= 0;
           	$_response['message']	 = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_order_id');         
        }
		return $_response;
	 }
    
   /**
	 * Function used to Send Push notification to supplier
	 * @param int $outlet_id
	 * @param int $order_id
	 * @return array
	 * @access public
	 */ 
    public function testpushcode(){
         
        $order_id   = $this->_common->test_input($_REQUEST['order_id']);
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
        $orderDetail = array('order_name'=>'Test Order','order_description'=>'Test Order Description');
        $this->push_notification_supplier($outlet_id,$order_id,$orderDetail);
    }
    
    public function push_notification_supplier($outlet_id = '',$order_id = '',$orderDetail = array()){
        if(!empty($outlet_id) && !empty($order_id)){
			$userIdArray = array();
			// get all active supplier type users
			$user_query = "select DISTINCT USER_NUMBER from USERTBL where USER_STATUS = 'Active' AND USER_OUTLET = ( SELECT OUTL_OUTLET FROM OutlTbl WHERE OUTL_OUTLET = '$outlet_id') OR USER_OUTLET in (SELECT CODE_KEY_NUM FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = USER_ZONE and CODE_KEY_NUM = '$outlet_id') OR ((USER_OUTLET = NULL OR USER_OUTLET = 0) AND (USER_ZONE = NULL OR USER_ZONE = ''))";
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->ms_con);
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->ms_con) > 0 ) { // User Ids with respect to outlets
				while($user_row = $this->_mssql->mssql_fetch_object_query($get_user_data)){
					$userIdArray[] = $user_row->USER_NUMBER;
				}
			}
            
			if(!empty($userIdArray)){
				$userIdArray = array_unique($userIdArray);
                    if(!empty($orderDetail)){
						// set order details in array
						$order_push_data['order_id'] = $order_id;
						$order_push_data['order_description'] = $orderDetail['order_description'];
						$order_push_data['push_type'] = 3;
                        // filter tab users from suppliers list        
                        $user_ids = implode("', '",$userIdArray);
                        $user_query = "select device_id,device_type from ".$this->supplier_device." where supplier_id IN ('".$user_ids."')";
                        $get_user_data = $this->_db->my_query($user_query,$this->con);
                        if( $this->_db->my_num_rows($get_user_data) > 0 ) {
                            $android_devices = array();
							while($user_row = $this->_db->my_fetch_array($get_user_data)){
								if(!empty($user_row)) {
									$android_devices[] = $user_row['device_id'];
								}	
							}
                            
                            if(!empty($android_devices) && count($android_devices) > 0) {                 
                                $this->_push->sendAndroidPushNotification_order_tablet($android_devices,$order_push_data);
                            }     
                        }
                    }                
					return true;
              }else{
                return false;
              }
        } else{
			return false;
        }
    }
    
    /**
	 * Function used to validate cart data
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function validate_cart() {
		
		// get post data
		$user_id     = $this->_common->test_input($_REQUEST['customer_id']);
		$cart_id     = $this->_common->test_input($_REQUEST['cart_id']);
		$notes    	 = $this->_common->test_input($_REQUEST['notes']);
		$pickup_date = $this->_common->test_input($_REQUEST['pickup_date']);
		$device_token = $this->_common->test_input($_REQUEST['device_token']);
		$modified_date = $this->_common->test_input($_REQUEST['modified_date']);
		$cart_total_amount = $this->_common->test_input($_REQUEST['total_amount']);
		
		$_response['result_code'] = 0;
		if(!empty($user_id) && !empty($cart_id)) {
			
			// get user's active cart details
			$user_cart = $this->get_user_cart_data($user_id);
			if(!empty($user_cart) && isset($user_cart->cart_id)) {
						
				// user's cart information
				$cart_data  = $this->get_cart_details($user_id);
				if(($cart_id != $cart_data['cart_id']) || ($modified_date != $cart_data['modified_date'])) {
					// return error msg if request coming from diffrent cart id
					$_response['result_code'] = 2;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cant_clone_cart');
					return $_response;
				}
				
				// compare cart total amount with user's existing cart amount
				if( (trim($cart_data['total_amount']) == trim($user_cart->total_amount)) &&  (trim($user_cart->total_amount) == trim($cart_total_amount)) ) {	
					// set pickup date
					$pickup_date = date('Y-m-d H:i:s',strtotime($pickup_date));
					// clone active cart
					$is_cloned = $this->clone_cart($cart_id,$notes,$pickup_date);
					
					if($is_cloned == 1) {
						
						// update cart additional details
						$update_array = array("status" => 0,"modified_date" => date('Y-m-d h:i:s'),"notes" => $notes,"pickup_date" => $pickup_date,"device_token" => $device_token);
						$where_clause = array("cart_id" => $cart_id);
						$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");
						// set response data in success
						$_response['result_code'] = 1;
						$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.success.valid_cart');
					} else {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.cant_clone_cart');
					}
				} else {
					// update db cart data
					$this->update_item_data($cart_data);
					$_response['result_code'] = 2;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.change_cart_data');
				}
			}  else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_cart');
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.invalid_cart');
		}
		
		return $_response;
	}
	
	/**
	 * Function used to create clone of active cart
	 * @param int cart_id
	 * @return int
	 * @access private
	 * 
	 */
	private function clone_cart($cart_id=0,$notes='',$pickup_date='') {
		// set default return status code
		$response_code = 0;
		// get cart data
		$cart_sql = "SELECT * FROM " . $this->cart_table . " WHERE cart_id = ".$cart_id;
		$cart_result = $this->_db->my_query($cart_sql);
		if ($this->_db->my_num_rows($cart_result) > 0) {
			$cart_row = $this->_db->my_fetch_object($cart_result);
			if(!empty($cart_row)) {
				
				// set cart param
				$outlet_id    = $cart_row->outlet_id;
				$user_id      = $cart_row->user_id;
				$total_amount = $cart_row->total_amount;
				$modified_date = $cart_row->modified_date;
				$notes        = (!empty($notes)) ? $notes : '';
				$pickup_date  = (isset($pickup_date)) ? $pickup_date : '';
				
				// insert cart data
				$insert_cart_sql = "INSERT INTO ".$this->cart_table."(user_id,outlet_id,total_amount,status,notes,pickup_date,modified_date) VALUES ('$user_id','$outlet_id','$total_amount',1,'$notes','$pickup_date','$modified_date')";
				$this->_db->my_query($insert_cart_sql);
				$clone_cart_id = mysql_insert_id();
				
				if($clone_cart_id > 0) {
					// get cart item data if exists
					$sql = "SELECT * FROM " . $this->cart_items_table . " WHERE is_deleted = 0 AND cart_id = ".$cart_row->cart_id;
					$result = $this->_db->my_query($sql);
					// total cart items
					$cart_item_count = $this->_db->my_num_rows($result);
					if ($cart_item_count > 0) {
						while($row = $this->_db->my_fetch_object($result)) {
							// set cart item values
							$product_id = $row->product_id;
							$product_name = mysql_real_escape_string($row->product_name);
							$product_description = mysql_real_escape_string($row->product_description);
							$product_qty = $row->product_qty;
							$price = $row->price;
							$total_price = $row->total_price;
						
							// insert cart item data
							$insert_sql = "INSERT INTO ".$this->cart_items_table."(cart_id,product_id,product_name,product_description,product_qty,price,total_price) VALUES ('$clone_cart_id','$product_id','".$product_name."','".$product_description."','$product_qty','$price','$total_price')";
							$this->_db->my_query($insert_sql);
							$cart_item_id = mysql_insert_id();
						}
						$response_code = 1;
					}
				}   
			}
		}
		return $response_code;
	}
	
	/**
	 * Function used to insert cart data
	 * @param array cart_data
	 * @return int
	 * @access private
	 * 
	 */
	 private function update_item_data($cart_data='') {
		
		 $return = false;
		 if(is_array($cart_data) && !empty($cart_data) && !empty($cart_data['cart_items'])) {
			// update cart total amount data
			$update_array = array("total_amount" => $cart_data['total_amount'],"modified_date" => date('Y-m-d h:i:s'));
			if($cart_data['total_amount'] == 0) {
				$update_array['status'] = 0; // inactive cart in case of 0 amount
			}
			$where_clause = array("cart_id" => $cart_data['cart_id']);
			$this->_db->UpdateAll($this->cart_table, $update_array, $where_clause, "");	
			// manage cart items updation
			$cart_items = $cart_data['cart_items'];
			foreach($cart_items as $item) {
				// update cart item data
				if(!empty($item['cart_item_id'])) {
					$update_array = array("product_qty" => $item['prod_cart_qty'],"price" => $item['prod_price'],"total_price" => $item['total_price'],"is_deleted" => $item['is_deleted'],"modified_date" => date('Y-m-d h:i:s'));
					$where_clause = array("cart_item_id" => $item['cart_item_id']);
					$this->_db->UpdateAll($this->cart_items_table, $update_array, $where_clause, "");	
				}
			}
			$return = true;
		 }
		 return $return;
	 }
    
    function test_price() {
		
		$outlet_id     = 792;
		$product_id    = 1932168;
		$product_price = 4.75;
		$product_qty   = 1;
		
		// set product values in array
		$product_data[] = array('product_id'=>$product_id,'qty'=>$product_qty,'price'=>$product_price,'special_price'=>0);
		// get promo price for product if exist
		$product_price_data = $this->manage_product_price($product_data,$outlet_id);
		$promo_price  = !empty($product_price_data[$product_id]) ? $product_price_data[$product_id] : $product_qty*$product_price;
		echo $promo_price;die;
	}
    
     public function test_push(){
 
		$androidPush = array();
		$deviceId = 'fbeuiNpYwaQ:APA91bGuiNmi5GuQ5nTlNnjx-ZeaXqHjPhvQEXVzAwliYp83pOOpLmibdkMNsesoJv1gyergrQndD00q7ZnH00p2iJ9UQyHmQEOQpFSfw37TGVYFcaWl0ilJXAjJR29VKfbXA2mv0lqu';
		$androidPush['order_id'] 	= '333';
		$androidPush['offer_name'] 	= '333';
		$androidPush['order_description'] ='Test First Description';
		$androidPush['push_type'] =3;
		$t = $this->_push->sendAndroidPushNotification_order_tablet(array($deviceId),$androidPush);
		echo '<pre>';
		print_r($t);
		die;
    }
    
    /**
	 * Function used to check order status from cart id
	 * @param int cart_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function check_order_status() {
		// get post data
		$cart_id  = $this->_common->test_input($_REQUEST['cart_id']);
		$_response['result_code'] = 3;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.no_order_placed');
		if(!empty($cart_id)) {
			// get order status of cart 
			$cart_sql = "SELECT order_id FROM ".$this->cart_table." WHERE cart_id = ".$cart_id;
			$cart_result = $this->_db->my_query($cart_sql);
			if ($this->_db->my_num_rows($cart_result) > 0) {
				$cart_row = $this->_db->my_fetch_object($cart_result);
				if(isset($cart_row->order_id) && !empty($cart_row->order_id)) {
					$_response['result_code'] = 1;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.success.order_created');
				}
			}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.no_cart_found');
		}
		return $_response;
	 }
   
    
} // End users class

?> 
