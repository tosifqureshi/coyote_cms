<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : Oct-05 03:09PM
 * Copyright : Coyote team
 *
 */
class reward {
	public $_response = array();
	public $result = array();
	private $reward_config_table = 'ava_reward_config';
	private $reward_transaction_table = 'ava_reward_transaction';
	private $reward_rule_table = 'ava_reward_rules';
	private $reward_rule_condition_table = 'ava_reward_rule_conditions';
	private $member_transaction_table = 'ava_cte_member_transaction_log';
	private $entity_master_table = 'ava_reward_entity_master';
	private $notification_tbl = 'ava_reward_notification';
    private $reward_catalogue = 'ava_reward_catalogue';
    private $reward_catalogue_products = 'ava_reward_catalogue_products';
    private $ava_stores_log = 'ava_stores_log';
    private $user_table = 'ava_users';
    private $notification_table = 'ava_reward_notification';
    private $users_contacts_tbl = 'ava_users_contacts';
    private $users_contacts_temp_tbl = 'ava_users_contacts_temp';
    private $reward_campaign_tbl = 'ava_reward_campaign';
    private $reward_redeem_log_tbl = 'ava_reward_redeem_log';
    private $reward_campaign_log = 'ava_reward_campaign_log';
    private $reward_cards = 'ava_reward_cards';
    private $edm_queue_log = 'ava_edm_queue_log';
    private $edm_view_log  = 'ava_edm_view_log';
    private $edm = 'ava_edm';
    private $PRODTBL = 'PRODTBL';
	private $kiosk_licence_table = 'ava_kiosk_licence_requests';


	/**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db 		= env::getInst(); // -- Create Database Connection instance
		$this->_mssql	= new mssql(); // Create instance of mssql class
		$this->con   	= $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
        $this->_common  = new commonclass(); // Create instance of commonclass
        $this->_locale  = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); // -- Create push notifications instance
        $this->_config  = config::getInst();
        $this->reward_img_path = IMG_URL.'reward_images/'; // -- Set image path
    }
    
    /**
	 * Function used to send birthday push and manage reward points in cron
	 * @param null
	 * @return array
	 * @access public
	 *  
	 */ 
	public function send_birthday_reward_points() {
		// get birthday config
		$sql = "SELECT birthday_points,birthday_notification FROM " . $this->reward_config_table . "";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
			$birthday_points = (!empty($row->birthday_points)) ? $row->birthday_points : 0;
			$birthday_notification = (!empty($row->birthday_notification)) ? $row->birthday_notification : 0;
			$members = array();
			// get app customers
			$user_query = "SELECT u.id,u.acc_number FROM ".$this->user_table." u WHERE u.active = 1 AND DATE_FORMAT(u.date_of_birth, '%m-%d') = '".date('m-d')."' AND ".date('Y')." NOT IN (SELECT YEAR(transaction_date) FROM ".$this->reward_transaction_table." WHERE transaction_type = 'birthday' and user_id = u.id)";
			$user_result = $this->_db->my_query($user_query);
			if ($this->_db->my_num_rows($user_result) > 0) {
				while($user_row = $this->_db->my_fetch_object($user_result)) {
					// set customer's params
					$user_id   = $user_row->id;
					//set user id in member array
					$members[] = $user_id;
				}
			}
			if($birthday_notification == 1) {
				// send push and insert reward log
				$this->handle_birthday_notification($members,$birthday_points);
			}
		}
		return true;
	}
	
	/**
	 * Function used to handle push and store procedure
	 * @param members(array),birthday_points(int),members_emails(array)
	 * @return void
	 * @access private
	 * 
	 */
     private function handle_birthday_notification($members,$birthday_points) {
		if(is_array($members) && count($members) > 0) {
			// push message
			$push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.birthday_note'),$birthday_points); 
			$reward_sql = '';
			$noti_where_sql = '';
			$datetime = date('Y-m-d H:i:s');
			// prepare sql multiple value string
			foreach($members as $key=>$val) {
				// prepare reward transaction where clause
				$reward_sql .= "('$val','$birthday_points','birthday',2,'$datetime')";
				$reward_sql .= (count($members) > 0 && count($members)-1 != $key) ? ',' : '';
				// prepare notification where clause
				$noti_where_sql .= "('".mysql_real_escape_string($push_message)."','birthday','$val','$datetime','$birthday_points')";
				$noti_where_sql .= (count($members) > 0 && count($members)-1 != $key) ? ',' : '';
			}
			// store members reward points
			if(!empty($reward_sql)) {
				$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date) VALUES $reward_sql";
				$this->_db->my_query($member_reward_sql);
				mysql_insert_id();
			}
			//  store notification records
			if(!empty($noti_where_sql)) {
				$notification_sql = "INSERT INTO ".$this->notification_table."(notification,notification_type,user_id,created_date,reward_point) VALUES $noti_where_sql";
				$this->_db->my_query($notification_sql);
				mysql_insert_id();
			}
		}
	}

    /*
     * @assign_order_purchase_points function is use to assign the reward points on purchase amount
     **/
    public function assign_order_purchase_points() {
        // get last reward point datetime for purchase type
		$sql = "SELECT transaction_date FROM ". $this->reward_transaction_table ." WHERE transaction_type = 'assign_order_purchase_points'";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
		}
        $transaction_date = (!empty($row->transaction_date)) ? date('Y-m-d H:i:s',strtotime($row->transaction_date)) : '';
		$current_datetime = date('Y-m-d H:i:s');
        $member_purchase = array();
		// set transaction start where clause
		$transaction_date_sql = (!empty($transaction_date)) ? "trx.transaction_datetime > '$transaction_date' AND" : "";
		// get member's purchase log
		$purchase_query = "SELECT SUM(trx.product_amount) as product_amount,u.id as user_id,u.email,u.device_token,u.device_type,card.reward_point_rate FROM ".$this->member_transaction_table." trx JOIN ".$this->user_table." u ON u.acc_number = trx.member_id JOIN ".$this->reward_cards." card ON card.id = u.reward_card_id WHERE $transaction_date_sql trx.transaction_datetime <= '$current_datetime' AND u.active = 1 AND u.deleted = 0 ORDER BY trx.member_id DESC";
        $purchase_result = $this->_db->my_query($purchase_query);
		if ($this->_db->my_num_rows($purchase_result) > 0) {
            // get data
			$purchase_row = $this->_db->my_fetch_object($purchase_result);
            $user_id = (!empty($purchase_row->user_id)) ? $purchase_row->user_id : 0;
            $email = (!empty($purchase_row->email)) ? $purchase_row->email : '';
            $product_amount = (!empty($purchase_row->product_amount)) ? $purchase_row->product_amount : 0;
            $device_token = (!empty($purchase_row->device_token)) ? $purchase_row->device_token : '';
            $device_type = (!empty($purchase_row->device_type)) ? $purchase_row->device_type : '';
            $reward_point_rate = (!empty($purchase_row->reward_point_rate)) ? $purchase_row->reward_point_rate : 0;
            
            //calculate reward point
            $user_reward_point = (($product_amount)*($reward_point_rate));
            // members reward points
			if(!empty($user_reward_point)) {
				$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date) VALUES ('".$user_id."','".$user_reward_point."','assign_order_purchase_points','1','".$current_datetime."')";
				//$this->_db->my_query($member_reward_sql);
				mysql_insert_id();
                
                $notification_sql = "INSERT INTO ".$this->notification_table."(notification,notification_type,user_id,created_date) VALUES ('Congrates! On purchasing from Nightowl app you have earned the ".$user_reward_point." reward points!. ','assign_order_purchase_points','".$user_id."','".$current_datetime."')";
				//$this->_db->my_query($notification_sql);
                
			}
		}
        return true;
    } //assign_order_purchase_points
    
    
    /**
	 * Function used to manage users purchase reward points in cron
	 * @param null
	 * @return void
	 * @access public
	 *  
	 */ 
    
    public function send_order_purchase_points() {
        // get last reward point datetime for purchase type
		$sql = "SELECT transaction_date FROM ". $this->reward_transaction_table ." WHERE transaction_type = 'make_purchase_points' order by transaction_date desc";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
		}
		$transaction_date = (!empty($row->transaction_date)) ? date('Y-m-d H:i:s',strtotime($row->transaction_date)) : '';
		$current_date = date('Y-m-d');
        // get members transaction data in between dates
		$member_purchase = $this->manage_member_transaction($transaction_date);
        // get reward rule details whos not expired
		$rule_query  = "SELECT id,rule_name,reward_points FROM ".$this->reward_rule_table." WHERE status = '1' AND is_deleted = '0' AND (start_date <= '".$current_date."' AND (end_date >= '".$current_date."' OR end_date = '')) ORDER BY created_at ASC" ;
		$rule_result = $this->_db->my_query($rule_query);
        if(!empty($member_purchase)) {
            if ($this->_db->my_num_rows($rule_result) > 0) {
                while($rule_row = $this->_db->my_fetch_object($rule_result)) {
                    // set rule params
                    $rule_id = (!empty($rule_row->id)) ? $rule_row->id : '';
                    $rule_name = (!empty($rule_row->rule_name)) ? $rule_row->rule_name : '';
                    $reward_points = (!empty($rule_row->reward_points)) ? $rule_row->reward_points : 0;
                    // get rule conditions
                    $condition_query = "SELECT * FROM ".$this->reward_rule_condition_table." WHERE rule_id = $rule_id ORDER BY created_at ASC";
                    $condition_result = $this->_db->my_query($condition_query);
                    if($this->_db->my_num_rows($condition_result) > 0) {
                        $condition_array = array();
                        $members_condition_array = $member_purchase;
                        $reward_member_array = array();
                        $codition_satisfied=1;
                        $total_conditions=1;
                        while($condition_row = $this->_db->my_fetch_object($condition_result)) {
                            // set condition params
                            $condition['condition_id'] = (!empty($condition_row->id)) ? $condition_row->id : '';
                            $condition['product_id']    = (!empty($condition_row->product_id)) ? $condition_row->product_id : '';
                            $condition['product_qty']  = (!empty($condition_row->product_qty)) ? $condition_row->product_qty : 0;
                            $condition_array[] = $condition;
                            // compare rule conditions for member purchase history
                            if(!empty($members_condition_array)) {
                                foreach($members_condition_array as $member_id=>$product_ids) {
                                    if(!empty($product_ids)) {
                                        foreach($product_ids as $key=>$val) {
                                            if(($condition['product_id'] == $val['product_id']) && $val['product_id'] > $condition['product_qty']){
                                               $reward_member_array['member_id'] = $member_id;
                                               $reward_member_array['acc_number'] = $val['acc_number'];
                                               $reward_member_array['codition_satisfied'] = $codition_satisfied;
                                               $reward_member_array['total_conditions'] = $total_conditions;
                                               $codition_satisfied++;
                                            }
                                        }
                                    }                            
                                }
                            }
                            $total_conditions++;
                        }
                        if(!empty($reward_member_array)) {
                            // insert reward data and send notification
                            $this->insert_reward_data($reward_member_array,$rule_id,$rule_name,$reward_points);
                        }
                    }
                }
            }
        }
        return true;
    } 
    
    	/**
	 * Function used to store users reward transaction data
	 * @param members_condition_array(array),rule_id(int),rule_name(string)
	 * @return void
	 * @access private
	 * 
	 */
     
    private function insert_reward_data($reward_member_array,$rule_id,$rule_name,$reward_points) {
        $member_id = (!empty($reward_member_array['member_id'])) ? $reward_member_array['member_id'] : 0;
        $codition_satisfied = (!empty($reward_member_array['codition_satisfied'])) ? $reward_member_array['codition_satisfied'] : 0;
        $total_conditions = (!empty($reward_member_array['total_conditions'])) ? $reward_member_array['total_conditions'] : 0;
        $acc_number = (!empty($reward_member_array['acc_number'])) ? $reward_member_array['acc_number'] : 0;
        if($total_conditions <= $codition_satisfied) {
            $comments = json_encode(array('rule_id'=>$rule_id,'rule_name'=>$rule_name,'acc_number'=>$acc_number));
            $member_reward_sql = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,transaction_date,status,comments) VALUES(
                        '".$member_id."',
                        '".$reward_points."',
                        'make_purchase_points',
                        '".date('y-m-d H:i:s')."',
                        '2',
                        '".$comments."')";
			$this->_db->my_query($member_reward_sql);

            // set notification message
            $push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.order_purchase_points_msg'),$reward_points);
            // store notification record
            $notification_data = ['user_id'=>$member_id,'notification'=>$push_message,'notification_type'=>'make_purchase_points','is_send'=>0];
            $this->_common->save_notification($notification_data);
        }
    }

    /**
	 * Function used to manage users transaction data
	 * @param transaction_date(string)
	 * @return array
	 * @access private
	 * 
	 */
     private function manage_member_transaction($transaction_date) {
		$current_datetime = date('Y-m-d H:i:s');
		$member_purchase = array();
		// set transaction start where clause
		$transaction_date_sql = (!empty($transaction_date)) ? "trx.transaction_datetime > '$transaction_date' AND" : "";
		// get member's purchase log
		$purchase_query = "SELECT trx.member_id,trx.product_id,trx.product_qty,trx.product_amount,trx.transaction_datetime,u.id as user_id,u.acc_number,u.device_token,u.device_type FROM ".$this->member_transaction_table." trx JOIN ".$this->user_table." u ON u.acc_number = trx.member_id WHERE $transaction_date_sql trx.transaction_datetime <= '$current_datetime' AND u.active = 1 AND u.deleted = 0 ORDER BY trx.member_id DESC";
		$purchase_result = $this->_db->my_query($purchase_query);
		if ($this->_db->my_num_rows($purchase_result) > 0) {
			$member_purchase = array();
			while($purchase_row = $this->_db->my_fetch_object($purchase_result)) {
                // collec the data
				$member_id = (!empty($purchase_row->user_id)) ? $purchase_row->user_id : 0;
				$product_id = (!empty($purchase_row->product_id)) ? $purchase_row->product_id : 0;
				$product_qty = (!empty($purchase_row->product_qty)) ? $purchase_row->product_qty : 0;
				$product_amount = (!empty($purchase_row->product_amount)) ? $purchase_row->product_amount : 0;
				$transaction_datetime = (!empty($purchase_row->transaction_datetime)) ? $purchase_row->transaction_datetime : 0;
				$acc_number = (!empty($purchase_row->acc_number)) ? $purchase_row->acc_number : 0;
				$device_token = (!empty($purchase_row->device_token)) ? $purchase_row->device_token : 0;
				$device_type = (!empty($purchase_row->device_type)) ? $purchase_row->device_type : 0;
                
                //preapare the array of member of product
                $member_purchase[$member_id][$product_id]['product_id'] = $product_id;
                $member_purchase[$member_id][$product_id]['product_qty'] = $product_qty;
                $member_purchase[$member_id][$product_id]['acc_number'] = $acc_number;
                $member_purchase[$member_id][$product_id]['product_amount'] = $product_amount;

			}
		}
		return $member_purchase;
	 }

	/**
	 * Function used to send reward push notification
	 * @param android_devices(array),ios_devices(array),push_message(string)
	 * @return bool
	 * @access private
	 * 
	 */
     private function send_push_notification($android_devices,$ios_devices,$push_message) {
		// set unique device ids array
		$android_devices = array_unique($android_devices);
		// reset device ids array
		$android_devices = $this->device_array_keys($android_devices);
		// send IOS push
		$this->_push->send_ios_reward_push($ios_devices,$push_message);
		// send andriod push 
		$this->_push->send_android_reward_push($android_devices,$push_message);
		return true;
	 }
    
    /**
     * @view_notification_list is list the all unread notifications to the users
     * @input user_id
     * @output array
     */
    public function view_notification_list(){
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $table = $this->notification_tbl; 
        $columns = '';
        $where = ['user_id'=>$user_id,'is_deleted'=>0];
        $orderby = 'created_date';
        $get_record = $this->_common->get_table_record($table,$columns,$where,$orderby);
        $result = array();
        if(!empty($get_record)) {
            foreach($get_record as $record) {
                //get notification type
                $notification_type = (!empty($record->notification_type)) ? $record->notification_type : 'reward_points';
                //get notification title
                $notification_title = $this->_common->get_reward_config($notification_type);
                //seting the title
                $record->notification_title = $notification_title;
                $record->banner_image = IMG_URL.'notifications_image'.DS.$notification_type.'.jpg';
                $result[] = $record;
            }
        }
        $_response['result_code'] = 1;
        if(!empty($get_record)) {
            $_response['result'] = $result;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_success');
        } else {
            $_response['result'] = '';
            $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
        return $_response;
    }
    
    /**
     *@mark_notification_read is used to mark read the notification
     *@input user_id(int) notification_id(int)
     *@output array
     */
    public function mark_notification_read(){
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $notification_id = (!empty($_REQUEST['notification_id'])) ? $_REQUEST['notification_id'] : 0;
        if(!empty($user_id) && !empty($notification_id)) {
            $table = $this->notification_tbl; 
            $columns = '';
            $where = ['user_id'=>$user_id,'id'=>$notification_id];
            if($notification_id == 'all') { // notification_id =  all means mark all notification read
                $where = ['user_id'=>$user_id];
            }
            $get_record = $this->_common->get_table_record($table,$columns,$where);
            $result = array();

            $_response['result_code'] = 1;
            if(!empty($get_record)) {
                foreach($get_record as $record) {
                    //get notification type
                    $notification_type = (!empty($record->notification_type)) ? $record->notification_type : 'reward_points';
                    //get notification title
                    $notification_title = $this->_common->get_reward_config($notification_type);
                    //seting the title
                    $record->notification_title = $notification_title;
                    $result[] = $record;
                }
                $updateArray = array("is_read" => 1);
                $whereClause = ['user_id'=>$user_id,'id'=>$notification_id];
                if($notification_id == 'all') {
                    $whereClause = ['user_id'=>$user_id];
                }
                $update = $this->_db->UpdateAll($this->notification_tbl, $updateArray, $whereClause, "");
                $_response['result'] = $result;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_success');
            } else {
                $_response['result'] = '';
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    }
    
    /**
     *@delete_notification is used to mark read the notification
     *@input user_id(int) notification_id(int)
     *@output array
     */
    public function delete_notification(){
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $notification_id = (!empty($_REQUEST['notification_id'])) ? $_REQUEST['notification_id'] : 0;
        if(!empty($user_id) && !empty($notification_id)) {
            $table = $this->notification_tbl; 
            $columns = '';
            $where = ['user_id'=>$user_id,'id'=>$notification_id,'is_deleted'=>0];
            if($notification_id == 'all') { // notification_id =  all means mark all notification read
                $where = ['user_id'=>$user_id];
            }
            $get_record = $this->_common->get_table_record($table,$columns,$where);
            $_response['result_code'] = 1;
            if(!empty($get_record)) {
                $updateArray = array("is_deleted" => 1);
                $whereClause = ['user_id'=>$user_id,'id'=>$notification_id];
                if($notification_id == 'all') {
                    $whereClause = ['user_id'=>$user_id];
                }
                $update = $this->_db->UpdateAll($this->notification_tbl, $updateArray, $whereClause, "");
                $_response['result'] = $get_record;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_deleted');
            } else {
                $_response['result'] = '';
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    }
    
    /**
     *@get_reward_catalogue is use the list the reward catalogues
     *@input : order(int)
     *@output : array
     */
    public function get_reward_catalogue() {
        $date = date('Y-m-d');
        // prepare the range data
        $ranges = ['0-500','500-1000','1000-2000','2000-5000','5000-10000','10000 >'];
        // get request parameters
        $sort = (!empty($_REQUEST['sort'])) ? $_REQUEST['sort'] : 1; // 2 : high to low, 1 : low to high
        $store_id = (!empty($_REQUEST['store_id'])) ? $_REQUEST['store_id'] : 0; // sotre_id
        $department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0; // sotre_id
        $range_id = (!empty($_REQUEST['range_id'])) ? $_REQUEST['range_id'] : 0; // sotre_id
        $sort = $this->_common->test_input($sort);
        $sortorder = 'ASC';
        
        //apply the filter condition for store, department and range
        if($sort == 2) {
            $sortorder = 'DESC';
        }
        $store_cond = '';
        if(!empty($store_id)) {
            $storeid = explode(',',$store_id);
            if(!empty($storeid)) {
                $store_cond = ' AND (';
                 $store_count = count($storeid);
                foreach($storeid as $skey=>$sid) {
                    $store_cond .= ' FIND_IN_SET('.$sid.', product.ms_store_id)';
					$store_cond .= ($store_count == ($skey+1)) ? ' ) ' : ' OR ' ;
                }
            }
        }
        $department_cond = '';
        if(!empty($department_id)) {
            $departmentid = explode(',',$department_id);
            if(!empty($departmentid)) {
                $department_cond = ' AND (';
                $department_count = count($departmentid);
                foreach($departmentid as $key=>$did) {
                    $department_cond .= ' FIND_IN_SET('.$did.', product.department_id)';
                    $department_cond .= ($department_count == ($key+1)) ? ' ) ' : ' OR ' ;
                }
            }
        }
        $range_cond = '';
        if(!empty($range_id)) {
            if($range_id == 0) {
                $range_cond = ' AND product.redeem_points <= 500 ';
            }
            if($range_id == 1) {
                $range_cond = ' AND (product.redeem_points >= 500 AND product.redeem_points <= 1000) ';
            }
            if($range_id == 2) {
                $range_cond = ' AND (product.redeem_points >= 1000 AND product.redeem_points <= 2000) ';
            }
            if($range_id == 3) {
                $range_cond = ' AND (product.redeem_points >= 2000 AND product.redeem_points <= 5000) ';
            }
            if($range_id == 4) {
                $range_cond = ' AND (product.redeem_points >= 5000 AND product.redeem_points <= 10000) ';
            }
            if($range_id == 5) {
                $range_cond = ' AND product.redeem_points >= 10000 ';
            }
        }
        $result_array    =   array();
        //prepare the filter data
        //$filter_query = "SELECT stores.ms_store_id,stores.store_name,product.department_id,product.department_name FROM ".$this->reward_catalogue_products." product JOIN ".$this->ava_stores_log." stores ON stores.ms_store_id = product.ms_store_id WHERE product.status = 1 ";
       //echo $filter_query = "SELECT stores.ms_store_id,stores.store_name,product.department_id,product.department_name FROM ".$this->reward_catalogue_products." product JOIN ".$this->ava_stores_log." stores ON FIND_IN_SET(stores.ms_store_id, product.ms_store_id) > 0 WHERE product.status = 1 GROUP BY stores.store_name";die;
        $filter_query = "SELECT product.department_id,product.department_name
          , CONCAT(stores1.store_name, if(stores2.store_name IS NULL,'',', '), ifnull(stores2.store_name,'')) as stores 
          , CONCAT(stores1.ms_store_id, if(stores2.ms_store_id IS NULL,'',', '), ifnull(stores2.ms_store_id,'')) as ms_store_id 
        FROM ava_reward_catalogue_products product 
        LEFT JOIN ava_stores_log stores2 ON 
         (replace(substring(substring_index(product.ms_store_id, ',', 2),
          length(substring_index(product.ms_store_id, ',', 2 - 1)) + 1), ',', '') = stores2.ms_store_id)
        INNER JOIN ava_stores_log stores1 ON 
         (replace(substring(substring_index(product.ms_store_id, ',', 1), 
          length(substring_index(product.ms_store_id, ',', 1 - 1)) + 1), ',', '') = stores1.ms_store_id)";
        $filter_result = $this->_db->my_query($filter_query);
        $store_array = array();
        $department_array = array();
        $store_temp_array = array();
        $department_temp_array = array();
        
        while($filter_row = $this->_db->my_fetch_object($filter_result)) {
            
            // collect the store filter
            $ms_store_id = (!empty($filter_row->ms_store_id)) ? $filter_row->ms_store_id : 0;
            $stores = (!empty($filter_row->stores)) ? $filter_row->stores : "NA";
            //explode the store id
            $ex_store_id = explode(',',$ms_store_id);
            $store_name = explode(',',$stores);
            if(!empty($ex_store_id)) {
                foreach($ex_store_id as $key=>$store_id) {
                    if(!empty($store_id)) {
                        if(!in_array($store_id,$store_temp_array)) {
                            $store['id'] = $store_id;
                            $store['name'] = (!empty($store_name[$key])) ? $store_name[$key] : '';
                            $store_temp_array[] = $store_id;
                            $store_array[] = $store;
                        }
                    }
                }
            }
            
            
            // collect the deaprtment filter
            $department_id = (!empty($filter_row->department_id)) ? $filter_row->department_id : 0;
            $departments = (!empty($filter_row->department_name)) ? $filter_row->department_name : "NA";
            //explode the departement id
            
            $ex_department_id = explode(',',$department_id);
            $department_name = explode(',',$departments);
            if(!empty($ex_department_id)) {
                foreach($ex_department_id as $key=>$departmentid) {
                    if(!empty($departmentid)) {
                        if(!in_array($departmentid,$department_temp_array)) {
                            $department['id'] = $departmentid;
                            $department['name'] = (!empty($department_name[$key])) ? $department_name[$key] : '';
                            $department_temp_array[] = $departmentid;
                            $department_array[] = $department;
                        }
                    }
                }
            }
            
        }

        $result_array['store_filter'] = $store_array;
        $result_array['department_filter'] = $department_array;
        
        // get data from db
        $query = "SELECT catalogue.title,catalogue.banner_image as cat_image,product.* FROM ".$this->reward_catalogue." catalogue JOIN ".$this->reward_catalogue_products." product ON product.catalogue_id = catalogue.id WHERE catalogue.status = 1 AND product.status = 1 ".$store_cond.$department_cond.$range_cond."AND (product.start_date <= '".$date."' AND product.end_date >= '".$date."') ORDER BY product.redeem_points ".$sortorder;
        $result = $this->_db->my_query($query);
        $_response['result_code'] = 1;
        $result_array['ranges'] = $ranges;
        $result_array['products'] = array();
        if ($this->_db->my_num_rows($result) > 0) {
            while($row = $this->_db->my_fetch_object($result)) {
                $result_array['catalogue_title'] = (!empty($row->title)) ? $row->title : '';
                $result_array['catalogue_image'] = (!empty($row->cat_image)) ? IMG_URL.'reward_images'.DS.$row->cat_image : '';
                
                $product['id'] = (!empty($row->id)) ? $row->id : '';
                $product['product_title'] = (!empty($row->product_title)) ? $row->product_title : '';
                $product['banner_image'] = (!empty($row->banner_image)) ? IMG_URL.'reward_images'.DS.$row->banner_image : '';
                $ms_store_id = (!empty($row->ms_store_id)) ? $row->ms_store_id : 0;
                $exp_ms_store_id = explode(',',$ms_store_id);
                if(!empty($exp_ms_store_id)){
                    foreach($exp_ms_store_id as $store_id){
                        $store_data = $this->get_store_data($store_id);
                        $product['store'][] = $store_data;
                    }
                }

                //$product['store_name'] = (!empty($row->store_name)) ? $row->store_name : '';
                $product['department_name'] = (!empty($row->department_name)) ? $row->department_name : '';
                $product['product_name'] = (!empty($row->product_name)) ?  trim(preg_replace('/\s+/',' ', $row->product_name)) : '';
                $product['redeem_points'] = (!empty($row->redeem_points)) ? $row->redeem_points : 0;
                $product['add_extra_price'] = (!empty($row->add_extra_price)) ? $row->add_extra_price : 0;
                $product['pay_amount_with_redeem_points'] = (!empty($row->redeem_points_with_extra_price)) ? $row->redeem_points_with_extra_price : 0;
                $product['extra_price'] = (!empty($row->extra_price)) ? $row->extra_price : 0;
                $product['start_date'] = (!empty($row->start_date)) ? $row->start_date : 0;
                $product['short_description'] = (!empty($row->short_description)) ? $row->short_description : 0;
                $product['long_description'] = (!empty($row->long_description)) ? $row->long_description : 0;
                $result_array['products'][] = $product;
            }
        }
        $_response['result']    =   $result_array;
        return $_response;
    }
    
    public function get_store_data($ms_store_id=0) {
		$query = "SELECT * FROM " . $this->ava_stores_log . " WHERE ms_store_id = '$ms_store_id'";
        $result = $this->_db->my_query($query);
        $row = $this->_db->my_fetch_object($result);
        
        $lat2 				= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
        $lng2 				= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
        $distance 			= $this->distance($lat1, $lng1, $lat2, $lng2, $unit);
        $array['id'] 		= (isset($row->store_id)) ? trim($row->store_id) : "";
        $array['ms_store_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
        $array['name'] 		= (isset($row->store_name)) ? trim($row->store_name) : ""; 
        $array['outlet_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
        $array['addr_1'] 	= (isset($row->store_address_1)) ? trim($row->store_address_1) : "";
        $array['addr_2'] 	= "";
        $array['addr_3'] 	= $array['addr_1']." ".$array['addr_2'];
        $array['post_code'] = (isset($row->store_post_code)) ? trim($row->store_post_code) : "";
        $array['phone_no'] 	= "07 ".(isset($row->store_phone)) ? trim($row->store_phone) : "";
        $array['lat'] 		= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
        $array['lng'] 		= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
        $array['distance'] 	= strval(round($distance,2));
        $array['open_hours'] = (isset($row->open_hours)) ? trim($row->open_hours) : "";
        return $array;
    }
    
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return ($miles * 1.609344);
		}
	}
    
	/**
	 * Function used to manage device array keys
	 * @param array device_ids
	 * @return void
	 * @access private
	 * 
	 */
     private function device_array_keys($device_ids='') {
		$device_array = array();
		if(!empty($device_ids)) {
			foreach($device_ids as $key=>$device_id) {
				$device_array[] = $device_id;
			}
		}
		return $device_array;
	 }
	 
	/**
	 * Function used to manage users refferal's purchase points in cron
	 * @param null
	 * @return void
	 * @access public
	 *  
	 */ 
	public function send_refferals_first_purchase_points() {
		 
		// get all users whos refferal make purchase
		$sql = "SELECT u.id,u.email,u.acc_number,u.my_reference_code,u.device_token,u.device_type,(SELECT referral_purchase_points FROM ".$this->reward_config_table." LIMIT 1) AS referral_purchase_points,(SELECT id FROM ".$this->user_table." WHERE my_reference_code = u.reference_code) AS referred_by FROM ".$this->user_table." u WHERE u.my_reference_code NOT IN ( SELECT comments FROM ". $this->reward_transaction_table ." WHERE transaction_type = 'referral_first_purchase' AND comments = u.my_reference_code ) AND  u.reference_code != '' AND acc_number != ''";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$android_devices = array();
			$ios_devices = array();
			while($row = $this->_db->my_fetch_object($result)) {
				// set members data
				$user_id = (!empty($row->id)) ? $row->id : 0;
				$referred_by = (!empty($row->referred_by)) ? $row->referred_by : 0;
				$acc_number = (!empty($row->acc_number)) ? $row->acc_number : 0;
				$reward_points = (!empty($row->referral_purchase_points)) ? $row->referral_purchase_points : 0;
				$my_reference_code = (!empty($row->my_reference_code)) ? $row->my_reference_code : 0;
				// get members purchase record
				$member_trx_query = "SELECT top 1 JNAH_TRX_NO as trx_no FROM JNLH_Account_TBL JOIN JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO JOIN Member on MEMB_NUMBER = JNAD_PRODUCT WHERE MEMB_Exclude_From_Competitions = '0' AND JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' AND JNAD_PRODUCT = $acc_number";
				$member_trx_data = $this->_mssql->my_mssql_query($member_trx_query,$this->con);
				if($this->_mssql->pdo_num_rows_query($member_trx_query,$this->con) > 0 ) {
					$datetime = date('Y-m-d H:i:s');
					// store referel reward points
					$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date,comments) VALUES ('$referred_by','$reward_points','referral_first_purchase',2,'$datetime','$my_reference_code')";
					$this->_db->my_query($member_reward_sql);
					mysql_insert_id();
					// set customer's device param
					$device_type = $row->device_type;
					$device_id   = trim($row->device_token);
					if(!empty($device_id)) {
						if($device_type == 'android'){
							// set andriod device token in array
							$android_devices[] = $device_id;
						}else{
							// set ios device token in array
							$ios_devices[] = $device_id;
						}
					}
					// set notification message
					$push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.referral_purchase_points'),$reward_points);
					// store notification record
					$notification_data = ['user_id'=>$referred_by,'notification'=>$push_message,'notification_type'=>'referral_first_purchase','is_send'=>1];
					$this->_common->save_notification($notification_data);
				}
			}
			// send push notification
			if(!empty($android_devices) || !empty($ios_devices)) {
				$this->send_push_notification($android_devices,$ios_devices,$this->_common->langText($this->_locale,'txt.reward.referral_purchase_points_push_msg'));
			}
		}
		return true;
	}
    
    /*
     * @sendNotification is use to send notification email and push to the users 
     * @input null
     * @output void
     **/
    public function sendNotification() {
        // get notification config status
		$sql = "SELECT notification_status FROM " . $this->reward_config_table . "";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
			$notification_status = (!empty($row->notification_status)) ? $row->notification_status : 0;
			if($notification_status == 1) {
				//get user to whome send notification will send
				$query = "SELECT ntf.*,user.email,user.firstname,user.lastname,user.device_token,user.device_type FROM ".$this->notification_tbl." ntf JOIN ".$this->user_table." user ON user.id = ntf.user_id WHERE ntf.is_send = 0 ORDER BY ntf.created_date DESC";
				$result = $this->_db->my_query($query);      
				if ($this->_db->my_num_rows($result) > 0) {
					while($row = $this->_db->my_fetch_array($result)) {
						$android_devices = array();
						$ios_devices = array();
						//get result data
						$id = (!empty($row['id'])) ? $row['id'] : '';
						$notification = (!empty($row['notification'])) ? $row['notification'] : '';
						$notification_type = (!empty($row['notification_type'])) ? $row['notification_type'] : '';
						$user_id = (!empty($row['user_id'])) ? $row['user_id'] : '';
						$email = (!empty($row['email'])) ? $row['email'] : '';
						$firstname = (!empty($row['firstname'])) ? $row['firstname'] : '';
						$lastname = (!empty($row['lastname'])) ? $row['lastname'] : '';
						$device_token = (!empty($row['device_token'])) ? trim($row['device_token']) : '';
						$device_type = (!empty($row['device_type'])) ? $row['device_type'] : '';
						$reward_point = (!empty($row['reward_point'])) ? $row['reward_point'] : '';
						
						// check empty email & notification type and then send email 
						if(!empty($notification_type) && !empty($email)) {
							//prepare email body
							$username = $firstname.' '.$lastname;
							$username = ucwords($username);
							$emailBody = $username.'{#STRINGBREAKER#}'.$notification.'{#STRINGBREAKER#}'.$reward_point;
							// preapare the array for email
							$emailData = ['emailBody'=>$emailBody,'receiverId'=>$email,'purpose'=>$notification_type];
						   //call function to send email
							$this->_common->send_email($emailData);
						} 
						// check empty device_token & device_type and then push
						if(!empty($device_token) && !empty($device_type)) {
							if($device_type == 'android') {     // send andriod push 
								$android_devices[] = $device_token;
								$this->_push->send_android_reward_push($android_devices,$notification);
							} else if ($device_type == 'ios') { // send IOS push
								$ios_devices[] = $device_token;
								$this->_push->send_ios_reward_push($ios_devices,$notification);
							}
						}
						//update user notification, that email and push has send to user
						$updateArray = array("is_send" => 1);
						$whereClause = ['user_id'=>$user_id,'id'=>$id];
						$update = $this->_db->UpdateAll($this->notification_tbl, $updateArray, $whereClause, "");
					}
				}
			}
		}
        return true;
    } // end sendNotification
    
    /**
     *  @addRewardPoints is use to insert/update reward points on user's different activities
     *  @input user_id(int) reward_type(string)
     *  @output void
     */
    public function addRewardPoints() {
        // get request parameters
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $reward_type = (!empty($_REQUEST['reward_type'])) ? $_REQUEST['reward_type'] : '';
        // check required parameters
        if(!empty($user_id) && !empty($reward_type)) {
            //check reward point exist or not
            $reward_existance = $this->_common->get_reward_config($reward_type);
            if($reward_existance != 'Not valid') {
                // get reward point w.r.t. reward_type
                $table = $this->reward_config_table; 
                $columns = [$reward_type];
                $get_record = $this->_common->get_table_record($table,$columns);
                $reward_points = (!empty($get_record[0]->$reward_type)) ? $get_record[0]->$reward_type : 0;
                if(!empty($reward_points)) {
                    $record_transaction_query = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                        '".mysql_real_escape_string($user_id)."',
                        '".mysql_real_escape_string($reward_points)."',
                        '".mysql_real_escape_string($reward_type)."',
                        '".date('y-m-d H:i:s')."',
                        '2')";
                    $this->_db->my_query($record_transaction_query);
                    $reward_message = $this->_common->get_reward_message($reward_type);
                    $notification_data = ['user_id'=>$user_id,'notification'=>$reward_message,'notification_type'=>$reward_type];
                    $this->_common->save_notification($notification_data);
                }
                $_response['result_code'] = 1;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.success.add_reward_points');  
            } else {
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.error.add_reward_points');   
            }
        } else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } //end addRewardPoints
    
    /*
    * @contactSynchronisation is use to sync the user contact list
    * @input user_id(int) contact_list(json) 
    * @output void
    **/
    public function contactSynchronisation() {
        // get request parameters
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $contact_list = (!empty($_REQUEST['contact_list'])) ? json_decode($_REQUEST['contact_list']) : 0;
        // check required parameters
        if(!empty($user_id) && !empty($contact_list)) {
            
            //award reward point to user on contact sync
            $reward_points_type = 'contact_synchronisation_points';
            $today_date = CURRENT_DATE;
            $reward_campaign_sql = "SELECT id,reward_points FROM ".$this->reward_campaign_tbl." where status = '1' AND is_deleted = '0' AND action_type = '3' AND start_date <= '".$today_date."' AND end_date >= '".$today_date ."'  " ;
            $get_reward_campaign = $this->_db->my_query($reward_campaign_sql);
            $reward_campaign = $this->_db->my_fetch_object($get_reward_campaign);
            
            $campaign_id = (!empty($reward_campaign->id)) ? $reward_campaign->id : 0;
            $reward_points = (!empty($reward_campaign->reward_points)) ? $reward_campaign->reward_points : 0;
            
            //check camapaign has reward adn can be assign to user or not
            $is_reward_assign = $this->_common->manageCompaignLog($user_id,$campaign_id);
            if(!empty($is_reward_assign)) {
				// get contact sync transaction log;If exist in db
				$contactsync_query = "SELECT id FROM " .$this->reward_transaction_table. " WHERE transaction_type = '$reward_points_type' AND user_id = '$user_id' ";
				$contactsync_result = $this->_db->my_query($contactsync_query);
                if($this->_db->my_num_rows($contactsync_result) == 0){
					$record_transaction_query = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                    '".mysql_real_escape_string($user_id)."',
                    '".mysql_real_escape_string($reward_points)."',
                    '".mysql_real_escape_string($reward_points_type)."',
                    '".date('y-m-d H:i:s')."',
                    '2')"; // 2 received or success
                    $this->_db->my_query($record_transaction_query);

					// save notification
					$push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.contact_synchronisation_points'),$reward_points); 
					
					$notification_data = ['user_id'=>$user_id,'notification'=>$push_message,'notification_type'=>$reward_points_type,'reward_point'=>$reward_points];
					$this->_common->save_notification($notification_data);      
				}               
            }
            
            //insert in contact list
            $insert_query = "INSERT INTO ".$this->users_contacts_temp_tbl." (user_id,friend_name,friend_mobile_no,friend_email_address,created_date) VALUES ";
            $insert_query_value = '';
            foreach($contact_list as $contact) {
                if(!empty($contact)) {
                    
                    $friend_name = (!empty($contact->friend_name)) ? $contact->friend_name : '';
                    $friend_mobile_no = (!empty($contact->friend_mobile_no)) ? $contact->friend_mobile_no : '';
                    $friend_email_address = (!empty($contact->friend_email_address)) ? $contact->friend_email_address : '';
                              
                    $insert_query_value .= "('".mysql_real_escape_string($user_id)."',
                                        '".mysql_real_escape_string($friend_name)."',
                                        '".mysql_real_escape_string($friend_mobile_no)."',
                                        '".mysql_real_escape_string($friend_email_address)."',
                                        '".date('y-m-d H:i:s')."'),";                
                }
            }
            if(!empty($insert_query_value)) {
                $insert_query_value = rtrim($insert_query_value,",");
                $insert_query = $insert_query.$insert_query_value;
                $this->_db->my_query($insert_query);
            }
            $_response['result_code'] = 1;
            
            $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.success.contact_synchronisation');
            if(!empty($is_reward_assign)) {
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.success.contact_synchronisation');
            }
        } else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    }
    //contactSynchronisation
    
    /*
     * @uploadContactSynchronisation is use to sync the user contact list
     * @input user_id(int) contact_list(json) 
     * @output void
     **/
    public function uploadContactSynchronisation() {
        //get contact from temp table
        $table = $this->users_contacts_temp_tbl; 
        $columns = '';
        $where = ['is_uploaded'=>0];
        $contact_list = $this->_common->get_table_record($table,$columns,$where);
            foreach($contact_list as $contact) {
                if(!empty($contact)) {
                    
                    $contacts_temp_id = (!empty($contact->id)) ? $contact->id : '';
                    $user_id = (!empty($contact->user_id)) ? $contact->user_id : '';
                    $friend_name = (!empty($contact->friend_name)) ? $contact->friend_name : '';
                    $friend_mobile_no = (!empty($contact->friend_mobile_no)) ? $contact->friend_mobile_no : '';
                    $friend_email_address = (!empty($contact->friend_email_address)) ? $contact->friend_email_address : '';
                    
                    //check mobile no exist in user contact list
                    $table = $this->users_contacts_tbl; 
                    $columns = ['id'];
                    $where = ['user_id'=>$user_id,'friend_mobile_no'=>$friend_mobile_no];
                    $get_record = $this->_common->get_table_record($table,$columns,$where);
                    // if mobile no exits in user contact list list then update the information
                    if(!empty($get_record)) {
                        $contact_list_id = (!empty($get_record[0]->id)) ? $get_record[0]->id : 0;
                        $updateArray = ["friend_name" => $friend_name,"friend_email_address" => $friend_email_address,"modified_date" => date('y-m-d H:i:s')];
                        $whereClause = ['id'=>$contact_list_id];
                        $update = $this->_db->UpdateAll($this->users_contacts_tbl, $updateArray, $whereClause, "");
                    } else {
                        //if mobile not exist in contact list then insert in contact list
                        $insert_query = "INSERT INTO ".$this->users_contacts_tbl." (user_id,friend_name,friend_mobile_no,friend_email_address,created_date) VALUES(
                            '".mysql_real_escape_string($user_id)."',
                            '".mysql_real_escape_string($friend_name)."',
                            '".mysql_real_escape_string($friend_mobile_no)."',
                            '".mysql_real_escape_string($friend_email_address)."',
                            '".date('y-m-d H:i:s')."')";
                        $this->_db->my_query($insert_query);
                    }
                    
                    $updateArray1 = ["is_uploaded" => 1,"modified_date" => date('y-m-d H:i:s')];
                    $whereClause1 = ['id'=>$contacts_temp_id];
                    $this->_db->UpdateAll($this->users_contacts_temp_tbl, $updateArray1, $whereClause1, "");
                    
                }
            }
        
    } //end contactSynchronisation
    
    /*
     * @contactList is listing all contact list
     * @input user_id(int)
     * @output void
     **/
    public function contactList() {
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;  
        if(!empty($user_id)) {
            // get contact list from table
            $table = $this->users_contacts_tbl; 
            $columns = [];
            $where = ['user_id'=>$user_id];
            $get_record = $this->_common->get_table_record($table,$columns,$where);
            $response_data['user_id'] = $user_id;
            // check record exist or not
            if(!empty($get_record)) {
                foreach($get_record as $record){
                    $result['friend_name'] = (!empty($record->friend_name)) ? $record->friend_name : '';
                    $result['friend_mobile_no'] = (!empty($record->friend_mobile_no)) ? $record->friend_mobile_no : '';
                    $result['friend_email_address'] = (!empty($record->friend_email_address)) ? $record->friend_email_address : '';
                    $response_data[] = $result;
                }
                $_response['result_code'] = 1;
                $_response['result_data'] = $response_data;
            } else {
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');   
            }
        }  else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } // end contactList
    
    /*
     * @contactSynchronisation is use to sync the user contact list
     * @input user_id(int) contact_list(json) 
     * @output void
     **/
    public function contactSynchronisation1() {
        // get request parameters
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $phone = (!empty($_REQUEST['phone'])) ? $_REQUEST['phone'] : '';
        $email = (!empty($_REQUEST['email'])) ? $_REQUEST['email'] : '';
         // check required parameters
        if(!empty($user_id)) {
            // define empty user_ids array
            $user_ids = array();
            $response_data = array();
            
            //explode phone no and email id into array
            $phone_array = explode(",", $phone); 
            $email_array = explode(",", $email); 
            
            //check phone exist into database
            if(!empty($phone_array)) {
                foreach($phone_array as $phone_no) {
                    if(!empty($phone_no)) {
                        $get_user_id = $this->checkMobileNo($phone_no);
                        if($get_user_id){
                            array_push($user_ids,$get_user_id);
                        }
                    }
                }
            }
            
            //check email address exist into database
            if(!empty($email_array)) {
                foreach($email_array as $email_id) {
                    if(!empty($email_id)) {
                        $get_user_id = $this->checkEmailAddress($email_id);
                        if($get_user_id){
                            array_push($user_ids,$get_user_id);
                        }
                    }
                }
            }
            
            //remove duplicate user ids
            $user_ids = array_unique($user_ids);
            if(!empty($user_ids)) {
                foreach($user_ids as $id_user) {
                    if(!empty($id_user)) {
                        $user_query = "SELECT * FROM " . $this->user_table . " WHERE id = '" . $id_user . "' ";
                        $sql_result = $this->_db->my_query($user_query);
                        while ($user_data = $this->_db->my_fetch_object($sql_result)) {
                            $response_data[] = $user_data;
                        }
                    }
                }
            }
            $_response['result_code'] = 1;
            $_response['response_data'] = $response_data;
        } else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } //end contactSynchronisation
    
    /*
     * @checkMobileNo is to check user exist with given mobile no. or not
     * @input phone_no(int)
     * @output int/boolean 
     **/
     
    public function checkMobileNo($phone_no ='') {
        //user w.r.t phone no
        $table = $this->user_table; 
        $columns = ['id'];
        $where = ['mobile'=>$phone_no];
        $get_record = $this->_common->get_table_record($table,$columns,$where);
        if(!empty($get_record)){
            $user_id = (!empty($get_record[0]->id)) ? $get_record[0]->id : 0;
            if(!empty($user_id)) {
                return $user_id;
            }
        }
        return false;
    }//end checkMobileNo
    
    /*
     * @checkEmailAddress is to check user exist with given email address. or not
     * @input email_id(int)
     * @output int/boolean 
     **/
     
    public function checkEmailAddress($email_id = '') {
        //user w.r.t email address
        $table = $this->user_table; 
        $columns = ['id'];
        $where = ['email'=>$email_id];
        $get_record = $this->_common->get_table_record($table,$columns,$where);
        if(!empty($get_record)){
            $user_id = (!empty($get_record[0]->id)) ? $get_record[0]->id : 0;
            if(!empty($user_id)) {
                return $user_id;
            }
        }
        return false;
    }//end checkEmailAddress
    
    
     /**
	  * @rewardCampaignList is listing of all reward campaign
      * @input: none
      * @output:json array
      **/
    public function rewardCampaignList() {
		$user_id = (!empty($_REQUEST['user_id']))?$_REQUEST['user_id']:0;
		$table   = $this->reward_campaign_tbl; 
        $Today   = date('Y-m-d');
        $user_query = "SELECT * FROM " .$table. " WHERE status = '1' AND is_deleted = '0' AND start_date <= '".$Today."' AND end_date >= '".$Today ."'  ";
        $get_record = $this->_db->my_query($user_query);
       	if($this->_db->my_num_rows($get_record) > 0){
			$record_array= array();
			while($row = $this->_db->my_fetch_object($get_record)){
                $record_array[]   = array( 'id' =>$row->id,
                                        'name'  =>$row->name,
                                        'short_description' =>$row->short_description,
                                        'long_description'  =>$row->long_description,
                                        'start_date'   =>$row->start_date,
                                        'end_date' 	   =>$row->end_date,
                                        'action_type'  =>$row->action_type,
                                        'action_sub_type'  =>$row->action_sub_type,
                                        'product_id'  =>$row->product_id,
                                        'reward_points'=>$row->reward_points,
                                        'image' =>!empty($row->image)?$this->reward_img_path.''.$row->image:'',
                                        'banner_image' =>!empty($row->banner_image)?$this->reward_img_path.''.$row->banner_image:'',
                                        'created_by' =>$row->created_by
                                    );
                
            }
            $_response['result'] 	    = $record_array;
            $_response['result_code']   = 1;
            $_response['balance_point'] = $this->_common->getBalancePoint($user_id);
            $_response['message'] 	    = $this->_common->langText($this->_locale,'txt.reward.campaign_success');
        } else {
                $_response['result'] = '';
                $_response['result_code'] = 0;
   				$_response['balance_point'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
        return $_response;    
	}
	

/**
  * @redeemReward for redeem 
  * @input: user_id,catalogue_id,acc_number
  * @output:json array
  **/
	public function redeemReward(){
		$reward_trans_table     = $this->reward_transaction_table;
		$reward_redeem_log_tbl  = $this->reward_redeem_log_tbl;
		$reward_catalogue_table = $this->reward_catalogue;		
		
        $date            = date('Y-m-d');
        $currentdatetime = CURRENT_TIME;
		$user_id         = (!empty($_REQUEST['user_id']))?$_REQUEST['user_id']:0;
		$catalogue_id    = (!empty($_REQUEST['catalogue_id']))?$_REQUEST['catalogue_id']:0;
		$product_id      = (!empty($_REQUEST['product_id']))?$_REQUEST['product_id']:0;
		$member_id       = (!empty($_REQUEST['acc_number']))?$_REQUEST['acc_number']:0;
		$is_extra_amount_pay       = (!empty($_REQUEST['is_extra_amount_pay']))?$_REQUEST['is_extra_amount_pay']:0;
      
        $_response['result']        = NULL;
        $_response['result_code']   = 0;
        $_response['message']       = $this->_common->langText($this->_locale,'txt.register.error.required');
        if(!empty($user_id) && !empty($catalogue_id) && !empty($member_id)){
            $user_point_array = $this->_common->getBalancePoint($user_id);
            $user_point = (!empty($user_point_array['redeem_points'])) ? $user_point_array['redeem_points'] : 0;
            $is_expired = (!empty($user_point_array['is_expired'])) ? $user_point_array['is_expired'] : 0;
            if(empty($is_expired)) {
                $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_previous_point');
                $_response['result_code'] = 0;
                return $_response;
            }
            $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_point_notnull');
            $_response['result_code'] = 0;
            
            if(!empty($user_point)){
                $query = "SELECT product.redeem_points,product.redeem_points_with_extra_price,product.add_extra_price,product.extra_price,product.product_id,product.product_name,product.catalogue_id FROM ".$this->reward_catalogue." catalogue JOIN ".$this->reward_catalogue_products." product ON product.catalogue_id = catalogue.id WHERE catalogue.status = 1 AND product.id = ".$product_id." AND product.status = 1 AND (product.start_date <= '".$date."' AND product.end_date >= '".$date."') ";
                
                $get_record_two = $this->_db->my_query($query);
                $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_invalid');
                $_response['result_code'] = 0;
                
                if($this->_db->my_num_rows($get_record_two) > 0){
                    $catalogue_data = $this->_db->my_fetch_object($get_record_two);    
                    //print_r($catalogue_data);die;                                    
                    if(!empty($catalogue_data->redeem_points) && $catalogue_data->redeem_points >0){
                        $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_point_not_enough');
                        $_response['result_code'] = 0;
                        $product_id = (!empty($catalogue_data->product_id)) ? $catalogue_data->product_id : 0 ; 
                        $product_name = (!empty($catalogue_data->product_name)) ? $catalogue_data->product_name : 0 ; 
                        $redeem_points = (!empty($catalogue_data->redeem_points)) ? $catalogue_data->redeem_points : 0 ; 
                        $redeem_points_with_extra_price = (!empty($catalogue_data->redeem_points_with_extra_price)) ? $catalogue_data->redeem_points_with_extra_price : 0 ; 
                        $add_extra_price = (!empty($catalogue_data->add_extra_price)) ? $catalogue_data->add_extra_price : 0 ; 
                        $extra_price = 0;
                        if(!empty($add_extra_price)) {
                            $extra_price = (!empty($catalogue_data->extra_price)) ? $catalogue_data->extra_price : 0 ;                                    
                        }  
                        if($user_point > $redeem_points){
                            // set qr code image name
                            $error_correction_level = 'H';
                            $matrix_point_size = 10;
                            // get barcode final digit
                            $barcode_digits = $this->_common->get_barcode_digits($member_id);
                            $qr_code        = $barcode_digits.'#'.$catalogue_data->product_id; 
                            $qr_filename    = md5($qr_code.'|'.$error_correction_level.'|'.$matrix_point_size).'.png';
                            include_once("qr_code.php"); // include to generate user barcode
                            // convert qr code image in byte code
                            if(!empty($qr_filename)) {
                                $qr_path = IMG_URL.'qr_codes/'.$qr_filename;
                                $type    = pathinfo($qr_path, PATHINFO_EXTENSION);
                                $data    = $this->_common->file_get_contents_curl($qr_path);
                                $qr_code_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
                            }
                                                             
                            $qr_path = (isset($qr_path) && !empty($qr_path))?$qr_path:'';
                            $Insert_redeem_log = "INSERT INTO ".$reward_redeem_log_tbl." (user_id,catalogue_id,product_id,product_name,section_type,qr_code,redeem_point,redeem_points_with_extra_price,extra_price,create_date,is_extra_amount_pay) VALUES('".$user_id."','".$catalogue_id."','".$product_id."','".$product_name."','1','".$qr_path."','".$redeem_points."','".$redeem_points_with_extra_price."','".$extra_price."','".$currentdatetime."','".$is_extra_amount_pay."')";
                            $this->_db->my_query($Insert_redeem_log);
                            $remain_point = (int)($user_point - $catalogue_data->reward_points);
                            $msg_success = $this->_common->langText($this->_locale,'txt.reward.redeem_sucessfully');
                            $_response['result']       = array('balance_point'=>$remain_point,'qr_path'=>$qr_path);
                            $_response['result_code'] = 1;
                            $_response['message']     = str_replace(array('{x}'), array($catalogue_data->title),$msg_success);
                        }        
                    }
                }
            }
        }
        return $_response; 
	}//redeemReward
    
    /*
     *@userRedeemHistory
     *@input: user_id
     *@output: array 
     **/
    public function userRedeemHistory() {
        //default result to be return
        $response =array();
        $_response['result_code'] = 0;
        $_response['message']       = $this->_common->langText($this->_locale,'txt.register.error.required');
        
        //get request parameters
        $user_id    =   (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        if(!empty($user_id)) {
            $query = "SELECT redemlog.id,redemlog.redeem_point,redemlog.product_name,redemlog.extra_price,redemlog.create_date FROM ".$this->reward_redeem_log_tbl." redemlog WHERE redemlog.user_id = '".$user_id."' AND redemlog.is_redeem = '1'";
            $get_result = $this->_db->my_query($query);
            if($this->_db->my_num_rows($get_result) > 0){
                while($data = $this->_db->my_fetch_object($get_result)) {
                    $result['id'] = (!empty($data->id)) ? $data->id : 0;
                    $result['redeem_point'] = (!empty($data->redeem_point)) ? $data->redeem_point : 0;
                    $result['create_date'] = (!empty($data->create_date)) ? $data->create_date : 0;
                    $result['product_name'] = (!empty($data->product_name)) ? $data->product_name : '';
                    $result['extra_price'] = (!empty($data->extra_price)) ? $data->extra_price : '';
                    $response[] = $result;
                }
                $_response['result_code'] = 1;
                $_response['result'] = $response;
                $_response['message']   = $this->_common->langText($this->_locale,'txt.reward.user_redeem_history');
            } else {
                $_response['message']       = $this->_common->langText($this->_locale,'txt.no_record_found');
            }
        }
        return $_response;
    }//userRedeemHistory
    
     /**
      * @shareApplication use to update the share info and assign the reward points 
      * @input: user_id,catalogue_id
      * @output:json array
      **/
	public function shareApplication(){
        // get the requst parameters
        $user_id    = (!empty($_REQUEST['user_id']))?$_REQUEST['user_id']:0;
		$campaign_id= (!empty($_REQUEST['campaign_id']))?$_REQUEST['campaign_id']:0;
		$share_with= (!empty($_REQUEST['share_with']))?$_REQUEST['share_with']:'';
        //check required parameters
        if(!empty($user_id) && !empty($campaign_id)){
            //check camapaign has reward adn can be assign to user or not
            $is_reward_assign = $this->_common->manageCompaignLog($user_id,$campaign_id);
            if(!empty($is_reward_assign)) {
                $transaction_type = 'application_social_sharing_points';
                $record_transaction_query = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,comments,transaction_date,status) VALUES(
                '".mysql_real_escape_string($user_id)."',
                '".mysql_real_escape_string($is_reward_assign)."',
                '".mysql_real_escape_string($transaction_type)."',
                '".mysql_real_escape_string($share_with)."',
                '".date('y-m-d H:i:s')."',
                '2')"; // 2 received or success
                $this->_db->my_query($record_transaction_query);
                
                //save as notification
                $push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.application_social_sharing_points'),$is_reward_assign); 
                $notification_data = ['user_id'=>$user_id,'notification'=>$push_message,'notification_type'=>$transaction_type,'reward_point'=>$is_reward_assign];
                $this->_common->save_notification($notification_data);
                     
                $_response['result_code'] = 1;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.share_pplication.success');
            } else {
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.share_pplication.notvalid');
            }
        }  else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
	}//shareApplication
    
    /*
     * @userTransactionHistory
     * */
    public function userTransactionHistory() {
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $table = $this->reward_transaction_table; 
        $columns = '';
        $where = ['user_id'=>$user_id];
        $orderby = 'transaction_date';
        $get_record = $this->_common->get_table_record($table,$columns,$where,$orderby);
		$result = array();
        if(!empty($get_record)) {
            foreach($get_record as $record) {
                //get notification type
                $notification_type = (!empty($record->transaction_type)) ? $record->transaction_type : 'reward_points';         
                //get notification title
                $notification_title = $this->_common->get_reward_config($notification_type,1);           
                //seting the title
                $record->notification_title = $notification_title;
                // set credit/debit point flag
                $record->is_credited = 1;
                $result[] = $record;
            }
        }
        $_response['result_code'] = 1;
        if(!empty($get_record)) {
            $_response['result'] = $result;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_success');
        } else {
            $_response['result'] = '';
            $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
        return $_response;
    } //userTransactionHistory
    
    
    /**
     * Fetch the email template as per the post purpose and language
	 * Return the email template data from zon_email_template table.
	 **/
	public function get_edm_template($edmId=0){
		if(!empty($edmId)){
			$query = "SELECT b.id,b.subject,b.body,b.purpose,b.email_from_name FROM ava_edm a LEFT JOIN ava_email_template b ON b.id  = a.email_id WHERE b.template_type = 2 AND a.id = ".$edmId;
			$get_result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($get_result) > 0){
				$emailData = $this->_db->my_fetch_object($get_result);
			 }
		 }
			return (!empty($emailData)?$emailData:"");
	}
	
    
    /**
	 * send_edms for sending edm to the user
	 * @access 
	 * @return  status of mailsent
	 **/	
	public function send_edms(){
		
		// set requested param
		$edm_id = (!empty($_REQUEST['edm_id'])) ? $_REQUEST['edm_id'] : 0;
		$limit  = (!empty($_REQUEST['limit'])) ? $_REQUEST['limit'] : 0;
		$offset = (!empty($_REQUEST['offset'])) ? $_REQUEST['offset'] : 0;
		$current_time = date('H:i:s');
		// fetch edm details
		$edm_sql = "SELECT id,send_time FROM ".$this->edm." WHERE id = $edm_id";
		$edm_result = $this->_db->my_query($edm_sql);
		if($this->_db->my_num_rows($edm_result) > 0){
			$email_data = $this->_db->my_fetch_object($edm_result);
		}
		$edm_send_time = (!empty($email_data->send_time)) ? $email_data->send_time : $current_time;
		if($edm_send_time <= $current_time) {
			// fetch queue records
			$query = "SELECT ql.id,ql.edm_id,ql.user_id,ql.email,u.firstname,u.lastname FROM ".$this->edm_queue_log." ql LEFT JOIN ".$this->user_table." u ON u.id = ql.user_id WHERE ql.edm_id = $edm_id AND ql.is_read = 0 AND DATE(ql.create_date) = '".date('Y-m-d')."' ORDER BY ql.id ASC LIMIT $limit OFFSET $offset";
			$get_result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($get_result)>0){
				$check_array = array();
				while($data  = $this->_db->my_fetch_object($get_result)){
					$update_sql = "UPDATE ".$this->edm_queue_log." SET is_read = 1 WHERE id =".$data->id;
					$this->_db->my_query($update_sql);				
					if(!empty($data->id) && !empty($data->edm_id) && !empty($data->email)){
						$emailId = $data->email;
						if(in_array($data->edm_id,$check_array)){
							$emailData = ${"template_".$data->edm_id};
						}else{
							$emailData = $this->get_edm_template($data->edm_id);
							${"template_".$data->edm_id} = $emailData;
							array_push($check_array,$data->edm_id);
						}
						if(!empty($emailData)){
							$edm_id			 = encode($data->edm_id);
							$viewLogId       = encode($data->id);
							$user_id         = (!empty($data->user_id)) ? encode($data->user_id) : 0;
							$user_name       = (!empty($data->firstname)) ? $data->firstname.' '.$data->lastname : 'Customer';
							$from_email      = (!empty($emailData->email_from_name)) ? $emailData->email_from_name : '';
							$edm_gif_url     = SERVICES_URL_PATH."reward/edm_view?eid=$viewLogId";	
							$view_product    = URL_PATH."promotion/product-detail.php?eid=$edm_id&uid=$user_id&qid=$viewLogId&pr=";
							$view_more_url   = URL_PATH."promotion/index.php?eid=$edm_id&uid=$user_id";	
							/*This is hard code url for img need to dynamic*/
							$site_url 		  = "http://cdnsolutionsgroup.com/coyotev3/";
							$searchArray      = array("{GIF_IMAGEURL}","{VIEW_MORE_PRODUCT}","{VIEW_PRODUCT}","{SITE_URL}","{RECIPIENT_NAME}");
							$replaceArray     = array($edm_gif_url,$view_more_url,$view_product,$site_url,$user_name);
							$emailBody	      = str_replace($searchArray, $replaceArray,$emailData->body);
							$this->_common->send_edm_mail($emailId,$emailData->subject,$emailBody,$from_email);
						}
					}
				} 
			}
		}
		exit;
	 }
	 
	function test_email_send() {
		$emailData = $this->get_edm_template(74);
		$emailId = 'tosifqureshi@cdnsol.com';
		$edm_id			  = encode(14);
		$user_id          = encode(246);
		$edm_gif_url      = SERVICES_URL_PATH."reward/edm_view?eid=$viewLogId";	
		$view_product    = URL_PATH."promotion/product-detail.php?eid=$edm_id&uid=$user_id&pr=";
		$view_more_url   = URL_PATH."promotion/index.php?eid=$edm_id&uid=$user_id";	
		/*This is hard code url for img need to dynamic*/
		$site_url 		  = "http://cdnsolutionsgroup.com/coyotev3/";
		$searchArray      = array("{#GIF_IMAGEURL#}","{#VIEW_MORE_PRODUCT#}","{#VIEW_PRODUCT#}","{#SITE_URL#}");
		$replaceArray     = array($edm_gif_url,$view_more_url,$view_product,$site_url);
		$emailBody	      = str_replace($searchArray, $replaceArray,$emailData->body);
		$this->_common->send_edm_mail($emailId,$emailData->subject,$emailBody);
	}
	 
	 /**
	  *@Insert OR Update to product review log
	  *@public
	  *@return json array
	  **/
	 public function edm_view_log_count($edmId,$userId,$productId,$email){
		if(!empty($edmId) && !empty($email) && !empty($productId)){ 
			$view_log_query    = "SELECT id FROM ".$this->edm_view_log." WHERE product_id ='".$productId."' AND email ='".$email."' AND edm_id = '".$edmId."'";
			$view_log_result   = $this->_db->my_query($view_log_query);
			$isviewLog 		   = ($this->_db->my_num_rows($view_log_result)>0)?TRUE:FALSE;
			if(empty($isviewLog)){//add
			$edm_view_query = "INSERT INTO ".$this->edm_view_log."(product_id,user_id,edm_id,email,view_count) VALUES('$productId','$userId','$edmId','$email','1')"; 
			$this->_db->my_query($edm_view_query);
			}else{// update 
			$view_log_data  = $this->_db->my_fetch_object($view_log_result);
			$view_log_id    = !empty($view_log_data->id)?$view_log_data->id:0;
				if($view_log_id){
					$edm_view_query = "UPDATE ".$this->edm_view_log." SET view_count = view_count + 1 WHERE id =".$view_log_id;
					$this->_db->my_query($edm_view_query);
				}
			}
			
		}					
	 }
	 
	 /**
	  *@edm_product for load all edm products
	  *@public
	  *@return json array
	  **/
	 public function edm_product(){
		$html = "";
		$product_count=0;
		$edm_queue_id = decode($_REQUEST['edmId']);
		$user_id	  = decode($_REQUEST['uId']);
		$isUser       = FALSE;
		if(!empty($user_id)){
			$User_exist_query  = "SELECT id FROM ".$this->user_table." WHERE id ='".$user_id."'ORDER BY id DESC LIMIT 1";
			$User_exist_result = $this->_db->my_query($User_exist_query);
			$isUser = ($this->_db->my_num_rows($User_exist_result)>0)?TRUE:FALSE;
		}		
		//if($isUser){	
		$query = "SELECT id,product_add_type,edm_sell_base_products_json,edm_products_json,product_top,product_bottom,product_other FROM ".$this->edm." WHERE id = '".$edm_queue_id."' ORDER BY id DESC LIMIT 1";
		$get_result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($get_result)>0){
				$edmproduct = $this->_db->my_fetch_object($get_result);
				
				$product_top 	= !empty($edmproduct->product_top)?json_decode($edmproduct->product_top):array();
				$product_bottom = !empty($edmproduct->product_bottom)?json_decode($edmproduct->product_bottom):array();
				$product_other  = !empty($edmproduct->product_other)?json_decode($edmproduct->product_other):array();
				$product_add_type  = !empty($edmproduct->product_add_type) ? $edmproduct->product_add_type : 0;
				$edm_sell_base_products_json = !empty($edmproduct->edm_sell_base_products_json) ? json_decode($edmproduct->edm_sell_base_products_json) : array();
				$edm_products_json  = !empty($edmproduct->edm_products_json) ? json_decode($edmproduct->edm_products_json) : array();
				$product_array = $edm_sell_base_products_json;
				if($product_add_type == 1) {
					$product_array = $edm_products_json;
				}
				
				if(!empty($product_array) && count($product_array) > 0){
					$product_count = count($product_array);
					//update edm view log 
					$imgcout=1;
					for($i=0;$i<count($product_array);$i++){
						$product_name  = $product_array[$i]->product_name;
						$product_id    = $product_array[$i]->product_id;
						$product_price = $product_array[$i]->product_price;
						$special_price = $product_array[$i]->special_price;
						$product_image = $product_array[$i]->product_image;
						$product_image_path = URL_PATH.'uploads/image_library/'.$product_image;
						$view_product_url = URL_PATH."promotion/product-detail.php?eid=".encode($edm_queue_id)."&uid=".encode($user_id)."&pr=".encode($product_id);
						$html .= "<div class='col-lg-3 col-md-6 mb-4'>
									<div class='card'>
									  <img class='card-img-top' src='$product_image_path' alt=''>
									  <div class='card-body'>
										 <h4 class='card-title'>$product_name</h4>
										 <div><table cellpadding='0' cellspacing='0' align='center' width='100%'><tbody><tr><td width='40%' style='text-align: left;font-size: 12px;letter-spacing: 1px;'>List Price</td><td width='' style='color: #a3a3a3;font-size: 20px;text-decoration: line-through;margin-right: 8px;text-align: right;font-weight: 600;padding-left: 0;padding-right:25px;letter-spacing: 0px;'>$product_price</td></tr><tr><td width='40%' style='text-align: left;font-size: 12px;letter-spacing: 1px;'>Special Price</td><td width='' style='color: #ffcb00;font-size: 32px;font-weight: 600;vertical-align: top;line-height: 30px;letter-spacing: -3px;padding-left: 0;text-align: right;padding-right:25px;'>$special_price</td></tr></tbody></table></div>
									  </div>
									  <div class='card-footer'>
										<a href='$view_product_url' class='add-to-cart btn btn-default'>Find Out More!</a>
									  </div>
								   </div>
								</div>";
						$imgcout++;
						if($imgcout == 5){
							$imgcout =1;
						}
					}
			  }
		   }
		//}else{
		//   $html = "<div class='error'><img src='img/404error.png'></div>";
		//}
		echo json_encode(array('html'=>$html,'product_count'=>$product_count));die;	
	 }

	     
	/**
	 *@edm_product_detail for load a edm products
	 *@public
	 *@return json array
	 **/
	 public function edm_product_detail(){
		$result 	  = FALSE;
		$html 	      = "<div class='error tac'><img src='img/404error.png'></div>";
		$edmId		  = (!empty($_REQUEST['edmId'])) ? decode($_REQUEST['edmId']) : 0;
		$queue_id	  = (!empty($_REQUEST['qid'])) ? decode($_REQUEST['qid']) : 0;
		$user_id	  = (!empty($_REQUEST['uId'])) ? decode($_REQUEST['uId']) : 0;
		$product_id	  = (!empty($_REQUEST['prId'])) ? decode($_REQUEST['prId']) : 0;
		
		if(!empty($edmId) && !empty($product_id)){
		
			// get user details
			if(!empty($user_id)) {
				$sql  = "SELECT id,email FROM ".$this->user_table." WHERE id ='".$user_id."' and role_id = 4";
				$result = $this->_db->my_query($sql);
				if($this->_db->my_num_rows($result) > 0){
					$user = $this->_db->my_fetch_object($result);
					$email = (!empty($user->email)) ? $user->email : '';
					$this->edm_view_log_count($edmId,$user_id,$product_id,$email);
				}
			} else if(!empty($queue_id)) {
				$sql  = "SELECT email FROM ".$this->edm_queue_log." WHERE id = $queue_id";
				$result = $this->_db->my_query($sql);
				if($this->_db->my_num_rows($result) > 0){
					$row = $this->_db->my_fetch_object($result);
					$email = (!empty($row->email)) ? $row->email : '';
					$this->edm_view_log_count($edmId,$user_id,$product_id,$email);
				}
			}
			
			// get product details
			$product_query    = "SELECT Prod_Desc,PROD_POS_DESC,Prod_Number FROM ".$this->PRODTBL." WHERE Prod_Number =$product_id";
			$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
			$product_count    =	$this->_mssql->mssql_num_rows_query($get_product_data);
			if($this->_mssql->mssql_num_rows_query($get_product_data) > 0){
				$ProductData = $this->_mssql->mssql_fetch_object_query($get_product_data);
				//$Prod_desc     = $result->Prod_Desc;
				//$PROD_POS_DESC = $result->PROD_POS_DESC;				
				$result 	   = TRUE;
			}

			$query = "SELECT id,product_add_type,edm_sell_base_products_json,edm_products_json,product_top,product_bottom,product_other FROM ".$this->edm." WHERE id = '".$edmId."' ORDER BY id DESC LIMIT 1";
			$get_result = $this->_db->my_query($query);
			$edmproduct = $this->_db->my_fetch_object($get_result);
				$product_top 	= !empty($edmproduct->product_top)?json_decode($edmproduct->product_top):array();
				$product_bottom = !empty($edmproduct->product_bottom)?json_decode($edmproduct->product_bottom):array();
				$product_other  = !empty($edmproduct->product_other)?json_decode($edmproduct->product_other):array();
				$product_add_type  = !empty($edmproduct->product_add_type) ? $edmproduct->product_add_type : 0;
				$edm_sell_base_products_json = !empty($edmproduct->edm_sell_base_products_json) ? json_decode($edmproduct->edm_sell_base_products_json) : array();
				$edm_products_json  = !empty($edmproduct->edm_products_json) ? json_decode($edmproduct->edm_products_json) : array();
				$product_array = $edm_sell_base_products_json;
				if($product_add_type == 1) {
					$product_array = $edm_products_json;
				}


			if(!empty($product_array) && count($product_array) > 0){
			$product_count = count($product_array);
			//update edm view log 
			$imgcout=1;
			for($i=0;$i<count($product_array);$i++){
			$pro_id        = $product_array[$i]->product_id;
			if($product_id == $pro_id ){
			$product_image = $product_array[$i]->product_image;
			$product_image = URL_PATH.'uploads/image_library/'.$product_image;
			}
			}
			}
		}
		echo json_encode(array('Product'=>(isset($ProductData)?$ProductData:""), 'product_image' => $product_image , 'result'=>$result,'html'=>$html));
		die;	
	 }    
	 
    /**
	 * edm_view for updating db,when edm has been opened via user
	 * @access 
	 * @return  status of mailsent(TRUE/FALSE)
	 **/	
	public function edm_view(){
	   //Begin the header output
		$edm_queue_id = $_REQUEST['eid'];
		if(!empty($edm_queue_id)){
		 $query = "UPDATE ".$this->edm_queue_log." SET is_read = 2 WHERE id =".decode($edm_queue_id);
		 $this->_db->my_query($query);
		}
		//Get the http URI to the image
		 $graphic_http = URL_PATH.'uploads/trans.gif';
		//Get the filesize of the image for headers
		 $filesize = filesize(ROOT.'/uploads/trans.gif');
		//Now actually output the image requested, while disregarding if the database was affected
		header( 'Content-Type: image/gif'); 
		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: private',false );
		header( 'Content-Disposition: attachment; filename="trans.gif"' );
		header( 'Content-Transfer-Encoding: binary' );
		header( 'Content-Length: '.$filesize );
		readfile( $graphic_http );
		//All done, get out!
		exit;
	}
    
    function remove_locking() {
		$member_id = $_REQUEST['member_id'];
		 $query = "delete from ava_kiosk_redeem_point_lock WHERE member_id = $member_id";
		 $this->_db->my_query($query);
	}
	
	function test() {
		// fetch kiosk setting details
		$kiosk_query  = "SELECT * FROM " . $this->kiosk_settings_tbl . " ";
		$kiosk_result = $this->_db->my_query($kiosk_query);
		if ($this->_db->my_num_rows($kiosk_result) > 0) {
			// fetch result object data
			$settings = $this->_db->my_fetch_object($kiosk_result);
			$employee_discount = (isset($settings->employee_discount)) ? $settings->employee_discount: 0;
			$normal_discount   = (isset($settings->normal_discount)) ? $settings->normal_discount: 0;
			$exclude_category  = (isset($settings->exclude_category)) ? $settings->exclude_category: '';
		}
		$_response['employee_discount'] = (isset($employee_discount)) ? $employee_discount : 0;
		$_response['normal_discount']   = (isset($normal_discount)) ? $normal_discount : 0;
		$_response['exclude_category']  = (isset($exclude_category)) ? $exclude_category : 0;
		return $_response;
		
	}
}
