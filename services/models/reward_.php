<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : Oct-05 03:09PM
 * Copyright : Coyote team
 *
 */
class reward {
	public $_response = array();
	public $result = array();
	private $reward_config_table = 'ava_reward_config';
	private $reward_transaction_table = 'ava_reward_transaction';
	private $reward_rule_table = 'ava_reward_rules';
	private $reward_rule_condition_table = 'ava_reward_rule_conditions';
	private $member_transaction_table = 'ava_cte_member_transaction_log';
	private $entity_master_table = 'ava_reward_entity_master';
	private $notification_tbl = 'ava_reward_notification';
    private $reward_catalogue = 'ava_reward_catalogue';
    private $reward_catalogue_products = 'ava_reward_catalogue_products';
    private $ava_stores_log = 'ava_stores_log';
    private $user_table = 'ava_users';
    private $notification_table = 'ava_reward_notification';
    private $users_contacts_tbl = 'ava_users_contacts';
    private $reward_campaign_tbl = 'ava_reward_campaign';
    private $reward_redeem_log_tbl = 'ava_reward_redeem_log';
    private $reward_campaign_log = 'ava_reward_campaign_log';
    private $reward_cards = 'ava_reward_cards';

	/**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db 		= env::getInst(); // -- Create Database Connection instance
		$this->_mssql	= new mssql(); // Create instance of mssql class
		$this->con   	= $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
        $this->_common  = new commonclass(); // Create instance of commonclass
        $this->_locale  = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); // -- Create push notifications instance
        $this->_config  = config::getInst();
        $this->reward_img_path = IMG_URL.'reward_images/'; // -- Set image path
    }
    
    /**
	 * Function used to send birthday push and manage reward points in cron
	 * @param null
	 * @return array
	 * @access public
	 *  
	 */ 
	public function send_birthday_reward_points() {
		// get birthday config
		$sql = "SELECT birthday_points FROM " . $this->reward_config_table . "";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
			$birthday_points = (!empty($row->birthday_points)) ? $row->birthday_points : 0;
			$members = array();
			// get app customers
			$user_query = "SELECT u.id,u.acc_number FROM ".$this->user_table." u WHERE u.active = 1 AND DATE_FORMAT(u.date_of_birth, '%m-%d') = '".date('m-d')."' AND ".date('Y')." NOT IN (SELECT YEAR(transaction_date) FROM ".$this->reward_transaction_table." WHERE transaction_type = 'birthday' and user_id = u.id)";
			$user_result = $this->_db->my_query($user_query);
			if ($this->_db->my_num_rows($user_result) > 0) {
				while($user_row = $this->_db->my_fetch_object($user_result)) {
					// set customer's params
					$user_id   = $user_row->id;
					//set user id in member array
					$members[] = $user_id;
				}
			}
			// send push and insert reward log
			$this->handle_birthday_notification($members,$birthday_points);
		}
		return true;
	}
	
	/**
	 * Function used to handle push and store procedure
	 * @param members(array),birthday_points(int),members_emails(array)
	 * @return void
	 * @access private
	 * 
	 */
     private function handle_birthday_notification($members,$birthday_points) {
		if(is_array($members) && count($members) > 0) {
			// push message
			$push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.birthday_note'),$birthday_points); 
			$reward_sql = '';
			$noti_where_sql = '';
			$datetime = date('Y-m-d H:i:s');
			// prepare sql multiple value string
			foreach($members as $key=>$val) {
				// prepare reward transaction where clause
				$reward_sql .= "('$val','$birthday_points','birthday',2,'$datetime')";
				$reward_sql .= (count($members) > 0 && count($members)-1 != $key) ? ',' : '';
				// prepare notification where clause
				$noti_where_sql .= "('".mysql_real_escape_string($push_message)."','birthday','$val','$datetime','$birthday_points')";
				$noti_where_sql .= (count($members) > 0 && count($members)-1 != $key) ? ',' : '';
			}
			// store members reward points
			if(!empty($reward_sql)) {
				$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date) VALUES $reward_sql";
				$this->_db->my_query($member_reward_sql);
				mysql_insert_id();
			}
			//  store notification records
			if(!empty($noti_where_sql)) {
				$notification_sql = "INSERT INTO ".$this->notification_table."(notification,notification_type,user_id,created_date,reward_point) VALUES $noti_where_sql";
				$this->_db->my_query($notification_sql);
				mysql_insert_id();
			}
		}
	}

    /*
     * @assign_order_purchase_points function is use to assign the reward points on purchase amount
     **/
    public function assign_order_purchase_points() {
        // get last reward point datetime for purchase type
		$sql = "SELECT transaction_date FROM ". $this->reward_transaction_table ." WHERE transaction_type = 'assign_order_purchase_points'";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
		}
        $transaction_date = (!empty($row->transaction_date)) ? date('Y-m-d H:i:s',strtotime($row->transaction_date)) : '';
		$current_datetime = date('Y-m-d H:i:s');
        $member_purchase = array();
		// set transaction start where clause
		$transaction_date_sql = (!empty($transaction_date)) ? "trx.transaction_datetime > '$transaction_date' AND" : "";
		// get member's purchase log
		$purchase_query = "SELECT SUM(trx.product_amount) as product_amount,u.id as user_id,u.email,u.device_token,u.device_type,card.reward_point_rate FROM ".$this->member_transaction_table." trx JOIN ".$this->user_table." u ON u.acc_number = trx.member_id JOIN ".$this->reward_cards." card ON card.id = u.reward_card_id WHERE $transaction_date_sql trx.transaction_datetime <= '$current_datetime' AND u.active = 1 AND u.deleted = 0 ORDER BY trx.member_id DESC";
        $purchase_result = $this->_db->my_query($purchase_query);
		if ($this->_db->my_num_rows($purchase_result) > 0) {
            // get data
			$purchase_row = $this->_db->my_fetch_object($purchase_result);
            $user_id = (!empty($purchase_row->user_id)) ? $purchase_row->user_id : 0;
            $email = (!empty($purchase_row->email)) ? $purchase_row->email : '';
            $product_amount = (!empty($purchase_row->product_amount)) ? $purchase_row->product_amount : 0;
            $device_token = (!empty($purchase_row->device_token)) ? $purchase_row->device_token : '';
            $device_type = (!empty($purchase_row->device_type)) ? $purchase_row->device_type : '';
            $reward_point_rate = (!empty($purchase_row->reward_point_rate)) ? $purchase_row->reward_point_rate : 0;
            
            //calculate reward point
            $user_reward_point = (($product_amount)*($reward_point_rate));
            // members reward points
			if(!empty($user_reward_point)) {
				$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date) VALUES ('".$user_id."','".$user_reward_point."','assign_order_purchase_points','1','".$current_datetime."')";
				$this->_db->my_query($member_reward_sql);
				mysql_insert_id();
                
                $notification_sql = "INSERT INTO ".$this->notification_table."(notification,notification_type,user_id,created_date) VALUES ('Congrates! On purchasing from Nightowl app you have earned the ".$user_reward_point." reward points!. ','assign_order_purchase_points','".$user_id."','".$current_datetime."')";
				$this->_db->my_query($notification_sql);
                
			}
		}
        return true;
    } //assign_order_purchase_points
    
    
    /**
	 * Function used to manage users purchase reward points in cron
	 * @param null
	 * @return void
	 * @access public
	 *  
	 */ 
	public function send_order_purchase_points() {
		 
		// get last reward point datetime for purchase type
		$sql = "SELECT transaction_date FROM ". $this->reward_transaction_table ." WHERE transaction_type = 'make_purchase_points'";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
		}
		$transaction_date = (!empty($row->transaction_date)) ? date('Y-m-d H:i:s',strtotime($row->transaction_date)) : '';
		$current_date = date('Y-m-d');
		// get members transaction data in between dates
		$member_purchase = $this->manage_member_transaction($transaction_date);
		// get reward rule details whos not expired
		$rule_query  = "SELECT id,rule_name,reward_points FROM ".$this->reward_rule_table." WHERE status = '1' AND is_deleted = '0' AND (start_date <= '".$current_date."' AND (end_date >= '".$current_date."' OR end_date = '')) ORDER BY created_at ASC" ;
		$rule_result = $this->_db->my_query($rule_query);
		if ($this->_db->my_num_rows($rule_result) > 0) {
			while($rule_row = $this->_db->my_fetch_object($rule_result)) {
				// set rule params
				$rule_id = (!empty($rule_row->id)) ? $rule_row->id : '';
				$rule_name = (!empty($rule_row->rule_name)) ? $rule_row->rule_name : '';
				$reward_points = (!empty($rule_row->reward_points)) ? $rule_row->reward_points : 0;
				// get rule conditions
				$condition_query = "SELECT c.id,c.entity_value,c.reward_entity as entity_id,e.reward_entity,e.entity_key FROM ".$this->reward_rule_condition_table." c JOIN ".$this->entity_master_table." e ON e.id = c.reward_entity  WHERE c.rule_id = $rule_id ORDER BY c.created_at ASC";
				$condition_result = $this->_db->my_query($condition_query);
				if($this->_db->my_num_rows($condition_result) > 0) {
					$condition_array = array();
					$members_condition_array = $member_purchase;
					$reward_member_array = array();
					while($condition_row = $this->_db->my_fetch_object($condition_result)) {
						// set condition params
						$condition['reward_entity'] = (!empty($condition_row->reward_entity)) ? $condition_row->reward_entity : '';
						$condition['entity_key']    = (!empty($condition_row->entity_key)) ? $condition_row->entity_key : '';
						$condition['entity_value']  = (!empty($condition_row->entity_value)) ? $condition_row->entity_value : 0;
						$condition_array[] = $condition;
						
						// compare rule conditions for member purchase history
						foreach($members_condition_array as $key=>$val) {
							switch($condition['entity_key']) {
								case 'total_amount' :
									if($val[$condition['entity_key']] < $condition['entity_value']) {
										unset($members_condition_array[$key]);
									}
								break;
								case 'total_qty' :
									if($val[$condition['entity_key']] < $condition['entity_value']) {
										unset($members_condition_array[$key]);
									}
								break;
								case 'product_ids' :
									$purchased_products = $val['product_ids'];
									$purchased_products = array_filter(explode(',',$purchased_products));
									$purchased_product_count = count($purchased_products);
									$conditional_products = $condition['entity_value'];
									$conditional_products = array_filter(explode(',',$conditional_products));
									// get products matching count
									$matched_count = count(array_intersect($purchased_products, $conditional_products));
									if($matched_count != $purchased_product_count) {
										unset($members_condition_array[$key]);
									}
								break;
								default:
									unset($members_condition_array[$key]);
								break;	
							}
						}
					}
					if(!empty($members_condition_array) && count($members_condition_array) > 0) {
						// insert reward data and send notification
						$this->insert_reward_data($members_condition_array,$rule_id,$rule_name,$reward_points);
					}
				}
			}
		}
	}
	
    /**
	 * Function used to manage users transaction data
	 * @param transaction_date(string)
	 * @return array
	 * @access private
	 * 
	 */
     private function manage_member_transaction($transaction_date) {
		$current_datetime = date('Y-m-d H:i:s');
		$member_purchase = array();
		// set transaction start where clause
		$transaction_date_sql = (!empty($transaction_date)) ? "trx.transaction_datetime > '$transaction_date' AND" : "";
		// get member's purchase log
		$purchase_query = "SELECT trx.member_id,trx.product_id,trx.product_qty,trx.product_amount,trx.transaction_datetime,u.id as user_id,u.acc_number,u.device_token,u.device_type FROM ".$this->member_transaction_table." trx JOIN ".$this->user_table." u ON u.acc_number = trx.member_id WHERE $transaction_date_sql trx.transaction_datetime <= '$current_datetime' AND u.active = 1 AND u.deleted = 0 ORDER BY trx.member_id DESC";
		$purchase_result = $this->_db->my_query($purchase_query);
		if ($this->_db->my_num_rows($purchase_result) > 0) {
			$member_purchase = array();
			while($purchase_row = $this->_db->my_fetch_object($purchase_result)) {
				// set member id
				$member_id = $purchase_row->user_id;
				// set purchase product data  
				$member_purchase[$member_id]['product_ids'] .= $purchase_row->product_id.',';
				$member_purchase[$member_id]['total_qty']    = $member_purchase[$member_id]['product_count']+$purchase_row->product_qty;
				$member_purchase[$member_id]['total_amount'] = $member_purchase[$member_id]['product_amount']+$purchase_row->product_amount;
				$member_purchase[$member_id]['transaction_datetime'] = $purchase_row->transaction_datetime;
				$member_purchase[$member_id]['acc_number']   = $purchase_row->acc_number;
				$member_purchase[$member_id]['device_token'] = $purchase_row->device_token;
				$member_purchase[$member_id]['device_type']  = $purchase_row->device_type;
			}
		}
		return $member_purchase;
	 }
	 
	/**
	 * Function used to store users reward transaction data
	 * @param members_condition_array(array),rule_id(int),rule_name(string)
	 * @return void
	 * @access private
	 * 
	 */
     private function insert_reward_data($members_condition_array,$rule_id,$rule_name,$reward_points) {
		$android_devices = array();
		$ios_devices = array();
		$reward_sql = '';
		$noti_where_sql = '';
		// prepare sql multiple value string
		$i=0;
		foreach($members_condition_array as $key=>$val) {
			$datetime = $val['transaction_datetime'];
			$comments = json_encode(array('rule_id'=>$rule_id,'rule_name'=>$rule_name,'acc_number'=>$val['acc_number']));
			$reward_sql .= "('$key','$reward_points','make_purchase_points',2,'$datetime','$comments')";
			$reward_sql .= (count($members_condition_array) > 0 && count($members_condition_array)-1 != $i) ? ',' : '';
			// set customer's device param
			$device_type = $val['device_type'];
			$device_id   = trim($val['device_token']);
			if(!empty($device_id)) {
				if($device_type == 'android'){
					// set andriod device token in array
					$android_devices[] = $device_id;
				}else{
					// set ios device token in array
					$ios_devices[] = $device_id;
				}
			}
			// set notification message
			$notification_message = sprintf($this->_common->langText($this->_locale,'txt.reward.order_purchase_points_msg'),$reward_points);
			$currentdatetime = date('Y-m-d H:i:s');
			// store notification record
			$noti_where_sql .= "('$notification_message','make_purchase_points','$key','$currentdatetime')";
			$noti_where_sql .= (count($members_condition_array) > 0 && count($members_condition_array)-1 != $i) ? ',' : '';
			$i++;
		}
		// store members reward points
		if(!empty($reward_sql)) {
			$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date,comments) VALUES $reward_sql";
			$this->_db->my_query($member_reward_sql);
			mysql_insert_id();
		}
		// store notification records
		if(!empty($noti_where_sql)) {
			$notification_sql = "INSERT INTO ".$this->notification_table."(notification,notification_type,user_id,created_date) VALUES $noti_where_sql";
			$this->_db->my_query($notification_sql);
			mysql_insert_id();
		}
		// send push notification
		if(!empty($android_devices) || !empty($ios_devices)) {
			$this->send_push_notification($android_devices,$ios_devices,$this->_common->langText($this->_locale,'txt.reward.order_purchase_points_push_msg'));
		}
		return true;
	 }
	 
	/**
	 * Function used to send reward push notification
	 * @param android_devices(array),ios_devices(array),push_message(string)
	 * @return bool
	 * @access private
	 * 
	 */
     private function send_push_notification($android_devices,$ios_devices,$push_message) {
		// set unique device ids array
		$android_devices = array_unique($android_devices);
		// reset device ids array
		$android_devices = $this->device_array_keys($android_devices);
		// send IOS push
		$this->_push->send_ios_reward_push($ios_devices,$push_message);
		// send andriod push 
		$this->_push->send_android_reward_push($android_devices,$push_message);
		return true;
	 }
    
    /**
     * @view_notification_list is list the all unread notifications to the users
     * @input user_id
     * @output array
     */
    public function view_notification_list(){
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $table = $this->notification_tbl; 
        $columns = '';
        $where = ['user_id'=>$user_id,'is_deleted'=>0];
        $orderby = 'created_date';
        $get_record = $this->_common->get_table_record($table,$columns,$where,$orderby);
        $_response['result_code'] = 1;
        if(!empty($get_record)) {
            $_response['result'] = $get_record;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_success');
        } else {
            $_response['result'] = '';
            $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
        return $_response;
    }
    
    /**
     *@mark_notification_read is used to mark read the notification
     *@input user_id(int) notification_id(int)
     *@output array
     */
    public function mark_notification_read(){
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $notification_id = (!empty($_REQUEST['notification_id'])) ? $_REQUEST['notification_id'] : 0;
        if(!empty($user_id) && !empty($notification_id)) {
            $table = $this->notification_tbl; 
            $columns = '';
            $where = ['user_id'=>$user_id,'id'=>$notification_id];
            if($notification_id == 'all') { // notification_id =  all means mark all notification read
                $where = ['user_id'=>$user_id];
            }
            $get_record = $this->_common->get_table_record($table,$columns,$where);
            $_response['result_code'] = 1;
            if(!empty($get_record)) {
                $updateArray = array("is_read" => 1);
                $whereClause = ['user_id'=>$user_id,'id'=>$notification_id];
                if($notification_id == 'all') {
                    $whereClause = ['user_id'=>$user_id];
                }
                $update = $this->_db->UpdateAll($this->notification_tbl, $updateArray, $whereClause, "");
                $_response['result'] = $get_record;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_success');
            } else {
                $_response['result'] = '';
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    }
    
    /**
     *@delete_notification is used to mark read the notification
     *@input user_id(int) notification_id(int)
     *@output array
     */
    public function delete_notification(){
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $notification_id = (!empty($_REQUEST['notification_id'])) ? $_REQUEST['notification_id'] : 0;
        if(!empty($user_id) && !empty($notification_id)) {
            $table = $this->notification_tbl; 
            $columns = '';
            $where = ['user_id'=>$user_id,'id'=>$notification_id,'is_deleted'=>0];
            if($notification_id == 'all') { // notification_id =  all means mark all notification read
                $where = ['user_id'=>$user_id];
            }
            $get_record = $this->_common->get_table_record($table,$columns,$where);
            $_response['result_code'] = 1;
            if(!empty($get_record)) {
                $updateArray = array("is_deleted" => 1);
                $whereClause = ['user_id'=>$user_id,'id'=>$notification_id];
                if($notification_id == 'all') {
                    $whereClause = ['user_id'=>$user_id];
                }
                $update = $this->_db->UpdateAll($this->notification_tbl, $updateArray, $whereClause, "");
                $_response['result'] = $get_record;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_deleted');
            } else {
                $_response['result'] = '';
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    }
    
    /**
     *@get_reward_catalogue is use the list the reward catalogues
     *@input : order(int)
     *@output : array
     */
    public function get_reward_catalogue() {
        $date = date('Y-m-d');
        // prepare the range data
        $ranges = ['0-500','500-1000','1000-2000','2000-5000','5000-10000','10000 >'];
        // get request parameters
        $sort = (!empty($_REQUEST['sort'])) ? $_REQUEST['sort'] : 1; // 1 : high to low, 2 : low to high
        $store_id = (!empty($_REQUEST['store_id'])) ? $_REQUEST['store_id'] : 0; // sotre_id
        $department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0; // sotre_id
        $range_id = (!empty($_REQUEST['range_id'])) ? $_REQUEST['range_id'] : 0; // sotre_id
        $sort = $this->_common->test_input($sort);
        $sortorder = 'DESC';
        
        //apply the filter condition for store, department and range
        if($sort == 2) {
            $sortorder = 'ASC';
        }
        $store_cond = '';
        if(!empty($store_id)) {
            $store_cond = ' AND product.ms_store_id = '.$store_id.' ';
        }
        $department_cond = '';
        if(!empty($department_id)) {
            $department_cond = ' AND product.department_id = '.$department_id.' ';
        }
        $range_cond = '';
        if(!empty($range_id)) {
            if($range_id == 1) {
                $range_cond = ' AND product.redeem_points <= 500 ';
            }
            if($range_id == 2) {
                $range_cond = ' AND (product.redeem_points >= 500 AND product.redeem_points <= 1000) ';
            }
            if($range_id == 3) {
                $range_cond = ' AND (product.redeem_points >= 1000 AND product.redeem_points <= 2000) ';
            }
            if($range_id == 4) {
                $range_cond = ' AND (product.redeem_points >= 2000 AND product.redeem_points <= 5000) ';
            }
            if($range_id == 5) {
                $range_cond = ' AND (product.redeem_points >= 5000 AND product.redeem_points <= 10000) ';
            }
            if($range_id == 5) {
                $range_cond = ' AND product.redeem_points >= 10000 ';
            }
        }
        $result_array    =   array();
        //prepare the filter data
        //$filter_query = "SELECT stores.ms_store_id,stores.store_name,product.department_id,product.department_name FROM ".$this->reward_catalogue_products." product JOIN ".$this->ava_stores_log." stores ON stores.ms_store_id = product.ms_store_id WHERE product.status = 1 ";
        $filter_query = "SELECT stores.ms_store_id,stores.store_name,product.department_id,product.department_name FROM ".$this->reward_catalogue_products." product JOIN ".$this->ava_stores_log." stores ON FIND_IN_SET(stores.ms_store_id, product.ms_store_id) > 0 WHERE product.status = 1 GROUP BY stores.store_name";
        $filter_result = $this->_db->my_query($filter_query);
        $store_array = array();
        $department_array = array();
        $store_temp_array = array();
        $department_temp_array = array();
        while($filter_row = $this->_db->my_fetch_object($filter_result)) {
            // collect the store filter
            $ms_store_id = (!empty($filter_row->ms_store_id)) ? $filter_row->ms_store_id : 0;
            $store_name = (!empty($filter_row->store_name)) ? $filter_row->store_name : "NA";
            if(!in_array($ms_store_id,$store_temp_array)) {
                $store['id'] = $ms_store_id;
                $store['name'] = $store_name;
                $store_temp_array[] = $ms_store_id;
                $store_array[] = $store;
            }
            // collect the deaprtment filter
            $department_id = (!empty($filter_row->department_id)) ? $filter_row->department_id : 0;
            $department_name = (!empty($filter_row->department_name)) ? $filter_row->department_name : "NA";
            if(!in_array($department_id,$department_temp_array)) {
                $department['id'] = $department_id;
                $department['name'] = $department_name;
                $department_temp_array[] = $department_id;
                $department_array[] = $department;
            }
        }
        $result_array['store_filter'] = $store_array;
        $result_array['department_filter'] = $department_array;
        
        // get data from db
        $query = "SELECT catalogue.title,catalogue.banner_image as cat_image,product.* FROM ".$this->reward_catalogue." catalogue JOIN ".$this->reward_catalogue_products." product ON product.catalogue_id = catalogue.id WHERE catalogue.status = 1 AND product.status = 1 ".$store_cond.$department_cond.$range_cond."AND (product.start_date <= '".$date."' AND product.end_date >= '".$date."') ORDER BY product.redeem_points ".$sortorder;
        $result = $this->_db->my_query($query);
        $_response['result_code'] = 1;
        $result_array['ranges'] = $ranges;
        $result_array['products'] = array();
        if ($this->_db->my_num_rows($result) > 0) {
            while($row = $this->_db->my_fetch_object($result)) {
                $result_array['catalogue_title'] = (!empty($row->title)) ? $row->title : '';
                $result_array['catalogue_image'] = (!empty($row->cat_image)) ? IMG_URL.'reward_images'.DS.$row->cat_image : '';
                
                $product['id'] = (!empty($row->id)) ? $row->id : '';
                $product['product_title'] = (!empty($row->product_title)) ? $row->product_title : '';
                $product['banner_image'] = (!empty($row->banner_image)) ? IMG_URL.'reward_images'.DS.$row->banner_image : '';
                $ms_store_id = (!empty($row->ms_store_id)) ? $row->ms_store_id : 0;
                $exp_ms_store_id = explode(',',$ms_store_id);
                if(!empty($exp_ms_store_id)){
                    foreach($exp_ms_store_id as $store_id){
                        $store_data = $this->get_store_data($store_id);
                        $product['store'][] = $store_data;
                    }
                }

                //$product['store_name'] = (!empty($row->store_name)) ? $row->store_name : '';
                $product['department_name'] = (!empty($row->department_name)) ? $row->department_name : '';
                $product['product_name'] = (!empty($row->product_name)) ?  trim(preg_replace('/\s+/',' ', $row->product_name)) : '';
                $product['redeem_points'] = (!empty($row->redeem_points)) ? $row->redeem_points : 0;
                $product['add_extra_price'] = (!empty($row->add_extra_price)) ? $row->add_extra_price : 0;
                $product['pay_amount_with_redeem_points'] = (!empty($row->redeem_points_with_extra_price)) ? $row->redeem_points_with_extra_price : 0;
                $product['extra_price'] = (!empty($row->extra_price)) ? $row->extra_price : 0;
                $product['start_date'] = (!empty($row->start_date)) ? $row->start_date : 0;
                $product['short_description'] = (!empty($row->short_description)) ? $row->short_description : 0;
                $product['long_description'] = (!empty($row->long_description)) ? $row->long_description : 0;
                $result_array['products'][] = $product;
            }
        }
        $_response['result']    =   $result_array;
        return $_response;
    }
    
    public function get_store_data($ms_store_id=0) {
		$query = "SELECT * FROM " . $this->ava_stores_log . " WHERE ms_store_id = '$ms_store_id'";
        $result = $this->_db->my_query($query);
        $row = $this->_db->my_fetch_object($result);
        
        $lat2 				= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
        $lng2 				= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
        $distance 			= $this->distance($lat1, $lng1, $lat2, $lng2, $unit);
        $array['id'] 		= (isset($row->store_id)) ? trim($row->store_id) : "";
        $array['ms_store_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
        $array['name'] 		= (isset($row->store_name)) ? trim($row->store_name) : ""; 
        $array['outlet_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
        $array['addr_1'] 	= (isset($row->store_address_1)) ? trim($row->store_address_1) : "";
        $array['addr_2'] 	= "";
        $array['addr_3'] 	= $array['addr_1']." ".$array['addr_2'];
        $array['post_code'] = (isset($row->store_post_code)) ? trim($row->store_post_code) : "";
        $array['phone_no'] 	= "07 ".(isset($row->store_phone)) ? trim($row->store_phone) : "";
        $array['lat'] 		= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
        $array['lng'] 		= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
        $array['distance'] 	= strval(round($distance,2));
        $array['open_hours'] = (isset($row->open_hours)) ? trim($row->open_hours) : "";
        return $array;
    }
    
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return ($miles * 1.609344);
		}
	}
    
	/**
	 * Function used to manage device array keys
	 * @param array device_ids
	 * @return void
	 * @access private
	 * 
	 */
     private function device_array_keys($device_ids='') {
		$device_array = array();
		if(!empty($device_ids)) {
			foreach($device_ids as $key=>$device_id) {
				$device_array[] = $device_id;
			}
		}
		return $device_array;
	 }
	 
	/**
	 * Function used to manage users refferal's purchase points in cron
	 * @param null
	 * @return void
	 * @access public
	 *  
	 */ 
	public function send_refferals_first_purchase_points() {
		 
		// get all users whos refferal make purchase
		$sql = "SELECT u.id,u.email,u.acc_number,u.my_reference_code,u.device_token,u.device_type,(SELECT referral_purchase_points FROM ".$this->reward_config_table." LIMIT 1) AS referral_purchase_points,(SELECT id FROM ".$this->user_table." WHERE my_reference_code = u.reference_code) AS referred_by FROM ".$this->user_table." u WHERE u.my_reference_code NOT IN ( SELECT comments FROM ". $this->reward_transaction_table ." WHERE transaction_type = 'referral_first_purchase' AND comments = u.my_reference_code ) AND  u.reference_code != '' AND acc_number != ''";
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$android_devices = array();
			$ios_devices = array();
			while($row = $this->_db->my_fetch_object($result)) {
				// set members data
				$user_id = (!empty($row->id)) ? $row->id : 0;
				$referred_by = (!empty($row->referred_by)) ? $row->referred_by : 0;
				$acc_number = (!empty($row->acc_number)) ? $row->acc_number : 0;
				$reward_points = (!empty($row->referral_purchase_points)) ? $row->referral_purchase_points : 0;
				$my_reference_code = (!empty($row->my_reference_code)) ? $row->my_reference_code : 0;
				// get members purchase record
				$member_trx_query = "SELECT top 1 JNAH_TRX_NO as trx_no FROM JNLH_Account_TBL JOIN JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO JOIN Member on MEMB_NUMBER = JNAD_PRODUCT WHERE MEMB_Exclude_From_Competitions = '0' AND JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' AND JNAD_PRODUCT = $acc_number";
				$member_trx_data = $this->_mssql->my_mssql_query($member_trx_query,$this->con);
				if($this->_mssql->mssql_num_rows_query($get_member_data) > 0 ) {
					$datetime = date('Y-m-d H:i:s');
					// store referel reward points
					$member_reward_sql = "INSERT INTO ".$this->reward_transaction_table."(user_id,balance_point,transaction_type,status,transaction_date,comments) VALUES ('$referred_by','$reward_points','referral_first_purchase',2,'$datetime','$my_reference_code')";
					$this->_db->my_query($member_reward_sql);
					mysql_insert_id();
					// set customer's device param
					$device_type = $row->device_type;
					$device_id   = trim($row->device_token);
					if(!empty($device_id)) {
						if($device_type == 'android'){
							// set andriod device token in array
							$android_devices[] = $device_id;
						}else{
							// set ios device token in array
							$ios_devices[] = $device_id;
						}
					}
					// set notification message
					$push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.referral_purchase_points'),$reward_points);
					// store notification record
					$notification_data = ['user_id'=>$referred_by,'notification'=>$push_message,'notification_type'=>'referral_first_purchase','is_send'=>1];
					$this->_common->save_notification($notification_data);
				}
			}
			// send push notification
			if(!empty($android_devices) || !empty($ios_devices)) {
				$this->send_push_notification($android_devices,$ios_devices,$this->_common->langText($this->_locale,'txt.reward.referral_purchase_points_push_msg'));
			}
		}
		return true;
	}
    
    /*
     * @sendNotification is use to send notification email and push to the users 
     * @input null
     * @output void
     **/
    
    public function sendNotification() {
        // set default response
        $_response['result_code'] = 0;
        $_response['result'] = array();
        //get user to whome send notification will send
        $query = "SELECT ntf.*,user.email,user.firstname,user.lastname,user.device_token,user.device_type FROM ".$this->notification_tbl." ntf JOIN ".$this->user_table." user ON user.id = ntf.user_id WHERE ntf.is_send = 0 ORDER BY ntf.created_date DESC";
        $result = $this->_db->my_query($query);      
        if ($this->_db->my_num_rows($result) > 0) {
            while($row = $this->_db->my_fetch_array($result)) {
                //get result data
                $id = (!empty($row['id'])) ? $row['id'] : '';
                $notification = (!empty($row['notification'])) ? $row['notification'] : '';
                $notification_type = (!empty($row['notification_type'])) ? $row['notification_type'] : '';
                $user_id = (!empty($row['user_id'])) ? $row['user_id'] : '';
                $email = (!empty($row['email'])) ? $row['email'] : '';
                $firstname = (!empty($row['firstname'])) ? $row['firstname'] : '';
                $lastname = (!empty($row['lastname'])) ? $row['lastname'] : '';
                $device_token = (!empty($row['device_token'])) ? $row['device_token'] : '';
                $device_type = (!empty($row['device_type'])) ? $row['device_type'] : '';
                $reward_point = (!empty($row['reward_point'])) ? $row['reward_point'] : '';
                
                // check empty email & notification type and then send email 
                if(!empty($notification_type) && !empty($email)) {
                    //prepare email body
                    $username = $firstname.' '.$lastname;
                    $username = ucwords($username);
                    $emailBody = $username.'{#STRINGBREAKER#}'.$notification.'{#STRINGBREAKER#}'.$reward_point;
                    // preapare the array for email
                    $emailData = ['emailBody'=>$emailBody,'receiverId'=>$email,'purpose'=>$notification_type];
                   //call function to send email
                    $this->_common->send_email($emailData);
                } 
                // check empty device_token & device_type and then push
                if(!empty($device_token) && !empty($device_type)) {
                    if($device_type == 'android') {     // send andriod push 
                        $this->_push->send_android_reward_push($device_token,$notification);
                    } else if ($device_type == 'ios') { // send IOS push
                        $this->_push->send_ios_reward_push($device_token,$notification);
                    }
                }
                //update user notification, that email and push has send to user
                $updateArray = array("is_send" => 1);
                $whereClause = ['user_id'=>$user_id,'id'=>$id];
                $update = $this->_db->UpdateAll($this->notification_tbl, $updateArray, $whereClause, "");

            }
        }
        return $_response;
    } // end sendNotification
    
    /**
     *  @addRewardPoints is use to insert/update reward points on user's different activities
     *  @input user_id(int) reward_type(string)
     *  @output void
     */
    public function addRewardPoints() {
        // get request parameters
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $reward_type = (!empty($_REQUEST['reward_type'])) ? $_REQUEST['reward_type'] : '';
        // check required parameters
        if(!empty($user_id) && !empty($reward_type)) {
            //check reward point exist or not
            $reward_existance = $this->_common->get_reward_config($reward_type);
            if($reward_existance != 'Not valid') {
                // get reward point w.r.t. reward_type
                $table = $this->reward_config_table; 
                $columns = [$reward_type];
                $get_record = $this->_common->get_table_record($table,$columns);
                $reward_points = (!empty($get_record[0]->$reward_type)) ? $get_record[0]->$reward_type : 0;
                if(!empty($reward_points)) {
                    $record_transaction_query = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                        '".mysql_real_escape_string($user_id)."',
                        '".mysql_real_escape_string($reward_points)."',
                        '".mysql_real_escape_string($reward_type)."',
                        '".date('y-m-d H:i:s')."',
                        '2')";
                    $this->_db->my_query($record_transaction_query);
                    $reward_message = $this->_common->get_reward_message($reward_type);
                    $notification_data = ['user_id'=>$user_id,'notification'=>$reward_message,'notification_type'=>$reward_type];
                    $this->_common->save_notification($notification_data);
                }
                $_response['result_code'] = 1;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.success.add_reward_points');  
            } else {
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.error.add_reward_points');   
            }
        } else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } //end addRewardPoints
    
    /*
     * @contactSynchronisation is use to sync the user contact list
     * @input user_id(int) contact_list(json) 
     * @output void
     **/
    public function contactSynchronisation() {
        // get request parameters
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $contact_list = (!empty($_REQUEST['contact_list'])) ? json_decode($_REQUEST['contact_list']) : 0;
        // check required parameters
        if(!empty($user_id) && !empty($contact_list)) {
            
            //award reward point to user on contact sync
            $reward_points_type = 'contact_synchronisation_points';
            $today_date = CURRENT_DATE;
            $reward_campaign_sql = "SELECT id,reward_points FROM ".$this->reward_campaign_tbl." where status = '1' AND is_deleted = '0' AND action_type = '3' AND start_date <= '".$today_date."' AND end_date >= '".$today_date ."'  " ;
            $get_reward_campaign = $this->_db->my_query($reward_campaign_sql);
            $reward_campaign = $this->_db->my_fetch_object($get_reward_campaign);
            
            $campaign_id = (!empty($reward_campaign->id)) ? $reward_campaign->id : 0;
            $reward_points = (!empty($reward_campaign->reward_points)) ? $reward_campaign->reward_points : 0;
            
            //check camapaign has reward adn can be assign to user or not
            $is_reward_assign = $this->_common->manageCompaignLog($user_id,$campaign_id);
            if(!empty($is_reward_assign)) {
                $record_transaction_query = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                    '".mysql_real_escape_string($user_id)."',
                    '".mysql_real_escape_string($reward_points)."',
                    '".mysql_real_escape_string($reward_points_type)."',
                    '".date('y-m-d H:i:s')."',
                    '2')"; // 2 received or success
                    $this->_db->my_query($record_transaction_query);

                // save notification
                $push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.contact_synchronisation_points'),$reward_points); 
                
                $notification_data = ['user_id'=>$user_id,'notification'=>$push_message,'notification_type'=>$reward_points_type];
                $this->_common->save_notification($notification_data);                     
            }
            foreach($contact_list as $contact) {
                if(!empty($contact)) {
                    
                    //check user syncronise contact fist time
                    /* $table1 = $this->users_contacts_tbl; 
                    $columns1 = ['id'];
                    $where1 = ['user_id'=>$user_id];
                    $get_record1 = $this->_common->get_table_record($table1,$columns1,$where1);*/
                    //if users_contacts table is empty
                    //if(empty($get_record1)) {
                        
                    //}
                    
                    $friend_name = (!empty($contact->friend_name)) ? $contact->friend_name : '';
                    $friend_mobile_no = (!empty($contact->friend_mobile_no)) ? $contact->friend_mobile_no : '';
                    $friend_email_address = (!empty($contact->friend_email_address)) ? $contact->friend_email_address : '';
                    
                    //check mobile no exist in user contact list
                    $table = $this->users_contacts_tbl; 
                    $columns = ['id'];
                    $where = ['user_id'=>$user_id,'friend_mobile_no'=>$friend_mobile_no];
                    $get_record = $this->_common->get_table_record($table,$columns,$where);
                    // if mobile no exits in user contact list list then update the information
                    if(!empty($get_record)) {
                        $contact_list_id = (!empty($get_record[0]->id)) ? $get_record[0]->id : 0;
                        $updateArray = ["friend_name" => $friend_name,"friend_email_address" => $friend_email_address,"modified_date" => date('y-m-d H:i:s')];
                        $whereClause = ['id'=>$contact_list_id];
                        $update = $this->_db->UpdateAll($this->users_contacts_tbl, $updateArray, $whereClause, "");
                    } else {
                        //if mobile not exist in contact list then insert in contact list
                        $insert_query = "INSERT INTO ".$this->users_contacts_tbl." (user_id,friend_name,friend_mobile_no,friend_email_address,created_date) VALUES(
                            '".mysql_real_escape_string($user_id)."',
                            '".mysql_real_escape_string($friend_name)."',
                            '".mysql_real_escape_string($friend_mobile_no)."',
                            '".mysql_real_escape_string($friend_email_address)."',
                            '".date('y-m-d H:i:s')."')";
                        $this->_db->my_query($insert_query);
                    }
                }
            }
            $_response['result_code'] = 1;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.success.contact_synchronisation');
        } else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } //end contactSynchronisation
    
    /*
     * @contactList is listing all contact list
     * @input user_id(int)
     * @output void
     **/
    public function contactList() {
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;  
        if(!empty($user_id)) {
            // get contact list from table
            $table = $this->users_contacts_tbl; 
            $columns = [];
            $where = ['user_id'=>$user_id];
            $get_record = $this->_common->get_table_record($table,$columns,$where);
            $response_data['user_id'] = $user_id;
            // check record exist or not
            if(!empty($get_record)) {
                foreach($get_record as $record){
                    $result['friend_name'] = (!empty($record->friend_name)) ? $record->friend_name : '';
                    $result['friend_mobile_no'] = (!empty($record->friend_mobile_no)) ? $record->friend_mobile_no : '';
                    $result['friend_email_address'] = (!empty($record->friend_email_address)) ? $record->friend_email_address : '';
                    $response_data[] = $result;
                }
                $_response['result_code'] = 1;
                $_response['result_data'] = $response_data;
            } else {
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');   
            }
        }  else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } // end contactList
    
    /*
     * @contactSynchronisation is use to sync the user contact list
     * @input user_id(int) contact_list(json) 
     * @output void
     **/
    public function contactSynchronisation1() {
        // get request parameters
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $phone = (!empty($_REQUEST['phone'])) ? $_REQUEST['phone'] : '';
        $email = (!empty($_REQUEST['email'])) ? $_REQUEST['email'] : '';
         // check required parameters
        if(!empty($user_id)) {
            // define empty user_ids array
            $user_ids = array();
            $response_data = array();
            
            //explode phone no and email id into array
            $phone_array = explode(",", $phone); 
            $email_array = explode(",", $email); 
            
            //check phone exist into database
            if(!empty($phone_array)) {
                foreach($phone_array as $phone_no) {
                    if(!empty($phone_no)) {
                        $get_user_id = $this->checkMobileNo($phone_no);
                        if($get_user_id){
                            array_push($user_ids,$get_user_id);
                        }
                    }
                }
            }
            
            //check email address exist into database
            if(!empty($email_array)) {
                foreach($email_array as $email_id) {
                    if(!empty($email_id)) {
                        $get_user_id = $this->checkEmailAddress($email_id);
                        if($get_user_id){
                            array_push($user_ids,$get_user_id);
                        }
                    }
                }
            }
            
            //remove duplicate user ids
            $user_ids = array_unique($user_ids);
            if(!empty($user_ids)) {
                foreach($user_ids as $id_user) {
                    if(!empty($id_user)) {
                        $user_query = "SELECT * FROM " . $this->user_table . " WHERE id = '" . $id_user . "' ";
                        $sql_result = $this->_db->my_query($user_query);
                        while ($user_data = $this->_db->my_fetch_object($sql_result)) {
                            $response_data[] = $user_data;
                        }
                    }
                }
            }
            $_response['result_code'] = 1;
            $_response['response_data'] = $response_data;
        } else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } //end contactSynchronisation
    
    /*
     * @checkMobileNo is to check user exist with given mobile no. or not
     * @input phone_no(int)
     * @output int/boolean 
     **/
     
    public function checkMobileNo($phone_no ='') {
        //user w.r.t phone no
        $table = $this->user_table; 
        $columns = ['id'];
        $where = ['mobile'=>$phone_no];
        $get_record = $this->_common->get_table_record($table,$columns,$where);
        if(!empty($get_record)){
            $user_id = (!empty($get_record[0]->id)) ? $get_record[0]->id : 0;
            if(!empty($user_id)) {
                return $user_id;
            }
        }
        return false;
    }//end checkMobileNo
    
    /*
     * @checkEmailAddress is to check user exist with given email address. or not
     * @input email_id(int)
     * @output int/boolean 
     **/
     
    public function checkEmailAddress($email_id = '') {
        //user w.r.t email address
        $table = $this->user_table; 
        $columns = ['id'];
        $where = ['email'=>$email_id];
        $get_record = $this->_common->get_table_record($table,$columns,$where);
        if(!empty($get_record)){
            $user_id = (!empty($get_record[0]->id)) ? $get_record[0]->id : 0;
            if(!empty($user_id)) {
                return $user_id;
            }
        }
        return false;
    }//end checkEmailAddress
    
    
     /**
	  * @rewardCampaignList is listing of all reward campaign
      * @input: none
      * @output:json array
      **/
    public function rewardCampaignList() {
		$user_id = (!empty($_REQUEST['user_id']))?$_REQUEST['user_id']:0;
		$table   = $this->reward_campaign_tbl; 
        $Today   = date('Y-m-d');
        $user_query = "SELECT * FROM " .$table. " WHERE status = '1' AND is_deleted = '0' AND start_date <= '".$Today."' AND end_date >= '".$Today ."'  ";
        $get_record = $this->_db->my_query($user_query);
       	if($this->_db->my_num_rows($get_record) > 0){
			$record_array= array();
			while($row = $this->_db->my_fetch_object($get_record)){
                $record_array[]   = array( 'id' =>$row->id,
                                        'name'  =>$row->name,
                                        'short_description' =>$row->short_description,
                                        'long_description'  =>$row->long_description,
                                        'start_date'   =>$row->start_date,
                                        'end_date' 	   =>$row->end_date,
                                        'action_type'  =>$row->action_type,
                                        'action_sub_type'  =>$row->action_sub_type,
                                        'product_id'  =>$row->product_id,
                                        'reward_points'=>$row->reward_points,
                                        'image' =>!empty($row->image)?$this->reward_img_path.''.$row->image:'',
                                        'created_by' =>$row->created_by
                                    );
                
            }
            $_response['result'] 	    = $record_array;
            $_response['result_code']   = 1;
            $_response['balance_point'] = $this->_common->getBalancePoint($user_id);
            $_response['message'] 	    = $this->_common->langText($this->_locale,'txt.reward.campaign_success');
        } else {
                $_response['result'] = '';
                $_response['result_code'] = 0;
   				$_response['balance_point'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
        return $_response;    
	}
	

/**
  * @redeemReward for redeem 
  * @input: user_id,catalogue_id,acc_number
  * @output:json array
  **/
	public function redeemReward(){
		$reward_trans_table     = $this->reward_transaction_table;
		$reward_redeem_log_tbl  = $this->reward_redeem_log_tbl;
		$reward_catalogue_table = $this->reward_catalogue;		
		
        $date            = date('Y-m-d');
        $currentdatetime = CURRENT_TIME;
		$user_id         = (!empty($_REQUEST['user_id']))?$_REQUEST['user_id']:0;
		$catalogue_id    = (!empty($_REQUEST['catalogue_id']))?$_REQUEST['catalogue_id']:0;
		$product_id      = (!empty($_REQUEST['product_id']))?$_REQUEST['product_id']:0;
		$member_id       = (!empty($_REQUEST['acc_number']))?$_REQUEST['acc_number']:0;
		$is_extra_amount_pay       = (!empty($_REQUEST['is_extra_amount_pay']))?$_REQUEST['is_extra_amount_pay']:0;
      
        $_response['result']        = '';
        $_response['result_code']   = 0;
        $_response['message']       = $this->_common->langText($this->_locale,'txt.register.error.required');
        if(!empty($user_id) && !empty($catalogue_id) && !empty($member_id)){
            $user_point_array = $this->_common->getBalancePoint($user_id);
            $user_point = (!empty($user_point_array['redeem_points'])) ? $user_point_array['redeem_points'] : 0;
            $is_expired = (!empty($user_point_array['is_expired'])) ? $user_point_array['is_expired'] : 0;
            if(empty($is_expired)) {
                $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_previous_point');
                $_response['result_code'] = 0;
                return $_response;
            }
            $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_point_notnull');
            $_response['result_code'] = 4;
            
            if(!empty($user_point)){
                $query = "SELECT product.redeem_points,product.redeem_points_with_extra_price,product.add_extra_price,product.extra_price,product.product_id,product.product_name,product.catalogue_id FROM ".$this->reward_catalogue." catalogue JOIN ".$this->reward_catalogue_products." product ON product.catalogue_id = catalogue.id WHERE catalogue.status = 1 AND product.id = ".$product_id." AND product.status = 1 AND (product.start_date <= '".$date."' AND product.end_date >= '".$date."') ";
                
                $get_record_two = $this->_db->my_query($query);
                $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_invalid');
                $_response['result_code'] = 3;
                
                if($this->_db->my_num_rows($get_record_two) > 0){
                    $catalogue_data = $this->_db->my_fetch_object($get_record_two);    
                    //print_r($catalogue_data);die;                                    
                    if(!empty($catalogue_data->redeem_points) && $catalogue_data->redeem_points >0){
                        $_response['message']       = $this->_common->langText($this->_locale,'txt.reward.redeem_point_not_enough');
                        $_response['result_code'] = 2;
                        $product_id = (!empty($catalogue_data->product_id)) ? $catalogue_data->product_id : 0 ; 
                        $product_name = (!empty($catalogue_data->product_name)) ? $catalogue_data->product_name : 0 ; 
                        $redeem_points = (!empty($catalogue_data->redeem_points)) ? $catalogue_data->redeem_points : 0 ; 
                        $redeem_points_with_extra_price = (!empty($catalogue_data->redeem_points_with_extra_price)) ? $catalogue_data->redeem_points_with_extra_price : 0 ; 
                        $add_extra_price = (!empty($catalogue_data->add_extra_price)) ? $catalogue_data->add_extra_price : 0 ; 
                        $extra_price = 0;
                        if(!empty($add_extra_price)) {
                            $extra_price = (!empty($catalogue_data->extra_price)) ? $catalogue_data->extra_price : 0 ;                                    
                        }  
                        if($user_point > $redeem_points){
                            // set qr code image name
                            $error_correction_level = 'H';
                            $matrix_point_size = 10;
                            // get barcode final digit
                            $barcode_digits = $this->_common->get_barcode_digits($member_id);
                            $qr_code        = $barcode_digits.'#'.$catalogue_data->product_id; 
                            $qr_filename    = md5($qr_code.'|'.$error_correction_level.'|'.$matrix_point_size).'.png';
                            include_once("qr_code.php"); // include to generate user barcode
                            // convert qr code image in byte code
                            if(!empty($qr_filename)) {
                                $qr_path = IMG_URL.'qr_codes/'.$qr_filename;
                                $type    = pathinfo($qr_path, PATHINFO_EXTENSION);
                                $data    = $this->_common->file_get_contents_curl($qr_path);
                                $qr_code_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
                            }
                                                             
                            $qr_path = (isset($qr_path) && !empty($qr_path))?$qr_path:'';
                            $Insert_redeem_log = "INSERT INTO ".$reward_redeem_log_tbl." (user_id,catalogue_id,product_id,product_name,section_type,qr_code,redeem_point,redeem_points_with_extra_price,extra_price,create_date,is_extra_amount_pay) VALUES('".mysql_real_escape_string($user_id)."','".mysql_real_escape_string($catalogue_id)."','".mysql_real_escape_string($product_id)."','".mysql_real_escape_string($product_name)."','1','".$qr_path."','".mysql_real_escape_string($redeem_points)."','".mysql_real_escape_string($redeem_points_with_extra_price)."','".mysql_real_escape_string($extra_price)."','".$currentdatetime."','".$is_extra_amount_pay."')";
                            $this->_db->my_query($Insert_redeem_log);
                            $remain_point = (int)($user_point - $catalogue_data->reward_points);
                            $msg_success = $this->_common->langText($this->_locale,'txt.reward.redeem_sucessfully');
                            $_response['result']       = array('balance_point'=>$remain_point,'qr_path'=>$qr_path);
                            $_response['result_code'] = 1;
                            $_response['message']     = str_replace(array('{x}'), array($catalogue_data->title),$msg_success);
                        }        
                    }
                }
            }
        }
        return $_response; 
	}//redeemReward
    
    /*
     *@userRedeemHistory
     *@input: user_id
     *@output: array 
     **/
    public function userRedeemHistory() {
        //default result to be return
        $response =array();
        $_response['result_code'] = 0;
        $_response['message']       = $this->_common->langText($this->_locale,'txt.register.error.required');
        
        //get request parameters
        $user_id    =   (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        if(!empty($user_id)) {
            $query = "SELECT redemlog.id,redemlog.redeem_point,redemlog.product_name,redemlog.extra_price,redemlog.create_date FROM ".$this->reward_redeem_log_tbl." redemlog WHERE redemlog.user_id = '".$user_id."' AND redemlog.is_redeem = '1'";
            $get_result = $this->_db->my_query($query);
            if($this->_db->my_num_rows($get_result) > 0){
                while($data = $this->_db->my_fetch_object($get_result)) {
                    $result['id'] = (!empty($data->id)) ? $data->id : 0;
                    $result['redeem_point'] = (!empty($data->redeem_point)) ? $data->redeem_point : 0;
                    $result['create_date'] = (!empty($data->create_date)) ? $data->create_date : 0;
                    $result['product_name'] = (!empty($data->product_name)) ? $data->product_name : '';
                    $result['extra_price'] = (!empty($data->extra_price)) ? $data->extra_price : '';
                    $response[] = $result;
                }
                $_response['result_code'] = 1;
                $_response['result'] = $response;
                $_response['message']   = $this->_common->langText($this->_locale,'txt.reward.user_redeem_history');
            } else {
                $_response['message']       = $this->_common->langText($this->_locale,'txt.no_record_found');
            }
        }
        return $_response;
    }//userRedeemHistory
    
     /**
      * @shareApplication use to update the share info and assign the reward points 
      * @input: user_id,catalogue_id
      * @output:json array
      **/
	public function shareApplication(){
        // get the requst parameters
        $user_id    = (!empty($_REQUEST['user_id']))?$_REQUEST['user_id']:0;
		$campaign_id= (!empty($_REQUEST['campaign_id']))?$_REQUEST['campaign_id']:0;
		$share_with= (!empty($_REQUEST['share_with']))?$_REQUEST['share_with']:'';
        //check required parameters
        if(!empty($user_id) && !empty($campaign_id)){
            //check camapaign has reward adn can be assign to user or not
            $is_reward_assign = $this->_common->manageCompaignLog($user_id,$campaign_id);
            if(!empty($is_reward_assign)) {
                $transaction_type = 'application_social_sharing_points';
                $record_transaction_query = "INSERT INTO ".$this->reward_transaction_table." (user_id,balance_point,transaction_type,comments,transaction_date,status) VALUES(
                '".mysql_real_escape_string($user_id)."',
                '".mysql_real_escape_string($is_reward_assign)."',
                '".mysql_real_escape_string($transaction_type)."',
                '".mysql_real_escape_string($share_with)."',
                '".date('y-m-d H:i:s')."',
                '2')"; // 2 received or success
                $this->_db->my_query($record_transaction_query);
                
                //save as notification
                $push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.application_social_sharing_points'),$is_reward_assign); 
                $notification_data = ['user_id'=>$user_id,'notification'=>$push_message,'notification_type'=>$transaction_type];
                $this->_common->save_notification($notification_data);
                     
                $_response['result_code'] = 1;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.share_pplication.success');
            } else {
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.share_pplication.notvalid');
            }
        }  else {
            $_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
	}//shareApplication
    
    /*
     * @userTransactionHistory
     * */
    public function userTransactionHistory() {
        $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
        $table = $this->reward_transaction_table; 
        $columns = '';
        $where = ['user_id'=>$user_id];
        $orderby = 'transaction_date';
        $get_record = $this->_common->get_table_record($table,$columns,$where,$orderby);
        $_response['result_code'] = 1;
        if(!empty($get_record)) {
            $_response['result'] = $get_record;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.reward.notification_success');
        } else {
            $_response['result'] = '';
            $_response['message'] = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
        return $_response;
    } //userTransactionHistory
}
