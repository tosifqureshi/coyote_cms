<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : Sept-09 12:20PM
 * Copyright : Coyote team
 *
 */
class scratchnwin {
	
    public $_response = array();
    public $result = array();
    public $iosPush = array();
    public $androidPush = array();
    private $scratch_n_win_table = 'ava_scratch_n_win';
    private $scratch_n_win_products_table = 'ava_scratch_n_win_products';
    private $scratch_n_win_prize_winners_table = 'ava_scratch_n_win_prize_winners';
    private $scratch_n_win_push_log_table = 'ava_scratch_n_win_push_log';
    private $scratch_n_win_product_sent_log_table = 'ava_scratch_n_win_product_sent_log';
    private $scratch_n_win_notification_log_table = 'ava_scratch_n_win_notification_log';
    private $ava_cte_campaign_push = 'ava_cte_campaign_push';
    private $ava_cte_basket_size = 'ava_cte_basket_size';
    private $user_account_table = 'ava_users';
    private $apn_table = 'APNTBL';
    private $member_table = 'Member';
    private $prod_table = 'ProdTbl';
    private $outp_table = 'OutpTbl';
    private $cte_brands_table = 'ava_cte_brands';
    private $cte_winners_table = 'ava_cte_winners_log';
	private $cte_brands_entries_table = 'ava_cte_brands_entry_availability';

   
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
		$this->_mssql = new mssql(); // Create instance of mssql class
		$this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
        $this->_config  = config::getInst();
        $this->scatchnwin_img_path = IMG_URL.'scratchnwin_images/'; // -- Set image path
		//$this->product_img_path = IMG_URL.'product_images/'; // -- Set image path
        $this->product_img_path = IMG_URL.'scratchnwin_images/'; // -- Set image path
    }
	
	 /**
	 * Function used to send scratchnwin's prize push notification
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
     public function scratchnwin_customer_push() {
		
        // get active scratch n win records 
		//$datetime = date('Y-m-d H:i');
		$datetime = date('Y-m-d H');
		$date = date('Y-m-d');
		$current_day = strtolower(date('l'));
		// get app versions
		$mini_ios_version = $this->_config->getKeyValue("mini_ios_version");
		$mini_android_version = $this->_config->getKeyValue("mini_android_version");
		/*$scratchnwin_query  = "SELECT snw.snw_id,snw.title,snw.notification_time,snw.second_chance_count,wp.push_sent_at FROM " . $this->scratch_n_win_products_table . " snwp 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id LEFT JOIN " .$this->scratch_n_win_push_log_table. " wp ON snw.snw_id = wp.snw_id WHERE snwp.product_image <> '' AND snwp.status = 1 AND snw.banner_image <> '' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date' 
				AND (snwp.total_prizes > (SELECT sum(product_sent_count) FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id) 
				OR NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id ) ) GROUP BY snwp.snw_id ORDER BY snw.snw_id DESC";*/
		$scratchnwin_query  = "SELECT snw.snw_id,snw.title,snw.notification_time,snw.second_chance_count,wp.push_sent_at FROM " . $this->scratch_n_win_products_table . " snwp 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id LEFT JOIN " .$this->scratch_n_win_push_log_table. " wp ON snw.snw_id = wp.snw_id WHERE snwp.product_image <> '' AND snwp.status = 1 AND snw.banner_image <> '' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND  snw.campaign_type = '1' AND snw.end_date >= '$date' AND snwp.days_availability  like '%,$current_day,%' GROUP BY snwp.snw_id ORDER BY snw.snw_id DESC";
		$scratchnwin_result = $this->_db->my_query($scratchnwin_query);
		
		if ($this->_db->my_num_rows($scratchnwin_result) > 0) {
			$snw_total_count = $this->_db->my_num_rows($scratchnwin_result);
			// get app customers
			$user_query = "SELECT u.device_token,u.device_type,u.acc_number,nt.giveaway_count,nt.prize_count,nt.viewed_count FROM ".$this->user_account_table." u LEFT JOIN ".$this->scratch_n_win_notification_log_table." nt ON (nt.member_id = u.acc_number AND nt.modified_at = '$date') WHERE u.device_token <> '' AND u.device_type <> '' AND u.acc_number <> '' AND u.active = 1 AND u.is_push_send = 1 AND ((u.device_type = 'ios' AND u.mini_app_version >= '$mini_ios_version') OR (u.device_type = 'android' AND u.mini_app_version >= '$mini_android_version') )";
			$user_result = $this->_db->my_query($user_query);
			if ($this->_db->my_num_rows($user_result) > 0) {
				$push_members = array();
				$android_devices = array();
				$ios_devices = array(); 
				while($user_row = $this->_db->my_fetch_object($user_result)) {
					// set customer's device param
					$device_type = $user_row->device_type;
					$device_id = trim($user_row->device_token);
					$member_id = $user_row->acc_number;
					$giveaway_count = (isset($user_row->giveaway_count)) ? $user_row->giveaway_count : '';
					$prize_count = (isset($user_row->prize_count)) ? $user_row->prize_count : '';
					$viewed_count = (isset($user_row->viewed_count)) ? $user_row->viewed_count : '';
					
					if(!empty($device_id)) {
						// get member existanse in competition
						$co_member_query = "SELECT MEMB_Exclude_From_Competitions FROM ".$this->member_table." where MEMB_NUMBER = '$member_id'";
						$get_co_member_data = $this->_mssql->my_mssql_query($co_member_query,$this->con);
						$co_member_result = $this->_mssql->mssql_fetch_object_query($get_co_member_data);
						// set exclude value
						$exclude_from_competitions = (isset($co_member_result->MEMB_Exclude_From_Competitions) && $co_member_result->MEMB_Exclude_From_Competitions == 1) ? 1 : 0;
						if($exclude_from_competitions == 0) {
							if($device_type == 'android'){
								// set andriod device token in array
								$android_devices[] = $device_id;
							}else{
								// set ios device token in array
								$ios_member_array['device_id'] = $device_id;
								$ios_member_array['giveaway_count'] = $giveaway_count;
								$ios_member_array['prize_count'] = $prize_count;
								$ios_member_array['viewed_count'] = $viewed_count;
								$ios_devices[] = $ios_member_array;
							}
							//set member id in push member array
							$push_members[] = $member_id;
						}
					}
				}
			}
			
			if(is_array($push_members) && count($push_members) > 0) {
				// send push for individual scratch & win entry
				while($row = $this->_db->my_fetch_object($scratchnwin_result)) {
					// set scratch values for push
					$push_sent_at = (isset($row->push_sent_at) && !empty($row->push_sent_at)) ? $row->push_sent_at : '';
					if(!empty($push_sent_at) && $push_sent_at == $date) {
						// do nothing in this case
					} else {
						// set scratch values for push
						$snw_id = (isset($row->snw_id) && !empty($row->snw_id)) ? $row->snw_id : 0;
						$title = (isset($row->title) && !empty($row->title)) ? $row->title : '';

						$notification_time = (isset($row->notification_time) && !empty($row->notification_time)) ? explode(':',$row->notification_time) : '';
						$notification_time = (isset($notification_time[0]) && !empty($notification_time[0])) ? $notification_time[0] : '00';
						$cron_execute_at = $date.' '.$notification_time;
						
						if($cron_execute_at <= $datetime) {
							// set unique device ids array
							$android_devices = array_unique($android_devices);
							// reset device ids array
							$android_devices = $this->device_array_keys($android_devices);
							// send prizes IOS push
							$this->_push->sendIphonePushNotification_prize($ios_devices,$snw_id,$title,$date,$snw_total_count);
							// split android device array in chunk
							$android_chunk_devices = array_chunk($android_devices, 950);
							$push_response_array = array();
							// send prizes andriod push
							for($i=0;$i<count($android_chunk_devices);$i++) {
								$sent_result = $this->_push->sendAndroidPushNotification_prize($android_chunk_devices[$i],$snw_id,$title,$date);
								$push_response_array[] = $sent_result;
							}
							// store prize push log for today
							$this->manage_push_log($push_members,$snw_id,$date,$sent_result,$android_devices,$ios_devices);
						}
					}
				}
			}
		}
        return true;
	}
     
     /**
	 * Function used to manage device array keys
	 * @param array device_ids
	 * @return void
	 * @access private
	 * 
	 */
     private function device_array_keys($device_ids='') {
		$device_array = array();
		if(!empty($device_ids)) {
			foreach($device_ids as $key=>$device_id) {
				$device_array[] = $device_id;
			}
		}
		return $device_array;
	 }
     
	/**
	 * Function used to manage customer's push log
	 * @param int snw_id , int member_id
	 * @return void
	 * @access private
	 * 
	 */
	private function manage_push_log($member_ids='',$snw_id=0,$date=0,$push_response='',$android_devices='',$ios_devices='') {
		
		$datetime = date('Y-m-d H:i:s');
		if(is_array($member_ids) && count($member_ids) > 0 && !empty($snw_id)) {
			
			// split member ids array in string
			$member_ids = implode(',',$member_ids);
			// write log in test file
			$filename =  BASEPATH.'/log.txt'; 
			file_put_contents($filename, $push_response);
			// optimize andriod push response
			$decoded_push_res = json_decode($push_response);
			// set android device count
			$android_device_count = count($android_devices);
			// set ios device count
			$ios_device_count = count($ios_devices);
			$push_res = array();
			if(isset($decoded_push_res->success) && isset($decoded_push_res->failure)) {
				$push_res = array('android_device_count'=>$android_device_count,'success'=>$decoded_push_res->success,'failure'=>$decoded_push_res->failure,'ios_device_count'=>$ios_device_count);
			}
			$android_push_res = json_encode($push_res);
			//$android_push_res = $push_response;
			// get customer's info scratch n win entry
			$snw_push_query  = "SELECT snw_push_id FROM " . $this->scratch_n_win_push_log_table . " WHERE snw_id = '$snw_id'" ;
			$snw_push_result = $this->_db->my_query($snw_push_query);
			if ($this->_db->my_num_rows($snw_push_result) > 0) {
				$push_row = $this->_db->my_fetch_object($snw_push_result);
				if(!empty($push_row) && isset($push_row->snw_push_id)) {
					// set push id
					$snw_push_id = $push_row->snw_push_id;
					// get push sent entry
					$push_sent_query  = "SELECT member_ids FROM " . $this->scratch_n_win_push_log_table . " WHERE snw_push_id = '$snw_push_id'" ;
					$push_sent_result = $this->_db->my_query($push_sent_query);
					if ($this->_db->my_num_rows($push_sent_result) > 0) {
						// update snw customer's push record
						if(!empty($snw_push_id)) {
							$update_array = array("modified_at"=>$datetime,"member_ids"=>$member_ids,"push_sent_at"=>$date,"push_sent_response"=>$android_push_res);// Update values
							$where_clause = array("snw_push_id"=>$snw_push_id);
							$this->_db->UpdateAll($this->scratch_n_win_push_log_table, $update_array, $where_clause, "");
						}
					}
				}
			} else {
				
				// insert customer's push count log
				$push_log_query = "INSERT INTO ".$this->scratch_n_win_push_log_table."(member_ids,snw_id,push_sent_at,push_sent_response,modified_at) VALUES ('$member_ids','$snw_id','$date','$android_push_res','$datetime')";
				$this->_db->my_query($push_log_query);
				$snw_push_id = mysql_insert_id();
			}
		}
		return true;
	}
     
    /**
	 * Function used to check scratchnwin active or not
	 * @param snw_id
	 * @return array
	 * @access public
	 * 
	 */
	public function validate_scratchnwin() {
		
		$date = date('Y-m-d');
		$datetime = date('Y-m-d H:i:s');
		$current_day = strtolower(date('l'));
		$snw_id = $this->_common->test_input($_REQUEST['snw_id']);
		$push_sent_at = $this->_common->test_input($_REQUEST['push_sent_at']);
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
        $push_type = $this->_common->test_input($_REQUEST['push_type']);
        $push_type = (!empty($push_type))?$push_type:5;
		$_response['result_code'] = 0;
		// check competition id exists or not
		if(empty($snw_id) || empty($member_id)) {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_invalid');
			return $_response;
		}
		/* Filteration with respect to scretch N win */
        if($push_type == 5){
            // get today's prize record if exist
            $todays_prize_query  = "SELECT member_id,prize_declare_at FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = '$snw_id' AND member_id = '$member_id' AND prize_declare_at = '$date' LIMIT 1";
            $todays_prize_result = $this->_db->my_query($todays_prize_query);
            if ($this->_db->my_num_rows($todays_prize_result) > 0) {
                // return response in case , if member is already a winner for today
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.already_played_today');
                return $_response;
            }
                $scatchnwin_query  = "SELECT snw.snw_id,snw.campaign_type,snw.title,snw.second_chance_draw,snw.second_chance_count,snw.banner_image,snwp.product_id,snwp.product_name,snwp.product_image,
                snwp.id as snw_product_id,pc.snw_push_id,pc.member_ids FROM " . $this->scratch_n_win_products_table . " snwp 
                JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
                WHERE snwp.product_image <> '' AND snwp.status = 1 AND snwp.is_deleted = 0 AND snw.status = '1' AND snw.is_deleted = '0' AND snw.snw_id = '$snw_id' 
                AND snw.end_date >= '$date' AND pc.push_sent_at = '$push_sent_at' ORDER BY RAND() LIMIT 1";
                $scatchnwin_result = $this->_db->my_query($scatchnwin_query);
                if ($this->_db->my_num_rows($scatchnwin_result) > 0) {
                    // fetch result object data
                    $row = $this->_db->my_fetch_object($scatchnwin_result);
                    // set scratchnwin values
                    $snw_array['snw_id'] = (isset($row->snw_id)) ? trim($row->snw_id) : "";
                    $snw_array['campaign_type'] = (isset($row->campaign_type)) ? trim($row->campaign_type) : "";
                    $snw_array['title']	= (isset($row->title)) ? trim($row->title) : "";
                    // set scratch n win default image
                    $banner_image	= (isset($row->banner_image) && !empty($row->banner_image)) ? trim($this->scatchnwin_img_path.$row->banner_image) : "";
                        
                    // convert banner image in byte code
                    if(!empty($banner_image)) {
                        $type = pathinfo($banner_image, PATHINFO_EXTENSION);
                        $data = $this->_common->file_get_contents_curl($banner_image);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }
                    $snw_array['snw_image']	= (isset($base64)) ? $base64 : '';
                    $snw_array['image_url']	= $banner_image;
                    
                    // append scratch n win basic information in result array
                    $_response['result_code'] = 1;
                    $_response['snw_id'] = $snw_id;
                    $_response['results'] = $snw_array; 
                    
                } else {
                    $_response['result_code'] = 0;
                    $_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_expired');
                }
        }
        
        /* Filteration with respect to Crack the Egg */
        if($push_type == 6){
			
            // get today's prize record if exist
            $todays_prize_query  = "SELECT member_id,prize_declare_at FROM " . $this->cte_winners_table . " WHERE cte_id = '$snw_id' AND member_id = '$member_id' AND prize_declare_at = '$date' LIMIT 1";
            $todays_prize_result = $this->_db->my_query($todays_prize_query);
            if ($this->_db->my_num_rows($todays_prize_result) > 0) {
                // return response in case , if member is already a winner for today
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.already_played_today');
                return $_response;
            }
            			
			// fetch crack the egg avilability / authentication
			$cracktheegg_query  = "SELECT snw.snw_id,snw.campaign_type,snw.title,snw.banner_image,cteb.id,cteb.brand,snw.banner_image,
			cteb.id as brand_id FROM " . $this->cte_brands_table . " cteb 
			JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id JOIN " . $this->cte_brands_entries_table . " cbea ON cbea.brand_id = cteb.id WHERE cteb.brand_image <> '' AND cteb.status = 1 AND cteb.is_deleted = 0 AND snw.status = '1' AND snw.is_deleted = '0' AND snw.snw_id = '$snw_id' AND cbea.entry_point > 0 AND cbea.entry_point != '' AND cbea.day = '$current_day' ORDER BY RAND() LIMIT 1";
			$cracktheegg_result = $this->_db->my_query($cracktheegg_query);
			if ($this->_db->my_num_rows($cracktheegg_result) > 0) {
				// fetch result object data
				$row = $this->_db->my_fetch_object($cracktheegg_result);
				// set scratchnwin values
				$snw_array['snw_id'] = (isset($row->snw_id)) ? trim($row->snw_id) : "";
				$snw_array['campaign_type'] = (isset($row->campaign_type)) ? trim($row->campaign_type) : "";
				$snw_array['title']	= (isset($row->title)) ? trim($row->title) : "";
				// set scratch n win default image
				$banner_image	= (isset($row->banner_image) && !empty($row->banner_image)) ? trim($this->scatchnwin_img_path.$row->banner_image) : "";
					
				// convert banner image in byte code
				if(!empty($banner_image)) {
					$type = pathinfo($banner_image, PATHINFO_EXTENSION);
					$data = $this->_common->file_get_contents_curl($banner_image);
					$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
				}
				$snw_array['snw_image']	= (isset($base64)) ? $base64 : '';
				$snw_array['image_url']	= $banner_image;
				
				// append scratch n win basic information in result array
				$_response['result_code'] = 1;
				$_response['snw_id'] = $snw_id;
				$_response['results'] = $snw_array; 
				
			} else {
				$_response['result_code'] = 0;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.cte.error.no_prize_available');
			}   
			                
        }
        
        return $_response;
	} // validate_scratchnwin
	
	/**
	 * Function used to get random prize in scratch
	 * @param int snw_id , int acc_number
	 * @return array
	 * @access public
	 * 
	 */
	public function scratchprize() {
		$date  = date('Y-m-d');
		$datetime  = date('Y-m-d H:i:s');
		$current_day = strtolower(date('l'));
		$snw_id    = $this->_common->test_input($_REQUEST['snw_id']);
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
        $push_type = $this->_common->test_input($_REQUEST['push_type']);
        $push_type = (!empty($push_type))?$push_type:5;
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.no_prize_available_member');
		// check scratchnwin id exists or not
		if(empty($snw_id)) {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_invalid');
			return $_response;
		}
		
		if(!empty($member_id)) {
			// get customer's data
			$customer_query = "SELECT firstname,lastname FROM " . $this->user_account_table . " WHERE acc_number = '" . $member_id . "' ";
			$customer_result = $this->_db->my_query($customer_query);
			if ($this->_db->my_num_rows($customer_result) > 0) {
				// fetch result object data
				$customer_row = $this->_db->my_fetch_object($customer_result);
				// set customer's first name
				$firstname = (isset($customer_row->firstname)) ? $customer_row->firstname : '';
                
                /* Filteraction for scretch N Win */
                 if($push_type == 5){
					// get scatchnwin details whos not expired
					/*$scatchnwin_query  = "SELECT snw.snw_id,snw.scrwin_id,snw.title,snw.second_chance_draw,snw.second_chance_count,snw.prize_frequency,snw.banner_image,snwp.product_id,snwp.product_name,snwp.product_image,
                    snwp.id as snw_product_id,pc.snw_push_id,pc.member_ids,snwp.prizes_per_day,psl.product_sent_count FROM " . $this->scratch_n_win_products_table . " snwp 
                    JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
                    LEFT JOIN " . $this->scratch_n_win_product_sent_log_table . " psl ON (psl.snw_id = snwp.snw_id AND psl.product_id = snwp.product_id AND psl.modified_at = '$date')
                    WHERE snwp.product_image <> '' AND snw.status = '1' AND snwp.status = '1' AND snwp.is_deleted = '0' AND snw.is_deleted = '0' AND snw.snw_id = '$snw_id' 
                    AND snw.end_date >= '$date' AND pc.push_sent_at = '$date' AND snwp.days_availability  like '%,$current_day,%' AND (snwp.total_prizes > (SELECT sum(product_sent_count) FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id) 
                    OR NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id ) ) ORDER BY RAND() LIMIT 1";*/
                    //$scatchnwin_query  .= "AND (snwp.days_availability  like '%,$current_day,%' OR snwp.days_availability = '') ";
                    
                    // fetch Scratch&Win record 
					$scatchnwin_query  = "SELECT  snw.snw_id,snw.scrwin_id,snw.title,snw.second_chance_draw,snw.second_chance_count,snw.prize_frequency,snw.banner_image FROM " . $this->scratch_n_win_table . " snw JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snw.snw_id WHERE snw.status = '1' AND snw.is_deleted = '0' AND snw.snw_id = '$snw_id' 
                    AND snw.end_date >= '$date' AND pc.push_sent_at = '$date'";
                    $scatchnwin_result = $this->_db->my_query($scatchnwin_query);
                    if ($this->_db->my_num_rows($scatchnwin_result) > 0) {
                        // fetch result object data
                        $snw_row = $this->_db->my_fetch_object($scatchnwin_result);
						$prize_avail_type = 3; // second chance type
                        // check total prizes available or not
                        $prize_stock_query  = "SELECT snw.snw_id,snw.scrwin_id,snw.title,snw.second_chance_draw,snw.second_chance_count,snw.prize_frequency,snw.banner_image,snwp.product_id,snwp.product_name,snwp.product_image,
						snwp.id as snw_product_id,pc.snw_push_id,pc.member_ids,snwp.prizes_per_day,psl.product_sent_count FROM " . $this->scratch_n_win_products_table . " snwp 
						JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
						LEFT JOIN " . $this->scratch_n_win_product_sent_log_table . " psl ON (psl.snw_id = snwp.snw_id AND psl.product_id = snwp.product_id AND psl.modified_at = '$date')
						WHERE snwp.product_image <> '' AND snw.status = '1' AND snwp.status = '1' AND snwp.is_deleted = '0' AND snw.is_deleted = '0' AND snw.snw_id = '$snw_id' 
						AND snw.end_date >= '$date' AND pc.push_sent_at = '$date' AND snwp.days_availability  like '%,$current_day,%' AND (snwp.total_prizes > (SELECT sum(product_sent_count) FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id) 
						OR NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id ) ) ORDER BY RAND() LIMIT 1";
						$prize_stock_result = $this->_db->my_query($prize_stock_query);
						if ($this->_db->my_num_rows($prize_stock_result) > 0) {
							// fetch result object data
							$row = $this->_db->my_fetch_object($prize_stock_result);
							// set product per day sent count
							$product_sent_count = (isset($row->product_sent_count) && !empty($row->product_sent_count)) ? $row->product_sent_count : 0; 
							$prizes_per_day = (isset($row->prizes_per_day) && !empty($row->prizes_per_day)) ? $row->prizes_per_day : 0;
							$prize_frequency = (isset($snw_row->prize_frequency) && !empty($snw_row->prize_frequency)) ? $snw_row->prize_frequency : 7;
							// get random key
							$random_key = $this->random_key($prize_frequency,1);
							if($random_key == 1 && $prizes_per_day > $product_sent_count) {
								// set snw param values
								$snw_product_id = $row->snw_product_id;
								$product_id 	= $row->product_id;
								$product_name  	= $row->product_name;
								$product_image 	= (isset($row->product_image) && !empty($row->product_image)) ? $row->product_image : '';
								// set prize availability type for winner
								$prize_avail_type = 1;
								// manage member's winning draw entry
								$prize_winner_id = $this->manage_snw_winner_entry($snw_id,$member_id,$product_id,$snw_product_id,$date,$datetime,$push_type);
								// convert image in byte code
								$product_image = $this->convert_image_in_byte($product_image,$this->product_img_path);
								// get product details
								//$prod_data = array();
								$prod_data = $this->get_product_data($product_id);
								$product_name = (isset($row->product_name) && !empty($row->product_name)) ? $row->product_name : '';
								$product_pos_name = (count($prod_data) > 0 && !empty($prod_data['prod_desc'])) ? $prod_data['prod_desc'] : '';
								$product_name = (!empty($product_name)) ? $product_name : $product_pos_name; 
								$product_pos_desc = (count($prod_data) > 0 && !empty($prod_data['prod_pos_desc'])) ? $prod_data['prod_pos_desc'] : '';
								// insert prize entry in mssql
								$this->ms_member_prize_entry($member_id,$row->scrwin_id,$row->title,$product_id,$push_type);
							 }
						}
						
                       // manage second chance entry 
                       if($prize_avail_type == 3) {
                            // set second draw params
                            $second_chance_draw = (isset($snw_row->second_chance_draw)) ? $snw_row->second_chance_draw : '';
                            $second_chance_count = (isset($snw_row->second_chance_count)) ? $snw_row->second_chance_count : 0;
                            $prize_avail_type = 2; // no win type
                            if($second_chance_draw == 1) {
                                // manage second chance member entry
                                $prize_avail_type = $this->manage_second_chance_entry($snw_id,$member_id,$second_chance_count,$prize_avail_type,$snw_row->scrwin_id,$snw_row->title,$date,$datetime,$push_type,'0');
                            }
                        }
                        
                        // set banner image for second chance
                        if($prize_avail_type == 3 && isset($snw_row->banner_image) && !empty($snw_row->banner_image)) {	
                            // convert product image in byte code
                            $banner_image = $this->convert_image_in_byte($snw_row->banner_image,$this->scatchnwin_img_path);
                        }
                        
                        // set notes based on return type
                        $note1 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.congrates_you_won_note1');
                        $note2 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.congrates_you_won_note2');
                        $note3 = '';
                        if($prize_avail_type == 2) {
                            // insert member's entry in No win case
                            $insert_query = "INSERT INTO ".$this->scratch_n_win_prize_winners_table."(member_id,snw_id,prize_declare_at,status,modified_at,push_type,ctelogid) VALUES ('$member_id','$snw_id','$date',0,'$datetime','$push_type','0')";
                            $this->_db->my_query($insert_query);
                            // set note for no win
                            $note1 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.no_win_note1');
                            $note2 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.no_win_note2');
                            $note2 = str_replace(array('{x}'), array($firstname), $note2);
                            $note3 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.no_win_note3'); 
                        } else if($prize_avail_type == 3) {
                            $note1 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.second_chance_draw_note1');
                            $note2 = $this->_common->langText($this->_locale,'txt.scratchnwin.success.second_chance_draw_note2');
                            $note2 = str_replace(array('{x}'), array($firstname), $note2);
                        }
                        
                        // manage notification count
                        $notification_count = $this->update_notification_counts($member_id,1);
                        // append basic information in result array
                        $_response['result_code'] = 1;
                        $_response['message'] =  $this->_common->langText($this->_locale,'txt.scratchnwin.success.prize_msg_'.$prize_avail_type);
                        $_response['prize_avail_type'] = $prize_avail_type;
                        $_response['prize_winner_id'] = (isset($prize_winner_id)) ? $prize_winner_id : 0;
                        $_response['product_image'] = (isset($product_image)) ? $product_image : '';
                        $_response['product_name'] = (isset($product_name)) ? trim(preg_replace('/\s+/',' ', $product_name)) : '';
                        //$_response['product_pos_desc'] = (isset($product_pos_desc)) ? $product_pos_desc : '';
                        $_response['product_pos_desc'] = $this->_common->langText($this->_locale,'txt.scratchnwin.win_free_note');
                        $_response['snw_image']	= (isset($banner_image)) ? $banner_image : '';
                        $_response['note1'] =  $note1;
                        $_response['note2'] =  $note2;
                        $_response['note3'] =  $note3;
                        $_response['expiry_note'] = $this->_common->langText($this->_locale,'txt.scratchnwin.offer_expire_note');
                        $_response['push_count'] = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
                        $_response['saved_prize_count'] = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
                    }
                }
                
                /* Filteraction for Crack the Egg */
                if($push_type == 6){
                    // get today's prize record if exist
					$todays_prize_query  = "SELECT member_id,prize_declare_at FROM " . $this->cte_winners_table . " WHERE cte_id = '$snw_id' AND member_id = '$member_id' AND prize_declare_at = '$date' LIMIT 1";
					$todays_prize_result = $this->_db->my_query($todays_prize_query);
					if ($this->_db->my_num_rows($todays_prize_result) > 0) {
						// return response in case , if member is already a winner for today
						$_response['result_code'] = 0;
						$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.already_played_today');
						return $_response;
					}
                    $cracktheegg_query  = "SELECT snw.snw_id,snw.title,snw.banner_image,cteb.id as brand_id,cteb.brand,cteb.brand_image,pc.snw_push_id,pc.member_ids,cbea.entry_point FROM " . $this->cte_brands_table . " cteb 
                    JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = cteb.cte_id JOIN " . $this->cte_brands_entries_table . " cbea ON cbea.brand_id = cteb.id               
                    WHERE cteb.brand_image <> '' AND snw.status = '1' AND cteb.status = '1' AND cteb.is_deleted = '0' AND snw.is_deleted = '0' AND snw.snw_id = '$snw_id' 
                    AND snw.end_date >= '$date' AND pc.push_sent_at = '$date' AND cbea.entry_point > 0 AND cbea.entry_point != '' AND cbea.day = '$current_day' AND cteb.days_availability like '%,$current_day,%' ORDER BY RAND() LIMIT 1";
                    $cracktheegg_result = $this->_db->my_query($cracktheegg_query);
                    if ($this->_db->my_num_rows($cracktheegg_result) > 0) {
                        // fetch result object data
                        $row = $this->_db->my_fetch_object($cracktheegg_result);                            
						// set brand param values
						$brand_id 		= $row->brand_id;
						$brand_name  	= $row->brand;
						$brand_image 	= (isset($row->brand_image) && !empty($row->brand_image)) ? $row->brand_image : '';
						$entry_point 	= (isset($row->entry_point) && !empty($row->entry_point)) ? (string)$row->entry_point : '0';
						// manage member's winning draw entry
						$prize_winner_id = $this->manage_cte_winner_entry($snw_id,$member_id,$brand_id,$entry_point,$date,$datetime);
						// convert image in byte code
						$brand_image = $this->convert_image_in_byte($brand_image,$this->product_img_path);
                        // set notes based on return type
                        $note1 = $this->_common->langText($this->_locale,'txt.cte.success.win_prize_note');
                        $note1 = str_replace(array('{x}'), array($brand_name), $note1);
                        // manage notification count
                        $notification_count = $this->update_notification_counts($member_id,1);
						// set coin position
						$coin_position = $this->get_coin_poistion($entry_point);
                        // append basic information in result array
                        $_response['result_code'] = 1;
                        $_response['message'] =  $this->_common->langText($this->_locale,'txt.scratchnwin.success.prize_msg_1');
                        $_response['prize_avail_type'] = 1;
                        $_response['prize_winner_id'] = (isset($prize_winner_id)) ? $prize_winner_id : 0;
                        $_response['product_image'] = (isset($brand_image)) ? $brand_image : '';
                        $_response['product_name'] = (isset($brand_name)) ? $brand_name : '';
                        $_response['product_pos_desc'] = '';
                        $_response['snw_image']	= (isset($banner_image)) ? $banner_image : '';
                        $_response['coin_position'] = $coin_position;
                        $_response['entry_point'] = $entry_point;
                        $_response['entry_lable'] = $this->_common->langText($this->_locale,'txt.cte.entries');
                        $_response['entry_point_txt'] = $entry_point.' '.$this->_common->langText($this->_locale,'txt.cte.entries');
                        $_response['note1'] =  $note1;
                        $_response['note2'] =  '';
                        $_response['note3'] =  '';
                        $_response['additional_prize_note_1'] = $this->_common->langText($this->_locale,'txt.cte.additional_prize_note_1');
                        $_response['additional_prize_note_2'] = '1';
                        $_response['additional_prize_note_3'] = $this->_common->langText($this->_locale,'txt.cte.additional_prize_note_2');
                        $_response['expiry_note'] = $this->_common->langText($this->_locale,'txt.cte.offer_expire_note');
                        $_response['push_count'] = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
                        $_response['saved_prize_count'] = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
                    }
				} 
			} else {
				$_response['result_code'] = 0;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_expired');
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to manage coin position
	 * @param int entry_point
	 * @return int
	 * @access private
	 * 
	 */
	private function get_coin_poistion($entry_point=0) {
		// set coin position
		if($entry_point == '2') {
			$coin_position = 0;
		} elseif($entry_point == '3') {
			$coin_position = 1;
		}  elseif($entry_point == '10') {
			$coin_position = 2;
		} else {
			// get random position digit
			$coin_position = mt_rand(0,2);
		}
		return $coin_position;
	}
	
	/**
	 * Function used to manage member's winning entry
	 * @param int snw_id , int member_id , int second_chance_count , int prize_avail_type
	 * @return int
	 * @access private
	 * 
	 */
	private function manage_snw_winner_entry($snw_id,$member_id,$product_id,$snw_product_id,$date,$datetime,$push_type=0,$ctelogid=0) {
		
		// insert prize product winner entry
		$insert_query = "INSERT INTO ".$this->scratch_n_win_prize_winners_table."(member_id,snw_id,snw_product_id,prize_declare_at,modified_at,push_type,ctelogid) VALUES ('$member_id','$snw_id','$snw_product_id','$date','$datetime','$push_type','$ctelogid')";
		$this->_db->my_query($insert_query);
		$prize_winner_id = mysql_insert_id();
		// set today product sent count
		$todays_product_query  = "SELECT id,product_sent_count FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = '$snw_id' AND product_id = '$product_id' AND modified_at = '".$date."' LIMIT 1";
		$todays_product_result = $this->_db->my_query($todays_product_query);
		// fetch result object data
		$todays_product_result_row = $this->_db->my_fetch_object($todays_product_result);
		if (!empty($todays_product_result_row) && isset($todays_product_result_row->id)) {
			// set product sent count
			$product_sent_count = (!empty($todays_product_result_row->product_sent_count)) ? $todays_product_result_row->product_sent_count+1 : 0;
			// update product's sent count
			$update_array = array("product_sent_count" => $product_sent_count);// Update values
			$where_clause = array("id" => $todays_product_result_row->id);
			$this->_db->UpdateAll($this->scratch_n_win_product_sent_log_table, $update_array, $where_clause, "");
		} else {
			// insert product sent count
			$insert_query = "INSERT INTO ".$this->scratch_n_win_product_sent_log_table."(snw_id,product_id,product_sent_count,modified_at) VALUES ('$snw_id','$product_id','1','$date')";
			$this->_db->my_query($insert_query);
			$prize_sent_id = mysql_insert_id();
		}
		return $prize_winner_id;
	}
	
	/**
	 * Function used to manage member's winning entry
	 * @param int snw_id , int member_id , int second_chance_count , int prize_avail_type
	 * @return int
	 * @access private
	 * 
	 */
	private function manage_cte_winner_entry($cte_id=0,$member_id=0,$brand_id=0,$entry_point=0,$date='',$datetime='') {
		
		// insert winner's entry
		$insert_query = "INSERT INTO ".$this->cte_winners_table."(member_id,cte_id,brand_id,entry_points,prize_declare_at,created_at) VALUES ('$member_id','$cte_id','$brand_id','$entry_point','$date','$datetime')";
		$this->_db->my_query($insert_query);
		$prize_winner_id = mysql_insert_id();
		return $prize_winner_id;
	}
	
	/**
	 * Function used to manage member's second chance draw 
	 * @param int snw_id , int member_id , int second_chance_count , int prize_avail_type
	 * @return int
	 * @access private
	 * 
	 */
	private function manage_second_chance_entry($snw_id,$member_id,$second_chance_count,$prize_avail_type,$scrwin_id,$title,$date,$datetime,$push_type = 0,$ctelogid=0) {
		
        // get second chance prize record if exist
        $whereFilter = '';
        if($push_type == 6){ // IN the case of crack the egg 
         //   $whereFilter = " AND ctelogid = '$ctelogid'";
        }
        $second_chance_query  = "SELECT count(snw_winner_id) as second_chance_member_count FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = '".$snw_id."' ".$whereFilter."  AND is_second_chance = 1 LIMIT 1";
        $second_chance_result = $this->_db->my_query($second_chance_query);
		// fetch result object data
		$second_chance_row = $this->_db->my_fetch_object($second_chance_result);
		$second_chance_member_count = (isset($second_chance_row->second_chance_member_count) && !empty($second_chance_row->second_chance_member_count)) ? $second_chance_row->second_chance_member_count : 0;
		
        
		if(!empty($second_chance_count) && $second_chance_count > 0  && $second_chance_count <= $second_chance_member_count) {
			// no action
            $prize_avail_type = 2;
		} else {
			// set prize availability type for second chance
			$prize_avail_type = 3;
			// get second chance prize record if exist
			$second_chance_prize_query  = "SELECT snw_winner_id FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = '".$snw_id."' ".$whereFilter." AND member_id = '".$member_id."' AND is_second_chance = 1 LIMIT 1";
			$second_chance_prize_result = $this->_db->my_query($second_chance_prize_query);
			// fetch result object data
			$second_chance_prize_row = $this->_db->my_fetch_object($second_chance_prize_result);
			if(!empty($second_chance_prize_row) && isset($second_chance_prize_row->snw_winner_id)) {
				$prize_winner_id = $second_chance_prize_row->snw_winner_id;
				// update second chance customer's entry
				$update_array = array("prize_declare_at" => $date,"modified_at" =>$datetime);// Update values
				$where_clause = array("snw_winner_id" => $prize_winner_id);
				$this->_db->UpdateAll($this->scratch_n_win_prize_winners_table, $update_array, $where_clause, "");
			} else {
				// insert second chance prize entry
				$insert_query = "INSERT INTO ".$this->scratch_n_win_prize_winners_table."(member_id,snw_id,is_second_chance,prize_declare_at,modified_at,push_type,ctelogid) VALUES ('$member_id','$snw_id',1,'$date','$datetime','$push_type','$ctelogid')";
				$this->_db->my_query($insert_query);
				$prize_winner_id = mysql_insert_id();
			}
			// insert second chance 
			if(isset($scrwin_id) && !empty($scrwin_id)) {
                /* Second Chance entry in mssql */
                    $this->ms_member_second_chance($member_id,$scrwin_id,$title); // currently comment, remove comment before deploy
			}
		}
		
		return $prize_avail_type;
	}
	
	/**
	 * Function used to convert image in byte code
	 * @param string image , string image_path
	 * @return array
	 * @access private
	 * 
	 */
	private function convert_image_in_byte($image,$image_path) {
		// convert image in byte code
		$path = $image_path.$image;
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = $this->_common->file_get_contents_curl($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
		$encoded_image	= $base64;
		return $encoded_image;
	}
	
	/**
	 * Function used to store winner's prize for later redemption
	 * @param int prize_winner_id
	 * @return array
	 * @access public
	 * 
	 */
	public function save_for_later() {
		$datetime  = date('Y-m-d H:i:s');
		$prize_winner_id = $this->_common->test_input($_REQUEST['prize_winner_id']);
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		if(!empty($prize_winner_id)) {
			// update winner's redeem status as save for later
			$update_array = array("is_redeemed" => 2,"modified_at" =>$datetime);// Update values
			$where_clause = array("snw_winner_id" => $prize_winner_id);
			$this->_db->UpdateAll($this->scratch_n_win_prize_winners_table, $update_array, $where_clause, "");
			// manage notification count
			$notification_count = $this->update_notification_counts($member_id);
			$push_count = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
			$saved_prize_count = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
			
			// set response params
			$_response['result_code'] = 1;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.success.save_for_later');
			$_response['push_count'] = $push_count;
			$_response['saved_prize_count'] = $saved_prize_count;
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_winner');
		}
		return $_response;
	}
	
	/**
	 * Function used to get customer's saved prizes
	 * @param int acc_number
	 * @return array
	 * @access public
	 * 
	 */
	public function my_saved_prizes() {
		
		$date  = date('Y-m-d');
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_member');
		
		if(!empty($member_id)) {
			$saved_prize_result = array();
			// get scatchnwin details whos not expired
			$winner_prizes_query  = "SELECT wp.snw_id,wp.snw_winner_id,wp.prize_declare_at,wp.is_second_chance,snwp.product_id,snwp.product_name,snwp.product_image,snwp.id as snw_product_id,snw.banner_image FROM " . $this->scratch_n_win_prize_winners_table . " wp JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = wp.snw_id LEFT JOIN " . $this->scratch_n_win_products_table . " snwp ON wp.snw_product_id = snwp.id WHERE (wp.is_redeemed = 2 OR wp.is_second_chance = 1) AND wp.member_id = '$member_id' AND snw.status = '1' AND wp.status = '1' AND wp.is_second_chance <> 1 AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND wp.status = 1 AND wp.prize_declare_at = '".$date."'";
			$winner_prizes_results = $this->_db->my_query($winner_prizes_query);
			if ($this->_db->my_num_rows($winner_prizes_results) > 0) {
				while($row = $this->_db->my_fetch_object($winner_prizes_results)) {
					// set product image
					$product_image = (isset($row->product_image) && !empty($row->product_image)) ? $row->product_image : '';
					$product_image = $this->product_img_path.$product_image;
					// set scratch n win banner image
					$banner_image	= (isset($row->banner_image) && !empty($row->banner_image)) ? trim($this->scatchnwin_img_path.$row->banner_image) : "";					
					// prepare prize values
					$array['prize_winner_id']  = (isset($row->snw_winner_id)) ? trim($row->snw_winner_id) : "";
					$array['snw_id']		   = (isset($row->snw_id)) ? trim($row->snw_id) : "";
					$array['product_name'] 	   = (isset($row->product_name)) ? trim($row->product_name) : "";
					$array['snw_product_id']   = (isset($row->snw_product_id)) ? trim($row->snw_product_id) : "";
					$array['prize_declare_at'] = (isset($row->prize_declare_at)) ? trim($row->prize_declare_at) : "";
					$array['product_image']    = $product_image;
					$array['snw_image'] 	   = $banner_image;
					$array['push_type'] 	   = $this->_config->getKeyValue("snw_push_type");
					$array['prize_avail_type'] = (isset($row->is_second_chance) && !empty($row->is_second_chance)) ? '3' : '1';
					// append saved prize array values
					$saved_prize_result[] = $array;
				}
			}
			
			// get crack the egg details whos not expired
			$cte_winner_prizes_query  = "SELECT ctew.cte_id,ctew.id,ctew.prize_declare_at,ctew.is_redeemed,ctew.qr_code,ctew.entry_points,cteb.id as brand_id,cteb.brand,cteb.brand_image,snw.banner_image FROM " . $this->cte_winners_table . " ctew JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = ctew.cte_id LEFT JOIN " . $this->cte_brands_table . " cteb ON ctew.brand_id = cteb.id WHERE ctew.member_id = '$member_id' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND ctew.prize_declare_at = '".$date."'";
			$cte_winner_prizes_results = $this->_db->my_query($cte_winner_prizes_query);
			if ($this->_db->my_num_rows($cte_winner_prizes_results) > 0) {
				while($row = $this->_db->my_fetch_object($cte_winner_prizes_results)) {
					// set product image
					$product_image  = (isset($row->brand_image) && !empty($row->brand_image)) ? $row->brand_image : '';
					$brand_id  		= (isset($row->brand_id) && !empty($row->brand_id)) ? $row->brand_id : '';
					$product_image  = $this->product_img_path.$product_image;
					$entry_point 	= (isset($row->entry_points) && !empty($row->entry_points)) ? (string)$row->entry_points : '0';
					$brand_name  	= (!empty($row->brand)) ? $row->brand : '';
					// set crack the egg banner image
					$banner_image	= (isset($row->banner_image) && !empty($row->banner_image)) ? trim($this->scatchnwin_img_path.$row->banner_image) : "";	
					// set coin position
					$coin_position = $this->get_coin_poistion($entry_point);
					// set notes based on return type
					$note1 = $this->_common->langText($this->_locale,'txt.cte.success.win_prize_note');
					$note1 = str_replace(array('{x}'), array($brand_name), $note1);			
					// prepare prize values
					$array['prize_winner_id']  = (isset($row->id)) ? trim($row->id) : "";
					$array['snw_id']		   = (isset($row->cte_id)) ? trim($row->cte_id) : "";
					$array['product_name'] 	   = (isset($row->brand)) ? trim($row->brand) : "";
					$array['is_redeemed'] 	   = (isset($row->is_redeemed)) ? trim($row->is_redeemed) : 0;
					$array['brand_id']  	   = $brand_id;
					$array['prize_declare_at'] = (isset($row->prize_declare_at)) ? trim($row->prize_declare_at) : "";
					$array['product_image']    = $product_image;
					$array['snw_image'] 	   = $banner_image;
					$array['qr_code_image']    = '';
					$array['coin_position']    = $coin_position;
					$array['entry_point'] 	   = $entry_point;
					$array['entry_lable']	   = $this->_common->langText($this->_locale,'txt.cte.entries');
					$array['entry_point_txt']  = $entry_point.' '.$this->_common->langText($this->_locale,'txt.cte.entries');
					$array['push_type'] 	   = $this->_config->getKeyValue("cte_push_type");
					$array['prize_avail_type'] = '1';
					$array['note1']			   = $note1;
					$array['expiry_note'] 	   = $this->_common->langText($this->_locale,'txt.cte.offer_expire_note');
					// append saved prize array values
					$saved_prize_result[] = $array;
				}
			}
			
			if(!empty($saved_prize_result) && count($saved_prize_result) > 0) {
				// append basic information in result array
				$_response['result_code'] = 1;
				$_response['message'] =  $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_prizes');
				$_response['prizes'] = $saved_prize_result;
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_prizes_no_found');
			}
			
			// manage notification count
			$notification_count = $this->update_notification_counts($member_id);
			$giveaways_count = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
			$saved_prize_count = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
		
			$_response['push_count'] = (isset($giveaways_count)) ? $giveaways_count : 0;
			$_response['saved_prize_count'] = (isset($saved_prize_count)) ? $saved_prize_count : 0;
		}
		return $_response;
	}
	
	/**
	 * Function used to get crack the egg prize info
	 * @param int snw_id , int member_id , int second_chance_count , int prize_avail_type
	 * @return int
	 * @access private
	 * 
	 */
	public function get_my_prize_data() {
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		$prize_winner_id = $this->_common->test_input($_REQUEST['prize_winner_id']);
		$brand_id = $this->_common->test_input($_REQUEST['brand_id']);
		$date  = date('Y-m-d');
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_member');
		if(!empty($member_id) && !empty($prize_winner_id)) {
			// get crack the egg details whos not expired
			$cte_winner_prizes_query  = "SELECT ctew.cte_id,ctew.id,ctew.prize_declare_at,ctew.is_redeemed,ctew.qr_code,ctew.entry_points,cteb.brand,cteb.brand_image,snw.banner_image,u.cte_qr_code_image FROM " . $this->cte_winners_table . " ctew JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = ctew.cte_id LEFT JOIN " . $this->cte_brands_table . " cteb ON ctew.brand_id = cteb.id JOIN ".$this->user_account_table." u ON u.acc_number = ctew.member_id WHERE ctew.member_id = '$member_id' AND ctew.id = '$prize_winner_id' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND ctew.prize_declare_at = '".$date."'";
			$cte_winner_prizes_results = $this->_db->my_query($cte_winner_prizes_query);
			if ($this->_db->my_num_rows($cte_winner_prizes_results) > 0) {
				$row = $this->_db->my_fetch_object($cte_winner_prizes_results);
				// set product image
				$brand_image    = (isset($row->brand_image) && !empty($row->brand_image)) ? $row->brand_image : '';
				$product_image  = $this->product_img_path.$brand_image;
				$entry_point 	= (isset($row->entry_points) && !empty($row->entry_points)) ? (string)$row->entry_points : '0';
				$brand_name  	= (!empty($row->brand)) ? $row->brand : '';
				// set crack the egg banner image
				$banner_image	= (isset($row->banner_image) && !empty($row->banner_image)) ? trim($this->scatchnwin_img_path.$row->banner_image) : "";	
				// convert qr code image in byte code
				if(!empty($row->cte_qr_code_image)) {
					$qr_path = IMG_URL.'qr_codes/'.$row->cte_qr_code_image;
					$type = pathinfo($qr_path, PATHINFO_EXTENSION);
					$data = $this->_common->file_get_contents_curl($qr_path);
					$qr_code_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
				}
				// set coin position
				$coin_position = $this->get_coin_poistion($entry_point);
				// convert image in byte code
				$brand_image = $this->convert_image_in_byte($brand_image,$this->product_img_path);
				// set notes based on return type
				$note1 = $this->_common->langText($this->_locale,'txt.cte.success.win_prize_note');
				$note1 = str_replace(array('{x}'), array($brand_name), $note1);			
				// prepare prize values
				$_response['prize_winner_id']  = (isset($row->id)) ? trim($row->id) : "";
				$_response['snw_id']		   = (isset($row->cte_id)) ? trim($row->cte_id) : "";
				$_response['product_name'] 	   = (isset($row->brand)) ? trim($row->brand) : "";
				$_response['is_redeemed'] 	   = (isset($row->is_redeemed)) ? trim($row->is_redeemed) : 0;
				$_response['prize_declare_at'] = (isset($row->prize_declare_at)) ? trim($row->prize_declare_at) : "";
				$_response['product_image']    = $brand_image;
				$_response['snw_image'] 	   = $banner_image;
				$_response['qr_code_image']    = (isset($qr_code_image)) ? $qr_code_image : '';
				$_response['coin_position']    = $coin_position;
				$_response['entry_point'] 	   = $entry_point;
				$_response['entry_lable']	   = $this->_common->langText($this->_locale,'txt.cte.entries');
				$_response['entry_point_txt']  = $entry_point.' '.$this->_common->langText($this->_locale,'txt.cte.entries');
				$_response['push_type'] 	   = $this->_config->getKeyValue("cte_push_type");
				$_response['prize_avail_type'] = '1';
				$_response['note1']			   = $note1;
				$_response['expiry_note'] 	   = $this->_common->langText($this->_locale,'txt.cte.offer_expire_note');
				$_response['redeem_bottom_note']   = $this->_common->langText($this->_locale,'txt.cte.redeem_bottom_note');
				$_response['additional_prize_note_1'] = $this->_common->langText($this->_locale,'txt.cte.additional_prize_note_1');
				$_response['additional_prize_note_2'] = '1';
				$_response['additional_prize_note_3'] = $this->_common->langText($this->_locale,'txt.cte.additional_prize_note_2');
				$_response['result_code']      = 1;
				$_response['message'] 		   = '';
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_expired');
			}
		}
		return $_response;
	 }
	
	/**
	 * Function used to generate random number
	 * @param null
	 * @return int
	 * @access private
	 * 
	 */
	private function random_key($max_value=2,$num_rows=1) {
		$values = range(0, $max_value);
		$counter = $max_value;
		for($i = 0; $i < $num_rows; $i++){
			$rand_num = rand(0,$counter);
			$current_number = $values[$rand_num];
			array_splice($values, $rand_num, 1);
			$counter--;
		}
		$current_number = (!empty($current_number)) ? $current_number : 1;
		return $current_number;
	}
	
	/**
	 * Function used to generate qr code and redeem product
	 * @param acc_number
	 * @return array
	 * @access public
	 * 
	 */
	public function redeem_now() {
		
		$date  = date('Y-m-d');
		$datetime  = date('Y-m-d H:i:s');
		$prize_winner_id = $this->_common->test_input($_REQUEST['prize_winner_id']);
		$push_type = $this->_common->test_input($_REQUEST['push_type']);
		$push_type = (!empty($push_type)) ? $push_type : $this->_config->getKeyValue("snw_push_type");
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_winner');
		if(!empty($prize_winner_id) && !empty($push_type)) {
			if($push_type == 5) {
				// manage scratch & win redeem now action
				$_response = $this->manage_snw_redeem_now($prize_winner_id);
			} else {
				// manage crack the egg redeem now action
				$_response = $this->manage_cte_redeem_now($prize_winner_id);
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to manage scratch & win redeem now action
	 * @param int snw_winner_id
	 * @return void
	 * @access private
	 * 
	 */
	private function manage_snw_redeem_now($snw_winner_id=0) {
		// default response error params
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_winner');
		// get winner's product prize log
		$winner_prizes_query  = "SELECT wp.member_id,wp.modified_at,snwp.product_id,snwp.promo_code,snwp.product_name,snwp.product_image,snw.title FROM " . $this->scratch_n_win_prize_winners_table . " wp JOIN " . $this->scratch_n_win_products_table . " snwp ON wp.snw_product_id = snwp.id JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id WHERE wp.snw_winner_id = '$snw_winner_id' AND snwp.status = 1";
		$winner_prizes_results = $this->_db->my_query($winner_prizes_query);
		if ($this->_db->my_num_rows($winner_prizes_results) > 0) {
			
			// fetch result object data
			$row = $this->_db->my_fetch_object($winner_prizes_results);
			// set product values
			$timeup_at   = (isset($row->modified_at) && !empty($row->modified_at)) ? date('Y-m-d H:i:s', strtotime($row->modified_at.' +10 minute')) : '';
			$title 		 = (isset($row->title) && !empty($row->title)) ? $row->title : 0;
			$product_id  = (isset($row->product_id) && !empty($row->product_id)) ? $row->product_id : 0;
			$promo_code  = (isset($row->promo_code) && !empty($row->promo_code)) ? $row->promo_code : 0;
			$member_id   = (isset($row->member_id) && !empty($row->member_id)) ? $row->member_id : 0;
			$product_img = (isset($row->product_image) && !empty($row->product_image)) ? $row->product_image : '';
			
			// convert product image in byte code
			if(!empty($product_img)) {
				$product_path = $this->product_img_path.$product_img;
				$type = pathinfo($product_path, PATHINFO_EXTENSION);
				$data = $this->_common->file_get_contents_curl($product_path);
				$product_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
			}
			
			// set qr code image name
			$error_correction_level = 'H';
			$matrix_point_size = 10;
			if(!empty($product_id) && !empty($member_id) && !empty($promo_code)) {
				// get barcode final digit
				$barcode_digits = $this->_common->get_barcode_digits($member_id);
				// set qr code
				$qr_code = $barcode_digits.'#'.$promo_code.'#'.$product_id; //memBerNo#promocode#prodnum
				$qr_filename = md5($qr_code.'|'.$error_correction_level.'|'.$matrix_point_size).'.png';
				include_once("qr_code.php"); // include to generate user barcode
				// add qr-code in customer's winner entry
				$update_array = array("qr_code" => $qr_filename,"is_redeemed" => 1,"modified_at" =>$datetime);
				$where_clause = array("snw_winner_id" => $snw_winner_id);
				$this->_db->UpdateAll($this->scratch_n_win_prize_winners_table, $update_array, $where_clause, "");
				// convert qr code image in byte code
				if(!empty($qr_filename)) {
					$qr_path = IMG_URL.'qr_codes/'.$qr_filename;
					$type = pathinfo($qr_path, PATHINFO_EXTENSION);
					$data = $this->_common->file_get_contents_curl($qr_path);
					$qr_code_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
				}
				
				// set success response data
				$_response['result_code'] 	= 1;
				$_response['snw_title'] 	= $title;
				$_response['product_id'] 	= $product_id;
				$_response['product_image'] = $product_image;
				$_response['product_image_src'] = (isset($product_path) && !empty($product_path)) ? $product_path : '';
				$_response['member_id']		= $member_id;
				$_response['qr_code_image'] = (isset($qr_code_image)) ? $qr_code_image : '';
				$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.success.scan_to_claim_prize');
			}
		} else {
			// change winner status as false
			$update_array = array("is_redeemed" => 0,"modified_at" =>$datetime);
			$where_clause = array("snw_winner_id" => $snw_winner_id);
			$this->_db->UpdateAll($this->scratch_n_win_prize_winners_table, $update_array, $where_clause, "");
			$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.snw_prize_no_found');
		}
		return $_response;
	}
	
	/**
	 * Function used to manage crack the egg redeem now action
	 * @param int prize_winner_id
	 * @return void
	 * @access private
	 * 
	 */
	private function manage_cte_redeem_now($prize_winner_id=0) {
		// default response error params
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_winner');
		// get winner's prize log
		$winner_prizes_query  = "SELECT snw.snw_id,ctew.member_id,ctew.modified_at,ctew.entry_points,cteb.id as brand_id,cteb.promo_code,cteb.brand,cteb.brand_image,snw.title,u.cte_qr_code_image FROM " . $this->cte_winners_table . " ctew JOIN " . $this->cte_brands_table . " cteb ON ctew.brand_id = cteb.id JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id JOIN ".$this->user_account_table." u ON u.acc_number = ctew.member_id WHERE ctew.id = '$prize_winner_id' AND cteb.status = 1";
		$winner_prizes_results = $this->_db->my_query($winner_prizes_query);
		if ($this->_db->my_num_rows($winner_prizes_results) > 0) {
			
			// fetch result object data
			$row = $this->_db->my_fetch_object($winner_prizes_results);
			// set brand values
			$cte_id 	 = (isset($row->snw_id) && !empty($row->snw_id)) ? $row->snw_id : 0;
			$title 		 = (isset($row->title) && !empty($row->title)) ? $row->title : 0;
			$brand_id  	 = (isset($row->brand_id) && !empty($row->brand_id)) ? $row->brand_id : 0;
			$entry_point = (isset($row->entry_points) && !empty($row->entry_points)) ? (string)$row->entry_points : '0';
			$promo_code  = (isset($row->promo_code) && !empty($row->promo_code)) ? $row->promo_code : 0;
			$member_id   = (isset($row->member_id) && !empty($row->member_id)) ? $row->member_id : 0;
			$product_img = (isset($row->brand_image) && !empty($row->brand_image)) ? $row->brand_image : '';
			$cte_qr_code_img = (isset($row->cte_qr_code_image) && !empty($row->cte_qr_code_image)) ? $row->cte_qr_code_image : '';
			
			// convert product image in byte code
			if(!empty($product_img)) {
				$product_path = $this->product_img_path.$product_img;
				$type = pathinfo($product_path, PATHINFO_EXTENSION);
				$data = $this->_common->file_get_contents_curl($product_path);
				$product_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
			}
			
			// set qr code image name
			$error_correction_level = 'H';
			$matrix_point_size = 10;
			if(!empty($brand_id) && !empty($member_id)) {
				
				if(!empty($cte_qr_code_img)) {
					$qr_path = IMG_URL.'qr_codes/'.$cte_qr_code_img;
					$type = pathinfo($qr_path, PATHINFO_EXTENSION);
					$data = $this->_common->file_get_contents_curl($qr_path);
					$qr_code_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
				} else {
					// get barcode final digit
					$barcode_digits = $this->_common->get_barcode_digits($member_id);
					// set qr code
					$qr_code = $barcode_digits; //memBerNo
					$qr_filename = md5($qr_code.'|'.$error_correction_level.'|'.$matrix_point_size).'.png';
					include_once("qr_code.php"); // include to generate user barcode
					// convert qr code image in byte code
					if(!empty($qr_filename)) {
						$qr_path = IMG_URL.'qr_codes/'.$qr_filename;
						$type = pathinfo($qr_path, PATHINFO_EXTENSION);
						$data = $this->_common->file_get_contents_curl($qr_path);
						$qr_code_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
					}
					// update cte redeemed qr code to member's profile
					$this->_db->UpdateAll($this->user_account_table, array("cte_qr_code_image" => $qr_filename), array("acc_number" => $member_id), "");
				}
				// update winner prize status as redeemed
				$update_array = array("is_redeemed" => 1);
				$where_clause = array("id" => $prize_winner_id);
				$this->_db->UpdateAll($this->cte_winners_table, $update_array, $where_clause, "");
				// set coin position
				$coin_position = $this->get_coin_poistion($entry_point);
				// set success response data
				$_response['result_code'] 	= 1;
				$_response['snw_title'] 	= $title;
				$_response['product_id'] 	= $product_id;
				$_response['coin_position'] = $coin_position;
				$_response['entry_point']   = $entry_point;
				$_response['entry_lable']   = $this->_common->langText($this->_locale,'txt.cte.entries');
				$_response['entry_point_txt'] = $entry_point.' '.$this->_common->langText($this->_locale,'txt.cte.entries');
				$_response['product_image'] = $product_image;
				$_response['product_image_src'] = (isset($product_path) && !empty($product_path)) ? $product_path : '';
				$_response['member_id']		= $member_id;
				$_response['qr_code_image'] = (isset($qr_code_image)) ? $qr_code_image : '';
				$_response['redeem_bottom_note']   = $this->_common->langText($this->_locale,'txt.cte.redeem_bottom_note');
				$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.success.scan_to_claim_prize');
			}
		}
		return $_response;
	}
	
	
	/**
	 * Function used to fetch customer's push count for active prizes
	 * @param int member_id
	 * @return void
	 * @access private
	 * 
	 */
	private function customer_push_count($member_id=0) {
        
		$push_count = 0;
		$current_day = strtolower(date('l'));
		if(!empty($member_id)) {
			$date= date('Y-m-d');
			$datetime = date('Y-m-d H:i:s');
			$datetimestr = strtotime(date('Y-m-d H:i:s'));
			// get customer's push count of perticuler scratch n win entry
			$snw_push_query  = "SELECT snw.snw_id,snw.title,snw.start_date,snw.end_date,snw.second_chance_draw,snw.second_chance_count,snw.banner_image,snwp.product_id,snwp.product_name,snwp.product_image,
				snwp.id as snw_product_id,pc.push_sent_at FROM " . $this->scratch_n_win_products_table . " snwp 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id
				JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
				WHERE snwp.product_image <> '' AND snw.campaign_type = '1'  AND snwp.status = 1 AND snwp.days_availability  like '%,$current_day,%' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date' AND NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = snwp.snw_id AND member_id = '$member_id' AND prize_declare_at = '$date') GROUP BY snwp.snw_id ORDER BY snw.snw_id DESC";
			$snw_push_result = $this->_db->my_query($snw_push_query);
			$active_prizes = array();
            if ($this->_db->my_num_rows($snw_push_result) > 0) {
				
				while($row = $this->_db->my_fetch_object($snw_push_result)) {
					$snw_member_id = (isset($row->member_id)) ? $row->member_id : "";
					$snw_member_ids = (isset($row->member_ids)) ? explode(',',$row->member_ids) : "";
					$push_sent_at	= (isset($row->push_sent_at)) ? date('Y-m-d',strtotime($row->push_sent_at)) : "";
					
					if($push_sent_at == $date) {
						// prepare prize values
						$array['snw_id']	= (isset($row->snw_id)) ? trim($row->snw_id) : "";
						// append active prize array values
						$active_prizes[] = $array;
					} 
				}
			}
            
               /* Check For Crack the egg */
                //~ $snw_push_query1  = "
                //~ (SELECT snw.snw_id,snw.campaign_type,DATE_ADD(ctecp.push_sent_at,INTERVAL snw.cte_expire_days DAY) AS cte_expire_days  FROM " . $this->scratch_n_win_products_table . " snwp 
				//~ JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id
				//~ JOIN " .$this->ava_cte_campaign_push. " ctecp ON ctecp.cte_campaign_id = snwp.snw_id
				//~ WHERE  snwp.product_image <> ''  AND snw.campaign_type = '3' AND (ctecp.status ='1')  AND ctecp.member_id='$member_id' AND snwp.status = 1 AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date'  AND (DATE_ADD(ctecp.push_sent_at,INTERVAL snw.cte_expire_days DAY)  >= '$datetime' OR (snw.end_date >= '$date' AND snw.cte_expire_days = '0' ) )
				//~ GROUP BY ctecp.id ORDER BY snw.snw_id DESC )";
				$snw_push_query1 = "SELECT snw.snw_id,snw.campaign_type,snw.title,snw.start_date,snw.end_date,snw.banner_image,pc.push_sent_at FROM " . $this->cte_brands_table . " cteb 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id
				JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = cteb.cte_id
				WHERE  cteb.brand_image <> ''  AND snw.campaign_type = '3' AND cteb.status = 1 AND pc.push_sent_at  = '$date' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date'
				AND NOT EXISTS (SELECT id FROM " . $this->cte_winners_table . " WHERE cte_id = cteb.cte_id AND member_id = '$member_id' AND prize_declare_at = '$date') GROUP BY cteb.cte_id ORDER BY snw.snw_id DESC";
                
                $snw_push_result1 = $this->_db->my_query($snw_push_query1);
                if ($this->_db->my_num_rows($snw_push_result1) > 0) {
                    while($row1 = $this->_db->my_fetch_object($snw_push_result1)) {
                        $campaign_type = (isset($row1->campaign_type)) ? $row1->campaign_type : "";
                        // prepare prize values
                            $array['snw_id']	= (isset($row->snw_id)) ? trim($row->snw_id) : "";                            
                            // append active prize array values
                            $active_prizes[] = $array;
                    }
                }
                
                
				
				if(count($active_prizes) > 0) {
					// append basic information in result array
					$push_count = count($active_prizes);
				}
            
		}
		return $push_count;
	}
	
	/**
	 * Function used to fetch customer's saved prize count
	 * @param int member_id
	 * @return void
	 * @access private
	 * 
	 */
	private function saved_prize_count($member_id) {
		$saved_prize_count = 0;
		if(!empty($member_id)) {
			$date= date('Y-m-d');
			// get scatchnwin details whos not expired
			$snw_winner_query  = "SELECT wp.snw_id FROM " . $this->scratch_n_win_prize_winners_table . " wp JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = wp.snw_id LEFT JOIN " . $this->scratch_n_win_products_table . " snwp ON wp.snw_product_id = snwp.id WHERE (wp.is_redeemed = 2 OR wp.is_second_chance = 1) AND wp.member_id = '$member_id' AND snw.status = '1' AND wp.status = '1' AND wp.is_second_chance <> 1 AND snw.status = 1 AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND wp.prize_declare_at = '".$date."'";
			$snw_prizes_results = $this->_db->my_query($snw_winner_query);
			$snw_saved_prize_count = $this->_db->my_num_rows($snw_prizes_results);
			// get cracktheegg details whos not expired
			$cte_winner_query  = "SELECT ctew.cte_id FROM " . $this->cte_winners_table . " ctew JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = ctew.cte_id WHERE ctew.member_id = '$member_id' AND snw.status = 1 AND snw.is_deleted = '0' AND snw.end_date >= '".$date."' AND ctew.prize_declare_at = '$date'";
			$cte_prizes_results = $this->_db->my_query($cte_winner_query);
			$cte_saved_prize_count = $this->_db->my_num_rows($cte_prizes_results);
			// total saved prizes
			$saved_prize_count = $snw_saved_prize_count+$cte_saved_prize_count;
		}
		return $saved_prize_count;
	}
	
	/**
	 * Function used to fetch customer's active prizes
	 * @param int member_id
	 * @return void
	 * @access private
	 * 
	 */
	public function active_prizes() {
		
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		$current_day = strtolower(date('l'));
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['push_count'] = 0;
		$_response['saved_prize_count'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_member');
		// get app versions
		$mini_ios_version = $this->_config->getKeyValue("mini_ios_version");
		$mini_android_version = $this->_config->getKeyValue("mini_android_version");
		// get cracktheEgg app versions
		$cte_mini_ios_version = $this->_config->getKeyValue("cte_mini_ios_version");
		$cte_mini_android_version = $this->_config->getKeyValue("cte_mini_android_version");
		if(!empty($member_id)) {
			$date= date('Y-m-d');
			$datetime= date('Y-m-d H:i:s');
			$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.no_active_prizes');
			// check member is exclude from competition or not
			$is_member_include_in_competition = $this->is_member_include_in_competition($member_id);
			if($is_member_include_in_competition == 0) {
				return $_response;
			}
			// get customer's push count of perticuler scratch n win entry
			/*$snw_push_query  = "SELECT snw.snw_id,snw.title,snw.start_date,snw.end_date,snw.second_chance_draw,snw.second_chance_count,snw.banner_image,snwp.product_id,snwp.product_name,snwp.product_image,
				snwp.id as snw_product_id,pc.push_sent_at FROM " . $this->scratch_n_win_products_table . " snwp 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id
				JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
				WHERE snwp.product_image <> '' AND snwp.status = 1 AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date'
				AND (snwp.total_prizes > (SELECT sum(product_sent_count) FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id) 
				OR NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_product_sent_log_table . " WHERE snw_id = snwp.snw_id AND product_id = snwp.product_id ) ) AND NOT EXISTS (SELECT id FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = snwp.snw_id AND member_id = '$member_id' AND prize_declare_at = '$date') GROUP BY snwp.snw_id ORDER BY snw.snw_id DESC";	
			*/
            
			$snw_push_query  = "(SELECT snw.snw_id,snw.campaign_type,snw.title,snw.start_date,snw.end_date,snw.banner_image,pc.push_sent_at FROM " . $this->scratch_n_win_products_table . " snwp 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = snwp.snw_id
				JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = snwp.snw_id
				JOIN ".$this->user_account_table." u ON u.acc_number = $member_id
				WHERE  snwp.product_image <> ''  AND snw.campaign_type = '1' AND snwp.status = 1 AND snwp.days_availability  like '%,$current_day,%' AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date'
				AND NOT EXISTS (SELECT snw_winner_id FROM " . $this->scratch_n_win_prize_winners_table . " WHERE snw_id = snwp.snw_id AND member_id = '$member_id' AND prize_declare_at = '$date') AND ((u.device_type = 'ios' AND u.mini_app_version >= '$mini_ios_version') OR (u.device_type = 'android' AND u.mini_app_version >= '$mini_android_version') ) GROUP BY snwp.snw_id ORDER BY snw.snw_id DESC ) 
                UNION
                (SELECT snw.snw_id,snw.campaign_type,snw.title,snw.start_date,snw.end_date,snw.banner_image,pc.push_sent_at FROM " . $this->cte_brands_table . " cteb 
				JOIN " . $this->scratch_n_win_table . " snw ON snw.snw_id = cteb.cte_id
				JOIN " .$this->scratch_n_win_push_log_table. " pc ON pc.snw_id = cteb.cte_id
				JOIN ".$this->user_account_table." u ON u.acc_number = $member_id
				WHERE  cteb.brand_image <> ''  AND snw.campaign_type = '3' AND cteb.status = 1 AND snw.status = '1' AND snw.is_deleted = '0' AND snw.start_date <= '$date' AND snw.end_date >= '$date'
				AND NOT EXISTS (SELECT id FROM " . $this->cte_winners_table . " WHERE cte_id = cteb.cte_id AND member_id = '$member_id' AND prize_declare_at = '$date') AND ((u.device_type = 'ios' AND u.mini_app_version >= '$cte_mini_ios_version') OR (u.device_type = 'android' AND u.mini_app_version >= '$cte_mini_android_version') ) GROUP BY cteb.cte_id ORDER BY snw.snw_id DESC)";	
			
			$snw_push_result = $this->_db->my_query($snw_push_query);
			if ($this->_db->my_num_rows($snw_push_result) > 0) {
				$active_prizes = array();
				while($row = $this->_db->my_fetch_object($snw_push_result)) {
					
                    $snw_id = (isset($row->snw_id)) ? trim($row->snw_id) : "";
					$campaign_type = (isset($row->campaign_type)) ? $row->campaign_type : "";
                    $push_type = 0;               
					$push_sent_at  = (isset($row->push_sent_at)) ? date('Y-m-d',strtotime($row->push_sent_at)) : "";
					// set campaign banner image
					$banner_image	= (isset($row->banner_image) && !empty($row->banner_image)) ? trim($this->scatchnwin_img_path.$row->banner_image) : "";
					if($push_sent_at == $date) {
						// prepare prize values
						$array['snw_id']	= $snw_id;
						$array['push_type']	= (isset($campaign_type) && $campaign_type == 1) ? '5' : '6';
						$array['title']		= (isset($row->title)) ? trim($row->title) : "";
						$array['start_date']	= (isset($row->start_date)) ? trim($row->start_date) : "";
						$array['end_date']		= (isset($row->end_date)) ? trim($row->end_date) : "";
						$array['push_sent_at']	= $push_sent_at;
						$array['snw_image'] = $banner_image;
						// append active prize array values
						$active_prizes[] = $array;
					} 
				}
				
				if(count($active_prizes) > 0) {
					// append basic information in result array
					$_response['result_code'] = 1;
					$_response['message'] =  $this->_common->langText($this->_locale,'txt.scratchnwin.success.snw_active_prizes');
					$_response['prizes'] = $active_prizes;
				}
			}
			// manage notification count
			$notification_count = $this->update_notification_counts($member_id);
			$giveaways_count = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
			$saved_prize_count = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
			
			$_response['push_count'] = (isset($giveaways_count)) ? $giveaways_count : 0;
			$_response['saved_prize_count'] = (isset($saved_prize_count)) ? $saved_prize_count : 0;
		}
		return $_response;
	}
	
	/**
	 * Function used to check user's competition existance 
	 * @param int acc_number
	 * @return void
	 * @access private
	 * 
	 */
	private function is_member_include_in_competition($acc_number=0) {
		$return_res = 0;
		// get member's log
		$member_query = "SELECT MEMB_NUMBER FROM MEMBER  WHERE MEMB_Exclude_From_Competitions = 0 AND MEMB_NUMBER = $acc_number";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$this->con);
		if($this->_mssql->pdo_num_rows_query($member_query,$this->con) > 0 ) {
			$return_res = 1;
		}
		return $return_res;
	}
	
	/**
	 * Function used to fetch customer's saved prize count
	 * @param int member_id
	 * @return void
	 * @access private
	 * 
	 */
	public function notification_counts() {
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		// set default response for invalid member
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.scratchnwin.error.invalid_member');
		$_response['push_count'] = 0;
		$_response['saved_prize_count'] = 0;
		if(!empty($member_id)) {
			// check member is exclude from competition or not
			$is_member_include_in_competition = $this->is_member_include_in_competition($member_id);
			if($is_member_include_in_competition == 0) {
				return $_response;
			}
			// get member's notification count
			$notification_query  = "SELECT id,giveaway_count,prize_count FROM " . $this->scratch_n_win_notification_log_table . " WHERE member_id = '$member_id'";
			$notification_results = $this->_db->my_query($notification_query);
			if ($this->_db->my_num_rows($notification_results) > 0) {
				// fetch result object data
				$row = $this->_db->my_fetch_object($notification_results);
				$giveaways_count = (isset($row->giveaway_count) && !empty($row->giveaway_count)) ? $row->giveaway_count : 0;
				$saved_prize_count = (isset($row->prize_count) && !empty($row->prize_count)) ? $row->prize_count : 0;
			}
			
			// update notification count
			$notification_count = $this->update_notification_counts($member_id);
			$giveaways_count = (isset($notification_count['push_count']) && !empty($notification_count['push_count'])) ? $notification_count['push_count'] : 0;
			$saved_prize_count = (isset($notification_count['saved_prize_count']) && !empty($notification_count['saved_prize_count'])) ? $notification_count['saved_prize_count'] : 0;
					
			// set response data
			$_response['result_code'] = 1;
			$_response['message'] = 'Notification counts';
			$_response['push_count'] = (isset($giveaways_count)) ? $giveaways_count : 0;
			$_response['saved_prize_count'] = (isset($saved_prize_count)) ? $saved_prize_count : 0;
		}
		$_response['result_code'] = 1;
		return $_response;
	}
	
	/**
	 * Function used to manage members notification counts
	 * @param int member_id, int is_viewed
	 * @return void
	 * @access public
	 * 
	 */
	public function update_notification_counts($member_id,$is_viewed='') {
		
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		// set default response for invalid member
		$_response['push_count'] = 0;
		$_response['saved_prize_count'] = 0;
		if(!empty($member_id)) {
			// get giveaways count
			$giveaways_count = $this->customer_push_count($member_id);
			// get saved prize count
			$saved_prize_count = $this->saved_prize_count($member_id);
			
			// get member's notification count
			$notification_query  = "SELECT id,viewed_count,modified_at FROM " . $this->scratch_n_win_notification_log_table . " WHERE member_id = '$member_id'";
			$notification_results = $this->_db->my_query($notification_query);
			
			if ($this->_db->my_num_rows($notification_results) > 0) {
				// fetch result object data
				$row = $this->_db->my_fetch_object($notification_results);
				// set modified date
				$modified_at = (isset($row->modified_at) && !empty($row->modified_at)) ? date('Y-m-d',strtotime($row->modified_at)) : '';
				// update member's notification count
				$update_array = array("giveaway_count" => $giveaways_count,"prize_count" => $saved_prize_count,"modified_at" =>$date);// Update values
				// set viewed count
				if($is_viewed == 1) {
					$viewed_count = $row->viewed_count+1;
					$update_array['viewed_count'] = intval($viewed_count);
				}
				
				if($modified_at != $date) {
					// update viewed count in case of date diffrence
					if($viewed_count > 0) {
						$update_array['viewed_count'] = 1;
					}
				}
				$where_clause = array("member_id" => $member_id);
				$this->_db->UpdateAll($this->scratch_n_win_notification_log_table, $update_array, $where_clause, "");	
			} else {
				// insert member's notification count
				$insert_query = "INSERT INTO ".$this->scratch_n_win_notification_log_table."(member_id,giveaway_count,prize_count,modified_at) VALUES ('$member_id','$giveaway_count','$prize_count','$date')";
				$this->_db->my_query($insert_query);
				$prize_winner_id = mysql_insert_id();
			}
			// set response data
			$_response['push_count'] = $giveaways_count;
			$_response['saved_prize_count'] = $saved_prize_count;
		}
		return $_response;
	}
	
	/**
	 * Function used to manage members second chance entry
	 * @param int member_id, int is_viewed
	 * @return void
	 * @access public
	 * 
	 */
	private function ms_member_second_chance($member_id=0,$snw_id=0,$snw_title='') {
		// set current date time
		$datetime = date('Y-m-d H:i:s');
		if(!empty($member_id) && !empty($snw_id) && !empty($snw_title)) {
			// insert data into second chance table
			$insert_sql = "INSERT SECOND_CHANCE_TBL (SecCh_Member_Id,SecCh_Snw_Id,SecCh_Campaign_Title,SecCh_Created_Date,SecCh_Modified_Date) VALUES ('$member_id','$snw_id','$snw_title','$datetime','$datetime')";
			$this->_mssql->my_mssql_query($insert_sql,$this->con);
		}
		return true;
	}
	
	/**
	 * Function to get product details from product id
	 * @param int product_id
	 * @return: obj array
	 * 
	 */
	public function get_product_data($product_id) {
		$prod_data = array();
		if(!empty($product_id)) {
			// get product's result
			$product_query  = "SELECT Prod_Desc,Prod_Number,PROD_POS_DESC FROM ".$this->prod_table." JOIN ".$this->outp_table." ON Prod_Number = OUTP_PRODUCT WHERE Prod_Number = $product_id";
			$product_result = $this->_mssql->my_mssql_query($product_query,$this->con);
			if ($this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0) {
				// fetch result object data
				$row = $this->_mssql->mssql_fetch_object_query($product_result);
				// set product data
				$prod_data['prod_desc'] = (isset($row->Prod_Desc) && !empty($row->Prod_Desc)) ? $row->Prod_Desc : '';
				$prod_data['prod_pos_desc'] = (isset($row->PROD_POS_DESC) && !empty($row->PROD_POS_DESC)) ? $row->PROD_POS_DESC : '';
			}
		}
		return $prod_data;
	}
	
	/**
	 * Function used to manage members winning entry
	 * @param int member_id, int is_viewed
	 * @return void
	 * @access public
	 * 
	 */
	private function ms_member_prize_entry($member_id=0,$snw_id=0,$snw_title='',$prod_number=0,$push_type=0) {
		// set current date time
		if($push_type == 5){
            $datetime = date('Y-m-d H:i:s');
            if(!empty($member_id) && !empty($snw_id) && !empty($snw_title) && !empty($prod_number)) {
                // insert data into prize winner table
                $insert_sql = "INSERT Prize_Winners_TBL (PW_MEMBER_ID,PW_Snw_Id,PW_Product_Number,PW_Campaign_Title,PW_CREATED_DATE,PW_MODIFIED_DATE) VALUES ('$member_id','$snw_id','$prod_number','$snw_title','$datetime','$datetime')";
                $this->_mssql->my_mssql_query($insert_sql,$this->con);
            }
            return true;
        }
	}
	
	function test_scratch_push() {
		$android_devices = $this->_common->test_input($_REQUEST['device_id']);
		$android_devices = array($android_devices);
		// get app customers
		$android_devices = array();
		$member_android_devices = array();
		$user_query = "SELECT device_token,device_type,email FROM ".$this->user_account_table." WHERE device_token <> '' AND device_type <> '' AND firstname like '%test%'";
		$user_result = $this->_db->my_query($user_query);
		if($this->_db->my_num_rows($user_result) > 0) {
			while($user_row = $this->_db->my_fetch_object($user_result)) {
				// set customer's device param
				$device_type = $user_row->device_type;
				$device_id = trim($user_row->device_token);
				$member_id = $user_row->email;
				
				if(!empty($device_id)) {
					if($device_type == 'android'){
						// set andriod device token in array
						$android_devices[] = $device_id;
						$member_android_devices[] = $member_id;
					}
				}
			}
		}
		
		
		echo '<pre>';
		print_r($member_android_devices);
		echo '================================';
		$snw_id = 93;
		$title = 'Test Scratch n Win 05 Nov';
		$date = date('Y-m-d');
	
		// send prizes andriod push 
		$this->_push->test_tab($android_devices,$snw_id,$title,$date);
		// set ios device token in array
		$ios_member_array['device_id'] = '8ff29da9c87f3133cedaec625cf4ac472d0eac82268f352701baf3fe5b9a0322';
		$ios_member_array['giveaway_count'] = 1;
		$ios_member_array['prize_count'] = 1;
		$ios_member_array['viewed_count'] = 1;
		$ios_devices[] = $ios_member_array;
		$date = date('Y-m-d');
		//$this->_push->sendIphonePushNotification_prize($ios_devices,1,'Ignore..For testing',$date,1);
		return true;
	 }
	 
	 function fcm_push() {
		// Replace with real client registration IDs ('Device ID')
		$registatoin_ids = array('doGxONsFIGY:APA91bEwkg9GiGeXaCgbXcLmnLr7o73zX3siuy0ayt52wfKzoAQ-gJSn8x3ZrYbtRBUkE9m65qQRHGPH1xRaTjad2FlYq8HbnGa54_5jX6IIMbvjkxV2ZBIsUBxCDWk0ytwmFzdeXVxf','c3DzmXwJWb8:APA91bFWLyETSgxxokkN9rHfnWzjQ21qi8u6SY5QmX28eSJmoBn9v-LOjclQvynxzF9VERTz0B4xhsLYoqweliuxaucFJ1yffthMcWo4qhVjB2b22V3Ij_vuv_q9GnTngedbylZb_ruo');
		$message = array('offer_id'=>1,'offer_name'=>'Test Order','offer_short_description'=>'Test Order','push_type'=>3);
		
		$fields = array (
		'registration_ids' => $registatoin_ids,
		'notification' => $message,
		"data" => $message
		);

		$ch = curl_init("https://fcm.googleapis.com/fcm/send");

		$header=array('Content-Type: application/json',
		"Authorization: key=AIzaSyAhmngWQ9LooPBIU4Ys2R_f23FJC1J2Wb8");

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);
		curl_close($ch);
		echo '<pre>';
		print_r($result);die;
	 }
     
     function remove_winner() {
		
		$member_id = $this->_common->test_input($_REQUEST['acc_number']);
		$date = date('Y-m-d');
		$member_id = (!empty($member_id)) ? $member_id : '2100000897';
		// remove favourite cart item data
		$remove_sql = "DELETE FROM ".$this->scratch_n_win_prize_winners_table." WHERE member_id = '$member_id' AND prize_declare_at = '$date'";
		$this->_db->my_query($remove_sql);
        
       // remove favourite cart item data
		$remove_sql = "DELETE FROM ".$this->cte_winners_table." WHERE member_id = '$member_id' AND prize_declare_at = '$date'";
		$this->_db->my_query($remove_sql);
        
		$_response['result_code'] = 1;
		return $_response;
	}
    
    
    function prize_winner_table_pushtype_entry() {
		
		// set today product sent count
		$todays_product_query  = "SELECT snw_id,snw_winner_id FROM " . $this->scratch_n_win_prize_winners_table;
		$todays_product_result = $this->_db->my_query($todays_product_query);
		// fetch result object data
		while($row = $this->_db->my_fetch_object($todays_product_result)){
            $snw_id = $row->snw_id;
            $snw_winner_id = $row->snw_winner_id;
            
			// set today product sent count
			$query  = "SELECT campaign_type FROM ".$this->scratch_n_win_table ." WHERE snw_id = '$snw_id'";
			$result = $this->_db->my_query($query);
			// fetch result object data
			$rowR = $this->_db->my_fetch_object($result);
            $campaign_type = $rowR->campaign_type;
            if($campaign_type == 1){
                $push_type = 5;
            }else if($campaign_type == 2){
                $push_type = 7;
            }else if($campaign_type == 3){
                $push_type = 6;
            }
			$update_array['push_type'] = $push_type;
			$where_clause = array("snw_winner_id" => $snw_winner_id);
			$this->_db->UpdateAll($this->scratch_n_win_prize_winners_table, $update_array, $where_clause, "");	 
        }
	}
	
} // End users class

?> 
