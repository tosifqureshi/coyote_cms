<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Amit
 * Timestamp : Jan-24
 * Copyright : Coyote team
 *
 */
class sendpush {
    public $_response = array();
    public $result = array();
    public $iosPush = array();
    public $androidPush = array();
    private $offers_table = 'ava_offers';
    private $beacon_offers_table = 'ava_beacon_offers';
    private $beacons_table = 'ava_beacons';
    private $stores_table = 'ava_stores';
    private $beacon_customer_table = 'ava_beacon_customer_offer';
    private $push_activities_table = 'ava_push_activities';
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
    }
    /**
     * @method get_offer() return the offers w.r.t. beacon_id and store_id .
     * @access public
     * @return array
     */
     
        public function send_beacon_offer_push() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$beacon_offers = array();
		$selectOffer = "SELECT * FROM " . $this->beacon_offers_table . "  WHERE status = '1' AND is_simple_offer = '1' AND is_delete = '0' AND ((start_date < '".$date."' AND end_date > '".$date."') OR (start_date = '".$date."' AND start_time < '".$time."') OR (end_date = '".$date."' AND end_time < '".$time."'))";
		$resultOffer = $this->_db->my_query($selectOffer);
		if ($this->_db->my_num_rows($resultOffer) > 0) { 
			while($row = $this->_db->my_fetch_object($resultOffer)) {
			$beacon_offers[$row->beacon_offer_id]['beacon_offer_id'] 	= 	$row->beacon_offer_id;
			$beacon_offers[$row->beacon_offer_id]['offer_name']	 	= $row->offer_name;
			$beacon_offers[$row->beacon_offer_id]['offer_short_description']	= $row->offer_short_description; 
			}
			foreach($beacon_offers as $beacon_offer) {
				$beacon_offer_id 	= 	$beacon_offer['beacon_offer_id'];
				$offer_name		 	= $beacon_offer['offer_name'];
				$offer_short_description	= $beacon_offer['offer_short_description'];
				
			
			$custArray = array();
			$custFinal = array();
			$selectPush = "SELECT * FROM ".$this->push_activities_table." WHERE beacon_offer_id = '$beacon_offer_id'";
			$resultPush = $this->_db->my_query($selectPush);
			if ($this->_db->my_num_rows($resultPush) > 0) { 
				$row = $this->_db->my_fetch_object($resultPush);
				$beacon_offer_id	= $row->beacon_offer_id;
				$push_activities_id	= $row->push_activities_id;
				if($row->is_push_send == 0){
					
					$selectCustomer = "SELECT id,device_token,device_type FROM ava_users WHERE active = 1 AND is_push_send != 1";
					$resultCustomer = $this->_db->my_query($selectCustomer);
					while($rowCust = $this->_db->my_fetch_object($resultCustomer)) {
						$id = $rowCust->id ;
						$device_token = $rowCust->device_token ;
						$device_type = $rowCust->device_type ;
						if($device_token != '' && $device_type != '') {
							$custArray['ACC_NUMBER']	= $id ;
							$custArray['device_token'] 	= $device_token ;
							$custArray['device_type'] 	= $device_type ;
							$custFinal[] = $custArray; 
						}
					}
					$addCust = "";
					foreach($custFinal as $value) {
						if ($value['device_type'] == 'ios') {
							$iosPush[$value['device_token']]['deviceId']    	= $value['device_token'];
							$iosPush[$value['device_token']]['offer_id'] 		= $beacon_offer_id;
							$iosPush[$value['device_token']]['offer_name'] 		= $offer_name;
							$iosPush[$value['device_token']]['offer_short_description'] = $offer_short_description;
						} else {
							$androidPush[$value['device_token']]['deviceId']    = $value['device_token'];
							$androidPush[$value['device_token']]['offer_id'] 	= $beacon_offer_id;
							$androidPush[$value['device_token']]['offer_name'] 	= $offer_name;
							$androidPush[$value['device_token']]['offer_short_description'] = $offer_short_description;
						}
						$addCust .= $value['ACC_NUMBER'].',';
					}
					if(!empty($iosPush)) {                 
						   $this->_push->sendIphonePushNotification($iosPush);
						} 
						
						if(!empty($androidPush)) {                 
						   $this->_push->sendAndroidPushNotification($androidPush);
						}
					$updateArray = array("customer_ids" => $addCust,"is_push_send" => 1,"send_at" => $datetime);
					$whereClause = array("push_activities_id" => $push_activities_id);
					$update = $this->_db->UpdateAll($this->push_activities_table, $updateArray, $whereClause, "");
					
					$_response['result_code'] = 1;
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.success.send');
				} else {
					$_response['result_code'] = 0;
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.error.no_offer');
				}
			} else {
				$insertPush = "INSERT INTO ".$this->push_activities_table."(beacon_offer_id,is_push_send,added_at) VALUES ('$beacon_offer_id','0','$datetime')";
				$this->_db->my_query($insertPush);						
				$_response['result_code'] = 1;
				$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.success.added');
			} 
		} } else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.error.no_offer');
		}
		return $_response;
	} 
    
    public function send_beacon_offer_push_old1() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		$selectOffer = "SELECT * FROM " . $this->beacon_offers_table . "  WHERE status = '1' AND is_simple_offer = '1' AND is_delete = '0' AND (start_time <= '".$datetime."' AND end_time >= '".$datetime."')";
		$resultOffer = $this->_db->my_query($selectOffer);
		if ($this->_db->my_num_rows($resultOffer) > 0) { 
			$row = $this->_db->my_fetch_object($resultOffer);
			$beacon_offer_id 	= 	$row->beacon_offer_id;
			$offer_name		 	= $row->offer_name;
			$offer_short_description	= $row->offer_short_description; 
			$custArray = array();
			$custFinal = array();
			$selectPush = "SELECT * FROM ".$this->push_activities_table." WHERE beacon_offer_id = '$beacon_offer_id'";
			$resultPush = $this->_db->my_query($selectPush);
			if ($this->_db->my_num_rows($resultPush) > 0) { 
				
				$row = $this->_db->my_fetch_object($resultPush);
				$beacon_offer_id	= $row->beacon_offer_id;
				$push_activities_id	= $row->push_activities_id;
				if($row->is_push_send == 0){
				
					$selectCustomer = "SELECT id,device_token,device_type FROM ava_users WHERE active = 1 AND is_push_send = 0";
					$resultCustomer = $this->_db->my_query($selectCustomer);
					while($rowCust = $this->_db->my_fetch_object($resultCustomer)) {
						$id = $rowCust->id ;
						$device_token = $rowCust->device_token ;
						$device_type = $rowCust->device_type ;
						if($device_token != '' && $device_type != '') {
							$custArray['ACC_NUMBER']	= $id ;
							$custArray['device_token'] 	= $device_token ;
							$custArray['device_type'] 	= $device_type ;
							$custFinal[] = $custArray; 
						}
					}
					
					$addCust = "";
					foreach($custFinal as $value) {
						if ($value['device_type'] == 'ios') {
							$iosPush[$value['device_token']]['deviceId']    	= $value['device_token'];
							$iosPush[$value['device_token']]['offer_id'] 		= $beacon_offer_id;
							$iosPush[$value['device_token']]['offer_name'] 		= $offer_name;
							$iosPush[$value['device_token']]['offer_short_description'] = $offer_short_description;
						} else {
							$androidPush[$value['device_token']]['deviceId']    = $value['device_token'];
							$androidPush[$value['device_token']]['offer_id'] 	= $beacon_offer_id;
							$androidPush[$value['device_token']]['offer_name'] 	= $offer_name;
							$androidPush[$value['device_token']]['offer_short_description'] = $offer_short_description;
						}
						$addCust .= $value['ACC_NUMBER'].',';
					}
					// send ios push notifications
					if(!empty($iosPush)) {                 
					   $this->_push->sendIphonePushNotification($iosPush);
					} 
					
					// send android push notifications	
					if(!empty($androidPush)) {                 
					  $this->_push->sendAndroidPushNotification($androidPush);
					}
					
					$updateArray = array("customer_ids" => $addCust,"is_push_send" => 1,"send_at" => $datetime);
					$whereClause = array("push_activities_id" => $push_activities_id);
					$update = $this->_db->UpdateAll($this->push_activities_table, $updateArray, $whereClause, "");
					
					$_response['result_code'] = 1;
					$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.success.send');
				} else {
					$_response['result_code'] = 0;
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.error.no_offer');
				}
			} else {
				$insertPush = "INSERT INTO ".$this->push_activities_table."(beacon_offer_id,is_push_send,added_at) VALUES ('$beacon_offer_id','0','$datetime')";
				$this->_db->my_query($insertPush);						
				$_response['result_code'] = 1;
				$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.success.added');
			} 
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.error.no_offer');
		}
	
		return $_response;
	}
     
    public function send_beacon_offer_push_old() {
		$datetime = date('Y-m-d H:i:s');
		$selectPush = "SELECT * FROM ".$this->push_activities_table." WHERE is_push_send = '0'";
		$resultPush = $this->_db->my_query($selectPush);
		if ($this->_db->my_num_rows($resultPush) > 0) { 
			$row = $this->_db->my_fetch_object($resultPush);
			$beacon_offer_id	= $row->beacon_offer_id;
			$push_activities_id	= $row->push_activities_id;
			$selectOffer = "SELECT * FROM " . $this->beacon_offers_table . " WHERE beacon_offer_id = '$beacon_offer_id'";
			$resultOffer = $this->_db->my_query($selectOffer);
			if ($this->_db->my_num_rows($resultOffer) > 0) { 
				$row = $this->_db->my_fetch_object($resultOffer);
				$beacon_offer_id 				= $row->beacon_offer_id;
				$offer_name		 				= $row->offer_name;
				$offer_short_description		= $row->offer_short_description; 
				$custArray = array();
				$custFinal = array();
				$selectCustomer = "SELECT ava_account.ACC_NUMBER,ava_account_data.device_token,ava_account_data.device_type FROM ava_account_data,ava_account WHERE ava_account_data.ACC_NUMBER = ava_account.ACC_NUMBER AND ava_account.ACC_STATUS = 1 ";
				$resultCustomer = $this->_db->my_query($selectCustomer);
				while($rowCust = $this->_db->my_fetch_object($resultCustomer)) {
					$ACC_NUMBER = $rowCust->ACC_NUMBER ;
					$device_token = $rowCust->device_token ;
					$device_type = $rowCust->device_type ;
					if($device_token != '' || $device_type != '') {
						$custArray['ACC_NUMBER']	= $ACC_NUMBER ;
						$custArray['device_token'] 	= $device_token ;
						$custArray['device_type'] 	= $device_type ;
						$custFinal[] = $custArray; 
					}
				}
				$addCust = "";
				foreach($custFinal as $value) {
					if ($value['device_type'] == 'ios') {
						$iosPush['deviceId']    	= $value['device_token'];
						$iosPush['offer_id'] 		= $beacon_offer_id;
						$iosPush['offer_name'] 		= $offer_name;
						$iosPush['offer_short_description'] = $offer_short_description;
						$iosArray[] = $iosPush;
						unset($iosPush);
					} else {
						$androidPush['deviceId']    = $value['device_token'];
						$androidPush['offer_id'] 	= $beacon_offer_id;
						$androidPush['offer_name'] 	= $offer_name;
						$androidPush['offer_short_description'] = $offer_short_description;
						$androidArray[] = $androidPush;
						unset($androidArray);
					}
					
					if(!empty($iosArray)) {                 
                       $this->_push->sendIphonePushNotification($iosArray);
                    } 
                    
                    if(!empty($androidArray)) {                 
                       $this->_push->sendAndroidPushNotification($androidArray);
                    }
                    $addCust .= $value['ACC_NUMBER'].',';
				}
				$updateArray = array("customer_ids" => $addCust,"is_push_send" => 1,"send_at" => $datetime);
				$whereClause = array("push_activities_id" => $push_activities_id);
				$update = $this->_db->UpdateAll($this->push_activities_table, $updateArray, $whereClause, "");
							
				$_response['result_code'] = 1;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.success');
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.error.no_offer');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_beacon_offer_push.error.no_push');
		} 
	return $_response;
	}
} // End sendPush class

?> 
