<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : Tosif Qureshi
 * Timestamp : June-28 3:10PM
 * Copyright : Coyote team
 *
 */
class stock {
    public $_response = array();
    public $result = array();
    private $user_table = 'USERTBL';
	private $OUTPTBL = 'OUTPTBL';
	private $OUTLTBL = 'OUTLTBL';
    private $PRODTBL = 'PRODTBL';
    private $CODETBL = 'CODETBL';
	private $TRXTBL  = 'TRXTBL';
    private $APNTBL  = 'APNTBL';
    private $OrdhTbl = 'OrdhTbl';
    private $OrdlTbl = 'OrdlTbl';
    private $TillTbl = 'TillTbl';
    private $FIFOTbl = 'FIFOTbl';
    private $sales_report_queue = 'ava_sales_report_queue';
    private $print_label_table = 'ava_print_label';
	private $app_setting_table = 'ava_app_settings';

    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_mssql = new mssql(); // Create instance of mssql class
		$this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
		
    }
    
     
    /**
	 * Function used to check current active app configuration
	 * @param string app_version , string os_version , string device_type
	 * @return array
	 * 
	 */
	function check_app_version() {
		//get post user params
		$avc = (isset($_REQUEST['app_version'])) ? $_REQUEST['app_version'] : "";
		$os = (isset($_REQUEST['os_version'])) ? $_REQUEST['os_version'] : "";
		$device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
		$return_response = 1;
		if(!empty($avc) && !empty($os) && !empty($device_type)) {
			// query to fetch app configuration
			$sql = "SELECT * FROM " . $this->app_setting_table." where device_type = '".$device_type."' and app_type = 2" ;
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				
				if(isset($row->mini_app_version) && $avc<$row->mini_app_version) {
					$return_response = 0;
				}
			}
		}
		return $return_response;
	}
    
    /**
     * Function used to get user's outlet listing
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function outlets() {
		
		//get post user params
		$user_id  = $this->_common->test_input($_REQUEST['user_id']);
		if (empty($user_id)) {
            $_response['result_code'] = 0;
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
        } else {
			// checking user email is already exists or not
			$user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id; 
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
				// get user row data
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				// get associated zone results from user's zone 
				$user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
				if(!empty($user_outlets)) {
					// append user result array in response
					$_response['result'] = $user_outlets;
					$_response['result_code'] = 1;
				} else {
					$_response['result_code'] = 0;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.error.suppliernot_associate');
				}
			} else {
				$_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
		}
		return $_response;
	 }
	 
	 /**
	 * Function used to get user's zone outlets
	 * @param string user_zone , string outlet_id 
	 * @return array
	 * @access private
	 * 
	 */
	 private function get_user_outlets($user_zone='',$outlet_id=0) {
		// initialize zone outlet array
		$user_outlets = array();
		$outlet_id = trim($outlet_id);
        $user_zone = trim($user_zone);
		
		if(!empty($outlet_id)) {
			$outlet_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_OUTLET in ('".$outlet_id."') ORDER BY OUTL_DESC ASC";
		} else if(!empty($user_zone)) {
			$outlet_query = "SELECT CODE_KEY_NUM as outlet_id ,OUTL_DESC as outlet_name FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '".$user_zone."'";
		} else {
			$outlet_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_STATUS = 'Active' and SubString(OUTL_DESC, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY OUTL_DESC ASC";
		}
		$get_outlet_data = $this->_mssql->my_mssql_query($outlet_query,$this->con );
		if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
			while($row = $this->_mssql->mssql_fetch_object_query($get_outlet_data)) {
		
				// prepare outlet values
				$outlet_array['outlet_id'] = $row->outlet_id;
				$outlet_array['outlet_name'] = $row->outlet_name.' '.$row->outlet_id;
				$user_outlets[] = $outlet_array;
			}
		}
		
		return $user_outlets;
	}
	
	/**
	 * Function used to get products listing
	 * @param int outlet_id, int limit, int offset
	 * @return array
	 * @access public
	 * 
	 */
	public function products() {
		
		// get post data
		$customer_id = $this->_common->test_input($_REQUEST['user_id']);
		$outlet_id   = $_REQUEST['outlet_id'];
		$search_keyword = $_REQUEST['search_keyword'];
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		$supplier = $this->_common->test_input($_REQUEST['supplier']);
        $order_id = $this->_common->test_input($_REQUEST['order_id']);
        $order_id = !empty($order_id)?$order_id:0; 
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
		if(!empty($outlet_id)) {
			
			// get total count of outlet's products
			$product_count_query = "SELECT count(Prod_Number) as product_count FROM  " .$this->OUTPTBL. " JOIN " .$this->PRODTBL. " on Outp_Product = Prod_Number WHERE OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'";
			$product_count_query1 = "SELECT count(Prod_Number) as product_count FROM  " .$this->OUTPTBL. " JOIN " .$this->PRODTBL. " on Outp_Product = Prod_Number WHERE OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'";
			// merge search entity in query
			
             $directSearchFlag =0;
            
            if(!empty($search_keyword)) {
				if(is_numeric($search_keyword)) {               
                    $directSearchFlag =1;
					$product_count_query .= " AND Prod_Number = $search_keyword";
				} else {
					// set search key value
					$search_keyword1 = str_replace("'","''",$search_keyword);
					$product_count_query .= " AND Prod_Desc like '%$search_keyword%'";
				}
			}
                    
			$product_count_data = $this->_mssql->my_mssql_query($product_count_query,$this->con);    
			$product_count_query_data = $this->_mssql->my_mssql_query($product_count_query,$this->con);
			$product_count_data1 = $this->_mssql->mssql_fetch_object_query($product_count_query_data);
			/*Check For APN Number */  
			
			if(($directSearchFlag == 1) && ($product_count_data1->product_count == 0) && !empty($search_keyword)){
					$prod_apn_query = "SELECT APN_PRODUCT FROM ".$this->APNTBL." where APN_NUMBER = '".$search_keyword."'";
					$get_prod_apn_data = $this->_mssql->my_mssql_query($prod_apn_query,$this->con);
					$prod_apn_numbers = array();
					$APN_PRODUCT = '';
					if( $this->_mssql->pdo_num_rows_query($prod_apn_query,$this->con) > 0 ) {
						while( $result = $this->_mssql->mssql_fetch_object_query($get_prod_apn_data) ) {
							// set apn values
							$APN_PRODUCT  = (isset($result->APN_PRODUCT)) ? trim($result->APN_PRODUCT) : "";
						}
					}
				
				if(!empty($APN_PRODUCT)){
					   $product_count_query = $product_count_query1." AND Prod_Number = $APN_PRODUCT";
					   $product_count_data = $this->_mssql->my_mssql_query($product_count_query,$this->con);
					   $search_keyword = $APN_PRODUCT;
				}	
			}
        
			if( $this->_mssql->pdo_num_rows_query($product_count_query,$this->con) > 0 ) {
				$product_count = $this->_mssql->mssql_fetch_object_query($product_count_data);
				// get outlets's products listing
				$product_query = "SELECT * FROM ( SELECT Prod_Number, Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,OUTP_MIN_ONHAND,OUTP_MAX_ONHAND,OUTP_MIN_REORDER_QTY,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row,PROD_PARENT FROM ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number where OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'";
				// merge search entity in query
				if(!empty($search_keyword)) {
					if(is_numeric($search_keyword)) {
						$product_query .= " AND Prod_Number = $search_keyword";
					} else {
						// set search key value
						$search_keyword2 = str_replace("'","''",$search_keyword);
						$product_query .= " AND Prod_Desc like '%$search_keyword2%'";
					}
				}
				// set limit string in query
				$product_query .= ") a WHERE row > ".$offset." AND row <= ".$limit;
				$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
				$products = array();
				 
				if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
					while( $result = $this->_mssql->mssql_fetch_object_query($get_product_data) ) {
						// set carton cost
						$prod_carton_qty = (isset($result->PROD_CARTON_QTY)) ? trim($result->PROD_CARTON_QTY) : "";
						$unit_cost = (isset($result->UnitCost)) ? trim($result->UnitCost) : "";
						$prod_carton_cost = (isset($result->PROD_CARTON_COST)) ? trim($result->PROD_CARTON_COST) : "";
                        $prod_parent = (isset($result->PROD_PARENT)) ? trim($result->PROD_PARENT) : 0;
                        $prod_number = (isset($result->Prod_Number)) ? trim($result->Prod_Number) : 0;
						$carton_cost = $prod_carton_qty*$unit_cost;
						// get supplier item number
						if(!empty($supplier) && isset($result->Prod_Number)) {
							$prod_number = $result->Prod_Number;
							$supplier_item_query = "SELECT SPRD_SUPPLIER_ITEM FROM SPRDTBL where SPRD_SUPPLIER = '$supplier' and SPRD_PRODUCT = $prod_number";
							$supplier_item_result = $this->_mssql->my_mssql_query($supplier_item_query,$this->con);
							if($this->_mssql->pdo_num_rows_query($supplier_item_query,$this->con) > 0 ){
								$supplier_item_row = $this->_mssql->mssql_fetch_object_query($supplier_item_result);
								$supplier_item = (isset($supplier_item_row->SPRD_SUPPLIER_ITEM) && !empty($supplier_item_row->SPRD_SUPPLIER_ITEM)) ? $supplier_item_row->SPRD_SUPPLIER_ITEM : '';
							}
						}
						
						// set product values
						$array['prod_desc'] 	= (isset($result->Prod_Desc)) ? utf8_encode($result->Prod_Desc) : "";
						$array['prod_number']	= (isset($result->Prod_Number)) ? trim($result->Prod_Number) : "";
						$array['prod_pos_desc'] = (isset($result->PROD_POS_DESC)) ? utf8_encode($result->PROD_POS_DESC) : "";
						$array['prod_unit_qty'] = (isset($result->PROD_UNIT_QTY)) ? trim($result->PROD_UNIT_QTY) : "";
						$array['prod_carton_qty'] = $prod_carton_qty;
						$array['prod_carton_cost']	= number_format($carton_cost,2, '.', '');
						//$array['unit_cost'] 	  	= number_format($unit_cost,2, '.', '');
						$array['unit_cost'] 	  	= $unit_cost;
						$array['unit_on_hand']   	= (isset($result->OUTP_QTY_ONHAND)) ? trim($result->OUTP_QTY_ONHAND) : "";
						$array['min_stock_level'] 	= (isset($result->OUTP_MIN_ONHAND)) ? trim($result->OUTP_MIN_ONHAND) : "";
						$array['max_stock_level'] 	= (isset($result->OUTP_MAX_ONHAND)) ? trim($result->OUTP_MAX_ONHAND) : "";
						$array['min_reorder_qty'] 	= (isset($result->OUTP_MIN_REORDER_QTY)) ? trim($result->OUTP_MIN_REORDER_QTY) : "";
						$array['supplier_item_number'] = (isset($supplier_item)) ? $supplier_item : '';
                        
                                /* Code To Add New FIELD IN GIVEN SERVICE - 20-06-2017 */
                    /* -------------         Start         --------------- */
                    $productFilter = '';
                    $productFilterString = '';
                    if(!empty($prod_number)){
                        $productFilter .= $prod_number.',';
                    }
                    if(!empty($prod_parent)){
                        $productFilter .= $prod_parent.',';
                    }
                    if(!empty($productFilter)){
                        $productFilter = rtrim($productFilter,',');
                        $productFilterString = " (ORDL_PRODUCT in ($productFilter)) AND ";    
                    }
                    if(!empty($order_id)){
                        $order_idFilter = trim($order_id);
                        $order_idFilterString = " (ORDL_ORDER_NO <> $order_idFilter) AND ";    
                    }
                    $array['min_on_hand'] = '';
                    $array['stock_on_hand'] = '';
                    $array['promotion_unit'] = '';
                    $array['already_on_order'] = '';
                    $array['current_order_amount'] = '';
                    if(!empty($outlet_id) && !empty($prod_number)){
                        $product_query =  "SELECT * FROM (SELECT OUTP_QTY_ONHAND as STOCK_ON_HAND,OUTP_MIN_ONHAND as MIN_ON_HAND,ORDL_TOTAL_UNITS as CURRENT_ORDER_AMOUNT, ORDL_Promo_Units as PROMOTION_UNIT,
                        ( select case coalesce(PROD_PARENT, 0)
                        when 0 then SUM(ORDL_TOTAL_UNITS) else SUM(ORDL_TOTAL_UNITS) * PROD_UNIT_QTY end AS UNITS_ON_ORDER
                        from OrdlTbl join PRODTBL on Prod_number = ORDL_PRODUCT where $productFilterString $order_idFilterString ORDL_OUTLET = $outlet_id AND ORDL_DOCUMENT_TYPE = 'ORDER' and ORDL_DOCUMENT_STATUS = 'ORDER' group by PROD_PARENT, PROD_UNIT_QTY )  as ALREADY_ON_ORDER
                         FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number LEFT JOIN ORDLTBL ON ( ORDL_OUTLET = OUTP_OUTLET AND Prod_Number = ORDL_PRODUCT AND ORDL_DOCUMENT_TYPE = 'ORDER') where OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'  AND Prod_Number = $prod_number) productQ";
                        $product_query_result = $this->_mssql->my_mssql_query($product_query,$this->con);
                        if( $this->_mssql->pdo_num_rows_query($prod_apn_query,$this->con) > 0 ) {
                        $productquery_row = $this->_mssql->mssql_fetch_object_query($product_query_result);
                            $array['min_on_hand'] = (!empty($productquery_row->MIN_ON_HAND)) ? $productquery_row->MIN_ON_HAND : 0;
                            $array['stock_on_hand'] = (!empty($productquery_row->STOCK_ON_HAND)) ? $productquery_row->STOCK_ON_HAND : 0;
                            $array['promotion_unit'] = (!empty($productquery_row->PROMOTION_UNIT)) ? $productquery_row->PROMOTION_UNIT : 0;
                            $array['already_on_order'] = (!empty($productquery_row->ALREADY_ON_ORDER)) ? $productquery_row->ALREADY_ON_ORDER : 0;
                            $array['current_order_amount'] = (!empty($productquery_row->CURRENT_ORDER_AMOUNT)) ? $productquery_row->CURRENT_ORDER_AMOUNT:0;
                        }
                 
                    }     
                    
                        /* -------------         END           --------------- */
                        /* Code To Add New FIELD IN GIVEN SERVICE - 20-06-2017 */
                        
						$products[] = $array;
					}
					
					// set response values
					$_response['result_code'] = 1;
					$_response['total_products'] = (isset($product_count->product_count)) ? $product_count->product_count : 0;
					$_response['result'] 	= $products;
					
				} else {
					$_response['result_code'] = 0;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
				}
			} else {
				
					$_response['result_code'] = 0;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
				}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.outlet_not_valid');
		}
		
		return $_response;
	}
	
	/**
	 * Function used to get product details
	 * @param int prod_number
	 * @return array
	 * @access public
	 * 
	 */
	public function product_detail() {
		
		// get post data
		$outlet_id   = $this->_common->test_input($_REQUEST['outlet_id']);
		$prod_number = $this->_common->test_input($_REQUEST['prod_number']);
		$is_apn = $this->_common->test_input($_REQUEST['is_apn']);
		$search_keyword = $this->_common->test_input($_REQUEST['search_keyword']);
		$supplier = $this->_common->test_input($_REQUEST['supplier']);
        $order_id = $this->_common->test_input($_REQUEST['order_id']);
        $order_id = !empty($order_id)?$order_id:0; 
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
		if(!empty($outlet_id)) {
			if(!empty($is_apn)) {
				// get product's id from apn numbers
				$prod_apn_query = "SELECT APN_PRODUCT FROM ".$this->APNTBL." where APN_NUMBER = '".$prod_number."'";
				$get_prod_apn_data = $this->_mssql->my_mssql_query($prod_apn_query,$this->con);
				$apn_result = $this->_mssql->mssql_fetch_object_query($get_prod_apn_data);
				$prod_number = (isset($apn_result->APN_PRODUCT) && !empty($apn_result->APN_PRODUCT)) ? $apn_result->APN_PRODUCT : ''; 
			}
			
			if(!empty($prod_number)) {
				// get department's products listing
				$product_query = "SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,OUTP_MIN_ONHAND,OUTP_MAX_ONHAND,OUTP_MIN_REORDER_QTY,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost,PROD_PARENT FROM ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number where OUTP_OUTLET = $outlet_id AND Prod_Number = $prod_number AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'";
				// merge search entity in query
				if(!empty($search_keyword)) {
					$product_query .= " AND Prod_Desc like '%".$search_keyword."%'";
				}
				$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
				$countFlag = 0;	
				
                if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
					// get product row data
					$countFlag = 1;
                    $result = $this->_mssql->mssql_fetch_object_query($get_product_data);
				}
				//  fetch product entry from APN number
                if($countFlag == 0) {
                    $prod_apn_query = "SELECT APN_PRODUCT FROM ".$this->APNTBL." where APN_NUMBER = '".$prod_number."'";
                    $get_prod_apn_data = $this->_mssql->my_mssql_query($prod_apn_query,$this->con);
                    $apn_result = $this->_mssql->mssql_fetch_object_query($get_prod_apn_data);
                    $prod_number = (isset($apn_result->APN_PRODUCT) && !empty($apn_result->APN_PRODUCT)) ? $apn_result->APN_PRODUCT : ''; 
                    if(!empty($prod_number)) {     
                        $product_query = "SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,OUTP_MIN_ONHAND,OUTP_MAX_ONHAND,OUTP_MIN_REORDER_QTY,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost,PROD_PARENT FROM ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number where OUTP_OUTLET = $outlet_id AND Prod_Number = $prod_number AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'";
						// merge search entity in query
                        if(!empty($search_keyword)) {
                            $product_query .= " AND Prod_Desc like '%".$search_keyword."%'";
                        }
                        $get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
                        if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
                            // get product row data
                            $result = $this->_mssql->mssql_fetch_object_query($get_product_data);
                        }                
                    }
				}
				// get average weekly sales value
				$average_weekly_sales = $this->average_weekly_sales($prod_number,$outlet_id);
				// get units on order values
				$units_data = $this->units_on_order($prod_number,$outlet_id);
				// prepare response product details if result exists
				if(isset($result) && !empty($result)) {
					// set carton cost
					$prod_carton_qty = (isset($result->PROD_CARTON_QTY)) ? trim($result->PROD_CARTON_QTY) : "";
					$prod_parent = (isset($result->PROD_PARENT)) ? trim($result->PROD_PARENT) : 0;
					$prod_number = (isset($result->Prod_Number)) ? trim($result->Prod_Number) : 0;
					$unit_cost = (isset($result->UnitCost)) ? trim($result->UnitCost) : "";
					$prod_carton_cost = (isset($result->PROD_CARTON_COST)) ? trim($result->PROD_CARTON_COST) : "";
					$carton_cost = $prod_carton_qty*$unit_cost;
					// get supplier item number
					if(!empty($supplier) && isset($result->Prod_Number)) {
						$prod_number = $result->Prod_Number;
						$supplier_item_query = "SELECT SPRD_SUPPLIER_ITEM FROM SPRDTBL where SPRD_SUPPLIER = '$supplier' and SPRD_PRODUCT = $prod_number";
						$supplier_item_result = $this->_mssql->my_mssql_query($supplier_item_query,$this->con);
						if($this->_mssql->pdo_num_rows_query($supplier_item_query,$this->con) > 0 ){
							$supplier_item_row = $this->_mssql->mssql_fetch_object_query($supplier_item_result);
							$supplier_item = (isset($supplier_item_row->SPRD_SUPPLIER_ITEM) && !empty($supplier_item_row->SPRD_SUPPLIER_ITEM)) ? $supplier_item_row->SPRD_SUPPLIER_ITEM : '';
						}
					}
					// set product values
					$product['prod_desc'] = (isset($result->Prod_Desc)) ? utf8_encode($result->Prod_Desc) : "";
					$product['prod_number']	= (isset($result->Prod_Number)) ? trim($result->Prod_Number) : "";
					$product['prod_pos_desc'] = (isset($result->PROD_POS_DESC)) ? utf8_encode($result->PROD_POS_DESC) : "";
					$product['prod_unit_qty'] = (isset($result->PROD_UNIT_QTY)) ? trim($result->PROD_UNIT_QTY) : "";
					$product['prod_carton_qty'] = $prod_carton_qty ;
					$product['prod_carton_cost']= number_format($carton_cost,2, '.', '');
					//$product['unit_cost']	= number_format($unit_cost,2, '.', '');
					$product['unit_cost']	= $unit_cost;
					$product['unit_on_hand'] = (isset($result->OUTP_QTY_ONHAND)) ? trim($result->OUTP_QTY_ONHAND) : "";
					$product['min_stock_level'] = (isset($result->OUTP_MIN_ONHAND)) ? trim($result->OUTP_MIN_ONHAND) : "";
					$product['max_stock_level'] = (isset($result->OUTP_MAX_ONHAND)) ? trim($result->OUTP_MAX_ONHAND) : "";
					$product['min_reorder_qty'] = (isset($result->OUTP_MIN_REORDER_QTY)) ? trim($result->OUTP_MIN_REORDER_QTY) : "";
					$product['average_weekly_sales'] = $average_weekly_sales;
					$product['units_on_order'] = 0;
					$product['units_on_pending_order'] = 0;
					$product['units_on_order_order'] = $units_data['units_on_order_order'];
					$product['units_on_order_new'] = $units_data['units_on_order_new'];
					$product['supplier_item_number'] = (isset($supplier_item)) ? $supplier_item : '';
                    
                /* Code To Add New FIELD IN GIVEN SERVICE - 20-06-2017 */
                    /* -------------         Start         --------------- */
                    $productFilter = '';
                    $productFilterString = '';
                    if(!empty($prod_number)){
                        $productFilter .= $prod_number.',';
                    }
                    if(!empty($prod_parent)){
                        $productFilter .= $prod_parent.',';
                    }
                    if(!empty($productFilter)){
                        $productFilter = rtrim($productFilter,',');
                        $productFilterString = " (ORDL_PRODUCT in ($productFilter)) AND  ";    
                    }
                    if(!empty($order_id)){
                        $order_idFilter = trim($order_id);
                        $order_idFilterString = " (ORDL_ORDER_NO <> $order_idFilter) AND ";    
                    }
                    $product['min_on_hand'] = '';
                    $product['stock_on_hand'] = '';
                    $product['promotion_unit'] = '';
                    $product['already_on_order'] = '';
                    $product['current_order_amount'] = '';
                    if(!empty($outlet_id) && !empty($prod_number)){
                        $product_query =  "SELECT * FROM (SELECT OUTP_QTY_ONHAND as STOCK_ON_HAND,OUTP_MIN_ONHAND as MIN_ON_HAND,ORDL_TOTAL_UNITS as CURRENT_ORDER_AMOUNT, ORDL_Promo_Units as PROMOTION_UNIT,
                        ( select case coalesce(PROD_PARENT, 0)
                        when 0 then SUM(ORDL_TOTAL_UNITS) else SUM(ORDL_TOTAL_UNITS) * PROD_UNIT_QTY end AS UNITS_ON_ORDER
                        from OrdlTbl join PRODTBL on Prod_number = ORDL_PRODUCT where $productFilterString $order_idFilterString  ORDL_OUTLET = $outlet_id AND ORDL_DOCUMENT_TYPE = 'ORDER' and ORDL_DOCUMENT_STATUS = 'ORDER' group by PROD_PARENT, PROD_UNIT_QTY )  as ALREADY_ON_ORDER
                         FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number LEFT JOIN ORDLTBL ON ( ORDL_OUTLET = OUTP_OUTLET AND Prod_Number = ORDL_PRODUCT AND ORDL_DOCUMENT_TYPE = 'ORDER') where OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'  AND Prod_Number = $prod_number) productQ";
                        $product_query_result = $this->_mssql->my_mssql_query($product_query,$this->con);
                        if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
                        $productquery_row = $this->_mssql->mssql_fetch_object_query($product_query_result);
                            $product['min_on_hand'] = (!empty($productquery_row->MIN_ON_HAND)) ? $productquery_row->MIN_ON_HAND : 0;
                            $product['stock_on_hand'] = (!empty($productquery_row->STOCK_ON_HAND)) ? $productquery_row->STOCK_ON_HAND : 0;
                            $product['promotion_unit'] = (!empty($productquery_row->PROMOTION_UNIT)) ? $productquery_row->PROMOTION_UNIT : 0;
                            $product['already_on_order'] = (!empty($productquery_row->ALREADY_ON_ORDER)) ? $productquery_row->ALREADY_ON_ORDER : 0;
                            $product['current_order_amount'] = (!empty($productquery_row->CURRENT_ORDER_AMOUNT)) ? $productquery_row->CURRENT_ORDER_AMOUNT:0;
                        }
                 
                    }     
                    
                        /* -------------         END           --------------- */
                        /* Code To Add New FIELD IN GIVEN SERVICE - 20-06-2017 */
                    
                    
					// append product result array in response
					$_response['result_code'] = 1;
					$_response['result'] 	  = $product;
					$_response['message']     = '';
				}
			}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.product_param_not_valid');
		}
		return $_response;
	}
	
	/**
	 * Function used to calculate Average Weekly Sales amount
	 * @param int prod_number , int outlet_id
	 * @return float
	 * @access private
	 * 
	 */
	function average_weekly_sales($prod_number=0,$outlet_id=0) {
		// set parameter values
		$v_days = 60;
		$v_avg_weekly = 0;
		$current_date  = date('Y-m-d');
		$prev_date = date('Y-m-d', strtotime($current_date.' -'.$v_days.' days'));
		// get trx quantity from trxTbl
		$trx_qty_query = "SELECT SUM(TRX_QTY) AS SUMQTY FROM ".$this->TRXTBL." WHERE TRX_PRODUCT = '".$prod_number."' AND TRX_OUTLET = '".$outlet_id."' AND (TRX_DATE BETWEEN '".$prev_date."' AND '".$current_date."') AND TRX_TYPE = 'ITEMSALE'";
		$get_trx_data  = $this->_mssql->my_mssql_query($trx_qty_query,$this->con);
		$result = $this->_mssql->mssql_fetch_object_query($get_trx_data);
		$trx_sum_qty = (isset($result->SUMQTY) && !empty($result->SUMQTY)) ? $result->SUMQTY : 0; 
		// calculate average weekly value
		if(!empty($trx_sum_qty)) {
			 $v_sum = $trx_sum_qty;
			 $v_sum2 = $v_sum / $v_days;
			 $v_avg_daily = number_format($v_sum2,2, '.', '');
			 $v_avg_weekly = $v_avg_daily * 7;
		}
		return $v_avg_weekly;
	 }
	 
	 /**
	 * Function used to calculate Units on order 
	 * @param int prod_number , int outlet_id
	 * @return float
	 * @access private
	 * 
	 */
	private function units_on_order($prod_number=0,$outlet_id=0) {
		
		// get units on order order from OrdlTbl
		$units_order_query = "select SUM(ORDL_TOTAL_UNITS) AS UNITS_ON_ORDER_ORDER from ".$this->OrdlTbl." where ORDL_OUTLET = '".$outlet_id."' and ORDL_DOCUMENT_TYPE = 'ORDER' and ORDL_DOCUMENT_STATUS = 'ORDER' and ORDL_PRODUCT = '".$prod_number."'";
		$get_units_order_data  = $this->_mssql->my_mssql_query($units_order_query,$this->con);
		$result = $this->_mssql->mssql_fetch_object_query($get_units_order_data);
		$units_on_order = (isset($result->UNITS_ON_ORDER_ORDER) && !empty($result->UNITS_ON_ORDER_ORDER)) ? $result->UNITS_ON_ORDER_ORDER : 0; 
		
		// get units on order new from OrdlTbl
		$units_order_new_query = "select SUM(ORDL_TOTAL_UNITS) AS UNITS_ON_ORDER_NEW from ".$this->OrdlTbl." where ORDL_OUTLET = '".$outlet_id."' and (ORDL_DOCUMENT_TYPE = 'ORDER' or ORDL_DOCUMENT_TYPE is null) and (ORDL_DOCUMENT_STATUS = 'NEW' or ORDL_DOCUMENT_STATUS is null) and ORDL_PRODUCT = '".$prod_number."'";
		$get_units_order_new_data  = $this->_mssql->my_mssql_query($units_order_new_query,$this->con);
		$ord_new_result = $this->_mssql->mssql_fetch_object_query($get_units_order_new_data);
		$units_on_order_new = (isset($ord_new_result->UNITS_ON_ORDER_NEW) && !empty($ord_new_result->UNITS_ON_ORDER_NEW)) ? $ord_new_result->UNITS_ON_ORDER_NEW : 0; 
		// set units values in array
		$units_array = array('units_on_order_order'=>$units_on_order,'units_on_order_new'=>$units_on_order_new);
		return $units_array;
	 }
	
	/**
	 * Function used to get suppliers listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function suppliers() {
		
		// initialize supplier array
		$suppliers = array();
		// get supplier results
		$supplier_query = "select CODE_KEY_NUM,CODE_DESC,CODE_KEY_ALP from CODETBL where CODE_KEY_TYPE = 'SUPPLIER' order by CODE_DESC";
		$get_supplier_data = $this->_mssql->my_mssql_query( $supplier_query,$this->con );
		if( $this->_mssql->pdo_num_rows_query($supplier_query,$this->con) > 0 ) {
			while($row = $this->_mssql->mssql_fetch_object_query($get_supplier_data)) {
				// prepare supplier values
				$supplier_array['supplier_id']  = $row->CODE_KEY_NUM;
				$supplier_array['supplier']     = utf8_encode($row->CODE_KEY_ALP);
				$supplier_array['supplier_name']= utf8_encode($row->CODE_DESC);
				$suppliers[] = $supplier_array;
			}
			
			$_response['result_code'] = 1;
			$_response['result'] = $suppliers;
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.no_record_found');
		}
		return $_response;
	}
	
	/**
	 * Function used to get create order and fetch order number
	 * @param outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function create_order() {
		
		// get post data
		$outlet_id  = $this->_common->test_input($_REQUEST['outlet_id']);
		if(!empty($outlet_id)) {
			// set order params
			$ord_created_at = date('M d Y h:i:s A');
			$order_date = date('M d Y');
			$ord_document_type = 'ORDER';
			$ord_document_status = (!empty($ord_document_status)) ? $ord_document_status : 'NEW';
			// get last inserted order id
			$sql = "select ORDH_ORDER_NO from " .$this->OrdhTbl. "  where ORDH_ORDER_NO =(select max( ORDH_ORDER_NO ) from OrdhTbl WHERE ORDH_OUTLET = $outlet_id )";
			$query = $this->_mssql->my_mssql_query($sql,$this->con);
			if($this->_mssql->pdo_num_rows_query($sql,$this->con) > 0 ){
				$row = $this->_mssql->mssql_fetch_object_query($query);
			}
			$last_order_id = (isset($row->ORDH_ORDER_NO) && !empty($row->ORDH_ORDER_NO)) ? $row->ORDH_ORDER_NO : 0;
			$new_order_id  = $last_order_id + 1;
			
			// insert order header details
			$insert_ord_sql = "INSERT " .$this->OrdhTbl. " (ORDH_OUTLET, ORDH_ORDER_NO, ORDH_DOCUMENT_TYPE, ORDH_DOCUMENT_STATUS,ORDH_ORDER_DATE,ORDH_TIMESTAMP,ORDH_Creatation_Type) VALUES ($outlet_id,$new_order_id,'".$ord_document_type."','".$ord_document_status."','".$order_date."','".$ord_created_at."',5)";
			$this->_mssql->my_mssql_query($insert_ord_sql,$this->con); 
			$_response['result_code'] = 1;
			$_response['order_number'] = $new_order_id;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.success.stock.placed_order');
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.required_order_input');
		}
		return $_response;
	 }
	
	/**
	 * Function used to get suppliers listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function create_order_old() {
		// get post data
		$order_id      = $this->_common->test_input($_REQUEST['order_number']);
		$outlet_id     = $this->_common->test_input($_REQUEST['outlet_id']);
		$supplier_name = $this->_common->test_input($_REQUEST['supplier_name']);
		$supplier      = $this->_common->test_input($_REQUEST['supplier']);
		$ord_date      = $this->_common->test_input($_REQUEST['order_date']);
		$ord_products  = $_REQUEST['ord_products'];
		$ord_document_status = $this->_common->test_input($_REQUEST['order_status']);
		// set order params
		$ord_created_at = date('M d Y h:i:s A');
		$order_date    = (!empty($order_date)) ? date('M d Y h:i:s A' , strtotime($ord_date)) : $ord_created_at;
		$ord_document_type = 'ORDER';
		$ord_document_status = (!empty($ord_document_status)) ? $ord_document_status : 'NEW';
		$ord_post_date = ($ord_document_status == 'ORDER') ? $ord_created_at : '';
		
		//get array value from json
		$product = json_decode($ord_products);
		
		if(!empty($outlet_id) && !empty($supplier) && !empty($supplier_name) && !empty($product)) {
			// get last inserted order id
			$sql = "select ORDH_ORDER_NO from " .$this->OrdhTbl. "  where ORDH_ORDER_NO =(select max( ORDH_ORDER_NO ) from OrdhTbl )";
			$query = $this->_mssql->my_mssql_query($sql,$this->con);
			if($this->_mssql->pdo_num_rows_query($sql,$this->con) > 0 ){
				$row = $this->_mssql->mssql_fetch_object_query($query);
			}
			$last_order_id = (isset($row->ORDH_ORDER_NO) && !empty($row->ORDH_ORDER_NO)) ? $row->ORDH_ORDER_NO : 0;
			$new_order_id  = $last_order_id + 1;
			
			// insert order header details
			$insert_ord_sql = "INSERT " .$this->OrdhTbl. " (ORDH_OUTLET, ORDH_ORDER_NO, ORDH_SUPPLIER, ORDH_SUPPLIER_NAME, ORDH_DOCUMENT_TYPE, ORDH_DOCUMENT_STATUS,ORDH_ORDER_DATE,ORDH_TIMESTAMP,ORDH_POSTED_DATE) VALUES ($outlet_id,$new_order_id,'".$supplier."','".$supplier_name."','".$ord_document_type."','".$ord_document_status."','".$order_date."','".$ord_created_at."','".$ord_post_date."')";
			$this->_mssql->my_mssql_query($insert_ord_sql,$this->con); 
			// insert order item log in db
			
			// set input item values
			$line_no = $key+1;
			$prod_number  = $product->prod_number;
			$prod_desc    = utf8_encode($product->prod_desc);
			$supplier_item= (isset($product->supplier_item_number)) ? $product->supplier_item_number : ''; // default set blank
			$carton_cost  = $product->carton_cost;
			$cartons   	  = (!empty($product->cartons)) ? $product->cartons : 0;
			$units        = (!empty($product->units)) ? $product->units : 0;
			$total_units  = $product->total_units;
			$line_total   = $product->line_total;
			$carton_qty   = $product->carton_qty;
			$document_type= 'ORDER';
			
			// prepare sql for inserting product entry
			$insert_item_sql = "INSERT " .$this->OrdlTbl. " (ORDL_OUTLET, ORDL_ORDER_NO, ORDL_LINE_NO, ORDL_PRODUCT, ORDL_DESC, ORDL_SUPPLIER_ITEM, ORDL_CARTON_COST, ORDL_CARTONS, ORDL_UNITS, ORDL_TOTAL_UNITS,ORDL_LINE_TOTAL,ORDL_CARTON_QTY,ORDL_DOCUMENT_TYPE,ORDL_DOCUMENT_STATUS,ORDL_FINAL_LINE_TOTAL,ORDL_FINAL_CARTON_COST,ORDL_POSTED_DATE ) VALUES ($outlet_id,$new_order_id,$line_no,$prod_number,'$prod_desc','$supplier_item','$carton_cost',$cartons,$units,$total_units,'$line_total',$carton_qty,'$document_type','$ord_document_status','$line_total','$carton_cost','$ord_post_date')";
			$this->_mssql->my_mssql_query($insert_item_sql,$this->con); 
			
			$_response['result_code'] = 1;
			$_response['order_number'] = $new_order_id;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.success.stock.placed_order');
			
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.required_order_input');
		}
		return $_response;
	 }
	 
	/**
	 * Function used to get suppliers listing
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function update_order() {
		
		// get post data
		$outlet_id     = $this->_common->test_input($_REQUEST['outlet_id']);
		$order_id      = $this->_common->test_input($_REQUEST['order_number']);
		$supplier_name = $this->_common->test_input($_REQUEST['supplier_name']);
		$supplier 	   = $this->_common->test_input($_REQUEST['supplier']);
		//$supplier      = $supplier_name;
		$ord_products  = $_REQUEST['ord_products'];
		$ord_post_date = 'null';//date('M d Y h:i:s A');
		$ord_post_date_p = date('M d Y h:i:s A');
		//get array value from json
		$product = json_decode($ord_products);
		// set default response as false
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.required_order_input');
		if(!empty($outlet_id) && !empty($order_id) && !empty($product)) {
			
			// set input item values
			$line_no = 1;
			$prod_number  = $product->prod_number;
			$prod_desc    = str_replace( "'", "", $product->prod_desc);
			$supplier_item= (isset($product->supplier_item_number) && !empty($product->supplier_item_number)) ? $product->supplier_item_number : ''; // default set blank
			$carton_cost  = $product->carton_cost;
			$cartons   	  = (!empty($product->cartons)) ? $product->cartons : 0;
			$units        = (!empty($product->units)) ? $product->units : 0;
			$total_units  = $product->total_units;
			$line_total   = $product->line_total;
			$carton_qty   = $product->carton_qty;
			$stock_on_hand   = (isset($product->stock_on_hand) && !empty($product->stock_on_hand)) ? $product->stock_on_hand : ''; //  Update OUTP Table
			$current_order_amount   = (isset($product->current_order_amount) && !empty($product->current_order_amount)) ? $product->current_order_amount : ''; // default set blank
			$document_type= 'ORDER';
			// set unit value ; if devisible by carton qty
			if ($total_units % $carton_qty == 0) {
				$units = 0;
			}
			if(!empty($prod_number)) {

              // To Get GST Value		
                $ordl_gst_ind = '';		
                $product_sql = "select PROD_TAX_CODE from ".$this->PRODTBL." where PROD_NUMBER = $prod_number";		
                $product_query = $this->_mssql->my_mssql_query($product_sql,$this->con);		
                if($this->_mssql->pdo_num_rows_query($product_sql,$this->con) > 0 ){		
                    $product_row = $this->_mssql->mssql_fetch_object_query($product_query);		
                    $ordl_gst_ind = (isset($product_row->PROD_TAX_CODE) && ($product_row->PROD_TAX_CODE == 'GST'))?'*' :'';		
                }		
	            
				// get max item line number
				$sql = "select max( ORDL_LINE_NO ) as last_line_no from " .$this->OrdlTbl. " WHERE ORDL_ORDER_NO = $order_id AND ORDL_OUTLET = $outlet_id ";
				$query = $this->_mssql->my_mssql_query($sql,$this->con);
				if($this->_mssql->pdo_num_rows_query($sql,$this->con) > 0 ){
					$row = $this->_mssql->mssql_fetch_object_query($query);
					$line_no = (isset($row->last_line_no)) ? $row->last_line_no+1 : 1;
				}
				// remove previouse order items
				$remove_prev_item_sql = "DELETE FROM " .$this->OrdlTbl. " WHERE ORDL_ORDER_NO = $order_id AND ORDL_PRODUCT = $prod_number AND ORDL_OUTLET = $outlet_id ";
				$this->_mssql->my_mssql_query($remove_prev_item_sql,$this->con);
				
				if(empty(trim($supplier_item))) {
					if(empty($supplier)) {
						// get supplier details from ordhtbl
						$ordh_supplier_query = "SELECT ORDH_SUPPLIER FROM ".$this->OrdhTbl." WHERE ORDH_ORDER_NO = $order_id AND ORDH_OUTLET = $outlet_id";
						$ordh_supplier_result = $this->_mssql->my_mssql_query($ordh_supplier_query,$this->con);
						if($this->_mssql->pdo_num_rows_query($ordh_supplier_query,$this->con) > 0 ){
							$ordh_supplier_row = $this->_mssql->mssql_fetch_object_query($ordh_supplier_result);
							$supplier = (isset($ordh_supplier_row->ORDH_SUPPLIER) && !empty($ordh_supplier_row->ORDH_SUPPLIER)) ? $ordh_supplier_row->ORDH_SUPPLIER : '';
						}
					}
					// get supplier item number
					if(!empty($supplier)) {
						$supplier_item_query = "SELECT SPRD_SUPPLIER_ITEM FROM SPRDTBL where SPRD_SUPPLIER = '$supplier' and SPRD_PRODUCT = $prod_number";
						$supplier_item_result = $this->_mssql->my_mssql_query($supplier_item_query,$this->con);
						if($this->_mssql->pdo_num_rows_query($supplier_item_query,$this->con) > 0 ){
							$supplier_item_row = $this->_mssql->mssql_fetch_object_query($supplier_item_result);
							$supplier_item = (isset($supplier_item_row->SPRD_SUPPLIER_ITEM) && !empty($supplier_item_row->SPRD_SUPPLIER_ITEM)) ? $supplier_item_row->SPRD_SUPPLIER_ITEM : '';
						}
					}
				}
				
                /* insert Current Order Amount is remaining ------ 20-06-2017 */
                
				// prepare sql for inserting product entry
				$insert_item_sql = "INSERT " .$this->OrdlTbl. " (ORDL_OUTLET, ORDL_ORDER_NO, ORDL_LINE_NO, ORDL_PRODUCT, ORDL_DESC, ORDL_SUPPLIER_ITEM, ORDL_CARTON_COST, ORDL_CARTONS, ORDL_UNITS, ORDL_TOTAL_UNITS,ORDL_LINE_TOTAL,ORDL_CARTON_QTY,ORDL_DOCUMENT_TYPE,ORDL_DOCUMENT_STATUS,ORDL_FINAL_LINE_TOTAL,ORDL_FINAL_CARTON_COST,ORDL_POSTED_DATE,ORDL_GST_IND ) VALUES ('$outlet_id','$order_id','$line_no','$prod_number','$prod_desc','$supplier_item','$carton_cost','$cartons','$units','$total_units','$line_total','$carton_qty','$document_type','','$line_total','$carton_cost',$ord_post_date,'$ordl_gst_ind')";
				$this->_mssql->my_mssql_query($insert_item_sql,$this->con); 
				
				// update order' modified date time
				$update_sql = "UPDATE ".$this->OrdhTbl." SET ORDH_TIMESTAMP = '".$ord_post_date_p."' WHERE ORDH_ORDER_NO = $order_id AND ORDH_OUTLET = $outlet_id";
				$this->_mssql->my_mssql_query($update_sql,$this->con);
                
                
                
                // update Stock on hand from OUTP Table
				$update_sql = "UPDATE ".$this->OUTPTBL." SET OUTP_QTY_ONHAND = '".$stock_on_hand."' WHERE Outp_Product = $prod_number AND OUTP_OUTLET = $outlet_id";
				$this->_mssql->my_mssql_query($update_sql,$this->con);
                
				
				$_response['result_code'] = 1;
				$_response['order_number'] = $order_id;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.success.stock.update_order');
			}
		}
		return $_response;
	 }
	 
	/**
	 * Function used to update order's supplier
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	 public function update_order_supplier() {
		
		// get post data
		$order_id      = $this->_common->test_input($_REQUEST['order_number']);
		$outlet_id     = $this->_common->test_input($_REQUEST['outlet_id']);
		$supplier_name = $this->_common->test_input($_REQUEST['supplier_name']);
		$supplier      = $this->_common->test_input($_REQUEST['supplier']);
		$ord_timestamp = date('M d Y h:i:s A');
		if(!empty($order_id) && !empty($supplier) && !empty($supplier_name)) {
			// pull first 30 char from supplier name
			$supplier_name = substr(html_entity_decode($supplier_name), 0, 30);
			// update order' supplier data entry
			$update_sql = "UPDATE ".$this->OrdhTbl." SET ORDH_SUPPLIER = '".$supplier."',ORDH_SUPPLIER_NAME = '".$supplier_name."',ORDH_TIMESTAMP = '".$ord_timestamp."' WHERE ORDH_ORDER_NO = $order_id ";
			if(!empty($outlet_id)) {
				$update_sql .= "AND ORDH_OUTLET = $outlet_id";
			}
		
			$this->_mssql->my_mssql_query($update_sql,$this->con);
			// set success response
			$_response['result_code'] = 1;
			$_response['order_number'] = $order_id;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.success.stock.update_order');
			
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.required_order_input');
		}
		return $_response;
	 }
	 
	/**
	 * Function to insert Print label request
	 * @param int outlet_id , float Product_Number , date-time Print_Batch
	 * @return array
	 * @access public
	 * 
	 */
	 public function print_label_request() {
		
		// get post data
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$user_id   = $this->_common->test_input($_REQUEST['user_id']);
		$products  = $_REQUEST['products'];
		
		if (empty($user_id)) {
            $_response['result_code'] = 0;
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
            return $_response;
        }
        $print_batch = date('Y-m-d H:i:s');
		$_response['result_code'] = 0;
		$products = json_decode($products); // convert json into array
        if(!empty($outlet_id) && !empty($products) && count($products) > 0) {
			
			// insert qurey to save print label in mssql database
			foreach($products as $product) {
				$prod_number = $product->prod_number; // set product id
				if(!empty($prod_number)) {
					$insert_print_lable_query = "INSERT PrintLabelFromTabletTbl (PLT_Outlet, PLT_Product_Number, PLT_Print_Batch) VALUES ('$outlet_id','$prod_number','$print_batch')";
					$this->_mssql->my_mssql_query($insert_print_lable_query,$this->con);
					// insert print label data in mysql db
					$insert_print_query = "INSERT INTO ".$this->print_label_table."(user_id,outlet_id,product_id,created_at) VALUES ('$user_id','$outlet_id','$prod_number','$print_batch')";
					$this->_db->my_query($insert_print_query);
				}
			}
			
			// set success return values 
			$_response['message'] =  $this->_common->langText($this->_locale,'txt.sucess.savedSuccessfully');
			$_response['result_code'] = 1;
        } else {
            $_response['message'] =  $this->_common->langText($this->_locale,'txt.error.fill_all_filed');
        }
        return $_response;
	}
	
	/**
	 * Function used to insert Print label request in bulk quantity
	 * @param int outlet_id , json array apn_items
	 * @return array
	 * @access public
	 * 
	 */
	 public function bulk_print_label_upload() {
		
		// get post data
		$apn_items  = $_REQUEST['apn_items'];
		$print_batch = date('Y-m-d H:i:s');
		$_response['result_code'] = 0;
		
		$apn_items = json_decode($apn_items); // convert json into array
        if(!empty($apn_items) && count($apn_items) > 0) {
			
			// insert qurey to save print label in mssql database
			foreach($apn_items as $key=>$item) {
				// set print product data
				$apn_number = $item->apn_number;  
				$outlet_id = $item->outlet_id;  
				if( !empty($outlet_id) && !empty($apn_number) ) {
					$insert_print_lable_query = "INSERT BulkPrintLabelFromTabletTbl (BPLT_Outlet, BPLT_Product_Number, BPLT_Print_Batch) VALUES ('$outlet_id','$apn_number','$print_batch')";
					$this->_mssql->my_mssql_query($insert_print_lable_query,$this->con);
				}
			}
			
			// set success return values 
			$_response['message'] =  $this->_common->langText($this->_locale,'txt.sucess.savedSuccessfully');
			$_response['result_code'] = 1;
        } else {
            $_response['message'] =  $this->_common->langText($this->_locale,'txt.error.fill_all_filed');
        }
        return $_response;
	}
	
	/**
	 * Function used to insert stocktake request in bulk quantity
	 * @param int outlet_id , json array apn_items
	 * @return array
	 * @access public
	 * 
	 */
	 public function bulk_stocktake_upload() {
		
		// get post data
		$apn_items  = $_REQUEST['apn_items'];
		$print_batch = date('Y-m-d H:i:s');
		$_response['result_code'] = 0;
        
		$apn_items = json_decode($apn_items); // convert json into array
        if(!empty($apn_items) && count($apn_items) > 0) {
			
			// insert qurey to save print label in mssql database
			foreach($apn_items as $key=>$item) {
				// set print product data
				$outlet_id = $item->outlet_id;  
				$apn_number = $item->apn_number; 
				$item_count = $item->item_count; 
				if( !empty($outlet_id) && !empty($apn_number) && $item_count > 0 ) {
					$insert_print_lable_query = "INSERT BulkStockTakeLabelFromTabletTbl (BSTFT_Outlet, BSTFT_Product_Number, BSTFT_Print_Batch, BSTFT_Quantity) VALUES ('$outlet_id','$apn_number','$print_batch','$item_count')";
					$this->_mssql->my_mssql_query($insert_print_lable_query,$this->con);
				}
			}
			
			// set success return values 
			$_response['message'] =  $this->_common->langText($this->_locale,'txt.sucess.savedSuccessfully');
			$_response['result_code'] = 1;
        } else {
            $_response['message'] =  $this->_common->langText($this->_locale,'txt.error.fill_all_filed');
        }
        return $_response;
	}
	
	/**
	 * Function used to insert order request in bulk quantity
	 * @param int outlet_id , json array apn_items
	 * @return array
	 * @access public
	 * 
	 */
	 public function bulk_order_upload() {
		
		// get post data
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$apn_items  = $_REQUEST['apn_items'];
		$print_batch = date('Y-m-d H:i:s');
		$_response['result_code'] = 0;
        
		$apn_items = json_decode($apn_items); // convert json into array
        if( !empty($apn_items) && count($apn_items) > 0) {
			
			// insert qurey to save print label in mssql database
			foreach($apn_items as $key=>$item) {
				// set print product data
				$outlet_id = $item->outlet_id;
				$apn_number = $item->apn_number;
				$item_count = $item->item_count;
				if( !empty($outlet_id) && !empty($apn_number) && $item_count > 0 ) {
					$insert_print_lable_query = "INSERT BulkOrderFromTabletTbl (BOFT_Outlet, BOFT_Product_Number, BOFT_Print_Batch,BOFT_Quantity) VALUES ('$outlet_id','$apn_number','$print_batch','$item_count')";
					$this->_mssql->my_mssql_query($insert_print_lable_query,$this->con);
				}
			}
			
			// set success return values 
			$_response['message'] =  $this->_common->langText($this->_locale,'txt.sucess.savedSuccessfully');
			$_response['result_code'] = 1;
        } else {
            $_response['message'] =  $this->_common->langText($this->_locale,'txt.error.fill_all_filed');
        }
        return $_response;
	}
	
	/**
	 * Function used to get user's print labels
	 * @param int prod_number
	 * @return array
	 * @access public
	 * 
	 */
	public function print_labels() {
		
		//get post user params
		$user_id  = $this->_common->test_input($_REQUEST['user_id']);
		if (empty($user_id)) {
            $_response['result_code'] = 0;
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
        } else {
			// fetch all print lables of user
			$print_query  = "SELECT * FROM ".$this->print_label_table." WHERE user_id = ".$user_id; 
			$print_result = $this->_db->my_query($print_query);
			$print_labels = array();
			if ($this->_db->my_num_rows($print_result) > 0) {
				while($row = $this->_db->my_fetch_object($print_result)) {
					// prepare print label values
					$print['outlet_id']  = $row->outlet_id;
					$print['product_id'] = $row->product_id;
					$print['print_batch_datetime'] = $row->created_at;
					$print_labels[] = $print;
					// append user result array in response
					$_response['result_code'] = 1;
                    $_response['result'] = $print_labels;  
				}
			} else {
				$_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
		}
		return $_response;
	}
	
	/**
     * Function used to get outlet ids in outlet array
	 * @param array $user_outlets
	 * @return string
	 * @access private
	 * 
	 */
	private function outlet_ids($user_outlets) {
		// set total outlet counts
		$outlet_count = count($user_outlets);
		$outlet_ids = '';
		foreach($user_outlets as $key=>$outlets) {
			if($key < ($outlet_count-1)) {
				$outlet_ids .= $outlets['outlet_id'].',';
			} else {
				$outlet_ids .= $outlets['outlet_id'];
			}
		}
		return $outlet_ids;
	}
	
	/**
     * Function used to get user's orders listing
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function my_orders() {
		
		//get post user params
		$user_id = $this->_common->test_input($_REQUEST['user_id']);
		$offset  = $this->_common->test_input($_REQUEST['offset']);
		$limit 	 = $this->_common->test_input($_REQUEST['limit']);
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
		if (empty($user_id)) {
            $_response['result_code'] = 0;
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
        } else {
			// checking user email is already exists or not
			$user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id;
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
				// get user row data
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				// get associated zone results from user's zone 
				$user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
				if(!empty($user_outlets) && count($user_outlets) > 0) {
					
					// get outlet ids
					$outlet_ids = $this->outlet_ids($user_outlets);
					
					// set query for total count 
					$order_count_query = "SELECT count(ORD.ORDH_ORDER_NO) as order_count FROM " .$this->OrdhTbl. " ORD LEFT JOIN " .$this->OUTLTBL. " OUT on OUT.OUTL_OUTLET = ORD.ORDH_OUTLET WHERE ORD.ORDH_OUTLET in ( $outlet_ids ) AND ORD.ORDH_DOCUMENT_STATUS = 'NEW' AND ORD.ORDH_ORDER_DATE >= DateAdd(d, -60, GetDate()) ";
					$order_count_data = $this->_mssql->my_mssql_query($order_count_query,$this->con);
					$order_count = $this->_mssql->mssql_fetch_object_query($order_count_data);
					
					// get orders results
					$order_query = "SELECT * FROM ( SELECT ORD.*,OUT.OUTL_DESC,  ROW_NUMBER() OVER (ORDER BY ORDH_TIMESTAMP DESC) as row FROM " .$this->OrdhTbl. " ORD LEFT JOIN " .$this->OUTLTBL. " OUT on OUT.OUTL_OUTLET = ORD.ORDH_OUTLET WHERE ORD.ORDH_OUTLET in ( $outlet_ids ) AND ORD.ORDH_DOCUMENT_STATUS = 'NEW' AND ORD.ORDH_ORDER_DATE >= DateAdd(d, -60, GetDate()) ) a WHERE row > $offset AND row <= $limit ";
					$orders = array();
					$get_order_data = $this->_mssql->my_mssql_query( $order_query,$this->con );
					if( $this->_mssql->pdo_num_rows_query($order_query,$this->con) > 0 ) {
						while($row = $this->_mssql->mssql_fetch_object_query($get_order_data)) {
							
							// prepare order values
							$ord_array['outlet_id']  	 = $row->ORDH_OUTLET;
							$ord_array['outlet_name']    = utf8_encode($row->OUTL_DESC);
							$ord_array['order_number']   = $row->ORDH_ORDER_NO;
							$ord_array['supplier']		 = (!empty($row->ORDH_SUPPLIER)) ? $row->ORDH_SUPPLIER : '';
							$ord_array['supplier_name']	 = (!empty($row->ORDH_SUPPLIER_NAME)) ? $row->ORDH_SUPPLIER_NAME : '';
							$ord_array['order_post_date']= (!empty($row->ORDH_POSTED_DATE)) ? date('d/m/Y',strtotime($row->ORDH_POSTED_DATE)) : '';
							$ord_array['order_date']	 = date('d/m/Y',strtotime($row->ORDH_ORDER_DATE));
							$ord_array['order_timestamp']= date('d/m/Y H:i:s',strtotime($row->ORDH_TIMESTAMP));
							$ord_array['order_type']	 = $row->ORDH_DOCUMENT_TYPE;
							$ord_array['order_status']	 = $row->ORDH_DOCUMENT_STATUS;
							$orders[] = $ord_array;
						}
						// append user result array in response
						$_response['result_code'] = 1;
						$_response['total_count'] = (isset($order_count->order_count)) ? $order_count->order_count : 0;
						$_response['result'] = $orders;
					} else {
						$_response['result_code'] = 0;
						$_response['message'] = $this->_common->langText($this->_locale,'txt.no_record_found');
					}
				}	
			} else {
				$_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
		}
		return $_response;
	 }
	 
   /**
	* Function used to view order details
	* @param int order_id
	* @return array
	* @access public
	* 
	*/
	public function order_detail() {
		
		// get post data
		$order_id  = $this->_common->test_input($_REQUEST['order_number']);
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
        $limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
        // set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 0;
		$offset = (!empty($offset)) ? $offset : 0;
		// set default response 
		$_response['result_code'] = 0;
		if(!empty($order_id) && !empty($outlet_id)) {
			// get order's information
			$order_data = $this->prepare_order_info($order_id,$outlet_id,$offset,$limit);
			if(!empty($order_data) && count($order_data) > 0) {
				// set success response
				$_response['result_code'] = 1;
				$_response['result']      = $order_data;
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.no_record_found');  
			}
		} else {
           	$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_order_id');         
        }
		return $_response;
	  }
	  
    /**
     * Function used to manage order's information
	 * @param int order_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function prepare_order_info($order_id=0,$outlet_id=0,$offset=0,$limit=0) {
		$order_data = array();
		$line_total = 0;
		$total_units = 0;
		// get order's result
		$ord_query = "select ORD.*,OUT.OUTL_DESC from " .$this->OrdhTbl. " ORD LEFT JOIN " .$this->OUTLTBL. " OUT on OUT.OUTL_OUTLET = ORD.ORDH_OUTLET  WHERE ORD.ORDH_ORDER_NO = $order_id AND OUTL_OUTLET = $outlet_id";
		$ord_result = $this->_mssql->my_mssql_query($ord_query,$this->con);
		if( $this->_mssql->pdo_num_rows_query($ord_query,$this->con) > 0 ) {
			// set order result
			$ord_result = $this->_mssql->mssql_fetch_object_query($ord_result);
			// prepare order values
			$order_data['outlet_id']   = $ord_result->ORDH_OUTLET;
			$order_data['order_number'] = $ord_result->ORDH_ORDER_NO;
			$order_data['outlet_name'] = utf8_encode($ord_result->OUTL_DESC);
			$order_data['supplier'] = (!empty($ord_result->ORDH_SUPPLIER)) ? $ord_result->ORDH_SUPPLIER : '';
			$order_data['supplier_name'] = (!empty($ord_result->ORDH_SUPPLIER_NAME)) ? $ord_result->ORDH_SUPPLIER_NAME : '';
			$order_data['order_date'] = date('Y-m-d H:i:s',strtotime($ord_result->ORDH_ORDER_DATE));
			$order_data['order_post_date']= (!empty($ord_result->ORDH_POSTED_DATE)) ? $ord_result->ORDH_POSTED_DATE : '';
			$order_data['order_type']	 = $ord_result->ORDH_DOCUMENT_TYPE;
			$order_data['order_status']	 = $ord_result->ORDH_DOCUMENT_STATUS;
            $offset_query = '';
            if(empty($offset) && empty($limit)){
                
            }else{
                $offset_query = "WHERE row > ".$offset." AND row <= ".$limit;
            }
            
            /*
            // get order's item data
			$order_product_query = "SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,ORDL_LINE_TOTAL,ORDL_TOTAL_UNITS,PROD_PARENT FROM ".$this->OrdlTbl." JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '".$ord_result->ORDH_ORDER_NO."' AND ORDL_OUTLET = '".$ord_result->ORDH_OUTLET."' ORDER BY ORDL_SortOrder, PROD_COMMODITY, PROD_REPLICATE, PROD_DESC";
            */
			// get order's item data
            
            $order_product_query_count = "SELECT count(ORDL_PRODUCT) as product_count  FROM ".$this->OrdlTbl." JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '".$ord_result->ORDH_ORDER_NO."' AND ORDL_OUTLET = '".$ord_result->ORDH_OUTLET."'";
            $product_count_data = $this->_mssql->my_mssql_query($order_product_query_count,$this->con);
            $product_count = $this->_mssql->mssql_fetch_object_query($product_count_data);
            
			$order_product_query = "SELECT * FROM (SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,PROD_PARENT, ROW_NUMBER() OVER (ORDER BY ORDL_SortOrder ASC, PROD_COMMODITY ASC,PROD_REPLICATE ASC, PROD_DESC ASC) as row  FROM ".$this->OrdlTbl." JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '".$ord_result->ORDH_ORDER_NO."' AND ORDL_OUTLET = '".$ord_result->ORDH_OUTLET."') a $offset_query";
			$order_product_result = $this->_mssql->my_mssql_query($order_product_query,$this->con);
			$order_products = array();
			if( $this->_mssql->pdo_num_rows_query($order_product_query,$this->con) > 0 ) {
				while($product_row = $this->_mssql->mssql_fetch_object_query($order_product_result)) {
                    // prepare stocktake outlet's product values
					$prod_number = $ord_products_array['prod_number']= $product_row->ORDL_PRODUCT;
					$ord_products_array['supplier_item_number']= $product_row->ORDL_SUPPLIER_ITEM;
					$ord_products_array['prod_desc']  = utf8_encode($product_row->ORDL_DESC);
					$ord_products_array['carton_cost'] = number_format($product_row->ORDL_CARTON_COST,2, '.', '');	
					$ord_products_array['line_total'] = number_format($product_row->ORDL_LINE_TOTAL,2, '.', '');	
					$ord_products_array['cartons']  = (!empty($product_row->ORDL_CARTONS)) ? $product_row->ORDL_CARTONS : '0';	
					$ord_products_array['units'] = (!empty($product_row->ORDL_UNITS)) ? $product_row->ORDL_UNITS : '';
					$ord_products_array['total_units'] = $product_row->ORDL_TOTAL_UNITS;
					$ord_products_array['carton_qty'] = $product_row->ORDL_CARTON_QTY;
					$prod_parent = $ord_products_array['prod_parent'] = (!empty($product_row->PROD_PARENT)) ? $product_row->PROD_PARENT : '';
					/* Code To Add New FIELD IN GIVEN SERVICE - 20-06-2017 */
                    /* -------------         Start         --------------- */
                    $productFilter = '';
                    $productFilterString = '';
                    if(!empty($prod_number)){
                        $productFilter .= $prod_number.',';
                    }
                    if(!empty($prod_parent)){
                        $productFilter .= $prod_parent.',';
                    }
                    if(!empty($productFilter)){
                        $productFilter = rtrim($productFilter,',');
                        $productFilterString = " (ORDL_PRODUCT in ($productFilter)) AND ";    
                    }
                    if(!empty($order_id)){
                        $order_idFilter = trim($order_id);
                        $order_idFilterString = " (ORDL_ORDER_NO <> $order_idFilter) AND ";    
                    }
                    $ord_products_array['min_on_hand'] = '';
                    $ord_products_array['stock_on_hand'] = '';
                    $ord_products_array['promotion_unit'] = '';
                    $ord_products_array['already_on_order'] = '';
                    $ord_products_array['current_order_amount'] = '';
                    if(!empty($outlet_id) && !empty($prod_number) && !empty($order_id)){
                        $product_query =  "SELECT * FROM (SELECT OUTP_QTY_ONHAND as STOCK_ON_HAND,OUTP_MIN_ONHAND as MIN_ON_HAND,ORDL_TOTAL_UNITS as CURRENT_ORDER_AMOUNT, ORDL_Promo_Units as PROMOTION_UNIT,
                        ( select case coalesce(PROD_PARENT, 0)
                        when 0 then SUM(ORDL_TOTAL_UNITS) else SUM(ORDL_TOTAL_UNITS) * PROD_UNIT_QTY end AS UNITS_ON_ORDER
                        from OrdlTbl join PRODTBL on Prod_number = ORDL_PRODUCT where $productFilterString $order_idFilterString  ORDL_OUTLET = $outlet_id AND ORDL_DOCUMENT_TYPE = 'ORDER' and ORDL_DOCUMENT_STATUS = 'ORDER' group by PROD_PARENT, PROD_UNIT_QTY ) as ALREADY_ON_ORDER FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number LEFT JOIN ORDLTBL ON ( ORDL_OUTLET = OUTP_OUTLET AND Prod_Number = ORDL_PRODUCT AND ORDL_DOCUMENT_TYPE = 'ORDER') where OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'  AND Prod_Number = $prod_number) productQ";
                        $product_query_result = $this->_mssql->my_mssql_query($product_query,$this->con);
                        if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
                        $productquery_row = $this->_mssql->mssql_fetch_object_query($product_query_result);
                            $ord_products_array['min_on_hand'] = (!empty($productquery_row->MIN_ON_HAND)) ? $productquery_row->MIN_ON_HAND : 0;
                            $ord_products_array['stock_on_hand'] = (!empty($productquery_row->STOCK_ON_HAND)) ? $productquery_row->STOCK_ON_HAND : 0;
                            $ord_products_array['promotion_unit'] = (!empty($productquery_row->PROMOTION_UNIT)) ? $productquery_row->PROMOTION_UNIT : 0;
                            $ord_products_array['already_on_order'] = (!empty($productquery_row->ALREADY_ON_ORDER)) ? $productquery_row->ALREADY_ON_ORDER : 0;
                            $ord_products_array['current_order_amount'] = (!empty($productquery_row->CURRENT_ORDER_AMOUNT)) ? $productquery_row->CURRENT_ORDER_AMOUNT:0;
                        }
                 
                    }     
                    
                    /* -------------         END           --------------- */
                    /* Code To Add New FIELD IN GIVEN SERVICE - 20-06-2017 */
                    
                    // append product array
					$order_products[] = $ord_products_array;
					// set item totals
					$line_total = $line_total+$product_row->ORDL_LINE_TOTAL;
					$total_units = $total_units+$product_row->ORDL_TOTAL_UNITS;
				}
			}
			$order_data['order_line_total']	 = number_format($line_total, 2, '.', '');
			$order_data['order_total_units'] = number_format($total_units, 2, '.', '');
			// append items data
			$order_data['order_products'] = $order_products;
            $order_data['total_count'] = (isset($product_count->product_count)) ? $product_count->product_count : 0;
		}
		return $order_data;
	 }
	  
	/**
     * Function used to remove order item
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function remove_order() {
		
		// get post data
		$order_number = $this->_common->test_input($_REQUEST['order_number']);
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		// set default response 
		$_response['result_code'] = 0;
		$_response['message']	 = $this->_common->langText($this->_locale,'txt.stock.error.no_order_found');
		if(!empty($order_number) && !empty($outlet_id)) {
			// get order's result
			$ord_query = "SELECT * from " .$this->OrdhTbl. " WHERE ORDH_ORDER_NO = $order_number AND ORDH_OUTLET = $outlet_id";
			$ord_result = $this->_mssql->my_mssql_query($ord_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($ord_query,$this->con) > 0 ) {
				// set order result
				$ord_result = $this->_mssql->mssql_fetch_object_query($ord_result);
				if(!empty($ord_result) && isset($ord_result->ORDH_DOCUMENT_STATUS) && $ord_result->ORDH_DOCUMENT_STATUS == 'NEW') {
					// remove order entry
					$remove_ord_sql = "DELETE FROM ".$this->OrdhTbl." WHERE ORDH_ORDER_NO = $order_number AND ORDH_OUTLET = $outlet_id";
					$this->_mssql->my_mssql_query($remove_ord_sql,$this->con);
					
					// remove order item entries
					$remove_ord_item_sql = "DELETE FROM ".$this->OrdlTbl." WHERE ORDL_ORDER_NO = $order_number AND ORDL_PRODUCT = $prod_number AND ORDL_OUTLET = $outlet_id";
					$this->_mssql->my_mssql_query($remove_ord_item_sql,$this->con);
					// set success response
					$_response['result_code'] = 1;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.deleted');
				} else {
					$_response['message']	 = $this->_common->langText($this->_locale,'txt.stock.error.order_not_allowed_remove');
				}	
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_order_id');
		}
		return $_response;
	 }
	  
	/**
     * Function used to remove order item
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function remove_order_item() {
		
		// get post data
		$order_number = $this->_common->test_input($_REQUEST['order_number']);
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$prod_number  = $this->_common->test_input($_REQUEST['prod_number']);
		// set default response 
		$_response['result_code'] = 0;
		$_response['message']	 = $this->_common->langText($this->_locale,'txt.stock.error.no_order_found');
		if(!empty($order_number) && !empty($prod_number)  && !empty($outlet_id)) {
			// get order's result
			$ord_query = "select * from " .$this->OrdhTbl. " WHERE ORDH_ORDER_NO = $order_number AND ORDH_OUTLET = $outlet_id";
			$ord_result = $this->_mssql->my_mssql_query($ord_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($ord_query,$this->con) > 0 ) {
				// set order result
				$ord_result = $this->_mssql->mssql_fetch_object_query($ord_result);
				if(!empty($ord_result) && isset($ord_result->ORDH_DOCUMENT_STATUS) && $ord_result->ORDH_DOCUMENT_STATUS == 'NEW') {
					// remove order item entries
					$remove_ord_item_sql = "DELETE FROM ".$this->OrdlTbl." WHERE ORDL_ORDER_NO = $order_number AND ORDL_PRODUCT = $prod_number AND ORDL_OUTLET = $outlet_id";
					$this->_mssql->my_mssql_query($remove_ord_item_sql,$this->con);
					// set success response
					$_response['result_code'] = 1;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.deleted');
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.order_not_allowed_remove');
				}	
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.invalid_order_items');
		}
		return $_response;
	 }
	 
	/**
     * Function used to get stocktake outlet list
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	public function get_stocktake_outlets() {
		
		//get post stocktake params
		$user_id = $this->_common->test_input($_REQUEST['user_id']);
		$_response['result_code'] = 0;
		if (empty($user_id)) {
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
        } else {
			// checking user email is already exists or not
			$user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id;
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
				// get user row data
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				// get associated zone results from user's zone 
				$user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
				
				if(!empty($user_outlets) && count($user_outlets) > 0) {
					// set outlet ids
					$outlet_ids = '';
					foreach($user_outlets as $key=>$outlets) {
						if((count($user_outlets)-1) == $key ) {
							$outlet_ids .= $outlets['outlet_id'];
						} else {
							$outlet_ids .= $outlets['outlet_id'].',';
						}
					}
					
					// get stocktake outlet data
					$stocktake_query = "SELECT CODE_KEY_NUM,CODE_DESC,CODE_ALP_1 FROM ".$this->CODETBL." WHERE CODE_KEY_TYPE = 'STOCKTAKE' AND CODE_KEY_NUM in ( $outlet_ids )";
					$stocktake_result = $this->_mssql->my_mssql_query($stocktake_query,$this->con);
					if( $this->_mssql->pdo_num_rows_query($stocktake_query,$this->con) > 0 ) {
						$stocktake = array();
						while($row = $this->_mssql->mssql_fetch_object_query($stocktake_result)) {
							// prepare stocktake outlet values
							$stocktake_array['stocktake_outlet_id']   = $row->CODE_KEY_NUM;
							$stocktake_array['stocktake_outlet_name'] = utf8_encode($row->CODE_DESC);
							$stocktake_array['stocktake_amount'] = $row->CODE_ALP_1;
							$stocktake_products = array();	
							// manage stocktake outlet products data
							/*$stocktake_product_query = "SELECT CODE_KEY_ALP,CODE_DESC,CODE_NUM_2,CODE_NUM_3,CODE_NUM_4,CODE_NUM_5,CODE_NUM_6 FROM ".$this->CODETBL." WHERE CODE_KEY_TYPE = 'STOCKTAKEI' AND CODE_KEY_NUM = ".$row->CODE_KEY_NUM;
							$stocktake_product_result = $this->_mssql->my_mssql_query($stocktake_product_query,$this->con);
							
							if( $this->_mssql->mssql_num_rows_query($stocktake_product_result) > 0 ) {
								while($product_row = $this->_mssql->mssql_fetch_object_query($stocktake_product_result)) {
						
									// prepare stocktake outlet's product values
									$stocktake_products_array['prod_number']= $product_row->CODE_KEY_ALP;
									$stocktake_products_array['prod_desc']  = utf8_encode($product_row->CODE_DESC);
									$stocktake_products_array['item_count'] = $product_row->CODE_NUM_2;	
									$stocktake_products_array['line_total'] = $product_row->CODE_NUM_3;	
									$stocktake_products_array['unit_cost']  = $product_row->CODE_NUM_6;	
									$stocktake_products_array['unit_on_hand'] = $product_row->CODE_NUM_4;
									$stocktake_products_array['variance_units'] = $product_row->CODE_NUM_5;
									// append product array
									$stocktake_products[] = $stocktake_products_array;
								}
							}*/
							$stocktake_array['stocktake_products'] = $stocktake_products;
							$stocktake[] = $stocktake_array;
							
						}
						
						$_response['result_code'] = 1;
						$_response['result'] = $stocktake;
					} else {
						
						$_response['message'] = $this->_common->langText($this->_locale,'txt.no_record_found');
					}
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.error.suppliernot_associate');
				}
			} else {
				 $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
		}
		return $_response;
	}
	
	/**
     * Function used to get stocktake outlet products list
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function get_stocktake_outlet_products() {
		
		//get post stocktake params
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		// set pagination limit and offset values
		$offset = (!empty($offset)) ? $offset : 0;
		$limit = (!empty($limit)) ? $limit : 10;
		$_response['result_code'] = 0;
		if (empty($outlet_id)) {
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
        } else {
			// get total count of records
			$stc_product_count_query = "SELECT count(CODE_KEY_ALP) as product_count FROM ".$this->CODETBL." WHERE CODE_KEY_TYPE = 'STOCKTAKEI' AND CODE_KEY_NUM = ".$outlet_id;
			$stc_product_count_data = $this->_mssql->my_mssql_query($stc_product_count_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($stc_product_count_query,$this->con) > 0 ) {
				$product_count = $this->_mssql->mssql_fetch_object_query($stc_product_count_data);
				// manage stocktake outlet products data
				$stocktake_product_query = "SELECT * FROM ( SELECT CODE_KEY_ALP,CODE_DESC,CODE_NUM_2,CODE_NUM_3,CODE_NUM_4,CODE_NUM_5,CODE_NUM_6, ROW_NUMBER() OVER (ORDER BY CODE_KEY_ALP ASC) as row FROM ".$this->CODETBL." WHERE CODE_KEY_TYPE = 'STOCKTAKEI' AND CODE_KEY_NUM = ".$outlet_id." ) a WHERE row > $offset and row <= $limit";
				$stocktake_product_result = $this->_mssql->my_mssql_query($stocktake_product_query,$this->con);
				$stocktake_products = array();
				if( $this->_mssql->pdo_num_rows_query($stocktake_product_query,$this->con) > 0 ) {
					while($product_row = $this->_mssql->mssql_fetch_object_query($stocktake_product_result)) {
						// prepare stocktake outlet's product values
						$stocktake_products_array['prod_number']= $product_row->CODE_KEY_ALP;
						$stocktake_products_array['prod_desc']  = utf8_encode($product_row->CODE_DESC);
						$stocktake_products_array['item_count'] = $product_row->CODE_NUM_2;	
						$stocktake_products_array['line_total'] = number_format($product_row->CODE_NUM_3,2, '.', '');	
						$stocktake_products_array['unit_cost']  = number_format($product_row->CODE_NUM_6,2, '.', '');	
						$stocktake_products_array['unit_on_hand'] = $product_row->CODE_NUM_4;
						$stocktake_products_array['variance_units'] = $product_row->CODE_NUM_5;
						$stocktake_products_array['status'] = 1; // 1 : No update, 2 : New, 3 : Update, 4 : Delete
						// append product array
						$stocktake_products[] = $stocktake_products_array;
					}
				}
			}
			$_response['result_code'] = 1;
			$_response['total_count'] = (isset($product_count->product_count)) ? $product_count->product_count : 0;
			$_response['result'] = $stocktake_products;
		}
		return $_response;
	}
	
	/**
     * Function used to delete stocktake data
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function remove_stocktake() {
		
		//get post stocktake params
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		if (!empty($outlet_id)) {
			// remove stocktake entries
			$remove_stocktake_sql = "DELETE FROM ".$this->CODETBL." WHERE CODE_KEY_NUM = $outlet_id AND CODE_KEY_TYPE in ( 'STOCKTAKE' , 'STOCKTAKEI') ";
			$this->_mssql->my_mssql_query($remove_stocktake_sql,$this->con);
			// set success response
			$_response['result_code'] = 1;
			$_response['message']   = $this->_common->langText($this->_locale,'txt.stock.success.remove_stocktake');
        } else {
			$_response['result_code'] = 0;
			$_response['message']   = $this->_common->langText($this->_locale,'txt.stock.error.invalid_outlet');
		}
		return $_response;
	 }
		
	/**
     * Function used to check outlet's existance in stocktake
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function set_stocktake_outlet() {
		
		//get post stocktake params
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$outlet_name = $this->_common->test_input($_REQUEST['outlet_name']);
		if (!empty($outlet_id)) {
			// get outlet value in stocktake batch
			$stocktake_query    = "select * from ".$this->CODETBL." where code_key_num = $outlet_id and CODE_KEY_TYPE = 'STOCKTAKE'";
			$get_stocktake_data = $this->_mssql->my_mssql_query($stocktake_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($stocktake_query,$this->con) > 0 ) {
				$_response['result_code'] = 0;
				$_response['message']   = $this->_common->langText($this->_locale,'txt.stock.error.already_in_stocktake');
			} else {
				if(!empty($outlet_name)) {
					// prepare sql for inserting outlet data entry
					$insert_outlet_sql = "INSERT ".$this->CODETBL." (CODE_KEY_TYPE,CODE_KEY_NUM,CODE_KEY_ALP,CODE_DESC,CODE_ALP_1,CODE_NUM_1,CODE_NUM_2,CODE_NUM_3) VALUES ('STOCKTAKE',$outlet_id,0,'".$outlet_name."',0,1,0,0)";
					$this->_mssql->my_mssql_query($insert_outlet_sql,$this->con); 
				}
				// set success response message param
				$_response['result_code'] = 1;
				$_response['message']   = $this->_common->langText($this->_locale,'txt.stock.error.not_in_stocktake');
			}
        } else {
			$_response['result_code'] = 0;
            $_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.invalid_outlet');
		}
		return $_response;
	}
		
	/**
     * Function used to add stocktake
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function add_stocktake() {
		
		//get post stocktake params
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$outlet_name = $this->_common->test_input($_REQUEST['outlet_name']);
		$products  = $_REQUEST['products'];
		$prod_exist_flag = false;
		// set error for temprary time
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.temp_stocktake_error');
		return $_response;
		// set default response params
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.add_products');
		// set false response in case of outlet authentication
		if (empty($outlet_id) || empty($outlet_name)) {
			$_response['result_code'] = 0;
            $_response['message']   = $this->_common->langText($this->_locale,'txt.stock.error.invalid_outlet');
			return $_response;
        }
		$filename =  BASEPATH.'/log.txt'; 
		file_put_contents($filename, $products);
        $products = json_decode($products); // convert json into array
		if (!empty($products) && count($products) > 0) {
			// set outlet values
			$total_gross_price = 0;
			// insert stocktake product log in db
			foreach($products as $key=>$product) {
				// set input item values
				$prod_number  = $product->prod_number;
				$prod_desc    = str_replace( "'", "", $product->prod_desc);
				$item_count   = $product->item_count;
				$line_total   = $product->line_total;
				$unit_cost    = $product->unit_cost;
				$code_num_4   = $product->unit_on_hand;
				$code_num_5   = $product->variance_units;
				$code_num_7   = 0;
				$status   	  = (isset($product->status)) ? $product->status : 1;  // 1 : No update, 2 : New, 3 : Update, 4 : Delete
				
				if( $status == 2 ) {
					// inserting product entry in stocktake
					$insert_item_sql = "INSERT ".$this->CODETBL." (CODE_KEY_TYPE,CODE_KEY_NUM,CODE_KEY_ALP,CODE_DESC,CODE_NUM_1,CODE_NUM_2,CODE_NUM_3,CODE_NUM_4,CODE_NUM_5,CODE_NUM_6,CODE_NUM_7,CODE_NUM_8 ) VALUES ('STOCKTAKEI',$outlet_id,$prod_number,'".$prod_desc."',$prod_number,'".$item_count."','".$line_total."','".$code_num_4."','".$code_num_5."','".$unit_cost."','".$code_num_7."','".$item_count."')";
					$this->_mssql->my_mssql_query($insert_item_sql,$this->con);
				} else if($status == 3) {
					// update product entry in stocktake
					$update_sql = "UPDATE ".$this->CODETBL." SET CODE_NUM_2 = '".$item_count."',CODE_NUM_3 = '".$line_total."',CODE_NUM_4 = '".$code_num_4."',CODE_NUM_5 = '".$code_num_5."',CODE_NUM_6 = '".$unit_cost."',CODE_NUM_7 = '".$code_num_7."',CODE_NUM_8 = '".$item_count."' WHERE CODE_KEY_TYPE = 'STOCKTAKEI' and CODE_KEY_NUM = $outlet_id and CODE_KEY_ALP = $prod_number";
					$this->_mssql->my_mssql_query($update_sql,$this->con);
				} else if($status == 4) {
					// delete product entry from stocktake
					$remove_stocktake_prod_sql = "DELETE FROM ".$this->CODETBL." WHERE CODE_KEY_TYPE = 'STOCKTAKEI' AND CODE_KEY_NUM = $outlet_id and CODE_KEY_ALP = $prod_number";
					$this->_mssql->my_mssql_query($remove_stocktake_prod_sql,$this->con);
				}
			}
			
			// get stocktake product's total amount details
			$stocktake_header_query = "select sum(CODE_NUM_3) as total_gross_price from ".$this->CODETBL." where CODE_KEY_NUM = $outlet_id and CODE_KEY_TYPE = 'STOCKTAKEI'";
			$get_stocktake_header_data = $this->_mssql->my_mssql_query($stocktake_header_query,$this->con);
			$total_gross_price = 0;
			if( $this->_mssql->pdo_num_rows_query($stocktake_header_query,$this->con) > 0 ) {
				$stocktake_amt_result = $this->_mssql->mssql_fetch_object_query($get_stocktake_header_data);
				$total_gross_price = $stocktake_amt_result->total_gross_price;
			}
			
			// update outlet total price data entry
			$update_sql = "UPDATE ".$this->CODETBL." SET CODE_ALP_1 = '".$total_gross_price."',CODE_NUM_3 = '".$total_gross_price."' WHERE CODE_KEY_TYPE = 'STOCKTAKE' and CODE_KEY_NUM = $outlet_id";
			$this->_mssql->my_mssql_query($update_sql,$this->con);
				
			// prepare success response
			$_response['result_code'] = 1;	
			$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.add_products');
        }
		return $_response;
	}
	
	/**
     * Function used to add multiple products in stocktake
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function old_bulk_stocktake_upload() {
		//get post stocktake params
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		$apn_items  = $_REQUEST['apn_items'];
		$_response['result_code'] = 0;
		if (empty($outlet_id)) {
            $_response['message']   = $this->_common->langText($this->_locale,'txt.stock.error.invalid_outlet');
			return $_response;
        }
        
		$apn_items = json_decode($apn_items); // convert json into array
		if (!empty($apn_items) && count($apn_items) > 0) {
			// get stocktake outlet data
			$stock_outlet_query = "SELECT CODE_KEY_TYPE FROM ".$this->CODETBL." WHERE CODE_KEY_NUM = $outlet_id AND CODE_KEY_TYPE = 'STOCKTAKE'";
			$get_stock_outlet = $this->_mssql->my_mssql_query($stock_outlet_query,$this->con);
			$stock_outlet_result = $this->_mssql->mssql_fetch_object_query($get_stock_outlet);
			if(!empty($stock_outlet_result)) {
				// set outlet total price
				$stock_outlet_total_amount = (!empty($stock_outlet_result->CODE_ALP_1)) ? $stock_outlet_result->CODE_ALP_1 : 0;
				// set outlet values
				$total_gross_price = 0;		
				$products = array();
				foreach($apn_items as $key=>$item) {
					// get product's id from apn numbers
					$prod_apn_query = "SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost FROM ".$this->OUTPTBL." JOIN ".$this->PRODTBL." on Outp_Product = Prod_Number JOIN ".$this->APNTBL." on APN_PRODUCT = Prod_Number where APN_NUMBER = '".$item->apn_number."' AND OUTP_OUTLET = $outlet_id";
					$get_prod_apn_data = $this->_mssql->my_mssql_query($prod_apn_query,$this->con);
					$apn_result = $this->_mssql->mssql_fetch_object_query($get_prod_apn_data);
					if(!empty($apn_result)) {
						$item_count = $item->item_count;
						// set unit cost
						$unit_cost = (!empty($apn_result->UnitCost)) ? number_format($apn_result->UnitCost, 2, '.', '') : 0;
						// set line total
						$line_total = $item_count*$unit_cost;
						// set units on hand
						$units_on_hand = $apn_result->OUTP_QTY_ONHAND;
						// set variance units
						if($units_on_hand < 0) {
							$variance_units = -$item_count-$units_on_hand;
							$variance_units = abs($variance_units);
						} else {
							$variance_units = $item_count-$units_on_hand;
						}
						
						// set input item values
						$prod_number  = $apn_result->Prod_Number;
						$prod_desc    = $apn_result->Prod_Desc;
						$item_count   = $item_count;
						$line_total   = $line_total;
						$unit_cost    = $unit_cost;
						$code_num_4   = $units_on_hand;
						$code_num_5   = $variance_units;
						$code_num_7   = 0;
						
						// get stocktake product data if exist
						$stock_product_query = "SELECT CODE_KEY_NUM,CODE_NUM_3 FROM ".$this->CODETBL." WHERE CODE_KEY_NUM = $outlet_id AND CODE_KEY_TYPE = 'STOCKTAKEI' AND CODE_KEY_ALP = $prod_number";
						$get_stock_product = $this->_mssql->my_mssql_query($stock_product_query,$this->con);
						$stock_product_result = $this->_mssql->mssql_fetch_object_query($get_stock_product);
						if(!empty($stock_product_result)) {
							// remove previose existing stocktake product
							$remove_stocktake_sql = "DELETE FROM ".$this->CODETBL." WHERE CODE_KEY_NUM = $outlet_id AND CODE_KEY_ALP = $prod_number AND CODE_KEY_TYPE = 'STOCKTAKEI') ";
							$this->_mssql->my_mssql_query($remove_stocktake_sql,$this->con);
							// manage stocktake outlet amount
							$stock_outlet_total_amount = $stock_outlet_total_amount-$stock_product_result->CODE_NUM_3;
						}
						// insert product data in stocktake
						$insert_item_sql = "INSERT ".$this->CODETBL." (CODE_KEY_TYPE,CODE_KEY_NUM,CODE_KEY_ALP,CODE_DESC,CODE_NUM_1,CODE_NUM_2,CODE_NUM_3,CODE_NUM_4,CODE_NUM_5,CODE_NUM_6,CODE_NUM_7,CODE_NUM_8 ) VALUES ('STOCKTAKEI',$outlet_id,$prod_number,'".$prod_desc."',$prod_number,$item_count,$line_total,'".$code_num_4."','".$code_num_5."',$unit_cost,$code_num_7,$item_count)";
						$this->_mssql->my_mssql_query($insert_item_sql,$this->con);
						
						// add item price in outlet total amount
						$total_gross_price = $total_gross_price+$line_total;
					}
				}
				// set stocktake total amount
				$stock_total_gross_price = $stock_outlet_total_amount+$total_gross_price;
				
				// update outlet total price data entry
				$update_sql = "UPDATE ".$this->CODETBL." SET CODE_ALP_1 = '".$stock_total_gross_price."',CODE_NUM_3 = '".$stock_total_gross_price."' WHERE CODE_KEY_TYPE = 'STOCKTAKE' and CODE_KEY_NUM = $outlet_id";
				$this->_mssql->my_mssql_query($update_sql,$this->con);
				
				// prepare success response
				$_response['result_code'] = 1;	
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.add_products');
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.stock_outlet_valid');
			}
        } else {
            $_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.add_products');
		}
		return $_response;
	}
	
	
	/**
     * Function used to get active products
	 * @param int search_type, string search_keyword, int limit , int offset
	 * @return array
	 * @access public
	 * 
	 */
	public function active_products() {
		
		// get post data
		$search_type = $this->_common->test_input($_REQUEST['search_type']); // 1: Department code , 2 : Replicate code
		$search_keyword = $this->_common->test_input($_REQUEST['search_keyword']);
		$outlet_id  = $_REQUEST['outlet_id'];
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
		$product_outlet_query = '';
		$product_outlet_where_str = '';
		if(!empty($outlet_id)) {
			$product_outlet_join_str = "JOIN " .$this->OUTPTBL. " ON Prod_Number = Outp_Product";
			$product_outlet_where_str = "OUTP_OUTLET = $outlet_id AND OUTP_STATUS = 'Active' AND";
		}
        $directSearchFlag =0;
		if(!empty($search_keyword)) {
			$search_type = (!empty($search_type)) ? $search_type : '';
			if($search_type == 1) {
				
				// query for fetching total count as per department code
				$product_count_query = "SELECT count(Prod_Number) as product_count FROM ".$this->PRODTBL." $product_outlet_join_str LEFT JOIN ".$this->CODETBL." on CODE_KEY_NUM = PROD_DEPARTMENT where $product_outlet_where_str PROD_STATUS = 'Active' and CODE_KEY_TYPE = 'DEPARTMENT' and CODE_KEY_NUM = $search_keyword";
				// query for fetch active product record as per department code
				$product_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,CODE_KEY_NUM,CODE_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->PRODTBL." $product_outlet_join_str LEFT JOIN ".$this->CODETBL." on CODE_KEY_NUM = PROD_DEPARTMENT where $product_outlet_where_str PROD_STATUS = 'Active' and CODE_KEY_TYPE = 'DEPARTMENT' and CODE_KEY_NUM = $search_keyword ) a WHERE row > ".$offset." and row <= ".$limit;
				
			} else if($search_type == 2) {
				// query for fetching total count as per replicate code
				$product_count_query = "SELECT count(Prod_Number) as product_count FROM ".$this->PRODTBL." $product_outlet_join_str where $product_outlet_where_str PROD_STATUS = 'Active' and PROD_REPLICATE = '".$search_keyword."'";
				// query for fetch active product record as per replicate code
				$product_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->PRODTBL." $product_outlet_join_str where $product_outlet_where_str PROD_STATUS = 'Active' and PROD_REPLICATE = '".$search_keyword."' ) a WHERE row > ".$offset." and row <= ".$limit;
				
			} else {
                if(is_numeric($search_keyword)){
                    $directSearchFlag =1;
                    // query for fetching total count as per search keyword
                    $product_count_query = "SELECT count(Prod_Number) as product_count FROM ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active' and ( Prod_Number = $search_keyword )";
                    // query for fetch active product record as per search keyword
                    $product_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str  PROD_STATUS = 'Active' and ( Prod_Number = $search_keyword ) ) a WHERE row > ".$offset." AND row <= ".$limit;
                }else{
                    // query for fetching total count as per search keyword
                    $product_count_query = "SELECT count(Prod_Number) as product_count FROM ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active' and ( Prod_Desc like '%".$search_keyword."%' )";
                    // query for fetch active product record as per search keyword
                    $product_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active' and (  Prod_Desc like '%".$search_keyword."%' ) ) a WHERE row > ".$offset." AND row <= ".$limit;
                }
			}
		} else {
			// query for fetching total count of active product record 
			$product_count_query = "SELECT count(Prod_Number) as product_count FROM ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active'";
			// query for fetch active product record 
			$product_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active' ) a WHERE row > ".$offset." and row <= ".$limit;
		}
		$product_count_data = $this->_mssql->my_mssql_query($product_count_query,$this->con);
        /*Check For APN Number */  
        if(($directSearchFlag == 1) && ($product_count_data->product_count == 0) && !empty($search_keyword)){
                $prod_apn_query = "SELECT APN_PRODUCT FROM ".$this->APNTBL." where APN_NUMBER = '".$search_keyword."'";
                $get_prod_apn_data = $this->_mssql->my_mssql_query($prod_apn_query,$this->con);
                $prod_apn_numbers = array();
                $APN_PRODUCT = '';
                if( $this->_mssql->pdo_num_rows_query($prod_apn_query,$this->con) > 0 ) {
                    while( $result = $this->_mssql->mssql_fetch_object_query($get_prod_apn_data) ) {
                        // set apn values
                        $APN_PRODUCT	  = (isset($result->APN_PRODUCT)) ? trim($result->APN_PRODUCT) : "";
                    }
                }
            
            if(!empty($APN_PRODUCT)){
                    // query for fetching total count as per search keyword
                    $product_count_query = "SELECT count(Prod_Number) as product_count FROM ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active' and ( Prod_Number = $APN_PRODUCT )";
                    // query for fetch active product record as per search keyword
                    $product_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from ".$this->PRODTBL." $product_outlet_join_str WHERE $product_outlet_where_str PROD_STATUS = 'Active' and ( Prod_Number = $APN_PRODUCT ) ) a WHERE row > ".$offset." AND row <= ".$limit;
                    $product_count_data = $this->_mssql->my_mssql_query($product_count_query,$this->con);
            }
                
        }
        
		if( $this->_mssql->pdo_num_rows_query($product_count_query,$this->con) > 0 ) {
			$product_count = $this->_mssql->mssql_fetch_object_query($product_count_data);
			$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
			$products = array();
			if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
				while( $result = $this->_mssql->mssql_fetch_object_query($get_product_data) ) {
				
					// set product values
					$array['prod_number']	= (isset($result->Prod_Number)) ? trim($result->Prod_Number) : "";
					$array['prod_desc'] 	= (isset($result->Prod_Desc)) ? utf8_encode($result->Prod_Desc) : "";
					$array['prod_pos_desc'] = (isset($result->PROD_POS_DESC)) ? utf8_encode($result->PROD_POS_DESC) : "";
					$products[] = $array;
				}
				// set response values
				$_response['result_code'] = 1;
				$_response['total_count'] = (isset($product_count->product_count)) ? $product_count->product_count : 0;
				$_response['result'] = $products;
			} else {
				$_response['result_code'] = 0;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');	
		}
		
		return $_response;
	}
	
	/**
     * Function used to get active product details
	 * @param int prod_number
	 * @return array
	 * @access public
	 * 
	 */
	public function active_product_details() {
		
		// get post data
		$prod_number = $this->_common->test_input($_REQUEST['prod_number']);
		$user_id = $this->_common->test_input($_REQUEST['user_id']);
		
		if(!empty($prod_number) && !empty($user_id)) {
			// checking user email is already exists or not
			$user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id; 
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
	
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
				// get user row data
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				// get product result
				$product_query = "SELECT * FROM ".$this->PRODTBL." WHERE PROD_NUMBER = $prod_number";
				$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
					
				if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
					// get product row data
					$result = $this->_mssql->mssql_fetch_object_query($get_product_data);
					// get product apn numbers
					$prod_apn_numbers = $this->product_apn_numbers($result->PROD_NUMBER);
					
					// set product detail values
					$product_detail['prod_number']	 = (isset($result->PROD_NUMBER)) ? trim($result->PROD_NUMBER) : "";
					$product_detail['prod_desc'] 	 = (isset($result->PROD_DESC)) ? utf8_encode($result->PROD_DESC) : "";
					$product_detail['prod_pos_desc'] = (isset($result->PROD_POS_DESC)) ? utf8_encode($result->PROD_POS_DESC) : "";
					$product_detail['prod_status']   = (isset($result->PROD_STATUS)) ? trim($result->PROD_STATUS) : "";
					$product_detail['prod_type']     = (isset($result->PROD_TYPE)) ? trim($result->PROD_TYPE) : "";
					$product_detail['prod_size']     = (isset($result->PROD_SIZE)) ? trim($result->PROD_SIZE) : "";
					$product_detail['prod_supplier'] = (isset($result->PROD_SUPPLIER)) ? $this->get_code_name($result->PROD_SUPPLIER,'SUPPLIER') : "";
					$product_detail['prod_supplier_code'] = (isset($result->PROD_SUPPLIER)) ? $result->PROD_SUPPLIER : "";
					$product_detail['prod_group']    = (isset($result->PROD_GROUP)) ? $this->get_code_name($result->PROD_GROUP,'GROUP') : "";
					$product_detail['prod_sell_unit_qty'] = (isset($result->PROD_UNIT_QTY)) ? trim($result->PROD_UNIT_QTY) : "";
					$product_detail['prod_carton_qty'] = (isset($result->PROD_CARTON_QTY)) ? trim($result->PROD_CARTON_QTY) : "";
					$product_detail['prod_department'] = (isset($result->PROD_DEPARTMENT)) ? $this->get_code_name($result->PROD_DEPARTMENT,'DEPARTMENT') : "";
					$product_detail['prod_commodity']  = (isset($result->PROD_COMMODITY)) ? $this->get_code_name($result->PROD_COMMODITY,'COMMODITY') : "";
					$product_detail['prod_category']   = (isset($result->PROD_CATEGORY)) ? $this->get_code_name($result->PROD_CATEGORY,'CATEGORY') : "";
					$product_detail['prod_replicate']  = (isset($result->PROD_REPLICATE)) ? trim($result->PROD_REPLICATE) : "";
					$product_detail['prod_manufacturer'] = (isset($result->PROD_MANUFACTURER)) ? $this->get_code_name($result->PROD_MANUFACTURER,'MANUFACTURER') : "";
					$product_detail['prod_last_apn_sold'] = (isset($prod_apn_numbers[0])) ? $prod_apn_numbers[0] : '';
					
					// set product parent values
					$parent_product['prod_parent']   = (isset($result->PROD_PARENT)) ? trim($result->PROD_PARENT) : "";
					$parent_product['prod_parent_unit_qty'] =  ""; // need to clear 
					$parent_product['prod_additional_info'] = (isset($result->PROD_INFO)) ? trim($result->PROD_INFO) : "";
					$parent_product['prod_restrict_outlet'] = (isset($result->PROD_ACCESS_OUTLET)) ? trim($result->PROD_ACCESS_OUTLET) : "";
					$parent_product['prod_unit_of_measure'] = (isset($result->PROD_UNIT_MEASURE)) ? trim($result->PROD_UNIT_MEASURE) : "";
					$parent_product['prod_tax_code'] = (isset($result->PROD_TAX_CODE)) ? trim($result->PROD_TAX_CODE) : "";
					$parent_product['prod_date_added'] = (isset($result->PROD_DATE_ADDED)) ? trim($result->PROD_DATE_ADDED) : "";
					$parent_product['prod_date_changed'] = (isset($result->PROD_DATE_CHANGED)) ? trim($result->PROD_DATE_CHANGED) : "";
					$parent_product['prod_tare_weight'] = (isset($result->PROD_TARE_WEIGHT)) ? trim($result->PROD_TARE_WEIGHT) : "";
					$parent_product['prod_pos_weight_scales'] = (isset($result->PROD_SCALE_IND)) ? trim($result->PROD_SCALE_IND) : "";
					$parent_product['prod_slow_moving_item'] = (isset($result->PROD_SLOW_MOVING_IND)) ? trim($result->PROD_SLOW_MOVING_IND) : "";
					$parent_product['prod_variety_item'] 	= (isset($result->PROD_VARIETY_IND)) ? trim($result->PROD_VARIETY_IND) : "";
					$parent_product['prod_australian_made'] = (isset($result->PROD_AUST_MADE_IND)) ? trim($result->PROD_AUST_MADE_IND) : "";
					$parent_product['prod_australian_owned']= (isset($result->PROD_AUST_OWNED_IND)) ? trim($result->PROD_AUST_OWNED_IND) : "";
					$parent_product['prod_organic_produce'] = (isset($result->PROD_ORGANIC_IND)) ? trim($result->PROD_ORGANIC_IND) : "";
					$parent_product['prod_national'] 		= (isset($result->PROD_NATIONAL)) ? trim($result->PROD_NATIONAL) : "";
					
					// set warehouse order codes
					$warehouse_codes['csd_host_number'] = (isset($result->PROD_HOST_NUMBER)) ? trim($result->PROD_HOST_NUMBER) : "";
					$warehouse_codes['igad_host_number']= (isset($result->PROD_HOST_NUMBER_2)) ? trim($result->PROD_HOST_NUMBER_2) : "";
					$warehouse_codes['spar_host_number']= (isset($result->PROD_HOST_NUMBER_3)) ? trim($result->PROD_HOST_NUMBER_3) : "";
					$warehouse_codes['csd_host_type'] 	= (isset($result->PROD_HOST_ITEMTYPE)) ? trim($result->PROD_HOST_ITEMTYPE) : "";
					$warehouse_codes['igad_host_type']	= (isset($result->PROD_HOST_ITEMTYPE_2)) ? trim($result->PROD_HOST_ITEMTYPE_2) : "";
					$warehouse_codes['spar_host_type'] 	= (isset($result->PROD_HOST_ITEMTYPE_3)) ? trim($result->PROD_HOST_ITEMTYPE_3) : "";
					// set product APN number values
					$product_apn['prod_apn_numbers'] = $prod_apn_numbers;
					$product_apn['warehouse_order_codes'] = $warehouse_codes;
					
					// get associated zone results from user's zone 
					$user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
					$prod_outlets = array();	
					if(!empty($user_outlets) && count($user_outlets) > 0) {
						// set total outlet counts
						$outlet_count = count($user_outlets);
						$outlet_ids = '';
						foreach($user_outlets as $key=>$outlets) {
							if($key < ($outlet_count-1)) {
								$outlet_ids .= $outlets['outlet_id'].',';
							} else {
								$outlet_ids .= $outlets['outlet_id'];
							}
						}
						// get product's outlet result		
						$prod_outlets = $this->product_outlets($product_detail['prod_number'],$outlet_ids);
					}
								
					// append product result array in response
					$_response['result_code'] = 1;
					$_response['product_detail'] = $product_detail;
					$_response['parent_product'] = $parent_product;
					$_response['apn_numbers'] = $product_apn;
					$_response['product_outlets'] = $prod_outlets;

				} else {
					$_response['result_code'] = 0;
					$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.no_products');
				}
			} else {
				$_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.product_param_not_valid');
		}
		
		return $_response;
	}
	
	/**
     * Function used to arrange product's outlets
	 * @param int prod_number , array product_outlets
	 * @return array
	 * @access private
	 * 
	 */
	 private function product_outlets($prod_number,$outlet_ids='') {
		
		// get product's outlet result
		$prod_outlet_query = "SELECT * FROM ".$this->OUTPTBL." JOIN ".$this->OUTLTBL." ON OUTL_OUTLET = OUTP_OUTLET where OUTP_OUTLET IN ( $outlet_ids ) AND OUTP_STATUS = 'Active' AND OUTP_PRODUCT = '".$prod_number."'";
		$get_prod_outlet_data = $this->_mssql->my_mssql_query($prod_outlet_query,$this->con);
		$prod_outlets = array();
		if( $this->_mssql->pdo_num_rows_query($prod_outlet_query,$this->con) > 0 ) {
			while( $result = $this->_mssql->mssql_fetch_object_query($get_prod_outlet_data) ) {
			
				// set outlet values
				$outlet['outlet_id']	 = (isset($result->OUTP_OUTLET)) ? trim($result->OUTP_OUTLET) : "";
				$outlet['outlet_name'] 	 = (isset($result->OUTL_DESC)) ? trim($result->OUTL_DESC) : "";
				$prod_outlets[] = $outlet;
			}
		}
		return $prod_outlets;
	 }
	 
	 /**
     * Function used to arrange product's outlets
	 * @param int prod_number , array product_outlets
	 * @return array
	 * @access public
	 * 
	 */
	 public function product_outlet_details() {
		
		 // get post data
		$prod_number = $this->_common->test_input($_REQUEST['prod_number']);
		$outlet_id = $this->_common->test_input($_REQUEST['outlet_id']);
		if(!empty($prod_number) && !empty($outlet_id)) {
			
			// get product's outlet result
			$prod_outlet_query = "SELECT * FROM ".$this->OUTPTBL." JOIN ".$this->OUTLTBL." ON OUTL_OUTLET = OUTP_OUTLET JOIN ".$this->PRODTBL." ON PROD_NUMBER = OUTP_PRODUCT where OUTP_PRODUCT = $prod_number and OUTP_OUTLET = $outlet_id";
			$get_prod_outlet_data = $this->_mssql->my_mssql_query($prod_outlet_query,$this->con);
			$prod_outlets = array();
			if( $this->_mssql->pdo_num_rows_query($prod_outlet_query,$this->con) > 0 ) {
				$result = $this->_mssql->mssql_fetch_object_query($get_prod_outlet_data);
				
				if(!empty($result)) {
					// get rebate price values
					$terms_rebate = $this->promo_rebate_price(1,$result->OUTP_PRODUCT,$result->OUTP_OUTLET);
					$promo_scan_rebate = $this->promo_rebate_price(2,$result->OUTP_PRODUCT,$result->OUTP_OUTLET);
					$promo_purchase_rebate = $this->promo_rebate_price(3,$result->OUTP_PRODUCT,$result->OUTP_OUTLET);
					// get total rebate price
					$total_rebate_price = $terms_rebate+$promo_scan_rebate+$promo_purchase_rebate;
					// find minimum price
					$minimum_cost =  min($result->OUTP_CARTON_COST, $result->OUTP_PROM_CTN_COST); 
					// set final carton cost
					$final_carton_cost = $minimum_cost-$total_rebate_price; 
					// set final item cost
					$final_item_cost = round($final_carton_cost/$result->PROD_CARTON_QTY,2); 
					
					// prepare normal price details
					$normal_prices['normal_price1'] = (isset($result->OUTP_NORM_PRICE_1)) ? trim($result->OUTP_NORM_PRICE_1) : "";
					$normal_prices['normal_price2'] = (isset($result->OUTP_NORM_PRICE_2)) ? trim($result->OUTP_NORM_PRICE_2) : "";
					$normal_prices['normal_price3'] = (isset($result->OUTP_NORM_PRICE_3)) ? trim($result->OUTP_NORM_PRICE_3) : "";
					$normal_prices['normal_price4'] = (isset($result->OUTP_NORM_PRICE_4)) ? trim($result->OUTP_NORM_PRICE_4) : "";
					$normal_prices['normal_price5']	= (isset($result->OUTP_NORM_PRICE_5)) ? trim($result->OUTP_NORM_PRICE_5) : "";
					$normal_prices['normal_price1_gp']	= round($this->price_gp($normal_prices['normal_price1'],$final_item_cost),2);
					$normal_prices['normal_price2_gp']	= round($this->price_gp($normal_prices['normal_price2'],$final_item_cost),2);
					$normal_prices['normal_price3_gp']	= round($this->price_gp($normal_prices['normal_price3'],$final_item_cost),2);
					$normal_prices['normal_price4_gp']	= round($this->price_gp($normal_prices['normal_price4'],$final_item_cost),2);
					$normal_prices['normal_price5_gp']	= round($this->price_gp($normal_prices['normal_price5'],$final_item_cost),2);
					$normal_prices['hold_option']	= (isset($result->OUTP_HOLD_NORM)) ? trim($result->OUTP_HOLD_NORM) : "";
					$normal_prices['open_price'] 	= (isset($result->OUTP_OPEN_PRICE_YN)) ? trim($result->OUTP_OPEN_PRICE_YN) : "";
					$normal_prices['promo_price1'] 	= (isset($result->OUTP_PROM_PRICE_1)) ? trim($result->OUTP_PROM_PRICE_1) : "";
					$normal_prices['promo_price2'] 	= (isset($result->OUTP_PROM_PRICE_2)) ? trim($result->OUTP_PROM_PRICE_2) : "";
					$normal_prices['promo_price3'] 	= (isset($result->OUTP_PROM_PRICE_3)) ? trim($result->OUTP_PROM_PRICE_3) : "";
					$normal_prices['promo_price4'] 	= (isset($result->OUTP_PROM_PRICE_4)) ? trim($result->OUTP_PROM_PRICE_4) : "";
					$normal_prices['special_price'] = (isset($result->OUTP_SPEC_PRICE)) ? trim($result->OUTP_SPEC_PRICE) : "";
					$normal_prices['special_carton_cost'] = (isset($result->OUTP_SPEC_CARTON_COST)) ? trim($result->OUTP_SPEC_CARTON_COST) : "";
					$normal_prices['promo_price1_gp'] = round($this->price_gp($normal_prices['promo_price1'],$final_item_cost),2);
					$normal_prices['promo_price2_gp'] = round($this->price_gp($normal_prices['promo_price2'],$final_item_cost),2);
					$normal_prices['special_price_gp']= round($this->price_gp($normal_prices['special_price'],$final_item_cost),2);
					$normal_prices['promo'] = "";
					$normal_prices['mix_match'] 	= (isset($result->OUTP_MIXMATCH)) ? trim($result->OUTP_MIXMATCH) : "";
					$normal_prices['mix_match2'] 	= (isset($result->OUTP_MIXMATCH2)) ? trim($result->OUTP_MIXMATCH2) : "";
					$normal_prices['offer_price1'] 	= (isset($result->OUTP_OFFER)) ? trim($result->OUTP_OFFER) : "";
					$normal_prices['offer_price2'] 	= (isset($result->OUTP_OFFER2)) ? trim($result->OUTP_OFFER2) : "";
					$normal_prices['offer_price3'] 	= (isset($result->OUTP_OFFER3)) ? trim($result->OUTP_OFFER3) : "";
					$normal_prices['offer_price4'] 	= (isset($result->OUTP_OFFER4)) ? trim($result->OUTP_OFFER4) : "";
					
					// prepare costs details
					$item_cost = round(($result->OUTP_CARTON_COST / $result->PROD_CARTON_QTY) * $result->PROD_UNIT_QTY,2);
					$costs['item_cost'] =   trim($item_cost);
					$costs['carton_cost'] = (isset($result->OUTP_CARTON_COST)) ? trim($result->OUTP_CARTON_COST) : "";
					$costs['promo_code'] = (isset($result->OUTP_BUY_PROM_CODE)) ? trim($result->OUTP_BUY_PROM_CODE) : "";
					$costs['promo_cost'] = (isset($result->OUTP_PROM_CTN_COST)) ? trim($result->OUTP_PROM_CTN_COST) : "";
					$costs['terms_rebate'] = $terms_rebate;
					$costs['promo_scan_rebate'] = $promo_scan_rebate;
					$costs['promo_purchase_rebate'] = $promo_purchase_rebate;
					$costs['host_ctn_cost'] = (isset($result->OUTP_CARTON_COST_HOST)) ? trim($result->OUTP_CARTON_COST_HOST) : "";
					$costs['inv_ctn_cost'] = (isset($result->OUTP_CARTON_COST_INV)) ? trim($result->OUTP_CARTON_COST_INV) : "";
					$costs['avg_ctn_cost'] = (isset($result->OUTP_CARTON_COST_AVG)) ? trim($result->OUTP_CARTON_COST_AVG) : "";
					$costs['default_supplier_code'] = (isset($result->OUTP_SUPPLIER)) ? trim($result->OUTP_SUPPLIER) : "";
					$costs['default_supplier'] = $this->get_code_name($costs['default_supplier_code'],'SUPPLIER');
					$costs['final_carton_cost'] = $final_carton_cost;
					$costs['final_item_cost'] = $final_item_cost;
					// prepare labels details
					$labels['no_of_labels'] = (isset($result->OUTP_LABEL_QTY)) ? trim($result->OUTP_LABEL_QTY) : "";
					$labels['short_label'] = (isset($result->OUTP_SHORT_LABEL_IND)) ? trim($result->OUTP_SHORT_LABEL_IND) : "";
					$labels['label_change'] = (isset($result->OUTP_CHANGE_LABEL_IND)) ? trim($result->OUTP_CHANGE_LABEL_IND) : "";
					$labels['deli_scale_plu'] = (isset($result->UTP_SCALE_PLU)) ? trim($result->UTP_SCALE_PLU) : "";
					$labels['units_on_hand']	= (isset($result->OUTP_QTY_ONHAND)) ? trim($result->OUTP_QTY_ONHAND) : "";
					$labels['min_stock_level'] = (isset($result->OUTP_MIN_ONHAND)) ? trim($result->OUTP_MIN_ONHAND) : "";
					$labels['max_stock_level'] = (isset($result->OUTP_MAX_ONHAND)) ? trim($result->OUTP_MAX_ONHAND) : "";
					$labels['min_reorder_qty'] = (isset($result->OUTP_MIN_REORDER_QTY)) ? trim($result->OUTP_MIN_REORDER_QTY) : "";
					$labels['packing_bin_loc'] = (isset($result->OUTP_PICKING_BIN_NO)) ? trim($result->OUTP_PICKING_BIN_NO) : "";
					$labels['skip_reorder_yn'] = (isset($result->OUTP_SKIP_REORDER_YN)) ? trim($result->OUTP_SKIP_REORDER_YN) : "";
					$labels['fifo_stock_yn']   = (isset($result->OUTP_FIFO_STOCK_YN)) ? trim($result->OUTP_FIFO_STOCK_YN) : "";
					// set outlet values
					$_response['result_code'] = 1;
					$_response['outlet_id']	 = (isset($result->OUTP_OUTLET)) ? trim($result->OUTP_OUTLET) : "";
					$_response['outlet_name'] = (isset($result->OUTL_DESC)) ? utf8_encode($result->OUTL_DESC) : "";
					$_response['normal_prices'] = $normal_prices;
					$_response['costs']	= $costs;
					$_response['labels'] = $labels;
				}
			}
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.stock.product_param_not_valid');
		}
		return $_response;
	 }
	 
	/**
	 * Function used to get code name
	 * @param int code_id , string code_key_type
	 * @return string
	 * @access private
	 * 
	 */
	 private function get_code_name($code_id=0,$code_key_type='') {
		
		if(!empty($code_id)) {
			// set code key field
			$code_key_field = 'CODE_KEY_NUM';
			if($code_key_type == 'SUPPLIER' || $code_key_type == 'MANUFACTURER') {
				$code_key_field = 'CODE_KEY_ALP';
			}
			// get code results
			$code_query = "SELECT CODE_DESC FROM CodeTbl WHERE CODE_KEY_TYPE = '".$code_key_type."' AND $code_key_field = '".$code_id."'";
			$get_code_data = $this->_mssql->my_mssql_query($code_query,$this->con );
			if( $this->_mssql->pdo_num_rows_query($code_query,$this->con) > 0 ) {
				$result = $this->_mssql->mssql_fetch_object_query($get_code_data);
			}
		}
		// set code name
		$code_name = (isset($result->CODE_DESC) && !empty($result->CODE_DESC)) ? $result->CODE_DESC : '';
		return $code_name;
	 }
	 
	 /**
	 * Function used to calculate GP item percentage
	 * @param float item_price , float final_item_cost
	 * @return string
	 * @access private
	 * 
	 */
	 private function price_gp($item_price=0,$final_item_cost=0) {
		$price_gp = (($item_price - $final_item_cost) * 100) / ($item_price);
		$price_gp = (!empty($price_gp)) ? $price_gp : 0;
		return $price_gp;
	 }
	 
	 /**
     * Function used to arrange product's apn numbers
	 * @param int prod_number
	 * @return array
	 * @access private
	 * 
	 */
	 private function product_apn_numbers($prod_number) {
		// get product's apn numbers result
		$prod_apn_query = "SELECT * FROM ".$this->APNTBL." where APN_PRODUCT = '".$prod_number."'";
		$get_prod_apn_data = $this->_mssql->my_mssql_query($prod_apn_query,$this->con);
		$prod_apn_numbers = array();
		if( $this->_mssql->pdo_num_rows_query($prod_apn_query,$this->con) > 0 ) {
			while( $result = $this->_mssql->mssql_fetch_object_query($get_prod_apn_data) ) {
				// set apn values
				$prod_apn_numbers[]	  = (isset($result->APN_NUMBER)) ? trim($result->APN_NUMBER) : "";
			}
		}
		return $prod_apn_numbers;
	}
	
	 /**
     * Function used to get product's rebate price
	 * @param int rebate_type ,int prod_number ,int prod_number
	 * @return string
	 * @access private
	 * 
	 */
	 private function promo_rebate_price($rebate_type,$prod_number,$outlet_id) {
		 if(!empty($prod_number) && !empty($outlet_id)) {
			
			// get product's rebate price result
			$prod_rebate_query = "select Sum(RDT_Rebate) as rebate_price from RebateDetailTbl join RebateHeaderTbl on RDT_Header_Id = RHT_Id left outer join RebateOutletTbl on ROT_Hdr_Id = RHT_Id where RHT_Type = $rebate_type and GetDate() between RHT_Start_Date and RHT_End_Date and RDT_Product = $prod_number and ((RHT_Zone in (select distinct CODE_KEY_ALP from CodeTbl where CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_NUM = $outlet_id )) or (ROT_Outlet = $outlet_id ))";
			$get_prod_rebate_data = $this->_mssql->my_mssql_query($prod_rebate_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($prod_rebate_query,$this->con) > 0 ) {
				$result = $this->_mssql->mssql_fetch_object_query($get_prod_rebate_data);
			}
		}
		// set rebate price
		$rebate_price = (isset($result->rebate_price) && !empty($result->rebate_price)) ? $result->rebate_price : '';
		return $rebate_price;
	}
	 
	
	/**
     * Function used to get active product details
	 * @param int prod_number
	 * @return array
	 * @access public
	 * 
	 */
	public function update_active_product() {
		
		//get post product params
		$outlet   = $_REQUEST['outlets'];
		$prod_number = $this->_common->test_input($_REQUEST['prod_number']);
		//$outlet = '{"outlet_id":"792","outlet_name":"KANGAROO POINT","normal_price1":"4.95","normal_price2":"0","normal_price3":"0","normal_price4":"0","carton_cost":"32.78","label_qty":"1","min_stock_level":"15","max_stock_level":"0","min_reorder_qty":"12","skip_reorder_yn":"N","fifo_stock_yn":"N"}'; 
		$outlet = json_decode($outlet); // convert outlet json into array
		
		if (empty($outlet) || count($outlet) == 0) {
            $_response['result_code'] = 0;
            $_response['message']   = $this->_common->langText($this->_locale,'txt.stock.error.invalid_outlet');
			return $_response;
        }
		// check product existance 
		$product_exist = $this->check_product_details($prod_number);
		if(!empty($product_exist) && isset($outlet->outlet_id)) {
			// check outlet exist or not
			$prod_outlet_exist_data = $this->check_product_outlet($prod_number,$outlet->outlet_id);
			if(!empty($prod_outlet_exist_data) && count($prod_outlet_exist_data) > 0) {
				
				// update outlet data entry
				$update_sql = "UPDATE ".$this->OUTPTBL." SET OUTP_NORM_PRICE_1 = '".$outlet->normal_price1."',OUTP_NORM_PRICE_2 = '".$outlet->normal_price2."',OUTP_NORM_PRICE_3 = '".$outlet->normal_price3."',OUTP_NORM_PRICE_4 = '".$outlet->normal_price4."',OUTP_CARTON_COST = '".$outlet->carton_cost."',OUTP_LABEL_QTY = '".$outlet->label_qty."',OUTP_MIN_ONHAND = '".$outlet->min_stock_level."',OUTP_MAX_ONHAND = '".$outlet->max_stock_level."',OUTP_MIN_REORDER_QTY = '".$outlet->min_reorder_qty."',OUTP_SKIP_REORDER_YN = '".$outlet->skip_reorder_yn."',OUTP_FIFO_STOCK_YN = '".$outlet->fifo_stock_yn."' ";
				// set flag in case of tablet app updation in price
				if(($outlet->normal_price1 != $prod_outlet_exist_data['normal_price1']) || ($outlet->normal_price2 != $prod_outlet_exist_data['normal_price2']) || ($outlet->normal_price3 != $prod_outlet_exist_data['normal_price3'])) {
					$update_sql .= ",OUTP_CHANGE_LABEL_IND = 'Yes'";
				}
				$update_sql .= " WHERE OUTP_OUTLET = '".$outlet->outlet_id."' and OUTP_PRODUCT = $prod_number";
				$this->_mssql->my_mssql_query($update_sql,$this->con);
				// set success message
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.update_product');
			} else {
				// add product's outlet data entry
				$insert_sql = "INSERT ".$this->OUTPTBL." (OUTP_OUTLET,OUTP_PRODUCT,OUTP_NORM_PRICE_1,OUTP_NORM_PRICE_2,OUTP_NORM_PRICE_3,OUTP_NORM_PRICE_4,OUTP_CARTON_COST,OUTP_LABEL_QTY,OUTP_MIN_ONHAND,OUTP_MAX_ONHAND,OUTP_MIN_REORDER_QTY,OUTP_SKIP_REORDER_YN,OUTP_FIFO_STOCK_YN ) VALUES ('".$outlet->outlet_id."','".$prod_number."','".$outlet->normal_price1."','".$outlet->normal_price2."','".$outlet->normal_price3."','".$outlet->normal_price4."','".$outlet->carton_cost."','".$outlet->label_qty."','".$outlet->min_stock_level."','".$outlet->max_stock_level."','".$outlet->min_reorder_qty."','".$outlet->skip_reorder_yn."', '".$outlet->fifo_stock_yn."')";
				$this->_mssql->my_mssql_query($insert_sql,$this->con); 
				// set success message
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.add_product');
			}
			
			$_response['result_code'] = 1;	
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.add_products');
		}
		return $_response;
	}
	
	/**
     * Function used to check product's existance
	 * @param int prod_number
	 * @return bool
	 * @access private
	 * 
	 */
	private function check_product_details($prod_number) {
		$product_exist = 0;
		if(!empty($prod_number)) {
			// get product result
			$product_query = "SELECT * FROM ".$this->PRODTBL." WHERE PROD_NUMBER = $prod_number";
			$get_product_data = $this->_mssql->my_mssql_query($product_query,$this->con);
			
			if( $this->_mssql->pdo_num_rows_query($product_query,$this->con) > 0 ) {
				$product_exist = 1;
			}
		}
		return $product_exist;
	}
	
	/**
     * Function used to check product's outlet existance
	 * @param int prod_number , int outlet_id
	 * @return bool
	 * @access private
	 * 
	 */
	 private function check_product_outlet($prod_number,$outlet_id) {
		
		$prod_outlet_data = array();
		// get product's outlet result
		$prod_outlet_query = "SELECT OUTP_NORM_PRICE_1,OUTP_NORM_PRICE_2,OUTP_NORM_PRICE_3,OUTP_NORM_PRICE_4 FROM ".$this->OUTPTBL." JOIN ".$this->OUTLTBL." ON OUTL_OUTLET = OUTP_OUTLET where OUTP_OUTLET = $outlet_id AND OUTP_PRODUCT = $prod_number";
		$get_prod_outlet_data = $this->_mssql->my_mssql_query($prod_outlet_query,$this->con);
		if( $this->_mssql->pdo_num_rows_query($prod_outlet_query,$this->con) > 0 ) {
			// fetch result
			$result = $this->_mssql->mssql_fetch_object_query($get_prod_outlet_data);
			// prepare product outlet  values
			$prod_outlet_data['normal_price1'] = (isset($result->OUTP_NORM_PRICE_1)) ? $result->OUTP_NORM_PRICE_1 : 0;
			$prod_outlet_data['normal_price2'] = (isset($result->OUTP_NORM_PRICE_2)) ? $result->OUTP_NORM_PRICE_2 : 0;
			$prod_outlet_data['normal_price3'] = (isset($result->OUTP_NORM_PRICE_3)) ? $result->OUTP_NORM_PRICE_3 : 0;
			$prod_outlet_data['normal_price4'] = (isset($result->OUTP_NORM_PRICE_4)) ? $result->OUTP_NORM_PRICE_4 : 0;
		
		}
		return $prod_outlet_data;
	 }
	 
	/**
     * Function used to fetch purchase history of user
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function purchase_history() {
		
		//get post params
		$user_id = $this->_common->test_input($_REQUEST['user_id']);
		$product_id = $this->_common->test_input($_REQUEST['product_id']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		// set pagination limit and offset values
		$offset = (!empty($offset)) ? $offset : 0;
		$limit = (!empty($limit)) ? $limit : 10;
		$_response['result_code'] = 0;
		if (!empty($user_id) && !empty($product_id)) {
			// checking user email is already exists or not
			$user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id;
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
				// get user row data
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				// get associated zone results from user's zone 
				$user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);	
				if(!empty($user_outlets) && count($user_outlets) > 0) {
					// set total outlet counts
					$outlet_count = count($user_outlets);
					$outlet_ids = '';
					foreach($user_outlets as $key=>$outlets) {
						if($key < ($outlet_count-1)) {
							$outlet_ids .= $outlets['outlet_id'].',';
						} else {
							$outlet_ids .= $outlets['outlet_id'];
						}
					}
					
					// get total count of records
					$purchased_count_query = "SELECT count(ORDL_ORDER_NO) as order_count FROM ORDLTBL LEFT JOIN ORDHTBL ON ORDH_OUTLET = ORDL_OUTLET AND ORDH_ORDER_NO = ORDL_ORDER_NO LEFT JOIN OUTLTBL ON OUTL_OUTLET = ORDL_OUTLET WHERE (ORDL_PRODUCT = $product_id) AND ORDH_OUTLET IN ( $outlet_ids )";
					$purchased_count_data = $this->_mssql->my_mssql_query($purchased_count_query,$this->con);
					if( $this->_mssql->pdo_num_rows_query($purchased_count_query,$this->con) > 0 ) {
						$order_count = $this->_mssql->mssql_fetch_object_query($purchased_count_data);
						// get purchase history data
						$purchase_query = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY ORDH_ORDER_DATE DESC, ORDH_POSTED_DATE DESC) as row FROM ORDLTBL LEFT JOIN ORDHTBL ON ORDH_OUTLET = ORDL_OUTLET AND ORDH_ORDER_NO = ORDL_ORDER_NO LEFT JOIN OUTLTBL ON OUTL_OUTLET = ORDL_OUTLET WHERE (ORDL_PRODUCT = $product_id) AND ORDH_OUTLET IN ( $outlet_ids ) ) a WHERE row > $offset and row <= $limit";
						$purchase_result = $this->_mssql->my_mssql_query($purchase_query,$this->con);
						if( $this->_mssql->pdo_num_rows_query($purchase_query,$this->con) > 0 ) {
							$purchase_history = array();
							while($row = $this->_mssql->mssql_fetch_object_query($purchase_result)) {
								// prepare purchase record values
								$purchase_array['outlet_id']   	= $row->ORDL_OUTLET;
								$purchase_array['outlet_name'] 	= utf8_encode($row->OUTL_DESC);
								$purchase_array['supplier_name']= $row->ORDH_SUPPLIER_NAME;
								$purchase_array['order_number'] = $row->ORDL_ORDER_NO;
								$purchase_array['prod_number'] 	= $row->ORDL_PRODUCT;
								$purchase_array['prod_desc'] 	= utf8_encode($row->ORDL_DESC);
								$purchase_array['carton_cost'] 	= number_format($row->ORDL_CARTON_COST, 2, '.', '');
								$purchase_array['units'] 		= $row->ORDL_UNITS;
								$purchase_array['total_units'] 	= $row->ORDL_TOTAL_UNITS;
								$purchase_array['line_totals']	= number_format($row->ORDL_LINE_TOTAL, 2, '.', '');
								$purchase_array['carton_qty']   = $row->ORDL_CARTON_QTY;
								$purchase_array['final_line_total'] = (!empty($row->ORDL_FINAL_LINE_TOTAL)) ? number_format($row->ORDL_FINAL_LINE_TOTAL, 2, '.', '') : '';
								$purchase_array['final_carton_cost'] = (!empty($row->ORDL_FINAL_CARTON_COST)) ? number_format($row->ORDL_FINAL_CARTON_COST, 2, '.', '') : '';
								$purchase_array['document_status'] = $row->ORDH_DOCUMENT_STATUS;
								$purchase_array['document_type'] = $row->ORDH_DOCUMENT_TYPE;
								$purchase_array['outlet_status'] = $row->OUTL_STATUS;
								$purchase_array['order_date'] = (!empty($row->ORDH_ORDER_DATE)) ? date('d/m/Y',strtotime($row->ORDH_ORDER_DATE)) : '';
								$purchase_history[] = $purchase_array;
							}
							// set success response values
							$_response['result_code'] = 1;
							$_response['total_count'] = (isset($order_count->order_count)) ? $order_count->order_count : 0;
							$_response['result'] = $purchase_history;
						} else {
							$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_purchase_history');
						}
					} else {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_purchase_history');
					}
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.error.suppliernot associate');
				}
			} else {
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
        } else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.required_weekly_report_param');
		}
		return $_response;
	}
	
	/**
	 * Function used to get user's zone outlets
	 * @param string user_zone
	 * @return array
	 * @access private
	 * 
	 */
	 private function get_zone_outlets($user_zone='') {
		 
		// initialize zone outlet array
		$user_outlets = array();
		// get associated zone results from user's zone
		if(!empty($user_zone)) {
			$outlet_query = "SELECT CODE_KEY_NUM as outlet_id ,OUTL_DESC as outlet_name FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '".$user_zone."'";
			$get_outlet_data = $this->_mssql->my_mssql_query($outlet_query,$this->con );
			if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
				while($row = $this->_mssql->mssql_fetch_object_query($get_outlet_data)) {
			
					// prepare outlet values
					$user_outlets[] = $row->outlet_id;
				}
			}
		}
		return $user_outlets;
	}
	
	/**
     * Function used to fetch weekly sales history of user
	 * @param int user_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function weekly_sales_history() {
		
		//get post params
		$user_id = $this->_common->test_input($_REQUEST['user_id']);
		$product_id = $this->_common->test_input($_REQUEST['product_id']);
		$start_date = $this->_common->test_input($_REQUEST['start_date']);
		$end_date = $this->_common->test_input($_REQUEST['end_date']);
		$_response['result_code'] = 0;
		if (!empty($user_id) && !empty($product_id) && !empty($start_date) && !empty($end_date)) {
           // checking user email is already exists or not
			$user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id;
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
				// get user row data
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				// get associated zone results from user's zone
				$user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);	
				if(!empty($user_outlets) && count($user_outlets) > 0) {
					// set total outlet counts
					$outlet_count = count($user_outlets);
					$outlet_ids = '';
					foreach($user_outlets as $key=>$outlets) {
						if($key < ($outlet_count-1)) {
							$outlet_ids .= $outlets['outlet_id'].',';
						} else {
							$outlet_ids .= $outlets['outlet_id'];
						}
					}
					// set start date
					$start_date = date('Y-m-d',strtotime($start_date));
					// set end date
					$end_date = date('Y-m-d',strtotime($end_date));		
					// get purchase history data
				    $purchase_query = "SELECT TRX_WEEK_END, TRX_OUTLET, OUTL_DESC,SUM(TRX_QTY)  AS SUM_QTY,SUM(TRX_COST) AS SUM_COST,SUM(TRX_AMT)  AS SUM_AMT,
					SUM(TRX_AMT) / NULLIF(SUM(TRX_QTY), 0) AS AVG_PRICE,SUM(TRX_AMT - TRX_COST) AS SUM_MARGIN,
					(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as GP_PCNT,
					SUM(TRX_DISCOUNT) as SUM_DISCOUNT,SUM(TRX_PROM_SALES) as SUM_PROM_SALES
					FROM TRXTBL LEFT JOIN OUTLTBL ON OUTL_OUTLET = TRX_OUTLET WHERE TRX_PRODUCT = $product_id AND TRX_TYPE = 'ITEMSALE' AND TRX_DATE BETWEEN '".$start_date."' AND '".$end_date."' AND TRX_QTY <> 0 AND TRX_OUTLET IN ( $outlet_ids ) GROUP BY TRX_WEEK_END, TRX_OUTLET, OUTL_DESC  ORDER BY TRX_WEEK_END DESC, TRX_OUTLET, OUTL_DESC";
					$purchase_result = $this->_mssql->my_mssql_query($purchase_query,$this->con);
					if( $this->_mssql->pdo_num_rows_query($purchase_query,$this->con) > 0 ) {
						$purchase_history = array();
						while($row = $this->_mssql->mssql_fetch_object_query($purchase_result)) {
							// prepare purchase record values
							$purchase_array['week_end_date'] = (!empty($row->TRX_WEEK_END)) ? date('d/m/Y',strtotime($row->TRX_WEEK_END)) : '';
							$purchase_array['outlet_id']   	= $row->TRX_OUTLET;
							$purchase_array['outlet_name'] 	= utf8_encode($row->OUTL_DESC);
							$purchase_array['sum_qty'] = $row->SUM_QTY;
							$purchase_array['sum_cost'] = number_format($row->SUM_COST, 2, '.', '');
							$purchase_array['sum_amount'] 	= number_format($row->SUM_AMT, 2, '.', '');
							$purchase_array['avg_price'] 	= number_format($row->AVG_PRICE, 2, '.', '');
							$purchase_array['sum_margin'] 	= number_format($row->SUM_MARGIN, 2, '.', '');
							$purchase_array['gp_pcnt'] 		= number_format($row->GP_PCNT, 2, '.', '');
							$purchase_array['sum_discount'] 	= number_format($row->SUM_DISCOUNT, 2, '.', '');
							$purchase_array['sum_prom_sales']	= $row->SUM_PROM_SALES;
							$purchase_history[] = $purchase_array;
						}
						// set success response values
						$_response['result_code'] = 1;
						$_response['result'] = $purchase_history;
					} else {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_purchase_history');
					}
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.error.suppliernot_associate');
				}
			} else {
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
			}
        } else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.required_weekly_report_param');
		}
		return $_response;
	 }
	 
	/**
	 * Function used to get all unsynched products listing of outlet
	 * @param int outlet_id, int limit, int offset
	 * @return array
	 * @access public
	 * 
	 */
	public function sync_tills() {
		
		// set default failure response
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_till_sync_records');
		// get post data
		$outlet_id   = $_REQUEST['outlet_id'];
		$limit  = $this->_common->test_input($_REQUEST['limit']);
		$offset = $this->_common->test_input($_REQUEST['offset']);
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
		
		if(!empty($outlet_id)) {
			
			// get total count of outlet's unsynched products
			$product_count_query = "SELECT count(TILL_NUMBER) as total_count FROM  " .$this->TillTbl. " WHERE TILL_STATUS = 'Active' and TILL_OUTLET = $outlet_id";
			$product_count_data = $this->_mssql->my_mssql_query($product_count_query,$this->con);    
         
			if( $this->_mssql->pdo_num_rows_query($product_count_query,$this->con) > 0 ) {
				$product_count = $this->_mssql->mssql_fetch_object_query($product_count_data);
				// get outlet's unsynched products
				$unsync_product_query = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY TILL_NUMBER) as row FROM ".$this->TillTbl." WHERE TILL_OUTLET = $outlet_id AND TILL_STATUS = 'Active') a WHERE row > ".$offset." AND row <= ".$limit;
				$get_unsync_product_data = $this->_mssql->my_mssql_query($unsync_product_query,$this->con);
				$products = array();
				if( $this->_mssql->pdo_num_rows_query($unsync_product_query,$this->con) > 0 ) {
					while( $result = $this->_mssql->mssql_fetch_object_query($get_unsync_product_data) ) {
						
						// set product values
						$array['till_desc'] 	= (isset($result->TILL_DESC)) ? trim($result->TILL_DESC) : "";
						$array['till_number']	= (isset($result->TILL_NUMBER)) ? trim($result->TILL_NUMBER) : "";
						$products[] = $array;
					}
					// set response values
					$_response['result_code'] = 1;
					$_response['total_products'] = (isset($product_count->total_count)) ? $product_count->total_count : 0;
					$_response['result'] 	= $products;
				}
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.outlet_not_valid');
		}
		return $_response;
	}
	
	/**
	 * Function used to sync outlet records
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function sync_till_records() {
		
		// set default failure response
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_till_sync_records');
		// get post data
		$outlet_id = $_REQUEST['outlet_id'];
		$current_timestamp = date('M d Y h:i:s A');
		if(!empty($outlet_id)) {
			// get outlet's unsynched products
			$unsync_product_query = "SELECT TILL_NUMBER,TILL_DESC FROM ".$this->TillTbl." WHERE TILL_OUTLET = $outlet_id AND TILL_STATUS = 'Active'";
			$get_unsync_product_data = $this->_mssql->my_mssql_query($unsync_product_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($unsync_product_query,$this->con) > 0 ) {
				while( $result = $this->_mssql->mssql_fetch_object_query($get_unsync_product_data) ) {
					
					// set till sync product values
					$till_desc	 = (isset($result->TILL_DESC)) ? trim($result->TILL_DESC) : "";
					$till_number = (isset($result->TILL_NUMBER)) ? trim($result->TILL_NUMBER) : "";
					// check till number availability in code tbl
					$till_entry_query  = "SELECT CODE_DESC FROM ".$this->CODETBL." WHERE CODE_KEY_NUM = '$till_number' AND CODE_KEY_TYPE = 'SYNCTILL' AND CODE_KEY_ALP = '0'";
					$till_entry_result = $this->_mssql->my_mssql_query($till_entry_query,$this->con);    
					if ( $this->_mssql->pdo_num_rows_query($till_entry_query,$this->con) > 0 ) {
						// update till entry in code table
						$update_sql = "UPDATE ".$this->CODETBL." SET CODE_ALP_1 = 'SYNC',CODE_NUM_1 = '$outlet_id',CODE_DESC = '$till_desc',CODE_TIMESTAMP = '$current_timestamp' WHERE CODE_KEY_TYPE = 'SYNCTILL' AND CODE_KEY_NUM = '$till_number' AND CODE_KEY_ALP = '0'";
						$this->_mssql->my_mssql_query($update_sql,$this->con);
					} else {
						// insert till entry in code table
						$insert_sql = "INSERT ".$this->CODETBL." (CODE_KEY_TYPE, CODE_KEY_NUM, CODE_KEY_ALP, CODE_ALP_1, CODE_NUM_1, CODE_DESC,CODE_TIMESTAMP) VALUES ('SYNCTILL','$till_number','0','SYNC','$outlet_id','$$till_desc','$current_timestamp')";
						$this->_mssql->my_mssql_query($insert_sql,$this->con);
					}
				}
				// set response values
				$_response['result_code'] = 1;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.records_synched');
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.outlet.error.outlet_not_valid');
		}
		return $_response;
	}
	
	/**
	 * Function used to adjust outlet product values in stock
	 * @param int outlet_id
	 * @return array
	 * @access public
	 * 
	 */
	public function stock_adjustment_record() {

		// set default failure response
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.stock_adjustment');
		// get post data
		$outlet_id = $_REQUEST['outlet_id'];
		$prod_number = $_REQUEST['prod_number'];
		$reason_code = $_REQUEST['reason_code'];
		$user_id = $_REQUEST['user_id'];
		$unit_on_hand    = $_REQUEST['unit_on_hand'];    // OUTP_QTY_ONHAND;
		$min_stock_level = $_REQUEST['min_stock_level']; // OUTP_MIN_ONHAND;
		$max_stock_level = $_REQUEST['max_stock_level']; // OUTP_MAX_ONHAND;
		$min_reorder_qty = $_REQUEST['min_reorder_qty']; // OUTP_MIN_REORDER_QTY;
		$current_timestamp = date('M d Y h:i:s A');
		if(!empty($outlet_id) && !empty($prod_number)) {
			// check till number availability in code tbl
			$prod_query  = "SELECT p.*,op.* FROM ".$this->PRODTBL." p JOIN ".$this->OUTPTBL." op ON op.OUTP_PRODUCT = p.Prod_Number WHERE p.Prod_Number = '".$prod_number."' AND op.OUTP_OUTLET = '".$outlet_id."'";
			$prod_result = $this->_mssql->my_mssql_query($prod_query,$this->con);    
			if( $this->_mssql->pdo_num_rows_query($prod_query,$this->con) > 0 ) {
				$prod_row = $this->_mssql->mssql_fetch_object_query($prod_result);
				// update outlet's product data
				$update_outp_sql = "UPDATE ".$this->OUTPTBL." SET OUTP_MIN_ONHAND = '".$min_stock_level."',OUTP_MAX_ONHAND = '".$max_stock_level."',OUTP_MIN_REORDER_QTY = '".$min_reorder_qty."',OUTP_QTY_ONHAND = '".$unit_on_hand."',OUTP_STATUS = 'Active' WHERE OUTP_OUTLET = '".$outlet_id."' and OUTP_PRODUCT = '".$prod_number."'";
				$this->_mssql->my_mssql_query($update_outp_sql,$this->con);
				// update product status as active
				$update_prod_sql = "UPDATE ".$this->PRODTBL." SET PROD_STATUS = 'Active' WHERE Prod_Number = '".$prod_number."'";
				$this->_mssql->my_mssql_query($update_prod_sql,$this->con);
				// TrxTbl then needs the adjustment record inserted into it
				$manufacturer = (isset($prod_row->PROD_MANUFACTURER) && !empty($prod_row->PROD_MANUFACTURER)) ? $prod_row->PROD_MANUFACTURER : 0;
				$group = (isset($prod_row->PROD_GROUP) && !empty($prod_row->PROD_GROUP)) ? $prod_row->PROD_GROUP : 0;
				$department = (isset($prod_row->PROD_DEPARTMENT) && !empty($prod_row->PROD_DEPARTMENT)) ? $prod_row->PROD_DEPARTMENT : 0;
				$commodity = (isset($prod_row->PROD_COMMODITY) && !empty($prod_row->PROD_COMMODITY)) ? $prod_row->PROD_COMMODITY : 0;
				$category = (isset($prod_row->PROD_CATEGORY) && !empty($prod_row->PROD_CATEGORY)) ? $prod_row->PROD_CATEGORY : 0;
				$cost = (isset($prod_row->OUTP_NORM_PRICE_1) && !empty($prod_row->OUTP_NORM_PRICE_1)) ? $prod_row->OUTP_NORM_PRICE_1 : 0;
				$outp_carton_cost = (isset($prod_row->OUTP_CARTON_COST) && !empty($prod_row->OUTP_CARTON_COST)) ? $prod_row->OUTP_CARTON_COST : 0;
				$week_end = date('Y-m-d', strtotime('next sunday'));
				$day = date('D');
				$carton_qty = (isset($prod_row->PROD_CARTON_QTY) && !empty($prod_row->PROD_CARTON_QTY)) ? $prod_row->PROD_CARTON_QTY : 0;
				$unit_qty = (isset($prod_row->PROD_UNIT_QTY) && !empty($prod_row->PROD_UNIT_QTY)) ? $prod_row->PROD_UNIT_QTY : 0;
				$tender = ($reason_code == 1) ? 'RECOUNT' : 'WASTAGE';
				$supplier = (isset($prod_row->PROD_SUPPLIER) && !empty($prod_row->PROD_SUPPLIER)) ? $prod_row->PROD_SUPPLIER : 0;
				$fifo_stock_yn = (isset($prod_row->OUTP_FIFO_STOCK_YN) && !empty($prod_row->OUTP_FIFO_STOCK_YN)) ? $prod_row->OUTP_FIFO_STOCK_YN : 0;
				$outp_qty_onhand = (isset($prod_row->OUTP_QTY_ONHAND) && !empty($prod_row->OUTP_QTY_ONHAND)) ? $prod_row->OUTP_QTY_ONHAND : 0;
				$new_onhand = $unit_on_hand;
				// set quantity onhand diffrence
				if($outp_qty_onhand > $unit_on_hand) {
					$stock_remaining = $outp_qty_onhand-$unit_on_hand;
					$stock_movement_qty = '-'.$stock_remaining;
				} else {
					$stock_remaining = $unit_on_hand-$outp_qty_onhand;
					$stock_movement_qty = $stock_remaining ;
				}
				
				// set trx cost from unit cost
				$unit_cost= ($outp_carton_cost / $carton_qty);
				$trx_cost = $stock_movement_qty*$unit_cost;
				$trx_cost = number_format($trx_cost,2, '.', '');
				
				// set gst trx cost
				$trx_gst_cost = 0;
				if(isset($prod_row->PROD_TAX_CODE) && $prod_row->PROD_TAX_CODE == 'GST') {
					$cost_gst = $this->my_gst_calc($trx_cost);
					$trx_gst_cost = $stock_movement_qty*$cost_gst;
					$trx_gst_cost = number_format($trx_gst_cost,2, '.', '');
				}
				
				// prepare trx table insert query
				$insert_trx_sql = "INSERT INTO TRXTBL(TRX_DATE,TRX_TYPE,TRX_PRODUCT,TRX_OUTLET,TRX_TILL,TRX_SEQ,TRX_TIMESTAMP,
				TRX_STORE,TRX_SUPPLIER,TRX_MANUFACTURER,TRX_GROUP,TRX_DEPARTMENT,TRX_COMMODITY,TRX_CATEGORY,TRX_SUBRANGE,
				TRX_REFERENCE,TRX_USER,TRX_QTY,TRX_AMT,TRX_AMT_GST,TRX_COST,TRX_COST_GST,TRX_DISCOUNT,TRX_PRICE,TRX_PROM_SELL,
				TRX_PROM_BUY,TRX_WEEK_END,TRX_DAY,TRX_NEW_ONHAND,TRX_MEMBER,TRX_POINTS,TRX_CARTON_QTY,TRX_UNIT_QTY,TRX_PARENT,
				TRX_STOCK_MOVEMENT,TRX_TENDER,TRX_MANUAL_IND,TRX_GL_ACCOUNT,TRX_GL_POSTED_IND,TRX_PROM_SALES,TRX_PROM_SALES_GST,
				TRX_DEBTOR,TRX_FLAGS,TRX_REFERENCE_TYPE,TRX_REFERENCE_NUMBER,TRX_Terms_Rebate_Code,TRX_Terms_Rebate,
				TRX_Promo_Scan_Rebate_Code,TRX_Promo_Scan_Rebate,TRX_Promo_Purchase_Rebate_Code,TRX_Promo_Purchase_Rebate)
				VALUES('$current_timestamp','ADJUSTMENT','$prod_number','$outlet_id',0,0,'$current_timestamp','$outlet_id','$supplier',
				'$manufacturer','$group','$department','$commodity','$category',0,0,'$user_id','$stock_movement_qty',0,0,
				'$trx_cost','$trx_gst_cost',0,0,0,0,'$week_end','$day','$new_onhand',0,0,'$carton_qty','$unit_qty',0,'$stock_movement_qty','$tender','','','No',0,0,0,0,'','','',0,'',0,'',0)";
				$this->_mssql->my_mssql_query($insert_trx_sql,$this->con);
					
				// manage FIFO outlet entries : If the “Unit on Hand” value was changed and the Outlet Product uses FIFO stock control (OutpTbl. OUTP_FIFO_STOCK_YN = ‘Y’) t
				if($unit_on_hand <> $outp_qty_onhand && $fifo_stock_yn == 'Y' && $stock_remaining <> 0) {
					// get all FIFO outlet entries
					$fifo_query = "Select * from ".$this->FIFOTbl." where FIFO_Outlet = '".$outlet_id."' and FIFO_PRODUCT = '".$prod_number."' order by FIFO_DATETIME";
					$fifo_result = $this->_mssql->my_mssql_query($fifo_query,$this->con);    
					if( $this->_mssql->pdo_num_rows_query($fifo_query,$this->con) > 0 ) {
						while($fifo_row = $this->_mssql->mssql_fetch_object_query($fifo_result)) {
							$fifo_qty_bal = (isset($fifo_row->FIFO_QTY_BAL)) ? $fifo_row->FIFO_QTY_BAL : 0;
							$fifo_date_time = (isset($fifo_row->FIFO_DATETIME)) ? $fifo_row->FIFO_DATETIME : '';
							$fifo_qty_sold = (isset($fifo_row->FIFO_QTY_SOLD)) ? $fifo_row->FIFO_QTY_SOLD : 0;
							// set adjustment quantity
							if( $fifo_qty_bal < $stock_remaining){
								$adj_qty = $fifo_qty_bal;
							} else if($fifo_qty_bal >= $stock_remaining) {
								$adj_qty = $stock_remaining;	
							}
							$stock_remaining = $stock_remaining-$adj_qty;
							if( $adj_qty <> 0) {
								$fifo_qty_bal = $fifo_qty_bal-$adj_qty;
								$fifo_qty_sold = $fifo_row->FIFO_QTY_SOLD+$adj_qty;
								// update FIFO data
								$update_fifo_sql = "UPDATE ".$this->FIFOTbl." SET FIFO_QTY_BAL = '".$fifo_qty_bal."',FIFO_QTY_SOLD = '".$fifo_qty_sold."' WHERE FIFO_PRODUCT = '".$prod_number."' AND FIFO_DATETIME = '".$fifo_date_time."'";
								$this->_mssql->my_mssql_query($update_fifo_sql,$this->con);
							}
						}
					}
				}
				
				// delete any zero balance records
				$fifo_remove_sql = "Delete from ".$this->FIFOTbl." where FIFO_Outlet = '".$outlet_id."' and FIFO_PRODUCT = '".$prod_number."' and FIFO_QTY_BAL = 0";
				$this->_mssql->my_mssql_query($fifo_remove_sql,$this->con);
				
				// If vRemaining <> 0 then create a new FIFO record for the remaining
				$rec_count = 0;
				$fifo_reccount_sql  = "Select Count(*) as RecCount from ".$this->FIFOTbl." where FIFO_Outlet = '".$outlet_id."' and FIFO_PRODUCT = '".$prod_number."'";
				$fifo_reccount_result = $this->_mssql->my_mssql_query($fifo_reccount_sql,$this->con);    
				if( $this->_mssql->pdo_num_rows_query($fifo_reccount_sql,$this->con) > 0 ) {
					$fifo_reccount_row = $this->_mssql->mssql_fetch_object_query($fifo_reccount_result);
					$rec_count = $fifo_reccount_row->RecCount;
				}
				$fifo_qty_inv = 0;
				if($rec_count == 0) {
					$fifo_qty_inv = $unit_on_hand + $stock_remaining;
				}
				$fifo_qty_bal = $fifo_qty_inv-$stock_remaining;
				if(!empty($fifo_qty_bal)) {
					// insert fifo entry
					$insert_fifo_sql = "INSERT INTO ".$this->FIFOTbl."(FIFO_PRODUCT, FIFO_OUTLET, FIFO_DATETIME, FIFO_TIMESTAMP, FIFO_UNIT_COST,FIFO_QTY_INV,FIFO_QTY_SOLD,FIFO_QTY_BAL,FIFO_SUPPLIER,FIFO_INVOICE_NO) values('$prod_number','$outlet_id','$current_timestamp','$current_timestamp','$unit_cost','$fifo_qty_inv','$stock_remaining','$fifo_qty_bal','STOCKADJ','OPENING')";
					$this->_mssql->my_mssql_query($insert_fifo_sql,$this->con);
				}
				
				// set response values
				$_response['result_code'] = 1;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.success.inserted_stock_adjustment');
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.product_adjustment');
			}
		}
		return $_response;
	}
	
	/**
	 * Function use for calculate the GST value 
	 * @param float pass_in_amt
	 * @return string
	 * @access private
	 * 
	 */
	private function my_gst_calc($pass_in_amt) {
		// get vmGstPercent 
		$tax_gst_query = "SELECT CODE_NUM_1 as vm_gst_percent FROM CODETBL WHERE CODE_KEY_TYPE = 'TAXCODE' AND CODE_KEY_ALP = 'GST'";
		$vm_gst_result = $this->_mssql->my_mssql_query($tax_gst_query,$this->con);
		$vm_gst_data = $this->_mssql->mssql_fetch_object_query($vm_gst_result);
		$vm_gst_percent = (isset($vm_gst_data->vm_gst_percent) && !empty($vm_gst_data->vm_gst_percent)) ? $vm_gst_data->vm_gst_percent : 0;
		
		$gst_inc_amt_in = $pass_in_amt;
		// calculate GST amount
		$gst_amt = 0;
		if ($gst_inc_amt_in <> 0 && $vm_gst_percent <> 0) {
			
			$rate_factor = 1 + ($vm_gst_percent / 100);
			$ex_gst_amt = $gst_inc_amt_in / $rate_factor;
			$gst_amt = $gst_inc_amt_in - $ex_gst_amt;
			$gst_amt = number_format($gst_amt,2, '.', '');
		}
		return $gst_amt;
	}
	
}
