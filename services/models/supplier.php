<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author : Tosif Qureshi
* Timestamp : May-18 5:20PM
* Copyright : Coyote team
*
*/
class supplier {
public $_response = array();
public $result = array();
private $user_table = 'USERTBL';
private $user_account = 'ava_users';
private $sales_report_queue = 'ava_sales_report_queue';
private $supplier_device = 'ava_supplier_device';
private $app_order_table = 'APP_ORDERS';
private $app_order_items_table = 'APP_ORDER_ITEMS';
private $settings_table = 'ava_settings';
private $app_setting_table = 'ava_app_settings';
private $device_address_log_table = 'ava_device_address_log';
private $supplier_device_tbl = 'ava_supplier_device';

/**
 * @method __construct
 * @see private constructor to protect beign inherited
 * @access private
 * @return void
 */
public function __construct() {
    $this->_db = env::getInst(); // -- Create Database Connection instance
    $this->_common = new commonclass(); // Create instance of commonclass
    $this->_locale = $this->_common->locale();
    $this->_mssql = new mssql(); // Create instance of mssql class
    $this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
    $this->_push = pushnotifications::getInst(); /// -- Create push notifications instance --
   
}

/**
 * Function used to check current active tablet app configuration
 * @param string app_version , string device_type
 * @return array
 * 
 */
function check_tab_app_version() {
	//get post user params
	$avc = (isset($_REQUEST['app_version'])) ? $_REQUEST['app_version'] : "";
	$device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
	if(!empty($avc) && !empty($device_type)) {
		$sql = "SELECT * FROM " . $this->app_setting_table." where device_type = '".$device_type."' and app_type = 2" ;
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
			if(isset($row->mini_app_version) && $avc<$row->mini_app_version) {
				$_response['result_code'] = 2;
				$_response['download_url'] = (isset($row->download_url) && !empty($row->download_url)) ? $row->download_url : '';
				$_response['result'] = $row;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.error.need_to_upgrade_app_version');
				return $_response;
			}
		}
	}
}

/**
 * Function used to check current active app configuration
 * @param string app_version , string os_version , string device_type
 * @return array
 * 
 */
function check_app_version() {
	//get post user params
	$avc = (isset($_REQUEST['app_version'])) ? $_REQUEST['app_version'] : "";
	$os = (isset($_REQUEST['os_version'])) ? $_REQUEST['os_version'] : "";
	$device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
	$return_response = 1;
	if(!empty($avc) && !empty($os) && !empty($device_type)) {
		// query to fetch app configuration
		$sql = "SELECT * FROM " . $this->app_setting_table." where device_type = '".$device_type."' and app_type = 2" ;
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
			
			if(isset($row->mini_app_version) && $avc<$row->mini_app_version) {
				$return_response = 0;
			}
		}
	}
	return $return_response;
}

/**
 * Function used to check unlock password value
 * @param unlock_password
 * @return array
 * 
 */
 public function check_unlock_password() {
	
	//get post user params
	$unlock_password = $_REQUEST['unlock_password'];
	// set default response for failure
	$_response['message']  = $this->_common->langText($this->_locale,'txt.error.invalid_unlock_password');
	$_response['result_code'] = 3;
	if(!empty($unlock_password)) {
		
		// get global unlock password value
		$unlock_pass_query  = $this->_db->my_query("SELECT value FROM ".$this->settings_table." WHERE name = 'unlock_password' and module='tablet'");
		$unlock_pass_result = $this->_db->my_fetch_object($unlock_pass_query);
		$unlock_pass_val = (!empty($unlock_pass_result)) ? md5($unlock_pass_result->value) : 0;
		if(!empty($unlock_pass_val) && $unlock_pass_val == $unlock_password) {
			$_response['message']  = $this->_common->langText($this->_locale,'txt.login.unlock_password_matched');
			$_response['result_code'] = 1;
		}
	}
	// return response data
    return $_response;
}

/**
 * Function used to login from supplier's account
 * @param email , password
 * @return array
 * 
 */
public function login() {
	
    //get post user params
    $user_number= $this->_common->test_input($_REQUEST['user_name']);
    $password 	= $this->_common->test_input($_REQUEST['password']);
    $device_type= $this->_common->test_input($_REQUEST['device_type']);
    $device_id 	= $this->_common->test_input($_REQUEST['device_id']);
    $unlock_password = $this->_common->test_input($_REQUEST['unlock_password']);        
    $imei_number = $this->_common->test_input($_REQUEST['imei_number']);        
    $mac_number  = $this->_common->test_input($_REQUEST['mac_number']);        
	$_response['result_code'] = 0;
    if (empty($user_number) || empty($password)) {
        $_response['message'] = $this->_common->langText($this->_locale,'txt.login.error.required');
    } else {
		
		if(!empty($imei_number) || !empty($mac_number))  {
			$imei_number = trim($imei_number);
			$mac_number = trim($mac_number);
			// get valid device address data
			if(empty($imei_number)) {
				$device_address_query  = $this->_db->my_query("SELECT id FROM ".$this->device_address_log_table." WHERE status = 1 AND mac_number = '$mac_number' ");
			} else if(empty($mac_number)) {
				$device_address_query  = $this->_db->my_query("SELECT id FROM ".$this->device_address_log_table." WHERE status = 1 AND imei_number = '$imei_number' ");
			} else {
				$device_address_query  = $this->_db->my_query("SELECT id FROM ".$this->device_address_log_table." WHERE status = 1 AND (imei_number = '$imei_number' OR mac_number = '$mac_number') ");
			}
			$device_address_result = $this->_db->my_fetch_object($device_address_query);
			if(!empty($device_address_result)) {
				// no action here
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.error.supplier.invalid_device_address');
				$_response['result_code'] = 0;
				return $_response;
			}
		}
		
		// checking user email is already exists or not
		$user_query = "select * from ".$this->user_table." where USER_NUMBER = '$user_number'";
		$get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
		if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
			// get user row data
			$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
			
			if( isset($user_row->USER_STATUS) && $user_row->USER_STATUS ==  'Active' ) {
				if( isset($user_row->USER_PASSWORD) && !empty($user_row->USER_PASSWORD) && $user_row->USER_PASSWORD == $password ) {
					// set user data values
					$result = $this->set_user_data($user_row);
					// set unique session token
					$session_token 	= uniqid();
					$result['session_token'] = $session_token;
					// set app logout time
					$result['session_timeout'] = 5;
					// append user result array in response
					$_response['profile_data'] = $result;
					$_response['message']  = $this->_common->langText($this->_locale,'txt.login.success');
					$_response['result_code'] = 1;
					
					/* Use to save or update device id with respect to supplier login */
					$supplierId = $result['user_id'];
					if(!empty($device_type)){
						$this->_deviceid_save_update($supplierId,$device_type,$device_id,$session_token);
					}
					 
				} else {
					$_response['message'] = $this->_common->langText($this->_locale,'txt.login.error.pswdnotexist');
				}
			} else {
				$_response['message'] = $this->_common->langText($this->_locale,'txt.login.error.active_0');
			}
		} else {
			$_response['message'] = $this->_common->langText($this->_locale,'txt.login.error.usrnotexist');
		}
    }
    // return response data
    return $_response;
} //end login()

 /**
 * Function used to Save or update device Id with respect to user login
 * @param int $supplierId
 * @param string $device_type
 * @param int $device_id;
 * @return array
 */
private function _deviceid_save_update($supplierId = '',$device_type = '',$device_id = '',$session_token='') {
    if(!empty($supplierId) && !empty($device_type)){
        // get report request results
        $report_query = "select * from ava_supplier_device where supplier_id = '$supplierId' AND is_deleted = 0 order by sp_id ASC LIMIT 1";
        $get_report_data = $this->_db->my_query($report_query);
        if ($this->_db->my_num_rows($get_report_data) > 0) {
            $deviceDetail = $this->_db->my_fetch_array($get_report_data);            
			$updateArray = array("device_type" =>$device_type,"device_id" =>$device_id,"session_token"=>$session_token);
			$whereClause = array("supplier_id" => $supplierId);
			$update = $this->_db->UpdateAll($this->supplier_device, $updateArray, $whereClause, "");
            
        }else{
                $insert_sql = "INSERT INTO ".$this->supplier_device." (supplier_id,device_type,device_id,session_token) VALUES ('".$supplierId."','".$device_type."','".$device_id."','".$session_token."')";
                $this->_db->my_query($insert_sql);
                $supplier_device_id = mysql_insert_id();
        }       
    }
}

 /**
 * Function used to get user's sales values
 * @param int user_id
 * @return array
 * 
 */
 public function sales_report() {
	
    //get post user params
    $user_id  = $this->_common->test_input($_REQUEST['user_id']);
    $is_error = 0; // default return code
    if (empty($user_id)) {
        $_response['result_code'] = 0;
        $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
    } else {
        // checking user email is already exists or not
        $user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id; 
        $get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
        if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
            // get user row data
            $user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
            // get associated zone results from user's zone 
            $user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
            // get commodity records
            $commodities = $this->get_commodities();
            // get zone records
            $zones = (isset($user_row->USER_OUTLET) && !empty($user_row->USER_OUTLET)) ? array() : $this->get_zones($user_row->USER_ZONE);
            // get department records
            $departments = $this->get_departments();
            // get suppliers records
            $suppliers = $this->get_suppliers();
            
            // append user result array in response
            $_response['outlets'] 	 = $user_outlets;
            $_response['zones'] 	 = $zones;
            $_response['departments']= $departments;
            $_response['commodities']= $commodities;
            $_response['suppliers']  = $suppliers;
            $is_error = 1;
            
        } else {
            $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
        }
    }
    // set response code
    $_response['result_code'] = $is_error;
    // return response data
    return $_response;
 }

/**
 * Function used to prepare user data
 * @param user_data
 * @return array
 * @access private
 * 
 */
 private function set_user_data($user_data='') {
    $result = array();
    // set user data in result
    if(!empty($user_data)) {
        $result['user_id'] 		= (isset($user_data->USER_NUMBER)) ? trim($user_data->USER_NUMBER) : "";
        $result['first_name'] 	= (isset($user_data->USER_FIRST_NAME)) ? trim($user_data->USER_FIRST_NAME) : "";
        $result['surname'] 		= (isset($user_data->USER_SURNAME)) ? trim($user_data->USER_SURNAME) : "";
        $result['password'] 	= (isset($user_data->USER_PASSWORD)) ? trim($user_data->USER_PASSWORD) : "";
        $result['addr_1'] 		= (isset($user_data->USER_ADDR_1)) ? trim($user_data->USER_ADDR_1) : "";
        $result['addr_2'] 		= (isset($user_data->USER_ADDR_2)) ? trim($user_data->USER_ADDR_2) : "";
        $result['addr_3'] 		= (isset($user_data->USER_ADDR_3)) ? trim($user_data->USER_ADDR_3) : "";
        $result['post_code'] 	= (isset($user_data->USER_POST_CODE)) ? trim($user_data->USER_POST_CODE) : "";
        $result['phone'] 		= (isset($user_data->USER_PHONE)) ? trim($user_data->USER_PHONE) : "";
        $result['mobile'] 		= (isset($user_data->USER_MOBILE)) ? trim($user_data->USER_MOBILE) : "";
        $result['email'] 		= (isset($user_data->USER_EMAIL)) ? trim($user_data->USER_EMAIL) : "";
        $result['gender'] 		= (isset($user_data->USER_GENDER)) ? trim($user_data->USER_GENDER) : "";
        $result['date_of_birth']= (isset($user_data->USER_DATE_OF_BIRTH)) ? trim($user_data->USER_DATE_OF_BIRTH) : "";
        $result['storegroup'] 	= (isset($user_data->USER_STOREGROUP)) ? trim($user_data->USER_STOREGROUP) : "";
        $result['store'] 		= (isset($user_data->USER_STORE)) ? trim($user_data->USER_STORE) : "";
        $result['outlet'] 		= (isset($user_data->USER_OUTLET)) ? trim($user_data->USER_OUTLET) : "";
        $result['zone'] 		= (isset($user_data->USER_ZONE)) ? trim($user_data->USER_ZONE) : "";
    }
    // return user data 
    return $result; 
}

/**
 * Function used to get user's zone outlets
 * @param string user_zone , string outlet_id 
 * @return array
 * @access private
 * 
 */
 private function get_user_outlets($user_zone='',$outlet_id=0) {
     
    // initialize zone outlet array
    $user_outlets = array();
    $outlet_id = trim($outlet_id);
    $user_zone = trim($user_zone);
    // get associated zone results from user's zone
	if(!empty($outlet_id)) {
        $outlet_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_OUTLET in ('".$outlet_id."') ORDER BY OUTL_DESC ASC";
    } else if(!empty($user_zone)) {
        $outlet_query = "SELECT CODE_KEY_NUM as outlet_id ,OUTL_DESC as outlet_name FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '".$user_zone."'";
    } else {
        $outlet_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_STATUS = 'Active' and SubString(OUTL_DESC, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY OUTL_DESC ASC";
    }
   
    $get_outlet_data = $this->_mssql->my_mssql_query($outlet_query,$this->con );
    if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
        while($row = $this->_mssql->mssql_fetch_object_query($get_outlet_data)) {
    
            // prepare outlet values
            $outlet_array['outlet_id'] = $row->outlet_id;
            $outlet_array['outlet_name'] = $row->outlet_name.' '.$row->outlet_id;
            $user_outlets[] = $outlet_array;
        }
    }
    return $user_outlets;
}

/**
 * Function used to get all commodities
 * @param null
 * @return array
 * @access private
 * 
 */
 private function get_commodities() {
    // initialize commodit array
    $commodities = array();
    // get commodity results
    $commodity_query = "SELECT CODE_KEY_NUM as commodity_id ,CODE_DESC as commodity_name FROM CodeTbl WHERE CODE_KEY_TYPE = 'COMMODITY' ORDER BY CODE_DESC";
    $get_commodity_data = $this->_mssql->my_mssql_query($commodity_query,$this->con );
    if( $this->_mssql->pdo_num_rows_query($commodity_query,$this->con) > 0 ) {
        while($row = $this->_mssql->mssql_fetch_object_query($get_commodity_data)) {
    
            // prepare commodity values
            $commodity_array['commodity_id'] = $row->commodity_id;
            $commodity_array['commodity_name'] = $row->commodity_name.' '.$row->commodity_id;
            $commodities[] = $commodity_array;
        }
    }
    return $commodities;
 }
 
/**
 * Function used to get all zones
 * @param null
 * @return array
 * @access private
 * 
 */
 private function get_zones($zone='') {
    // initialize zones array
    $zones = array();
    $zone = trim($zone);
    // get zone results
    if(!empty($zone)) {
		 $zone_query = "select CODE_KEY_NUM,CODE_KEY_ALP as zone_id,CODE_DESC as zone from CODETBL where CODE_KEY_TYPE = 'ZONE' and CODE_KEY_ALP = '$zone' order by CODE_DESC";
	} else {
		 $zone_query = "select CODE_KEY_NUM,CODE_KEY_ALP as zone_id,CODE_DESC as zone from CODETBL where CODE_KEY_TYPE = 'ZONE' order by CODE_DESC";
	}
    $get_zone_data = $this->_mssql->my_mssql_query($zone_query,$this->con );
    if( $this->_mssql->pdo_num_rows_query($zone_query,$this->con) > 0 ) {
        while($row = $this->_mssql->mssql_fetch_object_query($get_zone_data)) {
    
            // prepare zone values
            $zone_array['zone_id']  = $row->zone_id;
            $zone_array['zone_name']= $row->zone;
            $zones[] = $zone_array;
        }
    }
    return $zones;
 }
 
 /**
 * Function used to get all zones
 * @param null
 * @return array
 * @access private
 * 
 */
 private function get_departments() {
    // initialize department array
    $departments = array();
    // get department results
    $department_query = "select CODE_KEY_NUM,CODE_DESC from CODETBL where CODE_KEY_TYPE = 'DEPARTMENT' order by CODE_DESC";
    $get_department_data = $this->_mssql->my_mssql_query($department_query,$this->con );
    if( $this->_mssql->pdo_num_rows_query($department_query,$this->con) > 0 ) {
        while($row = $this->_mssql->mssql_fetch_object_query($get_department_data)) {
    
            // prepare department values
            $department_array['department_id']  = $row->CODE_KEY_NUM;
            $department_array['department_name']= $row->CODE_DESC.' '.$row->CODE_KEY_NUM;;
            $departments[] = $department_array;
        }
    }
    return $departments;
 }
 
/**
 * Function used to get suppliers
 * @param null
 * @return array
 * @access private
 * 
 */
 private function get_suppliers() {
    // initialize supplier array
    $suppliers = array();
    // get supplier results
    $supplier_query = "select CODE_KEY_NUM,CODE_DESC,CODE_KEY_ALP from CODETBL where CODE_KEY_TYPE = 'SUPPLIER' order by CODE_DESC";
    $get_supplier_data = $this->_mssql->my_mssql_query($supplier_query,$this->con );
    if( $this->_mssql->pdo_num_rows_query($supplier_query,$this->con) > 0 ) {
        while($row = $this->_mssql->mssql_fetch_object_query($get_supplier_data)) {
    
            // prepare supplier values
            $supplier_array['supplier_id']  = $row->CODE_KEY_NUM;
            $supplier_array['supplier']= utf8_encode($row->CODE_KEY_ALP);
            $supplier_array['supplier_name']= utf8_encode($row->CODE_DESC);
            $suppliers[] = $supplier_array;
        }
    }
    return $suppliers;
 }	
    
/**
 * Function used to get sales report of supplier
 * @param supplier_id , start_date , end_date
 * @return array
 * 
 */
public function item_sales_report() {
   
    //get post user params
    $start_date   = $this->_common->test_input($_REQUEST['start_date']);
    $end_date 	  = $this->_common->test_input($_REQUEST['end_date']);
    $outlet_id    = $this->_common->test_input($_REQUEST['outlet_id']);
    $suppliers    = $this->_common->test_input($_REQUEST['suppliers']);
    $department   = $this->_common->test_input($_REQUEST['department']);
    $commodity   = $this->_common->test_input($_REQUEST['commodity']);
    $product_from = $this->_common->test_input($_REQUEST['product_from']);
    $product_to   = $this->_common->test_input($_REQUEST['product_to']);
    $is_summary   = $this->_common->test_input($_REQUEST['is_summary']);
    $sales_type   = $this->_common->test_input($_REQUEST['sales_type']); // 1:department, 2:commodity ,3:category, 4:group, 5:supplier, 6:outlet, 7:no-sales
    $is_error = 0; // default return code
    // check item sales type value
    if(empty($sales_type)) {
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.sales_type_blank');
        return $_response;
    }
    
    if(!empty($start_date) && !empty($end_date)) {
        // set start date
		$start_date = str_ireplace('/','-',$start_date);
        $end_date = str_ireplace('/','-',$end_date);
        
        $start_date = date('Y-m-d',strtotime($start_date));
        // set end date
        $end_date = date('Y-m-d',strtotime($end_date));
        if($sales_type == 7) {
            // get order sales data from financial sales query
            $sales_query = $this->no_sales_report_query($start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary);
        } else {
            // get order sales data
            $sales_query = $this->sales_report_query($sales_type,$start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary);
        }
        //echo $sales_query;die;
    
        $order_sales_data = array();
        $get_sales_data = $this->_mssql->my_mssql_query($sales_query,$this->con);
        if( $this->_mssql->pdo_num_rows_query($sales_query,$this->con) > 0 ) {
            // fetch info from mssql table
            while ($data = $this->_mssql->mssql_fetch_object_query($get_sales_data)) {
                
                // set result values
                $sales_data['sum_code']   		= $data->SUM_CODE;
                //$sales_data['department'] 	= $data->TRX_DEPARTMENT;
                $sales_data['quantity']  		= $data->SUM_QTY;
                $sales_data['sales_amount']    	= $data->SUM_AMT;
                $sales_data['sales_cost'] 		= $data->SUM_COST;
                $sales_data['promo_sales']   	= $data->SUM_PROM_SALES;
                $sales_data['promo_sales_gst']	= $data->SUM_PROM_SALES_GST;
                $sales_data['margin'] 		   	= $data->SUM_MARGIN;
                $sales_data['gp_percent'] 		= $data->GP_PCNT;
                $sales_data['discount']   		= $data->SUM_DISCOUNT;
                $sales_data['prod_number']      = $data->TRX_PRODUCT;
                $sales_data['prod_desc'] 		= utf8_encode($data->PROD_DESC);
                $sales_data['code_desc'] 		= $data->CODE_DESC;
                $order_sales_data[] = $sales_data;
                $is_error = 1;
            }
            
            // append user result array in response
            $_response['sales_result'] = $order_sales_data;
        } else {
            $_response['message']  = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
    } else {
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.date_range');
    }
    
    // set response code
    $_response['result_code'] = $is_error;
    // return response data
    return $_response;
}

/**
 * Function used to set sales fields from item sales type
 * @param sales_type
 * @return array
 * @access private
 * 
 */
private function set_sales_type_fields($sales_type='') {
    //intilaize sales field array
    $sales_fields = array();
    switch($sales_type) {
        case 1:
        $sales_fields['sales_type_field'] = 'TRX_DEPARTMENT';
        $sales_fields['code_key_type'] = 'DEPARTMENT';
        break;
        case 2:
        $sales_fields['sales_type_field'] = 'TRX_COMMODITY';
        $sales_fields['code_key_type'] = 'COMMODITY';
        break;
        case 3:
        $sales_fields['sales_type_field'] = 'TRX_CATEGORY';
        $sales_fields['code_key_type'] = 'CATEGORY';
        break;
        case 4:
        $sales_fields['sales_type_field'] = 'TRX_GROUP';
        $sales_fields['code_key_type'] = 'GROUP';
        break;
        case 5:
        $sales_fields['sales_type_field'] = 'TRX_SUPPLIER';
        $sales_fields['code_key_type'] = 'SUPPLIER';
        break;
        case 6:
        $sales_fields['sales_type_field'] = 'TRX_OUTLET';
        break;
        default:
        $sales_fields = false;
        break;
    }
    return $sales_fields;
}

/**
 * Function used to manage sales report query
 * @param int sales_type, string start_date, string end_date, string outlet_id, string department 
 * @return string
 * @access private
 * 
 */
private function sales_report_query($sales_type='',$start_date='',$end_date='',$outlet_id='',$suppliers='',$department='',$commodity='',$product_from=0,$product_to=0,$is_summary=0,$user_id=0) {
   
    // get my outlets if input outlet is empty
    if(empty($outlet_id) && !empty($user_id)) {
		// checking user email is already exists or not
        $user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id; 
        $get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
        if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
            // get user row data
            $user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
            // get associated zone results from user's zone 
            $user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
			$outlet_ids = array();
			foreach($user_outlets as $outlets) {
				if(!empty($outlets['outlet_id'])) {
					$outlet_ids[] = $outlets['outlet_id'];
				}
			}
			$outlet_id = (count($outlet_ids) > 0) ? implode(',',$outlet_ids) : '';
		}
	}
    
    // set sales fields from type
    $sales_fields = $this->set_sales_type_fields($sales_type);
    // prepare sales report query
    $sales_query = "SELECT ";
    if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
        $sales_query .= $sales_fields['sales_type_field']." AS SUM_CODE,".$sales_fields['sales_type_field'].",";
    }
    
    $sales_query .= "SUM(TRX_QTY) AS SUM_QTY,
    SUM(TRX_AMT) AS SUM_AMT, SUM(TRX_COST) AS SUM_COST,
    SUM(TRX_PROM_SALES) AS SUM_PROM_SALES,
    SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,
    SUM(TRX_AMT - TRX_COST) AS SUM_MARGIN,
    (((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) AS GP_PCNT,
    SUM(TRX_DISCOUNT) AS SUM_DISCOUNT";
    
    // set code descrition field value
    if( is_array($sales_fields) && isset($sales_fields['code_key_type']) ) {
        $sales_query .= ",X.CODE_DESC AS CODE_DESC";
    }  else if( $sales_type == 6 ) {
        $sales_query .= ",OUTL_DESC AS CODE_DESC";
    }
   // set product field name
    if(!empty($is_summary) && isset($sales_fields['sales_type_field']) && $sales_type != 6) {
        $sales_query .= ",X.CODE_DESC AS PROD_DESC,".$sales_fields['sales_type_field']." AS TRX_PRODUCT";
    } else if( !empty($is_summary) && isset($sales_fields['sales_type_field']) && $sales_type == 6 ) {
        $sales_query .= ",OUTL_DESC AS PROD_DESC,".$sales_fields['sales_type_field']." AS TRX_PRODUCT";
    } else {
        $sales_query .= ",TRX_PRODUCT,PROD_DESC";
    }
    
    $sales_query .= " FROM TRXTBL";
    // set code_key_num
    $code_key_num = ($sales_type == 5) ? 'X.CODE_KEY_ALP' : 'X.CODE_KEY_NUM';
  
    // apply left join data as per request sales type
    if( is_array($sales_fields) && isset($sales_fields['code_key_type']) ) {
        $sales_query .= " LEFT JOIN CODETBL X ON X.CODE_KEY_TYPE = '".$sales_fields['code_key_type']."' AND  $code_key_num = ".$sales_fields['sales_type_field'];
    } else if( $sales_type == 6 ) {
        $sales_query .= " LEFT JOIN OUTLTBL ON OUTL_OUTLET = TRX_OUTLET";
    }
    
    if( empty($is_summary) ) {
        $sales_query .= " LEFT JOIN PRODTBL ON PROD_NUMBER = TRX_PRODUCT";
    }
    $sales_query .= " WHERE (TRX_DATE BETWEEN '".$start_date."' AND '".$end_date."')";
   
    // set outlet ids in query string
    if(!empty($outlet_id)) {
        $sales_query = $this->set_sales_conditions($sales_query,$outlet_id,'TRX_OUTLET');
    }
    // set department ids in query string
    if(!empty($department)) {
        $sales_query = $this->set_sales_conditions($sales_query,$department,'TRX_DEPARTMENT');
    }
    // set suppliers in query string
    if(!empty($suppliers)) {
        $sales_query = $this->set_sales_conditions($sales_query,$suppliers,'TRX_SUPPLIER');
    }
    // set commodity in query string
    if(!empty($commodity)) {
        $sales_query = $this->set_sales_conditions($sales_query,$commodity,'TRX_COMMODITY');
    }
    
    $code_desc_field = ($sales_type == 6) ? 'OUTL_DESC' : 'CODE_DESC';
    if(!empty($product_from) && !empty($product_to) && empty($suppliers)) {
        $sales_query .= " AND (TRX_PRODUCT >= $product_from) AND (TRX_PRODUCT <= $product_to)";
    }
    // set group by 
    $sales_query .= " AND (TRX_TYPE = 'ITEMSALE') ";
    // set group by in query
    $sales_query .= $this->group_by_sales($is_summary,$sales_type,$code_desc_field,$sales_fields);        
    // set order by in query
    $sales_query .= $this->order_by_sales($sales_type,$code_desc_field,$sales_fields,$is_summary);
    return $sales_query;
}

/**
 * Function used to manage group by field values
 * @param int sales_type, string start_date, string end_date, string outlet_id, string department 
 * @return string
 * @access private
 * 
 */
private function group_by_sales($is_summary,$sales_type,$code_desc_field,$sales_fields) {
	$sales_query = " GROUP BY ";
	$sales_type_field = (is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) ? ",".$sales_fields['sales_type_field'] : '';
	if(!empty($is_summary)) {
		$sales_query .= "$code_desc_field"."$sales_type_field";
	} else {
		switch($sales_type) {
			case 1:
				$sales_query .= "TRX_DEPARTMENT, PROD_DESC, CODE_DESC, TRX_PRODUCT ";
			break;
			case 2:
			case 3:
			case 4:
			case 5:
				$sales_query .= "CODE_DESC, PROD_DESC $sales_type_field, TRX_PRODUCT ";
			break;
			case 6:
				$sales_query .= "OUTL_DESC, TRX_OUTLET, PROD_DESC, TRX_PRODUCT ";
			break;
			default:
			  $sales_query .= "";
			break;
		}
	}
	
	return $sales_query;
}

/**
 * Function used to manage no sales report query
 * @param string start_date, string end_date, string outlet_id, string department 
 * @return string
 * @access private
 * 
 */
private function no_sales_report_query($start_date='',$end_date='',$outlet_id='',$suppliers='',$department='',$commodity='',$product_from=0,$product_to=0,$is_summary=0,$user_id=0) {
	// get my outlets if input outlet is empty
    if(empty($outlet_id) && !empty($user_id)) {
		// checking user email is already exists or not
        $user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id; 
        $get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
        if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
            // get user row data
            $user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
            // get associated zone results from user's zone 
            $user_outlets = $this->get_user_outlets($user_row->USER_ZONE,$user_row->USER_OUTLET);
			$outlet_ids = array();
			foreach($user_outlets as $outlets) {
				if(!empty($outlets['outlet_id'])) {
					$outlet_ids[] = $outlets['outlet_id'];
				}
			}
			$outlet_id = (count($outlet_ids) > 0) ? implode(',',$outlet_ids) : '';
		}
	}
    // prepare sales report query
    $sales_query = "SELECT
                    OUTP_PRODUCT, 
                    PROD_DEPARTMENT as SUM_CODE, 
                    PROD_DEPARTMENT, 
                    0 as SUM_AMT, 0 as SUM_COST, 
                    0 as SUM_PROM_SALES, 0 as SUM_PROM_SALES_GST,
                    0 as SUM_MARGIN, 0 as GP_PCNT, 0 as SUM_DISCOUNT, 
                    OUTP_PRODUCT AS TRX_PRODUCT, 
                    SUM(OUTP_QTY_ONHAND) as SUM_QTY, 
                    PROD_DESC, 
                    X.CODE_DESC as CODE_DESC 
                    FROM OUTPTBL 
                    JOIN PRODTBL 
                    ON (PROD_NUMBER = OUTP_PRODUCT)";
    // set commodity in query string
    if(!empty($commodity)) {
        $sales_query = $this->set_sales_conditions($sales_query,$commodity,'PROD_COMMODITY');
    }	
    // set department ids in query string
    if(!empty($department)) {
        $sales_query = $this->set_sales_conditions($sales_query,$department,'PROD_DEPARTMENT');
    }
    // set suppliers in query string
    if(!empty($suppliers)) {
        //$sales_query = $this->set_sales_conditions($sales_query,$suppliers,'TRX_SUPPLIER');
    }
    $sales_query .= " LEFT JOIN CODETBL X 
                    ON X.CODE_KEY_TYPE = 'DEPARTMENT' AND X.CODE_KEY_NUM = PROD_DEPARTMENT
                    WHERE OUTP_STATUS = 'Active' ";
    if(!empty($product_from) && !empty($product_to)) {
        $sales_query .= " AND (OUTP_PRODUCT >= $product_from) AND (OUTP_PRODUCT <= $product_to) ";
    }											                
    // set outlet ids in query string
    if(!empty($outlet_id)) {
        $sales_query = $this->set_sales_conditions($sales_query,$outlet_id,'OUTP_OUTLET');
    }	
    $sales_query .= "AND (NOT (EXISTS
                    (SELECT TRX_PRODUCT FROM TRXTBL WHERE TRXTBL.TRX_OUTLET = OUTPTBL.OUTP_OUTLET
                    AND TRXTBL.TRX_PRODUCT = OUTPTBL.OUTP_PRODUCT 
                    AND TRX_DATE BETWEEN '".$start_date."'
                    AND '".$end_date."'
                    AND TRX_TYPE = 'ITEMSALE')))
                    GROUP BY PROD_DEPARTMENT, PROD_DESC, CODE_DESC, OUTP_PRODUCT 
                    ORDER BY PROD_DEPARTMENT, PROD_DESC, CODE_DESC, OUTP_PRODUCT";		
     return $sales_query;				
}

/**
 * Function used to manage query condition values
 * @param string sales_query, string sales_values
 * @return array
 * @access private
 * 
 */
private function set_sales_conditions($sales_query='',$sales_values='',$sales_field='') {
    
    // convert string values to array 
    $values = array_filter(explode(',',$sales_values));
    $i = 0;
    // prepare query string 
    $sales_query .= " AND (";
    foreach($values as $key=>$value) {
        if($i == 0) {
            $sales_query .= "($sales_field = '$value') ";
        } else {
            $sales_query .= " OR ($sales_field = '$value')";
        }
        $i++;
    }
    $sales_query .= ") ";
    return $sales_query;
}

public function logout() {
    if($this->_common->test_input($_REQUEST['customer_id']) != '') {
        $customerId 	= $this->_common->test_input($_REQUEST['customer_id']);
        $updateArray = array("session_token" => " ");
        $whereClause = array("id" => $customerId);
        $update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
        
        $_response['result_code'] = 1;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.logout.success');
    } else {
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.session');
    }
    return $_response;
} //logout

 /**
 /* Amit Neema 08-06-2016 
 * Function used to get sales report of supplier
 * @param supplier_id , start_date , end_date
 * @return array
 * 
 */
public function item_sales_report_pdf() {
    
    // on the beginning of your script save original memory limit
    //echo $original_mem = ini_get('memory_limit');
    ini_set('memory_limit','640M');
    //ini_set('max_execution_time', 3000);
    
    //get post user params
    $start_date   = $this->_common->test_input($_REQUEST['start_date']);
    $end_date 	  = $this->_common->test_input($_REQUEST['end_date']);
    $start_date = str_ireplace('/','-',$start_date);
    $end_date = str_ireplace('/','-',$end_date);
        
    $outlet_id    = $this->_common->test_input($_REQUEST['outlet_id']);
    $suppliers    = $this->_common->test_input($_REQUEST['suppliers']);
    $department   = $this->_common->test_input($_REQUEST['department']);
    $commodity   = $this->_common->test_input($_REQUEST['commodity']);
    $product_from = $this->_common->test_input($_REQUEST['product_from']);
    $product_to   = $this->_common->test_input($_REQUEST['product_to']);
    $is_summary   = $this->_common->test_input($_REQUEST['is_summary']);
    $sales_type   = $this->_common->test_input($_REQUEST['sales_type']); // 1:department, 2:commodity ,3:category, 4:group, 5:supplier, 6:outlet, 7:no-sales
    
    $sales_typeString = array(
    '1'=>'Item Sales by Department',
    '2'=>'Item Sales by Commodity',
    '3'=>'Item Sales by Category',
    '4'=>'Item Sales by Group',
    '5'=>'Item Sales by Supplier',
    '6'=>'Item Sales by Outlet',
    '7'=>'Items With No Sales',
    );
    
    
    $is_error = 0; // default return code
    // check item sales type value
    if(empty($sales_type)) {
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.sales_type_blank');
        return $_response;
    }
    
    if(!empty($start_date) && !empty($end_date)) {
        // set start date
        $start_date = date('Y-m-d',strtotime($start_date));
        // set end date
        $end_date = date('Y-m-d',strtotime($end_date));
        // get order sales data
        $sales_query = $this->sales_report_query($sales_type,$start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary);
        //echo $sales_query;die;
    
        header('Content-type: text/html'); // To rendering HTML View
        
        $html = '<!DOCTYPE html>
        <html><body><table border="0" cellpadding="2" cellspacing="0" >';

        $html .='<tr>
        <td colspan="9" align="center" style="font-size:8px;font-weight:bold;">
        '.$sales_typeString[$sales_type].'
        </td></tr>';
       
        if(!empty($start_date) && !empty($end_date)){
            $html .='<tr>
            <td colspan="9" align="center" style="font-size:8px;font-weight:bold;">
            '.date('d-m-Y',strtotime($start_date)).' to '.date('d-m-Y',strtotime($end_date)).'
            </td>
            </tr>
            ';
        }
        $html .='<tr>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Number</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;width:200px" width="100px">Description</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Promo Sales</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Discount</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Quantity</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Sales Cost</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Margin</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">GP%</td>
        <td style="font-size:8px;font-weight:bold;background-color: #BCE6FF;">Sales Amt</td>
        </tr>
        ';

        /* In html view Array Unique depedency with respect to CODE_DESC Array */
        $code_descA = array();

        $order_sales_data = array();
        $get_sales_data = $this->_mssql->my_mssql_query($sales_query,$this->con);
        
        $countResult =  $this->_mssql->pdo_num_rows_query($sales_query,$this->con);
                    
        if($this->_mssql->pdo_num_rows_query($sales_query,$this->con) > 0 ) {
            // fetch info from mssql table
            $j = 0;
            $PROM_SALES_SUM = 0;
            $DISCOUNT_SUM = 0;
            $QTY_SUM = 0;
            $DISCOUNT_SUM = 0;
            $COST_SUM = 0;
            $MARGIN_SUM = 0;
            $GP_PCNT_SUM = 0;
            $AMT_SUM = 0;
             
            /* OverAll Total FINAL */    
            $PROM_SALES_SUM_FINAL = 0;
            $DISCOUNT_SUM_FINAL = 0;
            $QTY_SUM_FINAL = 0;
            $COST_SUM_FINAL = 0;
            $MARGIN_SUM_FINAL = 0;
            $GP_PCNT_SUM_FINAL = 0;
            $AMT_SUM_FINAL = 0;
            
            while ($data = $this->_mssql->mssql_fetch_object_query($get_sales_data)) {
                $j++;

                if(!in_array($data->CODE_DESC,$code_descA)){
                    $code_descA[] = $data->CODE_DESC;
                    if($j > 1){ // Total count with respect to CODE_DESC
                        
						$html .= '<tr>
								 <td style="font-size:8px;font-weight:bold;"></td>
								 <td style="font-size:8px;font-weight:bold;color:#7F0000"></td>
								 <td style="font-size:8px;font-weight:bold;">'.$PROM_SALES_SUM.'</td>';
						if($DISCOUNT_SUM < 0){ // Negative Condition
							//$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM);
							//$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
							$DISCOUNT_SUM1 = $DISCOUNT_SUM;
							$html .='<td style="font-size:8px;font-weight:bold;">'.$DISCOUNT_SUM1.'</td>';
						}else{
							$html .='<td style="font-size:8px;font-weight:bold;">'.$DISCOUNT_SUM.'</td>';
						}        
						
						$html .='<td style="font-size:8px;font-weight:bold;">'.$QTY_SUM.'</td>
								 <td style="font-size:8px;font-weight:bold;">'.$COST_SUM.'</td>
								 <td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($MARGIN_SUM,2).'</td>
								 <td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($GP_PCNT_SUM,1).'</td>
								 <td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($AMT_SUM,2).'</td>
								</tr>
								';
                        $PROM_SALES_SUM = 0;
                        $DISCOUNT_SUM = 0;
                        $QTY_SUM = 0;
                        $COST_SUM = 0;
                        $MARGIN_SUM = 0;
                        $GP_PCNT_SUM = 0;
                        $AMT_SUM = 0;
                    }
                    $html .= '<tr>
                            <td colspan="9" align="left" style="font-size:8px;font-weight:bold;color:#7F0000; background-color: #F3F3F3;" >'.$data->CODE_DESC.'</td>
                            </tr>
                            ';
                    }                       
                
                $html .= '<tr>
                        <td style="font-size:8px;">'.$data->TRX_PRODUCT.'</td>
                        <td style="font-size:8px">'.utf8_encode($data->PROD_DESC).'</td>
                        <td style="font-size:8px">'.$data->SUM_PROM_SALES.'</td>';
				if($data->SUM_DISCOUNT < 0){ // Negative Condition
					//$DISCOUNT_SUM1 = str_ireplace('-','',$data->SUM_DISCOUNT);
					//$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
					$DISCOUNT_SUM1 = $data->SUM_DISCOUNT;
					$html .='<td style="font-size:8px;">'.$DISCOUNT_SUM1.'</td>';
				}else{
					$html .='<td style="font-size:8px;">'.$data->SUM_DISCOUNT.'</td>';
				}  
                
                $html .='<td style="font-size:8px">'.$data->SUM_QTY.'</td>
                        <td style="font-size:8px;">'.$data->SUM_COST.'</td>
                        <td style="font-size:8px;">'.$this->number_format_result($data->SUM_MARGIN,2).'</td>
                        <td style="font-size:8px;">'.$this->number_format_result($data->GP_PCNT,1).'</td>
                        <td style="font-size:8px;">'.$this->number_format_result($data->SUM_AMT,2).'</td>
						</tr>';
                
            /* Group Total */
            $PROM_SALES_SUM += $data->SUM_PROM_SALES;
            $DISCOUNT_SUM += $data->SUM_DISCOUNT;
            $QTY_SUM += $data->SUM_QTY;
            $COST_SUM += $data->SUM_COST;
            $MARGIN_SUM += $data->SUM_MARGIN;
            $GP_PCNT_SUM += $data->GP_PCNT;
            $AMT_SUM += $data->SUM_AMT;
            
            /* OverAll Total */     
            $PROM_SALES_SUM_FINAL += $data->SUM_PROM_SALES;
            $DISCOUNT_SUM_FINAL += $data->SUM_DISCOUNT;
            $QTY_SUM_FINAL += $data->SUM_QTY;
            $COST_SUM_FINAL += $data->SUM_COST;
            $MARGIN_SUM_FINAL += $data->SUM_MARGIN;
            $GP_PCNT_SUM_FINAL += $data->GP_PCNT;
            $AMT_SUM_FINAL += $data->SUM_AMT;
            
            $GP_PER_AVG_LOOP = ((($AMT_SUM - $COST_SUM) / $AMT_SUM)*100);
            
			 /* For last Count */
			 if($j == $countResult){
				$html .= '<tr>
					<td style="font-size:8px;font-weight:bold;"></td>
					<td style="font-size:8px;font-weight:bold;color:#7F0000"></td>
					<td style="font-size:8px;font-weight:bold;">'.$PROM_SALES_SUM.'</td>';
				if($DISCOUNT_SUM < 0){ // Negative Condition
					//$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM);
					//$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
					$DISCOUNT_SUM1 = $DISCOUNT_SUM;
					$html .='<td style="font-size:8px;font-weight:bold;">'.$DISCOUNT_SUM1.'</td>';
				}else{
					$html .='<td style="font-size:8px;font-weight:bold;">'.$DISCOUNT_SUM.'</td>';
				}                                    
				$html .='<td style="font-size:8px;font-weight:bold;">'.$QTY_SUM.'</td>
						<td style="font-size:8px;font-weight:bold;">'.$COST_SUM.'</td>
						<td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($MARGIN_SUM,2).'</td>
						<td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($GP_PER_AVG_LOOP,1).'</td>
						<td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($AMT_SUM,2).'</td>
					</tr>';
			}
               
			// set result values
			$sales_data['sum_code']   		= $data->SUM_CODE;
			//$sales_data['department'] 	= $data->TRX_DEPARTMENT;
			$sales_data['quantity']  		= $data->SUM_QTY;
			$sales_data['sales_amount']    	= $data->SUM_AMT;
			$sales_data['sales_cost'] 		= $data->SUM_COST;
			$sales_data['promo_sales']   	= $data->SUM_PROM_SALES;
			$sales_data['promo_sales_gst']	= $data->SUM_PROM_SALES_GST;
			$sales_data['margin'] 		   	= $data->SUM_MARGIN;
			$sales_data['gp_percent'] 		= $data->GP_PCNT;
			$sales_data['discount']   		= $data->SUM_DISCOUNT;
			$sales_data['prod_number']      = $data->TRX_PRODUCT;
			$sales_data['prod_desc'] 		= utf8_encode($data->PROD_DESC);
			$sales_data['code_desc'] 		= $data->CODE_DESC;
			$order_sales_data[] = $sales_data;
			$is_error = 1;
		}
            
		/* Over ALL Sum OF Report - Start */        
		$GP_PER_AVG = ((($AMT_SUM_FINAL - $COST_SUM_FINAL) / $AMT_SUM_FINAL)*100);
                
		$html .= '<tr>
				<td style="font-size:8px;font-weight:bold;"></td>
				<td style="font-size:8px;font-weight:bold;">Report Total</td>
				<td style="font-size:8px;font-weight:bold;">'.$PROM_SALES_SUM_FINAL.'</td>';
		if($DISCOUNT_SUM_FINAL < 0){ // Negative Condition
			//$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM_FINAL);
			//$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1_FINAL.')';
			$DISCOUNT_SUM1 = $DISCOUNT_SUM_FINAL;
			$html .='<td style="font-size:8px;font-weight:bold;">'.$DISCOUNT_SUM1_FINAL.'</td>';
		}else{
			$html .='<td style="font-size:8px;font-weight:bold;">'.$DISCOUNT_SUM_FINAL.'</td>';
		}                                    
		$html .='<td style="font-size:8px;font-weight:bold;">'.$QTY_SUM_FINAL.'</td>
				<td style="font-size:8px;font-weight:bold;">'.$COST_SUM_FINAL.'</td>
				<td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($MARGIN_SUM_FINAL,2).'</td>
				<td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($GP_PER_AVG,1).'</td>
				<td style="font-size:8px;font-weight:bold;">'.$this->number_format_result($AMT_SUM_FINAL,2).'</td>
				</tr>';  
				
		$html .= '</table></body></html>';
		
		/* html to pdf conversion */
		ob_start();
		require_once('core/tcpdf/examples/tcpdf_include.php');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Coyote');
		// set auto page breaks
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// Add a page
		// This method has several options, check the source code documentation for more information.
		//$pdf->AddPage();
		
		// protrait
		//$pdf->AddPage( 'P', 'LETTER' );
		// landscape
		$pdf->AddPage( 'L', 'LETTER' );
		
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		

		// Set some content to print
		/* 
		$html = <<<EOD
		<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
		EOD;
		*/
		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output('test.pdf', 'I');
		die;
            
		// append user result array in response
		$_response['sales_result'] = $order_sales_data;
		} else {
			$_response['message']  = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
		}
    } else {
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.date_range');
    }
    
    // set response code
    $_response['result_code'] = $is_error;
    // return response data
    return $_response;
}



 /**
 /* Amit Neema 08-06-2016 
 * Function used to get sales report of supplier
 * @param supplier_id , start_date , end_date
 * @return array
 * 
 */
public function item_sales_report_html() {
    
    $headerHtml = '<table>';
    $is_mail      = (!empty($this->_common->test_input($_REQUEST['is_mail'])))?$this->_common->test_input($_REQUEST['is_mail']):'0';
    if($is_mail == 1){
        $font_size = 'font-size:8px;';
    }else{
        $font_size = 'font-size:12px;';
    }

    //echo $original_mem = ini_get('memory_limit');
    //ini_set('memory_limit','640M');
    
    $product_from = $this->_common->test_input($_REQUEST['product_from']);
    $product_to   = $this->_common->test_input($_REQUEST['product_to']);
	$user_id   = $this->_common->test_input($_REQUEST['user_id']);
    
    if(!empty($product_from) && !empty($product_to)){
        $headerHtml .='<tr>
        <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;float:left">
        Product Range
        </td>
        </tr>
        <tr>
        <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;float:left">
          '.$product_from.' to '.$product_to.'
        </td>
        </tr>
        ';
    }
    
    /* NewParamter Use */
    $selectedzones = '';
    $outletArray = array();    
   //get post user params
    $start_date   = $this->_common->test_input($_REQUEST['start_date']);
    $end_date 	  = $this->_common->test_input($_REQUEST['end_date']);
    
    $start_date = str_ireplace('/','-',$start_date);
    $end_date = str_ireplace('/','-',$end_date);
        
    $outlet_idA    = $_REQUEST['outlet_id'];
    $outlet_idA    = json_decode($outlet_idA); 
    
    /*
    $outletString = '';
    if(count($outlet_idA) > 1){
        $outletString = '(Selected Outlets)';
    }else if(count($outlet_idA) == 1){
        $outletString = ''.$outlet_idA[0]->outlet_name.'';
    }else{
        $outletString = '(All Outlets)';
    }
    */
    
    /* Find Outlets Array */
    if(!empty($outlet_idA) && $outlet_idA != NULL){
        $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Outlets</td></tr>';
        foreach($outlet_idA as $keyO => $valO){
            if(!empty($valO->outlet_id)){
               $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valO->outlet_name.'</td></tr>';
               $outletArray[] = $valO->outlet_id;
            }
        }
    }
    $commodityA    = $_REQUEST['commodity'];
    $commodityA    = json_decode($commodityA);
    /* Find Commodity Array */
    $commodity = '';
    if(!empty($commodityA) && $commodityA != NULL){
       $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Commodities</td></tr>';
        foreach($commodityA as $keyC => $valC){
            if(!empty($valC->commodity_id)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valC->commodity_name.' '.$valC->commodity_id.'</td></tr>';
                $commodity .= $valC->commodity_id.',';
            }
        }
        if(!empty($commodity)){
        $commodity = rtrim($commodity,',');
        }
    }
    $suppliersA    = $_REQUEST['suppliers'];
    $suppliersA    = json_decode($suppliersA);
    /* Find Suppliers Array */
    $suppliers = '';
    if(!empty($suppliersA)){
        $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Suppliers</td></tr>';
        foreach($suppliersA as $keyS => $valS){
            if(!empty($valS->supplier)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valS->supplier_name.' '.$valS->supplier.'</td></tr>';
                $suppliers .= $valS->supplier.',';
            }
        }
        if(!empty($suppliers)){
        $suppliers = rtrim($suppliers,',');
        }
    }
    
    $departmentA   = $_REQUEST['department'];
    $departmentA   = json_decode($departmentA);
    /* Find Department Array */
    $department = '';
    if(!empty($departmentA)){
        $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Departments</td></tr>';
        
        foreach($departmentA as $keyD => $valD){
            if(!empty($valD->department_id)){
                   $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valD->department_name.'</td></tr>';
                $department .= $valD->department_id.',';
            }
        }
        if(!empty($department)){
        $department = rtrim($department,',');
        }
    }
    $zones        = $_REQUEST['zone_id'];
    $zones =  json_decode($zones);
    
    /* Find Outlets With respect to zones */
    if(!empty($zones)){
        $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Zones</td></tr>';
        foreach($zones as $keyZ => $valZ){
            if(!empty($valZ->zone_id)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valZ->zone_name.' '.$valZ->zone_id.'</td></tr>';
                $selectedzones .= "'".$valZ->zone_id."',";
            }
        }
        if(!empty($selectedzones)){   
            $selectedzones = rtrim($selectedzones,',');
            $user_outlets = array();
            // get associated outlets results from user's zone
            $outlet_query = "SELECT CODE_KEY_NUM as outlet_id ,OUTL_DESC as outlet_name FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP IN (".$selectedzones.")";
            $get_outlet_data = $this->_mssql->my_mssql_query($outlet_query,$this->con );
            if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
                while($row = $this->_mssql->mssql_fetch_object_query($get_outlet_data)) {
                // prepare outlet values
                $outletArray[] = $row->outlet_id;
               }
            }
        }
    }
    
    $headerHtml .= '</table>';
    
    
    $outletString = '';
    if(count($outletArray) > 1){
        $outletString = '(Selected Outlets)';
    }else if(count($outletArray) == 1){
        $outletString = ''.$outlet_idA[0]->outlet_name.'';
    }else{
        $outletString = '(All Outlets)';
    }
    
    
    
    $outlet_id = '';
    /* Convert Outlet Array Into "," String */
    if(!empty($outletArray)){
        $outletArray =  array_unique($outletArray);
        foreach($outletArray as $keyO => $valO){
            if(!empty($valO)){
                $outlet_id .= $valO.',';
            }
        }
        $outlet_id = rtrim($outlet_id,',');
    }
    
    
    $is_summary   = $this->_common->test_input($_REQUEST['is_summary']);
    $sales_type   = $this->_common->test_input($_REQUEST['sales_type']); // 1:department, 2:commodity ,3:category, 4:group, 5:supplier, 6:outlet, 7:no-sales
    
    $sales_typeString = array(
    '1'=>'Item Sales by Department',
    '2'=>'Item Sales by Commodity',
    '3'=>'Item Sales by Category',
    '4'=>'Item Sales by Group',
    '5'=>'Item Sales by Supplier',
    '6'=>'Item Sales by Outlet',
    '7'=>'Items With No Sales',
    );
    
    $pdf_name = array(
    '1'=>'Sales_by_Department.pdf',
    '2'=>'Sales_by_Commodity.pdf',
    '3'=>'Sales_by_Category.pdf',
    '4'=>'Sales_by_Group.pdf',
    '5'=>'Sales_by_Supplier.pdf',
    '6'=>'Sales_by_Outlet.pdf',
    '7'=>'No-Sales.pdf',
    );
    
    $is_error = 0; // default return code
    // check item sales type value
    if(empty($sales_type)) {
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.sales_type_blank');
        return $_response;
    }
    
    if(!empty($start_date) && !empty($end_date)) {
        // set date formating
        $start_date = str_ireplace('/','-',$start_date);
        $end_date = str_ireplace('/','-',$end_date);
        
        $start_date = date('Y-m-d',strtotime($start_date));
        $end_date = date('Y-m-d',strtotime($end_date));
		//$html = ' Before S - '.$start_date.' E - '.$end_date;
        //$html .= ' <br>\n Converted  ===== S - '.$start_date.' E - '.$end_date;
        //$filename =  BASEPATH.'/log.txt'; 
		//$data = $_REQUEST;                  
		//file_put_contents($filename, $html);       
        
        if($sales_type == 7) {
            // get order sales data from financial sales query
            $sales_query = $this->no_sales_report_query($start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary,$user_id);
        } else {
            // get order sales data
            $sales_query = $this->sales_report_query($sales_type,$start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary,$user_id);
        }
        //echo $sales_query;die;
    
        //header('Content-type: text/html'); // To rendering HTML View
        
        $html = '<!DOCTYPE html>
        <html><body><table border="0" cellpadding="2" cellspacing="0" width="100%" >
        <tr><td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">AA01SANDBOX</td></tr>';
       if(!empty($outletString)){
            $html .='<tr>
            <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">
            '.$outletString.'
            </td>
            </tr>
            ';
        }

        $html .='<tr>
        <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">
        '.$sales_typeString[$sales_type].'
        </td>
        </tr>
        ';
       
        if(!empty($start_date) && !empty($end_date)){
            $html .='<tr>
            <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">
            '.date('d-m-Y',strtotime($start_date)).' to '.date('d-m-Y',strtotime($end_date)).'
            </td>
            </tr>
            ';
        }
        $html .='<tr>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Number</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;width:300px" width="300px">Description</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Promo Sales</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Discount</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Quantity</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Cost</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Margin</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">GP%</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Amt</td>
        </tr>
        ';

        /* In html view Array Unique depedency with respect to CODE_DESC Array */
        $code_descA = array();

        $order_sales_data = array();
        $get_sales_data = $this->_mssql->my_mssql_query($sales_query,$this->con);
        
        $countResult =  $this->_mssql->pdo_num_rows_query($sales_query,$this->con);
                   
        if($this->_mssql->pdo_num_rows_query($sales_query,$this->con) > 0 ) {
            // fetch info from mssql table
            $j = 0;
            $PROM_SALES_SUM = 0;
            $DISCOUNT_SUM = 0;
            $QTY_SUM = 0;
            $DISCOUNT_SUM = 0;
            $COST_SUM = 0;
            $MARGIN_SUM = 0;
            $GP_PCNT_SUM = 0;
            $AMT_SUM = 0;
            $code_desc_name = '';
            $code_desc_Id = '';
            
                 
            /* OverAll Total FINAL */
                 
            $PROM_SALES_SUM_FINAL = 0;
            $DISCOUNT_SUM_FINAL = 0;
            $QTY_SUM_FINAL = 0;
            $COST_SUM_FINAL = 0;
            $MARGIN_SUM_FINAL = 0;
            $GP_PCNT_SUM_FINAL = 0;
            $AMT_SUM_FINAL = 0;
            $GP_PER_AVG_LOOP_SINGLE = 0;
            
            while ($data = $this->_mssql->mssql_fetch_object_query($get_sales_data)) {
                $j++;
                
              
                    
                if(!in_array($data->CODE_DESC,$code_descA)){
                    
                    $code_desc_name = (!empty($code_desc_name) && isset($code_desc_name))?$code_desc_name:'';
                    $code_desc_Id = (!empty($code_desc_Id) && isset($code_desc_Id))?$code_desc_Id:'';
                
                
                  
                    $GP_PER_AVG_LOOP_SINGLE = ((($AMT_SUM - $COST_SUM) / $AMT_SUM)*100);
            
                    
                    
                    $code_descA[] = $data->CODE_DESC;
                    if($j > 1 && $is_summary != 1){ // Total count with respect to CODE_DESC
                        
                    $html .= '<tr>
                             <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_Id.'</td>
                             <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_name.'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($PROM_SALES_SUM,2).'</td>';
                    if($DISCOUNT_SUM < 0){ // Negative Condition
                        //$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM);
                        //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
                        $DISCOUNT_SUM1 = $DISCOUNT_SUM;
                        $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM1,2).'</td>';
                    }else{
                        $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM,2).'</td>';
                    }        
                    
                    $html .='<td style="'.$font_size.'font-weight:bold;">'.$QTY_SUM.'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($COST_SUM,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($MARGIN_SUM,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($GP_PER_AVG_LOOP_SINGLE,1).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($AMT_SUM,2).'</td>
                            </tr>
                            ';
                        $PROM_SALES_SUM = 0;
                        $DISCOUNT_SUM = 0;
                        $QTY_SUM = 0;
                        $COST_SUM = 0;
                        $MARGIN_SUM = 0;
                        $GP_PCNT_SUM = 0;
                        $AMT_SUM = 0;
                    }
                    if($is_summary != 1){
                        $html .= '<tr>
                            <td colspan="9" align="left" style="'.$font_size.'font-weight:bold;color:#7F0000; background-color: #F3F3F3;" >'.$data->CODE_DESC.'</td>
                            </tr>
                            ';
                    }
                }                       
                
                $code_desc_name = $data->CODE_DESC; // For previous code Name show at the end
                $code_desc_Id = $data->SUM_CODE;// For previous code id show at the end
                
                $html .= '<tr>
                        <td style="'.$font_size.'">'.$data->TRX_PRODUCT.'</td>
                        <td style="'.$font_size.'">'.utf8_encode($data->PROD_DESC).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_PROM_SALES,2).'</td>';
                  if($data->SUM_DISCOUNT < 0){ // Negative Condition
                        //$DISCOUNT_SUM1 = str_ireplace('-','',$data->SUM_DISCOUNT);
                        //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
                        $DISCOUNT_SUM1 = $data->SUM_DISCOUNT;
                        $html .='<td style="'.$font_size.'">'.$this->number_format_result($DISCOUNT_SUM1,2).'</td>';
                    }else{
                        $html .='<td style="'.$font_size.'">'.$this->number_format_result($data->SUM_DISCOUNT,2).'</td>';
                    }  
                
                $html .='<td style="'.$font_size.'">'.$data->SUM_QTY.'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_COST,2).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_MARGIN,2).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->GP_PCNT,1).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_AMT,2).'</td>
                            </tr>
                            ';
                
            /* Group Total */
            $PROM_SALES_SUM += $data->SUM_PROM_SALES;
            $DISCOUNT_SUM += $data->SUM_DISCOUNT;
            $QTY_SUM += $data->SUM_QTY;
            $COST_SUM += $data->SUM_COST;
            $MARGIN_SUM += $data->SUM_MARGIN;
            $GP_PCNT_SUM += $data->GP_PCNT;
            $AMT_SUM += $data->SUM_AMT;
            
            /* OverAll Total */
                 
            $PROM_SALES_SUM_FINAL += $data->SUM_PROM_SALES;
            $DISCOUNT_SUM_FINAL += $data->SUM_DISCOUNT;
            $QTY_SUM_FINAL += $data->SUM_QTY;
            $COST_SUM_FINAL += $data->SUM_COST;
            $MARGIN_SUM_FINAL += $data->SUM_MARGIN;
            $GP_PCNT_SUM_FINAL += $data->GP_PCNT;
            $AMT_SUM_FINAL += $data->SUM_AMT;
            
            
            $GP_PER_AVG_LOOP = ((($AMT_SUM - $COST_SUM) / $AMT_SUM)*100);
            
                 /* For last Count */
                 if($j == $countResult && $is_summary != 1){
                            $html .= '<tr>
                            <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_Id.'</td>
                            <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_name.'</td>
                            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($PROM_SALES_SUM,2).'</td>';
                            if($DISCOUNT_SUM < 0){ // Negative Condition
                                //$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM);
                                //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
                                $DISCOUNT_SUM1 = $DISCOUNT_SUM;
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM1,2).'</td>';
                            }else{
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM,2).'</td>';
                            }                                    
                            $html .='<td style="'.$font_size.'font-weight:bold;">'.$QTY_SUM.'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($COST_SUM,2).'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($MARGIN_SUM,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($GP_PER_AVG_LOOP,1).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($AMT_SUM,2).'</td>
                                 </tr>
                                ';
                    }
                 
                
          
                
                // set result values
                $sales_data['sum_code']   		= $data->SUM_CODE;
                //$sales_data['department'] 	= $data->TRX_DEPARTMENT;
                $sales_data['quantity']  		= $data->SUM_QTY;
                $sales_data['sales_amount']    	= $data->SUM_AMT;
                $sales_data['sales_cost'] 		= $data->SUM_COST;
                $sales_data['promo_sales']   	= $data->SUM_PROM_SALES;
                $sales_data['promo_sales_gst']	= $data->SUM_PROM_SALES_GST;
                $sales_data['margin'] 		   	= $data->SUM_MARGIN;
                $sales_data['gp_percent'] 		= $data->GP_PCNT;
                $sales_data['discount']   		= $data->SUM_DISCOUNT;
                $sales_data['prod_number']      = $data->TRX_PRODUCT;
                $sales_data['prod_desc'] 		= utf8_encode($data->PROD_DESC);
                $sales_data['code_desc'] 		= $data->CODE_DESC;
                $order_sales_data[] = $sales_data;
                $is_error = 1;
            }
            
            
             /* Over ALL Sum OF Report - Start */
                
            $GP_PER_AVG = ((($AMT_SUM_FINAL - $COST_SUM_FINAL) / $AMT_SUM_FINAL)*100);
                
                  $html .= '<tr style="'.$font_size.'font-weight:bold; background-color: #F3F3F3;">
                            <td style="'.$font_size.'font-weight:bold;"></td>
                            <td style="'.$font_size.'font-weight:bold;">Report Total</td>
                            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($PROM_SALES_SUM_FINAL,2).'</td>';
                            if($DISCOUNT_SUM_FINAL < 0){ // Negative Condition
                                //$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM_FINAL);
                                //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1_FINAL.')';
                                $DISCOUNT_SUM1 = $DISCOUNT_SUM_FINAL;
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM1_FINAL,2).'</td>';
                            }else{
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM_FINAL,2).'</td>';
                            }                                    
                            $html .='<td style="'.$font_size.'font-weight:bold;">'.$QTY_SUM_FINAL.'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($COST_SUM_FINAL,2).'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($MARGIN_SUM_FINAL,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($GP_PER_AVG,1).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($AMT_SUM_FINAL,2).'</td>
                                 </tr>
                                ';  
              $html .= '</table></body></html>';
            
             $html =  $html.''.$headerHtml;
            
            
            /* html to pdf conversion */
            if($is_mail == 1){
                /* html to pdf conversion */
                ob_start();
                require_once('core/tcpdf/examples/tcpdf_include.php');
                // create new PDF document
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('Coyote');
                // set auto page breaks
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
                //$pdf->AddPage();
                $pdf->AddPage( 'L', 'LETTER' );
                $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                // Print text using writeHTMLCell()
                
                
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                // ---------------------------------------------------------
                
                $pdf->Output(BASEPATH.'/../pdf/'.$pdf_name[$sales_type], 'F');
        
              die;
            }else{
            
                //header('Content-type: text/html'); // To rendering HTML View
                //echo $html;
                //die;
                //$_response['outlet_id'] = (array) $outletArray;
                $_response['outlet_id'] = array();
                $_response['pdf_name'] = $pdf_name[$sales_type];
                $_response['sales_result'] = $html;
            }
           
        } else {
            $_response['message']  = $this->_common->langText($this->_locale,'txt.error.no_sales_data_found');
        }
    } else {
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.date_range');
    }
    
    // set response code
    $_response['result_code'] = $is_error;
    // return response data
    return $_response;
}


/**
 * Function used to manage order by sales
 * @param string sales_type, string sales_values
 * @return array
 * @access private
 * 
 */
private function order_by_sales($sales_type,$code_desc_field,$sales_fields,$is_summary){
$order_string = " ORDER BY ";
    switch($sales_type) {
        case 1:
            
            if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
                $order_string .= $sales_fields['sales_type_field'].",";
            }
            if(!empty($is_summary)) {
                $order_string .= "$code_desc_field";
            } else {
                $order_string .= "PROD_DESC,$code_desc_field,TRX_PRODUCT";
            }
        break;
        case 2:
       
            if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
                $order_string_type = $sales_fields['sales_type_field'];
            }
            if(!empty($is_summary)) {
                if(!empty($order_string_type)){
                    $order_string .= "$code_desc_field,$order_string_type";
                }else{
                    $order_string .= "$code_desc_field";
                }
                
            } else {
                if(!empty($order_string_type)){
                   $order_string .= "$code_desc_field,PROD_DESC,".$order_string_type.",TRX_PRODUCT";
                }else{
                    $order_string .= "$code_desc_field,PROD_DESC,TRX_PRODUCT";
                }
            }
        break;
        case 3:
        
            if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
                $order_string_type = $sales_fields['sales_type_field'];
            }
            if(!empty($is_summary)) {
                if(!empty($order_string_type)){
                    $order_string .= "$code_desc_field,$order_string_type";
                }else{
                    $order_string .= "$code_desc_field";
                }
                
            } else {
                if(!empty($order_string_type)){
                   $order_string .= "$code_desc_field,PROD_DESC,".$order_string_type.",TRX_PRODUCT";
                }else{
                    $order_string .= "$code_desc_field,PROD_DESC,TRX_PRODUCT";
                }
            }
        break;
        case 4:

            if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
                $order_string_type = $sales_fields['sales_type_field'];
            }
            if(!empty($is_summary)) {
                if(!empty($order_string_type)){
                    $order_string .= "$code_desc_field,$order_string_type";
                }else{
                    $order_string .= "$code_desc_field";
                }
                
            } else {
                if(!empty($order_string_type)){
                   $order_string .= "$code_desc_field,PROD_DESC,".$order_string_type.",TRX_PRODUCT";
                }else{
                    $order_string .= "$code_desc_field,PROD_DESC,TRX_PRODUCT";
                }
            }
        break;
        case 5:
       
		if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
			$order_string_type = $sales_fields['sales_type_field'];
		}
		if(!empty($is_summary)) {
			$order_string .= "$code_desc_field,$order_string_type";
		} else {
			$order_string .= "$code_desc_field,PROD_DESC,$order_string_type,TRX_PRODUCT";
		}
		
        break;
        case 6:
         
            if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
                $order_string_type = $sales_fields['sales_type_field'];
            }
            if(!empty($is_summary)) {
                if(!empty($order_string_type)){
                    $order_string .= "$code_desc_field,$order_string_type";
                }else{
                    $order_string .= "$code_desc_field";
                }
                
            } else {
                if(!empty($order_string_type)){
                   $order_string .= "$code_desc_field,".$order_string_type.",PROD_DESC,TRX_PRODUCT";
                }else{
                    $order_string .= "$code_desc_field,PROD_DESC,TRX_PRODUCT";
                }
            }
        break;
        default:
            
            if( is_array($sales_fields) && isset($sales_fields['sales_type_field']) ) {
                $sales_query .= $sales_fields['sales_type_field'].",";
            }
            if(!empty($is_summary)) {
                $order_string .= "$code_desc_field";
            } else {
                $order_string .= "PROD_DESC,$code_desc_field,TRX_PRODUCT";
            }
        break;
    }
    return $order_string;

}



 /**
 /* Amit Neema 21-06-2016 
 * Function used to get sales report of supplier
 * @param supplier_id , start_date , end_date
 * @return array
 * 
 */
public function item_sales_report_download() {
    
    /* NewParamter Use */
    $selectedzones = '';
    $outletArray = array();    
    $requestA = array();    
    $is_error = 0; // default return code
       //get post user params
        $user_id   = $this->_common->test_input($_REQUEST['user_id']);
        $email_id   = $this->_common->test_input($_REQUEST['email_id']);
        $report_type   = $this->_common->test_input($_REQUEST['report_type']);
        
        $create_date   = CURRENT_TIME;

        $start_date   = $this->_common->test_input($_REQUEST['start_date']);
        $end_date 	  = $this->_common->test_input($_REQUEST['end_date']);
        
        $start_date = str_ireplace('/','-',$start_date);
        $end_date = str_ireplace('/','-',$end_date);
        
        
        
        if($report_type == 'financial'){
            $start_date_strtotime = strtotime($start_date);
            $start_date_seven_strtotime = strtotime("+7 day", $start_date_strtotime);
            $end_date_strtotime 	  = strtotime($end_date);
            if($end_date_strtotime > $start_date_seven_strtotime){
                $_response['result_code'] = 0;
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.seven_day_validation');
                return $_response;
            }                    
        } 
        
        $outlet_idA    = $_REQUEST['outlet_id'];
        $outlet_idA    = json_decode($outlet_idA);
         
        $commodityA    = $_REQUEST['commodity'];
        $commodityA    = json_decode($commodityA);
        
        $suppliersA    = $_REQUEST['suppliers'];
        $suppliersA    = json_decode($suppliersA);
        
        $departmentA   = $_REQUEST['department'];
        $departmentA   = json_decode($departmentA);
        
        $zones        = $_REQUEST['zone_id'];
        $zones        =  json_decode($zones);
        
        $product_from = $this->_common->test_input($_REQUEST['product_from']);
        $product_to   = $this->_common->test_input($_REQUEST['product_to']);
        $is_summary   = $this->_common->test_input($_REQUEST['is_summary']);
        $sales_type   = $this->_common->test_input($_REQUEST['sales_type']); // 1:department, 2:commodity ,3:category, 4:group, 5:supplier, 6:outlet, 7:no-sales
        
        $requestA['start_date'] = $start_date;
        $requestA['end_date'] = $end_date;
        $requestA['outlet_id'] = $outlet_idA;
        $requestA['commodity'] = $commodity;
        $requestA['suppliers'] = $suppliers;
        $requestA['department'] = $department;
        $requestA['zones'] = $zones;
        $requestA['product_from'] = $product_from;
        $requestA['product_to'] = $product_to;
        $requestA['is_summary'] = $is_summary;
        $requestA['sales_type'] = $sales_type;
        $requestParameter = json_encode($requestA);

         $request_id = 0;
         if(!empty($start_date) && !empty($end_date)) {
            if(!empty($user_id) && !empty($email_id)){
                $is_error = 1;
                $insert_sql = "INSERT INTO ".$this->sales_report_queue."(user_id,request_parameter,email_id,status,create_date,report_type) VALUES ('".$user_id."','".$requestParameter."','".$email_id."','0','".$create_date."','".$report_type."')";
                $this->_db->my_query($insert_sql);
                $sales_report_id = mysql_insert_id();
                if($sales_report_id){
                    $_response['message'] = $this->_common->langText($this->_locale,'txt.error.request_saved');
                    $_response['sales_report_id'] = $sales_report_id;
                }else{
                    $_response['message'] = $this->_common->langText($this->_locale,'txt.error.request_not_saved');
                }
            }else{
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.User_invalid');
            }
         }else{
          $_response['message'] = $this->_common->langText($this->_locale,'txt.error.date_range');
         }
        // set response code
        $_response['result_code'] = $is_error;
        // return response data
        return $_response;
    }

 /**
 /* Amit Neema 21-06-2016 
 * Function used to send Sales report on mail Id
 * @return array
 * 
 */
public function item_sales_report_send() {
    $requestA = array();
    // get report request results
    $report_query = "select * from ava_sales_report_queue where status = '0' AND is_deleted = 0 order by id ASC LIMIT 1";
    $get_report_data = $this->_db->my_query($report_query);
    if ($this->_db->my_num_rows($get_report_data) > 0) {
    while($request_report = $this->_db->my_fetch_array($get_report_data)) {
        $report_type = $request_report['report_type'];    
            
        // get user's email in mssql account table
        switch($report_type) {
        case 'sales': 
            $this->sales_report_download_cron($request_report);
        break;
        case 'financial': 
                $this->finacial_report_download_cron($request_report);
            break;	
        }    
      
        }
    }
    return true;
}   

/**
 /* Amit Neema 21-06-2016 
 * Function used to send Sales report on mail Id
 * @return array
 * 
 */
public function sales_report_download_cron($request_report) {
    $sales_report_id = $request_report['id'];
    $user_id = $request_report['user_id'];
    $email_id = $request_report['email_id'];
    $font_size = 'font-size:8px;';
    
    /* NewParamter Use */
    $selectedzones = '';
    $outletArray = array();  
    
    $headerHtml = '<table>';
    
    $request_paramter = $request_report['request_parameter'];
    $request_paramter = json_decode($request_paramter);
      
      
     
    $product_from = $request_paramter->product_from;
    $product_to   = $request_paramter->product_to;  
    
    if(!empty($product_from) && !empty($product_to)){
        $headerHtml .='<tr>
        <td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">
        Product Range
        </td>
        </tr>
        <tr>
        <td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">
          '.$product_from.' to '.$product_to.'
        </td>
        </tr>
        ';
    }
    
      
   //get post user params
    $start_date   = $request_paramter->start_date;
    $end_date 	  = $request_paramter->end_date;
    
    $outlet_idA   = $request_paramter->outlet_id;
      
    
    /* Find Outlets Array */
    if(!empty($outlet_idA) && $outlet_idA != NULL){
        $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Outlets</td></tr>';
        foreach($outlet_idA as $keyO => $valO){
            if(!empty($valO->outlet_id)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valO->outlet_name.' '.$valO->outlet_id.'</td></tr>';
                $outletArray[] = $valO->outlet_id;
            }
        }
    }
    $commodityA    = $request_paramter->commodity;
    /* Find Commodity Array */
    $commodity = '';
    if(!empty($commodityA) && $commodityA != NULL){
             $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Commodities</td></tr>';
        foreach($commodityA as $keyC => $valC){
            if(!empty($valC->commodity_id)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valC->commodity_name.' '.$valC->commodity_id.'</td></tr>';
                $commodity .= $valC->commodity_id.',';
            }
        }
        if(!empty($commodity)){
        $commodity = rtrim($commodity,',');
        }
    }
    $suppliersA    = $request_paramter->suppliers;
    /* Find Suppliers Array */
    $suppliers = '';
    if(!empty($suppliersA)){
         $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Suppliers</td></tr>';
        foreach($suppliersA as $keyS => $valS){
            if(!empty($valS->supplier)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valS->supplier_name.' '.$valS->supplier.'</td></tr>';
                $suppliers .= $valS->supplier.',';
            }
        }
        if(!empty($suppliers)){
        $suppliers = rtrim($suppliers,',');
        }
    }
    
    $departmentA   = $request_paramter->department;
    /* Find Department Array */
    $department = '';
    if(!empty($departmentA)){
        $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Departments</td></tr>';
        foreach($departmentA as $keyD => $valD){
            if(!empty($valD->department_id)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valD->department_name.' '.$valD->department_id.'</td></tr>';
                $department .= $valD->department_id.',';
            }
        }
        if(!empty($department)){
        $department = rtrim($department,',');
        }
    }
    $zones        = $request_paramter->zone_id;
    /* Find Outlets With respect to zones */
    if(!empty($zones)){
          $headerHtml  .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">Zones</td></tr>';
        foreach($zones as $keyZ => $valZ){
            if(!empty($valZ->zone_id)){
                $headerHtml .='<tr><td colspan="9" align="left" style="'.$font_size.'font-weight:bold;float:left">'.$valZ->zone_name.' '.$valZ->zone_id.'</td></tr>';
                $selectedzones .= "'".$valZ->zone_id."',";
            }
        }
        if(!empty($selectedzones)){
            $selectedzones = rtrim($selectedzones,',');
            $user_outlets = array();
            // get associated outlets results from user's zone
            $outlet_query = "SELECT CODE_KEY_NUM as outlet_id ,OUTL_DESC as outlet_name FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP IN (".$selectedzones.")";
            $get_outlet_data = $this->_mssql->my_mssql_query($outlet_query,$this->con );
            if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
                while($row = $this->_mssql->mssql_fetch_object_query($get_outlet_data)) {
                // prepare outlet values
                $outletArray[] = $row->outlet_id;
               }
            }
        }
    }
   
    $outletString = '';
    if(count($outletArray) > 1){
        $outletString = '(Selected Outlets)';
    }else if(count($outletArray) == 1){
        $outletString = ''.$outlet_idA[0]->outlet_name.'';
    }else{
        $outletString = '(All Outlets)';
    }
    
    $outlet_id = '';
    /* Convert Outlet Array Into "," String */
    if(!empty($outletArray)){
        $outletArray =  array_unique($outletArray);
        foreach($outletArray as $keyO => $valO){
            if(!empty($valO)){
                $outlet_id .= $valO.',';
            }
        }
        $outlet_id = rtrim($outlet_id,',');
    }
    
    
    $headerHtml .= '</table>';
   
    $is_summary   = $request_paramter->is_summary;
    $sales_type   = $request_paramter->sales_type; // 1:department, 2:commodity ,3:category, 4:group, 5:supplier, 6:outlet, 7:no-sales
    
    $sales_typeString = array(
    '1'=>'Item Sales by Department',
    '2'=>'Item Sales by Commodity',
    '3'=>'Item Sales by Category',
    '4'=>'Item Sales by Group',
    '5'=>'Item Sales by Supplier',
    '6'=>'Item Sales by Outlet',
    '7'=>'Items With No Sales',
    );
    
    $pdf_name = array(
    '1'=>'Sales_by_Department.pdf',
    '2'=>'Sales_by_Commodity.pdf',
    '3'=>'Sales_by_Category.pdf',
    '4'=>'Sales_by_Group.pdf',
    '5'=>'Sales_by_Supplier.pdf',
    '6'=>'Sales_by_Outlet.pdf',
    '7'=>'No-Sales.pdf',
    );
    
    
    
    $is_error = 0; // default return code
    
    if(!empty($start_date) && !empty($end_date)) {
        // set start date
        
        $start_date = str_ireplace('/','-',$start_date);
        $end_date = str_ireplace('/','-',$end_date);
        
        
        $start_date = date('Y-m-d',strtotime($start_date));
        // set end date
        $end_date = date('Y-m-d',strtotime($end_date));
        // get order sales data
        
        if($sales_type == 7) {
            // get order sales data from financial sales query
            $sales_query = $this->no_sales_report_query($start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary,$user_id);
        } else {
            // get order sales data
            $sales_query = $this->sales_report_query($sales_type,$start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary,$user_id);
        }
        
       // $sales_query = $this->sales_report_query($sales_type,$start_date,$end_date,$outlet_id,$suppliers,$department,$commodity,$product_from,$product_to,$is_summary);
        //echo $sales_query;die;
    
        //header('Content-type: text/html'); // To rendering HTML View
        
        $html = '<!DOCTYPE html>
        <html><body><table border="0" cellpadding="2" cellspacing="0" width="100%" >
        <tr><td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">AA01SANDBOX</td></tr>
        ';
        
       if(!empty($outletString)){
            $html .='<tr>
            <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">
            '.$outletString.'
            </td>
            </tr>
            ';
        }

        $html .='<tr>
        <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">
        '.$sales_typeString[$sales_type].'
        </td>
        </tr>
        ';
       
        if(!empty($start_date) && !empty($end_date)){
            $html .='<tr>
            <td colspan="9" align="center" style="'.$font_size.'font-weight:bold;">
            '.date('d-m-Y',strtotime($start_date)).' to '.date('d-m-Y',strtotime($end_date)).'
            </td>
            </tr>
            ';
        }
        $html .='<tr>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;" width="50px">Number</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;" width="150px">Description</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Promo Sales</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Discount</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Quantity</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Cost</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Margin</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">GP%</td>
        <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Amt</td>
        </tr>
        ';

        /* In html view Array Unique depedency with respect to CODE_DESC Array */
        $code_descA = array();

        $order_sales_data = array();
        $get_sales_data = $this->_mssql->my_mssql_query($sales_query,$this->con);
        
        $countResult =  $this->_mssql->pdo_num_rows_query($sales_query,$this->con);
                    
        if($this->_mssql->pdo_num_rows_query($sales_query,$this->con) > 0 ) {
            // fetch info from mssql table
            $j = 0;
            $PROM_SALES_SUM = 0;
            $DISCOUNT_SUM = 0;
            $QTY_SUM = 0;
            $DISCOUNT_SUM = 0;
            $COST_SUM = 0;
            $MARGIN_SUM = 0;
            $GP_PCNT_SUM = 0;
            $AMT_SUM = 0;
            $code_desc_name = '';
            $code_desc_Id = '';
             
                 
            /* OverAll Total FINAL */
                 
            $PROM_SALES_SUM_FINAL = 0;
            $DISCOUNT_SUM_FINAL = 0;
            $QTY_SUM_FINAL = 0;
            $COST_SUM_FINAL = 0;
            $MARGIN_SUM_FINAL = 0;
            $GP_PCNT_SUM_FINAL = 0;
            $AMT_SUM_FINAL = 0;
            $GP_PER_AVG_LOOP_SINGLE = 0;
            while ($data = $this->_mssql->mssql_fetch_object_query($get_sales_data)) {
                $j++;
                if(!in_array($data->CODE_DESC,$code_descA)){
                    $code_descA[] = $data->CODE_DESC;
                    
                    
                  
                    $GP_PER_AVG_LOOP_SINGLE = ((($AMT_SUM - $COST_SUM) / $AMT_SUM)*100);
            
                    
                    
                    if($j > 1 && $is_summary != 1){ // Total count with respect to CODE_DESC
                        
                    $html .= '<tr>
                            <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_Id.'</td>
                            <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_name.'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($PROM_SALES_SUM,2).'</td>';
                    if($DISCOUNT_SUM < 0){ // Negative Condition
                        //$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM);
						//$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
                        $DISCOUNT_SUM1 = $DISCOUNT_SUM;
                        $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM1,2).'</td>';
                    }else{
                        $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM,2).'</td>';
                    }        
                    
                    $html .='<td style="'.$font_size.'font-weight:bold;">'.$QTY_SUM.'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($COST_SUM,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($MARGIN_SUM,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($GP_PER_AVG_LOOP_SINGLE,1).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($AMT_SUM,2).'</td>
                        </tr>
                            ';
                        $PROM_SALES_SUM = 0;
                        $DISCOUNT_SUM = 0;
                        $QTY_SUM = 0;
                        $COST_SUM = 0;
                        $MARGIN_SUM = 0;
                        $GP_PCNT_SUM = 0;
                        $AMT_SUM = 0;
                    }
                    if($is_summary != 1){
                        $html .= '<tr>
                            <td colspan="9" align="left" style="'.$font_size.'font-weight:bold;color:#7F0000; background-color: #F3F3F3;" >'.$data->CODE_DESC.'</td>
                            </tr>
                            ';
                    }
                }
                
                
                $code_desc_name = $data->CODE_DESC; // For previous code Name show at the end
                $code_desc_Id = $data->SUM_CODE;// For previous code id show at the end
                
                
                                       
                $html .= '<tr>
                        <td style="'.$font_size.'">'.$data->TRX_PRODUCT.'</td>
                        <td style="'.$font_size.'">'.utf8_encode($data->PROD_DESC).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_PROM_SALES,2).'</td>';
                  if($data->SUM_DISCOUNT < 0){ // Negative Condition
                        //$DISCOUNT_SUM1 = str_ireplace('-','',$data->SUM_DISCOUNT);
                        //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
                        $DISCOUNT_SUM1 = $data->SUM_DISCOUNT;
                        $html .='<td style="'.$font_size.'">'.$this->number_format_result($DISCOUNT_SUM1,2).'</td>';
                    }else{
                        $html .='<td style="'.$font_size.'">'.$this->number_format_result($data->SUM_DISCOUNT,2).'</td>';
                    }  
                
                $html .='<td style="'.$font_size.'">'.$data->SUM_QTY.'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_COST,2).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_MARGIN,2).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->GP_PCNT,1).'</td>
                        <td style="'.$font_size.'">'.$this->number_format_result($data->SUM_AMT,2).'</td>
                        </tr>
                            ';
            /* Group Total */
            $PROM_SALES_SUM += $data->SUM_PROM_SALES;
            $DISCOUNT_SUM += $data->SUM_DISCOUNT;
            $QTY_SUM += $data->SUM_QTY;
            $COST_SUM += $data->SUM_COST;
            $MARGIN_SUM += $data->SUM_MARGIN;
            $GP_PCNT_SUM += $data->GP_PCNT;
            $AMT_SUM += $data->SUM_AMT;
            
            /* OverAll Total */
                 
            $PROM_SALES_SUM_FINAL += $data->SUM_PROM_SALES;
            $DISCOUNT_SUM_FINAL += $data->SUM_DISCOUNT;
            $QTY_SUM_FINAL += $data->SUM_QTY;
            $COST_SUM_FINAL += $data->SUM_COST;
            $MARGIN_SUM_FINAL += $data->SUM_MARGIN;
            $GP_PCNT_SUM_FINAL += $data->GP_PCNT;
            $AMT_SUM_FINAL += $data->SUM_AMT;
            
            
            $GP_PER_AVG_LOOP = ((($AMT_SUM - $COST_SUM) / $AMT_SUM)*100);
            
                 /* For last Count */
                 if($j == $countResult && $is_summary != 1){
                            $html .= '<tr >
                            
                            <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_Id.'</td>
                            <td style="'.$font_size.'font-weight:bold;color:#7F0000">'.$code_desc_name.'
                            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($PROM_SALES_SUM,2).'</td>';
                            if($DISCOUNT_SUM < 0){ // Negative Condition
                                //$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM);
                                //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1.')';
                                $DISCOUNT_SUM1 = $DISCOUNT_SUM;
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM1,2).'</td>';
                            }else{
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM,2).'</td>';
                            }                                    
                            $html .='<td style="'.$font_size.'font-weight:bold;">'.$QTY_SUM.'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($COST_SUM,2).'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($MARGIN_SUM,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($GP_PER_AVG_LOOP,1).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($AMT_SUM,2).'</td>
                                 </tr>
                                ';
                    }
                 
                
          
                
                // set result values
                $sales_data['sum_code']   		= $data->SUM_CODE;
                //$sales_data['department'] 	= $data->TRX_DEPARTMENT;
                $sales_data['quantity']  		= $data->SUM_QTY;
                $sales_data['sales_amount']    	= $data->SUM_AMT;
                $sales_data['sales_cost'] 		= $data->SUM_COST;
                $sales_data['promo_sales']   	= $data->SUM_PROM_SALES;
                $sales_data['promo_sales_gst']	= $data->SUM_PROM_SALES_GST;
                $sales_data['margin'] 		   	= $data->SUM_MARGIN;
                $sales_data['gp_percent'] 		= $data->GP_PCNT;
                $sales_data['discount']   		= $data->SUM_DISCOUNT;
                $sales_data['prod_number']      = $data->TRX_PRODUCT;
                $sales_data['prod_desc'] 		= utf8_encode($data->PROD_DESC);
                $sales_data['code_desc'] 		= $data->CODE_DESC;
                $order_sales_data[] = $sales_data;
                $is_error = 1;
            }
            
            
             /* Over ALL Sum OF Report - Start */
                
            $GP_PER_AVG = ((($AMT_SUM_FINAL - $COST_SUM_FINAL) / $AMT_SUM_FINAL)*100);
                
                  $html .= '<tr style="'.$font_size.'font-weight:bold; background-color: #F3F3F3;">
                            <td style="'.$font_size.'font-weight:bold;"></td>
                            <td style="'.$font_size.'font-weight:bold;">Report Total</td>
                            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($PROM_SALES_SUM_FINAL,2).'</td>';
                            if($DISCOUNT_SUM_FINAL < 0){ // Negative Condition
                                //$DISCOUNT_SUM1 = str_ireplace('-','',$DISCOUNT_SUM_FINAL);
                                //$DISCOUNT_SUM1 = '('.$DISCOUNT_SUM1_FINAL.')';
                                $DISCOUNT_SUM1 = $DISCOUNT_SUM_FINAL;
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM1_FINAL,2).'</td>';
                            }else{
                                $html .='<td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($DISCOUNT_SUM_FINAL,2).'</td>';
                            }                                    
                            $html .='<td style="'.$font_size.'font-weight:bold;">'.$QTY_SUM_FINAL.'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($COST_SUM_FINAL,2).'</td>
                                <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($MARGIN_SUM_FINAL,2).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($GP_PER_AVG,1).'</td>
                             <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($AMT_SUM_FINAL,2).'</td>
                                 </tr>
                                ';  
              $html .= '</table></body></html>';
              
              $html = $html.''.$headerHtml;
			// set report status as in trunk initially
			$updateArray = array("status" => 2);
			$whereClause = array("id" => $sales_report_id);
			$update = $this->_db->UpdateAll($this->sales_report_queue, $updateArray, $whereClause, "");
                /* html to pdf conversion */
               
                ini_set('upload_max_filesize', '100M');  
                ini_set('max_execution_time', 3000);
                ini_set('post_max_size', '500M');
                ini_set("memory_limit","512M");
               
                /* html to pdf conversion */
                ob_start();
                require_once('core/tcpdf/examples/tcpdf_include.php');
				
                // create new PDF document
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('Coyote');
                // set auto page breaks
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                    require_once(dirname(__FILE__).'/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
                //$pdf->AddPage();
                $pdf->AddPage( 'L', 'LETTER' );
                $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                // Print text using writeHTMLCell()
                //$html = addslashes($html);
               
                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                // ---------------------------------------------------------
                $pdfCreate = $pdf->Output(BASEPATH.'/../pdf/'.$pdf_name[$sales_type], 'F');
                
                $filename = $pdf_name[$sales_type]; 
                $status = 1;
                $filepath = BASEPATH.'/../pdf/'.$pdf_name[$sales_type];
               
                /* Use To send PDF data - Start */
                require_once("libraries/class.phpmailer.php");
                $mail = new PHPMailer();
                $mail->IsSMTP();                 	// set mailer to use SMTP
                $mail->SMTPAuth = true;     		// turn on SMTP authentication
                $mail->CharSet="windows-1251";
                $mail->CharSet="utf-8";
                $mail->WordWrap = 50;      			// set word wrap to 50 characters
                $mail->IsHTML(true);  
                $emailid = $email_id;
                $user_name = 'NightOwl: Sales Report';
                $mail->AddAddress($emailid,$user_name);	//sender user email id
                // get email template and subject form table
                $body = 'Please check your requested report -'.$sales_typeString[$sales_type];
                $mail->Subject = $sales_typeString[$sales_type].' Report';
                $mail->Body    = $body;
                $mail->Body    = $body;
                $mail->AddAttachment($filepath);
                if($mail->send()){
                    $updateArray = array("filename" => $filename,"status" => $status,"filepath" => $filepath);
                    $whereClause = array("id" => $sales_report_id);
                    $update = $this->_db->UpdateAll($this->sales_report_queue, $updateArray, $whereClause, "");
                    echo "Mail Send";
                    unlink($filepath);
                    
                }else{
                    echo "Mail Not Send";
                    unlink($filepath);
                }
                /* Use To send PDF data */
              die;
            

            } 
        }else {
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.date_range');
            }
        }
    
/**
 /* Amit Neema 21-06-2016 
 * Function used to send Finacial report on mail Id
 * @return array
 * 
 */
public function finacial_report_download_cron($request_report = array()) {
    $sales_report_id = $request_report['id'];
    $user_id = $request_report['user_id'];
    $email_id = $request_report['email_id'];
    $font_size = 'font-size:8px;';
    
    /* NewParamter Use */
    $selectedzones = '';
    $outletArray = array();  
    
    $request_paramter = $request_report['request_parameter'];
    $request_paramter = json_decode($request_paramter);
      
      
   //get post user params
    $start_date   = $request_paramter->start_date;
    $end_date 	  = $request_paramter->end_date;
   
    $outlet_idA   = $request_paramter->outlet_id;
    /* Find Outlets Array */
   
    /*
    if(!empty($outlet_idA) && $outlet_idA != NULL){
        foreach($outlet_idA as $keyO => $valO){
            if(!empty($valO->outlet_id)){
                $outletArray[] = $valO->outlet_id;
            }
        }
    }
    */
    $outlet_id = '';
    /* Convert Outlet Array Into "," String */
    /*
    if(!empty($outletArray)){
        $outletArray =  array_unique($outletArray);
        foreach($outletArray as $keyO => $valO){
            if(!empty($valO)){
                $outlet_id .= $valO.',';
            }
        }
        $outlet_id = rtrim($outlet_id,',');
    }
    */
    
    
    $sales_typeString = 'Finacial Summary';
    
    $pdf_name = 'finacial_summary.pdf';
    
    
    
    $is_error = 0; // default return code
    
    if(!empty($start_date) && !empty($end_date)) {
    
        $financial_report = $this->financial_sales_report_calculation($start_date,$end_date,$outlet_idA);
    
    
    $outletString = '';
    if(count($outlet_idA) > 1){
        $outletString = '(Selected Outlets)';
    }else if(count($outlet_idA) == 1){
        $outletString = ''.$outlet_idA[0]->outlet_name.'';
    }else{
    }



            $font_size = 'font-size:8px;';
            $font_size_big = 'font-size:10px;';
    
            $html = '<!DOCTYPE html>
            <html><body><table border="0" cellpadding="2" cellspacing="0" width="100%" >
            <tr><td colspan="8" align="center" style="'.$font_size.'font-weight:bold;">AA01SANDBOX</td></tr>';
               
            if(!empty($outletString)){
                $html .='<tr>
                <td colspan="8" align="center" style="'.$font_size.'font-weight:bold;">
                '.$outletString.'
                </td>
                </tr>
                ';
            }
            
            
            $html .='<tr><td colspan="8" align="center" style="'.$font_size_big.';font-weight:bold;">Financial Summary</td></tr>
            ';

            if(!empty($start_date) && !empty($end_date)){
            $html .='<tr>
            <td colspan="8" align="center" style="'.$font_size.'font-weight:bold;">
            For Trading Day '.date('d-m-Y',strtotime($start_date)).' to '.date('d-m-Y',strtotime($end_date)).'
            </td>
            </tr>
            ';
            }
            $html .='<tr>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;width:50px" width="50px">Number</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;width:150px" width="150px">Department</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">% Total</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Cost</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Margin</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">GP%</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Amt</td>
            <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales-ExGst</td>
            </tr>
            ';
/* Row Data Print */
    foreach($financial_report['department_sales_result'] as $financial_reportKey => $financial_reportVal){
    $html .= '<tr>
    <td style="'.$font_size.'">'.$financial_reportVal['vsf_dep_number'].'</td>
    <td style="'.$font_size.'">'.$financial_reportKey.'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_total_pcnt'],1).'</td>
    <td style="'.$font_size.'">'.$financial_reportVal['vsf_dep_sales_cost'].'</td>
    <td style="'.$font_size.'">'.$financial_reportVal['vsf_dep_margin'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_gp'],1).'</td>
    <td style="'.$font_size.'">'.$financial_reportVal['vsf_dep_sales_amt'].'</td>
    <td style="'.$font_size.'">'.$financial_reportVal['vsf_dep_sales_ex_gst'].'</td>
    </tr>
        ';
    
    if($financial_reportVal['vsf_dep_number'] == 26){
        if($financial_report['Commision']['total_lotto_amt'] > 0){
           $html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (LOTTO '.$this->number_format_result($financial_report['Commision']['total_lotto_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }
         if($financial_report['Commision']['total_scratch_amt'] > 0){
           $html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> ((SCRATCHIES '.$this->number_format_result($financial_report['Commision']['total_scratch_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }
         if($financial_report['Commision']['total_etoll_amt'] > 0){
           $html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (ETOLL '.$this->number_format_result($financial_report['Commision']['total_etoll_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            } 

        }  else if($financial_reportVal['vsf_dep_number'] == 37){
			
			if($financial_report['Commision']['total_unleaded_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (UNLEADED '.$this->number_format_result($financial_report['Commision']['total_unleaded_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }  
			
			if($financial_report['Commision']['total_ultimate_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (ULTIMATE '.$this->number_format_result($financial_report['Commision']['total_ultimate_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }   
			if($financial_report['Commision']['total_premium_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (PREMIUM '.$this->number_format_result($financial_report['Commision']['total_premium_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }   
                 
			if($financial_report['Commision']['total_diesel_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (DIESEL '.$this->number_format_result($financial_report['Commision']['total_diesel_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }
		}
        
        
    }

                /* Show Total Data Print */
            $html .= '<tr>
            <td style="'.$font_size.'"></td>
            <td style="'.$font_size.'"></td>
            <td style="'.$font_size.'"></td>
            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_sales_cost'],2).'</td>
            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_margin'],2).'</td>
            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_gp'],1).'</td>
            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_sales_amt'],2).'</td>
            <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_sales_ex_gst'],2).'</td>
            </tr>';

            /* Show Row On Table */
            $html .= '<tr>
                <td colspan="8"><hr></td>
            </tr>';

            /* Show DownSide View */
            /* Heading */
            
            /* Show Row On Table */
            $html .= '<tr>
                <td colspan="4">
                   <table border="0" cellpadding="2" cellspacing="0" width="100%" >';
            
            /* Sales View Start */               
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">Sales</td>
    <td style="'.$font_size.'font-weight:bold;">Qty</td>
    <td style="'.$font_size.'font-weight:bold;">Total</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Sales:</td>
    <td style="'.$font_size.'">'.round($financial_report['sales']['total_sales_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['sales']['total_sales_price'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Rounding: </td>
    <td style="'.$font_size.'">'.$financial_report['sales']['total_rounding_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['sales']['total_rounding_price'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'"></td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'"><hr></td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">TOTAL: </td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['sales']['gross_total_amt'],2).'</td>
    </tr>';
    /* Sales View End */  

    /* payment Type Start */             
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">PAYMENT TYPES</td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Cash:</td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['cash_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['cash_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Eftpos: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['eftpos_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['eftpos_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Amex: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['amex_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['amex_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Diners: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['diners_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['diners_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Other: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['other_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['other_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'"></td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'"><hr></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">TOTAL: </td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['total_amt'],2).'</td>
    </tr>';
    /* payment Type End */

    /* Cash Balance Start */             
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">CASH BALANCE</td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Cash Sales:</td>
    <td style="'.$font_size.'">'.$financial_report['cash_balance']['cash_sales_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['cash_sales_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Less Eftpos Cashout:</td>
    <td style="'.$font_size.'">'.$financial_report['cash_balance']['less_eftpos_cashout_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['less_eftpos_cashout_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Less Pickup Cash:</td>
    <td style="'.$font_size.'">'.$financial_report['cash_balance']['less_pickup_cash_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['less_pickup_cash_amt'],2).'</td>
    </tr>
    <tr><td colspan="2" style="'.$font_size.'">Less Paidout Cash: </td><td style="'.$font_size.'">'.$financial_report['cash_balance']['less_paidout_cash_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['less_paidout_cash_amt'],2).'</td></tr>
    <tr><td colspan="2" style="'.$font_size.'"></td><td style="'.$font_size.'"></td><td style="'.$font_size.'"><hr></td></tr>
    <tr><td colspan="2" style="'.$font_size.'">CASH NET: </td><td style="'.$font_size.'"></td><td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['total_cash_balance'],2).'</td></tr>';

    /*Cash Balance End */
               
    $html .='</table>
    </td>
    <td colspan="4" style="vertical-align:top" valign="top">
    <table border="0" cellpadding="2" cellspacing="0" width="100%" >';
    /* TOTALS Start */    
    $html .='<tr>
    <td colspan="3" style="'.$font_size.'font-weight:bold;">TOTALS</td>
    <td style="'.$font_size.'font-weight:bold;">Total</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Sales Less Gst:</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['sales_less_gst'],2).'</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Total Gst Levied: </td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['total_gst_levied'],2).'</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Non-Gst Product Sales: </td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['non_gst_product_sales'],2).'</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Gst Product Sales: </td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['gst_product_sales'],2).'</td>
    </tr>';
    /* TOTALS End */    

    /* COUNTERS Start */             
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">COUNTERS</td>
    <td style="'.$font_size.'font-weight:bold;">Qty</td>
    <td style="'.$font_size.'font-weight:bold;">Total</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Cancelled Sales:</td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_cancelled_sales_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_cancelled_sales_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Void/Cancel Items:</td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_cancelled_item_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_cancelled_item_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Refund Items: </td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_refund_item_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_refund_item_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Promo Discounts: </td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_promo_disc_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_promo_disc_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Pos Markdowns:  </td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_pos_markdown_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_pos_markdown_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Customers:  </td>
    <td style="'.$font_size.'">'.$financial_report['counters']['total_customers'].'</td>
    <td style="'.$font_size.'"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Avg Items Per Customer:  </td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['avg_items_per_customer'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Avg Spend Per Customer: </td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['avg_spend_per_customer'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total No Sales:</td>
    <td style="'.$font_size.'">'.$financial_report['counters']['total_no_sales_qty'].'</td>
    <td style="'.$font_size.'"></td>
    </tr>
    <tr>
    <td colspan="4" style="'.$font_size.'"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Keyed Open Sales: </td>
    <td style="'.$font_size.'">'.$financial_report['counters']['total_keyed_open_sales_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_keyed_open_sales_amt'],2).'</td>
    </tr>
    </table>
    </td>';
            
    /*Cash Balance End */

    $html .= '</tr></table></body></html>';
          
	// set report status as in trunk initially
	$updateArray = array("status" => 2);
	$whereClause = array("id" => $sales_report_id);
	$update = $this->_db->UpdateAll($this->sales_report_queue, $updateArray, $whereClause, "");     
             
            ini_set('upload_max_filesize', '100M');  
            ini_set('max_execution_time', 3000);
            ini_set('post_max_size', '500M');
            ini_set("memory_limit","512M");
            /* html to pdf conversion */
            header('Content-type: text/html'); 
            ob_start();
            require_once('core/tcpdf/examples/tcpdf_include.php');
            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Coyote');
            // set auto page breaks
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            //$pdf->AddPage();
            $pdf->AddPage( 'L','LETTER');
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // Print text using writeHTMLCell()
            //$html = addslashes($html);
                    
            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            // ---------------------------------------------------------
            $pdfCreate = $pdf->Output(BASEPATH.'/../pdf/finacial_summary.pdf', 'F');
            
            $filename  = 'finacial_summary.pdf';  
            $status = 1;
            $filepath = BASEPATH.'/../pdf/finacial_summary.pdf';
           
            
            /* Use To send PDF data - Start */
            require_once("libraries/class.phpmailer.php");
            $mail = new PHPMailer();
            $mail->IsSMTP();                 	// set mailer to use SMTP
            $mail->SMTPAuth = true;     		// turn on SMTP authentication
            $mail->CharSet="windows-1251";
            $mail->CharSet="utf-8";
            $mail->WordWrap = 50;      			// set word wrap to 50 characters
            $mail->IsHTML(true);  
            $emailid = $email_id;
            $user_name = 'NightOwl: Sales Report';
            $mail->AddAddress($emailid,$user_name);	//sender user email id
            // get email template and subject form table
            $body = 'Please check your requested report - Financial Summary ';
            $mail->Subject = 'Financial Summary Report';
            $mail->Body    = $body;
            $mail->Body    = $body;
            $mail->AddAttachment($filepath);
            if($mail->send()){
                $updateArray = array("filename" => $filename,"status" => $status,"filepath" => $filepath);
            $whereClause = array("id" => $sales_report_id);
            $update = $this->_db->UpdateAll($this->sales_report_queue, $updateArray, $whereClause, "");
            echo "Mail Send";
            unlink($filepath);
            
        }else{
            echo "Mail Not Send";
            unlink($filepath);
        }
    }
}

/**
 * Function used to prepare financial sales query
 * @param null
 * @return string
 * @access private
 * 
 */
 private function financial_sales_query($start_date,$end_date,$outlet_ids) {
     // set outlet count
     $total_outlets = count($outlet_ids);
    // prepare financial sales query
    $sales_query = "SELECT 
    JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, 
    JNLH_TRX_AMT, JNLH_CASHIER, 
    JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, 
    JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, 
    PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, 
    OUTP_CARTON_COST, OUTP_PROM_CTN_COST,
    CODE_DESC,CODE_KEY_NUM
    FROM JNLHTBL 
    JOIN JNLDTBL 
    ON JNLD_YYYYMMDD = JNLH_YYYYMMDD 
    AND JNLD_HHMMSS = JNLH_HHMMSS 
    AND JNLD_OUTLET = JNLH_OUTLET 
    AND JNLD_TILL = JNLH_TILL 
    AND JNLD_TRX_NO = JNLH_TRX_NO 
    LEFT JOIN PRODTBL 
    ON PROD_NUMBER = JNLD_PRODUCT
    LEFT JOIN CODETBL 
    ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'
    LEFT JOIN OUTPTBL 
    ON OUTP_PRODUCT = JNLD_PRODUCT AND 
    OUTP_OUTLET = JNLD_OUTLET
    WHERE (JNLH_TRADING_DATE >= '".$start_date."')
    AND (JNLH_TRADING_DATE <= '".$end_date."') AND (";
    // set outlet id in sales query
    foreach($outlet_ids as $key => $val) {
        if(!empty($val)) {
            $sales_query .= "(JNLH_OUTLET = '".$val->outlet_id."') ";
            if($key < ($total_outlets-1)) {
                $sales_query .= " OR ";
            } else {
                $sales_query .= ")";
            }
        }
    }
    $sales_query .= " ORDER BY CODE_KEY_NUM,JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO";
    return $sales_query;
 }

/**
 * Function used to manage financial sales report 
 * @param null
 * @return string
 * @access public
 * 
 */
public function financial_sales_report() {
	
    // set outlet ids
    $outlet_ids    = $_REQUEST['outlet_id'];
    $outlet_ids    = json_decode($outlet_ids);
    //get post date params
    $start_date   = $this->_common->test_input($_REQUEST['start_date']);
    $end_date 	  = $this->_common->test_input($_REQUEST['end_date']);
    $start_date = str_ireplace('/','-',$start_date);
    $end_date = str_ireplace('/','-',$end_date);
   
    // check outlet ids
    if(empty($outlet_ids) || count($outlet_ids) == 0) {
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.supplier_required_outlets');
        return $_response;
    }
    $outletString = '';
    if(count($outlet_ids) > 1){
        $outletString = '(Selected Outlets)';
    }else if(count($outlet_ids) == 1){
        $outletString = $outlet_ids[0]->outlet_name;
    }else{
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.supplier_required_outlets');
        return $_response;
    
    }
    
    $start_date_strtotime = strtotime($start_date);
    $start_date_seven_strtotime = strtotime("+7 day", $start_date_strtotime);
    $end_date_strtotime 	  = strtotime($end_date);
    if($end_date_strtotime > $start_date_seven_strtotime){
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.seven_day_validation');
		return $_response;
    }
    
    if(!empty($start_date) && !empty($end_date)) {
		 
        $financial_report = $this->financial_sales_report_calculation($start_date,$end_date,$outlet_ids);
        //echo '<pre>';
        //print_r($financial_report);die;
        //print_r($financial_report);die;		
        // HTML Creation For Journal Report
        $_response = $this->financial_sales_report_html($financial_report,$start_date,$end_date,$outletString);
    } else {
        $_response['result_code'] = 0;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.error.supplier_required_dates');
    }
    return $_response;
}
 
/**
 * Function use for calculate the GST value 
 * @param float pass_in_amt
 * @return string
 * @access private
 * 
 */
private function my_gst_calc($pass_in_amt,$vm_gst_percent) {
	
	$gst_inc_amt_in = $pass_in_amt;
	// calculate GST amount
	$gst_amt = 0;
	if ($gst_inc_amt_in <> 0 && $vm_gst_percent <> 0) {
		
		$rate_factor = 1 + ($vm_gst_percent / 100);
		$ex_gst_amt = $gst_inc_amt_in / $rate_factor;
		$gst_amt = $gst_inc_amt_in - $ex_gst_amt;
		//$gst_amt = round($gst_amt);
	}
	return $gst_amt;
}
    
 /* function use for financial_sales_report_calculation   
    @param - startdata , enddate, otletid
    @return array response
*/
function financial_sales_report_calculation($start_date,$end_date,$outlet_ids){
    if(!empty($start_date) && !empty($end_date)) {
		
		// set date
		$start_date = date('Y-m-d',strtotime($start_date));
		$end_date = date('Y-m-d',strtotime($end_date));
		// get vmGstPercent 
		$tax_gst_query = "SELECT CODE_NUM_1 as vm_gst_percent FROM CODETBL WHERE CODE_KEY_TYPE = 'TAXCODE' AND CODE_KEY_ALP = 'GST'";
        $vm_gst_result = $this->_mssql->my_mssql_query($tax_gst_query,$this->con);
        $vm_gst_data = $this->_mssql->mssql_fetch_object_query($vm_gst_result);
		$vm_gst_percent = (isset($vm_gst_data->vm_gst_percent) && !empty($vm_gst_data->vm_gst_percent)) ? $vm_gst_data->vm_gst_percent : 0;
        
        // get financial sales query
		$sales_query =  $this->financial_sales_query($start_date,$end_date,$outlet_ids);
	
		/*
		$sales_query = "SELECT 
		JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, 
		JNLH_TRX_AMT, JNLH_CASHIER, 
		JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, 
		JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, 
		PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, 
		OUTP_CARTON_COST, OUTP_PROM_CTN_COST 
		FROM JNLHTBL 
		JOIN JNLDTBL 
		ON JNLD_YYYYMMDD = JNLH_YYYYMMDD 
		AND JNLD_HHMMSS = JNLH_HHMMSS 
		AND JNLD_OUTLET = JNLH_OUTLET 
		AND JNLD_TILL = JNLH_TILL 
		AND JNLD_TRX_NO = JNLH_TRX_NO 
		LEFT JOIN PRODTBL 
		ON PROD_NUMBER = JNLD_PRODUCT 
		LEFT JOIN OUTPTBL 
		  ON OUTP_PRODUCT = JNLD_PRODUCT AND 
			 OUTP_OUTLET = JNLD_OUTLET
		WHERE (JNLH_TRADING_DATE >= '2016-05-02')
		  AND (JNLH_TRADING_DATE <= '2016-05-02')
		 AND  ((JNLH_OUTLET = 792)) 
		ORDER BY JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO ";*/
		// echo $sales_query;die;
        //$sales_query  = "SELECT * FROM financial_test_tbl"; 
        $sales_result = $this->_mssql->my_mssql_query($sales_query,$this->con);
        //$sales_result = $this->_db->my_query($sales_query);
        // initialize default param values
        $total_qty = 0;
        $TrxTotal  = 0;
        $tot_qty_cancel_sales = 0;
        $tot_amt_cancel_sales = 0;
        $department_array = array();// initialize department product sales array
        if($this->_mssql->pdo_num_rows_query($sales_query,$this->con) > 0 ) {
		//if ($this->_db->my_num_rows($sales_result) > 0) {
            while($result = $this->_mssql->mssql_fetch_object_query($sales_result)) {
            //while($result = $this->_db->my_fetch_object($sales_result)) {
 
                // prepare journal values for sales array
                $product_sales_array['JNLH_OUTLET'] = $result->JNLH_OUTLET;
                $product_sales_array['JNLH_TRX_NO'] = $result->JNLH_TRX_NO;
                $product_sales_array['JNLH_TYPE'] 	= $result->JNLH_TYPE;
                $product_sales_array['JNLH_STATUS'] = $result->JNLH_STATUS;
                $product_sales_array['JNLH_TRX_AMT']= $result->JNLH_TRX_AMT;
                $product_sales_array['JNLH_CASHIER']= $result->JNLH_CASHIER;
                $product_sales_array['JNLD_TYPE'] 	= $result->JNLD_TYPE;
                $product_sales_array['JNLD_STATUS'] = $result->JNLD_STATUS;
                $product_sales_array['JNLD_QTY']	= $result->JNLD_QTY;
                $product_sales_array['JNLD_AMT'] 	= $result->JNLD_AMT;
                $product_sales_array['JNLD_COST'] 	= $result->JNLD_COST;
                $product_sales_array['JNLD_GST_AMT']= $result->JNLD_GST_AMT;
                $product_sales_array['JNLD_DESC'] 	= $result->JNLD_DESC;
                $product_sales_array['JNLD_OFFER'] 	= $result->JNLD_OFFER;
                $product_sales_array['JNLD_MIXMATCH'] 	= $result->JNLD_MIXMATCH;
                $product_sales_array['JNLD_DISC_AMT'] 	= $result->JNLD_DISC_AMT;
                $product_sales_array['JNLD_POST_STATUS'] = $result->JNLD_POST_STATUS;
                $product_sales_array['PROD_DEPARTMENT'] = $result->PROD_DEPARTMENT;
                $product_sales_array['PROD_TAX_CODE'] = $result->PROD_TAX_CODE;
                $product_sales_array['PROD_CATEGORY'] = $result->PROD_CATEGORY;
                $product_sales_array['PROD_COMMODITY']= $result->PROD_COMMODITY;
                
                // append product sales data in department index
                $department_array[$result->CODE_DESC][] = $product_sales_array;
                //$department_array[$result->PROD_DEPARTMENT][] = $product_sales_array;
            }
        }
		//print_r($department_array); 
		//die;
        $financial_report = array();
        $department_sales_array = array();
        // initialize department total param value
        $total_sales_cost = 0;
        $total_sales_amt  = 0;
        $total_sales_qty  = 0;
        $total_margin  	= 0;
        $total_sales_ex_gst = 0;
        $total_rounding_qty = 0;
        $total_rounding_amt = 0;
        $total_cashout_qty = 0;
        $total_cashout_amt = 0;
        $total_pickup_qty = 0;
        $total_pickup_amt = 0;
        $total_paidout_qty = 0;
        $total_paidout_amt = 0;
        $total_gst_amt = 0;
        $total_non_gst_amt = 0;
        $total_cash_qty = 0;
        $total_cash_amt = 0;
        $total_eftpos_qty = 0;
        $total_eftpos_amt = 0;
        $total_amex_qty = 0;
        $total_amex_amt = 0;
        $total_diners_qty = 0;
        $total_diners_amt = 0;
        $total_other_qty = 0;
        $total_other_amt = 0;
        $total_cancel_sales_qty = 0;
        $total_cancel_sales_amt = 0;
        $total_cancel_item_qty = 0;
        $total_cancel_item_amt = 0;
        $total_refund_item_qty = 0;
        $total_refund_item_amt = 0;
        $total_markdown_qty = 0;
        $total_markdown_amt = 0;
        $total_promo_disc_qty = 0;
        $total_promo_disc_amt = 0;
        $total_item_qty = 0;
        $total_customers = 0;
        $total_customer_qty_avg = 0;
        $total_no_sale_qty = 0;
        $total_keyed_open_sales_qty = 0;
        $total_keyed_open_sales_amt = 0;
        $total_lotto_amt = 0;
        $total_scratch_amt = 0;
        $total_etoll_amt = 0;
        $total_unleaded_amt = 0;
        $total_ultimate_amt = 0;
        $total_premium_amt = 0;
        $total_diesel_amt = 0;
		$trx_numbers =array();
        // prepare department sales report result
        foreach($department_array as $key=>$department_results) {
            // initialize default values
            $vsf_dep_amt = 0;
            $vsf_dep_cost = 0;
            $vsf_dep_gst_amt = 0;
            $vsf_dep_sale_count = 0;
            $vsf_dep_margin = 0;
            $vsf_dep_sales_qty = 0;
            $vsf_dep_rounding_qty = 0;
            $vsf_dep_rounding_amt = 0;
            $vsf_dep_cashout_qty = 0;
            $vsf_dep_cashout_amt = 0;
            $vsf_dep_pickup_qty = 0;
            $vsf_dep_pickup_amt = 0;
            $vsf_dep_paidout_qty = 0;
            $vsf_dep_paidout_amt = 0;
            $vsf_dep_non_gst_sales_amt = 0;
            $vsf_dep_cash_qty = 0;
            $vsf_dep_cash_amt = 0;
            $vsf_dep_eftpos_qty = 0;
            $vsf_dep_eftpos_amt = 0;
            $vsf_dep_amex_qty = 0;
            $vsf_dep_amex_amt = 0;
            $vsf_dep_diners_qty = 0;
            $vsf_dep_diners_amt = 0;
            $vsf_dep_other_qty = 0;
            $vsf_dep_other_amt = 0;
            $vsf_dep_cancel_sales_qty = 0;
            $vsf_dep_cancel_sales_amt = 0;
            $vsf_dep_cancel_item_qty = 0;
            $vsf_dep_cancel_item_amt = 0;
            $vsf_dep_refund_item_qty = 0;
            $vsf_dep_refund_item_amt = 0;
            $vsf_dep_markdown_qty = 0;
            $vsf_dep_markdown_amt = 0;
            $vsf_dep_promo_disc_qty = 0;
            $vsf_dep_promo_disc_amt = 0;
            $vsf_dep_customers_count = 0;
            $vsf_dep_item_qty = 0;
            $vsf_dep_last_trx_no = 0;
            $vsf_dep_no_sale_qty = 0;
            $vsf_dep_keyed_open_sales_qty = 0;
            $vsf_dep_keyed_open_sales_amt = 0;
            $vsf_dep_lotto_amt = 0;
            $vsf_dep_scratch_amt = 0;
            $vsf_dep_etoll_amt = 0;
            $vsf_dep_unleaded_amt = 0;
            $vsf_dep_ultimate_amt = 0;
            $vsf_dep_premium_amt = 0;
            $vsf_dep_diesel_amt = 0;
            // set sales total values of department
            foreach($department_results as $d_result) {
                if(!empty($key)) {
                    if($d_result['JNLD_STATUS'] == 1 && $d_result['JNLD_TYPE'] == 'SALE') {
                        // add sales values
                        $vsf_dep_amt 		= $vsf_dep_amt+$d_result['JNLD_AMT'];
                        $vsf_dep_cost 		= $vsf_dep_cost+$d_result['JNLD_COST'];
                        $vsf_dep_gst_amt	= $vsf_dep_gst_amt+$d_result['JNLD_GST_AMT'];
                        $vsf_dep_sales_qty	= $vsf_dep_sales_qty+$d_result['JNLD_QTY'];
                        $vsf_dep_sale_count = $vsf_dep_sale_count+1;
                        $vsf_dep_number		= $d_result['PROD_DEPARTMENT'];
                    }
                }
                // set values for rounding , cashout , pickup and paidout sale type
                if($d_result['JNLH_STATUS'] == 1 && $d_result['JNLD_STATUS'] == 1) {
                    
                    if($d_result['JNLD_TYPE'] == 'ROUNDING') { // set values for rounding
                        $vsf_dep_rounding_qty = $vsf_dep_rounding_qty+1;
                        $vsf_dep_rounding_amt = $vsf_dep_rounding_amt+$d_result['JNLD_AMT'];
                    } else if($d_result['JNLD_TYPE'] == 'CASHOUT') { // set values for cashout
                        $vsf_dep_cashout_qty = $vsf_dep_cashout_qty+1;
                        $vsf_dep_cashout_amt = $vsf_dep_cashout_amt+$d_result['JNLD_AMT'];
                    } else if($d_result['JNLD_TYPE'] == 'PICKUP') { // set values for pickup
                        $vsf_dep_pickup_qty = $vsf_dep_pickup_qty+1;
                        $vsf_dep_pickup_amt = $vsf_dep_pickup_amt+$d_result['JNLD_AMT'];
                    } else if($d_result['JNLD_TYPE'] == 'PAIDOUT') { // set values for paidout
                        $vsf_dep_paidout_qty = $vsf_dep_paidout_qty+1;
                        $vsf_dep_paidout_amt = $vsf_dep_paidout_amt+$d_result['JNLD_AMT'];
                    }
                    
                } else {
                    // set cancelled sales data
                    if($d_result['JNLH_STATUS'] == 0 && $d_result['JNLD_TYPE'] == 'SALE') {
                        $vsf_dep_cancel_sales_qty = $vsf_dep_cancel_sales_qty+1;
                        $vsf_dep_cancel_sales_amt = $vsf_dep_cancel_sales_amt+$d_result['JNLD_AMT'];
                    }
                    // set cancelled item data
                    if($d_result['JNLD_STATUS'] == 0 && $d_result['JNLD_TYPE'] == 'SALE') {
                        $vsf_dep_cancel_item_qty = $vsf_dep_cancel_item_qty+1;
                        $vsf_dep_cancel_item_amt = $vsf_dep_cancel_item_amt+$d_result['JNLD_AMT'];
                    }
                }
                
                if($d_result['JNLD_STATUS'] == 1 && $d_result['JNLD_GST_AMT'] == 0 && $d_result['JNLD_TYPE'] == 'SALE') {
					$gst_amount = 0;
					if($d_result['PROD_TAX_CODE'] == 'GST') {
						// get calculated non gst amount
						$gst_amount = $this->my_gst_calc($d_result['JNLD_AMT'],$vm_gst_percent);
					}
				}
			
                // set non gst sales amount
                if(isset($gst_amount) && $gst_amount == 0 ) {
					$vsf_dep_non_gst_sales_amt = $vsf_dep_non_gst_sales_amt+$d_result['JNLD_AMT'];
                }
				$gst_amount = 1;
                // set total payment type quantities
                if($d_result['JNLD_TYPE'] == 'TENDER' && $d_result['JNLD_STATUS'] == 1) {
                     
                    if($d_result['JNLD_DESC'] == 'CASH') {
                        $vsf_dep_cash_qty = $vsf_dep_cash_qty+1;
                        $vsf_dep_cash_amt = $vsf_dep_cash_amt+$d_result['JNLD_AMT'];
                    } else if($d_result['JNLD_DESC'] == 'EFTPOS' || $d_result['JNLD_DESC'] == 'VISA' || $d_result['JNLD_DESC'] == 'MASTERCARD') {
                        $vsf_dep_eftpos_qty = $vsf_dep_eftpos_qty+1;
                        $vsf_dep_eftpos_amt = $vsf_dep_eftpos_amt+$d_result['JNLD_AMT'];
                    } else if($d_result['JNLD_DESC'] == 'AMEX') {
                        $vsf_dep_amex_qty = $vsf_dep_amex_qty+1;
                        $vsf_dep_amex_amt = $vsf_dep_amex_amt+$d_result['JNLD_AMT'];
                    } else if($d_result['JNLD_DESC'] == 'DINERS') {
                        $vsf_dep_diners_qty = $vsf_dep_diners_qty+1;
                        $vsf_dep_diners_amt = $vsf_dep_diners_amt+$d_result['JNLD_AMT'];
                    } else {
                        $vsf_dep_other_qty = $vsf_dep_other_qty+1;
                        $vsf_dep_other_amt = $vsf_dep_other_amt+$d_result['JNLD_AMT'];
                    }
                }
                
                // set discount item values
                if($d_result['JNLD_DISC_AMT'] <> 0) {
                    if($d_result['JNLD_MIXMATCH'] == '' && $d_result['JNLD_OFFER'] == '') {
                        // set markdown value
                        $vsf_dep_markdown_qty = $vsf_dep_markdown_qty+$d_result['JNLD_QTY'];
                        $vsf_dep_markdown_amt = $vsf_dep_markdown_amt+$d_result['JNLD_DISC_AMT'];
                    } else {
                        // set promo discount values
                        $vsf_dep_promo_disc_qty = $vsf_dep_promo_disc_qty+$d_result['JNLD_QTY'];
                        $vsf_dep_promo_disc_amt = $vsf_dep_promo_disc_amt+$d_result['JNLD_DISC_AMT'];
                    }
                }
                
                // set total customers
                if($d_result['JNLH_STATUS'] == 1 && $d_result['JNLH_TYPE'] == 'SALE') {
					if(!in_array($d_result['JNLH_TRX_NO'],$trx_numbers)) {
						$trx_numbers[] = $d_result['JNLH_TRX_NO'];
						// add customer count
						if($last_trx_no <> $d_result['JNLH_TRX_NO']) {
							$vsf_dep_customers_count = $vsf_dep_customers_count+1;
						}
						// set last trx numbers
						$last_trx_no = $d_result['JNLH_TRX_NO'];
					}
                }
                
                if($d_result['JNLD_STATUS'] == 1 && $d_result['JNLD_TYPE'] == 'SALE') {
                    // set item quantities
                    $vsf_dep_item_qty = $vsf_dep_item_qty+$d_result['JNLD_QTY'];
                    // set refund item values
                    if($d_result['JNLD_AMT'] < 0) {
                        $vsf_dep_refund_item_qty = $vsf_dep_refund_item_qty+$d_result['JNLD_QTY'];
                        $vsf_dep_refund_item_amt = $vsf_dep_refund_item_amt+$d_result['JNLD_AMT'];
                    }
                }
                
                // set no sales item quantities
                if($d_result['JNLD_STATUS'] == 1 && $d_result['JNLD_TYPE'] == 'NOSALE') {
                    $vsf_dep_no_sale_qty = $vsf_dep_no_sale_qty+1;
                }
                // set keyed open sales values
                if($d_result['PROD_CATEGORY'] == 1001) {
                    $vsf_dep_keyed_open_sales_qty = $vsf_dep_keyed_open_sales_qty+$d_result['JNLD_QTY'];
                    $vsf_dep_keyed_open_sales_amt = $vsf_dep_keyed_open_sales_amt+$d_result['JNLD_AMT'];
                }
                // set commodity and depatment specific type values
                if( $d_result['PROD_DEPARTMENT'] == 26 && $d_result['PROD_COMMODITY'] == 4701) {
                    $vsf_dep_lotto_amt = $vsf_dep_lotto_amt+$d_result['JNLD_AMT'];
                } else if( $d_result['PROD_DEPARTMENT'] == 26 && $d_result['PROD_COMMODITY'] == 4702) {
                    $vsf_dep_scratch_amt = $vsf_dep_scratch_amt+$d_result['JNLD_AMT'];
                } else if( $d_result['PROD_DEPARTMENT'] == 26 && $d_result['PROD_COMMODITY'] == 461) {
                    $vsf_dep_etoll_amt = $vsf_dep_etoll_amt+$d_result['JNLD_AMT'];
                } else if( $d_result['PROD_DEPARTMENT'] == 37 && $d_result['PROD_COMMODITY'] == 4001) {
                    $vsf_dep_unleaded_amt = $vsf_dep_unleaded_amt+$d_result['JNLD_AMT'];
                } else if( $d_result['PROD_DEPARTMENT'] == 37 && $d_result['PROD_COMMODITY'] == 4101) {
                    $vsf_dep_ultimate_amt = $vsf_dep_ultimate_amt+$d_result['JNLD_AMT'];
                } else if( $d_result['PROD_DEPARTMENT'] == 37 && $d_result['PROD_COMMODITY'] == 4201) {
                    $vsf_dep_premium_amt = $vsf_dep_premium_amt+$d_result['JNLD_AMT'];
                } else if( $d_result['PROD_DEPARTMENT'] == 37 && $d_result['PROD_COMMODITY'] == 4301) {
                    $vsf_dep_diesel_amt = $vsf_dep_diesel_amt+$d_result['JNLD_AMT'];
                }
            }
            // append sales value in array
            $dept_data['vsf_dep_sales_cost'] = $vsf_dep_cost; // sales cost
            $dept_data['vsf_dep_sales_amt']  = $vsf_dep_amt; // sales amount
            $dept_data['vsf_dep_gst_amt']	 = $vsf_dep_gst_amt;
            $dept_data['vsf_dep_margin'] 	 = $vsf_dep_amt-$vsf_dep_cost; // margin
            $dept_data['vsf_dep_sales_ex_gst'] = $vsf_dep_amt-$vsf_dep_gst_amt; // sales exGst
            $dept_data['vsf_dep_sale_count'] = $vsf_dep_sale_count;
            $dept_data['vsf_dep_number'] 	 = $vsf_dep_number;
    
            // set total department values
            $total_sales_cost 	= $total_sales_cost+$vsf_dep_cost;
            $total_sales_amt  	= $total_sales_amt+$vsf_dep_amt;
            $total_margin 	  	= $total_margin+$dept_data['vsf_dep_margin'];
            $total_sales_ex_gst = $total_sales_ex_gst+$dept_data['vsf_dep_sales_ex_gst'];
            $total_sales_qty 	= $total_sales_qty+$vsf_dep_sales_qty;
            $total_gst_amt		= $total_gst_amt+$vsf_dep_gst_amt;
            $total_non_gst_amt  = $total_non_gst_amt+$vsf_dep_non_gst_sales_amt;
            // set total values for rounding , cashout , pickup and paidout sale type
            $total_rounding_qty = $total_rounding_qty+$vsf_dep_rounding_qty;
            $total_rounding_amt = $total_rounding_amt+$vsf_dep_rounding_amt;
            $total_cashout_qty  = $total_cashout_qty+$vsf_dep_cashout_qty;
            $total_cashout_amt  = $total_cashout_amt+$vsf_dep_cashout_amt;
            $total_pickup_qty   = $total_pickup_qty+$vsf_dep_pickup_qty;
            $total_pickup_amt   = $total_pickup_amt+$vsf_dep_pickup_amt;
            $total_paidout_qty  = $total_paidout_qty+$vsf_dep_paidout_qty;
            $total_paidout_amt  = $total_paidout_amt+$vsf_dep_paidout_amt;
            // set payment types quantities
            $total_cash_qty 	= $total_cash_qty+$vsf_dep_cash_qty;
            $total_eftpos_qty   = $total_eftpos_qty+$vsf_dep_eftpos_qty;
            $total_amex_qty   	= $total_amex_qty+$vsf_dep_amex_qty;
            $total_diners_qty   = $total_diners_qty+$vsf_dep_diners_qty;
            $total_other_qty    = $total_other_qty+$vsf_dep_other_qty;
            // set payment types total amount
            $total_cash_amt 	= $total_cash_amt+$vsf_dep_cash_amt;
            $total_eftpos_amt   = $total_eftpos_amt+$vsf_dep_eftpos_amt;
            $total_amex_amt   	= $total_amex_amt+$vsf_dep_amex_amt;
            $total_diners_amt   = $total_diners_amt+$vsf_dep_diners_amt;
            $total_other_amt    = $total_other_amt+$vsf_dep_other_amt;
            // set counters total sales values
            $total_cancel_sales_qty = $total_cancel_sales_qty+$vsf_dep_cancel_sales_qty;
            $total_cancel_sales_amt = $total_cancel_sales_amt+$vsf_dep_cancel_sales_amt;
            // set counters total items values
            $total_cancel_item_qty = $total_cancel_item_qty+$vsf_dep_cancel_item_qty;
            $total_cancel_item_amt = $total_cancel_item_amt+$vsf_dep_cancel_item_amt;
            // set counters total refund item values
            $total_refund_item_qty = $total_refund_item_qty+$vsf_dep_refund_item_qty;
            $total_refund_item_amt = $total_refund_item_amt+$vsf_dep_refund_item_amt;
            // set counters total markdown values
            $total_markdown_qty  = $total_markdown_qty+$vsf_dep_markdown_qty;
            $total_markdown_amt  = $total_markdown_amt+$vsf_dep_markdown_amt;
            // set counters total promo discount values
            $total_promo_disc_qty = $total_promo_disc_qty+$vsf_dep_promo_disc_qty;
            $total_promo_disc_amt = $total_promo_disc_amt+$vsf_dep_promo_disc_amt;
            // set total customers count
            $total_customers = $total_customers+$vsf_dep_customers_count;
            // set total item quantity
            $total_item_qty = $total_item_qty+$vsf_dep_item_qty;
            // set total no sales quantity
            $total_no_sale_qty = $total_no_sale_qty+$vsf_dep_no_sale_qty;
            // set total keyed open sales values
            $total_keyed_open_sales_qty = $total_keyed_open_sales_qty+$vsf_dep_keyed_open_sales_qty;
            $total_keyed_open_sales_amt = $total_keyed_open_sales_amt+$vsf_dep_keyed_open_sales_amt;
            
            // set commission types total values 
            $total_lotto_amt 	= $total_lotto_amt+$vsf_dep_lotto_amt;
            $total_scratch_amt  = $total_scratch_amt+$vsf_dep_scratch_amt;
            $total_etoll_amt    = $total_etoll_amt+$vsf_dep_etoll_amt;
            $total_unleaded_amt = $total_unleaded_amt+$vsf_dep_unleaded_amt;
            $total_ultimate_amt = $total_ultimate_amt+$vsf_dep_ultimate_amt;
            $total_premium_amt  = $total_premium_amt+$vsf_dep_premium_amt;
            $total_diesel_amt   = $total_diesel_amt+$vsf_dep_diesel_amt;
            
            /* financial array to get commision value for sub raw */
            
            $financial_report['Commision']['total_lotto_amt'] = $total_lotto_amt;
            $financial_report['Commision']['total_scratch_amt'] = $total_scratch_amt;
            $financial_report['Commision']['total_etoll_amt'] = $total_etoll_amt;
            $financial_report['Commision']['total_unleaded_amt'] = $total_unleaded_amt;
            $financial_report['Commision']['total_ultimate_amt'] = $total_ultimate_amt;
            $financial_report['Commision']['total_premium_amt'] = $total_premium_amt;
            $financial_report['Commision']['total_diesel_amt'] = $total_diesel_amt;
            
            
            // append result in department array
            $department_sales_array[$key] = $dept_data;	
        }
        
        // re initialize department array
        $department_sales = array();
        foreach($department_sales_array as $key=>$sales_report) {
            // append sales value in array
            $dept_sales['vsf_dep_total_pcnt']   = ($sales_report['vsf_dep_sales_amt'] / $total_sales_amt) * 100; // total percentage
            $dept_sales['vsf_dep_sales_cost']   = $sales_report['vsf_dep_sales_cost']; // sales cost
            $dept_sales['vsf_dep_margin'] 	    = $sales_report['vsf_dep_margin']; // margin
            $dept_sales['vsf_dep_gp']		    = ((($sales_report['vsf_dep_sales_amt'] - $sales_report['vsf_dep_sales_cost']) * 100) / $sales_report['vsf_dep_sales_amt']); // total percentage
            $dept_sales['vsf_dep_sales_amt']    = $sales_report['vsf_dep_sales_amt']; // sales amount
            $dept_sales['vsf_dep_sales_ex_gst'] = $sales_report['vsf_dep_sales_ex_gst']; // sales exGst
            $dept_sales['vsf_dep_sale_count']   = $sales_report['vsf_dep_sale_count'];
            $dept_sales['vsf_dep_number'] 	    = $sales_report['vsf_dep_number'];
            
            // append result in department array
            if(!empty($key)) {
                $department_sales[$key] = $dept_sales;
            }
        }
        // set total payment type amount
        $total_payment_type_amt = $total_cash_amt+$total_eftpos_amt+$total_amex_amt+$total_diners_qty+$total_other_amt;
        // set total cash balance amount
        $total_cash_balance_amt = $total_cash_amt-$total_cashout_amt-$total_pickup_amt-$total_paidout_amt;
        // set total customers average quantity
        if($total_customers > 0 && $total_item_qty <> 0) {
            $total_customer_qty_avg = $total_item_qty/$total_customers;
        }
        // set total customers average amount
        if($total_customers > 0 && $total_sales_amt <> 0) {
            $total_customer_amt_avg = $total_sales_amt / $total_customers;
        }
        // set total sales net amount
        $total_sales_net_amt = $total_sales_amt+$total_rounding_amt;
        // append final department array in financial report
        $financial_report['department_sales_result'] = $department_sales;
        $financial_report['total_sales_cost']= $total_sales_cost;
        $financial_report['total_sales_amt'] = $total_sales_amt;
        $financial_report['total_margin'] 	 = $total_margin;
        $financial_report['total_gp'] 		 = ((($total_sales_amt - $total_sales_cost) * 100) / $total_sales_amt);
        $financial_report['total_sales_ex_gst'] = $total_sales_ex_gst;
        // prepare sales data in financial array
        $financial_report['sales'] 			= array('total_sales_qty'=>$total_sales_qty,
                                            'total_sales_price'=>$total_sales_amt,
                                            'total_rounding_qty'=>$total_rounding_qty,
                                            'total_rounding_price'=>$total_rounding_amt,
                                            'gross_total_amt'=>($total_sales_net_amt)
                                            );
        // prepare total data in financial array								
        $financial_report['totals'] 		= array('sales_less_gst'=>($total_sales_ex_gst+$total_rounding_amt),
                                            'total_gst_levied'=>$total_gst_amt,
                                            'non_gst_product_sales'=>$total_non_gst_amt,
                                            'gst_product_sales'=>($total_sales_net_amt-$total_non_gst_amt)
                                            );
        // prepare payment types data in financial array									
        $financial_report['payment_types']	= array('cash_qty'=>$total_cash_qty,
                                            'cash_amt'=>$total_cash_amt,
                                            'eftpos_qty'=>$total_eftpos_qty,
                                            'eftpos_amt'=>$total_eftpos_amt,
                                            'amex_qty'=>$total_amex_qty,
                                            'amex_amt'=>$total_amex_amt,
                                            'diners_qty'=>$total_diners_qty,
                                            'diners_amt'=>$total_diners_amt,
                                            'other_qty'=>$total_other_qty,
                                            'other_amt'=>$total_other_amt,
                                            'total_amt'=>$total_payment_type_amt
                                            );
        // prepare cash balance data in financial array									
        $financial_report['cash_balance'] 	= array('cash_sales_qty'=>$total_cash_qty,
                                            'cash_sales_amt'=>$total_cash_amt,
                                            'less_eftpos_cashout_qty'=>$total_cashout_qty,
                                            'less_eftpos_cashout_amt'=>$total_cashout_amt,
                                            'less_pickup_cash_qty'=>$total_pickup_qty,
                                            'less_pickup_cash_amt'=>$total_pickup_amt,
                                            'less_paidout_cash_qty'=>$total_paidout_qty,
                                            'less_paidout_cash_amt'=>$total_paidout_amt,
                                            'total_cash_balance'=>$total_cash_balance_amt
                                            );
        // prepare counters data in financial array
        $financial_report['counters'] 		= array('total_cancelled_sales_qty'=>$total_cancel_sales_qty,
                                            'total_cancelled_sales_amt'=>$total_cancel_sales_amt,
                                            'total_cancelled_item_qty'=>$total_cancel_item_qty,
                                            'total_cancelled_item_amt'=>$total_cancel_item_amt,
                                            'total_refund_item_qty'=>$total_refund_item_qty,
                                            'total_refund_item_amt'=>$total_refund_item_amt,
                                            'total_promo_disc_qty'=>$total_promo_disc_qty,
                                            'total_promo_disc_amt'=>$total_promo_disc_amt,
                                            'total_pos_markdown_qty'=>$total_markdown_qty,
                                            'total_pos_markdown_amt'=>$total_markdown_amt,
                                            'total_customers'=>$total_customers,
                                            'avg_items_per_customer'=>$total_customer_qty_avg,
                                            'avg_spend_per_customer'=>$total_customer_amt_avg,
                                            'total_no_sales_qty'=>$total_no_sale_qty,
                                            'total_keyed_open_sales_qty'=>$total_keyed_open_sales_qty,
                                            'total_keyed_open_sales_amt'=>$total_keyed_open_sales_amt,
                                            );
        return $financial_report;
    }
}
  
 
/**
 * Function used to prepare financial sales html / pdf
 * @param null
 * @return string
 * @access private
 * 
 */
private function financial_sales_report_html($financial_report,$start_date='',$end_date='',$outletString = '') {		
    // set html / pdf input values
    $is_pdf = !empty($this->_common->test_input($_REQUEST['is_pdf'])) ? $this->_common->test_input($_REQUEST['is_pdf']):0; 
    if($is_pdf == 1){
        $font_size = 'font-size:8px;';
        $font_size_big = 'font-size:10px;';
    }else{
        $font_size = 'font-size:12px;';
        $font_size_big = 'font-size:14px;';
    }
                
    $html = '<!DOCTYPE html>
    <html><body><table border="0" cellpadding="2" cellspacing="0" width="100%" >';
    
    $html .='<tr>
        <td colspan="8" align="center" style="'.$font_size.'font-weight:bold;">AA01SANDBOX</td></tr>';
    
    
    if(!empty($outletString)){
        $html .='<tr>
        <td colspan="8" align="center" style="'.$font_size.'font-weight:bold;">
        '.$outletString.'
        </td>
        </tr>
        ';
    }

    $html .='<tr><td colspan="8" align="center" style="'.$font_size_big.';font-weight:bold;">Financial Summary</td></tr>
    ';

    if(!empty($start_date) && !empty($end_date)){
        $html .='<tr>
        <td colspan="8" align="center" style="'.$font_size.'font-weight:bold;">
        For Trading Day '.date('d-m-Y',strtotime($start_date)).' to '.date('d-m-Y',strtotime($end_date)).'
        </td>
        </tr>
        ';
    }
    $html .='<tr>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;width:50px"  width="50px">Number</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;width:150px" width="150px">Department</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">% Total</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Cost</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Margin</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">GP%</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales Amt</td>
    <td style="'.$font_size.'font-weight:bold;background-color: #BCE6FF;">Sales-ExGst</td>
    </tr>
    ';
    /* Row Data Print */
    foreach($financial_report['department_sales_result'] as $financial_reportKey => $financial_reportVal){
    $html .= '<tr>
    <td style="'.$font_size.'">'.$financial_reportVal['vsf_dep_number'].'</td>
    <td style="'.$font_size.'">'.$financial_reportKey.'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_total_pcnt'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_sales_cost'],2).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_margin'],2).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_gp'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_sales_amt'],2).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_reportVal['vsf_dep_sales_ex_gst'],2).'</td>
    </tr>
        ';
    
    if($financial_reportVal['vsf_dep_number'] == 26){
        if($financial_report['Commision']['total_lotto_amt'] > 0){
           $html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (LOTTO '.$this->number_format_result($financial_report['Commision']['total_lotto_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }
         if($financial_report['Commision']['total_scratch_amt'] > 0){
           $html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> ((SCRATCHIES '.$this->number_format_result($financial_report['Commision']['total_scratch_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }
         if($financial_report['Commision']['total_etoll_amt'] > 0){
           $html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (ETOLL '.$this->number_format_result($financial_report['Commision']['total_etoll_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            } 

        }  else if($financial_reportVal['vsf_dep_number'] == 37){
			
			if($financial_report['Commision']['total_unleaded_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (UNLEADED '.$this->number_format_result($financial_report['Commision']['total_unleaded_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }  
			
			if($financial_report['Commision']['total_ultimate_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (ULTIMATE '.$this->number_format_result($financial_report['Commision']['total_ultimate_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }   
			if($financial_report['Commision']['total_premium_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (PREMIUM '.$this->number_format_result($financial_report['Commision']['total_premium_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }   
                 
			if($financial_report['Commision']['total_diesel_amt'] > 0){
			$html .= '<tr>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'"> (DIESEL '.$this->number_format_result($financial_report['Commision']['total_diesel_amt'],2).')</td>
                <td style="'.$font_size.'"></td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.0</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                <td style="'.$font_size.'">0.00</td>
                </tr>';
            }
		}
        
        
    }

    /* Show Total Data Print */
    $html .= '<tr>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_sales_cost'],2).'</td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_margin'],2).'</td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_gp'],1).'</td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_sales_amt'],2).'</td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['total_sales_ex_gst'],2).'</td>
    </tr>';

    /* Show Row On Table */
    $html .= '<tr>
    <td colspan="8"><hr></td>
    </tr>';

    /* Show DownSide View */
    /* Heading */
    /* Show Row On Table */
    $html .= '<tr>
    <td colspan="4">
    <table border="0" cellpadding="2" cellspacing="0" width="100%" >';
    /* Sales View Start */               
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">Sales</td>
    <td style="'.$font_size.'font-weight:bold;">Qty</td>
    <td style="'.$font_size.'font-weight:bold;">Total</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Sales:</td>
    <td style="'.$font_size.'">'.round($financial_report['sales']['total_sales_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['sales']['total_sales_price'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Rounding: </td>
    <td style="'.$font_size.'">'.$financial_report['sales']['total_rounding_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['sales']['total_rounding_price'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'"></td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'"><hr></td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">TOTAL: </td>
    <td style="'.$font_size.'font-weight:bold;">'.$this->number_format_result($financial_report['sales']['gross_total_amt'],2).'</td>
    </tr>';
    /* Sales View End */  

    /* payment Type Start */             
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">PAYMENT TYPES</td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Cash:</td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['cash_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['cash_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Eftpos: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['eftpos_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['eftpos_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Amex: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['amex_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['amex_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Diners: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['diners_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['diners_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Other: </td>
    <td style="'.$font_size.'">'.$financial_report['payment_types']['other_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['other_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'"></td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'"><hr></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">TOTAL: </td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['payment_types']['total_amt'],2).'</td>
    </tr>';
    /* payment Type End */

    /* Cash Balance Start */             
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">CASH BALANCE</td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    <td style="'.$font_size.'font-weight:bold;"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Cash Sales:</td>
    <td style="'.$font_size.'">'.$financial_report['cash_balance']['cash_sales_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['cash_sales_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Less Eftpos Cashout:</td>
    <td style="'.$font_size.'">'.$financial_report['cash_balance']['less_eftpos_cashout_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['less_eftpos_cashout_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Less Pickup Cash:</td>
    <td style="'.$font_size.'">'.$financial_report['cash_balance']['less_pickup_cash_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['less_pickup_cash_amt'],2).'</td>
    </tr>
    <tr><td colspan="2" style="'.$font_size.'">Less Paidout Cash: </td><td style="'.$font_size.'">'.$financial_report['cash_balance']['less_paidout_cash_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['less_paidout_cash_amt'],2).'</td></tr>
    <tr><td colspan="2" style="'.$font_size.'"></td><td style="'.$font_size.'"></td><td style="'.$font_size.'"><hr></td></tr>
    <tr><td colspan="2" style="'.$font_size.'">CASH NET: </td><td style="'.$font_size.'"></td><td style="'.$font_size.'">'.$this->number_format_result($financial_report['cash_balance']['total_cash_balance'],2).'</td></tr>';

    /*Cash Balance End */
               
    $html .='</table>
    </td>
    <td colspan="4" style="vertical-align:top" valign="top">
    <table border="0" cellpadding="2" cellspacing="0" width="100%" >';
    /* TOTALS Start */    
    $html .='<tr>
    <td colspan="3" style="'.$font_size.'font-weight:bold;">TOTALS</td>
    <td style="'.$font_size.'font-weight:bold;">Total</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Sales Less Gst:</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['sales_less_gst'],2).'</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Total Gst Levied: </td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['total_gst_levied'],2).'</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Non-Gst Product Sales: </td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['non_gst_product_sales'],2).'</td>
    </tr>
    <tr>
    <td colspan="3" style="'.$font_size.'">Gst Product Sales: </td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['totals']['gst_product_sales'],2).'</td>
    </tr>';
    /* TOTALS End */    

    /* COUNTERS Start */             
    $html .='<tr>
    <td colspan="2" style="'.$font_size.'font-weight:bold;">COUNTERS</td>
    <td style="'.$font_size.'font-weight:bold;">Qty</td>
    <td style="'.$font_size.'font-weight:bold;">Total</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Cancelled Sales:</td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_cancelled_sales_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_cancelled_sales_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Void/Cancel Items:</td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_cancelled_item_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_cancelled_item_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Refund Items: </td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_refund_item_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_refund_item_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Promo Discounts: </td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_promo_disc_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_promo_disc_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Pos Markdowns:  </td>
    <td style="'.$font_size.'">'.round($financial_report['counters']['total_pos_markdown_qty'],1).'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_pos_markdown_amt'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Customers:  </td>
    <td style="'.$font_size.'">'.$financial_report['counters']['total_customers'].'</td>
    <td style="'.$font_size.'"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Avg Items Per Customer:  </td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['avg_items_per_customer'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Avg Spend Per Customer: </td>
    <td style="'.$font_size.'"></td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['avg_spend_per_customer'],2).'</td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total No Sales:</td>
    <td style="'.$font_size.'">'.$financial_report['counters']['total_no_sales_qty'].'</td>
    <td style="'.$font_size.'"></td>
    </tr>
    <tr>
    <td colspan="4" style="'.$font_size.'"></td>
    </tr>
    <tr>
    <td colspan="2" style="'.$font_size.'">Total Keyed Open Sales: </td>
    <td style="'.$font_size.'">'.$financial_report['counters']['total_keyed_open_sales_qty'].'</td>
    <td style="'.$font_size.'">'.$this->number_format_result($financial_report['counters']['total_keyed_open_sales_amt'],2).'</td>
    </tr>
    </table>
    </td>';
            
    /*Cash Balance End */

    $html .= '</tr></table></body></html>';
	//$is_pdf = 1; 
    if($is_pdf == 1){
        /* html to pdf conversion */
               
        ini_set('upload_max_filesize', '100M');  
        ini_set('max_execution_time', 3000);
        ini_set('post_max_size', '500M');
        ini_set("memory_limit","256M");
        /* html to pdf conversion */
        //header('Content-type: text/html'); 
        ob_start();
        require_once('core/tcpdf/examples/tcpdf_include.php');
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Coyote');
        // set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        //$pdf->AddPage();
        $pdf->AddPage( 'L', 'LETTER' );
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // Print text using writeHTMLCell()
        //$html = addslashes($html);
            
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------
        $pdfCreate = $pdf->Output(BASEPATH.'/../pdf/finacial_summary.pdf', 'F');
        
        $filename = 'finacial_summary.pdf'; 
        $status = 1;
        $filepath = BASEPATH.'/../pdf/finacial_summary.pdf';
                
    }else{
		// header('Content-type: text/html'); // To rendering HTML View
		//echo $html;die;            
        $_response['pdf_name'] = 'finacial_summary.pdf';
        $_response['sales_result'] = $html;
        $_response['result_code'] = 1;
        $_response['message'] = $this->_common->langText($this->_locale,'txt.success.report_created');
                
    }
    return $_response;
}

    
    /**
	 * Function used to get order detail wit respect to supplier
	 * @param int $user_id
	 * @return array
	 * @access public
	 */ 
    public function member_orders() {
		
        $user_id = $this->_common->test_input($_REQUEST['user_id']);
        $offset  = $this->_common->test_input($_REQUEST['offset']);
		$limit 	 = $this->_common->test_input($_REQUEST['limit']);
		$order_status_type  = $this->_common->test_input($_REQUEST['order_status_type']);
		// set pagination limit and offset values
		$limit = (!empty($limit)) ? $limit : 10;
		$offset = (!empty($offset)) ? $offset : 0;
     
		$_response['result_code'] = 0;
		//$_response['message'] = '';
		$_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_order_found');
        if (empty($user_id)) {
            $_response['message']   = $this->_common->langText($this->_locale,'txt.error.valid_user');
        } else {
            // checking user email is already exists or not
            $user_query = "select * from ".$this->user_table." where USER_NUMBER = ".$user_id; 
            $get_user_data = $this->_mssql->my_mssql_query($user_query,$this->con);
            if( $this->_mssql->pdo_num_rows_query($user_query,$this->con) > 0 ) {
                // get user row data
                $user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
                // initialize zone outlet array
                $user_outlets = array();
                // get associated zone results from user's zone
                $outlet_query = '';
                
                $user_zone = trim($user_row->USER_ZONE);
                $outlet_id = trim($user_row->USER_OUTLET);
                $user_outlets = array();
                $user_outlet_name = array();
              
				if(!empty($outlet_id)) {
                    $outlet_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_OUTLET in ('".$outlet_id."') ORDER BY OUTL_DESC ASC";
                } else if(!empty($user_zone)) {
                    $outlet_query = "SELECT CODE_KEY_NUM as outlet_id ,OUTL_DESC as outlet_name FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = '".$user_zone."'";
                } else {
					$outlet_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_STATUS = 'Active' and SubString(OUTL_DESC, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY OUTL_DESC ASC";
				}
             
                if(!empty($outlet_query)){
                    $get_outlet_data = $this->_mssql->my_mssql_query($outlet_query,$this->con );
                    if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
                        while($row = $this->_mssql->mssql_fetch_object_query($get_outlet_data)) {
                            // prepare outlet values
                            $user_outlets[] = $row->outlet_id;
                            $user_outlet_name[$row->outlet_id] = $row->outlet_name;
                        }
                    }
                   
                    $ord_array = array();
                    if(!empty($user_outlets)){
                        $user_outlets_ids = implode(',',$user_outlets);
                        // get total count of records
						$order_count_query = "SELECT count(APPORD_ORD_ID) as order_count FROM APP_ORDERS WHERE APPORD_OUTLET IN ( $user_outlets_ids )";
						if(!empty($order_status_type)) {
							$order_count_query .= " AND APPORD_STATUS = $order_status_type ";
						}
						$order_count_data = $this->_mssql->my_mssql_query($order_count_query,$this->con);
                        if( $this->_mssql->pdo_num_rows_query($outlet_query,$this->con) > 0 ) {
							$order_count = $this->_mssql->mssql_fetch_object_query($order_count_data);
						}
                        
                        
						$sql = "SELECT * FROM ( SELECT APPORD_ORD_ID,APPORD_OUTLET,APPORD_DATE,APPORD_MODIFIED_DATE,APPORD_TRX_AMT,APPORD_STATUS,APPORD_NOTES,APPORD_PICKUP_DATE, ROW_NUMBER() OVER (ORDER BY APPORD_MODIFIED_DATE DESC) as row FROM APP_ORDERS WHERE APPORD_OUTLET IN ( $user_outlets_ids ) ";
						if(!empty($order_status_type)) {
							$sql .= " AND APPORD_STATUS = $order_status_type ";
						}
						$sql .= " ) a WHERE row > $offset AND row <= $limit";
						
                        $query = $this->_mssql->my_mssql_query($sql,$this->con);
                        if($this->_mssql->pdo_num_rows_query($sql,$this->con) > 0 ){
                            
                            $i = 0;
                            while($ord_result = $this->_mssql->mssql_fetch_object_query($query)){
                                //$order_id = (isset($row->APPORD_ORD_ID) && !empty($row->APPORD_ORD_ID)) ? $row->APPORD_ORD_ID : 0;    
                                $ord_array[$i]['order_id']  	= (isset($ord_result->APPORD_ORD_ID)) ? trim($ord_result->APPORD_ORD_ID) : "";
                                $outletId = $ord_array[$i]['outlet_id']  = (isset($ord_result->APPORD_OUTLET)) ? trim($ord_result->APPORD_OUTLET) : "";
                                $ord_array[$i]['outlet_name']  	= (isset($user_outlet_name[$outletId])) ? trim($user_outlet_name[$outletId]) : "";
                                $ord_array[$i]['order_date'] 	= (isset($ord_result->APPORD_DATE)) ? date('d-m-Y',strtotime($ord_result->APPORD_DATE)) : "";
								$ord_array[$i]['modified_date'] = (isset($ord_result->APPORD_MODIFIED_DATE)) ? date('d-m-Y',strtotime($ord_result->APPORD_MODIFIED_DATE)) : "";
                                $ord_array[$i]['order_amount']	= (isset($ord_result->APPORD_TRX_AMT)) ? trim($ord_result->APPORD_TRX_AMT) : "";
                                $ord_array[$i]['order_notes']	= (isset($ord_result->APPORD_NOTES)) ? trim($ord_result->APPORD_NOTES) : "";
                                $ord_array[$i]['order_pickup_date']	= (isset($ord_result->APPORD_PICKUP_DATE)) ? date('d-m-Y',strtotime($ord_result->APPORD_PICKUP_DATE)) : "";
                                // set order status
                                $order_status  = (isset($ord_result->APPORD_STATUS)) ? trim($ord_result->APPORD_STATUS) : "";
                                if($order_status == 2) {
                                    $status = 'In Progress';
                                } else if($order_status == 3) {
                                    $status = 'Pick Up';
                                } else {
                                    $status = 'Sent';
                                }
                                 $ord_array[$i]['order_status']  = $status;                                    
                                 $ord_array[$i]['order_status_type']  =  $order_status;                                    
                             $i++;   
                            }
                            if(!empty($ord_array)){
                                $_response['total_count'] = (isset($order_count->order_count)) ? $order_count->order_count : 0;
                                $_response['message'] = '';
                                $_response['result_code'] = 1;
								$_response['result'] = $ord_array;
                            }
                        } else {
							 $_response['message'] = $this->_common->langText($this->_locale,'txt.stock.error.no_order_found');
						}
                    }
                }
            }else{
                $_response['message'] = $this->_common->langText($this->_locale,'txt.error.valid_user');
            }
        }
        return $_response;
    }
    
	/**
	 * Function used to get order's details
	 * @param int cart_id
	 * @return array
	 * @access public
	 * 
	 */
	 public function view_order() {
		
		// get post data
		$order_id  = $this->_common->test_input($_REQUEST['order_id']);
		// set default response 
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.no_record_found');  
		if(!empty($order_id)) {
			// get order's result
			$ord_query = "SELECT APP_ORDERS.APPORD_ORD_ID,APP_ORDERS.APPORD_DATE,APP_ORDERS.APPORD_TRX_NO,APP_ORDERS.APPORD_MODIFIED_DATE,APP_ORDERS.APPORD_NOTES,APP_ORDERS.APPORD_PICKUP_DATE,APP_ORDERS.APPORD_STATUS,APP_ORDERS.APPORD_TRX_AMT,APP_ORDERS.APPORD_USER_ID,APP_ORDERS.APPORD_OUTLET,OutlTbl.OUTL_DESC as outlet_name FROM ".$this->app_order_table." LEFT JOIN OutlTbl on OutlTbl.OUTL_OUTLET = APP_ORDERS.APPORD_OUTLET  WHERE APP_ORDERS.APPORD_ORD_ID = '".$order_id."'";
			$ord_result = $this->_mssql->my_mssql_query($ord_query,$this->con);
			if( $this->_mssql->pdo_num_rows_query($ord_query,$this->con) > 0 ) {
				
				// set order result
				$ord_result = $this->_mssql->mssql_fetch_object_query($ord_result);
				// set order values
				$ord_array['order_id']  	= (isset($ord_result->APPORD_ORD_ID)) ? trim($ord_result->APPORD_ORD_ID) : "";
				$ord_array['order_date'] 	= (isset($ord_result->APPORD_DATE)) ? date('d-m-Y',strtotime($ord_result->APPORD_DATE)) : "";
				$ord_array['modified_date'] = (isset($ord_result->APPORD_MODIFIED_DATE)) ? date('d-m-Y',strtotime($ord_result->APPORD_MODIFIED_DATE)) : "";
				$ord_array['order_amount']	= (isset($ord_result->APPORD_TRX_AMT)) ? number_format($ord_result->APPORD_TRX_AMT, 2, '.', '') : "";
				$ord_array['order_notes']	= (isset($ord_result->APPORD_NOTES)) ? trim($ord_result->APPORD_NOTES) : "";
				$ord_array['order_pickup_date']	= (isset($ord_result->APPORD_PICKUP_DATE)) ? date('d-m-Y H:i:s',strtotime($ord_result->APPORD_PICKUP_DATE)) : "";
                $ord_array['outlet_name']   = (isset($ord_result->outlet_name)) ? trim($ord_result->outlet_name) : "";
                $ord_array['outlet_id']     = (isset($ord_result->APPORD_OUTLET)) ? trim($ord_result->APPORD_OUTLET) : "";
                $ord_array['order_trx_no']	= (isset($ord_result->APPORD_TRX_NO)) ? $ord_result->APPORD_TRX_NO : "";
				$customer_id = $ord_array['customer_id']	= (isset($ord_result->APPORD_USER_ID)) ? trim($ord_result->APPORD_USER_ID) : "";
                /* Customer Detail For user table from mysql */
                if(!empty($customer_id)){
                    $selectuser = "SELECT firstname,lastname,mobile FROM ".$this->user_account." WHERE id = '$customer_id'";
                    $resultuser = $this->_db->my_query($selectuser);
                    if ($this->_db->my_num_rows($resultuser) > 0) { 
                        $row = $this->_db->my_fetch_object($resultuser);
                        $ord_array['customer_name'] = $row->firstname.' '.$row->lastname;
                        $ord_array['customer_mobile'] = $row->mobile;
                        
                    }
                }
				// set order status
				$order_status  = (isset($ord_result->APPORD_STATUS)) ? trim($ord_result->APPORD_STATUS) : "";
				if($order_status == 2) {
					$status = 'In Progress';
				} else if($order_status == 3) {
					$status = 'Pick Up';
				} else {
					$status = 'Sent';
				}
				$ord_array['order_status']  = $status;
				
				// get order's item result
				$ord_item_query  = "SELECT * FROM ".$this->app_order_items_table." WHERE APPORDITM_ORD_ID = '".$order_id."'";
				$ord_item_result = $this->_mssql->my_mssql_query($ord_item_query,$this->con);
				if( $this->_mssql->pdo_num_rows_query($ord_item_query,$this->con) > 0 ) {
					$order_items = array();
					while( $result = $this->_mssql->mssql_fetch_object_query($ord_item_result) ) {
						
						// set order product values
						$item['prod_number']	= (isset($result->APPORDITM_PRODUCT)) ? trim($result->APPORDITM_PRODUCT) : "";
						$item['prod_desc'] 		= (isset($result->APPORDITM_PRODUCT_DESC)) ? trim($result->APPORDITM_PRODUCT_DESC) : "";
						$item['prod_pos_desc'] 	= (isset($result->APPORDITM_PRODUCT_POS_DESC)) ? $result->APPORDITM_PRODUCT_POS_DESC : "";
						$item['prod_qty'] 		= (isset($result->APPORDITM_QTY)) ? trim($result->APPORDITM_QTY) : "";
						$item['prod_price']		= (isset($result->APPORDITM_BASE_AMT)) ? trim($result->APPORDITM_BASE_AMT) : "";
						$item['prod_total_price'] = (isset($result->APPORDITM_AMT)) ? trim($result->APPORDITM_AMT) : "";
						$item['prod_image']	= IMG_URL."No_Image_Available.png";
						$order_items[] = $item;
					}
					$ord_array['order_items'] = $order_items;
					// prepare success response
					$_response['result_code'] = 1;
					$_response['message'] 	  = '';
					$_response['results']     = $ord_array;
				}
			}
		} else {
           	$_response['message']	 = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_order_id');         
        }
		return $_response;
	 }
     
     /**
	 * Function used to get Update order status
	 * @param int order_id
	 * @param int customer_id
	 * @param int status
	 * @return array
	 * @access public
	 * 
	 */
	public function change_order_status(){
		
        // get post data
		$order_id  = $this->_common->test_input($_REQUEST['order_id']);
        $customer_id  = $this->_common->test_input($_REQUEST['customer_id']);   
        $status  = $this->_common->test_input($_REQUEST['status']);   
        // set default response 
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.no_record_found');  
        
        if(!empty($order_id)) {
            if(!empty($customer_id) && !empty($status)){
				$ord_modified_date_time = date('M d Y h:i:s A');
                $updateSql = "UPDATE ".$this->app_order_table." SET APPORD_STATUS = '$status', APPORD_MODIFIED_DATE = '$ord_modified_date_time' WHERE APPORD_ORD_ID='$order_id'";
                    if($this->_mssql->my_mssql_query($updateSql,$this->con)) {
                        /* Push notification to customer */
						$selectuser = "SELECT device_token,device_type FROM ".$this->user_account." WHERE id = '$customer_id'";
						$resultuser = $this->_db->my_query($selectuser);
                        if ($this->_db->my_num_rows($resultuser) > 0) { 
                            $row = $this->_db->my_fetch_object($resultuser);
                            $device_type = $row->device_type;
                            $device_id = $row->device_token;
                            
                            $order_status  = $status;
                            if($order_status == 2) {
                                $status_string = 'In Progress';
                            } else if($order_status == 3) {
                                $status_string = 'Pick Up';
                            } else {
                                $status_string = 'Sent';
                            }
                            if(!empty($device_id)) {
								if($device_type == 'android'){
									$androidPushArray[$device_id]['device_id'] = $device_id;
									$androidPushArray[$device_id]['order_id'] = $order_id;
									$androidPushArray[$device_id]['order_description'] = $status_string;
									$androidPushArray[$device_id]['push_type'] = 4;
									$this->_push->sendAndroidPushNotification_order($androidPushArray);
								}else{                              
									$iosPushArray[$device_id]['device_id'] = $device_id;
									$iosPushArray[$device_id]['order_id'] = $order_id;
									$iosPushArray[$device_id]['order_description'] = $status_string;
									$iosPushArray[$device_id]['push_type'] = 4;
									$this->_push->sendIphonePushNotification_order($iosPushArray);     
								}
							}
                        }                          
                        $_response['result_code'] = 1;
                        $_response['message']	 = $this->_common->langText($this->_locale,'txt.ordering.success.status_changed');     
                    }else{
                        $_response['message']	 = $this->_common->langText($this->_locale,'txt.ordering.error.status_not_changed');     
                    }
            }else{
                $_response['message']	 = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_customer_id');     
            }
        }else{
            $_response['message']	 = $this->_common->langText($this->_locale,'txt.ordering.error.invalid_order_id');     
        }
        
        return $_response;
     }
     
	/**
	 * Function used to get Number formate with no. type
	 * @param int number
	 * @param int digit
	 * @access public
	 * 
	 */
	public function number_format_result($number = '',$digit = 2){
		$result = number_format($number,$digit, '.', '');  
		return $result;
	}
      
	/**
	 * Function used to update user's device details
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function update_device_details() {
		
		// set post params
        $user_number= $this->_common->test_input($_REQUEST['user_id']);
		$device_type= $this->_common->test_input($_REQUEST['device_type']);
		$device_id 	= $this->_common->test_input($_REQUEST['device_id']);
        
		$datetime  = date('Y-m-d H:i:s');
		// set default response for invalid request
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.device_details');
		if(!empty($user_number) && !empty($device_type) && !empty($device_id)) {
			// update member's device details
			$update_array = array("device_type" => $device_type,"device_id" => $device_id,"modified_date" =>$datetime);
			$where_clause = array("supplier_id" => $user_number);
			$this->_db->UpdateAll($this->supplier_device, $update_array, $where_clause, "");
			// set success response 
			$_response['result_code'] = 1;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.register.success.device_details');		
		}
		return $_response;
	}
    
    /**
     * Function used to authenticate session token
     * @access public
     * @return array
     */
    public function check_active_session_token() {
		// return response in case of expired session token
		$_response['result_code'] = 3;
		$_response['message'] = "Login session expired!";
		return $_response;	
	}

} // End supplier class
?> 
