<?php
if (!defined('BASEPATH') || ACCESSKEY != '367534gfyh78f265262a4221338e5dda35') {
    exit('No direct script access allowed');
}
class sync_mssql {
    public function __construct() {
        $this->_db 		= env::getInst();
        $this->_config  = config::getInst();
        $this->_mssql = new mssql(); // Create instance of mssql class
		$this->_common = new commonclass(); // Create instance of commonclass
    }
    public function test($result) {
		print_r($result);
		die;
	} 
	
	/*
	 * @sync_mssql_user() function call from @register_user() to insert user in mssql server 
	 * $result = array('customer_id' => '21786', 'ACC_FIRST_NAME' => 'ashish', 'ACC_SURNAME' => 'sabestian','ACC_EMAIL' => 'aaa1.cdn@gmail.com','ACC_MOBILE' => '9087654321','ACC_DATE_OF_BIRTH' => '1990-12-10','ACC_ADDR_1' => '','ACC_POST_CODE' => '','ACC_GENDER' => 1,'country' => '','session_token' => '5654741c6981e','user_barcode' => 'http://192.168.0.108/coyote_dev/uploads/user_barcodes/21786302.png','profile_pic' => 'http://192.168.0.108/coyote_dev/uploads/user_images/user.jpg', 'ACC_DATE_ADDED' => '2015-11-25 00:28:41','is_push_send' => 0);
	 * */
    function sync_mssql_user($result) {
		$current_date 		= CURRENT_TIME;
		//get data to update in mmsql ACCOUNT table.
		$email 				= (isset($result['ACC_EMAIL'])) ? trim($result['ACC_EMAIL']) : ""; 
		$ACC_PASSWORD 		= (isset($result['ACC_PASSWORD'])) ? trim($result['ACC_PASSWORD']) : ""; 
		$ACC_FIRST_NAME 	= (isset($result['ACC_FIRST_NAME'])) ? trim($result['ACC_FIRST_NAME']) : "";
		$ACC_SURNAME 		= (isset($result['ACC_SURNAME'])) ? trim($result['ACC_SURNAME']) : "";
		$ACC_MOBILE 		= (isset($result['ACC_MOBILE'])) ? trim($result['ACC_MOBILE']) : "";
		$ACC_DATE_OF_BIRTH 	= (isset($result['ACC_DATE_OF_BIRTH'])) ? trim($result['ACC_DATE_OF_BIRTH']) : "";
		$ACC_ADDR_1 		= (isset($result['ACC_ADDR_1'])) ? trim($result['ACC_ADDR_1']) : "";
		$ACC_POST_CODE 		= (isset($result['ACC_POST_CODE'])) ? trim($result['ACC_POST_CODE']) : "";
		$ACC_GENDER 		= (isset($result['ACC_GENDER'])) ? trim($result['ACC_GENDER']) : "";
		$ACC_DATE_ADDED 	= (isset($current_date)) ? trim($current_date) : "";
		
		$status = '0'; // 0 means active on live mssql db 
		
		// connecting with mssql live server db
		$con = $this->_mssql->mssql_db_connection();
		
		// checking user is already exists or not
		$checkEmail = "select ACC_NUMBER from ACCOUNT where ACC_EMAIL = '$email' ORDER BY ACC_NUMBER DESC";
		$getData = $this->_mssql->my_mssql_query($checkEmail,$con);
		if($this->_mssql->pdo_num_rows_query($checkEmail,$con) > 0 ) {
			$emailrow = $this->_mssql->mssql_fetch_object_query($getData);
			return $emailrow->ACC_NUMBER;  // return ACC_NUMBER if user already have account on live server
		}
		
		// get last insert id(ACC_NUMBER)
		$sql = "select ACC_NUMBER from ACCOUNT where ACC_NUMBER =(select max( ACC_NUMBER ) from ACCOUNT )";
		$query = $this->_mssql->my_mssql_query($sql,$con);
		if($this->_mssql->pdo_num_rows_query($sql,$con) > 0 ){
			$row = $this->_mssql->mssql_fetch_object_query($query);
		}
		$lastId 		= (!empty($row->ACC_NUMBER)) ? $row->ACC_NUMBER : 0;
        if(!empty($lastId)) {
            $newInsertId 	= $lastId + 1;
            // get barcode final digit
            $barcode_digits = $this->_common->get_barcode_digits($newInsertId);
            // insert data into ACCOUNT table with incremented ACC_NUMBER
            $insertSQL = "INSERT ACCOUNT  (ACC_NUMBER, ACC_CARD_NUMBER, ACC_FIRST_NAME, ACC_SURNAME, ACC_EMAIL, ACC_MOBILE, ACC_DATE_OF_BIRTH, ACC_ADDR_1, ACC_GENDER, ACC_DATE_ADDED, ACC_STATUS) VALUES ('$newInsertId','$barcode_digits','$ACC_FIRST_NAME','$ACC_SURNAME','$email','$ACC_MOBILE','$ACC_DATE_OF_BIRTH', '$ACC_ADDR_1','$ACC_GENDER','$ACC_DATE_ADDED','$status')";
            $this->_mssql->my_mssql_query($insertSQL,$con);
            
            // syn MEMBER tbl with ACCOUNT tbl on live db
            $insertMemSQL = "INSERT MEMBER  (MEMB_NUMBER, MEMB_LOYALTY_TYPE, MEMB_ACCUM_POINTS_IND, MEMB_POINTS_BALANCE,MEMB_Exclude_From_Competitions) VALUES ('$newInsertId','0','1','0','0')";
            $this->_mssql->my_mssql_query($insertMemSQL,$con);
            
            return $newInsertId;  //return new inserted ACC_NUMBER
        } else  {
            return false;
        }
	}  // end sync_mssql_user()
	
	/*
	 * @mssql_user_status() function call from @change_user() to update user in mssql server 
	 * */
	function mssql_user_status($acc_number='') {
		$checkEmail = "select ACC_STATUS from ACCOUNT where ACC_NUMBER = '$acc_numbers' ORDER BY ACC_NUMBER DESC";
		$getData = $this->_mssql->my_mssql_query($checkEmail,$con);
		if($this->_mssql->pdo_num_rows_query($checkEmail,$con) > 0 ) { 
			$emailrow = $this->_mssql->mssql_fetch_object_query($getData);
			return $emailrow->ACC_STATUS;  // return ACC_STATUS for user
		} else {
			return 0;
		}
	} //end mssql_user_status()
	
	/*
	 * @update_mssql_user() function call from @change_user() to update user in mssql server 
	 * */
	function update_mssql_user($result,$acc_number) {
		
		//get data to update in mmsql ACCOUNT table.
		$current_date 		= CURRENT_TIME;
		$ACC_FIRST_NAME 	= (isset($result['ACC_FIRST_NAME'])) ? trim($result['ACC_FIRST_NAME']) : "";
		$ACC_SURNAME 		= (isset($result['ACC_SURNAME'])) ? trim($result['ACC_SURNAME']) : "";
		$ACC_MOBILE 		= (isset($result['ACC_MOBILE'])) ? trim($result['ACC_MOBILE']) : "";
		$ACC_DATE_OF_BIRTH 	= (isset($result['ACC_DATE_OF_BIRTH'])) ? trim($result['ACC_DATE_OF_BIRTH']) : "";
		$ACC_ADDR_1 		= (isset($result['ACC_ADDR_1'])) ? trim($result['ACC_ADDR_1']) : "";
		$ACC_POST_CODE 		= (isset($result['ACC_POST_CODE'])) ? trim($result['ACC_POST_CODE']) : "";
		$ACC_GENDER 		= (isset($result['ACC_GENDER'])) ? trim($result['ACC_GENDER']) : "";
		$ACC_DATE_CHANGE 	= (isset($current_date)) ? trim($current_date) : "";
		if($gender == ''){
			$gender = 0;
		}
		// connecting with mssql live server db
		$con = $this->_mssql->mssql_db_connection();
		
		$updateSql = "UPDATE ACCOUNT SET ACC_FIRST_NAME = '$ACC_FIRST_NAME', ACC_SURNAME = '$ACC_SURNAME', ACC_MOBILE = '$ACC_MOBILE', ACC_DATE_OF_BIRTH = '$ACC_DATE_OF_BIRTH', ACC_ADDR_1 = '$ACC_ADDR_1', ACC_POST_CODE = '$ACC_POST_CODE', ACC_GENDER = '$ACC_GENDER', ACC_DATE_CHANGE = '$ACC_DATE_CHANGE' WHERE ACC_NUMBER='$acc_number'";
		if($this->_mssql->my_mssql_query($updateSql,$con)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}//end update_mssql_user()
	
	
	function mssql_test($result) {
		$this->_sync_mssql = new sync_mssql(); 
		$result = array('customer_id' => '21786', 'ACC_FIRST_NAME' => 'ashish', 'ACC_SURNAME' => 'sabestian','ACC_EMAIL' => 'aaa1.cdn@gmail.com','ACC_MOBILE' => '9087654321','ACC_DATE_OF_BIRTH' => '1990-12-10','ACC_ADDR_1' => '','ACC_POST_CODE' => '','ACC_GENDER' => 1,'country' => '','session_token' => '5654741c6981e','user_barcode' => 'http://192.168.0.108/coyote_dev/uploads/user_barcodes/21786302.png','profile_pic' => 'http://192.168.0.108/coyote_dev/uploads/user_images/user.jpg', 'ACC_DATE_ADDED' => '2015-11-25 00:28:41','is_push_send' => 0);
		echo $this->_sync_mssql->test($result);
		die;
		$con = $this->_mssql->mssql_db_connection();
		/*$sql = "select ACC_NUMBER,ACC_EMAIL from ACCOUNT ORDER BY ACC_NUMBER DESC";
		$getData = $this->_mssql->my_mssql_query($sql,$con);
		if($this->_mssql->mssql_num_rows_query($getData) > 0 ) { 
			$emailrow = $this->_mssql->mssql_fetch_object_query($getData);
			return $emailrow->ACC_NUMBER;  // return ACC_NUMBER if user already have account on live server
		}*/
		$current_date 		= CURRENT_TIME;
		//get data to update in mmsql ACCOUNT table.
		$email 				= (isset($result['ACC_EMAIL'])) ? trim($result['ACC_EMAIL']) : ""; 
		$ACC_PASSWORD 		= (isset($result['ACC_PASSWORD'])) ? trim($result['ACC_PASSWORD']) : ""; 
		$ACC_FIRST_NAME 	= (isset($result['ACC_FIRST_NAME'])) ? trim($result['ACC_FIRST_NAME']) : "";
		$ACC_SURNAME 		= (isset($result['ACC_SURNAME'])) ? trim($result['ACC_SURNAME']) : "";
		$ACC_MOBILE 		= (isset($result['ACC_MOBILE'])) ? trim($result['ACC_MOBILE']) : "";
		$ACC_DATE_OF_BIRTH 	= (isset($result['ACC_DATE_OF_BIRTH'])) ? trim($result['ACC_DATE_OF_BIRTH']) : "";
		$ACC_ADDR_1 		= (isset($result['ACC_ADDR_1'])) ? trim($result['ACC_ADDR_1']) : "";
		$ACC_POST_CODE 		= (isset($result['ACC_POST_CODE'])) ? trim($result['ACC_POST_CODE']) : "";
		$ACC_GENDER 		= (isset($result['ACC_GENDER'])) ? trim($result['ACC_GENDER']) : "";
		$ACC_DATE_ADDED 	= (isset($current_date)) ? trim($current_date) : "";
				$status = '1';

		$newInsertId		= '2100000016';
		$insertSQL = "INSERT ACCOUNT  (ACC_NUMBER, ACC_FIRST_NAME, ACC_SURNAME, ACC_EMAIL, ACC_MOBILE, ACC_DATE_OF_BIRTH, ACC_ADDR_1, ACC_GENDER, ACC_DATE_ADDED, ACC_STATUS) VALUES ('$newInsertId','$ACC_FIRST_NAME','$ACC_SURNAME','$email','$ACC_MOBILE','$ACC_DATE_OF_BIRTH', '$ACC_ADDR_1','$ACC_GENDER','$ACC_DATE_ADDED','$status')";
		
		$getData = $this->_mssql->my_mssql_query($insertSQL,$con);
		echo $getData;
		die;
	}
    
}
?>
