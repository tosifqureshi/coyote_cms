<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author    : Tosif Qureshi
 * Timestamp : Nov-24
 * Copyright : cdnsolutionsgroup.com
 */

class synccron {
    public $_response = array();
    public $result = array();
    private $stores_table = 'ava_stores_log';
    private $user_account = 'ava_users';
	private $offer_table  = 'ava_offers';
	private $beacon_offer_table  = 'ava_beacon_offers';
	
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Mysql Database Connection instance 
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_mssql = new mssql(); // Create instance of mssql class
		$this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
    }
    
    /**
     * @Function to sync mysql user in mssql live user log
     * @access public
     * @return array
     */
    public function sync_user() {
		
		// get user whos not in mssql account table
		$sql = "SELECT id,acc_number,email,password_hash,firstname,lastname,mobile,gender,active,date_of_birth,address,post_code FROM " . $this->user_account . " WHERE acc_number IS NULL AND role_id != 1 order by id asc";
		$sqlResult = $this->_db->my_query($sql);
		
		if ($this->_db->my_num_rows($sqlResult) > 0) {
			while ($user = $this->_db->my_fetch_object($sqlResult)) {
				// connecting with mssql live server db
				$ms_con = $this->_mssql->mssql_db_connection();
				
				// get last inserted user in mssql account table
				$last_user_query = "select ACC_NUMBER from ACCOUNT ORDER BY ACC_NUMBER DESC";
				$get_last_user_data = $this->_mssql->my_mssql_query($last_user_query,$ms_con);
				
				if($this->_mssql->mssql_num_rows_query($get_last_user_data) > 0 ) {
					
					// fetch user's last account id from mssql table
					$user_row = $this->_mssql->mssql_fetch_object_query($get_last_user_data);
					// set account number
					$ms_users_number = $user_row->ACC_NUMBER; 
					if(!empty($ms_users_number)) {
						$ms_users_number = $ms_users_number+1; // increment
						// check user's email in mssql account table
						$is_exists_user = $this->check_user_existance($user->email,$ms_con);
						if(!empty($is_exists_user) && is_array($is_exists_user) && count($is_exists_user) > 0) {
							// set existing user's data
							$acc_number = (isset($is_exists_user['acc_number'])) ? $is_exists_user['acc_number'] : 0;
							$acc_card_number = (isset($is_exists_user['acc_card_number'])) ? $is_exists_user['acc_card_number'] : 0;
							// get 13 digit barcode with checksum value
							$barcode_digits = $this->_common->get_barcode_digits($is_exists_user);
							// generate barcode from account number
							$barcode = $acc_number;
							include_once("ean_13_barcode.php"); // include to generate user barcode
							$user_barcode = $image_name;
							// update account is exist status in mysql user table
							$update_array = array("is_already_exist" => 1,"acc_number" => $acc_number,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode);
							$where_clause = array("id" => $user->id);
							$update = $this->_db->UpdateAll($this->user_account, $update_array, $where_clause, "");
						} else {
							// insert user log in mssql account log 
							$account_add = $this->insert_ms_user($user,$ms_users_number,$ms_con);
							if($account_add) {
								// check member number is exist or not
								$is_member_exists = $this->check_membership_existance($ms_users_number,$ms_con);
								if($is_member_exists) {
									// syn MEMBER tbl with ACCOUNT tbl on live db
									$insert_member_query = "INSERT MEMBER  (MEMB_NUMBER, MEMB_LOYALTY_TYPE, MEMB_ACCUM_POINTS_IND, MEMB_POINTS_BALANCE, MEMB_Exclude_From_Competitions) VALUES ('$ms_users_number','0','1','0','0')";
									$this->_mssql->my_mssql_query($insert_member_query,$ms_con);
								}
							}
						}
					}	
				}
			}
		}
		return true;
	}
	
	/**
     * @Function to check mssql user exist or not
     * @input  : email, ms_con
     * @return : void 
     * @access private
     */
	private function check_user_existance($email='',$ms_con='') {

		if(!empty($email)) {
			// get user's email in mssql account table
		    $user_query = "select ACC_NUMBER,ACC_CARD_NUMBER from ACCOUNT where LOWER(ACC_EMAIL) = '$email'";
			$get_user_data = $this->_mssql->my_mssql_query($user_query,$ms_con);
			if($this->_mssql->mssql_num_rows_query($get_user_data) > 0 ) {
				$user_row = $this->_mssql->mssql_fetch_object_query($get_user_data);
				if(!empty($user_row) && isset($user_row->ACC_NUMBER)) {
					$user_data['acc_number'] = $user_row->ACC_NUMBER;
					$user_data['acc_card_number'] = $user_row->ACC_CARD_NUMBER;
					//return  true;
					return $user_data;
				}
			}
		}
		return false;
	}
    
    /**
     * @Function to check mssql user membership exist or not
     * @input  : mem_number, ms_con
     * @return : void 
     * @access private
     */
	private function check_membership_existance($mem_number='',$ms_con='') {

		if(!empty($mem_number)) {
			// get user's email in mssql account table
		    $member_query = "select MEMB_LOYALTY_TYPE from MEMBER where MEMB_NUMBER = '$mem_number'";
			$get_member_data = $this->_mssql->my_mssql_query($member_query,$ms_con);
			if($this->_mssql->mssql_num_rows_query($get_member_data) > 0 ) {
				$member_row = $this->_mssql->mssql_fetch_object_query($get_member_data);
				if(!empty($member_row) && isset($member_row->MEMB_LOYALTY_TYPE)) {
					return  false;
				}
			}
		}
		return true;
	}
	
	/**
     * @Function to insert user data in mssql account log
     * @input  : user, ms_users_number, ms_con
     * @return : void 
     * @access private
     */
	private function insert_ms_user($user='',$ms_users_number='',$ms_con='') {
		if(!empty($user) && !empty($ms_users_number)) {
			//prepare data values
			$email 			= (isset($user->email)) ? trim($user->email) : ""; 
			$firstname 	    = (isset($user->firstname)) ? trim($user->firstname) : ""; 
			$lastname 		= (isset($user->lastname)) ? trim($user->lastname) : ""; 
			$mobile 		= (isset($user->mobile)) ? trim($user->mobile) : ""; 
			$date_of_birth 	= (isset($user->date_of_birth) && $user->date_of_birth != '0000-00-00') ? trim($user->date_of_birth) : ""; 
			$address 		= (isset($user->address)) ? trim($user->address) : ""; 
			$post_code 		= (isset($user->post_code)) ? trim($user->post_code) : ""; 
			$gender 		= (isset($user->gender)) ? trim($user->gender) : ""; 
			$current_date 	= CURRENT_TIME;
			$status         = '0';
			// get barcode final digit
			$barcode_digits = $this->_common->get_barcode_digits($ms_users_number);
			// insert data into ACCOUNT table with incremented ACC_NUMBER
			$insert_account_query = "INSERT ACCOUNT  (ACC_NUMBER, ACC_CARD_NUMBER, ACC_FIRST_NAME, ACC_SURNAME, ACC_EMAIL, ACC_MOBILE, ACC_DATE_OF_BIRTH, ACC_ADDR_1, ACC_GENDER, ACC_DATE_ADDED, ACC_STATUS) VALUES ('$ms_users_number','$barcode_digits','$firstname','$lastname','$email','$mobile','$date_of_birth', '$address','$gender','$current_date','$status')";
			$this->_mssql->my_mssql_query($insert_account_query,$ms_con);
			// generate barcode from account number
			$barcode = $ms_users_number; 
			include_once("ean_13_barcode.php"); // include to generate user barcode
			$user_barcode = $image_name;
			// update account number in mysql user table
			$update_array = array("acc_number" => $ms_users_number,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode);
			$where_clause = array("id" => $user->id);
			$update = $this->_db->UpdateAll($this->user_account, $update_array, $where_clause, "");
			return true;
		}
		return false;
	}
	
	/**
	 * Function to create user's barcode
	 * @access  : public
	 * 
	 */
	public function create_user_barcode() {
		$barcode   = $this->_common->test_input($_REQUEST['barcode']);
		$account_number   = $this->_common->test_input($_REQUEST['acc_number']);
		if(!empty($barcode)) {
			$barcode_dirpath = IMG_FOLDER_PATH.'user_barcodes/'.$account_number.'.png';
			unlink($barcode_dirpath);
			// generate barcode from account number
			include_once("ean_13_barcode.php"); // include to generate user barcode
		}
		return true;
	}
	
	/**
     * @Function to sync users barcode
     * @access public
     * @return array
     */
    public function manage_user_barcode() {
		// get all mssql updated users
		$sql = "SELECT id,acc_number,email FROM " . $this->user_account . " WHERE password_hash <> '' and acc_number <> '' and user_barcode_image = ''";
		$sqlResult = $this->_db->my_query($sql);
		$i = 0;
		if ($this->_db->my_num_rows($sqlResult) > 0) {
			while ($user = $this->_db->my_fetch_object($sqlResult)) {
				$acc_number = $user->acc_number;
				$user_id = $user->id;
				if(!empty($acc_number) && !empty($user_id)) {
					$user_barcode_image = $acc_number.'.png';
					$barcode = $acc_number;
					// generate barcode from account number
					include("ean_13_barcode.php");
					$updated_at = date('Y-m-d H:i:s');
					// update barcode number  is exist status in mysql user table
					$update_array = array("user_barcode_image" => $user_barcode_image,"update_at"=>$updated_at);
					$where_clause = array("id" => $user_id);
					$update = $this->_db->UpdateAll($this->user_account, $update_array, $where_clause, "");
					
					echo '<pre>';
					print_r($user);
					$i++;
				}
			}
		}
		die;
	}
	
	/**
	 * Function to manage mssql and mysql store data
	 * @author  : Tosif Qureshi
	 * @access  : public
	 */
	 public function manage_stores() {
		 
		// connecting with mssql live server db
		$ms_con = $this->_mssql->mssql_db_connection();
		// fetch outlets
		$store_query = "select * from OutlTbl ORDER BY OUTL_DESC ASC";
		$get_store_data = $this->_mssql->my_mssql_query($store_query,$ms_con);
		if($this->_mssql->mssql_num_rows_query($get_store_data) > 0 ) {
			// fetch store info from mssql table
			while ($store = $this->_mssql->mssql_fetch_object_query($get_store_data)) {
				if(!empty($store)) {
					// set store values
					$store_name      = (isset($store->OUTL_Name_On_App)) ? trim($store->OUTL_Name_On_App) : "";
					$ms_store_id     = (isset($store->OUTL_OUTLET)) ? trim($store->OUTL_OUTLET) : "";
					$store_address_1 = (isset($store->OUTL_Address_On_App)) ? trim($store->OUTL_Address_On_App) : ""; 
					// check store id existance
					if(!empty($ms_store_id)) {
						// prepare store input values
						$store_abn 		  = "";
						$store_address_2  = (isset($store->OUTL_ADDR_2)) ? trim($store->OUTL_ADDR_2) : ""; 
						$store_post_code  = (isset($store->OUTL_POST_CODE)) ? trim($store->OUTL_POST_CODE) : ""; 
						$store_phone 	  = (isset($store->OUTL_PHONE)) ? trim($store->OUTL_PHONE) : ""; 
						$store_fax 		  = (isset($store->OUTL_FAX)) ? trim($store->OUTL_FAX) : ""; 
						$store_latitude   = (isset($store->OUTL_Latitude)) ? trim($store->OUTL_Latitude) : ""; 
						$store_longitude  = (isset($store->OUTL_Longitude)) ? trim($store->OUTL_Longitude) : ""; 
						$store_email 	  = (isset($store->OUTL_Email)) ? trim($store->OUTL_Email) : ""; 
						$open_hours 	  = (isset($store->OUTL_Open_Hours)) ? trim($store->OUTL_Open_Hours) : ""; 
						$status 	      = (isset($store->OUTL_STATUS) && $store->OUTL_STATUS == 'Active') ? 1 : 0;
						$is_online 	      = (isset($store->OUTL_App_Orders)) ? $store->OUTL_App_Orders : 0;
						$display_on_app   = (isset($store->OUTL_Display_On_App)) ? $store->OUTL_Display_On_App : 0;
						$modified_at      = date('Y-m-d h:i:s');
						// set last store modified date formate
						$last_ms_modified_date = (strtotime($store->OUTL_Last_Modified_Date) > 0) ? date('Y-m-d h:i:s',strtotime($store->OUTL_Last_Modified_Date)) : '';
						
						// get store data from store log table
						$sql = "SELECT store_id,modified_at FROM " . $this->stores_table . " WHERE ms_store_id = $ms_store_id LIMIT 1";
						$sqlResult = $this->_db->my_query($sql);
						if($this->_db->my_num_rows($sqlResult) > 0 ) {
							// fetch store data
							$store_row = $this->_db->my_fetch_object($sqlResult);
							// set last store modified date formate
							$last_modified_date = (strtotime($store_row->modified_at) > 0) ? date('Y-m-d h:i:s',strtotime($store_row->modified_at)) : '';
							// update store's log
							if(!empty($store_row)) {
								// prepare store field values
								$update_array = array("store_address_2" => $store_address_2,"store_post_code" => $store_post_code,"store_fax" => $store_fax,"status"=>$status,"modified_at"=>$modified_at,"store_email"=>$store_email,"is_online"=>$is_online,"display_on_app"=>$display_on_app);
								if(!empty($store_name)) {
									$update_array['store_name'] = $store_name;
								}
								if(!empty($store_address_1)) {
									$update_array['store_address_1'] = $store_address_1;
								}
								if(!empty($store_phone)) {
									$update_array['store_phone'] = $store_phone;
								}
								if(!empty($open_hours)) {
									$update_array['open_hours'] = $open_hours;
								}
								if(!empty($store_latitude) && !empty($store_longitude)) {
									$update_array['store_latitude'] = $store_latitude;
									$update_array['store_longitude'] = $store_longitude;
								}
								$where_clause = array("ms_store_id" => $ms_store_id);
								$this->_db->UpdateAll($this->stores_table, $update_array, $where_clause, "");
							}
						} else {
							// insert store in mysql store log
							$query = "INSERT INTO " . $this->stores_table . " (ms_store_id, store_name, store_abn, store_address_1, store_address_2, store_post_code, store_phone, store_fax, status, modified_at,store_email,store_latitude,store_longitude,open_hours,is_online,display_on_app) VALUES ('$ms_store_id', '$store_name', '$store_abn', '$store_address_1', '$store_address_2', '$store_post_code', '$store_phone', '$store_fax', '$status', '$modified_at','$store_email','$store_latitude','$store_longitude','$open_hours','$is_online','$display_on_app')";
							$this->_db->my_query($query);
						}
					}
				}
			}
		}
		return true;
	 }
	 
	 
	/**
     * @Function to check mssql user membership exist or not
     * @input : store_name , ms_store_id 
     * @return: void
     * @access: private
     */
	private function check_store_exist($store_name='',$ms_store_id=0) {
		
		if(!empty($store_name)) {
			// get store data from store log table
			$sql = "SELECT store_id FROM " . $this->stores_table . " WHERE store_name = '".$store_name."' OR ms_store_id = $ms_store_id LIMIT 1";
			$sqlResult = $this->_db->my_query($sql);
			if($this->_db->my_num_rows($sqlResult) > 0 ) {
				// fetch store data
				$store_row = $this->_db->my_fetch_object($sqlResult);
				if(!empty($store_row) && isset($store_row->store_id)) {
					return  false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Function to manage expire offer status
	 * @author  : Tosif Qureshi
	 * @access  : public
	 */
	 public function manage_offers_expiry() {
		 // set current date-time
		$current_datetime 	= CURRENT_TIME;
		$current_date 	= date('Y-m-d');
		// get instore expire offer 
		$sql = "SELECT offer_id FROM " . $this->offer_table . " WHERE status = 1 AND end_time < '".$current_datetime."'  order by offer_id asc";
		$sql_result = $this->_db->my_query($sql);
		
		if ($this->_db->my_num_rows($sql_result) > 0) {
			while ($offers = $this->_db->my_fetch_object($sql_result)) {
				if(!empty($offers->offer_id)) {
					// update instore offer status as inactive
					$update_array = array("status" => 0);
					$where_clause = array("offer_id" => $offers->offer_id);
					$this->_db->UpdateAll($this->offer_table, $update_array, $where_clause, "");
				}
			}
		}
		
		// update Beacon / VIP simple offer status as inactive for expired offer
		$offer_sql = "SELECT beacon_offer_id FROM " . $this->beacon_offer_table . " WHERE status = 1 AND end_date < '".$current_date."'  order by beacon_offer_id asc";
		$sql_offer_result = $this->_db->my_query($offer_sql);
		
		if ($this->_db->my_num_rows($sql_offer_result) > 0) {
			while ($beacon_offers = $this->_db->my_fetch_object($sql_offer_result)) {
				if(!empty($beacon_offers->beacon_offer_id)) {
					// update Beacon / VIP simple offer status as inactive
					$update_array = array("status" => 0);
					$where_clause = array("beacon_offer_id" => $beacon_offers->beacon_offer_id);
					$this->_db->UpdateAll($this->beacon_offer_table, $update_array, $where_clause, "");
				}
			}
		}
		return true;
	 }
	  
	 public function get_test_data($type='') {
	
		//echo $inc_time;die;
		$type = $_GET['apn'];
		$buy_qty = 3;
		$discount_qty = 1;
		$prod_name = 'VIP';
		$i = 0;
		// connecting with mssql live server db
		$ms_con = $this->_mssql->mssql_db_connection();
		$snw_title = 'Tesrt';
		$datetime = date('Y-m-d H:i:s');
		$member_id = 2100000094;
		$snw_id = 1;
		//$updateSql = "UPDATE OUTPTBL SET OUTP_NORM_PRICE_1 = '4.95',OUTP_NORM_PRICE_2 = '0',OUTP_NORM_PRICE_3 = '0',OUTP_NORM_PRICE_4 = '0',OUTP_CARTON_COST = '32.78',OUTP_LABEL_QTY = '1',OUTP_MIN_ONHAND = '15',OUTP_MAX_ONHAND = '0',OUTP_MIN_REORDER_QTY = '12',OUTP_SKIP_REORDER_YN = 'N',OUTP_FIFO_STOCK_YN = 'N' ,OUTP_CHANGE_LABEL_IND = 'Yes' WHERE OUTP_OUTLET = '792' and OUTP_PRODUCT = 6";
		//$this->_mssql->my_mssql_query($updateSql,$ms_con);
		//$updateSql = "UPDATE OUTPTBL SET OUTP_CHANGE_LABEL_IND = 'No' WHERE OUTP_OUTLET = '792' and OUTP_PRODUCT = 1176";
		//$this->_mssql->my_mssql_query($updateSql,$ms_con);
		//$updateSql = "DELETE FROM SECOND_CHANCE_TBL where SecCh_created_date >= '2016-11-17'";
		//$this->_mssql->my_mssql_query($updateSql,$ms_con);
		//$updateSql = "UPDATE CODETBL SET CODE_ALP_1 = 'DONE',CODE_NUM_1 = '792',CODE_DESC = 'KANGAROO POINT DEV TILL (POP TEST 2)' WHERE CODE_KEY_TYPE = 'SYNCTILL' AND CODE_KEY_NUM = '7924' AND CODE_KEY_ALP = '0'";
		//$updateSql ="INSERT INTO TRXTBL(TRX_DATE,TRX_TYPE,TRX_PRODUCT,TRX_OUTLET,TRX_TILL,TRX_SEQ,TRX_TIMESTAMP, TRX_STORE,TRX_SUPPLIER,TRX_MANUFACTURER,TRX_GROUP,TRX_DEPARTMENT,TRX_COMMODITY,TRX_CATEGORY,TRX_SUBRANGE, TRX_REFERENCE,TRX_USER,TRX_QTY,TRX_AMT,TRX_AMT_GST,TRX_COST,TRX_COST_GST,TRX_DISCOUNT,TRX_PRICE,TRX_PROM_SELL, TRX_PROM_BUY,TRX_WEEK_END,TRX_DAY,TRX_NEW_ONHAND,TRX_MEMBER,TRX_POINTS,TRX_CARTON_QTY,TRX_UNIT_QTY,TRX_PARENT, TRX_STOCK_MOVEMENT,TRX_TENDER,TRX_MANUAL_IND,TRX_GL_ACCOUNT,TRX_GL_POSTED_IND,TRX_PROM_SALES,TRX_PROM_SALES_GST, TRX_DEBTOR,TRX_FLAGS,TRX_REFERENCE_TYPE,TRX_REFERENCE_NUMBER,TRX_Terms_Rebate_Code,TRX_Terms_Rebate, TRX_Promo_Scan_Rebate_Code,TRX_Promo_Scan_Rebate,TRX_Promo_Purchase_Rebate_Code,TRX_Promo_Purchase_Rebate) VALUES('Oct 19 2016 09:53:36 PM','ADJUSTMENT','1013011','792',0,0,'Oct 19 2016 09:53:36 PM','792','COKE', '38081','1','11','201','38',0,0,'79201','0',0,0, '2.7316666666667','0.24833333333333',0,0,0,0,'2016-10-23','Wed','227',0,0,'12','1',0,'0','RECOUNT','','','No',0,0,0,0,'','','',0,'',0,'',0)";
		//$this->_mssql->my_mssql_query($updateSql,$ms_con);
		// get user's email in mssql account table
		switch($type) {
			case 1: 
				$member_query = "select distinct Prod_Desc, Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT where OUTP_STATUS = 'active' and Prod_Desc like '%".$prod_name."%'";
			break;
			case 2: 
				$member_query= 'SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY ATRX_DATE) as row FROM ACCOUNT_TRXTBL where ATRX_MEMBER = 2000000211) a WHERE row > 1 and row <= 10';
			break;
			case 3: 
				$member_query = 'select top 20 * from JNLHTBL where JNLH_OUTLET = 998 order by JNLH_YYYYMMDD desc';
			break;
			case 4: 
				$member_query = 'select top 20 * from JNLDTBL where JNLD_OUTLET = 998 order by JNLD_YYYYMMDD desc, JNLD_HHMMSS desc';
				
			break;
			case 5: 
				$member_query = 'select top 20 * from TRXTBL where TRX_Store = 998 order by TRX_TIMESTAMP desc';
			break;
			case 6:
				$member_query = 'select ATRX_DATE,ATRX_MEMBER,ATRX_PRODUCT from ACCOUNT_TRXTBL where ATRX_ACCOUNT = 2100000827 order by ATRX_DATE desc';
			break;
			case 7:
				$member_query = "select Cast(Sum(ATRX_QTY) as Integer)  % (".$buy_qty." + ".$discount_qty.") from ACCOUNT_TRXTBL where ATRX_ACCOUNT = 2100000011 and ATRX_PRODUCT in (79111,79540) and ATRX_DATE between '20160320' and '20160420' and ATRX_TYPE = 'ITEMSALE' having Sum(ATRX_QTY) > 0";
			break;
			
		}
		$member_query = "select * from PRMHTBL where PRMH_STATUS = 'Active' and PRMH_PROMOTION_TYPE in ('SELLING', 'MIXMATCH', 'OFFER') and GetDate() between PRMH_START and PRMH_END";
		
		//$member_query = "SELECT * FROM ( SELECT *,  ROW_NUMBER() OVER (ORDER BY ORDH_ORDER_NO) as row FROM OrdhTbl where ORDH_POSTED_DATE between '20160520' and '20160628') a WHERE row > 0 AND row <= 10";	
		//$member_query = "SELECT * FROM OrdhTbl where ORDH_SUPPLIER_NAME = 'METCASH' and ORDH_POSTED_DATE between '20160607' and '20160630'";
		$member_query = "SELECT * FROM OrdlTbl where ORDL_ORDER_NO = 150422";
		//$member_query = "SELECT * FROM PrintLabelFromTabletTbl where PLT_Print_Batch between '20150520' and '20160630'";
		
		//$member_query = 'select top 20 * from JNLDTBL where JNLD_OUTLET = 998 order by JNLD_YYYYMMDD desc, JNLD_HHMMSS desc';
		$member_query = "select * from PrmpTbl where PRMP_PROM_CODE = 'PROMO-TEST-02'";
		$member_query = "select PRMP_PRODUCT, * from PRMPTBL 
		join PRMHTBL on PRMP_PROM_CODE = PRMH_PROM_CODE
		where GetDate() between PRMH_START and PRMH_END 
		and PRMH_STATUS = 'Active'
		and PRMP_STATUS = 'Active'
		and (PRMH_OUTLET_ZONE in 
		(select distinct CODE_KEY_ALP from CodeTbl 
		where CODE_KEY_TYPE = 'ZONEOUTLET'
		and CODE_KEY_NUM = 792) 
		and PRMP_PRODUCT in (7636))";
		//$member_query = "select PRMP_PRODUCT, PRMP_PROM_CODE,PRMP_OFFER_GROUP,PRMH_PROMOTION_TYPE from PRMPTBL join PRMHTBL on PRMP_PROM_CODE = PRMH_PROM_CODE where PRMH_PROMOTION_TYPE in ('SELLING', 'MIXMATCH', 'OFFER') and (PRMH_OUTLET_ZONE in (select distinct CODE_KEY_ALP from CodeTbl where CODE_KEY_TYPE = 'ZONEOUTLET' ) and PRMP_PRODUCT in (7636)";
		//$member_query = "select CODE_KEY_NUM, CODE_DESC from CodeTbl where code_key_num = 792 and CODE_KEY_TYPE = 'STOCKTAKE'";
		$member_query = "SELECT sum(ORDL_CARTON_COST) FROM OrdlTbl where ORDL_ORDER_NO = 5839";
		$member_query = "select USER_NUMBER from USERTBL where USER_OUTLET = '725'";
		$member_query = "SELECT OUTL_OUTLET as outlet_id ,OUTL_DESC as outlet_name FROM OutlTbl WHERE OUTL_OUTLET in ('725') ORDER BY OUTL_DESC ASC";

		$member_query = "SELECT CODE_KEY_ALP as zone,USER_NUMBER FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET  JOIN USERTBL on USER_ZONE = CODE_KEY_ALP WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_NUM = '725'";
		$member_query = "select * from CodeTbl where CODE_KEY_NUM  = 768 and CODE_KEY_TYPE = 'STOCKTAKEI'"; // get products
		$member_query = "select CODE_KEY_ALP, CODE_DESC,  CODE_NUM_1 FROM CodeTbl WHERE CODE_KEY_NUM = 793 AND CODE_KEY_TYPE in ( 'STOCKTAKE' , 'STOCKTAKEI')"; // get outlet 
		
		//$member_query = "select CODE_KEY_NUM, CODE_DESC, CODE_ALP_1 from CodeTbl where code_key_num in (AAA, BBB, CCC) and CODE_KEY_TYPE = 'STOCKTAKE'";
		//$member_query = "select OUTP_QTY_ONHAND from OutpTbl where Outp_Outlet = 792 and OUTP_PRODUCT = 1621092";
		//$member_query = "select OUTP_QTY_ONHAND, (OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost from OutpTbl join PRODTBL on Prod_Number = OUTP_PRODUCT where Outp_Outlet = 792 and OUTP_PRODUCT = 1381207"; // unit cost
		/* // Which value maps to what column 
		CODE_KEY_TYPE = 'STOCKTAKEI'
		CODE_KEY_NUM = 792 (Outlet Number), 
		CODE_KEY_ALP = Prod_Number,
		CODE_DESC = Prod_Desc, 
		CODE_NUM_1 = Prod_Number,
		CODE_NUM_2 = Item Count,
		CODE_NUM_3 = Line Total (Unit cost * Item Count),
		CODE_NUM_4 = OUTP_QTY_ONHAND (Stock on Hand Value),
		CODE_NUM_5 = Variance qty, 
		CODE_NUM_6 = unit cost, 
		CODE_NUM_7 = Variance cost (Unit cost * Variance qty), 
		CODE_NUM_8 = Item Count	*/

		$member_query = "select * from APP_ORDERS where APPORD_OUTLET IN ('792')";
		$member_query = "SELECT * FROM ( SELECT Prod_Number, Prod_Desc, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from PRODTBL WHERE  PROD_STATUS = 'Active' and PROD_TYPE = 'InStore' ) a WHERE row > 0 AND row <= 10";
		$member_query= "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM PRODTBL where PROD_STATUS = 'Active' and PROD_TYPE = 'InStore') a WHERE row > 0 and row <= 10";
						
		$member_query = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from PRODTBL where PROD_STATUS = 'Active' and PROD_NUMBER = 1357591 ) a WHERE row > 0 and row <= 10";
        $member_query = "SELECT * FROM OutpTbl JOIN OutlTbl ON OUTL_OUTLET = OUTP_OUTLET where OUTP_STATUS = 'Active' and OUTP_PRODUCT = '1013011'";        
		$member_query = "select OUTP_SUPPLIER, OUTP_BUY_PROM_CODE, OUTP_PROM_CTN_COST, (OUTP_CARTON_COST / PROD_CARTON_QTY) * PROD_UNIT_QTY as ItemCost,* from OUTPTBL join ProdTbl on OUTP_PRODUCT = PROD_NUMBER where OUTP_OUTLET = 780 and OUTP_PRODUCT =  3069";
		
		$member_query = "SELECT * FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number where OUTP_OUTLET = 792 AND Prod_Number = 7617";
		$member_query = "SELECT * FROM ACCOUNT_TRXTBL  WHERE ATRX_TYPE = 'ITEMSALE' AND ATRX_QTY > 1 ORDER BY ATRX_DATE DESC ";		
        $member_query = "select top 20 * from JNLDTBL where JNLD_OUTLET = 792 AND JNLD_TRX_NO =356 AND JNLD_TILL = 7923 AND JNLD_TYPE='SALE' order by JNLD_YYYYMMDD desc, JNLD_HHMMSS desc";
        //$member_query = "select top 3 * from JNLHTBL where JNLH_OUTLET = 792  AND  JNLH_TYPE = 'SALE' order by JNLH_TIMESTAMP DESC";
        //$member_query = "select top 20 JNLHTBL.*,JNLDTBL.* from JNLHTBL LEFT JOIN JNLDTBL ON JNLHTBL.JNLH_TRX_NO = JNLDTBL.JNLD_TRX_NO WHERE  JNLHTBL.JNLH_TRX_NO = 357 order by JNLDTBL.JNLD_YYYYMMDD desc";
        //$member_query = "select JNLDTBL.*,JNLDTBL.JNLD_QTY as purchase_count, JNLHTBL.JNLH_TRX_NO as TRX_NO,JNLHTBL.JNLH_TIMESTAMP as last_order_date from JNLHTBL JOIN JNLDTBL ON JNLHTBL.JNLH_TRX_NO = JNLDTBL.JNLD_TRX_NO AND JNLHTBL.JNLH_YYYYMMDD = JNLDTBL.JNLD_YYYYMMDD AND JNLHTBL.JNLH_TILL = JNLDTBL.JNLD_TILL WHERE  JNLHTBL.JNLH_TRX_NO = 356 AND JNLDTBL.JNLD_TYPE='SALE' order by JNLDTBL.JNLD_YYYYMMDD desc";
        /* Query For Target Notification */
        $member_query = "select JNLD_PRODUCT,JNLD_TRX_NO from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TYPE = 'MEMBER'   "; // Direct Fetch Member Details 
        $member_query = "select SUM(JNLD_AMT),SUM(JNLD_QTY),JNLD_TRX_NO from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TYPE = 'SALE' group by JNLD_TRX_NO "; // Direct Fetch Member Details 
        $member_query = "select SUM(JNLD_AMT) as SUM_AMT,SUM(JNLD_QTY) as SUM_QTY,JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TYPE = 'SALE' GROUP BY JNLD_TRX_NO HAVING SUM(JNLD_QTY) >= 7 "; // TRANSACTION DETAIL WITH RESPCT  TO PRODUCT COUNT 
        $member_query = "select SUM(JNLD_AMT) as SUM_AMT,SUM(JNLD_QTY) as SUM_QTY,JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TYPE = 'SALE' GROUP BY JNLD_TRX_NO HAVING SUM(JNLD_AMT) >= 14 "; // TRANSACTION DETAIL WITH RESPCT  TO TOTAL PRODUCT AMOUNT 
        $member_query = "select SUM(JNLD_AMT) as SUM_AMT,SUM(JNLD_QTY) as SUM_QTY,JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TYPE = 'SALE' GROUP BY JNLD_TRX_NO HAVING ( SUM(JNLD_AMT) >= 14 AND SUM(JNLD_QTY) >= 7 ) "; // TRANSACTION DETAIL WITH RESPCT  TO TOTAL PRODUCT AMOUNT AND PRODUCT COUNT
        $member_query = "select JNLD_PRODUCT,JNLD_TRX_NO from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TRX_NO='357' AND JNLD_TYPE = 'MEMBER'   "; // Direct Fetch Member Details 
        $member_query = "select SUM(JNLD_AMT) as SUM_AMT,SUM(JNLD_QTY) as SUM_QTY,JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160721 AND JNLD_YYYYMMDD <= 20160721 ) AND JNLD_TYPE = 'SALE' GROUP BY JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS HAVING ( SUM(JNLD_AMT) >= 14 AND SUM(JNLD_QTY) >= 7 ) "; // TRANSACTION DETAIL WITH RESPCT  TO TOTAL PRODUCT AMOUNT AND PRODUCT COUNT
        $member_query = "SELECT SUM(JNLD_QTY) as purchase_count,JNLD_TRX_NO,JNLD_YYYYMMDD as last_order_date,JNLD_HHMMSS  FROM JNLDTBL  WHERE  JNLD_TYPE = 'SALE'   AND (JNLD_YYYYMMDD >= '20160719' AND JNLD_YYYYMMDD <= '20160721')   AND (JNLD_HHMMSS >= '030000' AND JNLD_HHMMSS <= '112559')    GROUP BY JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS "; // TRANSACTION DETAIL WITH RESPCT  TO TOTAL PRODUCT AMOUNT AND PRODUCT COUNT

		$member_query = "SELECT * FROM OUTPTBL JOIN OUTLTBL ON OUTL_OUTLET = OUTP_OUTLET where OUTP_OUTLET = 792 AND OUTP_PRODUCT = 1006030";
		$member_query = "select * from PRODTBL where PROD_NUMBER = 1006030";
		$member_query = "select Sum(RDT_Rebate) as rebate_price from RebateDetailTbl
		join RebateHeaderTbl on RDT_Header_Id = RHT_Id
		left outer join RebateOutletTbl on ROT_Hdr_Id = RHT_Id
		where RHT_Type = 3 and GetDate() between RHT_Start_Date and RHT_End_Date
		and RDT_Product = 1006030
		and
		(
		(RHT_Zone in
		(select distinct CODE_KEY_ALP from CodeTbl
		where CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_NUM = 792 )) or (ROT_Outlet = 792 ))";
		$member_query = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from PRODTBL where PROD_STATUS = 'Active' and PROD_NUMBER = 1357591 ) a WHERE row > 0 and row <= 10";
       
		// AND TRX_OUTLET IN ( 792 ,725,793 )
		
		/*$member_query = "SELECT * FROM ( SELECT TRX_WEEK_END, TRX_OUTLET, OUTL_DESC,SUM(TRX_QTY)  AS SUM_QTY,SUM(TRX_COST) AS SUM_COST,SUM(TRX_AMT)  AS SUM_AMT,
		SUM(TRX_AMT) / NULLIF(SUM(TRX_QTY), 0) AS AVG_PRICE,SUM(TRX_AMT - TRX_COST) AS SUM_MARGIN,
		(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as GP_PCNT,
		SUM(TRX_DISCOUNT) as SUM_DISCOUNT,SUM(TRX_PROM_SALES) as SUM_PROM_SALES, ROW_NUMBER() OVER (ORDER BY TRX_OUTLET ) as row 
		FROM TRXTBL LEFT JOIN OUTLTBL ON OUTL_OUTLET = TRX_OUTLET WHERE TRX_PRODUCT = 1176 
		AND TRX_TYPE = 'ITEMSALE'  AND TRX_DATE BETWEEN '2014-01-02' AND '2016-07-01' AND TRX_QTY <> 0 ) a WHERE row > 0 and row <= 3";
		*/
		$member_query = "SELECT TRX_WEEK_END, TRX_OUTLET, OUTL_DESC,SUM(TRX_QTY)  AS SUM_QTY,SUM(TRX_COST) AS SUM_COST,SUM(TRX_AMT)  AS SUM_AMT,
		SUM(TRX_AMT) / NULLIF(SUM(TRX_QTY), 0) AS AVG_PRICE,SUM(TRX_AMT - TRX_COST) AS SUM_MARGIN,
		(((SUM(TRX_AMT) - SUM(TRX_COST)) * 100) / NULLIF(SUM(TRX_AMT), 0)) as GP_PCNT,
		SUM(TRX_DISCOUNT) as SUM_DISCOUNT,SUM(TRX_PROM_SALES) as SUM_PROM_SALES
		FROM TRXTBL LEFT JOIN OUTLTBL ON OUTL_OUTLET = TRX_OUTLET WHERE TRX_PRODUCT = 1176 AND TRX_TYPE = 'ITEMSALE' AND TRX_DATE BETWEEN '2016-06-22' AND '2016-06-27' AND TRX_QTY <> 0 AND TRX_OUTLET IN ( 700 ) GROUP BY TRX_WEEK_END, TRX_OUTLET, OUTL_DESC  ORDER BY TRX_WEEK_END DESC, TRX_OUTLET, OUTL_DESC";
		
		$member_query = "select ATRX_DATE,ATRX_MEMBER,ATRX_PRODUCT,ATRX_QTY from ACCOUNT_TRXTBL where ATRX_ACCOUNT = 2100000829 and ATRX_PRODUCT IN (117056) order by ATRX_DATE desc";
		$t = "M&M'S";
		$t = str_replace("'","''",$t);
		
		$member_query = "SELECT count(Prod_Number) as product_count FROM  OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number WHERE OUTP_OUTLET = 788 AND OUTP_STATUS = 'Active' AND PROD_NATIONAL = 1 AND Prod_Desc like '%m&m''s%'";
		//$member_query = "SELECT * FROM ( SELECT USER_NUMBER,USER_OUTLET,USER_ZONE,USER_PASSWORD, ROW_NUMBER() OVER (ORDER BY USER_NUMBER) as row from USERTBL where USER_ZONE <> '' and USER_ZONE <> 0 and  USER_STATUS = 'ACTIVE') a WHERE row > 0 and row <= 6000";
		//$member_query = "SELECT OUTP_OUTLET,APN_NUMBER,Prod_Number,Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number JOIN APNTBL on APN_PRODUCT = Prod_Number where APN_NUMBER = $type";
        //$member_query = "SELECT APN_PRODUCT FROM APNTBL where APN_NUMBER = 9300605107403";
        $member_query = "select top 10 * from TRXTBL where TRX_Store = 998 AND TRX_SUPPLIER != '' order by TRX_TIMESTAMP  desc";
        $member_query = "SELECT count(Prod_Number) as product_count FROM PRODTBL WHERE PROD_STATUS = 'Active' and ( Prod_Number = 9310036116493 )";
        $member_query = "SELECT APN_NUMBER FROM APNTBL where APN_PRODUCT = 1048414";
		$member_query = "select USER_NUMBER,USER_OUTLET,USER_ZONE,USER_PASSWORD from USERTBL where USER_OUTLET = 0 and USER_ZONE <> '' and USER_STATUS = 'ACTIVE'";
		$member_query = "SELECT * FROM ( SELECT ORD.ORDH_OUTLET,SUM(ORDL.ORDL_CARTON_QTY),  ROW_NUMBER() OVER (ORDER BY ORDH_TIMESTAMP DESC) as row FROM OrdhTbl ORD LEFT JOIN OrdlTbl ORDL on ORD.ORDH_OUTLET = ORDL.ORDL_OUTLET AND ORD.ORDH_ORDER_NO = ORDL.ORDL_ORDER_NO WERE ORD.ORDH_OUTLET in ( 792 ) AND ORD.ORDH_DOCUMENT_STATUS = 'NEW' AND ORD.ORDH_ORDER_DATE >= DateAdd(d, -60, GetDate()) ) a WHERE row > 0 AND row <= 10 ";
		//$insert_print_lable_query = "INSERT BulkOrderFromTabletTbl (BOFT_Outlet,  BOFT_Product_Number, BOFT_Print_Batch DateTime) VALUES ('792','1176','25 Aug 2016')";
		//$this->_mssql->my_mssql_query($insert_print_lable_query,$ms_con);
			
		$member_query = "SELECT * from BulkPrintLabelFromTabletTbl order by BPLT_Print_Batch desc";
		$member_query = "SELECT * from BulkStockTakeLabelFromTabletTbl order by BSTFT_Print_Batch desc";
		$member_query = "SELECT * from BulkOrderFromTabletTbl order by BOFT_Print_Batch desc";
		$member_query = "SELECT OUTL_OUTLET,OUTL_DESC,OUTL_App_Orders,OUTL_ADDR_1,OUTL_Last_Modified_Date FROM OUTLTBL order by OUTL_App_Orders DESC";
		//$member_query = "SELECT SUM(ATRX_QTY) as purchase_count, ATRX_ACCOUNT as member_id  FROM ACCOUNT_TRXTBL JOIN MEMBER on ATRX_ACCOUNT = MEMB_NUMBER AND  MEMB_Exclude_From_Competitions = 0 WHERE ATRX_PRODUCT = '702' and ATRX_DATE>='20160812' AND ATRX_DATE<='20160901' and ATRX_TYPE = 'ITEMSALE' group by ATRX_ACCOUNT ";
		//$member_query = "select top 1 ATRX_DATE,ATRX_TIMESTAMP,ATRX_MEMBER,ATRX_PRODUCT,ATRX_QTY from ACCOUNT_TRXTBL where ATRX_ACCOUNT = 2100000094 and ATRX_PRODUCT = 702 order by ATRX_TIMESTAMP desc";
		$member_query = "SELECT SUM(JNLD_QTY) as product_count,SUM(JNLD_AMT) as purchase_count,JNLD_TRX_NO,JNLD_YYYYMMDD as last_order_date
		,JNLD_HHMMSS as last_order_time   FROM JNLDTBL  WHERE  JNLD_TYPE = 'SALE' AND (JNLD_YYYYMMDD >= 20160905
		 AND JNLD_YYYYMMDD <= 20160906) AND (JNLD_HHMMSS >= 000000 AND JNLD_HHMMSS <= 232359) AND JNLD_PRODUCT
		 IN ('709') GROUP BY JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS 
		 ORDER BY JNLD_TRX_NO DESC ";
		//$member_query = "select SUM(JNLD_AMT),SUM(JNLD_QTY),JNLD_TRX_NO from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20160906 AND JNLD_YYYYMMDD <= 20160906 ) AND JNLD_TYPE = 'SALE' group by JNLD_TRX_NO "; // Direct Fetch Member Details 
        $member_query = "SELECT SUM(JNLD_QTY) as product_count,SUM(JNLD_AMT) as purchase_count,JNLD_TRX_NO,JNLD_YYYYMMDD as last_order_date
		,JNLD_HHMMSS as last_order_time   FROM JNLDTBL  WHERE  JNLD_TYPE = 'SALE'  AND JNLD_PRODUCT IN ('737'
		)   GROUP BY JNLD_TRX_NO,JNLD_YYYYMMDD,JNLD_HHMMSS  HAVING ( (SUM(JNLD_AMT)
		 <= 10 ) ) ORDER BY JNLD_TRX_NO DESC"; // Direct Fetch Member Details 
        //$member_query = "select JNLD_PRODUCT from JNLDTBL WHERE JNLD_YYYYMMDD = '20160909' AND JNLD_TYPE = 'MEMBER'"; // Direct Fetch Member Details 
		//$member_query = "SELECT SUM(ATRX_QTY) as purchase_count, ATRX_ACCOUNT as member_id  FROM ACCOUNT_TRXTBL JOIN MEMBER on ATRX_ACCOUNT = MEMB_NUMBER AND  MEMB_Exclude_From_Competitions = 0 WHERE ATRX_PRODUCT = '97896' and ATRX_TIMESTAMP>'2016-09-06 00:00:00' AND ATRX_TIMESTAMP<='2016-09-08 21:39:00' and ATRX_TYPE = 'ITEMSALE' group by ATRX_ACCOUNT";
		//$member_query = "SELECT APN_NUMBER FROM APNTBL where APN_PRODUCT = '1176'";
		$member_query = "SELECT * FROM SECOND_CHANCE_TBL";
		$member_query = "SELECT * FROM TRXTBL where TRX_OUTLET=792 and  TRX_PRODUCT = 1013011 order by TRX_DATE desc";
	
		$member_query = "SELECT ACC_NUMBER FROM ACCOUNT where ACC_EMAIL IN ( 'anneka.simpkins@nightowl.com.au','davina@polesy.com.au','mitchell.thomaswaters@nightowl.com.au','yumi.espiritu@nightowl.com.au','Richard.moore@coyotesoftware.com.au','Scott.nolan@coyotesoftware.com.au','support@coyotesoftware.com.au','licence@coyotesoftware.com.au','rrekken@gmail.com')";
		
		
		//$member_query = "select ATRX_DATE,ATRX_TIMESTAMP,ATRX_MEMBER,ATRX_PRODUCT,ATRX_QTY from ACCOUNT_TRXTBL where ATRX_ACCOUNT = 2100000827 and ATRX_PRODUCT IN (1234,782,737,751) order by ATRX_DATE desc";
		$member_query = "select * from Prize_Winners_TBL";		
		//$member_query = "Select * from TRXTBL where TRX_OUTLET = 792 and TRX_PRODUCT = 1013011";	 
		$member_query = "SELECT * FROM ( SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,ROW_NUMBER() OVER (ORDER BY Prod_Number) as row from PRODTBL JOIN OUTPTBL ON Prod_Number = Outp_Product WHERE OUTP_OUTLET = 792 AND PROD_STATUS = 'Active' ) a WHERE row > 0 and row <= 10";		
		$member_query = "SELECT Prod_Number,Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,OUTP_MIN_ONHAND,OUTP_MAX_ONHAND,OUTP_MIN_REORDER_QTY,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number where OUTP_OUTLET = 792 AND Prod_Number = 1 AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'";
		$member_query = "select ACC_NUMBER, ACC_CARD_NUMBER, ACC_FIRST_NAME, ACC_SURNAME, ACC_EMAIL, * from ACCOUNT where ACC_Last_Modified_Date > '20161020'";
		//$member_query = "select * from OutlTbl where SubString(OUTL_DESC, 1, 2) not in ('ZZ','ZG','ZA') ORDER BY OUTL_DESC ASC";
        $member_query = "Select Count(*) as RecCount from FIFOTbl where FIFO_Outlet = 743 and FIFO_PRODUCT = 100040";	
		$member_query = "select MEMB_Exclude_From_Competitions from Member where MEMB_NUMBER = 2100003894";	
		$member_query = "select * from USERTBL where USER_NUMBER = 44";
		$member_query = "select DISTINCT USER_NUMBER,USER_OUTLET,USER_ZONE,USER_PASSWORD from USERTBL where USER_STATUS = 'Active' AND USER_OUTLET = ( SELECT OUTL_OUTLET FROM OutlTbl WHERE OUTL_OUTLET = 792) OR USER_OUTLET in (SELECT CODE_KEY_NUM FROM CodeTbl LEFT JOIN OutlTbl on CODE_KEY_NUM = OUTL_OUTLET WHERE CODE_KEY_TYPE = 'ZONEOUTLET' and CODE_KEY_ALP = USER_ZONE and CODE_KEY_NUM = 792) OR ((USER_OUTLET = NULL OR USER_OUTLET = 0) AND (USER_ZONE = NULL OR USER_ZONE = ''))";
        $member_query = "select ACC_EMAIL from ACCOUNT where ACC_NUMBER IN (2100004401)";
		$member_query = "SELECT CODE_KEY_NUM, CODE_DESC FROM OUTPTBL join PRODTBL  on Outp_Product = Prod_Number join CODETBL on CODE_KEY_NUM = PROD_DEPARTMENT where  OUTP_OUTLET = 750 and OUTP_STATUS = 'Active' and CODE_KEY_TYPE = 'DEPARTMENT' and PROD_NATIONAL = 0 GROUP BY CODE_KEY_NUM, CODE_DESC ORDER BY CODE_DESC";
		$member_query = "select * from OrdhTbl where ORDH_ORDER_NO = 25285 and ORDH_OUTLET = 788";
        $member_query = "SELECT * FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number where OUTP_OUTLET = '799' AND Prod_Number = '1176' ";	
		$member_query = "Select * from outptbl join prodtbl on outp_product = prod_number where outp_outlet = '799' and prod_number = '1176' ";
		
		$member_query = "SELECT O.OUTP_PRODUCT_NUMBER FROM OUTPTBL O JOIN PRODTBL P ON P.PROD_NUMBER = O.OUTP_PRODUCT WHERE OUTP_PRODUCT = 1176 AND OUTP_OUTLET = 792 ORDER BY OUTP_PRODUCT DESC";
		$member_query = "SELECT * FROM SECOND_CHANCE_TBL where SecCh_created_date >= '2016-10-17'";
		//$member_query = "SELECT SPRD_SUPPLIER_ITEM FROM SPRDTBL where SPRD_SUPPLIER = 'COKE' and SPRD_PRODUCT = 1176";
		$member_query = "select * from OrdhTbl where ORDH_ORDER_NO = 19201886";
		//$updateSql = "DELETE FROM CODETBL WHERE CODE_KEY_TYPE = 'STOCKTAKEI' AND CODE_KEY_NUM = 701 and CODE_KEY_ALP = 1144";
		//$this->_mssql->my_mssql_query($updateSql,$ms_con);
		$member_query = "SELECT * FROM OrdlTbl where ORDL_ORDER_NO = 153787";
		$member_query = "select * from OutlTbl where OUTL_OUTLET = 712";
		$member_query = "select top 20 * from JNLDTBL ";
		$member_query = "select top 20 * from JNLD_Account_TBL ";
		$member_query = "select top 20 * from JNLH_Account_TBL ";
        $member_query = "select top 20 * from JNLH_Account_TBL ORDER BY JNAH_YYYYMMDD DESC  ";
        $member_query = "select top 20 * from JNLDTBL ORDER BY JNLD_YYYYMMDD DESC ";
        $member_query = "select top 20 * from JNLD_Account_TBL ";
        $member_query = "SELECT JNAH_TRX_NO as trx_no,JNAH_TIMESTAMP as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' ORDER BY JNAH_TIMESTAMP ASC ";
        $member_query = "SELECT JNLD_QTY as product_count,JNLD_PRODUCT as product_id,JNLD_AMT as product_amount FROM JNLDTBL  WHERE JNLD_STATUS = 1 AND JNLD_TYPE = 'SALE' AND JNLD_TRX_NO = '9439' ORDER BY JNLD_TRX_NO DESC ";
        $member_query = "select * from JNLDTBL WHERE JNLD_STATUS = 1 AND JNLD_TYPE = 'SALE' AND JNLD_TRX_NO = '9439'";
        $member_query = "select top 20 * from JNLH_Account_TBL ";
		$member_query = "select top 20 * from JNLDTBL WHERE JNLD_TRX_NO = '374' AND JNLD_TYPE = 'SALE' ";
        $member_query = "select top 20 * from JNLD_Account_TBL WHERE JNAD_TRX_NO = '90565'";
        
        $member_query= "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row FROM PRODTBL where PROD_STATUS = 'Active' and PROD_TYPE = 'InStore') a WHERE row > 0 and row <= 10";
 
        $member_query = "SELECT * FROM ( SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id,ROW_NUMBER() OVER (ORDER BY JNAH_TRX_TimeStamp) as row  FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE') a WHERE row > 0 and row <= 2";
         $member_query = " SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' AND JNAH_TRX_TimeStamp > 'Jun 01 2017 04:33:13'";
         $member_query = " SELECT * FROM ( SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id,ROW_NUMBER() OVER (ORDER BY JNAH_TRX_TimeStamp ASC) as row  FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' AND JNAH_TRX_TimeStamp > '2017-05-31 16:06:09' ) a WHERE row > 0 and row <= 2";
        $member_query = "SELECT trx_no,SUM(product_qty) AS ItemCount,member_id,SUM(product_amount) AS Amount FROM ava_cte_member_transaction_log  WHERE product_amount <= 0.1  AND product_id NOT IN (1234,1175,81173) GROUP BY trx_no,member_id";
		$member_query = "select top 20 * from JNLDTBL WHERE JNLD_TRX_NO = '374' AND JNLD_TYPE = 'SALE' ";
		$member_query = "SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO WHERE JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE'  ORDER BY JNAH_TRX_TimeStamp ASC ";
		$member_query = "SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO JOIN Member on MEMB_NUMBER = JNAD_PRODUCT WHERE MEMB_Exclude_From_Competitions = '0' AND JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE'  ORDER BY JNAH_TRX_TimeStamp ASC ";
        //$member_query = "UPDATE Member SET MEMB_Exclude_From_Competitions = '0' WHERE MEMB_NUMBER = '2100000075'";
        
         $member_query = "select top 20 * from ACCOUNT_LOYALTY ORDER BY ATRXL_REDEEM_DATE DESC ";
         $member_query = "select top 20 * from BROADCAST ORDER BY BRDC_KEY DESC";
         $member_query = "select top 20 * from CSC ORDER BY CSC_DATETIME_CAPT DESC";
         $member_query = "select top 20 * from LARGE_DATA ORDER BY LARGE_ID DESC ";
         $member_query = "select top 20 * from SECOND_CHANCE_TBL ORDER BY SecCh_ID DESC";
         $member_query = "select top 20 * from APNTBL";
         
         $member_query ="SELECT *,OUTP_QTY_ONHAND,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number JOIN APNTBL on APN_PRODUCT = Prod_Number where APN_NUMBER = '617'";
         
         $member_query = "SELECT * FROM OutpTbl JOIN OutlTbl ON OUTL_OUTLET = OUTP_OUTLET where OUTP_STATUS = 'Active' and OUTP_PRODUCT = '1013011'";
         $member_query =  "SELECT * FROM ( SELECT Prod_Number, Prod_Desc,PROD_POS_DESC,PROD_CARTON_QTY,PROD_UNIT_QTY,PROD_CARTON_COST,OUTP_NORM_PRICE_1,OUTP_CARTON_COST,OUTP_QTY_ONHAND,OUTP_MIN_ONHAND,OUTP_MAX_ONHAND,OUTP_MIN_REORDER_QTY,(OUTP_CARTON_COST / PROD_CARTON_QTY) as UnitCost, ROW_NUMBER() OVER (ORDER BY Prod_Number) as row ,ORDL_TOTAL_UNITS as Current_Order_Amt, ORDL_Promo_Units as ORDL_Promo_Units,
         (SELECT SUM(ORDL_TOTAL_UNITS) AS UNITS_ON_ORDER FROM ORDLTBL WHERE (ORDL_OUTLET = 792) AND (ORDL_PRODUCT in (1176)) AND (ORDL_DOCUMENT_TYPE = 'ORDER' ) and (ORDL_ORDER_NO <> 1952)) as totalQ
         FROM OUTPTBL JOIN PRODTBL on Outp_Product = Prod_Number LEFT JOIN ORDLTBL ON ORDL_OUTLET = OUTP_OUTLET AND Prod_Number = ORDL_PRODUCT AND ORDL_DOCUMENT_TYPE = 'ORDER' where OUTP_OUTLET = 792 AND OUTP_STATUS = 'Active' AND Prod_Status = 'Active'  AND Prod_Number = 1176) a";
      
		//      $member_query = "SELECT SUM(ORDL_TOTAL_UNITS) AS UNITS_ON_ORDER FROM ORDLTBL WHERE (ORDL_OUTLET = 792) AND (ORDL_PRODUCT in (1176)) AND (ORDL_DOCUMENT_TYPE = 'ORDER' ) and (ORDL_ORDER_NO <> 1952)";
      
        //$member_query = "select top 20 * from ORDLTBL WHERE ORDL_DOCUMENT_TYPE = 'ORDER' AND ORDL_Promo_Units != '' ORDER BY ORDL_ORDER_NO DESC ";
        
        $member_query = "SELECT * FROM OUTPTBL WHERE Outp_Product = 1176 AND OUTP_OUTLET = 700";
        
		$member_query = "SELECT * FROM OUTPTBL WHERE Outp_Product = 1176 AND OUTP_OUTLET = 700";
		$member_query = " select OUTP_QTY_ONHAND, null, null, null, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, Prod_Parent, OUTP_STATUS, Outp_Product

		from OutpTbl

		join ProdTbl on Outp_Product = Prod_Number

		where Outp_Outlet = 792

		group by Outp_Product, OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, Prod_Parent, OUTP_STATUS

		";


		$member_query = " select null, Sum(ORDL_TOTAL_UNITS ), null, null, null, null, null, null, null, null, ordl_product

		from OrdlTbl

		join OutpTbl on Outp_Product = ordl_product and Outp_Outlet =  792

		join ProdTbl on Outp_Product = Prod_Number

		where Ordl_Outlet = 792 and ORDL_DOCUMENT_TYPE = 'ORDER'

		group by ordl_product";

		$member_query = " select  case

		when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY)

		when TRX_TYPE = 'CHILDSALE' then

		case

		when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT)

		else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty

		end

		end / 8 as AvgWeeklySales

		from ProdTbl

		LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = 792 AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate())

		where TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = 792

			  where (Prod_Parent = 0 or Prod_Parent is null) )

		AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE')

		group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty";





		$member_query = " select OUTP_QTY_ONHAND as SOH, Sum(ORDL_TOTAL_UNITS ) as ONORDER,null, null, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, Prod_Parent, OUTP_STATUS, Outp_Product

		from OutpTbl

		join ProdTbl on Outp_Product = Prod_Number

		left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet =  792 and ORDL_DOCUMENT_TYPE = 'ORDER'

		where Outp_Outlet = 792

		group by ordl_product,Outp_Product, OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, Prod_Parent, OUTP_STATUS

		";

		$member_query = "select null, null, null, case

		when OUTP_PROM_SELL_YN = 'Y' or OUTP_PROM_MIX_YN = 'Y' or OUTP_PROM_OFFER_YN = 'Y' then 'Sell Promo'

		when OUTP_PROM_BUY_YN = 'Y' then 'Cost Promo'

		end as OnPromo, null, null, null, null, null, null, Outp_Product

		from OutpTbl

		where Outp_outlet = 792

		and (OUTP_PROM_BUY_YN = 'Y' or OUTP_PROM_SELL_YN = 'Y' or OUTP_PROM_MIX_YN = 'Y' or OUTP_PROM_OFFER_YN = 'Y')";


		$member_query = "select * from (select (select case

		when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY)

		when TRX_TYPE = 'CHILDSALE' then

		case

		when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT)

		else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty

		end

		end / 8 as AvgWeeklySales

		from ProdTbl

		LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = 792 AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate())

		where Prod_Number = OutpTbl.Outp_Product AND  TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = 792

			  where (Prod_Parent = 0 or Prod_Parent is null) )

		AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE')

		group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty)


		as AvgWeeklySales,

		Outp_Product as product_number,OUTP_QTY_ONHAND as soh, Sum(ORDL_TOTAL_UNITS ) as on_order, OUTP_SKIP_REORDER_YN as skip_order, OUTP_NORM_PRICE_1 as price, OUTP_MIN_ONHAND as min_stock, OUTP_MIN_REORDER_QTY as min_order_qty, PROD_PARENT as prod_parent, OUTP_STATUS as product_status,PROD_DESC as prod_desc,PROD_POS_DESC as prod_pos_desc,PROD_SIZE as prod_size,PROD_HOST_NUMBER as host_number,ROW_NUMBER() OVER (order by Outp_Product ASC) as row  from OutpTbl join ProdTbl on Outp_Product = Prod_Number  left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet = 792 and ORDL_DOCUMENT_TYPE = 'ORDER' where Outp_Outlet = 792 group by ordl_product,Outp_Product,OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, PROD_PARENT,PROD_DESC,PROD_POS_DESC,PROD_SIZE,OUTP_STATUS,PROD_HOST_NUMBER ) product_data WHERE row > 0 AND row <= 1000 ";   

		$member_query = "select case

		when OUTP_PROM_SELL_YN = 'Y' or OUTP_PROM_MIX_YN = 'Y' or OUTP_PROM_OFFER_YN = 'Y' then 'Sell Promo'

		when OUTP_PROM_BUY_YN = 'Y' then 'Cost Promo'

		end as OnPromo,Outp_Product

		from OutpTbl

		where Outp_outlet = 792

		and (OUTP_PROM_BUY_YN = 'Y' or OUTP_PROM_SELL_YN = 'Y' or OUTP_PROM_MIX_YN = 'Y' or OUTP_PROM_OFFER_YN = 'Y') ORDER BY Outp_Product ASC";



		$member_query = "select * from (select OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN, (select case

		when TRX_TYPE = 'ITEMSALE' then SUM(TRX_QTY)

		when TRX_TYPE = 'CHILDSALE' then

		case

		when ISNULL(PROD_PARENT, 0) = 0 then -1 * SUM(TRX_STOCK_MOVEMENT)

		else (-1 * SUM(TRX_STOCK_MOVEMENT)) * Prod_unit_qty

		end

		end / 8 as AvgWeeklySales

		from ProdTbl

		LEFT JOIN TRXTBL ON TRX_PRODUCT = Prod_Number AND TRX_OUTLET = 792 AND TRX_DATE Between DATEADD(dd, -57, GetDate()) and DATEADD(dd, -1, GetDate())

		where Prod_Number = OutpTbl.Outp_Product AND  TRX_PRODUCT in (select distinct Prod_Number from ProdTbl join OutpTbl on Prod_Number = OUTP_PRODUCT and Outp_Outlet = 792

				  where (Prod_Parent = 0 or Prod_Parent is null) )

		AND TRX_TYPE in ('ITEMSALE', 'CHILDSALE')

		group by Prod_Number, TRX_TYPE, PROD_PARENT, Prod_unit_qty)


		as AvgWeeklySales,

		Outp_Product as product_number,OUTP_QTY_ONHAND as soh, Sum(ORDL_TOTAL_UNITS ) as on_order, OUTP_SKIP_REORDER_YN as skip_order, OUTP_NORM_PRICE_1 as price, OUTP_MIN_ONHAND as min_stock, OUTP_MIN_REORDER_QTY as min_order_qty, PROD_PARENT as prod_parent, OUTP_STATUS as product_status,PROD_DESC as prod_desc,PROD_POS_DESC as prod_pos_desc,PROD_SIZE as prod_size,PROD_HOST_NUMBER as host_number,ROW_NUMBER() OVER (order by Outp_Product ASC) as row  from OutpTbl join ProdTbl on Outp_Product = Prod_Number  left join OrdlTbl on Outp_Product = ordl_product and Outp_Outlet = 792 and ORDL_DOCUMENT_TYPE = 'ORDER' where Outp_Outlet = 792 group by ordl_product,Outp_Product,OUTP_QTY_ONHAND, OUTP_SKIP_REORDER_YN, OUTP_NORM_PRICE_1, OUTP_MIN_ONHAND, OUTP_MIN_REORDER_QTY, PROD_PARENT,PROD_DESC,PROD_POS_DESC,PROD_SIZE,OUTP_STATUS,PROD_HOST_NUMBER,OUTP_PROM_SELL_YN,OUTP_PROM_MIX_YN,OUTP_PROM_OFFER_YN,OUTP_PROM_BUY_YN ) product_data WHERE row > 0 AND row <= 1000 ";   

		$member_query = "select * from (SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,ORDL_LINE_TOTAL,ORDL_TOTAL_UNITS,PROD_PARENT,ROW_NUMBER() OVER (order by Outp_Product ASC) as row FROM OrdlTbl JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '764' AND ORDL_OUTLET = '792' ORDER BY ORDL_SortOrder, PROD_COMMODITY, PROD_REPLICATE, PROD_DESC) as order_product_data WHERE row > 0 AND row <= 10 ";
		$member_query = "SELECT * FROM (SELECT Prod_Number,ORDL_ORDER_NO,ORDL_SortOrder,PROD_COMMODITY,ORDL_PRODUCT,PROD_DESC,PROD_REPLICATE,ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,ORDL_LINE_TOTAL,ORDL_TOTAL_UNITS,PROD_PARENT, ROW_NUMBER() OVER (ORDER BY ORDL_SortOrder DESC, PROD_COMMODITY DESC, PROD_REPLICATE DESC, PROD_DESC DESC) as row  FROM OrdlTbl JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '764' AND ORDL_OUTLET = '792') a WHERE row > 0 and row <= 10 ";

        $member_query = "SELECT * FROM OUTPTBL WHERE Outp_Product = 1176 AND OUTP_OUTLET = 700";
        $member_query = "select top 20 * from ProdTbl";
        
        $member_query = "UPDATE OUTPTBL SET OUTP_QTY_ONHAND = '44', OUTP_NORM_PRICE_1 = '22', OUTP_MIN_ONHAND = '11' , OUTP_MIN_REORDER_QTY = '10'  WHERE OUTP_PRODUCT = 11 AND OUTP_OUTLET = 792";
        $member_query = "select top 20  from OUTPTBL WHERE Outp_Product = 11 AND OUTP_OUTLET = 792 ";
        $member_query = "SELECT * FROM (SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,ORDL_LINE_TOTAL,ORDL_TOTAL_UNITS,PROD_PARENT, ROW_NUMBER() OVER (ORDER BY ORDL_SortOrder DESC, PROD_COMMODITY DESC, PROD_REPLICATE DESC, PROD_DESC DESC) as row FROM OrdlTbl JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '764' AND ORDL_OUTLET = '792' ORDER BY ORDL_SortOrder, PROD_COMMODITY, PROD_REPLICATE, PROD_DESC) a  WHERE row > 0 AND row <= 10";

		$member_query = "SELECT * FROM (SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,PROD_PARENT, ROW_NUMBER() OVER (ORDER BY ORDL_PRODUCT ASC, ORDL_SortOrder DESC,PROD_COMMODITY DESC, PROD_REPLICATE DESC, PROD_DESC DESC) as row FROM OrdlTbl JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '764' AND ORDL_OUTLET = '792') a WHERE row > 0 AND row <= 10";
        
		$member_query = "select top 20 * from JNLD_Account_TBL ORDER BY JNAD_YYYYMMDD DESC";
		$member_query = "select top 20 * from JNLH_Account_TBL ORDER BY JNAH_YYYYMMDD DESC";
		$member_query = "select top 20 * from OutpTbl WHERE Outp_Product = 11 AND OUTP_OUTLET = 792 ";
		$member_query = "SELECT * FROM (SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,PROD_PARENT, ROW_NUMBER() OVER (ORDER BY ORDL_PRODUCT ASC, ORDL_SortOrder DESC,PROD_COMMODITY DESC, PROD_REPLICATE DESC, PROD_DESC DESC) as row  FROM OrdlTbl JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '764' AND ORDL_OUTLET = '792') a ";
		$member_query = " 
		SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO JOIN Member on MEMB_NUMBER = JNAD_PRODUCT WHERE MEMB_Exclude_From_Competitions = '0' AND JNAD_TYPE = 'MEMBER' AND JNAH_TYPE = 'SALE' AND JNAH_TRX_TimeStamp > 'Jul  3 2017 11:58:25:830PM' ORDER BY JNAH_TRX_TimeStamp ASC ";
		//$member_query = "SELECT ORDL_PRODUCT,ORDL_SUPPLIER_ITEM,ORDL_DESC,ORDL_CARTON_COST,ORDL_LINE_TOTAL,ORDL_CARTONS,ORDL_UNITS,ORDL_TOTAL_UNITS,ORDL_CARTON_QTY,ORDL_LINE_TOTAL,ORDL_TOTAL_UNITS,PROD_PARENT FROM OrdlTbl JOIN PRODTBL ON ORDL_PRODUCT = Prod_Number WHERE ORDL_ORDER_NO = '764' AND ORDL_OUTLET = '792' ORDER BY ORDL_SortOrder, PROD_COMMODITY, PROD_REPLICATE, PROD_DESC";
		$member_query = "SELECT JNAD_QTY as product_count,JNAD_PRODUCT as product_id,JNAD_AMT as product_amount FROM JNLD_Account_TBL  WHERE JNAD_STATUS = 1 AND JNAD_TYPE = 'SALE'  ORDER BY JNAD_TRX_NO DESC";
		$member_query = 'select top 20 * from SCRATCH_WIN_PRODUCT_TBL ORDER BY ScrWinProd_Id DESC';
       
       	$member_query = "select USER_NUMBER,USER_OUTLET,USER_ZONE,USER_PASSWORD from USERTBL where USER_NUMBER = 4 and USER_STATUS = 'ACTIVE'";
       
		$member_query = "select top 20 * from SALES_HIST_TEMP_2";
		$member_query = "select * from ACCOUNT_TRXTBL where ATRX_ACCOUNT = 2100000094 and ATRX_PRODUCT<>0 and ATRX_TYPE = 'ITEMSALE' order by ATRX_DATE desc";
		
		$member_query = "SELECT Prod_Desc,PROD_POS_DESC,Prod_Number FROM PRODTBL WHERE Prod_Number IN (59232.0,81173.0,1811066.0,99996.0)";
		//$member_query = "select top 10 CODE_KEY_TYPE,CODE_KEY_NUM,CODE_KEY_ALP,CODE_DESC,CODE_TIMESTAMP FROM CodeTbl where CODE_KEY_TYPE = 'MANUFACTURER' and CODE_DESC like '%coke%'";
        //$member_query = "select JNLD_PRODUCT,JNLD_TRX_NO from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20180205 AND JNLD_YYYYMMDD <= 20180206 ) AND JNLD_TYPE = 'MEMBER'   ";
      //  $member_query = "select JNLD_PRODUCT,JNLD_TRX_NO from JNLDTBL WHERE  ( JNLD_YYYYMMDD >= 20180205 AND JNLD_YYYYMMDD <= 20180206 ) AND JNLD_TYPE = 'MEMBER'   ";
       $member_query = "select Prod_Desc,Prod_Number,PROD_DEPARTMENT from ProdTbl  where (Prod_Number = 79542)";
        //~ $member_query = " 
		//~ SELECT * FROM JNLD_Account_TBL  WHERE JNAD_STATUS = 1 AND JNAD_TYPE = 'SALE' AND JNAD_TRX_NO = '2098' ORDER BY JNAD_TRX_NO DESC";
		//~ $member_query = "select USER_NUMBER,USER_OUTLET,USER_ZONE,USER_PASSWORD,USER_STOREGROUP from USERTBL where USER_STOREGROUP <> 0";
		//~ $member_query = "SELECT DISTINCT OUTL_REGION FROM OutlTbl";
		//$member_query = "SELECT * FROM JNLD_Account_TBL WHERE JNAD_STATUS = 1 AND JNAD_TYPE = 'SALE' AND JNAD_TRX_NO = '552' ORDER BY JNAD_TRX_NO DESC";
		$member_query = "SELECT JNAH_TRX_NO as trx_no,JNAH_TRX_TimeStamp as trx_timestamp,JNAD_PRODUCT as member_id FROM JNLH_Account_TBL JOIN 
        JNLD_Account_TBL on JNAD_TRX_NO = JNAH_TRX_NO JOIN Member on MEMB_NUMBER = JNAD_PRODUCT WHERE JNAH_TRX_TimeStamp > '2018-04-19 22:40:16.003' ORDER BY JNAH_TRX_TimeStamp ASC";
		$get_member_data = $this->_mssql->my_mssql_query($member_query,$ms_con);
		$co_member_txns = array();
		$member_array = array();
		$total_qty = 0;
		$TrxTotal  = 0;
        echo $this->_mssql->mssql_num_rows_query($get_member_data);
		if($this->_mssql->mssql_num_rows_query($get_member_data) > 0 ) {
			while($result = $this->_mssql->mssql_fetch_object_query($get_member_data)) {
				$result = (array) $result;
				$result = (object) $result;
				print_r($result);
				$i++;
				
			}
			echo $i;
		}
		die;
	}
	
	/**
     * @Function to sync users acc card number
     * @access public
     * @return array
     */
    public function update_acc_card_number() {
		$offset = 180;
		$limit = 200;
		// get all mssql updated users
		$ms_user_query = "SELECT * FROM ( select ACC_NUMBER, ACC_CARD_NUMBER,  ROW_NUMBER() OVER (ORDER BY ACC_NUMBER ASC) as row from ACCOUNT where ACC_Last_Modified_Date > '20161020') a WHERE row > $offset AND row <= $limit ";
		$ms_user_data = $this->_mssql->my_mssql_query($ms_user_query,$this->con);
		$i = 0;
		if($this->_mssql->mssql_num_rows_query($ms_user_data) > 0 ) {
			while ($ms_user_row = $this->_mssql->mssql_fetch_object_query($ms_user_data)) {
				$acc_number = $ms_user_row->ACC_NUMBER;
				$barcode_digits = $ms_user_row->ACC_CARD_NUMBER;
				// check existance of user in app users list
				$sql = "SELECT id,acc_number,email FROM " . $this->user_account . " WHERE acc_number = '".$acc_number."' and password_hash <> ''";
				$sqlResult = $this->_db->my_query($sql);
				if($this->_db->my_num_rows($sqlResult) > 0 ) {
					// fetch store data
					$user_row = $this->_db->my_fetch_object($sqlResult);
					if(isset($user_row->id) && !empty($user_row->id)) {
						// update barcode number  is exist status in mysql user table
						$update_array = array("user_barcode" => $barcode_digits);
						$where_clause = array("id" => $user_row->id);
						$update = $this->_db->UpdateAll($this->user_account, $update_array, $where_clause, "");
					}
					echo '<pre>';
					print_r($user_row);
					$i++;
				}
			}
		}
		die;
	}
		
	 public function update_user_barcode() {
		$sql = "select id,user_barcode_image,acc_number,user_barcode,email from ava_users where user_barcode_image <> '' AND acc_number <> '' AND user_barcode = '' order by created_on desc";
		$sqlResult = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($sqlResult) > 0) {
			while ($user = $this->_db->my_fetch_object($sqlResult)) {
				if(!empty($user->acc_number) && !empty($user->id)) {
					$barcode_digits = $this->_common->get_barcode_digits($user->acc_number);
					// update account is exist status in mysql user table
					$update_array = array("user_barcode" => $barcode_digits);
					$where_clause = array("id" => $user->id);
					$update = $this->_db->UpdateAll($this->user_account, $update_array, $where_clause, "");
				}
			}
		}
		die;
	}
		
	public function test_user_barcode() {
		// check user's email in mssql account table
		//$is_exists_user = $this->check_user_existance('mangeshvyas@cdnsol.com',$this->con);		
				
		$acc_card_number = '0020000000260';
		$acc_number =20;
		// set existing user's data
		$account_number = $acc_number;
		$card_number_length = strlen($acc_card_number);
		if(!empty($acc_card_number) && $card_number_length == 13) {
			$barcode_digits = $acc_card_number; 
			// generate barcode from account number
			$barcode = $acc_card_number;
		} else {
			// get 13 digit barcode with checksum value
			$barcode_digits = $this->_common->get_barcode_digits($is_exists_user);
			// generate barcode from account number
			$barcode = $acc_number;
		}
		// generate barcode from account number
		include_once("ean_13_barcode.php"); // include to generate user barcode
		
		return true;
	}
    
    public function sync_suppliers__() {
        //establish the mssql connection
        $ms_con = $this->_mssql->mssql_db_connection();
				
        // get last inserted user in mssql account table
        $supplier_query = "select * from CODETBL WHERE CODE_KEY_TYPE = 'SUPPLIER'";
        $get_supplier_data = $this->_mssql->my_mssql_query($supplier_query,$ms_con);
        
        if($this->_mssql->mssql_num_rows_query($get_supplier_data) > 0 ) {
            
            // fetch user's last account id from mssql table
            while($supplier_row = $this->_mssql->mssql_fetch_object_query($get_supplier_data)) {
                // set account number
                print_r($supplier_row);
            }
            die;
        }
    }
    
    /**
     * @Function used to sync product data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_products() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		// get product queue log
		$ms_queue_query = "SELECT * FROM sync_queue_log WHERE sync_type='products'";
		$ms_queue_data = $this->_mssql->my_mssql_query($ms_queue_query,$cdncon);
		$ms_queue_row = $this->_mssql->mssql_fetch_object_query($ms_queue_data);
		// set queue params
		$q_id = (!empty($ms_queue_row->id)) ? $ms_queue_row->id : 0;
		$q_offset = (!empty($ms_queue_row->queue_offset)) ? $ms_queue_row->queue_offset : 0;
		$q_limit = (!empty($ms_queue_row->queue_limit)) ? $ms_queue_row->queue_limit : 0;
		$q_status = (!empty($ms_queue_row->status)) ? $ms_queue_row->status : 0;
		if($q_status != 1 && $q_limit > 0) {
			// get all products from prodtbl
			$ms_prod_query = "SELECT * FROM ( SELECT *,  ROW_NUMBER() OVER (ORDER BY PROD_NUMBER ASC) as row FROM PRODTBL ) a WHERE row > $q_offset AND row <= $q_limit";
			$ms_prod_data = $this->_mssql->my_mssql_query($ms_prod_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_prod_data) > 0 ) {
				// update queue status as running
				$updateSql = "UPDATE sync_queue_log SET status = 1 WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
				
				while ($ms_prod_row = $this->_mssql->mssql_fetch_object_query($ms_prod_data)) {
					// set product params
					$prod_id 			= (!empty($ms_prod_row->PROD_NUMBER)) ? $ms_prod_row->PROD_NUMBER : '';
					$prod_name			= (!empty($ms_prod_row->PROD_DESC)) ? $ms_prod_row->PROD_DESC : '';
					$prod_description	= (!empty($ms_prod_row->PROD_POS_DESC)) ? $ms_prod_row->PROD_POS_DESC : '';
					$prod_status		= (!empty($ms_prod_row->PROD_STATUS)) ? $ms_prod_row->PROD_STATUS : '';
					$prod_type			= (!empty($ms_prod_row->PROD_TYPE)) ? $ms_prod_row->PROD_TYPE : '';
					$prod_supplier 		= (!empty($ms_prod_row->PROD_SUPPLIER)) ? $ms_prod_row->PROD_SUPPLIER : '';
					$prod_commodity_id	= (!empty($ms_prod_row->PROD_COMMODITY)) ? $ms_prod_row->PROD_COMMODITY : '';
					$prod_department_id	= (!empty($ms_prod_row->PROD_DEPARTMENT)) ? $ms_prod_row->PROD_DEPARTMENT : '';
					$prod_manufacturer_id	= (!empty($ms_prod_row->PROD_MANUFACTURER)) ? $ms_prod_row->PROD_MANUFACTURER : '';
					$prod_group_id		= (!empty($ms_prod_row->PROD_GROUP)) ? $ms_prod_row->PROD_GROUP : '';
					$prod_category_id	= (!empty($ms_prod_row->PROD_CATEGORY)) ? $ms_prod_row->PROD_CATEGORY : '';
					$prod_access_outlet	= (!empty($ms_prod_row->PROD_ACCESS_OUTLET)) ? $ms_prod_row->PROD_ACCESS_OUTLET : '';
					$prod_tax_code 		= (!empty($ms_prod_row->PROD_TAX_CODE)) ? $ms_prod_row->PROD_TAX_CODE : '';
					$prod_parent_id 	= (!empty($ms_prod_row->PROD_PARENT)) ? $ms_prod_row->PROD_PARENT : '';
					$prod_carton_qty	= (!empty($ms_prod_row->PROD_CARTON_QTY)) ? $ms_prod_row->PROD_CARTON_QTY : '';
					$prod_unit_qty		= (!empty($ms_prod_row->PROD_UNIT_QTY)) ? $ms_prod_row->PROD_UNIT_QTY : '';
					$prod_carton_cost	= (!empty($ms_prod_row->PROD_CARTON_COST)) ? $ms_prod_row->PROD_CARTON_COST : '';
					$prod_tare_weight	= (!empty($ms_prod_row->PROD_TARE_WEIGHT)) ? $ms_prod_row->PROD_TARE_WEIGHT : '';
					$prod_subrange		= (!empty($ms_prod_row->PROD_SUBRANGE)) ? $ms_prod_row->PROD_SUBRANGE : '';
					$prod_national		= (!empty($ms_prod_row->PROD_NATIONAL)) ? $ms_prod_row->PROD_NATIONAL : '';
					$prod_unit_measure	= (!empty($ms_prod_row->PROD_UNIT_MEASURE)) ? $ms_prod_row->PROD_UNIT_MEASURE : '';
					$prod_tun_number	= (!empty($ms_prod_row->PROD_TUN_NUMBER)) ? $ms_prod_row->PROD_TUN_NUMBER : '';
					$prod_freight		= (!empty($ms_prod_row->PROD_FREIGHT)) ? $ms_prod_row->PROD_FREIGHT : '';
					$prod_size			= (!empty($ms_prod_row->PROD_SIZE)) ? $ms_prod_row->PROD_SIZE : '';
					$prod_label_qty		= (!empty($ms_prod_row->PROD_LABEL_QTY)) ? $ms_prod_row->PROD_LABEL_QTY : '';
					$prod_litres		= (!empty($ms_prod_row->PROD_LITRES)) ? $ms_prod_row->PROD_LITRES : '';
					$prod_last_apn_sold = (!empty($ms_prod_row->PROD_LAST_APN_SOLD)) ? $ms_prod_row->PROD_LAST_APN_SOLD : '';
					$prod_rrp			= (!empty($ms_prod_row->PROD_RRP)) ? $ms_prod_row->PROD_RRP : '';
					$prod_info			= (!empty($ms_prod_row->PROD_INFO)) ? $ms_prod_row->PROD_INFO : '';
					$prod_replicate		= (!empty($ms_prod_row->PROD_REPLICATE)) ? $ms_prod_row->PROD_REPLICATE : '';
					$prod_variety_ind	= (!empty($ms_prod_row->PROD_VARIETY_IND)) ? $ms_prod_row->PROD_VARIETY_IND : '';
					$prod_heart_smart_ind = (!empty($ms_prod_row->PROD_HEART_SMART_IND)) ? $ms_prod_row->PROD_HEART_SMART_IND : '';
					$prod_seasonal_ind	= (!empty($ms_prod_row->PROD_SEASONAL_IND)) ? $ms_prod_row->PROD_SEASONAL_IND : '';
					$prod_generic_ind	= (!empty($ms_prod_row->PROD_GENERIC_IND)) ? $ms_prod_row->PROD_GENERIC_IND : '';
					$prod_gm_flag_ind	= (!empty($ms_prod_row->PROD_GM_FLAG_IND)) ? $ms_prod_row->PROD_GM_FLAG_IND : '';
					$prod_warehouse_frozen_ind	= (!empty($ms_prod_row->PROD_WAREHOUSE_FROZEN_IND)) ? $ms_prod_row->PROD_WAREHOUSE_FROZEN_IND : '';
					$prod_store_frozen_ind  = (!empty($ms_prod_row->PROD_STORE_FROZEN_IND)) ? $ms_prod_row->PROD_STORE_FROZEN_IND : '';
					$prod_aust_made_ind 	= (!empty($ms_prod_row->PROD_AUST_MADE_IND)) ? $ms_prod_row->PROD_AUST_MADE_IND : '';
					$prod_aust_owned_ind	= (!empty($ms_prod_row->PROD_SLOW_MOVING_IND)) ? $ms_prod_row->PROD_SLOW_MOVING_IND : '';
					$prod_organic_ind		= (!empty($ms_prod_row->PROD_ORGANIC_IND)) ? $ms_prod_row->PROD_ORGANIC_IND : '';
					$prod_slow_moving_ind	= (!empty($ms_prod_row->PROD_SLOW_MOVING_IND)) ? $ms_prod_row->PROD_SLOW_MOVING_IND : '';
					$prod_scale_ind			= (!empty($ms_prod_row->PROD_SCALE_IND)) ? $ms_prod_row->PROD_SCALE_IND : '';
					$prod_timestamp			= (!empty($ms_prod_row->PROD_TIMESTAMP)) ? $ms_prod_row->PROD_TIMESTAMP : '';
					$prod_date_added		= (!empty($ms_prod_row->PROD_DATE_ADDED)) ? $ms_prod_row->PROD_DATE_ADDED : '';
					$prod_date_deleted		= (!empty($ms_prod_row->PROD_DATE_DELETED)) ? $ms_prod_row->PROD_DATE_DELETED : '';
					$prod_date_deactivated	= (!empty($ms_prod_row->PROD_DATE_DEACTIVATED)) ? $ms_prod_row->PROD_DATE_DEACTIVATED : '';
					$prod_date_changed		= (!empty($ms_prod_row->PROD_DATE_CHANGED)) ? $ms_prod_row->PROD_DATE_CHANGED : '';
					$prod_created_date		= date('Y-m-d H:i:s');
					$prod_modified_date 	= date('Y-m-d H:i:s');
					// insert product data into table
					$insert_prod_query = "INSERT products (prod_id,prod_name,prod_description,prod_status,prod_type,prod_supplier,prod_commodity_id,prod_department_id,prod_manufacturer_id,prod_group_id,prod_category_id,prod_access_outlet,prod_tax_code,prod_parent_id,prod_carton_qty,prod_unit_qty,prod_carton_cost,prod_tare_weight,prod_subrange,prod_national,prod_weight,prod_unit_measure,prod_tun_number,prod_freight,prod_size,prod_label_qty,prod_litres,prod_last_apn_sold,prod_rrp,prod_info,prod_variety_ind,prod_heart_smart_ind,prod_seasonal_ind,prod_generic_ind,prod_gm_flag_ind,prod_warehouse_frozen_ind,prod_store_frozen_ind,prod_aust_made_ind,prod_aust_owned_ind,prod_organic_ind,prod_slow_moving_ind,prod_scale_ind,prod_timestamp,prod_date_added,prod_date_deleted,prod_date_deactivated,prod_date_changed,prod_created_date,prod_modified_date ) VALUES ($prod_id,'$prod_name','$prod_description','$prod_status','$prod_type','$prod_supplier','$prod_commodity_id','$prod_department_id','$prod_manufacturer_id','$prod_group_id','$prod_category_id','$prod_access_outlet','$prod_tax_code','$prod_parent_id','$prod_carton_qty','$prod_unit_qty','$prod_carton_cost','$prod_tare_weight','$prod_subrange','$prod_national','$prod_weight','$prod_unit_measure','$prod_tun_number','$prod_freight','$prod_size','$prod_label_qty','$prod_litres','$prod_last_apn_sold','$prod_rrp','$prod_info','$prod_variety_ind','$prod_heart_smart_ind','$prod_seasonal_ind','$prod_generic_ind','$prod_gm_flag_ind','$prod_warehouse_frozen_ind','$prod_store_frozen_ind','$prod_aust_made_ind','$prod_aust_owned_ind','$prod_organic_ind','$prod_slow_moving_ind','$prod_scale_ind','$prod_timestamp','$prod_date_added','$prod_date_deleted','$prod_date_deactivated','$prod_date_changed','$prod_created_date','$prod_modified_date' )";
					$this->_mssql->my_mssql_query($insert_prod_query,$cdncon);
					
					// set host params
					$host_itemtype_1  = (!empty($ms_prod_row->PROD_HOST_ITEMTYPE)) ? $ms_prod_row->PROD_HOST_ITEMTYPE : '';
					$host_itemtype_2  = (!empty($ms_prod_row->PROD_HOST_ITEMTYPE2)) ? $ms_prod_row->PROD_HOST_ITEMTYPE2 : '';
					$host_itemtype_3  = (!empty($ms_prod_row->PROD_HOST_ITEMTYPE3)) ? $ms_prod_row->PROD_HOST_ITEMTYPE3 : '';
					$host_itemtype_4  = (!empty($ms_prod_row->PROD_HOST_ITEMTYPE4)) ? $ms_prod_row->PROD_HOST_ITEMTYPE4 : '';
					$host_itemtype_5  = (!empty($ms_prod_row->PROD_HOST_ITEMTYPE5)) ? $ms_prod_row->PROD_HOST_ITEMTYPE5 : '';
					$host_number_1  = (!empty($ms_prod_row->PROD_HOST_NUMBER)) ? $ms_prod_row->PROD_HOST_NUMBER : '';
					$host_number_2  = (!empty($ms_prod_row->PROD_HOST_NUMBER2)) ? $ms_prod_row->PROD_HOST_NUMBER2 : '';
					$host_number_3  = (!empty($ms_prod_row->PROD_HOST_NUMBER3)) ? $ms_prod_row->PROD_HOST_NUMBER3 : '';
					$host_number_4  = (!empty($ms_prod_row->PROD_HOST_NUMBER4)) ? $ms_prod_row->PROD_HOST_NUMBER4 : '';
					$host_number_5  = (!empty($ms_prod_row->PROD_HOST_NUMBER5)) ? $ms_prod_row->PROD_HOST_NUMBER5 : '';
					// insert product host data
					$insert_prod_host_query = "INSERT products_host_info  (host_prod_id,host_itemtype_1,host_itemtype_2,host_itemtype_3,host_itemtype_4,host_itemtype_5,host_number_1,host_number_2,host_number_3,host_number_4,host_number_5,host_created_date,host_modified_date ) VALUES ($prod_id,'$host_itemtype_1','$host_itemtype_2','$host_itemtype_3','$host_itemtype_4','$host_itemtype_5','$host_number_1','$host_number_2','$host_number_3','$host_number_4','$host_number_5','$prod_created_date','$prod_modified_date' )";
					$this->_mssql->my_mssql_query($insert_prod_host_query,$cdncon);
				}
				// update queue status as completed
				$inc_offset = $q_offset+1000;
				$inc_limit = $q_limit+1000;
				// update queue status as completed
				$updateSql = "UPDATE sync_queue_log SET status = 2,queue_offset = $inc_offset,queue_limit = $inc_limit,modified_date = '$modified_date' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
			}
		} else {
			echo 'Queue already running!!!';
		}
		
		die;
	}
	
	/**
     * @Function used to sync outlet product data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_outlet_products() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		// get product queue log
		$ms_queue_query = "SELECT * FROM sync_queue_log WHERE sync_type='outlet_products'";
		$ms_queue_data = $this->_mssql->my_mssql_query($ms_queue_query,$cdncon);
		$ms_queue_row = $this->_mssql->mssql_fetch_object_query($ms_queue_data);
		// set queue params
		$q_id = (!empty($ms_queue_row->id)) ? $ms_queue_row->id : 0;
		$q_offset = (!empty($ms_queue_row->queue_offset)) ? $ms_queue_row->queue_offset : 0;
		$q_limit = (!empty($ms_queue_row->queue_limit)) ? $ms_queue_row->queue_limit : 0;
		$q_status = (!empty($ms_queue_row->status)) ? $ms_queue_row->status : 0;
		if($q_status != 1 && $q_limit > 0) {
			// get all products from prodtbl
			$ms_prod_query = "SELECT * FROM ( SELECT *,  ROW_NUMBER() OVER (ORDER BY OUTP_OUTLET ASC) as row FROM OUTPTBL WHERE OUTP_PRODUCT != 0 ) a WHERE row > $q_offset AND row <= $q_limit";
			$ms_prod_data = $this->_mssql->my_mssql_query($ms_prod_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_prod_data) > 0 ) {
				// update queue status as running
				$updateSql = "UPDATE sync_queue_log SET status = 1 WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
				
				while ($ms_prod_row = $this->_mssql->mssql_fetch_object_query($ms_prod_data)) {
					// set product params
					$prod_id 			= (!empty($ms_prod_row->OUTP_PRODUCT)) ? $ms_prod_row->OUTP_PRODUCT : '';
					$outlet_id			= (!empty($ms_prod_row->OUTP_OUTLET)) ? $ms_prod_row->OUTP_OUTLET : '';
					$timestamp			= (!empty($ms_prod_row->OUTP_TIMESTAMP)) ? $ms_prod_row->OUTP_TIMESTAMP : '';
					$status	   			= (!empty($ms_prod_row->OUTP_STATUS)) ? trim($ms_prod_row->OUTP_STATUS) : '';
					$till_yn			= (!empty($ms_prod_row->OUTP_TILL_YN)) ? trim($ms_prod_row->OUTP_TILL_YN) : '';
					$open_price_yn 		= (!empty($ms_prod_row->OUTP_OPEN_PRICE_YN)) ? trim($ms_prod_row->OUTP_OPEN_PRICE_YN) : '';
					$carton_cost		= (!empty($ms_prod_row->OUTP_CARTON_COST)) ? $ms_prod_row->OUTP_CARTON_COST : '';
					$carton_cost_host 	= (!empty($ms_prod_row->OUTP_CARTON_COST_HOST)) ? $ms_prod_row->OUTP_CARTON_COST_HOST : '';
					$carton_cost_inv 	= (!empty($ms_prod_row->OUTP_CARTON_COST_INV)) ? $ms_prod_row->OUTP_CARTON_COST_INV : '';
					$carton_cost_avg	= (!empty($ms_prod_row->OUTP_CARTON_COST_AVG)) ? $ms_prod_row->OUTP_CARTON_COST_AVG : '';
					$qty_onhand			= (!empty($ms_prod_row->OUTP_QTY_ONHAND)) ? $ms_prod_row->OUTP_QTY_ONHAND : '';
					$min_onhand			= (!empty($ms_prod_row->OUTP_MIN_ONHAND)) ? $ms_prod_row->OUTP_MIN_ONHAND : '';
					$max_onhand			= (!empty($ms_prod_row->OUTP_MAX_ONHAND)) ? $ms_prod_row->OUTP_MAX_ONHAND : '';
					$min_reorder_qty 	= (!empty($ms_prod_row->OUTP_MIN_REORDER_QTY)) ? $ms_prod_row->OUTP_MIN_REORDER_QTY : '';
					$drawfrom_outlet	= (!empty($ms_prod_row->OUTP_DRAWFROM_OUTLET)) ? $ms_prod_row->OUTP_DRAWFROM_OUTLET : '';
					$picking_bin_no		= (!empty($ms_prod_row->OUTP_PICKING_BIN_NO)) ? trim($ms_prod_row->OUTP_PICKING_BIN_NO) : '';
					$change_label_ind	= (!empty($ms_prod_row->OUTP_CHANGE_LABEL_IND)) ? trim($ms_prod_row->OUTP_CHANGE_LABEL_IND) : '';
					$change_till_ind	= (!empty($ms_prod_row->OUTP_CHANGE_TILL_IND)) ? trim($ms_prod_row->OUTP_CHANGE_TILL_IND) : '';
					$hold_norm		 	= (!empty($ms_prod_row->OUTP_HOLD_NORM)) ? $ms_prod_row->OUTP_HOLD_NORM : '';
					$change_label_printed	= (!empty($ms_prod_row->OUTP_CHANGE_LABEL_PRINTED)) ? $ms_prod_row->OUTP_CHANGE_LABEL_PRINTED : '';
					$label_qty			= (!empty($ms_prod_row->OUTP_LABEL_QTY)) ? $ms_prod_row->OUTP_LABEL_QTY : '';
					$short_label_ind	= (!empty($ms_prod_row->OUTP_SHORT_LABEL_IND)) ? $ms_prod_row->OUTP_SHORT_LABEL_IND : '';
					$supplier_code		= (!empty($ms_prod_row->OUTP_SUPPLIER)) ? $ms_prod_row->OUTP_SUPPLIER : '';
					$skip_reorder_yn	= (!empty($ms_prod_row->OUTP_SKIP_REORDER_YN)) ? trim($ms_prod_row->OUTP_SKIP_REORDER_YN) : '';
					$gen_code			= (!empty($ms_prod_row->OUTP_GEN_CODE)) ? $ms_prod_row->OUTP_GEN_CODE : '';
					$scale_plu			= (!empty($ms_prod_row->OUTP_SCALE_PLU)) ? $ms_prod_row->OUTP_SCALE_PLU : '';
					$fifo_stock_yn		= (!empty($ms_prod_row->OUTP_FIFO_STOCK_YN)) ? trim($ms_prod_row->OUTP_FIFO_STOCK_YN) : '';
					$max_retail_price	= (!empty($ms_prod_row->OUTP_Max_Retail_Price)) ? $ms_prod_row->OUTP_Max_Retail_Price : '';
					// prices
					$price_1	= (!empty($ms_prod_row->OUTP_NORM_PRICE_1)) ? $ms_prod_row->OUTP_NORM_PRICE_1 : '';
					$price_2	= (!empty($ms_prod_row->OUTP_NORM_PRICE_2)) ? $ms_prod_row->OUTP_NORM_PRICE_2 : '';
					$price_3	= (!empty($ms_prod_row->OUTP_NORM_PRICE_3)) ? $ms_prod_row->OUTP_NORM_PRICE_3 : '';
					$price_4	= (!empty($ms_prod_row->OUTP_NORM_PRICE_4)) ? $ms_prod_row->OUTP_NORM_PRICE_4 : '';
					$price_5	= (!empty($ms_prod_row->OUTP_NORM_PRICE_5)) ? $ms_prod_row->OUTP_NORM_PRICE_5 : '';
					
					// promotion data
					$prom_ctn_cost	= (!empty($ms_prod_row->OUTP_PROM_CTN_COST)) ? $ms_prod_row->OUTP_PROM_CTN_COST : '';
					$prom_comp_yn	= (!empty($ms_prod_row->OUTP_PROM_COMP_YN)) ? $ms_prod_row->OUTP_PROM_COMP_YN : '';
					$prom_buy_yn	= (!empty($ms_prod_row->OUTP_PROM_BUY_YN)) ? $ms_prod_row->OUTP_PROM_BUY_YN : '';
					$prom_sell_yn	= (!empty($ms_prod_row->OUTP_PROM_SELL_YN)) ? $ms_prod_row->OUTP_PROM_SELL_YN : '';
					$prom_mix_yn	= (!empty($ms_prod_row->OUTP_PROM_MIX_YN)) ? $ms_prod_row->OUTP_PROM_MIX_YN : '';
					$prom_mix2_yn	= (!empty($ms_prod_row->OUTP_PROM_MIX2_YN)) ? $ms_prod_row->OUTP_PROM_MIX2_YN : '';
					$prom_offer_yn	= (!empty($ms_prod_row->OUTP_PROM_OFFER_YN)) ? $ms_prod_row->OUTP_PROM_OFFER_YN : '';
					$prom_offer2_yn	= (!empty($ms_prod_row->OUTP_PROM_OFFER2_YN)) ? $ms_prod_row->OUTP_PROM_OFFER2_YN : '';
					$prom_offer3_yn	= (!empty($ms_prod_row->OUTP_PROM_OFFER3_YN)) ? $ms_prod_row->OUTP_PROM_OFFER3_YN : '';
					$prom_offer4_yn	= (!empty($ms_prod_row->OUTP_PROM_OFFER4_YN)) ? $ms_prod_row->OUTP_PROM_OFFER4_YN : '';
					$buy_prom_code	= (!empty($ms_prod_row->OUTP_BUY_PROM_CODE)) ? $ms_prod_row->OUTP_BUY_PROM_CODE : '';
					$sell_prom_code	= (!empty($ms_prod_row->OUTP_SELL_PROM_CODE)) ? $ms_prod_row->OUTP_SELL_PROM_CODE : '';
					$comp			= (!empty($ms_prod_row->OUTP_COMP)) ? $ms_prod_row->OUTP_COMP : '';
					$mixmatch		= (!empty($ms_prod_row->OUTP_MIXMATCH)) ? $ms_prod_row->OUTP_MIXMATCH : '';
					$mixmatch2		= (!empty($ms_prod_row->OUTP_MIXMATCH2)) ? $ms_prod_row->OUTP_MIXMATCH2 : '';
					$offer			= (!empty($ms_prod_row->OUTP_OFFER)) ? $ms_prod_row->OUTP_OFFER : '';
					$offer2			= (!empty($ms_prod_row->OUTP_OFFER2)) ? $ms_prod_row->OUTP_OFFER2 : '';
					$offer3			= (!empty($ms_prod_row->OUTP_OFFER3)) ? $ms_prod_row->OUTP_OFFER3 : '';
					$offer4			= (!empty($ms_prod_row->OUTP_OFFER4)) ? $ms_prod_row->OUTP_OFFER4 : '';
					$prom_price_1	= (!empty($ms_prod_row->OUTP_PROM_PRICE_1)) ? $ms_prod_row->OUTP_PROM_PRICE_1 : '';
					$prom_price_2	= (!empty($ms_prod_row->OUTP_PROM_PRICE_2)) ? $ms_prod_row->OUTP_PROM_PRICE_2 : '';
					$prom_price_3	= (!empty($ms_prod_row->OUTP_PROM_PRICE_3)) ? $ms_prod_row->OUTP_PROM_PRICE_3 : '';
					$prom_price_4	= (!empty($ms_prod_row->OUTP_PROM_PRICE_4)) ? $ms_prod_row->OUTP_PROM_PRICE_4 : '';
					$prom_price_5	= (!empty($ms_prod_row->OUTP_PROM_PRICE_5)) ? $ms_prod_row->OUTP_PROM_PRICE_5 : '';
					$spec_price		= (!empty($ms_prod_row->OUTP_SPEC_PRICE)) ? $ms_prod_row->OUTP_SPEC_PRICE : '';
					$spec_code		= (!empty($ms_prod_row->OUTP_SPEC_CODE)) ? $ms_prod_row->OUTP_SPEC_CODE : '';
					$spec_from		= (!empty($ms_prod_row->OUTP_SPEC_FROM)) ? $ms_prod_row->OUTP_SPEC_FROM : '';
					$spec_to		= (!empty($ms_prod_row->OUTP_SPEC_TO)) ? $ms_prod_row->OUTP_SPEC_TO : '';
					$spec_carton_cost	= (!empty($ms_prod_row->OUTP_SPEC_CARTON_COST)) ? $ms_prod_row->OUTP_SPEC_CARTON_COST : '';
					$created_date		= date('Y-m-d H:i:s');
					$modified_date 		= date('Y-m-d H:i:s');
					// insert product data into table
					$insert_prod_query = "INSERT outlet_products ( prod_id,outlet_id,timestamp,status,till_yn,open_price_yn,carton_cost,carton_cost_host,carton_cost_inv,carton_cost_avg,qty_onhand,min_onhand,max_onhand,min_reorder_qty,drawfrom_outlet,picking_bin_no,change_label_ind,change_till_ind,hold_norm,change_label_printed,label_qty,short_label_ind,supplier_code,skip_reorder_yn,gen_code,scale_plu,fifo_stock_yn,max_retail_price,created_date,modified_date ) VALUES ( $prod_id,$outlet_id,'$timestamp','$status','$till_yn','$open_price_yn','$carton_cost','$carton_cost_host','$carton_cost_inv','$carton_cost_avg','$qty_onhand','$min_onhand','$max_onhand','$min_reorder_qty','$drawfrom_outlet','$picking_bin_no','$change_label_ind','$change_till_ind','$hold_norm','$change_label_printed','$label_qty','$short_label_ind','$supplier_code','$skip_reorder_yn','$gen_code','$scale_plu','$fifo_stock_yn','$max_retail_price','$created_date','$modified_date' )";
					
					$this->_mssql->my_mssql_query($insert_prod_query,$cdncon);
					// get last inserted id
					$last_id_query = 'SELECT MAX(id) AS last_id FROM outlet_products';
					$last_inserted_data = $this->_mssql->my_mssql_query($last_id_query,$cdncon);
					$last_inserted_row = $this->_mssql->mssql_fetch_object_query($last_inserted_data);
					$outlet_products_id = (!empty($last_inserted_row->last_id)) ? $last_inserted_row->last_id : 0;
					if(!empty($outlet_products_id)) {
						// insert product price data
						$insert_price_query = "INSERT outlet_products_prices ( outlet_products_id,price_1,price_2,price_3,price_4,price_5,created_date,modified_date ) VALUES ($outlet_products_id,'$price_1','$price_2','$price_3','$price_4','$price_5','$created_date','$modified_date' )";
						$this->_mssql->my_mssql_query($insert_price_query,$cdncon);
						// insert promo data
						$insert_promo_query = "INSERT outlet_products_promo_log ( outlet_products_id,prom_ctn_cost,prom_comp_yn,prom_buy_yn,prom_sell_yn,prom_mix_yn,prom_mix2_yn,prom_offer_yn,prom_offer2_yn,prom_offer3_yn,prom_offer4_yn,buy_prom_code,sell_prom_code,comp,mixmatch,mixmatch2,offer,offer2,offer3,offer4,prom_price_1,prom_price_2,prom_price_3,prom_price_4,prom_price_5,spec_price,spec_code,spec_from,spec_to,spec_carton_cost,created_date,modified_date ) VALUES ( $outlet_products_id,'$prom_ctn_cost','$prom_comp_yn','$prom_buy_yn','$prom_sell_yn','$prom_mix_yn','$prom_mix2_yn','$prom_offer_yn','$prom_offer2_yn','$prom_offer3_yn','$prom_offer4_yn','$buy_prom_code','$sell_prom_code','$comp','$mixmatch','$mixmatch2','$offer','$offer2','$offer3','$offer4','$prom_price_1','$prom_price_2','$prom_price_3','$prom_price_4','$prom_price_5','$spec_price','$spec_code','$spec_from','$spec_to','$spec_carton_cost','$created_date','$modified_date' )";
						$this->_mssql->my_mssql_query($insert_promo_query,$cdncon);
					}	
				}
				$inc_offset = $q_offset+1000;
				$inc_limit = $q_limit+1000;
				// update queue status as completed
				$updateSql = "UPDATE sync_queue_log SET status = 2,queue_offset = $inc_offset,queue_limit = $inc_limit,modified_date = '$modified_date' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
			}
		} else {
			echo 'Queue already running!!!';
		}
		
		die;
	}
	
	/**
     * @Function used to sync product data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_trx() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		// get product queue log
		$ms_queue_query = "SELECT * FROM sync_queue_log WHERE sync_type='trx'";
		$ms_queue_data = $this->_mssql->my_mssql_query($ms_queue_query,$cdncon);
		$ms_queue_row = $this->_mssql->mssql_fetch_object_query($ms_queue_data);
		// set queue params
		$q_id = (!empty($ms_queue_row->id)) ? $ms_queue_row->id : 0;
		$q_offset = (!empty($ms_queue_row->queue_offset)) ? $ms_queue_row->queue_offset : 0;
		$q_limit = (!empty($ms_queue_row->queue_limit)) ? $ms_queue_row->queue_limit : 0;
		$q_status = (!empty($ms_queue_row->status)) ? $ms_queue_row->status : 0;
		$modified_date	 		= date('Y-m-d H:i:s');
		if($q_status != 1 && $q_limit > 0) {
			// get all transaction from trxtbl
			$ms_query = "SELECT * FROM ( SELECT *,  ROW_NUMBER() OVER (ORDER BY TRX_DATE ASC) as row FROM TRXTBL  ) a WHERE row > $q_offset AND row <= $q_limit";
			$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
				// update queue status as running
				$updateSql = "UPDATE sync_queue_log SET status = 1,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
				while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
					// set product params
					$trx_type 				= (!empty($row->TRX_TYPE)) ? $row->TRX_TYPE : '';
					$trx_date				= (!empty($row->TRX_DATE)) ? $row->TRX_DATE : '';
					$trx_outlet_id			= (!empty($row->TRX_OUTLET)) ? $row->TRX_OUTLET : '';
					$trx_product_id			= (!empty($row->TRX_PRODUCT)) ? $row->TRX_PRODUCT : '';
					$trx_till				= (!empty($row->TRX_TILL)) ? $row->TRX_TILL : '';
					$trx_seq				= (!empty($row->TRX_SEQ)) ? $row->TRX_SEQ : '';
					$trx_timestamp 			= (!empty($row->TRX_TIMESTAMP)) ? $row->TRX_TIMESTAMP : '';
					$trx_rtm_id				= 0;
					$trx_store_id			= (!empty($row->TRX_STORE)) ? $row->TRX_STORE : '';
					$trx_supplier_id		= 0;
					$trx_supplier_code		= (!empty($row->TRX_SUPPLIER)) ? $row->TRX_SUPPLIER : '';
					$trx_manufaturer_id		= (!empty($row->TRX_MANUFACTURER)) ? $row->TRX_MANUFACTURER : '';
					$trx_group_id			= (!empty($row->TRX_GROUP)) ? $row->TRX_GROUP : '';
					$trx_zone_id 			= 0;
					$trx_department_id 		= (!empty($row->TRX_DEPARTMENT)) ? $row->TRX_DEPARTMENT : '';
					$trx_commodity_id		= (!empty($row->TRX_COMMODITY)) ? $row->TRX_COMMODITY : '';
					$trx_category_id		= (!empty($row->TRX_CATEGORY)) ? $row->TRX_CATEGORY : '';
					$trx_subrange			= (!empty($row->TRX_SUBRANGE)) ? $row->TRX_SUBRANGE : '';
					$trx_user				= (!empty($row->TRX_USER)) ? $row->TRX_USER : '';
					$trx_qty				= (!empty($row->TRX_QTY)) ? $row->TRX_QTY : '';
					$trx_amt				= (!empty($row->TRX_AMT)) ? $row->TRX_AMT : '';
					$trx_amt_gst			= (!empty($row->TRX_AMT_GST)) ? $row->TRX_AMT_GST : '';
					$trx_cost				= (!empty($row->TRX_COST)) ? $row->TRX_COST : '';
					$trx_cost_gst			= (!empty($row->TRX_COST_GST)) ? $row->TRX_COST_GST : '';
					$trx_discount			= (!empty($row->TRX_DISCOUNT)) ? $row->TRX_DISCOUNT : '';
					$trx_price				= (!empty($row->TRX_PRICE)) ? $row->TRX_PRICE : '';
					$trx_week_end			= (!empty($row->TRX_WEEK_END)) ? $row->TRX_WEEK_END : '';
					$trx_day				= (!empty($row->TRX_DAY)) ? $row->TRX_DAY : '';
					$trx_new_onhand			= (!empty($row->TRX_NEW_ONHAND)) ? $row->TRX_NEW_ONHAND : '';
					$trx_member 			= (!empty($row->TRX_MEMBER)) ? $row->TRX_MEMBER : '';
					$trx_points				= (!empty($row->TRX_POINTS)) ? $row->TRX_POINTS : '';
					$trx_carton_qty			= (!empty($row->TRX_CARTON_QTY)) ? $row->TRX_CARTON_QTY : '';
					$trx_unit_qty			= (!empty($row->TRX_UNIT_QTY)) ? $row->TRX_UNIT_QTY : '';
					$trx_parent				= (!empty($row->TRX_PARENT)) ? $row->TRX_PARENT : '';
					$trx_stock_movement 	= (!empty($row->TRX_STOCK_MOVEMENT)) ? $row->TRX_STOCK_MOVEMENT : '';
					$trx_tender				= (!empty($row->TRX_TENDER)) ? $row->TRX_TENDER : '';
					$trx_manual_ind			= (!empty($row->TRX_MANUAL_IND)) ? $row->TRX_MANUAL_IND : '';
					$trx_gl_account			= (!empty($row->TRX_GL_ACCOUNT)) ? $row->TRX_GL_ACCOUNT : '';
					$trx_gl_posted_ind		= (!empty($row->TRX_GL_POSTED_IND)) ? $row->TRX_GL_POSTED_IND : '';
					$trx_debtor  			= (!empty($row->TRX_DEBTOR)) ? $row->TRX_DEBTOR : '';
					$trx_flags 				= (!empty($row->TRX_FLAGS)) ? $row->TRX_FLAGS : '';
					$trx_refrence			= (!empty($row->TRX_REFERENCE)) ? $row->TRX_REFERENCE : '';
					$trx_reference_type		= (!empty($row->TRX_REFERENCE_TYPE)) ? $row->TRX_REFERENCE_TYPE : '';
					$trx_reference_number	= (!empty($row->TRX_REFERENCE_NUMBER)) ? $row->TRX_REFERENCE_NUMBER : '';
					$trx_terms_rebate_code	= (!empty($row->TRX_Terms_Rebate_Code)) ? $row->TRX_Terms_Rebate_Code : '';
					$trx_terms_rebate		= (!empty($row->TRX_Terms_Rebate)) ? $row->TRX_Terms_Rebate : '';
					$created_date			= date('Y-m-d H:i:s');
					
					// insert trx data into table
					$insert_trx_query = "INSERT bi_trx (trx_type,trx_date,trx_outlet_id,trx_product_id,trx_till,trx_seq,trx_timestamp,trx_rtm_id,trx_store_id,trx_supplier_code,trx_manufaturer_id,trx_group_id,trx_zone_id,trx_department_id,trx_commodity_id,trx_category_id,trx_subrange,trx_user,trx_qty,trx_amt,trx_amt_gst,trx_cost,trx_cost_gst,trx_discount,trx_price,trx_week_end,trx_day,trx_new_onhand,trx_member,trx_points,trx_carton_qty,trx_unit_qty,trx_parent,trx_stock_movement,trx_tender,trx_manual_ind,trx_gl_account,trx_gl_posted_ind,trx_debtor,trx_flags,trx_refrence,trx_reference_type,trx_reference_number,trx_terms_rebate_code,trx_created_date,trx_modified_date ) VALUES ( '$trx_type','$trx_date','$trx_outlet_id','$trx_product_id','$trx_till','$trx_seq','$trx_timestamp','$trx_rtm_id','$trx_store_id','$trx_supplier_code','$trx_manufaturer_id','$trx_group_id','$trx_zone_id','$trx_department_id','$trx_commodity_id','$trx_category_id','$trx_subrange','$trx_user','$trx_qty','$trx_amt','$trx_amt_gst','$trx_cost','$trx_cost_gst','$trx_discount','$trx_price','$trx_week_end','$trx_day','$trx_new_onhand','$trx_member','$trx_points','$trx_carton_qty','$trx_unit_qty','$trx_parent','$trx_stock_movement','$trx_tender','$trx_manual_ind','$trx_gl_account','$trx_gl_posted_ind','$trx_debtor','$trx_flags','$trx_refrence','$trx_reference_type','$trx_reference_number','$trx_terms_rebate_code','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_trx_query,$cdncon);
					
					// get last inserted id
					$last_id_query = 'SELECT MAX(trx_id) AS last_id FROM bi_trx';
					$last_inserted_data = $this->_mssql->my_mssql_query($last_id_query,$cdncon);
					$last_inserted_row = $this->_mssql->mssql_fetch_object_query($last_inserted_data);
					$trx_id = (!empty($last_inserted_row->last_id)) ? $last_inserted_row->last_id : 0;
					
					if(!empty($trx_id)) {
						// set promo params
						$promo_sell 	 		 = (!empty($row->TRX_PROM_SELL)) ? $row->TRX_PROM_SELL : '';
						$promo_buy  	  		 = (!empty($row->TRX_PROM_BUY)) ? $row->TRX_PROM_BUY : '';
						$promo_sales	  		 = (!empty($row->TRX_PROM_SALES)) ? $row->TRX_PROM_SALES : '';
						$promo_sales_gst  		 = (!empty($row->TRX_PROM_SALES_GST)) ? $row->TRX_PROM_SALES_GST : '';
						$promo_scan_rebate_code  = (!empty($row->TRX_Promo_Scan_Rebate_Code)) ? $row->TRX_Promo_Scan_Rebate_Code : '';
						$promo_scan_rebate  	 = (!empty($row->TRX_Promo_Scan_Rebate)) ? $row->TRX_Promo_Scan_Rebate : '';
						$promo_purchase_rebate_code  = (!empty($row->TRX_Promo_Purchase_Rebate_Code)) ? $row->TRX_Promo_Purchase_Rebate_Code : '';
						$promo_purchase_rebate  = (!empty($row->TRX_Promo_Purchase_Rebate)) ? $row->TRX_Promo_Purchase_Rebate : '';
						// insert trx promo data
						$insert_promo_query = "INSERT bi_trx_promos  (trx_id,promo_sell,promo_buy,promo_sales,promo_sales_gst,promo_scan_rebate_code,promo_scan_rebate,promo_purchase_rebate_code,promo_purchase_rebate,created_date,modified_date ) VALUES ($trx_id,'$promo_sell','$promo_buy','$promo_sales','$promo_sales_gst','$promo_scan_rebate_code','$promo_scan_rebate','$promo_purchase_rebate_code','$promo_purchase_rebate','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
						$this->_mssql->my_mssql_query($insert_promo_query,$cdncon);
					}
				}
				// update queue status as completed
				$inc_offset = $q_limit;
				$inc_limit = $q_limit+1000;
				// update queue status as completed
				$updateSql = "UPDATE sync_queue_log SET status = 2,queue_offset = $inc_offset,queue_limit = $inc_limit,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
			}
		} else {
			echo 'Queue already running!!!';
		}
		
		die;
	}
	
	/**
     * @Function used to sync department wise transaction data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_trx_departments() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		// get product queue log
		$ms_queue_query = "SELECT * FROM sync_queue_log WHERE sync_type='trx_departments'";
		$ms_queue_data = $this->_mssql->my_mssql_query($ms_queue_query,$cdncon);
		$ms_queue_row = $this->_mssql->mssql_fetch_object_query($ms_queue_data);
		// set queue params
		$q_id = (!empty($ms_queue_row->id)) ? $ms_queue_row->id : 0;
		$q_offset = (!empty($ms_queue_row->queue_offset)) ? $ms_queue_row->queue_offset : 0; //450610
		$q_limit = (!empty($ms_queue_row->queue_limit)) ? $ms_queue_row->queue_limit : 0; //450910
		$q_status = (!empty($ms_queue_row->status)) ? $ms_queue_row->status : 0;
		$modified_date	 		= date('Y-m-d H:i:s');
		if($q_status != 1 && $q_limit > 0) {
			
			$data_year = 2018;
			// get all transaction from trxtbl
			$ms_query = "SELECT * FROM ( SELECT TRX_DATE,TRX_OUTLET,TRX_DEPARTMENT,SUM(TRX_AMT) AS SUM_AMT,SUM(TRX_AMT_GST) AS SUM_AMT_GST, SUM(TRX_COST) AS SUM_COST, SUM(TRX_COST_GST) AS SUM_COST_GST, SUM(TRX_QTY) AS SUM_QTY, SUM(TRX_PRICE) AS SUM_PRICE, SUM(TRX_DISCOUNT) AS SUM_DISCOUNT, SUM(TRX_PROM_SALES) AS SUM_PROM_SALES, SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,ROW_NUMBER() OVER (ORDER BY TRX_DATE,TRX_DEPARTMENT,TRX_OUTLET ASC) as row FROM TRXTBL WHERE YEAR(TRX_DATE) = $data_year AND TRX_DEPARTMENT <> 0 AND (TRX_TYPE = 'ITEMSALE') GROUP BY TRX_OUTLET,TRX_DEPARTMENT,TRX_DATE ) a WHERE row > $q_offset AND row <= $q_limit";
			$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
				// update queue status as running
				$updateSql = "UPDATE sync_queue_log SET status = 1,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
				while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
					// set product params
					$trx_date				= (!empty($row->TRX_DATE)) ? $row->TRX_DATE : '';
					$trxd_outlet_id			= (!empty($row->TRX_OUTLET)) ? $row->TRX_OUTLET : '';
					$trxd_department_id		= (!empty($row->TRX_DEPARTMENT)) ? $row->TRX_DEPARTMENT : '';
					$trxd_category_id		= (!empty($row->TRX_CATEGORY)) ? $row->TRX_CATEGORY : '';
					$trxd_qty				= (!empty($row->SUM_QTY)) ? $row->SUM_QTY : 0;
					$trxd_amt				= (!empty($row->SUM_AMT)) ? $row->SUM_AMT : 0;
					$trxd_amt_gst			= (!empty($row->SUM_AMT_GST)) ? $row->SUM_AMT_GST : 0;
					$trxd_cost				= (!empty($row->SUM_COST)) ? $row->SUM_COST : 0;
					$trxd_cost_gst			= (!empty($row->SUM_COST_GST)) ? $row->SUM_COST_GST : 0;
					$trxd_discount			= (!empty($row->SUM_DISCOUNT)) ? $row->SUM_DISCOUNT : 0;
					$trxd_price				= (!empty($row->SUM_PRICE)) ? $row->SUM_PRICE : 0;
					$trxd_promo_sales		= (!empty($row->SUM_PROM_SALES)) ? $row->SUM_PROM_SALES : 0;
					$trxd_promo_sales_gst	= (!empty($row->SUM_PROM_SALES_GST)) ? $row->SUM_PROM_SALES_GST : 0;
					// set trx time params
					$trxt_week = date('W', strtotime($trx_date));
					$trxt_quarter = ceil(date('n', strtotime($trx_date))/3);
					$trxt_year    = date('Y',strtotime($trx_date));
					$trxt_month   = date('m',strtotime($trx_date));
					$trxt_day     = strtoupper(date("D",strtotime($trx_date)));
					
					// insert trx time data into table
					$insert_trxt_query = "INSERT bi_trx_time (trxt_date,trxt_year,trxt_month,trxt_quarter,trxt_week,trxt_day,trxt_type,trxt_created_date,trxt_modified_date) VALUES ( '$trx_date','$trxt_year','$trxt_month','$trxt_quarter','$trxt_week','$trxt_day','DEPARTMENT','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_trxt_query,$cdncon);
					
					// get last inserted id
					$last_id_query = 'SELECT MAX(trxt_id) AS last_id FROM bi_trx_time';
					$last_inserted_data = $this->_mssql->my_mssql_query($last_id_query,$cdncon);
					$last_inserted_row = $this->_mssql->mssql_fetch_object_query($last_inserted_data);
					$trxd_time_id = (!empty($last_inserted_row->last_id)) ? $last_inserted_row->last_id : 0;
					
					// insert trx department data into table
					$insert_trxd_query = "INSERT bi_trx_department (trxd_outlet_id,trxd_department_id,trxd_time_id,trxd_qty,trxd_amt,trxd_amt_gst,trxd_cost,trxd_cost_gst,trxd_price,trxd_discount,trxd_promo_sales,trxd_promo_sales_gst,trxd_transaction_date,trxd_created_date,trxd_modified_date) VALUES ( '$trxd_outlet_id','$trxd_department_id','$trxd_time_id','$trxd_qty','$trxd_amt','$trxd_amt_gst','$trxd_cost','$trxd_cost_gst','$trxd_price','$trxd_discount','$trxd_promo_sales','$trxd_promo_sales_gst','$trx_date','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_trxd_query,$cdncon);
				}
				// update queue status as completed
				$inc_offset = $q_limit;
				$inc_limit = $q_limit+300;
				// update queue status as completed
				$updateSql = "UPDATE sync_queue_log SET status = 2,queue_offset = $inc_offset,queue_limit = $inc_limit,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
			}
		} else {
			echo 'Queue already running!!!';
		}
		
		die;
	}
	
	/**
     * @Function used to sync commodity wise transaction data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_trx_commodities() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		// get product queue log
		$ms_queue_query = "SELECT * FROM sync_queue_log WHERE sync_type='trx_commodities'";
		$ms_queue_data = $this->_mssql->my_mssql_query($ms_queue_query,$cdncon);
		$ms_queue_row = $this->_mssql->mssql_fetch_object_query($ms_queue_data);
		// set queue params
		$q_id = (!empty($ms_queue_row->id)) ? $ms_queue_row->id : 0;
		$q_offset = (!empty($ms_queue_row->queue_offset)) ? $ms_queue_row->queue_offset : 0;
		$q_limit = (!empty($ms_queue_row->queue_limit)) ? $ms_queue_row->queue_limit : 0;
		$q_status = (!empty($ms_queue_row->status)) ? $ms_queue_row->status : 0;
		$modified_date	 		= date('Y-m-d H:i:s');
		if($q_status != 1 && $q_limit > 0) {
			
			$data_year = 2017;
			// get all transaction from trxtbl
			$ms_query = "SELECT * FROM ( SELECT TRX_DATE,TRX_OUTLET,TRX_DEPARTMENT,TRX_COMMODITY,SUM(TRX_AMT) AS SUM_AMT,SUM(TRX_AMT_GST) AS SUM_AMT_GST, SUM(TRX_COST) AS SUM_COST, SUM(TRX_COST_GST) AS SUM_COST_GST, SUM(TRX_QTY) AS SUM_QTY, SUM(TRX_PRICE) AS SUM_PRICE, SUM(TRX_DISCOUNT) AS SUM_DISCOUNT, SUM(TRX_PROM_SALES) AS SUM_PROM_SALES, SUM(TRX_PROM_SALES_GST) AS SUM_PROM_SALES_GST,ROW_NUMBER() OVER (ORDER BY TRX_DATE,TRX_DEPARTMENT,TRX_OUTLET ASC) as row FROM TRXTBL WHERE YEAR(TRX_DATE) = $data_year AND TRX_DEPARTMENT <> 0 AND (TRX_TYPE = 'ITEMSALE') GROUP BY TRX_OUTLET,TRX_DEPARTMENT,TRX_COMMODITY,TRX_DATE ) a WHERE row > $q_offset AND row <= $q_limit";
			$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
				// update queue status as running
				$updateSql = "UPDATE sync_queue_log SET status = 1,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
				while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
					// set product params
					$trx_date				= (!empty($row->TRX_DATE)) ? $row->TRX_DATE : '';
					$trxc_outlet_id			= (!empty($row->TRX_OUTLET)) ? $row->TRX_OUTLET : '';
					$trxc_department_id		= (!empty($row->TRX_DEPARTMENT)) ? $row->TRX_DEPARTMENT : '';
					$trxc_commodity_id		= (!empty($row->TRX_COMMODITY)) ? $row->TRX_COMMODITY : '';
					$trxc_qty				= (!empty($row->SUM_QTY)) ? $row->SUM_QTY : 0;
					$trxc_amt				= (!empty($row->SUM_AMT)) ? $row->SUM_AMT : 0;
					$trxc_amt_gst			= (!empty($row->SUM_AMT_GST)) ? $row->SUM_AMT_GST : 0;
					$trxc_cost				= (!empty($row->SUM_COST)) ? $row->SUM_COST : 0;
					$trxc_cost_gst			= (!empty($row->SUM_COST_GST)) ? $row->SUM_COST_GST : 0;
					$trxc_discount			= (!empty($row->SUM_DISCOUNT)) ? $row->SUM_DISCOUNT : 0;
					$trxc_price				= (!empty($row->SUM_PRICE)) ? $row->SUM_PRICE : 0;
					$trxc_promo_sales		= (!empty($row->SUM_PROM_SALES)) ? $row->SUM_PROM_SALES : 0;
					$trxc_promo_sales_gst	= (!empty($row->SUM_PROM_SALES_GST)) ? $row->SUM_PROM_SALES_GST : 0;
					// set trx time params
					$trxt_week = date('W', strtotime($trx_date));
					$trxt_quarter = ceil(date('n', strtotime($trx_date))/3);
					$trxt_year    = date('Y',strtotime($trx_date));
					$trxt_month   = date('m',strtotime($trx_date));
					$trxt_day     = strtoupper(date("D",strtotime($trx_date)));
					
					// insert trx time data into table
					$insert_trxt_query = "INSERT bi_trx_time (trxt_date,trxt_year,trxt_month,trxt_quarter,trxt_week,trxt_day,trxt_type,trxt_created_date,trxt_modified_date) VALUES ( '$trx_date','$trxt_year','$trxt_month','$trxt_quarter','$trxt_week','$trxt_day','COMMODITY','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_trxt_query,$cdncon);
					
					// get last inserted id
					$last_id_query = 'SELECT MAX(trxt_id) AS last_id FROM bi_trx_time';
					$last_inserted_data = $this->_mssql->my_mssql_query($last_id_query,$cdncon);
					$last_inserted_row = $this->_mssql->mssql_fetch_object_query($last_inserted_data);
					$trxc_time_id = (!empty($last_inserted_row->last_id)) ? $last_inserted_row->last_id : 0;
					
					// insert trx commodity data into table
					$insert_trxc_query = "INSERT bi_trx_commodity (trxc_outlet_id,trxc_department_id,trxc_commodity_id,trxc_time_id,trxc_qty,trxc_amt,trxc_amt_gst,trxc_cost,trxc_cost_gst,trxc_price,trxc_discount,trxc_promo_sales,trxc_promo_sales_gst,trxc_transaction_date,trxc_created_date,trxc_modified_date) VALUES ( '$trxc_outlet_id','$trxc_department_id','$trxc_commodity_id','$trxc_time_id','$trxc_qty','$trxc_amt','$trxc_amt_gst','$trxc_cost','$trxc_cost_gst','$trxc_price','$trxc_discount','$trxc_promo_sales','$trxc_promo_sales_gst','$trx_date' ,'".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_trxc_query,$cdncon);
				}
				// update queue status as completed
				$inc_offset = $q_limit;
				$inc_limit = $q_limit+300;
				// update queue status as completed
				$updateSql = "UPDATE sync_queue_log SET status = 2,queue_offset = $inc_offset,queue_limit = $inc_limit,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
			}
		} else {
			echo 'Queue already running!!!';
		}
		
		die;
	}
	
	/**
     * @Function used to sync commodity wise transaction data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_outlets() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		
			// get all outlets
			$ms_query = "SELECT * FROM OUTLTBL";
			$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
				while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
					// set product params
					$outl_id		= (!empty($row->OUTL_OUTLET)) ? $row->OUTL_OUTLET : '';
					$outl_name		= (!empty($row->OUTL_DESC)) ? $row->OUTL_DESC : '';
					$outl_email		= (!empty($row->OUTL_Email)) ? $row->OUTL_Email : '';
					$outl_region	= (!empty($row->OUTL_REGION)) ? $row->OUTL_REGION : '';
					$outl_address_1	= (!empty($row->OUTL_ADDR_1)) ? $row->OUTL_ADDR_1 : '';
					$outl_address_2	= (!empty($row->OUTL_ADDR_2)) ? $row->OUTL_ADDR_2 : '';
					$outl_address_3	= (!empty($row->OUTL_ADDR_3)) ? $row->OUTL_ADDR_3 : '';
					$outl_status	= (!empty($row->OUTL_STATUS)) ? $row->OUTL_STATUS : '';
					$outl_phone		= (!empty($row->OUTL_PHONE)) ? $row->OUTL_PHONE : '';
					$outl_postal_code	= (!empty($row->OUTL_POST_CODE)) ? $row->OUTL_POST_CODE : '';
					$outl_created_date	= (!empty($row->OUTL_TIMESTAMP)) ? $row->OUTL_TIMESTAMP : '';
					$outl_modified_date	= (!empty($row->SUM_PROM_SALES)) ? $row->OUTL_Last_Modified_Date : '';
					
					// insert outlet data into table
					$insert_outl_query = "INSERT bi_outlets (outl_id,outl_name,outl_email,outl_region,outl_address_1,outl_address_2,outl_address_3,outl_status,outl_phone,outl_postal_code,outl_created_date,outl_modified_date) VALUES ( '$outl_id','$outl_name','$outl_email','$outl_region','$outl_address_1','$outl_address_2','$outl_address_3','$outl_status','$outl_phone','$outl_postal_code','$outl_created_date','$outl_modified_date' )";
					$this->_mssql->my_mssql_query($insert_outl_query,$cdncon);
				}
			}
		die;
	}
	
	/**
     * @Function used to sync commodity wise transaction data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_zones() {
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		
			// get all zones
			$ms_query = "select CODE_KEY_ALP,CODE_DESC from CODETBL where CODE_KEY_TYPE = 'ZONE'";
			$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
				while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
					// set product params
					$zone_code	= (!empty($row->CODE_KEY_ALP)) ? $row->CODE_KEY_ALP : '';
					$zone_label		= (!empty($row->CODE_DESC)) ? $row->CODE_DESC : '';
					
					// insert zone data into table
					$insert_zone_query = "INSERT zone (zone_code,zone_label,zone_created_date,zone_modified_date) VALUES ( '$zone_code','$zone_label','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_zone_query,$cdncon);
				}
			}
		die;
	}
	
	public function sync_zone_outlets() {
		
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		
		// get all zones
		$ms_query = "select CODE_KEY_NUM,CODE_KEY_ALP,CODE_DESC from CODETBL where CODE_KEY_TYPE = 'ZONEOUTLET'";
		$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
		$i = 0;
		if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
			while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
				// set product params
				$zout_outlet_id	= (!empty($row->CODE_KEY_NUM)) ? $row->CODE_KEY_NUM : '';
				$zone_code	= (!empty($row->CODE_KEY_ALP)) ? $row->CODE_KEY_ALP : '';
				// get zone id from code
				$zone_query = "SELECT zone_id FROM zone where zone_code = '$zone_code'";
				$zone_data = $this->_mssql->my_mssql_query($zone_query,$cdncon);
				$zone_row = $this->_mssql->mssql_fetch_object_query($zone_data);
				$zone_id = (!empty($zone_row->zone_id)) ? $zone_row->zone_id : 0;
				if(!empty($zone_id)) {
					// insert zone data into table
					$insert_zone_query = "INSERT zone_outlet (zout_outlet_id,zout_zone_id,zout_created_date,zout_modified_date) VALUES ( '$zout_outlet_id','$zone_id','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_zone_query,$cdncon);
				}
				
			}
		}
		die;
	}
	
	public function sync_manufacturer() {
		
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		$q_offset = 11000;
		$q_limit  = 12000;
		// get all zones
		$ms_query = "SELECT * FROM ( select CODE_KEY_ALP,CODE_DESC,ROW_NUMBER() OVER (ORDER BY CODE_KEY_ALP ASC) as row FROM CODETBL WHERE CODE_KEY_TYPE = 'MANUFACTURER' GROUP BY CODE_KEY_ALP,CODE_DESC ) a WHERE row > $q_offset AND row <= $q_limit";
		$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
		$i = 0;
		if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
			while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
				// set params
				$manu_code	= (!empty($row->CODE_KEY_ALP)) ? $row->CODE_KEY_ALP : '';
				$manu_label	= (!empty($row->CODE_DESC)) ? $row->CODE_DESC : '';
				// insert data into table
				$insert_query = "INSERT manufacturer (manu_code,manu_label,manu_created_date,manu_modified_date) VALUES ( '$manu_code','$manu_label','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
				$this->_mssql->my_mssql_query($insert_query,$cdncon);
			}
		}
		die;
	}
	
	public function sync_suppliers() {
		
		$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		$cdncon  = $this->_mssql->mssql_cdn_db_connection();  // Connecting with mssql cdn server db
		
		// get product queue log
		$ms_queue_query = "SELECT * FROM sync_queue_log WHERE sync_type='suppliers'";
		$ms_queue_data = $this->_mssql->my_mssql_query($ms_queue_query,$cdncon);
		$ms_queue_row = $this->_mssql->mssql_fetch_object_query($ms_queue_data);
		// set queue params
		$q_id 		 = (!empty($ms_queue_row->id)) ? $ms_queue_row->id : 0;
		$q_offset 	 = (!empty($ms_queue_row->queue_offset)) ? $ms_queue_row->queue_offset : 0;
		$q_limit	 = (!empty($ms_queue_row->queue_limit)) ? $ms_queue_row->queue_limit : 0;
		$q_status	 = (!empty($ms_queue_row->status)) ? $ms_queue_row->status : 0;
		if($q_status != 1 && $q_limit > 0) {
			// get all zones
			$ms_query = "SELECT * FROM ( select *,ROW_NUMBER() OVER (ORDER BY CODE_KEY_ALP ASC) as row FROM CODETBL WHERE CODE_KEY_TYPE = 'SUPPLIER' ) a WHERE row > $q_offset AND row <= $q_limit";
			$ms_data = $this->_mssql->my_mssql_query($ms_query,$livecon);
			$i = 0;
			if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
				// update queue status as running
				$updateSql = "UPDATE sync_queue_log SET status = 1,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
				while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
					// set params
					$supp_code			= (!empty($row->CODE_KEY_ALP)) ? $row->CODE_KEY_ALP : '';
					$supp_label			= (!empty($row->CODE_DESC)) ? $row->CODE_DESC : '';
					$supp_address_1		= (!empty($row->CODE_ALP_1)) ? $row->CODE_ALP_1 : '';
					$supp_address_2		= (!empty($row->CODE_ALP_2)) ? $row->CODE_ALP_2 : '';
					$supp_address_3		= (!empty($row->CODE_ALP_3)) ? $row->CODE_ALP_3 : '';
					$supp_phone_1		= (!empty($row->CODE_ALP_5)) ? $row->CODE_ALP_5 : '';
					$supp_phone_2		= (!empty($row->CODE_ALP_6)) ? $row->CODE_ALP_6 : '';
					$supp_email			= (!empty($row->CODE_ALP_8)) ? $row->CODE_ALP_8 : '';
					$supp_abn			= (!empty($row->CODE_ALP_11)) ? $row->CODE_ALP_11 : '';
					$supp_update_cost	= (!empty($row->CODE_ALP_9)) ? $row->CODE_ALP_9 : '';
					$supp_promo_code	= (!empty($row->CODE_ALP_12)) ? $row->CODE_ALP_12 : '';
					$supp_contact_name	= (!empty($row->CODE_ALP_7)) ? $row->CODE_ALP_7 : '';
					//$supp_post_code	= (!empty($row->CODE_ALP_4)) ? $row->CODE_ALP_4 : '';
					// insert data into table
					$insert_query = "INSERT supplier (supp_code,supp_label,supp_address_1,supp_address_2,supp_address_3,supp_phone_1,supp_phone_2,supp_email,supp_abn,supp_update_cost,supp_promo_code,supp_contact_name,supp_created_date,supp_modified_date) VALUES ( '$supp_code','$supp_label','$supp_address_1','$supp_address_2','$supp_address_3','$supp_phone_1','$supp_phone_2','$supp_email','$supp_abn','$supp_update_cost','$supp_promo_code','$supp_contact_name','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."' )";
					$this->_mssql->my_mssql_query($insert_query,$cdncon);
				}
				// update queue status as completed
				$inc_offset = $q_limit;
				$inc_limit = $q_limit+1000;
				// update queue status as completed
				$updateSql = "UPDATE sync_queue_log SET status = 2,queue_offset = $inc_offset,queue_limit = $inc_limit,modified_date = '".date('Y-m-d H:i:s')."' WHERE id = $q_id";
				$this->_mssql->my_mssql_query($updateSql,$cdncon);
			}
		}
		die;
	}
	
	/**
	 * Function used to manage sales report query
	 * @param int sales_type, string start_date, string end_date, string outlet_id, string department 
	 * @return string
	 * @access private
	 * 
	 */
	public function get_sql_result() {
		$sales_query   = $_REQUEST['sales_query'];
		$sales_data = array();
		$ms_data = $this->_mssql->my_mssql_query($sales_query,$this->con);
		if($this->_mssql->mssql_num_rows_query($ms_data) > 0 ) {
			while ($row = $this->_mssql->mssql_fetch_object_query($ms_data)) {
				echo '<pre>';
				print_r($row);
			}
		}
		die;
	}
	
	/**
     * @Function used to sync department wise transaction data
	 * @input : null
     * @return: void
     * @access: public
     */    
    public function sync_trx_departments_aggregate() {
	
		// set outlet ids
		$outlet_id    = 792;
		//get post date params
		$start_date   = '2017-07-01';
		if(!empty($start_date)) {
			$financial_report = $this->financial_sales_report_calculation($start_date,$outlet_id);
			echo '<pre>';
			print_r($financial_report);die;
		} else {
			$_response['result_code'] = 0;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.error.supplier_required_dates');
		}
		return $_response;
	}
	
	function financial_sales_report_calculation($start_date,$outlet_id){
		//$livecon = $this->_mssql->mssql_live_db_connection();  // Connecting with mssql live server db
		if(!empty($start_date)) {
			// set date
			$start_date = date('Y-m-d',strtotime($start_date));
			// prepare financial sales query
			$sales_query = "SELECT 
			JNLH_OUTLET, JNLH_TILL, JNLH_TRX_NO, JNLH_TYPE, JNLH_STATUS, 
			JNLH_TRX_AMT, JNLH_CASHIER, 
			JNLD_TYPE, JNLD_STATUS, JNLD_DESC, JNLD_MIXMATCH, JNLD_OFFER, 
			JNLD_QTY, JNLD_AMT, JNLD_COST, JNLD_GST_AMT, JNLD_DISC_AMT, JNLD_POST_STATUS, 
			PROD_TAX_CODE, PROD_COMMODITY, PROD_CATEGORY, PROD_DEPARTMENT, PROD_CARTON_QTY, PROD_UNIT_QTY, 
			OUTP_CARTON_COST, OUTP_PROM_CTN_COST,
			CODE_DESC,CODE_KEY_NUM
			FROM JNLHTBL 
			JOIN JNLDTBL 
			ON JNLD_YYYYMMDD = JNLH_YYYYMMDD 
			AND JNLD_HHMMSS = JNLH_HHMMSS 
			AND JNLD_OUTLET = JNLH_OUTLET 
			AND JNLD_TILL = JNLH_TILL 
			AND JNLD_TRX_NO = JNLH_TRX_NO 
			LEFT JOIN PRODTBL 
			ON PROD_NUMBER = JNLD_PRODUCT
			LEFT JOIN CODETBL 
			ON CODE_KEY_NUM = PROD_DEPARTMENT AND CODE_KEY_TYPE = 'DEPARTMENT'
			LEFT JOIN OUTPTBL 
			ON OUTP_PRODUCT = JNLD_PRODUCT AND 
			OUTP_OUTLET = JNLD_OUTLET
			WHERE (JNLH_TRADING_DATE = '".$start_date."') AND ( JNLH_OUTLET = $outlet_id) AND JNLD_TYPE = 'SALE'
			ORDER BY CODE_KEY_NUM,JNLH_YYYYMMDD, JNLH_HHMMSS, JNLH_TILL, JNLH_TRX_NO";
			$sales_result = $this->_mssql->my_mssql_query($sales_query,$this->con);
			// initialize default param values
			$total_qty = 0;
			$TrxTotal  = 0;
			$tot_qty_cancel_sales = 0;
			$tot_amt_cancel_sales = 0;
			$department_array = array();// initialize department product sales array
			if($this->_mssql->mssql_num_rows_query($sales_result) > 0 ) {
				while($result = $this->_mssql->mssql_fetch_object_query($sales_result)) {
					// prepare journal values for sales array
					$product_sales_array['JNLH_OUTLET'] = $result->JNLH_OUTLET;
					$product_sales_array['JNLH_TRX_NO'] = $result->JNLH_TRX_NO;
					$product_sales_array['JNLH_TYPE'] 	= $result->JNLH_TYPE;
					$product_sales_array['JNLH_STATUS'] = $result->JNLH_STATUS;
					$product_sales_array['JNLH_TRX_AMT']= $result->JNLH_TRX_AMT;
					$product_sales_array['JNLH_CASHIER']= $result->JNLH_CASHIER;
					$product_sales_array['JNLD_TYPE'] 	= $result->JNLD_TYPE;
					$product_sales_array['JNLD_STATUS'] = $result->JNLD_STATUS;
					$product_sales_array['JNLD_QTY']	= $result->JNLD_QTY;
					$product_sales_array['JNLD_AMT'] 	= $result->JNLD_AMT;
					$product_sales_array['JNLD_COST'] 	= $result->JNLD_COST;
					$product_sales_array['JNLD_GST_AMT']= $result->JNLD_GST_AMT;
					$product_sales_array['JNLD_DESC'] 	= $result->JNLD_DESC;
					$product_sales_array['JNLD_OFFER'] 	= $result->JNLD_OFFER;
					$product_sales_array['JNLD_MIXMATCH'] 	= $result->JNLD_MIXMATCH;
					$product_sales_array['JNLD_DISC_AMT'] 	= $result->JNLD_DISC_AMT;
					$product_sales_array['JNLD_POST_STATUS'] = $result->JNLD_POST_STATUS;
					$product_sales_array['PROD_DEPARTMENT'] = $result->PROD_DEPARTMENT;
					$product_sales_array['PROD_TAX_CODE'] = $result->PROD_TAX_CODE;
					$product_sales_array['PROD_CATEGORY'] = $result->PROD_CATEGORY;
					$product_sales_array['PROD_COMMODITY']= $result->PROD_COMMODITY;
					
					// append product sales data in department index
					$department_array[$result->CODE_DESC][] = $product_sales_array;
				}
			}
			print_r($department_array);die;
			$financial_report = array();
			$department_sales_array = array();
			// initialize department total param value
			$total_sales_cost = 0;
			$total_sales_amt  = 0;
			$total_sales_qty  = 0;
			$total_margin  	= 0;
			$total_promo_disc_qty = 0;
			$total_promo_disc_amt = 0;
			$total_item_qty = 0;
			$total_customers = 0;
			$total_customer_qty_avg = 0;
			$total_no_sale_qty = 0;
			$trx_numbers =array();
			// prepare department sales report result
			foreach($department_array as $key=>$department_results) {
				// initialize default values
				$vsf_dep_amt = 0;
				$vsf_dep_cost = 0;
				$vsf_dep_sale_count = 0;
				$vsf_dep_margin = 0;
				$vsf_dep_sales_qty = 0;
				$vsf_dep_promo_disc_qty = 0;
				$vsf_dep_promo_disc_amt = 0;
				$vsf_dep_customers_count = 0;
				$vsf_dep_item_qty = 0;
				$vsf_dep_last_trx_no = 0;
				$vsf_dep_no_sale_qty = 0;
				// set sales total values of department
				foreach($department_results as $d_result) {
					if(!empty($key)) {
						if($d_result['JNLD_STATUS'] == 1 && $d_result['JNLD_TYPE'] == 'SALE') {
							// add sales values
							$vsf_dep_amt 		= $vsf_dep_amt+$d_result['JNLD_AMT'];
							$vsf_dep_cost 		= $vsf_dep_cost+$d_result['JNLD_COST'];
							$vsf_dep_gst_amt	= $vsf_dep_gst_amt+$d_result['JNLD_GST_AMT'];
							$vsf_dep_sales_qty	= $vsf_dep_sales_qty+$d_result['JNLD_QTY'];
							$vsf_dep_sale_count = $vsf_dep_sale_count+1;
							$vsf_dep_number		= $d_result['PROD_DEPARTMENT'];
						}
					}
					
					// set discount item values
					if($d_result['JNLD_DISC_AMT'] <> 0) {
						if($d_result['JNLD_MIXMATCH'] == '' && $d_result['JNLD_OFFER'] == '') {
						} else {
							// set promo discount values
							$vsf_dep_promo_disc_qty = $vsf_dep_promo_disc_qty+$d_result['JNLD_QTY'];
							$vsf_dep_promo_disc_amt = $vsf_dep_promo_disc_amt+$d_result['JNLD_DISC_AMT'];
						}
					}
					
					// set total customers
					if($d_result['JNLH_STATUS'] == 1 && $d_result['JNLH_TYPE'] == 'SALE') {
						if(!in_array($d_result['JNLH_TRX_NO'],$trx_numbers)) {
							$trx_numbers[] = $d_result['JNLH_TRX_NO'];
							// add customer count
							if($last_trx_no <> $d_result['JNLH_TRX_NO']) {
								$vsf_dep_customers_count = $vsf_dep_customers_count+1;
							}
							// set last trx numbers
							$last_trx_no = $d_result['JNLH_TRX_NO'];
						}
					}
					
					if($d_result['JNLD_STATUS'] == 1 && $d_result['JNLD_TYPE'] == 'SALE') {
						// set item quantities
						$vsf_dep_item_qty = $vsf_dep_item_qty+$d_result['JNLD_QTY'];
					}
				}
				// set total department values
				$total_sales_cost 	= $total_sales_cost+$vsf_dep_cost;
				$total_sales_amt  	= $total_sales_amt+$vsf_dep_amt;
				$total_margin 	  	= $total_margin+$dept_data['vsf_dep_margin'];
				$total_sales_ex_gst = $total_sales_ex_gst+$dept_data['vsf_dep_sales_ex_gst'];
				$total_sales_qty 	= $total_sales_qty+$vsf_dep_sales_qty;
				
				// set counters total promo discount values
				$total_promo_disc_qty = $total_promo_disc_qty+$vsf_dep_promo_disc_qty;
				$total_promo_disc_amt = $total_promo_disc_amt+$vsf_dep_promo_disc_amt;
				// set total customers count
				$total_customers = $total_customers+$vsf_dep_customers_count;
				// set total item quantity
				$total_item_qty = $total_item_qty+$vsf_dep_item_qty;
			}
			
			// set total customers average quantity
			if($total_customers > 0 && $total_item_qty <> 0) {
				$total_customer_qty_avg = $total_item_qty/$total_customers;
			}
			// set total customers average amount
			if($total_customers > 0 && $total_sales_amt <> 0) {
				$total_customer_amt_avg = $total_sales_amt / $total_customers;
			}
			// append final department array in financial report
			$financial_report['total_sales_cost']= $total_sales_cost;
			$financial_report['total_sales_amt'] = $total_sales_amt;
			$financial_report['total_margin'] 	 = $total_margin;
			$financial_report['total_gp'] 		 = ((($total_sales_amt - $total_sales_cost) * 100) / $total_sales_amt);
			$financial_report['total_sales_ex_gst'] = $total_sales_ex_gst;
			$financial_report['total_customers'] 	= $total_customers;
			$financial_report['avg_items_per_customer'] = $total_customer_qty_avg;
			$financial_report['avg_spend_per_customer'] = $total_customer_amt_avg;
			$financial_report['total_promo_disc_qty'] = $total_promo_disc_qty;
			$financial_report['total_promo_disc_amt'] = $total_promo_disc_amt;
			return $financial_report;
		}
	}
	
    
} // End users class
?> 

