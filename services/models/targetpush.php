<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Amit
 * Timestamp : Jan-24
 * Copyright : Coyote team
 *
 */
class targetpush {
    public $_response = array();
    public $result = array();
    public $iosPush = array();
    public $androidPush = array();
  
    private $ava_targetpush_notificaiton = 'ava_targetpush_notificaiton';
    private $targetpush_activities_table = 'ava_targetpush_activities';
    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_locale = $this->_common->locale();
        $this->_push    = pushnotifications::getInst(); /// -- Create push notifications instance --
        $this->_config  = config::getInst();
    }
    /**
     * @method get_offer() return the offers w.r.t. beacon_id and store_id .
     * @access public
     * @return array
     */
     
        public function send_target_push() {
		$datetime = date('Y-m-d H:i:s');
		$date = date('Y-m-d');
		$time = date('H:i:s');
		// get app versions
		$mini_ios_version = $this->_config->getKeyValue("mini_ios_version");
		$mini_android_version = $this->_config->getKeyValue("mini_android_version");
		$targetpush_offers = array();
		$selectOffer = "SELECT * FROM " . $this->ava_targetpush_notificaiton . "  WHERE status = '1'  AND is_simple_offer = '2' AND is_delete = '0' AND (notification_start_date <= '".$date."' AND notification_end_date >= '".$date."')";
		$resultOffer = $this->_db->my_query($selectOffer);
		if ($this->_db->my_num_rows($resultOffer) > 0) { 
			while($row = $this->_db->my_fetch_object($resultOffer)) {
                $targetpush_id 	= 	$row->targetpush_id;
                $targetpush_name	 	= $row->targetpush_name;
                $targetpush_short_description	= $row->targetpush_short_description; 
                $customer_ids =  $member_ids = $member_ids	= $row->member_ids; 
            /* Check already sent date wise */
                $selectPush = "SELECT * FROM ".$this->targetpush_activities_table." WHERE targetpush_id = '$targetpush_id' AND send_at ='$date'";
                $resultPush = $this->_db->my_query($selectPush);
                if (empty($this->_db->my_num_rows($resultPush))) { 
                /* Member respect to search */
                        
                    $member_idA = array();
                    $member_ids = str_ireplace('-',',',$member_ids);
                    $member_idA = explode(',',$member_ids);
                    $custArray = array();
                    $custFinal = array();
                    foreach($member_idA as $memberVal){
                        if(!empty($memberVal)){
                            $selectCustomer = "SELECT id,device_token,device_type FROM ava_users WHERE acc_number = $memberVal AND active = 1 AND is_push_send = 1 AND ((device_type = 'ios' AND mini_app_version >= '$mini_ios_version') OR (device_type = 'android' AND mini_app_version >= '$mini_android_version') )";
                            $resultCustomer = $this->_db->my_query($selectCustomer);
                            while($rowCust = $this->_db->my_fetch_object($resultCustomer)) {
                                    $id = $rowCust->id ;
                                    $device_token = $rowCust->device_token ;
                                    $device_type = $rowCust->device_type ;
                                        if($device_token != '' && $device_type != '') {
                                            $custArray['ACC_NUMBER']	= $id ;
                                            $custArray['device_token'] 	= $device_token;
                                            $custArray['device_type'] 	= $device_type;
                                            $custFinal[] = $custArray; 
                                        }
                                    }
                                }       
                            }
                            
                        if(!empty($custFinal)){
                            foreach($custFinal as $value) {
                                
                                $deviceId = $value['device_token'];
                                
                                if ($value['device_type'] == 'ios') {
                                    $iosPush[$deviceId]['deviceId']    	= $deviceId;
                                    $iosPush[$deviceId]['offer_id'] 		= $targetpush_id;
                                    $iosPush[$deviceId]['offer_name'] 		= $targetpush_name;
                                    $iosPush[$deviceId]['offer_short_description'] = $targetpush_short_description;
                                } else {
                                    $androidPush[$deviceId]['deviceId']    = $value['device_token'];
                                    $androidPush[$deviceId]['offer_id'] 	= $targetpush_id;
                                    $androidPush[$deviceId]['offer_name'] 	= $targetpush_name;
                                    $androidPush[$deviceId]['offer_short_description'] = $targetpush_short_description;
                                }
                            }
                        }
                        
                       
                        $flagToSend = 0;
                        /* START send Target Notification */
                        if(!empty($iosPush)) {                 
						   $flagToSend++;
                           $this->_push->sendIphonePushNotification_target($iosPush);
						} 
						
						if(!empty($androidPush)) {                 
                            $flagToSend++;
						   $this->_push->sendAndroidPushNotification_target($androidPush);
						}
                        /* END Target Notification */
                        if($flagToSend > 0){
                            $insertPush = "INSERT INTO ".$this->targetpush_activities_table."(targetpush_id,is_push_send,added_at,customer_ids,send_at) VALUES ('$targetpush_id','1','$datetime','$customer_ids','$date')";
                            $this->_db->my_query($insertPush);						
                            $_response['result_code'] = 1;
                            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_targetpush_push.success.send');                        
                        }else{
                            $_response['result_code'] = 0;
                            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_targetpush_push.not.send');                        
                        }
                    }else{
                            $_response['result_code'] = 0;
                            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_targetpush_push.already.send');                        
                    
                    }      
                }        
            } else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.sendPush.send_targetpush_push.error.no_offer');
		}
		return $_response;
	} 
    
    /**
     * @method get_targetpush_details() return the targetpush detail w.r.t. targetpush id 
     * @access public
     * @return array
    */
    public function get_targetpush_details() {
		$targetpush_id = $this->_common->test_input($_REQUEST['id']);
		if (!empty($targetpush_id)) { 
			$selecttargetpush = "SELECT * FROM " . $this->ava_targetpush_notificaiton . " WHERE targetpush_id = '$targetpush_id' AND status = '1' AND is_delete = '0'";
			$resulttargetpush = $this->_db->my_query($selecttargetpush);
			if ($this->_db->my_num_rows($resulttargetpush) > 0) { 
				$row = $this->_db->my_fetch_object($resulttargetpush);
					$result['id'] 				= $row->targetpush_id;
					$result['name']		 		= $row->targetpush_name;
					$result['offer_short_description'] = $row->targetpush_short_description; 
					$result['offer_long_description'] 	= $row->targetpush_long_description;
					$result['offer_image_1'] 			= IMG_URL.'offer_images'.DS.$row->targetpush_image_1;
					$result['offer_image_2'] 			= IMG_URL.'offer_images'.DS.$row->targetpush_image_2;
					$result['offer_image_3'] 			= IMG_URL.'offer_images'.DS.$row->targetpush_image_3;
                    $result['start_date'] 		        = $row->notification_start_date;
                    $result['end_date'] 		        = $row->notification_end_date;
                    $result['start_time'] 		        = '00:00:00';
                    $result['end_time'] 		        = '23:59:59';
					$result['offer_price'] 			= $row->targetpush_price;
					$result['is_banner_image'] 				= $row->is_banner_image;
					$result['barcode_image'] 				= IMG_URL.'barcodes'.DS.$row->barcode_image;
					if($row->is_banner_image == 1) {
						$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->targetpush_image_1;
					} else if($row->is_banner_image == 2) {
						$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->targetpush_image_2;
					} else {
						$result['is_default_image'] 		= IMG_URL.'offer_images'.DS.$row->targetpush_image_3;
					}
					$_response['result_code'] = 1;
					$_response['result'] = $result;
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_targetpush_details.error.no_offer');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_targetpush_details.error.required');
		}
		return $_response;
	} // end get_target_details()
	
      public function test_push(){
           $androidPush = array();
           $deviceId = 'be28ff60755ef93e3cab5d818a25e0be5c6721067e8ed64dd38e484555698ee4';
           $deviceId1 = 'eutT8HM5dzc:APA91bEGK1aDuFa3IAfIasXVBsN8SLIJuZKgTlfzywMhk0AuuRqatSeiqydwUnwemNMgYssFazETjo8yqrDyPv02k5rA9qYAwkGyRp-zDQlC7mumkXYaiDGQj9A87eLzGc8O5B_T1hOd';
          // $deviceId = 'cXSVddoiUeA:APA91bHsxHINP92rM9FaapyKlk-qlSXymvvUv4va-ZN1i_H4lOVM-xrIREkSIoZ_MAzCiSXqyLDRG5K9_Lx5NNe34r4rROHKg5YFPJDbEvEOs_L1pLT1wjKm73_dP7A7UyC2mVhTXEW5';
			$androidPush[$deviceId]['deviceId']    = $deviceId1;
			$androidPush[$deviceId]['offer_id'] 	= '333';
			$androidPush[$deviceId]['offer_name'] 	= 'Test Second';
			$androidPush[$deviceId]['offer_short_description'] ='Test First Description';
			$this->_push->sendAndroidPushNotification_target($androidPush);
			//$this->_push->sendIphonePushNotification_target($androidPush);
            die;
        }
    
} // End targetpush class

?> 
