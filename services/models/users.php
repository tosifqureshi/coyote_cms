<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Author : Amit
 * Timestamp : Jan-24
 * Copyright : airdoc team
 *
 */
class users {
    public $_response = array();
    public $result = array();
    private $stores_table = 'ava_stores_log';
    private $stores_open_hours_table = 'ava_store_open_hours';
    private $user_account = 'ava_users';
    private $user_member = 'ava_member';
    private $user_account_data = 'ava_account_data';
    private $setting_table = 'ava_app_settings';
    private $users_table = 'ava_users';
    private $users_otp_table = 'ava_users_opt_log';
    private $reward_config = 'ava_reward_config';
    private $reward_transaction = 'ava_reward_transaction';
    private $reward_campaign_table = 'ava_reward_campaign';
    private $kiosk_licence_table = 'ava_kiosk_licence_keys';

    /**
     * @method __construct
     * @see private constructor to protect beign inherited
     * @access private
     * @return void
     */
    public function __construct() {
        $this->_db = env::getInst(); // -- Create Database Connection instance --
        $this->_config  = config::getInst();
        $this->_common = new commonclass(); // Create instance of commonclass
        $this->_push    = pushnotifications::getInst(); // -- Create push notifications instance
        $this->_locale = $this->_common->locale();
        $this->_mssql	= new mssql(); // Create instance of mssql class
        $this->con = $this->_mssql->mssql_db_connection();  // Connecting with mssql live server db
    }
    
    /**
     * Function used to authenticate app version
     * @access public
     * @return array
     */
    function check_app_version() {
		$avc = (isset($_REQUEST['avc'])) ? $_REQUEST['avc'] : "";
		$os = (isset($_REQUEST['os'])) ? $_REQUEST['os'] : "";
		if($avc != ''){
			$device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
			$sql = "SELECT * FROM " . $this->setting_table." where device_type = '".$device_type."' and app_type = 1" ;
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				if($avc < $row->mini_app_version) {
					$_response['result_code'] = 111;
					$_response['result'] = $row;
					$_response['error_msg'] = "Your app version is too old. Need to upgrade it!.";
					return $_response;
				} 
			}
		}
	}
	
    /**
     * @method register_user() is use to registered user whose email id is not available in database.
     * @access public
     * @return array
     */
    public function register_user() {
		// set form post params
        $current_date 		= CURRENT_TIME;
        $ACC_EMAIL 			= $this->_common->test_input($_REQUEST['ACC_EMAIL']);
        $ACC_PASSWORD 		= md5($this->_common->test_input($_REQUEST['ACC_PASSWORD']));
        $ACC_MOBILE 		= $this->_common->test_input($_REQUEST['ACC_MOBILE']);
        $ACC_FIRST_NAME 	= $this->_common->test_input($_REQUEST['ACC_FIRST_NAME']);
        $ACC_SURNAME 		= $this->_common->test_input($_REQUEST['ACC_SURNAME']);
        $ACC_DATE_OF_BIRTH	= $this->_common->test_input($_REQUEST['ACC_DATE_OF_BIRTH']);
        $ACC_ADDR_1 		= $this->_common->test_input($_REQUEST['ACC_ADDR_1']);
        $ACC_POST_CODE 		= $this->_common->test_input($_REQUEST['ACC_POST_CODE']);
        $country 			= $this->_common->test_input($_REQUEST['country']);
        $device_type 		= $this->_common->test_input($_REQUEST['device_type']);
        $device_id 			= $this->_common->test_input($_REQUEST['device_id']);
        $device_token 		= $this->_common->test_input($_REQUEST['device_token']);
        $gender				= 1;
        $facebook_id		= '';
        $instagram_id		= '';
		// set vendor id
        $vendor_id    = (isset($_REQUEST['vendor_id']) && !empty($_REQUEST['vendor_id'])) ? trim($_REQUEST['vendor_id']) : "";
        if(isset($_REQUEST['facebook_id'])) {
			$facebook_id 	= $this->_common->test_input($_REQUEST['facebook_id']);
		}
		if(isset($_REQUEST['instagram_id'])) {
			$instagram_id 	= $this->_common->test_input($_REQUEST['instagram_id']);
		}
        
        if (!empty($ACC_FIRST_NAME) && !empty($ACC_EMAIL) && !empty($ACC_SURNAME) && !empty($ACC_PASSWORD) && !empty($ACC_MOBILE)) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) && $email != "") {
                $_response['result_code'] 	= 0;
                $_response['error_msg'] 	= $this->_common->langText($this->_locale,'txt.register.error.invalidemail');
            } else {
                $is_email_exists = $this->_common->check_user_email($ACC_EMAIL, '0'); // check email id exits or not
                if ($is_email_exists == TRUE) { // if email address found in database
                    $_response['result_code'] 	= 0;
                    $_response['error_msg'] 	= $this->_common->langText($this->_locale,'txt.register.error.unique');
                } else {
					$this->_common->clean_device_token($device_token); //clear privous device tokens from database
					
					$query = "INSERT INTO " . $this->user_account . " (email, password_hash, mobile, firstname, lastname, date_of_birth, address, post_code, active, created_on, update_at,device_token,device_type,facebook_id,instagram_id,last_login_at,country,device_id,gender,vendor_id) VALUES ('$ACC_EMAIL', '$ACC_PASSWORD', '$ACC_MOBILE', '$ACC_FIRST_NAME', '$ACC_SURNAME', '$ACC_DATE_OF_BIRTH', '$ACC_ADDR_1', '$ACC_POST_CODE', '1','$current_date','$current_date','$device_token','$device_type','$facebook_id','$instagram_id','$current_date','$country','$device_id','$gender','$vendor_id')";
                    $this->_db->my_query($query);
                    $account_id = $this->_db->mysqli_insert_id();
                    log_message("debug", "common file included"); // -- Log Debug Message --
                    if ($account_id) {
						define('ACCESSKEY',$this->_config->getKeyValue("access_key"));
						$sync_mssql = new sync_mssql();  // create instance syn_mssql class--
                        $acc_number = $sync_mssql->sync_mssql_user($_REQUEST);
						if(!empty($acc_number)) {
							$session_token = uniqid();
							//$barcode = $account_id.rand(0,1000); // account_id need to change with mssql user id after data sync
							$barcode = $acc_number; 
							include_once("ean_13_barcode.php"); // include to generate user barcode
							$user_barcode = $image_name;
							// get barcode final digit
							$barcode_digits = $this->_common->get_barcode_digits($barcode);
							$acc_status = $sync_mssql->mssql_user_status($acc_number);
							$updateArray = array("acc_number" => $acc_number,"session_token" => $session_token,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode,"status" => $acc_status);
							$whereClause = array("id" => $account_id);
							$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");			
							
							$account_member_query = "INSERT INTO ".$this->user_member."(MEMB_NUMBER,MEMB_LOYALTY_TYPE,MEMB_ACCUM_POINTS_IND,MEMB_POINTS_BALANCE,MEMB_FLAGS) VALUES ('$account_id','0','1','0','0')";
							$this->_db->my_query($account_member_query);

							$result = array();
							$result['customer_id'] 		= $account_id;
							$result['ACC_FIRST_NAME'] 	= $ACC_FIRST_NAME;
							$result['ACC_SURNAME'] 		= $ACC_SURNAME;
							$result['ACC_EMAIL'] 		= $ACC_EMAIL;
							$result['ACC_MOBILE'] 		= $ACC_MOBILE;
							$result['ACC_DATE_OF_BIRTH']= $ACC_DATE_OF_BIRTH;
							$result['ACC_ADDR_1'] 		= $ACC_ADDR_1;
							$result['ACC_POST_CODE'] 	= $ACC_POST_CODE;
							$result['ACC_GENDER'] 		= $gender;
							$result['country'] 			= $country;
							$result['session_token'] 	= $session_token;
							$result['user_barcode'] 	= IMG_URL.'user_barcodes/'.$user_barcode;
							$result['profile_pic'] 		= IMG_URL.'user_images/user.jpg';
							$result['ACC_DATE_ADDED'] 	= $current_date;
							$result['is_push_send'] 	= 1;
							$result['acc_number'] 		= $acc_number;
							$_response['result_code']   = 1;
							$_response['result'] = $result;
							$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.register.success');
						} else {
							$_response['result_code'] 	= 0;
							$_response['error_msg'] 	= $this->_common->langText($this->_locale,'txt.error.insert');
						}
                    } else {
                        $_response['result_code'] 	= 0;
                        $_response['error_msg'] 	= $this->_common->langText($this->_locale,'txt.error.insert');
                    }
                }
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.required');
        }
        return $_response;
    } //end register_user()
    
    /**
     * @method login() is use to login user which are active and not deleted.
     * @access public
     * @return array
     */
	
	public function login() {
        $current_date = CURRENT_TIME;
        $ACC_EMAIL 		= $this->_common->test_input($_REQUEST['ACC_EMAIL']);
        $ACC_PASSWORD 	= $this->_common->test_input($_REQUEST['ACC_PASSWORD']);
        $device_type 	= $this->_common->test_input($_REQUEST['device_type']);
        $device_token 	= $this->_common->test_input($_REQUEST['device_token']);
        // get app versions
		$mini_ios_version = $this->_config->getKeyValue("mini_ios_version");
		$mini_android_version = $this->_config->getKeyValue("mini_android_version");
		// get app version
        $app_version    = (isset($_REQUEST['avc'])) ? trim($_REQUEST['avc']) : "";
        // set vendor id
        $vendor_id    = (isset($_REQUEST['vendor_id']) && !empty($_REQUEST['vendor_id'])) ? trim($_REQUEST['vendor_id']) : "";
        $session_token 	= uniqid();
		$_response['result_code'] = 0;
        if($ACC_PASSWORD != "") {
			$ACC_PASSWORD =  md5($ACC_PASSWORD);
		}
        if (empty($ACC_EMAIL) || empty($ACC_PASSWORD)) {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.login.error.required');
        } else {
            
			$sql = "SELECT * FROM " . $this->user_account . " WHERE email = '" . $ACC_EMAIL . "' ";
            $sqlResult = $this->_db->my_query($sql);
            if ($this->_db->my_num_rows($sqlResult) > 0) {
                while ($user = $this->_db->my_fetch_object($sqlResult)) {
                    if ($user->active == 0) { // check user status
                        $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.login.error.active_'.$user->status);
                    } else if ($user->deleted == 1) {
                        $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.login.error.delete');
                    } else if ($user->is_verified == 0) {
						$mobile_number  = (!empty($user->mobile)) ? $user->mobile : '';
						$customer_id 	= (!empty($user->id)) ? $user->id : '';
						$date_of_birth 	= (!empty($user->date_of_birth)) ? $user->date_of_birth : '';
						$firstname		= (!empty($user->firstname)) ? $user->firstname : '';
						$lastname 		= (!empty($user->lastname)) ? $user->lastname : '';
						// get random otp key
						$otp = $this->random_key(4);
						// set OTP message text
						$otp_message = 'Your Verification Code is '.$otp.'. Please use this to verify your NightOwl account.';
						// send otp to user for authentication
						$sent_otp = $this->send_otp($mobile_number,$otp,$otp_message);
						// remove temp otp log
						$query = "Delete FROM ".$this->users_otp_table." WHERE customer_id = $customer_id";
						$this->_db->my_query($query);
						// insert otp details in db
						//$query = "INSERT INTO ".$this->users_otp_table." (email,mobile_number,otp,otp_type,customer_id,date_of_birth,firstname,lastname) VALUES('".mysql_real_escape_string($ACC_EMAIL)."','".mysql_real_escape_string($mobile_number)."','".mysql_real_escape_string($otp)."','2','".$customer_id."','".mysql_real_escape_string($date_of_birth)."','".mysql_real_escape_string($firstname)."','".mysql_real_escape_string($lastname)."')";
                        $query = "INSERT INTO ".$this->users_otp_table." (email,mobile_number,otp,otp_type,customer_id,date_of_birth,firstname,lastname) VALUES('".mysqli_real_escape_string($this->_db->_con,$ACC_EMAIL)."','".mysqli_real_escape_string($this->_db->_con,$mobile_number)."','".mysqli_real_escape_string($this->_db->_con,$otp)."','2','".$customer_id."','".mysqli_real_escape_string($this->_db->_con,$date_of_birth)."','".mysqli_real_escape_string($this->_db->_con,$firstname)."','".mysqli_real_escape_string($this->_db->_con,$lastname)."')";
                        if($this->_db->my_query($query)) {
							$user_otp_id = $this->_db->mysqli_insert_id();
						}
						// set response params
						$_result['customer_id']		= $customer_id;
						$_result['ACC_MOBILE']		= $mobile_number;
						$_result['otp'] 			= $otp;
						$_response['is_varified'] 	= 0;
						$_response['result_code']   = 1;
						$_response['result'] 		= $_result;
                        $_response['error_msg'] 	= $this->_common->langText($this->_locale,'txt.login.error.not_verified');
                    } else if ($user->password_hash == "" || $ACC_PASSWORD == $user->password_hash) {
						$customer_id = $user->id;
						// update customer's app version
						//$this->update_user_app_version($customer_id,$app_version);
						// generate barcode if not exist
						$user_barcode = $user->user_barcode_image;
						$barcode 	  = $user->user_barcode;
						if(empty($user_barcode)) {
							$acc_number = $user->acc_number;
							if($acc_number != '') {
								$barcode = $acc_number;
								include_once("ean_13_barcode.php");
								$user_barcode 	= $image_name;
								$barcode_digits = $this->_common->get_barcode_digits($barcode);
							}
						}
						if($user->password_hash == "") {
							$updateArray = array("password_hash" => $ACC_PASSWORD,"device_type" => $device_type,"device_token" => $device_token,"session_token" => $session_token,"last_login_at" => $current_date,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode,"mini_app_version" => $app_version,"vendor_id" => $vendor_id,"update_at" =>$current_date);
						} else {
							$updateArray = array("device_type" => $device_type,"device_token" => $device_token,"session_token" => $session_token,"last_login_at" => $current_date,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode,"mini_app_version" => $app_version,"vendor_id" => $vendor_id,"update_at" =>$current_date);
						}
						/*update facebook id and instagram id if not exit*/
                        if( isset($_REQUEST['facebook_id']) || isset($_REQUEST['instagram_id'])) {
							if(isset($_REQUEST['facebook_id'])) {
								$facebook_id 	= $this->_common->test_input($_REQUEST['facebook_id']);
								$updateArray = array("facebook_id"=>$facebook_id,"device_type" => $device_type,"device_token" => $device_token,"session_token" => $session_token,"last_login" => $current_date);
							}
							if(isset($_REQUEST['instagram_id'])) {
								$instagram_id 	= $this->_common->test_input($_REQUEST['instagram_id']);
								$updateArray = array("instagram_id"=>$instagram_id,"device_type" => $device_type,"device_token" => $device_token,"session_token" => $session_token,"last_login" => $current_date);
							}
							//return $customer_id;
							
						}
                        // clear previouse device details if exists
                        $update_sql = "UPDATE  " . $this->user_account . " set device_type = '' , device_token = '' where device_type = '$device_type' AND device_token = '$device_token'";
                        $updatesqlResult = $this->_db->my_query($update_sql);
                                            
						$whereClause = array("id" => $customer_id);
						$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
						
						$account_data 	= $this->_common->get_account_data_by_user_id($user->ACC_NUMBER);
						
						//update referral code;if user don't have referral code
						$my_reference_code = (!empty($user->my_reference_code)) ? $user->my_reference_code : '';
						$firstname = (isset($user->firstname)) ? trim($user->firstname) : "";
						$my_reference_code = $this->_common->update_referral_code($customer_id,$firstname,$my_reference_code);
						// get reward referral config data
						$columns = ['referral_invitation_description','referral_terms_n_condition'];
						$get_record = $this->_common->get_table_record($this->reward_config,$columns);
						$referral_invitation_detail = (!empty($get_record[0]->referral_invitation_description)) ? $get_record[0]->referral_invitation_description : '';
						$referral_terms_n_condition = (!empty($get_record[0]->referral_terms_n_condition)) ? $get_record[0]->referral_terms_n_condition : '';
						//get user profile		
						$profile_image 					= (!empty($user->profile_image)) ? $user->profile_image : '';
						/*end update facebook id and instagram id*/
                        $result['customer_id'] 			= $customer_id;
                        $result['ACC_FIRST_NAME'] 		= $firstname; 
						$result['ACC_SURNAME'] 			= (isset($user->lastname)) ? trim($user->lastname) : "";
						$result['ACC_EMAIL'] 			= (isset($user->email)) ? trim($user->email) : "";
						$result['ACC_MOBILE'] 			= (isset($user->mobile)) ? trim($user->mobile) : "";
						$result['ACC_DATE_OF_BIRTH']	= ($user->date_of_birth != '0000-00-00') ? $user->date_of_birth : '';
						$result['ACC_ADDR_1'] 			= (isset($user->address)) ? trim($user->address) : "";
						$result['ACC_POST_CODE'] 		= (isset($user->post_code)) ? trim($user->post_code) : "";
						$result['ACC_GENDER'] 			= (isset($user->gender)) ? trim($user->gender) : "";
						$result['ACC_STORE'] 			= (isset($user->store_id)) ? trim($user->store_id) : "";
						$result['country'] 				= (isset($user->country)) ? trim($user->country) : "";
						$result['session_token'] 		= (isset($user->session_token)) ? trim($user->session_token) : "";
						$result['is_push_send'] 		= (isset($user->is_push_send)) ? trim($user->is_push_send) : "";
						$result['user_barcode'] 		= IMG_URL.'user_barcodes/'.$user_barcode;
						$result['profile_pic'] 			= IMG_URL.'user_images/user.jpg';
						$result['profile_image'] 		= (!empty($profile_image)) ? IMG_URL.'user_images/'.$profile_image : IMG_URL.'user_images/user.jpg';
						$result['acc_number'] 			= (isset($user->acc_number)) ? $user->acc_number : "";
						$result['ACC_DATE_ADDED'] 		= (isset($user->created_on)) ? date('Y-m-d H:i:s',strtotime($user->created_on)) : "";	
						$result['my_reference_code'] 	= $my_reference_code;
						$result['referral_invitation_detail'] 	= $referral_invitation_detail;
						$result['referral_terms_n_condition'] 	= $referral_terms_n_condition;
						
                        $_response['result_code'] = 1;
                        $_response['is_varified'] = 1;
                        $_response['result'] = $result;
                        $_response['success_msg'] = $this->_common->langText($this->_locale,'txt.login.success');
						
					} else {
                        $_response['result_code'] = 0;
                        $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.login.error.pswdnotexist');
                    }
                    
                }
            } else {
                $_response['result_code'] = 0;
                $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.login.error.usrnotexist');
            }
        }
        return $_response;
    } //end login()
    
    /**
    * @method social_login() is use to check user is login through social sites(facebook/intragram) or not.
    * @access public
    * @return user_id
    */
    public function social_login() { 
		$session_token 	= uniqid(); 
		$current_date = CURRENT_TIME;
		$device_type 	= $this->_common->test_input($_REQUEST['device_type']);
		$device_token 	= $this->_common->test_input($_REQUEST['device_token']);
		
		if(isset($_REQUEST['facebook_id']) || isset($_REQUEST['instagram_id'])) {
			if(isset($_REQUEST['facebook_id'])) {
				$facebook_id = $this->_common->test_input($_REQUEST['facebook_id']);
				 $where = " WHERE facebook_id = '".$facebook_id."'";
			} else if(isset($_REQUEST['instagram_id'])) {
				$instagram_id = $this->_common->test_input($_REQUEST['instagram_id']);
				$where = " WHERE instagram_id = '".$instagram_id."'";
			}
			$sql = "SELECT * FROM ".$this->user_account." ".$where."";
			$result = $this->_db->my_query($sql);
			$array =array();
			if($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				$user 	= $this->_common->get_user_data_by_user_id($row->id);
				if (!empty($user)) {
					if($user->active == 0) {
						$_response['result_code'] = 0;
                        $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.user.social_login.error.active_'.$user->status);
					} else {
						$this->_common->clean_device_token($device_token); //clear privous device tokens from database
						
						$updateArray = array("device_type" => $device_type,"device_token" => $device_token,"session_token" => $session_token,"last_login_at" => $current_date);
						$whereClause = array("id" => $row->id);
						$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");						
						//update referral code;if user don't have referral code
						$my_reference_code = (!empty($user->my_reference_code)) ? $user->my_reference_code : '';
						$firstname = (isset($user->firstname)) ? trim($user->firstname) : "";
						$my_reference_code = $this->_common->update_referral_code($customer_id,$firstname,$my_reference_code);
						// get reward referral config data
						$columns = ['referral_invitation_description','referral_terms_n_condition'];
						$get_record = $this->_common->get_table_record($this->reward_config,$columns);
						$referral_invitation_detail = (!empty($get_record[0]->referral_invitation_description)) ? $get_record[0]->referral_invitation_description : '';
						$referral_terms_n_condition = (!empty($get_record[0]->referral_terms_n_condition)) ? $get_record[0]->referral_terms_n_condition : '';

						// set response params
						$customer_id = $user->id;
						$resultArray['customer_id'] 		= $user->id;
						$resultArray['ACC_FIRST_NAME'] 		= $user->firstname;
						$resultArray['ACC_SURNAME'] 		= $user->lastname;
						$resultArray['ACC_EMAIL'] 			= $user->email;
						$resultArray['ACC_MOBILE'] 			= $user->mobile;
						$resultArray['ACC_DATE_OF_BIRTH']	= $user->date_of_birth;
						$resultArray['ACC_ADDR_1'] 			= $user->address;
						$resultArray['ACC_POST_CODE'] 		= $user->post_code;
						$resultArray['ACC_GENDER'] 			= $user->gender;
						$resultArray['country'] 			= $user->country;
						$resultArray['session_token'] 		= $user->session_token;
						$resultArray['is_push_send'] 		= (isset($user->is_push_send) && !empty($user->is_push_send)) ? $user->is_push_send : '';
						$resultArray['user_barcode'] 		= IMG_URL.'user_barcodes/'.$user->user_barcode_image;
						$resultArray['profile_pic'] 		= IMG_URL.'user_images/user.jpg';
						$resultArray['profile_image'] 		= (!empty($profile_image)) ? IMG_URL.'user_images/'.$profile_image : IMG_URL.'user_images/user.jpg';
						$resultArray['acc_number'] 			= (isset($user->acc_number)) ? $user->acc_number : "";
						$resultArray['ACC_DATE_ADDED'] 		= $user->created_on;
						$resultArray['my_reference_code'] 	= $my_reference_code;
						$resultArray['referral_invitation_detail'] 	= $referral_invitation_detail;
						$resultArray['referral_terms_n_condition'] 	= $referral_terms_n_condition;
						
						$_response['result_code'] = 1;
						$_response['result'] = $resultArray;
						$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.user.social_login.success');
					}
				} else {
					$_response['result_code'] = 0;
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.user.social_login.error.notexist');
				}
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.user.social_login.error.notexist');
			}
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.user.social_login.error.required');
        }
		return $_response;
	}// end check_facebook_user()
    
    /**
     * @method change_user() is use to update user profile by him self.
     * @access public
     * @return array
     */
    public function change_user() {
        $current_date = CURRENT_TIME;
        $_response['result_code'] = 0;
        $_response['is_verified'] = 1;
		//if($this->_common->test_input($_REQUEST['session_token']) != '' && $this->_common->check_session($_REQUEST['session_token'])) {
		if($this->_common->test_input($_REQUEST['customer_id']) != '') {
			if(isset($_REQUEST['ACC_EMAIL'])) {
				$email 		= $this->_common->test_input($_REQUEST['ACC_EMAIL']);
			}
            $mobile 		= $this->_common->test_input($_REQUEST['ACC_MOBILE']);
            $firstname 		= $this->_common->test_input($_REQUEST['ACC_FIRST_NAME']);
            $lastname 		= $this->_common->test_input($_REQUEST['ACC_SURNAME']);
            $date_of_birth 	= $this->_common->test_input($_REQUEST['ACC_DATE_OF_BIRTH']);
            $address 		= $this->_common->test_input($_REQUEST['ACC_ADDR_1']);
            $post_code 		= $this->_common->test_input($_REQUEST['ACC_POST_CODE']);
            $country 		= $this->_common->test_input($_REQUEST['country']);
            $gender 		= $this->_common->test_input($_REQUEST['ACC_GENDER']);
            $store_id 		= $this->_common->test_input($_REQUEST['ACC_STORE']);
            if($gender == ''){
				$gender = 0;
			}
            $profile_image = $this->_common->file_image_upload();
            if (!empty($firstname) && !empty($lastname) && !empty($mobile)) {
				$customerId = $_REQUEST['customer_id'];
				// fetch other account details with requested mobile number
				$sql = "SELECT id FROM " . $this->user_account . " WHERE id != '".$customerId."' AND mobile = '".$mobile."'";
				$sqlResult = $this->_db->my_query($sql);
				if ($this->_db->my_num_rows($sqlResult) > 0) {
					// return request, if mobile associated with another user
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.change_user.error.mobile_exist');
					return $_response;
				}
				// get user details from id
				$user 		= $this->_common->get_user_data_by_user_id($customerId);
				$acc_number = $user->acc_number;
                if(empty($profile_image)) {
                    $profile_image = (!empty($user->profile_image)) ? $user->profile_image : '';
                }
				if($acc_number != '') {
					define('ACCESSKEY',$this->_config->getKeyValue("access_key"));
					$sync_mssql = new sync_mssql();  // create instance syn_mssql class--
					$sync_mssql->update_mssql_user($_REQUEST,$acc_number);
				}
				//$customerId = $this->_common->check_session($session_token);
				// set update values
				$whereClause = array("id" => $customerId);
                $updateArray = array("country" => $country,"mobile"=>$mobile,"firstname"=>$firstname,"lastname"=>$lastname,"date_of_birth"=>$date_of_birth,"address"=>$address,"post_code"=>$post_code,"gender"=>$gender,"update_at"=>$current_date,"profile_image"=>$profile_image);
				// set account varify value, in case of diffrent mobile number
				if(!empty($user->mobile) && $user->mobile != $mobile) {
					$updateArray['is_verified'] = 0;
					$_response['is_verified'] = 0;
				}
				$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
				
				if ($update) {
					$_response['result_code'] = 1;
                    $profile_image = (!empty($profile_image)) ? IMG_URL.'user_images/'.$profile_image : IMG_URL.'user_images/user.jpg';		
					$_response['result'] = $profile_image;
					$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.change_user.success');
				} else {
					$_response['result_code'] = 0;
					$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.change_user.error.unable');
				}
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.required');
			}
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.error.session');
        }
        return $_response;
    } //end change_user()
    
    /**
     * @method get_user_profile() is use to get user profile .
     * @access public
     * @return array
     */
    public function get_user_profile() {
        $current_date = CURRENT_TIME;
        $app_version = (isset($_REQUEST['avc'])) ? $_REQUEST['avc'] : "";
		$os = (isset($_REQUEST['os_version'])) ? $_REQUEST['os_version'] : "";
		$device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
        
		//if($this->_common->test_input($_REQUEST['session_token']) != '' && $this->_common->check_session($_REQUEST['session_token'])) {
		if($this->_common->test_input($_REQUEST['customer_id']) != '') {
			//$customerId 	= $this->_common->check_session($_REQUEST['session_token']);
			$customerId 	= $this->_common->test_input($_REQUEST['customer_id']);
            $user 			= $this->_common->get_user_data_by_user_id($customerId);
            if (!empty($user)) {
                $acc_number = (!empty($user->acc_number)) ? $user->acc_number : 0;
                $reward_card_id = (!empty($user->reward_card_id)) ? $user->reward_card_id : 1;
                // set user's applicable loyalty card
                $assign_reward_card = $this->assignRewardLoyaltyCard($acc_number,$reward_card_id);
                $reward_card_detail['card_name'] = '';
                $reward_card_detail['end_date'] = '';
                $reward_card_detail['card_image'] = IMG_URL.'No_Card_Assign.png';
                $reward_card_detail['background_color'] = '000000';
                $reward_card_detail['font_color'] = '000000';
                $reward_card_detail['card_type_color'] = '000000';
                $reward_card_detail['logo_color'] = '000000';
                $reward_card_detail['sub_title_color'] = '000000';
                $reward_card_detail['balance_point'] = 0;
                $reward_card_id = (!empty($user->reward_card_id)) ? $user->reward_card_id : $assign_reward_card;
                if(!empty($reward_card_id)) {
                    //get assign card details
                    $tabel = 'ava_reward_cards';
                    $columns = ['card_name','description','end_date','card_image','background_color','font_color','font_color_2','logo_color','sub_title_color'];
                    $where = ['id'=>$reward_card_id,'status'=>1,'is_deleted'=>0];
                    $reward_data = $this->_common->get_table_record($tabel,$columns,$where);
                    $reward_card_detail['card_name'] = (!empty($reward_data[0]->card_name)) ? $reward_data[0]->card_name : '';
                    $reward_card_detail['description'] = (!empty($reward_data[0]->description)) ? $reward_data[0]->description : '';
                    $reward_card_detail['end_date'] = (!empty($reward_data[0]->end_date)) ? $reward_data[0]->end_date : '';
                    $reward_card_detail['card_image'] = (!empty($reward_data[0]->card_image)) ? IMG_URL.'reward_images/'.$reward_data[0]->card_image : '';
                    $reward_card_detail['background_color'] = (!empty($reward_data[0]->background_color)) ? $reward_data[0]->background_color : '';
                    $reward_card_detail['font_color'] = (!empty($reward_data[0]->font_color)) ? $reward_data[0]->font_color : '';
                    $reward_card_detail['card_type_color'] = (!empty($reward_data[0]->font_color_2)) ? $reward_data[0]->font_color_2 : '';
                    $reward_card_detail['logo_color'] = (!empty($reward_data[0]->logo_color)) ? $reward_data[0]->logo_color : '';
                    $reward_card_detail['sub_title_color'] = (!empty($reward_data[0]->sub_title_color)) ? $reward_data[0]->sub_title_color : '';

                }
                //get transcation balance point         
                $reward_card_detail['balance_point'] = $this->_common->getBalancePoint($customerId);
                $mobileNum = $user->mobile;
                if(!empty($mobileNum) && (($device_type == 'ios' && $app_version >= 2.8) || ($device_type == 'android' && $app_version >= 30)) ){
                    $mobileFirst = substr($mobileNum,0,1);
                    if($mobileFirst != '0'){
                        $mobileNum = '0'.$mobileNum;
                    }
                }
                //update referral code;if user don't have referral code
				$my_reference_code = (!empty($user->my_reference_code)) ? $user->my_reference_code : '';
				// get reward referral config data
				$columns = ['referral_invitation_description','referral_terms_n_condition'];
				$get_record = $this->_common->get_table_record($this->reward_config,$columns);
				$referral_invitation_detail = (!empty($get_record[0]->referral_invitation_description)) ? $get_record[0]->referral_invitation_description : '';
				$referral_terms_n_condition = (!empty($get_record[0]->referral_terms_n_condition)) ? $get_record[0]->referral_terms_n_condition : '';
				// get total entries
				$total_entries = $this->_common->get_cte_entry_points($acc_number); 
				((!empty($reward_card_detail))?$result['reward_card_detail'] = $reward_card_detail:'');
                $result['customer_id'] 			= $customerId;
				$result['ACC_FIRST_NAME'] 		= $user->firstname;
				$result['ACC_SURNAME'] 			= $user->lastname;
				$result['ACC_EMAIL'] 			= $user->email;
				$result['ACC_MOBILE'] 			= $mobileNum;
				$result['ACC_DATE_OF_BIRTH']	= ($user->date_of_birth != '0000-00-00') ? $user->date_of_birth : '';
				$result['ACC_ADDR_1'] 			= (!empty($user->address)) ? $user->address : '';
				$result['ACC_POST_CODE'] 		= (!empty($user->post_code)) ? $user->post_code : '';
				$result['ACC_GENDER'] 			= (!empty($user->gender)) ? $user->gender : 0;
				$result['country'] 				= (!empty($user->country)) ? $user->country : 0;
				$result['user_barcode'] 		= IMG_URL.'user_barcodes/'.$user->user_barcode_image;
				$result['profile_pic'] 			= IMG_URL.'user_images/user.jpg';
				//get user profile		
                $profile_image 					= (!empty($user->profile_image)) ? $user->profile_image : '';

                $result['profile_image'] 		= (!empty($profile_image)) ? IMG_URL.'user_images/'.$profile_image : IMG_URL.'user_images/user.jpg';
				$result['ACC_DATE_ADDED'] 		= date('Y-m-d H:i:s',strtotime($user->created_on));	
				$result['ACC_STORE'] 			= (!empty($user->store_id)) ? $user->store_id : 0;
				$result['is_push_send'] 	    = $user->is_push_send;
				$result['session_token'] 		= (!empty($user->session_token)) ? $user->session_token : 0;
				$result['acc_number'] 			= (!empty($user->acc_number)) ? $user->acc_number : 0;
				$result['total_entries'] 		= $total_entries.$this->_common->langText($this->_locale,'txt.cte.total_entries');
				$result['my_reference_code'] 	= $my_reference_code;
				$result['referral_invitation_detail'] 	= $referral_invitation_detail;
				$result['referral_terms_n_condition'] 	= $referral_terms_n_condition;
				$_response['result_code'] = 1;
				$_response['result'] = $result;
				$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.get_user_profile.success');
			} else {
				$_response['result_code'] = 0;
				$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_user_profile.error.notfound');
			}
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.error.session');
        }
        return $_response;
    } //end get_user_profile()
    
    /**
     * @method change_user_password() is use to update current password if user know the current password.
     * @access public
     * @return array
     */
    public function change_user_password() {
        $current_date = CURRENT_TIME;
		//if($this->_common->test_input($_REQUEST['session_token']) != '' && $this->_common->check_session($_REQUEST['session_token'])) {
		if($this->_common->test_input($_REQUEST['customer_id']) != '') {
            if ($this->_common->test_input($_REQUEST['USER_PASSWORD']) != '' && $this->_common->test_input($_REQUEST['NEW_USER_PASSWORD']) != '') {
                $USER_PASSWORD = md5($this->_common->test_input($_REQUEST['USER_PASSWORD']));
                $NEW_USER_PASSWORD = md5($this->_common->test_input($_REQUEST['NEW_USER_PASSWORD']));
                
				//$customerId 	= $this->_common->check_session($_REQUEST['session_token']);
				$customerId 	= $this->_common->test_input($_REQUEST['customer_id']);
                $selectCustomer = "SELECT * FROM " . $this->user_account . " WHERE id  = '$customerId'";
                $resultCustomer = $this->_db->my_query($selectCustomer);
                $customer = $this->_db->my_fetch_object($resultCustomer);
                if ($USER_PASSWORD == $customer->password_hash) { // check the current entered password
                    $updateArray = array("password_hash" => $NEW_USER_PASSWORD);
                    $whereClause = array("id" => $customerId);
                    $update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
                    if ($update) {
                        $_response['result_code'] = 1;
                        $_response['success_msg'] = $this->_common->langText($this->_locale,'txt.change_user_password.success');
                    } else {
                        $_response['result_code'] = 0;
                        $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.change_user.error.unable');
                    }
                } else {
                    $_response['result_code'] = 0;
                    $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.change_user_password.error.current_password');
                }
            } else {
                $_response['result_code'] = 0;
                $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.change_user_password.error.required');
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.error.session');
        }
        return $_response;
    } //end change_user_password()
    
    /**
     * @method forgot_password() Forget password function allows user to send request for resetting password .
     * @access public
     * @return void
     */
    public function forgot_password() {
        require_once ("libraries/sendMailCron.php");
        $current_date = CURRENT_TIME;
        if ($this->_common->test_input($_REQUEST['ACC_EMAIL']) != '') {
            $USER_EMAIL = $this->_common->test_input($_REQUEST['ACC_EMAIL']);
            if (!filter_var($USER_EMAIL, FILTER_VALIDATE_EMAIL) && $USER_EMAIL != "") {
                $_response['result_code'] = 0;
                $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.register.error.invalidemail');
            } else {
                $profileDetail = $this->_common->get_user_data_by_email($USER_EMAIL);
                if (!empty($profileDetail)) {
                    $customer_id = $profileDetail->id;
                    $firstName = ucfirst($profileDetail->firstname);
                    $lastName = ucfirst($profileDetail->lastname);
                    //$verification_code = sha1(rand(0,1000));
                    $password = uniqid();
                    $ACC_PASSWORD = md5($password);
                    
                    $updateArray = array("password_hash" => $ACC_PASSWORD);
                    $whereClause = array("id" => $customer_id);
                    $update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
                    $emailBody = $firstName . ' ' . $lastName.'{#STRINGBREAKER#}'.$password;
                    
                    //get templdate data
                    $sql = "SELECT * FROM `ava_email_template` WHERE `purpose` = 'reset_password' AND `language` = 'en'";
                    $resulttemp = $this->_db->my_query($sql);
                    $row    = $this->_db->my_fetch_array($resulttemp);
                    $row2 = (!empty($row[2]))?$row[2]:'';
                    $row3 = (!empty($row[3]))?$row[3]:'';
                    $emailTemplateData = $row2."||".$row3;
                    //$emailBody .= $password;
                    sendActivationMailAction($USER_EMAIL, 'reset_password', 'en', $emailBody,$emailTemplateData); // send link on email
                    
                    $_response['result_code'] = 1;
                    $_response['success_msg'] = $this->_common->langText($this->_locale,'txt.forgot_password.success');
                } else {
                    $_response['result_code'] = 0;
                    $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.forgot_password.error.email_not_exist');
                }
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.forgot_password.error.required');
        }
        return $_response;
    } //end forgot password()
    
    /**
     * @method reset_user_password() is the web url function access by user to reset_password .
     * @access public
     * @return void
     * @NOTE This function is temporary function till reset password form will not create
     */
    public function reset_user_password() {
        if ($this->_common->test_input($_REQUEST['verification_code']) != '') {
            $USER_NUMBER = $this->_common->get_account_id_by_verification($_REQUEST['verification_code']);
            $profileDetail = $this->_common->get_user_data_by_user_id($USER_NUMBER);
            if (!empty($profileDetail)) {
                if ($this->_common->test_input($_REQUEST['USER_PASSWORD']) != '') {
                    $USER_PASSWORD = md5($this->_common->test_input($_REQUEST['USER_PASSWORD']));
                    
                    $updateArray = array("ACC_PASSWORD" => $USER_PASSWORD);
                    $whereClause = array("ACC_NUMBER" => $USER_NUMBER);
                    $update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
                    $_response['result_code'] = 1;
                    $_response['success_msg'] = 'Password Reset Successfully.';
                } else {
                    $_response['result_code'] = 0;
                    $_response['error_msg'] = 'Password field can\'t be black.';
                }
            } else {
                $_response['result_code'] = 0;
                $_response['error_msg'] = 'Error 300 (net::ERR_INVALID_URL): Unknown error';
            }
        } else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = 'Error 300 (net::ERR_INVALID_URL): Unknown error';
        }
        return $_response;
    } // end reset_user_password
    
    public function logout() {
		//if($this->_common->test_input($_REQUEST['session_token']) != '' && $this->_common->check_session($_REQUEST['session_token'])) {
		if($this->_common->test_input($_REQUEST['customer_id']) != '') {
			$customerId 	= $this->_common->test_input($_REQUEST['customer_id']);
			$updateArray = array("session_token" => " ","device_token"=>'');
			$whereClause = array("id" => $customerId);
			$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
			
			$_response['result_code'] = 1;
            $_response['success_msg'] = $this->_common->langText($this->_locale,'txt.logout.success');
		} else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.error.session');
        }
		return $_response;
	} //logout
	
	public function push_setting() {
		//if($this->_common->test_input($_REQUEST['session_token']) != '' && $this->_common->check_session($_REQUEST['session_token'])) {
		if($this->_common->test_input($_REQUEST['customer_id']) != '' ) {
			//$customerId 	= $this->_common->check_session($_REQUEST['session_token']);
			$customerId 	= $this->_common->test_input($_REQUEST['customer_id']);
			$is_push_send = $this->_common->test_input($_REQUEST['is_push_send']);
			/*if($is_push_send == 0) {
				$is_push_send = 1;
			} else {
				$is_push_send = 0;
			}*/
			$is_push_send = ($is_push_send == 1) ? 1 : 0;
			$updateArray = array("is_push_send" => $is_push_send);
			$whereClause = array("id" => $customerId);
			$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
			
			$_response['result_code'] = 1;
			$_response['success_msg'] = $this->_common->langText($this->_locale,'txt.push_setting.success');
		} else {
            $_response['result_code'] = 0;
            $_response['error_msg'] = $this->_common->langText($this->_locale,'txt.error.session');
        }
		return $_response;
	}
    
    public function get_store() {
		$lat1 = $this->_common->test_input($_REQUEST['lat']);
        $lng1 = $this->_common->test_input($_REQUEST['lng']);
		$selectStore = "SELECT * FROM " . $this->stores_table . " WHERE status = '1' and display_on_app = 1 ORDER BY store_id";
		$resultStore = $this->_db->my_query($selectStore);
		if ($this->_db->my_num_rows($resultStore) > 0) {	
			while($row = $this->_db->my_fetch_object($resultStore)) {
				$array = array();
				$lat2 				= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
				$lng2 				= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
				$distance 			= $this->distance($lat1, $lng1, $lat2, $lng2, $unit);
				$array['id'] 		= (isset($row->store_id)) ? trim($row->store_id) : "";
				$array['ms_store_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
				$array['name'] 		= (isset($row->store_name)) ? trim($row->store_name) : ""; 
				$array['outlet_id'] = (isset($row->ms_store_id)) ? trim($row->ms_store_id) : "";
				$array['addr_1'] 	= (isset($row->store_address_1)) ? trim($row->store_address_1) : "";
				//$array['addr_2'] 	= (isset($row->store_address_2)) ? trim($row->store_address_2) : "";
				$array['addr_2'] 	= "";
				$array['addr_3'] 	= $array['addr_1']." ".$array['addr_2'];
				$array['post_code'] = (isset($row->store_post_code)) ? trim($row->store_post_code) : "";
				$array['phone_no'] 	= "07 ".(isset($row->store_phone)) ? trim($row->store_phone) : "";
				$array['lat'] 		= (isset($row->store_latitude)) ? trim($row->store_latitude) : "";
				$array['lng'] 		= (isset($row->store_longitude)) ? trim($row->store_longitude) : "";
				$array['distance'] 	= strval(round($distance,2));
				$array['open_hours'] = (isset($row->open_hours)) ? trim($row->open_hours) : "";
				$result[] = $array;
			}
			foreach ($result as $key => $row) {
				$mid[$key]  = $row['distance'];
			}
			array_multisort($mid, SORT_ASC, $result);

			$_response['result_code'] = 1;
			$_response['result'] = $result;
		} else {
			$_response['result_code'] = 0;
			$_response['error_msg'] = $this->_common->langText($this->_locale,'txt.get_store.error.no_store');
		}
		return $_response;
	} //end get_offer()
	
	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return ($miles * 1.609344);
		}
	}
	
	public function get_open_hours($index='0') {
		if($index == 1) {
				return "5am to Midnight Mon to Thurs & Sun, 24 hrs Fri & Sat";
		} 
		if($index == 2) {
				return "24 hours";
		} 
		if($index == 3) {
				return "5am to Midnight Mon – Thurs, 24 Hours Fri to Sun";
		} 
		if($index == 4) {
				return "5am to 11pm";
		} 
		if($index == 5) {
				return "5am to Midnight";
		} 
	}
	
	function mssql_test($result) {
		$result = array('customer_id' => '21786', 'ACC_FIRST_NAME' => 'ashish', 'ACC_SURNAME' => 'sabestian','ACC_EMAIL' => '','ACC_MOBILE' => '432870026','ACC_DATE_OF_BIRTH' => '1990-12-10','ACC_ADDR_1' => '','ACC_POST_CODE' => '','ACC_GENDER' => 1,'country' => '','session_token' => '5654741c6981e','user_barcode' => 'http://192.168.0.108/coyote_dev/uploads/user_barcodes/21786302.png','profile_pic' => 'http://192.168.0.108/coyote_dev/uploads/user_images/user.jpg', 'ACC_DATE_ADDED' => '2015-11-25 00:28:41','is_push_send' => 0);
		define('ACCESSKEY',$this->_config->getKeyValue("access_key"));
        $sync_mssql = new sync_mssql();  // create instance syn_mssql class--
		
		$sync_mssql->test($result);die;
		
		
		echo $this->_sync_mssql->test($result);
		die;
		$con = $this->_mssql->mssql_db_connection();
		/*$sql = "select ACC_NUMBER,ACC_EMAIL from ACCOUNT ORDER BY ACC_NUMBER DESC";
		$getData = $this->_mssql->my_mssql_query($sql,$con);
		if($this->_mssql->mssql_num_rows_query($getData) > 0 ) { 
			$emailrow = $this->_mssql->mssql_fetch_object_query($getData);
			return $emailrow->ACC_NUMBER;  // return ACC_NUMBER if user already have account on live server
		}*/
		$current_date 		= CURRENT_TIME;
		//get data to update in mmsql ACCOUNT table.
		$email 				= (isset($result['ACC_EMAIL'])) ? trim($result['ACC_EMAIL']) : ""; 
		$ACC_PASSWORD 		= (isset($result['ACC_PASSWORD'])) ? trim($result['ACC_PASSWORD']) : ""; 
		$ACC_FIRST_NAME 	= (isset($result['ACC_FIRST_NAME'])) ? trim($result['ACC_FIRST_NAME']) : "";
		$ACC_SURNAME 		= (isset($result['ACC_SURNAME'])) ? trim($result['ACC_SURNAME']) : "";
		$ACC_MOBILE 		= (isset($result['ACC_MOBILE'])) ? trim($result['ACC_MOBILE']) : "";
		$ACC_DATE_OF_BIRTH 	= (isset($result['ACC_DATE_OF_BIRTH'])) ? trim($result['ACC_DATE_OF_BIRTH']) : "";
		$ACC_ADDR_1 		= (isset($result['ACC_ADDR_1'])) ? trim($result['ACC_ADDR_1']) : "";
		$ACC_POST_CODE 		= (isset($result['ACC_POST_CODE'])) ? trim($result['ACC_POST_CODE']) : "";
		$ACC_GENDER 		= (isset($result['ACC_GENDER'])) ? trim($result['ACC_GENDER']) : "";
		$ACC_DATE_ADDED 	= (isset($current_date)) ? trim($current_date) : "";
				$status = '1';

		$newInsertId		= '2100000016';
		$insertSQL = "INSERT ACCOUNT  (ACC_NUMBER, ACC_FIRST_NAME, ACC_SURNAME, ACC_EMAIL, ACC_MOBILE, ACC_DATE_OF_BIRTH, ACC_ADDR_1, ACC_GENDER, ACC_DATE_ADDED, ACC_STATUS) VALUES ('$newInsertId','$ACC_FIRST_NAME','$ACC_SURNAME','$email','$ACC_MOBILE','$ACC_DATE_OF_BIRTH', '$ACC_ADDR_1','$ACC_GENDER','$ACC_DATE_ADDED','$status')";
		
		$getData = $this->_mssql->my_mssql_query($insertSQL,$con);
		echo $getData;
		die;
	}
    
    
    function facebook_feed() {
        $data  = $this->_common->file_get_contents_curl("https://graph.facebook.com/NightOwlConvenience/feed?access_token=563114237125797|ytpRuiPLaMp8LokDXNltnLji4yA");
        $postData = json_decode($data, true);
       
        $i = 1; 
        $_response['message'] = $this->_common->langText($this->_locale,'txt.register.no.facebook_feed');		
        $_response['result_code'] = 0;
        $facebookfeed = array();
        if(!empty($postData['data'])) {
                $j = 0;
            foreach($postData['data'] as $dataRecords){
                if(!empty($dataRecords['message'])) {
                    $facebookfeed[$j]['message'] = $dataRecords['message'];
                    $facebookfeed[$j]['created_time'] = $dataRecords['created_time'];
                    $facebookfeed[$j]['link'] = $dataRecords['link'];
                   $j++;
                }
                $i++;
                $_response['result_code'] = 1;
            }
                $_response['facebook_feed'] = $facebookfeed;
        }
        return $_response;
    }
    
    /**
     * @Function used to update user's current app version
     * @input int customer_id, string app_version
     * @access private
     * @return void
    */
	private function update_user_app_version($customer_id=0,$app_version='') {
		if(!empty($app_version) && !empty($customer_id)) {
			$datetime = date('Y-m-d H:i:s');
			// update member's app version
			$update_array = array("mini_app_version" => $app_version,"update_at" =>$datetime);// Update values
			$where_clause = array("id" => $customer_id);
			$this->_db->UpdateAll($this->users_table, $update_array, $where_clause, "");	
		}
		return true;
	}
	
	/**
	 * Function used to update user's device details
	 * @param null
	 * @return array
	 * @access public
	 * 
	 */
	public function update_device_details() {
		
		// set post params
		$customer_id  = $this->_common->test_input($_REQUEST['customer_id']);
		$device_type  = $this->_common->test_input($_REQUEST['device_type']);
        $device_token = $this->_common->test_input($_REQUEST['device_token']);
        $app_version  = $this->_common->test_input($_REQUEST['avc']);
		$datetime  = date('Y-m-d H:i:s');
		// set default response for invalid request
		$_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.register.error.device_details');
		if(!empty($customer_id) && !empty($device_type) && !empty($device_token) && !empty($app_version)) {
			// update member's device details
			$update_array = array("device_type" => $device_type,"device_token" => $device_token,"mini_app_version" => $app_version,"update_at" =>$datetime);
			$where_clause = array("id" => $customer_id);
			$this->_db->UpdateAll($this->users_table, $update_array, $where_clause, "");
			// set success response 
			$_response['result_code'] = 1;
			$_response['message'] = $this->_common->langText($this->_locale,'txt.register.success.device_details');		
		}
		return $_response;
	}
	
	/**
	 * Function used to verify user's mobile number and send otp
	 * @param mobile_number(number)
	 * @return array
	 * @access public
	 * 
	 */
	 public function register_valid_account() {
        
        // set post params
        $email 			= $this->_common->test_input(trim($_REQUEST['ACC_EMAIL']));
        $password 		= $this->_common->test_input(trim($_REQUEST['ACC_PASSWORD']));
        $mobile_number 	= $this->_common->test_input(trim($_REQUEST['ACC_MOBILE']));
        $firstname 		= $this->_common->test_input(trim($_REQUEST['ACC_FIRST_NAME']));
        $lastname 		= $this->_common->test_input(trim($_REQUEST['ACC_SURNAME']));
        $date_of_birth	= (!empty($_REQUEST['ACC_DATE_OF_BIRTH'])) ? date('Y-m-d',strtotime($_REQUEST['ACC_DATE_OF_BIRTH'])) : '';
        $gender			= $this->_common->test_input(trim($_REQUEST['ACC_GENDER']));
        $store_id		= $this->_common->test_input(trim($_REQUEST['ACC_STORE']));
        $reference_code		= $this->_common->test_input(trim($_REQUEST['reference_code']));
        $profile_image = $this->_common->file_image_upload();
        $temp_customer_oldid = $temp_customer_id = (!empty($_REQUEST['temp_customer_id'])) ? trim($_REQUEST['temp_customer_id']) : '';	
		$facebook_id = '';
        if(isset($_REQUEST['facebook_id'])) {
			$facebook_id = $this->_common->test_input($_REQUEST['facebook_id']);
		}
        $instagram_id = '';
		if(isset($_REQUEST['instagram_id'])) {
			$instagram_id = $this->_common->test_input($_REQUEST['instagram_id']);
		}
		// set response param as default
        $_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.register.invalid_request');
		// get random otp key
		$otp = $this->random_key(4);
		// set OTP message text 
		$otp_message = 'Your NightOwl verification code is '.$otp.'. Please enter it in your NightOwl app to verify your account. Thank you.';
		if (!empty($firstname) && !empty($lastname) && !empty($email) && !empty($password) && !empty($mobile_number)) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $_response['result_code'] 	= 0;
                $_response['message'] 	= $this->_common->langText($this->_locale,'txt.register.error.invalidemail');
            } else {
				// get user data if number exist in db
				$sql = "SELECT u.* FROM ".$this->user_account." u WHERE u.mobile = '$mobile_number' OR u.email = '$email'";
				$result = $this->_db->my_query($sql);
				if ($this->_db->my_num_rows($result) > 0) {
					$user = $this->_db->my_fetch_object($result);
					// set response message 
					$_response['message'] = $this->_common->langText($this->_locale,'txt.register.already_exist');
					if ($user->active == 0) {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.login.error.active_'.$user->status);
					} else if ($user->deleted == 1) {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.login.error.delete');
					}
					
					// set response user data
					$_result['customer_id']	= (!empty($user->id)) ? $user->id : 0;
					$_result['firstname'] 	= (!empty($user->firstname)) ? $user->firstname : '';
					$_result['lastname'] 	= (!empty($user->lastname)) ? $user->lastname : '';
					$_result['email'] 		= (isset($user->email)) ? trim($user->email) : "";
					$_result['ACC_MOBILE']  = (isset($user->mobile)) ? trim($user->mobile) : "";
					$_result['acc_number'] 	= (isset($user->acc_number)) ? $user->acc_number : "";
					// append result array in response array
					$_response['result'] 		= $_result;
					$_response['is_verified']	= (!empty($user->is_verified)) ? $user->is_verified : 0;
					return $_response;
				}
                
				// set response data, if sms already sent to user
				$sql = "SELECT * FROM ".$this->users_otp_table." WHERE otp_type = 1 AND (mobile_number = '$mobile_number' OR email = '$email')";
				$sql_result = $this->_db->my_query($sql);
				if ($this->_db->my_num_rows($sql_result) > 0) {
					$result = $this->_db->my_fetch_object($sql_result);
					$temp_customer_id	 = (isset($result->id)) ? trim($result->id) : 0;
					$temp_mobile_number  = (isset($result->mobile_number)) ? trim($result->mobile_number) : "";
                    
                   /* Update Otp table when temp_customer_id is same as id given by mobile end */
                   if($temp_customer_oldid ==  $temp_customer_id){
                        $update_array = array("email" => $email,"password"=>$password,"firstname"=>$firstname,"lastname"=>$lastname,"facebook_id"=> $facebook_id,"instagram_id"=>$instagram_id,"date_of_birth"=>$date_of_birth,"gender"=>$gender,"store_id"=>$store_id);
                        $where_clause = array("id" => $temp_customer_id);
                        $this->_db->UpdateAll($this->users_otp_table, $update_array, $where_clause, "");
                    }                        
                    if($temp_mobile_number == $mobile_number) {
						$otp = (isset($result->otp)) ? $result->otp : "";
						// set response message
						$_response['message'] = $this->_common->langText($this->_locale,'txt.register.already_sent_otp');
					} else {
						// send otp to user for authentication
						$sent_otp = $this->send_otp($mobile_number,$otp,$otp_message);
						// update new mobile number in temp user log
						$update_array = array("mobile_number" => $mobile_number,"otp"=>$otp);
						$where_clause = array("id" => $temp_customer_id);
						$this->_db->UpdateAll($this->users_otp_table, $update_array, $where_clause, "");
						$_response['message'] = $this->_common->langText($this->_locale,'txt.register.otp_success');	
					}
					// set response user data
					$_result['temp_customer_id'] = $temp_customer_id;
					$_result['otp'] 		= $otp;
					$_result['ACC_MOBILE'] 	= $mobile_number;
					// append result array in response array
					$_response['result'] = $_result;
					$_response['is_verified']	= 0;
					$_response['result_code'] 	= 1;
					return $_response;
				}
                
                //check reference_code if not empty
                $reference_customer_id = 0;
                if(!empty($reference_code)) {
                    $check_reference_code = "SELECT my_reference_code,id FROM ".$this->user_account." WHERE my_reference_code = '".$reference_code."' AND is_verified = 1 AND active = 1";
                    $reference_code_result = $this->_db->my_query($check_reference_code);
                    if ($this->_db->my_num_rows($reference_code_result) <= 0) {
                        $_response['message'] = $this->_common->langText($this->_locale,'txt.register.invalid_refernce_code');
                        return $_response;
                    } else {
                        $reference_code_data = $this->_db->my_fetch_object($reference_code_result);
                        $reference_customer_id	 = (isset($reference_code_data->id)) ? trim($reference_code_data->id) : 0;
                    }
                }
				
				// send otp to user for authentication
				$sent_otp = $this->send_otp($mobile_number,$otp,$otp_message);
				//$sent_otp = true;
				//if($sent_otp) {
					// insert otp details in db
					$query = "INSERT INTO ".$this->users_otp_table." (email,password,mobile_number,firstname,lastname,facebook_id,instagram_id,date_of_birth,otp,otp_type,gender,store_id,reference_code,profile_image) VALUES(
							'".mysqli_real_escape_string($this->_db->_con,$email)."',
							'".mysqli_real_escape_string($this->_db->_con,$password)."',
							'".mysqli_real_escape_string($this->_db->_con,$mobile_number)."',
							'".mysqli_real_escape_string($this->_db->_con,$firstname)."',
							'".mysqli_real_escape_string($this->_db->_con,$lastname)."',
							'".mysqli_real_escape_string($this->_db->_con,$facebook_id)."',
							'".mysqli_real_escape_string($this->_db->_con,$instagram_id)."',
							'".mysqli_real_escape_string($this->_db->_con,date('Y-m-d',strtotime($date_of_birth)))."',
							'".mysqli_real_escape_string($this->_db->_con,$otp)."',
							'1',
							'".mysqli_real_escape_string($this->_db->_con,$gender)."',
							'".mysqli_real_escape_string($this->_db->_con,$store_id)."',
							'".mysqli_real_escape_string($this->_db->_con,$reference_code)."',
							'".mysqli_real_escape_string($this->_db->_con,$profile_image)."')";
					if($this->_db->my_query($query)){
						$temp_customer_id = $this->_db->mysqli_insert_id();
						// set success params
						$_result['otp'] = $otp;
						$_result['temp_customer_id'] = $temp_customer_id;
						$_result['ACC_MOBILE'] = $mobile_number;
						// set response params
						$_response['result_code'] = 1;
						$_response['is_varified'] = 0;
						$_response['message'] = $this->_common->langText($this->_locale,'txt.register.otp_success');	
						$_response['result'] = $_result;
					} else {
						$_response['message'] = $this->_common->langText($this->_locale,'txt.register.otp_error');	
					}
				//}
			}
		}
		return $_response;
	 }
	 
	/**
	 * Function used to generate random number
	 * @param max_value , num_rows
	 * @return int
	 * @access private
	 * 
	 */
	function random_key($digits=4) {
		$i = 0; //counter
		$pin = ""; //our default pin is blank.
		while($i < $digits){
			//generate a random number between 0 and 9.
			$pin .= mt_rand(0, 9);
			$i++;
		}
		return $pin;
	}
	
	/**
	 * Function used to send otp pin code
	 * @input to_phone_no , otp_pin 
	 * @return bool
	 * @access public
	 *
	 */
	function send_otp($to_phone_no=0,$otp_pin=0,$message=''){
		$sent_otp = false;
		if(!empty($to_phone_no) && !empty($otp_pin)) {
			// get Twilio auth keys
			$account_sid 	= $this->_config->getKeyValue("twilio_account_sid");
			$auth_token 	= $this->_config->getKeyValue("twilio_auth_token");
			$from_no 		= $this->_config->getKeyValue("twilio_from_no");
			$to_phone_no	= $to_phone_no;
			// include twilio class         
			require('./twillo/Services/Twilio.php');
			$client = new Services_Twilio($account_sid, $auth_token);
			$countryCode = '+61';
			$message = (!empty($message)) ? $message : 'Your Verification Code is '.$otp_pin.'.';
			if(!empty($to_phone_no)){
				try{
					// send otp in SMS
					$sendMesageDetail = $client->account->messages->create(array(
						'To' => trim($countryCode).''.trim($to_phone_no),
						'From' => $from_no,
						'Body' => $message,
					));
					$sent_otp = true;
				}catch(Exception $e){
					$sent_otp = false;
				}
			}
		}
		return $sent_otp;
	}
	
	/**
	 * Function used to authenticate otp pin code
	 * @input otp(int) , device_token(string) , mobile_number(number)
	 * @return array
	 * @access public
	 *
	 */
	public function verify_otp(){

		// get post params	
		$otp		 	= (!empty($_REQUEST['otp'])) ? trim($_REQUEST['otp']) : '';	
        $mobile_number 	= (!empty($_REQUEST['ACC_MOBILE'])) ? $_REQUEST['ACC_MOBILE'] : '';
        $customer_id 	= (!empty($_REQUEST['customer_id'])) ? $_REQUEST['customer_id'] : '';
		// set response param as default
        $_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.register.invalid_request');	
        if(!empty($otp) && !empty($mobile_number)) {
			// get otp details
			$sql = "SELECT uo.id as otp_id,uo.firstname,uo.lastname,uo.password,uo.date_of_birth,uo.facebook_id,uo.instagram_id,uo.email,uo.mobile_number,uo.gender,uo.store_id,uo.reference_code,uo.profile_image,u.id as user_id,u.email as user_email,u.mobile,u.acc_number,u.user_barcode_image,u.country as user_country FROM ".$this->users_otp_table." uo LEFT JOIN ".$this->user_account." u ON u.id = uo.customer_id WHERE uo.otp = '$otp' AND uo.mobile_number = '$mobile_number'";
            $result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
			$row = $this->_db->my_fetch_object($result);
            $app_version = (isset($_REQUEST['avc'])) ? $_REQUEST['avc'] : "";
            $os = (isset($_REQUEST['os_version'])) ? $_REQUEST['os_version'] : "";
            $device_type = (isset($_REQUEST['device_type'])) ? $_REQUEST['device_type'] : "";
            $mobileNum = $row->mobile_number; 
            if(!empty($mobileNum) && (($device_type == 'ios' && $app_version >= 2.8) || ($device_type == 'android' && $app_version >= 30)) ){
                $mobileFirst = substr($mobileNum,0,1);
                if($mobileFirst != '0'){
                    $mobileNum = '0'.$mobileNum;
                }
            }
            $mobileNum = (!empty($mobileNum)) ? $mobileNum : 0;
         		// set result params
				$otp_id 			= (!empty($row->otp_id)) ? $row->otp_id : 0;
				$otp_email 			= (!empty($row->email)) ? $row->email : '';
				$firstname 			= (!empty($row->firstname)) ? $row->firstname : '';
				$lastname 			= (!empty($row->lastname)) ? $row->lastname : '';
				$mobile_number 		= (!empty($row->mobile_number)) ? $row->mobile_number : 0;
				$password 			= (!empty($row->password)) ? $row->password : '';
				$date_of_birth 		= (!empty($row->date_of_birth)) ? $row->date_of_birth : '';
				$facebook_id 		= (!empty($row->facebook_id)) ? $row->facebook_id : 0;
				$instagram_id 		= (!empty($row->instagram_id)) ? $row->instagram_id : 0;
				$user_id 			= (!empty($row->user_id)) ? $row->user_id : 0;
				$registered_email 	= (!empty($row->user_email)) ? $row->user_email : '';
				$registered_mobile	= (!empty($row->mobile)) ? $row->mobile : 0;
				$acc_number			= (!empty($row->acc_number)) ? $row->acc_number : 0;
				$user_barcode		= (!empty($row->user_barcode_image)) ? $row->user_barcode_image : '';
				$gender				= (!empty($row->gender)) ? $row->gender : 0;
				$store_id			= (!empty($row->store_id)) ? $row->store_id : 0;
				$country			= (!empty($row->user_country)) ? $row->user_country : '';
				$reference_code			= (!empty($row->reference_code)) ? $row->reference_code : '';
				$profile_image			= (!empty($row->profile_image)) ? $row->profile_image : '';
				if(!empty($user_id)) {
					// check number existance with other users
					$is_number_exist = $this->check_number_existance($user_id,$mobile_number);
					if(!empty($is_number_exist)) {
						$_response['message'] 		= $this->_common->langText($this->_locale,'txt.register.number_already_exist');
						return $_response;
					}
					// update user's verified status
					$where_clause = array("id" => $user_id);
					$update_array = array("is_verified"=>1,"mobile"=>$mobile_number,"update_at"=>date('y-m-d H:i:s'));
					$update = $this->_db->UpdateAll($this->user_account, $update_array, $where_clause, "");
					// prepare user success
					$_response['result_code']   = 1;
					$_response['result'] 		= $this->prepare_user_response($user_id,$firstname,$lastname,$otp_email,$mobileNum,$date_of_birth,'',$user_barcode,$acc_number,$gender,$store_id,$country,$reference_code);
					$_response['message'] 		= $this->_common->langText($this->_locale,'txt.register.account_verified');
				} else {
                    //check reference_code if not empty
                    $reference_customer_id = 0;
                    $reference_device_token = '';
                    $reference_device_type = '';
                    $reference_email = '';
                    $reference_name = '';
                    if(!empty($reference_code)) {
                        $check_reference_code = "SELECT my_reference_code,id,device_type,device_token,firstname,lastname,email FROM ".$this->user_account." WHERE my_reference_code = '".$reference_code."' AND is_verified = 1 AND active = 1";
                        $reference_code_result = $this->_db->my_query($check_reference_code);
                        if ($this->_db->my_num_rows($reference_code_result) > 0) {
                            $reference_code_data = $this->_db->my_fetch_object($reference_code_result);
                            $reference_customer_id	 = (!empty($reference_code_data->id)) ? trim($reference_code_data->id) : 0;
                            $reference_device_token	 = (!empty($reference_code_data->device_token)) ? trim($reference_code_data->device_token) : 0;
                            $reference_device_type	 = (!empty($reference_code_data->device_type)) ? trim($reference_code_data->device_type) : 0;
                            $reference_first_name	 = (!empty($reference_code_data->firstname)) ? trim($reference_code_data->firstname) : '';
                            $reference_surname	 = (!empty($reference_code_data->lastname)) ? trim($reference_code_data->lastname) : '';
                            $reference_email	 = (!empty($reference_code_data->email)) ? trim($reference_code_data->email) : '';
                            $reference_name = $reference_first_name.' '.$reference_surname;
                        }
                    }
					// prepare user data
					$user_data['ACC_EMAIL'] = $otp_email;
					$user_data['ACC_FIRST_NAME']= $firstname;
					$user_data['ACC_SURNAME']   = $lastname;
					$user_data['ACC_PASSWORD']  = $password;
					$user_data['ACC_MOBILE']    = $mobile_number;
					$user_data['ACC_DATE_OF_BIRTH'] = $date_of_birth;
					$user_data['ACC_ADDR_1']    = '';
					$user_data['ACC_POST_CODE'] = '';
					$user_data['ACC_GENDER']    = $gender;
					$user_data['instagram_id']  = $instagram_id;
					$user_data['facebook_id']   = $facebook_id;
					$user_data['store_id']  = $store_id;
					$user_data['reference_code']    = $reference_code;
					$user_data['reference_customer_id']    = $reference_customer_id;
					$user_data['reference_device_token']    = $reference_device_token;
					$user_data['reference_device_type']    = $reference_device_type;
					$user_data['reference_name']    = $reference_name;
					$user_data['reference_email']    = $reference_email;
					$user_data['profile_image']    = $profile_image;
					// insert customer data from temp data
					$insert_temp_user = $this->register_user_from_temp($user_data);
					if($insert_temp_user['result_code'] == 0) {
						$_response['message'] = $insert_temp_user['msg'];
						return $_response;
					} else {
						// prepare user success
                        $my_reference_code    = (!empty($insert_temp_user['my_reference_code'])) ? $insert_temp_user['my_reference_code'] : 0;
						$_response['result_code']  	= 1;
						$_response['result'] 		= $this->prepare_user_response($insert_temp_user['customer_id'],$firstname,$lastname,$otp_email,$mobileNum,$date_of_birth,$insert_temp_user['session_token'],$insert_temp_user['user_barcode'],$insert_temp_user['acc_number'],$gender,$store_id,$country,$my_reference_code,$profile_image);
						$_response['message'] 		= $this->_common->langText($this->_locale,'txt.register.success');
					}
				}
				// remove temp otp log
				if(!empty($otp_id) && $_response['result_code'] == 1) {
					$query = "DELETE FROM ".$this->users_otp_table." WHERE id = '$otp_id'";
					$this->_db->my_query($query);
				}
			} else {
				$_response['message'] 	= $this->_common->langText($this->_locale,'txt.register.invalid_otp');
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to prepare user response
	 * @input customer_id(int) , mobile_number(number)
	 * @return array
	 * @access public
	 *
	 */
	 function prepare_user_response($customer_id=0,$firstname='',$lastname='',$email='',$mobile_number='',$date_of_birth='',$session_token='',$user_barcode='',$acc_number='',$gender=0,$store_id=0,$country=0,$reference_code=0,$profile_image='') {
		 // prepare user response
		$result['customer_id'] 		= $customer_id;
		$result['ACC_FIRST_NAME'] 	= $firstname;
		$result['ACC_SURNAME'] 		= $lastname;
		$result['ACC_EMAIL'] 		= $email;
		$result['ACC_MOBILE'] 		= $mobile_number;
		$result['ACC_DATE_OF_BIRTH']= $date_of_birth;
		$result['ACC_GENDER']		= $gender;
		$result['ACC_STORE']		= $store_id;
		$result['ACC_ADDR_1'] 		= '';
		$result['ACC_POST_CODE'] 	= '';
		$result['session_token'] 	= $session_token;
		$result['country'] 			= $country;
		$result['user_barcode'] 	= IMG_URL.'user_barcodes/'.$user_barcode;
		$result['profile_pic'] 		= IMG_URL.'user_images/user.jpg';
		$result['profile_image'] 		= (!empty($profile_image)) ? IMG_URL.'user_images/'.$profile_image : IMG_URL.'user_images/user.jpg';
		$result['ACC_DATE_ADDED'] 	= date('Y-m-d H:i:s');
		$result['is_push_send'] 	= 1;
		$result['acc_number'] 		= $acc_number;
		$result['my_reference_code'] 		= $reference_code;
		return $result;
	 }
	
	
	/**
	 * Function used to change mobile number
	 * @input customer_id(int) , mobile_number(number)
	 * @return array
	 * @access public
	 *
	 */
	public function change_mobile_number(){

		// get post params	
		$customer_id	= (!empty($_REQUEST['customer_id'])) ? trim($_REQUEST['customer_id']) : '';
		$is_logged_in	= (!empty($_REQUEST['is_logged_in'])) ? trim($_REQUEST['is_logged_in']) : 0;
		$temp_customer_id = (!empty($_REQUEST['temp_customer_id'])) ? trim($_REQUEST['temp_customer_id']) : '';	
        $mobile_number 	= (!empty($_REQUEST['ACC_MOBILE'])) ? $_REQUEST['ACC_MOBILE'] : '';
		$current_date 	= CURRENT_TIME;
		// set response param as default
        $_response['result_code'] = 0;
        $_response['is_varified'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.register.invalid_request');	
        
        if((!empty($customer_id) || !empty($temp_customer_id)) && !empty($mobile_number)) {
			// check number existance with other users
			$is_number_exist = $this->check_number_existance($customer_id,$mobile_number);
			if(!empty($is_number_exist)) {
				$_response['message'] 		= $this->_common->langText($this->_locale,'txt.register.number_already_exist');
				return $_response;
			}
			if(!empty($customer_id)) {
				// get user details
				$sql = "SELECT id,email,mobile,date_of_birth,firstname,lastname FROM ".$this->user_account." WHERE id = '$customer_id'";
			} else {
				// get temp user details
				$sql = "SELECT id,email,mobile_number FROM ".$this->users_otp_table." WHERE id = '$temp_customer_id'";
			}
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				// get random otp key
				$otp = $this->random_key(4);
				// send otp to user for authentication
				$sent_otp = $this->send_verify_otp($mobile_number,$otp);
				
				if(!empty($customer_id)) {
					if($is_logged_in == 1) {
						// set DOB
						$email 			= (!empty($row->email)) ? $row->email : '';
						$date_of_birth 	= (!empty($row->date_of_birth)) ? $row->date_of_birth : '';
						$firstname		= (!empty($row->firstname)) ? $row->firstname : '';
						$lastname 		= (!empty($row->lastname)) ? $row->lastname : '';
						// remove temp otp log
						$query = "Delete FROM ".$this->users_otp_table." WHERE customer_id = $customer_id";
						$this->_db->my_query($query);
						// insert otp details in db, if customer is loggendin
						//$query = "INSERT INTO ".$this->users_otp_table." (email,mobile_number,otp,otp_type,customer_id,date_of_birth,firstname,lastname) VALUES('".mysql_real_escape_string($email)."','".mysql_real_escape_string($mobile_number)."','".mysql_real_escape_string($otp)."','2','".$customer_id."','".mysql_real_escape_string($date_of_birth)."','".mysql_real_escape_string($firstname)."','".mysql_real_escape_string($lastname)."')";
                        $query = "INSERT INTO ".$this->users_otp_table." (email,mobile_number,otp,otp_type,customer_id,date_of_birth,firstname,lastname) VALUES('".mysqli_real_escape_string($this->_db->_con,$email)."','".mysqli_real_escape_string($this->_db->_con,$mobile_number)."','".mysqli_real_escape_string($this->_db->_con,$otp)."','2','".$customer_id."','".mysqli_real_escape_string($this->_db->_con,$date_of_birth)."','".mysqli_real_escape_string($this->_db->_con,$firstname)."','".mysqli_real_escape_string($this->_db->_con,$lastname)."')";
                        if($this->_db->my_query($query)) {
							$user_otp_id = $this->_db->mysqli_insert_id();
						}
						$_response['is_varified'] 	= 1;
					} else {
						// update otp details
						$update_sql = "UPDATE ".$this->users_otp_table." SET otp = '$otp',mobile_number='$mobile_number' WHERE otp_type = 2 AND customer_id = $customer_id";
						$updatesqlResult = $this->_db->my_query($update_sql);
					}
				} else {
					// update otp details
					$update_sql = "UPDATE ".$this->users_otp_table." SET otp = '$otp',mobile_number='$mobile_number' WHERE id = '$temp_customer_id'";
					$updatesqlResult = $this->_db->my_query($update_sql);
				}
				
				// set response params
				$_result['customer_id']	 	= $customer_id;
				$_result['temp_customer_id']= $temp_customer_id;
				$_result['otp']			 	= $otp;
				$_result['ACC_MOBILE']		= $mobile_number;
				$_response['result'] 		= $_result;
				$_response['result_code'] 	= 1;
				$_response['message'] 		= $this->_common->langText($this->_locale,'txt.register.mobile_otp_updated');
			} else {
				$this->_common->langText($this->_locale,'txt.register.invalid_otp');
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to check user's mobile number existance
	 * @input customer_id(int) , mobile_number(number)
	 * @return array
	 * @access public
	 *
	 */
	private function check_number_existance($customer_id=0,$mobile_number=''){
		$is_number_exist = 0;
		// get user details
		$sql = "SELECT id FROM ".$this->user_account." WHERE mobile = '$mobile_number'";
		if(!empty($customer_id)) {
			$sql .= " AND id <> '$customer_id'";
		}
		$result = $this->_db->my_query($sql);
		if ($this->_db->my_num_rows($result) > 0) {
			$is_number_exist = 1;
		}
		return $is_number_exist;
	}
	
	/**
	 * Function used to resend otp
	 * @input customer_id(int) , mobile_number(number)
	 * @return array
	 * @access public
	 *
	 */
	public function resend_otp(){

		// get post params
        $mobile_number 	= (!empty($_REQUEST['ACC_MOBILE'])) ? $_REQUEST['ACC_MOBILE'] : '';
        $otp_type 		= (!empty($_REQUEST['otp_type'])) ? $_REQUEST['otp_type'] : '';
		// set response param as default
        $_response['result_code'] = 0;
		$_response['message'] = $this->_common->langText($this->_locale,'txt.register.invalid_request');	
        if(!empty($mobile_number)) {
			// get otp details
			$sql = "SELECT uo.id as otp_id,uo.email,uo.mobile_number FROM ".$this->users_otp_table." uo WHERE uo.mobile_number = '$mobile_number'";
			$result = $this->_db->my_query($sql);
			if ($this->_db->my_num_rows($result) > 0) {
				$row = $this->_db->my_fetch_object($result);
				// get random otp key
				$otp = $this->random_key(4);
				// send otp to user for authentication
				$sent_otp = $this->send_verify_otp($mobile_number,$otp);
				// update otp details
				$update_sql = "UPDATE ".$this->users_otp_table." SET otp = '$otp' where mobile_number = '$mobile_number'";
				$updatesqlResult = $this->_db->my_query($update_sql);
				// set response params
				$_result['otp']			 = $otp;
				$_result['ACC_MOBILE']	 = $mobile_number;
				$_response['result'] 	 = $_result;
				$_response['result_code'] = 1;
				$_response['is_varified'] = 0;
				$_response['message'] = $this->_common->langText($this->_locale,'txt.register.otp_success');
			} else {
				$this->_common->langText($this->_locale,'txt.register.invalid_otp');
			}
		}
		return $_response;
	}
	
	/**
	 * Function used to register customer from temp details
	 * @param mobile_number(number)
	 * @return array
	 * @access public
	 * 
	 */
	function register_user_from_temp($user_data=array()) {
		// set user data
		$ACC_EMAIL 		= $user_data['ACC_EMAIL'];
		$ACC_FIRST_NAME = $user_data['ACC_FIRST_NAME'];
		$ACC_SURNAME	= $user_data['ACC_SURNAME'];
		$ACC_PASSWORD 	= $user_data['ACC_PASSWORD'];
		$ACC_MOBILE 	= $user_data['ACC_MOBILE'];
		$ACC_DATE_OF_BIRTH = ($user_data['ACC_DATE_OF_BIRTH'] != '0000-00-00') ? date('Y-m-d',strtotime($user_data['ACC_DATE_OF_BIRTH'])) : '';
		$ACC_ADDR_1 	= $user_data['ACC_ADDR_1'];
		$ACC_POST_CODE 	= $user_data['ACC_POST_CODE'];
		$ACC_GENDER 	= $user_data['ACC_GENDER'];
		$instagram_id 	= $user_data['instagram_id'];
		$facebook_id 	= $user_data['facebook_id'];
		$store_id 		= $user_data['store_id'];
		$reference_code = (!empty($user_data['reference_code'])) ? $user_data['reference_code'] : 0;
		$reference_customer_id = (!empty($user_data['reference_customer_id'])) ? $user_data['reference_customer_id'] : 0;
		$reference_device_token = (!empty($user_data['reference_device_token'])) ? $user_data['reference_device_token'] : '';
		$reference_device_type = (!empty($user_data['reference_device_type'])) ? $user_data['reference_device_type'] : '';
		$reference_name = (!empty($user_data['reference_name'])) ? $user_data['reference_name'] : '';
		$reference_email = (!empty($user_data['reference_email'])) ? $user_data['reference_email'] : '';
		$profile_image = (!empty($user_data['profile_image'])) ? $user_data['profile_image'] : '';
        
		$current_date 	= CURRENT_TIME;
		$device_type 	= $this->_common->test_input($_REQUEST['device_type']);
        $device_token 	= $this->_common->test_input($_REQUEST['device_token']);
		$user_result['result_code'] = 0;
		if (!empty($user_data)) {
			
			$is_email_exists = $this->_common->check_user_email($ACC_EMAIL, '0'); // check email id exits or not
			if ($is_email_exists == TRUE) { // if email address found in database
				$user_result['msg'] = $this->_common->langText($this->_locale,'txt.register.error.unique');
			} else {
				$this->_common->clean_device_token($device_token); //clear privous device tokens from database
				// insert user data in table
				$query = "INSERT INTO " . $this->user_account . " (email, password_hash, mobile, firstname, lastname, date_of_birth, created_on, update_at,device_token,device_type,facebook_id,instagram_id,last_login_at,is_verified,gender,reference_code,profile_image,reward_card_id,store_id) VALUES ('$ACC_EMAIL', '".md5($ACC_PASSWORD)."', '$ACC_MOBILE', '$ACC_FIRST_NAME', '$ACC_SURNAME', '$ACC_DATE_OF_BIRTH','$current_date','$current_date','$device_token','$device_type','$facebook_id','$instagram_id','$current_date',1,'$ACC_GENDER','$reference_code','$profile_image','1','$store_id')";
				$this->_db->my_query($query);
				$account_id = $this->_db->my_query_id();
				log_message("debug", "common file included"); // -- Log Debug Message --
				if ($account_id) {
					define('ACCESSKEY',$this->_config->getKeyValue("access_key"));
					$sync_mssql = new sync_mssql();  // create instance syn_mssql class--
                    $acc_number = $sync_mssql->sync_mssql_user($user_data);
					if(!empty($acc_number) ) {
						$session_token = uniqid();
						$barcode = $acc_number; 
						include_once("ean_13_barcode.php"); // include to generate user barcode
						$user_barcode = $image_name;
						// get barcode final digit
						$barcode_digits = $this->_common->get_barcode_digits($barcode);
						$acc_status = $sync_mssql->mssql_user_status($acc_number);
						$updateArray = array("acc_number" => $acc_number,"session_token" => $session_token,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode,"status" => $acc_status);
						$whereClause = array("id" => $account_id);
						$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");			
						// insert user data in user member table 
						$account_member_query = "INSERT INTO ".$this->user_member."(MEMB_NUMBER,MEMB_LOYALTY_TYPE,MEMB_ACCUM_POINTS_IND,MEMB_POINTS_BALANCE,MEMB_FLAGS) VALUES ('$account_id','0','1','0','0')";
						$this->_db->my_query($account_member_query);
						// prepare user result data
						$user_result['customer_id'] 	= $account_id;
						$user_result['session_token'] 	= $session_token;
						$user_result['user_barcode'] 	= $user_barcode;
						$user_result['ACC_DATE_ADDED'] 	= $current_date;
						$user_result['is_push_send'] 	= 1;
						$user_result['acc_number'] 		= $acc_number;
						$user_result['result_code']  	= 1;
						$user_result['msg'] = $this->_common->langText($this->_locale,'txt.register.success');
                        $my_reference_code = ''; //as user signup so reference code would be null
						$my_reference_code = $this->_common->update_referral_code($account_id,$ACC_FIRST_NAME,$my_reference_code);
                        $user_result['my_reference_code']  	= $my_reference_code;
                        // add point to refer by user
                        if(!empty($reference_code) && !empty($reference_customer_id)) {
                                    //$get_reward_points = $this->_common->get_reward_config('referral_invitation_points');
                           /* $get_reward_points = 'referral_invitation_points';
                            $table = $this->reward_config; 
                            $columns = ['referral_invitation_points'];
                            $get_record = $this->_common->get_table_record($table,$columns);
                            $referral_invitation_points = 0;
                            if(!empty($get_record)) {
                                $referral_invitation_points = (!empty($get_record[0]->referral_invitation_points)) ? $get_record[0]->referral_invitation_points : 0;
                            }
                            */
                            $get_reward_points = 'referral_invitation_points';
                            $today_date = CURRENT_DATE;
                            $reward_campaign_sql = "SELECT id,reward_points FROM ".$this->reward_campaign_table." where status = '1' AND is_deleted = '0' AND action_type = '1' AND start_date <= '".$today_date."' AND end_date >= '".$today_date ."'  " ;
                            $get_reward_campaign = $this->_db->my_query($reward_campaign_sql);
                            $reward_campaign = $this->_db->my_fetch_object($get_reward_campaign);
                            
                            $campaign_id = (!empty($reward_campaign->id)) ? $reward_campaign->id : 0;
                            $referral_invitation_points = (!empty($reward_campaign->reward_points)) ? $reward_campaign->reward_points : 0;
                            
                            //check camapaign has reward adn can be assign to user or not
                            $is_reward_assign = $this->_common->manageCompaignLog($reference_customer_id,$campaign_id);
                            
                            if(!empty($is_reward_assign)) {
                                /*$record_transaction_query = "INSERT INTO ".$this->reward_transaction." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                                '".mysql_real_escape_string($reference_customer_id)."',
                                '".mysql_real_escape_string($referral_invitation_points)."',
                                '".mysql_real_escape_string($get_reward_points)."',
                                '".date('y-m-d H:i:s')."',
                                '2')"; // 2 received or success
                                */
                                $record_transaction_query = "INSERT INTO ".$this->reward_transaction." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                                '".mysqli_real_escape_string($this->_db->_con,$reference_customer_id)."',
                                '".mysqli_real_escape_string($this->_db->_con,$referral_invitation_points)."',
                                '".mysqli_real_escape_string($this->_db->_con,$get_reward_points)."',
                                '".date('y-m-d H:i:s')."',
                                '2')"; // 2 received or success
                                
                                $this->_db->my_query($record_transaction_query);
                                // push message
                                $refered_user_name = $ACC_FIRST_NAME.' '.$ACC_SURNAME;
                                $refered_user_name = ucwords($refered_user_name);
                                $push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.referral_invitation_points'),$referral_invitation_points,$refered_user_name); 
                                
                                $notification_data = ['user_id'=>$reference_customer_id,'notification'=>$push_message,'notification_type'=>$get_reward_points,'reward_point'=>$referral_invitation_points];	
                                $this->_common->save_notification($notification_data);
                            }                            
                        }
                        // set first time registration point to user
                        //$get_reward_points = $this->_common->get_reward_config('registration_points');
                        $get_reward_points = 'registration_points';
                        $table = $this->reward_config; 
                        $columns = ['registration_points'];
                        $get_record = $this->_common->get_table_record($table,$columns);
                        $registration_points = 0;
                        if(!empty($get_record)) {
                            $registration_points = (!empty($get_record[0]->registration_points)) ? $get_record[0]->registration_points : 0;
                        }
                        $record_transaction_query = "INSERT INTO ".$this->reward_transaction." (user_id,balance_point,transaction_type,transaction_date,status) VALUES(
                        '".mysqli_real_escape_string($this->_db->_con,$account_id)."',
                        '".mysqli_real_escape_string($this->_db->_con,$registration_points)."',
                        '". ($get_reward_points)."',
                        '".date('y-m-d H:i:s')."',
                        '2')"; // 2 received or success
                        $this->_db->my_query($record_transaction_query);
                        // push message
                        $push_message = sprintf($this->_common->langText($this->_locale,'txt.reward.registration_points'),$registration_points); 
                        $notification_data = ['user_id'=>$account_id,'notification'=>$push_message,'notification_type'=>$get_reward_points,'reward_point'=>$registration_points];	
                        $this->_common->save_notification($notification_data);


					} else {
						$user_result['msg'] = $this->_common->langText($this->_locale,'txt.error.insert');
					}
				} else {
					$user_result['msg'] 	= $this->_common->langText($this->_locale,'txt.error.insert');
				}
			}   
        }
        return $user_result;
	}
        /*
     * @assignRewardLoyaltyCard is to assign loyalty reward card to user
     * @input acc_number(bigint)
     * @output void
     **/
    
    public function assignRewardLoyaltyCard($acc_number = '',$existong_reward_card_id=0) {
    
        //get the transaction id w.r.t acc_number
        $user = "select JNLD_TRX_NO from JNLDTBL WHERE JNLD_TYPE = 'MEMBER' AND JNLD_PRODUCT = ".$acc_number; // Direct Fetch Member Details 
        $user_data = $this->_mssql->my_mssql_query($user,$this->con);
        $JNLD_TRX_NOS ='';
        $amount = 0;
        
        //collect the transaction id comma separated
        while($result = $this->_mssql->mssql_fetch_object_query($user_data)) {
            $JNLD_TRX_NO = (!empty($result->JNLD_TRX_NO)) ? $result->JNLD_TRX_NO : '';
            $JNLD_TRX_NOS .= $JNLD_TRX_NO.',';
            
        }
        $JNLD_TRX_NOS = rtrim($JNLD_TRX_NOS,',');
        
        //get total amount from transaction ids
        $trans_query = "select SUM(JNLD_AMT) as amount from JNLDTBL WHERE JNLD_TYPE = 'SALE' AND JNLD_TRX_NO IN (".$JNLD_TRX_NOS.")"; // Direct Fetch Member Details 
        $trans_data = $this->_mssql->my_mssql_query($trans_query,$this->con);
        while($trans_result = $this->_mssql->mssql_fetch_object_query($trans_data)) {
            $amount = (!empty($trans_result->amount)) ? $trans_result->amount : 0;
        }
        //get eligible reward card for the user                     
        $reward_card_query = 'SELECT  id  FROM ava_reward_cards WHERE min_purchase_amount <='.$amount.' AND max_purchase_amount >'.$amount.' AND status = "1" AND is_deleted = "0" ORDER BY max_purchase_amount DESC';
        $reward_card_result = $this->_db->my_query($reward_card_query);
        $reward_card_row = $this->_db->my_fetch_object($reward_card_result);
        $reward_card_id = (!empty($reward_card_row->id)) ? $reward_card_row->id : 0;
        
        //assign the reward card to user
        if($reward_card_id > $existong_reward_card_id) {
			$updateArray = ["reward_card_id" => $reward_card_id];
			$whereClause = ['acc_number'=>$acc_number];
			$update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");
		} else {
			$reward_card_id =  $existong_reward_card_id;
		}
        return $reward_card_id;
    } //end assignRewardLoyaltyCard
    
	
	/**
	 * Function used to send otp
	 * @input mobile_number(number)
	 * @return bool
	 * @access public
	 *
	 */
	 private function send_verify_otp($mobile_number='',$otp='') {
		// set OTP message text
		$otp_message = 'Your NightOwl verification code is '.$otp.'. Please enter it in your NightOwl app to verify your account. Thank you.';
		// send otp to user for authentication
		$sent_otp = $this->send_otp($mobile_number,$otp,$otp_message);
		return $sent_otp;
	 }
	 
	 function test_otp() {
		// get Twilio auth keys
		$account_sid 	= 'ACb5e11f816a1cbffa807441fe3ee7bae1';
		$auth_token 	= '93ef6bdeea59d01f23216d802ebc0563';
		$from_no 		= 'nightowl';
		$to_phone_no	= '0424707815';
		// include twilio class         
		require('./twillo/Services/Twilio.php');
		$client = new Services_Twilio($account_sid, $auth_token);
		$countryCode = '+61';
		$message = 'Test SMS';
		if(!empty($to_phone_no)){
			try{
				// send otp in SMS
				$sendMesageDetail = $client->account->messages->create(array(
					'To' => trim($countryCode).''.trim($to_phone_no),
					'From' => $from_no,
					'Body' => $message,
				));
				$sent_otp = true;
			}catch(Exception $e){
				$sent_otp = false;
			}
		}
		
		echo $sent_otp;die;
	 }
	
    
    public function insert_acc_number() {
        $current_date 		= CURRENT_TIME;
        //$user_array = ['60926','60927','60928','60929','60930','60931','116'];
        $user_array = ['116'];
        foreach($user_array as $customerId) {
            if(!empty($customerId)) {
                $sql = "SELECT * FROM " . $this->user_account . " WHERE id = '".$customerId."'";
                $sqlResult = $this->_db->my_query($sql);
				if ($this->_db->my_num_rows($sqlResult) > 0) {
                    $user_data = array();
                    $result = $this->_db->my_fetch_object($sqlResult);
                    $account_id 				= (isset($result->id)) ? trim($result->id) : ""; 
                    $user_data['ACC_EMAIL'] 				= (isset($result->email)) ? trim($result->email) : ""; 
                    $user_data['ACC_PASSWORD'] 		= (isset($result->password_hash)) ? trim($result->password_hash) : ""; 
                    $user_data['ACC_FIRST_NAME'] 	= (isset($result->firstname)) ? trim($result->firstname) : "";
                    $user_data['ACC_SURNAME'] 		= (isset($result->lastname)) ? trim($result->lastname) : "";
                    $user_data['ACC_MOBILE'] 		= (isset($result->mobile)) ? trim($result->mobile) : "";
                    $user_data['ACC_DATE_OF_BIRTH'] 	= (isset($result->date_of_birth)) ? trim($result->date_of_birth) : "";
                    $user_data['ACC_ADDR_1'] 		= (isset($result->address)) ? trim($result->address) : "";
                    $user_data['ACC_POST_CODE'] 		= (isset($result->post_code)) ? trim($result->post_code) : "";
                    $user_data['ACC_GENDER'] 		= (isset($result->gender)) ? trim($result->gender) : "";
                    $user_data['ACC_DATE_ADDED'] 	= (isset($current_date)) ? trim($current_date) : "";
                    
                    define('ACCESSKEY',$this->_config->getKeyValue("access_key"));
                    $sync_mssql = new sync_mssql();  // create instance syn_mssql class--
                    $acc_number = $sync_mssql->sync_mssql_user($user_data);
                    
                    if(!empty($acc_number) ) {
                        $session_token = uniqid();
                        $barcode = $acc_number; 
                        include_once("ean_13_barcode.php"); // include to generate user barcode
                        $user_barcode = $image_name;
                        // get barcode final digit
                        $barcode_digits = $this->_common->get_barcode_digits($barcode);
                        $acc_status = $sync_mssql->mssql_user_status($acc_number);
                        $updateArray = array("acc_number" => $acc_number,"session_token" => $session_token,"user_barcode" => $barcode_digits,"user_barcode_image" => $user_barcode,"status" => $acc_status);
                        $whereClause = array("id" => $account_id);
                        $update = $this->_db->UpdateAll($this->user_account, $updateArray, $whereClause, "");	
                    }
                }
            }
        }
        print_r($user_array);die;
    }

} // End users class
?> 
