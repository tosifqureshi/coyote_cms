<?php

	/**
	 * -----------------------------------------
	 * Generate QR Code
	 * -----------------------------------------
	 */
	//DIRECTORY_SEPARATOR
	$png_temp_dir = realpath(dirname(__FILE__) . '/..'.DIRECTORY_SEPARATOR.'uploads/qr_codes/');
    include "Qr_code/qrlib.php"; 
	//ofcourse we need rights to create temp dir
    if (!file_exists($png_temp_dir))
        mkdir($png_temp_dir);
    
	// set qr code image name
	$filename = $png_temp_dir.DIRECTORY_SEPARATOR.$qr_filename;
	// create qr code
	QRcode::png($qr_code, $filename, $error_correction_level, $matrix_point_size, 2);
?>
